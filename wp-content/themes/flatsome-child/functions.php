<?php
remove_action('template_redirect', 'redirect_canonical');
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );