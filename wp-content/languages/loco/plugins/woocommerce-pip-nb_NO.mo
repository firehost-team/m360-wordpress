��    0      �              ;        Y     i     z  	   �     �     �     �     �     �     �  L   �     '     5     =     L      \     }     �     �     �     �     �     �     �     �     �                    '     /     3     D  	   T     ^  
   k  	   v  
   �     �     �     �     �     �     �     �     �  �  �  >   �     )     8     F     T     \     b     q     �     �     �     �     �     �     �     �  !   �     	     	     !	     :	     F	     Z	  
   f	     q	     z	     	     �	     �	     �	  	   �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     
     
  
   
     '
     3
     ?
     K
   %1$sCoupon used:%3$s %4$s %1$sCoupons used (%2$s):%3$s %4$s Billing Address Customer Details Customer details Discount: Email Email Invoice Email Packing List Email Pick List Email: Emails Facsimile label. Try to keep this below 16 characters length.Sample Invoice Free Shipping Invoice Invoice Number Invoice number: Invoice/Packing List (Order #%s) Invoice: %s Invoices Invoices/Packing Lists No shipping Order Date: %s Packing List Packing Lists Payment Method: Price Print Invoice Print Packing List Product Quantity Refund: SKU Shipping Address Shipping Method Shipping: Shop Address State Tax: Subtotal: Telephone: Total Weight Total: Totals: VAT Number: View Invoice View invoice View your invoice. View your invoice: %s Project-Id-Version: WooCommerce Print Invoices/Packing Lists 3.5.2
Report-Msgid-Bugs-To: https://woocommerce.com/my-account/marketplace-ticket-form/
POT-Creation-Date: 2018-05-03 14:23:12+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-06-16 12:31+0000
Last-Translator: pineno <wordpress@zebramedia.no>
Language-Team: Norsk bokmål
Language: nb_NO
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Loco https://localise.biz/ %1$sBrukt kupong:%3$s %4$s %1$sBrukt kuponger (%2$s):%3$s %4$s fakturaadresse Kundedetaljer Kundedetaljer Rabatt: Epost Send e-faktura Epost pakke liste Send Pakkeliste e-post Epost: Eposter Eksempelfaktura Gratis frakt Faktura Fakturanummer Fakturanummer Faktura / Pakke liste (Ordre #%s) Faktura: %s Fakturas Fakturaer/Pakkingslister Ingen frakt Bestillingsdato: %s Pakke liste Pakkeliste Betaling Pris Skrivut faktura Skriv ut pakkeliste Produkt Stk Refusjon: Varenummer Sendingsadresse Fraktmetoder Frakt: Butikkadresse Mva: Delsum: Telefon: Vekt: Sum: Sum: MVA nummer Vis faktura Vis faktura Vis faktura Vis faktura: %s 