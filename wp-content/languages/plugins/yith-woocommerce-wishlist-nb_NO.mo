��          �               %        C     S     c  &   l     �     �     �     �     �  ,   �       '     
   @     K  �  T  .        ;     P  	   a  $   k     �     �     �     �     �  *   �     �  $        *     /   Add the selected products to the cart Add to Wishlist Browse Wishlist In Stock No products were added to the wishlist Out of Stock Product Name Select options Show Stock status Show Unit price Show unit price for each product in wishlist Stock Status The product is already in the wishlist! Unit Price Wishlist Project-Id-Version: YITH WooCommerce Wishlist
POT-Creation-Date: 2018-10-24 17:52:58+00:00
PO-Revision-Date: 2018-11-12 00:16+0000
Last-Translator: nobody <Server@m360.no>
Language-Team: Norsk bokmål
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: ../plugin-fw
Report-Msgid-Bugs-To:  Legg til det valgte produktet til handlekurven Legg til Ønskeliste Se i ønskeliste På lager Ingen varer er lagt til ønskelisten Utsolgt Produktnavn Legg til produkt Vis lagerstatus Vis pris Vis pris for hver av varene i ønskelisten Lagerstatus Produktet er allerede i ønskelisten Pris Ønskeliste 