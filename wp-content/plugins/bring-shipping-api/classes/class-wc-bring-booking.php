<?php

if (!defined( 'ABSPATH' ))
{
    exit; // Exit if accessed directly
}

class WC_Bring_Booking extends WC_Integration
{
    const CUSTOMER_ENDPOINT = 'https://api.bring.com/booking/api/customers';
    const BOOKING_ENDPOINT  = 'https://api.bring.com/booking/api/booking';
    /**
     * @var WooCommerce
     */
    protected $wooCommerce;

    /**
     * @var WC_Bring
     */
    protected $bring_settings;

    /**
     * @var bool
     */
    protected $mybringEnabled;

    /**
     * @var string
     */
    protected $apiEmail;

    /**
     * @var string
     */
    protected $apiKey;

    public $NO_MYBRING_PRODUCTS = [
      'SERVICEPAKKE'                        => 'SERVICEPAKKE',
      'EKSPRESS09'                          => 'EKSPRESS09',
      'BPAKKE_DOR-DOR'                      => 'BPAKKE_DOR-DOR',
      'PA_DOREN'                            => 'PA_DOREN',
      'BPAKKE_DOR-DOR_RETURSERVICE'         => 'BPAKKE_DOR-DOR_RETURSERVICE',
      'EKSPRESS09_RETURSERVICE'             => 'EKSPRESS09_RETURSERVICE',
      'SERVICEPAKKE_RETURSERVICE'           => 'SERVICEPAKKE_RETURSERVICE',
      'MINIPAKKE'                           => 'MINIPAKKE',
      'CARRYON_BUSINESS'                    => 'CARRYON_BUSINESS',
      'CARRYON_HOMESHOPPING'                => 'CARRYON_HOMESHOPPING',
      'CARRYON_BUSINESS_PALLET'             => 'CARRYON_BUSINESS_PALLET',
      'CARRYON_BUSINESS_HALFPALLET'         => 'CARRYON_BUSINESS_HALFPALLET',
      'CARRYON_BUSINESS_QUARTERPALLET'      => 'CARRYON_BUSINESS_QUARTERPALLET',
      'CARRYON_BUSINESS_BULKSPLIT'          => 'CARRYON_BUSINESS_BULKSPLIT',
      'CARRYON_BUSINESS_BULKSPLIT_0900'     => 'CARRYON_BUSINESS_BULKSPLIT_0900',
      'CARRYON_HOMESHOPPING_BULKSPLIT'      => 'CARRYON_HOMESHOPPING_BULKSPLIT',
      'CARRYON_HOMESHOPPING_BULKSPLIT_HOME' => 'CARRYON_HOMESHOPPING_BULKSPLIT_HOME',
      'CARRYON_HOMESHOPPING_BULKSPLIT_MINI' => 'CARRYON_HOMESHOPPING_BULKSPLIT_MINI',
    ];

    public function __construct()
    {
        global $woocommerce;
		
        $this->wooCommerce = $woocommerce;

        $this->wooCommerce->shipping()->load_shipping_methods();

        $this->id                 = 'bring_booking';
        $this->method_title       = __( 'MyBring', WC_Bring::TEXT_DOMAIN );
        $this->method_description = __( 'Integration with mybring.com for automatic generation of shipping labels', WC_Bring::TEXT_DOMAIN );

        $this->init_form_fields();
        $this->init_settings();

        $this->bring_settings = new WC_Bring();
        $this->mybringEnabled = $this->get_option( 'mybring_enabled' ) === 'yes';

        $this->apiEmail = $this->get_option( 'mybring_api_email' );
        $this->apiKey   = $this->get_option( 'mybring_api_key' );
		
        add_action( 'woocommerce_order_status_processing', [ $this, 'getBookingLabel' ] );
        add_action( 'woocommerce_update_options_integration_' . $this->id, [ $this, 'process_admin_options' ] );
        add_filter( 'woocommerce_admin_order_actions', [ $this, 'showBookingLabel' ], 10, 2 );

    }

    public function showBookingLabel( array $actions, WC_Order $order )
    {
        if ($url = get_post_meta( $order->id, '_tracking_label', true ))
        {
            $actions[] = [
              'url'    => $url,
              'name'   => __( 'Tracking label', WC_Bring::TEXT_DOMAIN ),
              'action' => 'tracking-label',
            ];
        }

        $styles = plugins_url( '../stylesheet.css', __FILE__ );
        wp_enqueue_style( 'wc-bring-booking', $styles );

        return $actions;
    }

    public function init_form_fields()
    {
        $this->form_fields = [
          'mybring_enabled'     => [
            'title'       => __( 'Mybring integration', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'description' => __( 'Enable MyBring integration to get pre-made shipping labels and automatic invoicing from bring', WC_Bring::TEXT_DOMAIN ),
            'default'     => 'off',
          ],
          'mybring_testing'     => [
            'title'       => __( 'Mybring Testing', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'description' => __( 'This will generate test labels, and no charges will be incur at mybring', WC_Bring::TEXT_DOMAIN ),
            'default'     => 'on',
          ],
          'mybring_api_email'   => [

            'title'       => __( 'Api email', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This is the email you use to login mybring.com".', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',

          ],
          'mybring_api_key'     => [

            'title'       => __( 'Api key', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'You can find your API key on your profile on mybring.com, there\'s a tab called "My API-Key".', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',

          ],
          'sender_name'         => [
            'title'       => __( 'Sender name', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the name in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_addressline1' => [
            'title'       => __( 'Sender address line 1', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the addresss line 1 in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_addressline2' => [
            'title'       => __( 'Sender address line 2', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the addresss line 2 in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_zip_code'     => [
            'title'       => __( 'Sender zip code', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the zip code in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_zip_name'     => [
            'title'       => __( 'Sender zip name/city', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the zip name/city in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_email'        => [
            'title'   => __( 'Sender email', WC_Bring::TEXT_DOMAIN ),
            'type'    => 'text',
            'default' => '',
          ],
          'sender_phone'        => [
            'title'   => __( 'Sender phone', WC_Bring::TEXT_DOMAIN ),
            'type'    => 'text',
            'default' => '',
          ],
        ];
    }

    protected function validate_mybring_enabled_field( $key )
    {
        if ($this->is_mybring_enabled())
        {
            $shipping_methods = $this->bring_settings->get_option( 'noproductstype' );
            $unsupported      = array_diff( $shipping_methods, array_keys( $this->NO_MYBRING_PRODUCTS ) );

            if (count( $unsupported ) > 0)
            {
                $this->errors[] = sprintf(
                  __( "%s is/are not supported by bring, and can't be used with mybring integration.", WC_Bring::TEXT_DOMAIN ),
                  join( ',', $unsupported )
                );

                return 'no';
            }
        }

        return $this->validate_checkbox_field( $key );
    }

    protected function validate_mybring_api_email_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_mybring_api_key_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_name_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_addressline1_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_zip_code_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_zip_name_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_email_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_sender_phone_field( $key )
    {
        return $this->validate_all( $key );
    }

    protected function validate_all( $key )
    {
        $fullKey = $this->get_field_key( $key );

        if ($this->is_mybring_enabled())
        {
            if (isset( $_POST[ $fullKey ] ))
            {
                $value = $_POST[ $fullKey ];

                if (strlen( (string) $value ) < 1)
                {
                    $this->errors[] = sprintf( __( '%s is missing', WC_Bring::TEXT_DOMAIN ), $this->form_fields[ $key ][ 'title' ] . ' is missing' );
                }

                return $value;
            }
        }

        return '';
    }


    protected function is_mybring_enabled()
    {
        return $this->validate_checkbox_field( 'mybring_enabled' ) === 'yes';
    }

    public function display_errors()
    {
        foreach ($this->errors as $key => $value)
        {
            ?>
            <div class="error">
                <p><?php _e( $value, WC_Bring::TEXT_DOMAIN ); ?></p>
            </div>
            <?php
        }
    }

    public function getBookingLabel( $order_id )
    {

        if (!$this->mybringEnabled)
        {
            return;
        }

        $order = new WC_Order( $order_id );
		
		if (!$order->needs_shipping_address())
        {
            WC_Admin_Settings::add_error( 'No shipping necessary for this order' );

            return;
        }

        $packageType    = $this->getPackageType( $order );
		
        $customerNumber = $this->getBringCustomerNumber( $order, $packageType );

        $bookingObject = $this->createBookingObject( $order, $customerNumber, $packageType );

//        var_dump( $bookingObject );

        $response = wp_remote_post( self::BOOKING_ENDPOINT, [
          'method'  => 'POST',
          'headers' => $this->getHeaders(),
          'body'    => json_encode( $bookingObject ),
        ] );

        if (!is_wp_error( $response ))
        {
            $body = json_decode( $response[ 'body' ] );
            foreach ($body->consignments as $consignment)
            {
                $confirmation = $consignment->confirmation;
                if (!is_null( $confirmation ))
                {
                    $deliveryDate = ( new DateTime( sprintf( '@%d', $confirmation->dateAndTimes->expectedDelivery / 1000 ) ) )->format( 'd.m.Y' );

                    $order->add_order_note( sprintf( 'Expected delivery: %s', $deliveryDate ) );
                    $order->add_order_note(
                      sprintf( 'Label: <a href="%s" target="_blank">%s</a>', esc_attr( $confirmation->links->labels ), $confirmation->links->labels )
                    );
                    $order->add_order_note(
                      sprintf( 'Tracking: <a href="%s" target="_blank">%s</a>', esc_attr( $confirmation->links->tracking ), $confirmation->links->tracking )
                    );
                    $order->add_order_note(
                      sprintf( 'Consignment number: %s', $confirmation->consignmentNumber )
                    );

                    update_post_meta( $order->id, '_tracking_label', $confirmation->links->labels );
					update_post_meta( $order->id, '_tracking_url', $confirmation->links->tracking ); 
                    update_post_meta( $order->id, '_expected_delivery', $deliveryDate ); 
                }
                else
                {
                    foreach ($consignment->errors as $error)
                    {
                        $order->add_order_note(
                          sprintf( sprintf( 'ERROR! Code: %s. Message. %s', $error->code, current( $error->messages )->message ) )
                        );
                    }

                }
            }
        }

    }

    public function createBookingObject( WC_Order $order, $customerNumber, $packageType )
    {
        $weight = 0;
        $height = 0;
        $length = 0;
        $width  = 0;
        foreach ($order->get_items() as $item)
        {
            $product = new WC_Product( $item[ 'product_id' ] );
            if ($product->needs_shipping())
            {
                $weight += $this->bring_settings->get_weight( $product->get_weight() );
                $height += $this->bring_settings->get_dimension( $product->get_height() );
                $length += $this->bring_settings->get_dimension( $product->get_length() );
                $width += $this->bring_settings->get_dimension( $product->get_width() );
            }
        }

        return [
          'schemaVersion' => 1,
          'testIndicator' => $this->get_option( 'mybring_testing' ) === 'yes',
          'consignments'  => [
            [
              'shippingDateTime' => ( time() + ( 60 * 60 ) ) * 1000,
              'parties'          => [
                'sender'      => [
                  'name'         => $this->get_option( 'sender_name' ),
                  'addressLine'  => $this->get_option( 'sender_addressline1' ),
                  'addressLine2' => $this->get_option( 'sender_addressline2' ),
                  'postalCode'   => $this->get_option( 'sender_zip_code' ),
                  'city'         => $this->get_option( 'sender_zip_name' ),
                  'countryCode'  => $this->bring_settings->get_option( 'shopbasecountry' ),
                  'contact'      => [
                    'name'        => $this->get_option( 'sender_name' ),
                    'email'       => $this->get_option( 'sender_email' ),
                    'phoneNumber' => $this->get_option( 'sender_phone' ),
                  ],
                ],
                'recipient'   => [
                  'name'                  => sprintf( '%s %s', $order->shipping_first_name, $order->shipping_last_name ),
                  'addressLine'           => $order->shipping_address_1,
                  'addressLine2'          => $order->shipping_address_2,
                  'postalCode'            => $order->shipping_postcode,
                  'city'                  => $order->shipping_city,
                  'countryCode'           => 'NO',
                  'additionalAddressInfo' => $order->customer_note,
                  'contact'               => [
                    'name'        => sprintf( '%s %s', $order->shipping_first_name, $order->shipping_last_name ),
                    'email'       => $order->billing_email,
                    'phoneNumber' => $order->billing_phone,
                  ],
                ],
                'pickupPoint' => null,
              ],
              'product'          => [
                'id'                 => $packageType,
                'customerNumber'     => $customerNumber,
                'services'           => null,
                'services'           => [
                  'recipientNotification' =>
                    [
                      'email'  => $order->billing_email,
                      'mobile' => $order->billing_phone,
                    ],
                ],
                'customsDeclaration' => null,
              ],
              'purchaseOrder'    => null,
              'correlationId'    => $order->id,
              'packages'         => [
                [
                  'weightInKg' => $weight / 1000.0,
                  'dimensions' => [
                    'heightInCm' => $height,
                    'widthInCm'  => $width,
                    'lengthInCm' => $length,
                  ],
                ],
              ],
            ],
          ],
        ];
    }

    protected function getPackageType( WC_Order $order )
    {
        $bring = current( $this->findBringShipping( $order ) );
		$expBring = explode (':',$bring[ 'method_id' ]);
		isset($this->settings[ 'pickuppoint' ]) ? $this->settings[ 'pickuppoint' ] : null;
		return isset($expBring[1]) ? strtoupper($expBring[1]): null ;
        //return strtoupper( str_replace( 'bring:', '', $bring[ 'method_id' ] ) );
    }

    /**
     * @param WC_Order $order
     * @return string
     */
    protected function getBringCustomerNumber( WC_Order $order, $packageType )
    {
        $response = wp_remote_get( self::CUSTOMER_ENDPOINT, [
          'method'  => 'GET',
          'headers' => $this->getHeaders(),
        ] );

        $result = json_decode( $response[ 'body' ] );
		
        $customer = array_filter( $result->customers, function ( $customer ) use ( $packageType )
        {
            return $customer->countryCode == 'NO' && in_array( $packageType, $customer->products );
        } );

        return count( $customer ) > 0 ? current( $customer )->customerNumber : false;
    }

    protected function getHeaders()
    {
        return [
          'X-MyBring-API-Uid'  => $this->apiEmail,
          'X-MyBring-API-Key'  => $this->apiKey,
          'Content-Type'       => 'application/json',
          'Accept'             => 'application/json',
          'X-Bring-Client-URL' => urlencode( get_site_url() ),
        ];
    }

    protected function findBringShipping( WC_Order $order )
    {
        return array_filter( $order->get_shipping_methods(), function ( array $shipping_method )
        {
            $haystack = $shipping_method[ 'method_id' ];
            $needle   = 'bring';

            return strrpos( $haystack, $needle, -strlen( $haystack ) ) !== false;
        } );
    }
}

