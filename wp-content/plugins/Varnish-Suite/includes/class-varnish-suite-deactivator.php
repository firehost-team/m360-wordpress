<?php

/**
 * Fired during plugin deactivation
 *
 * @link       patrick@trafficfox.no
 * @since      1.0.0
 *
 * @package    Varnish_Suite
 * @subpackage Varnish_Suite/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Varnish_Suite
 * @subpackage Varnish_Suite/includes
 * @author     Traffic Fox <patrick@trafficfox.no>
 */
class Varnish_Suite_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
