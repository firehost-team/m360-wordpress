<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       patrick@trafficfox.no
 * @since      1.0.0
 *
 * @package    Varnish_Suite
 * @subpackage Varnish_Suite/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Varnish_Suite
 * @subpackage Varnish_Suite/includes
 * @author     Traffic Fox <patrick@trafficfox.no>
 */
class Varnish_Suite_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'varnish-suite',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
