<?php

add_filter( 'is_page' , 'my_is_page' );

function my_is_page( $where ) {
    return TRUE;
}

function varnishsuite_load_admin_bar() {
    if(!is_admin()) die;
    echo '<script>jQuery.getScript("/wp-includes/js/admin-bar.min.js");jQuery("<link/>", { rel: "stylesheet", type: "text/css", href: "/wp-includes/css/dashicons.min.css" }).appendTo("head");jQuery("<link/>", { rel: "stylesheet", type: "text/css", href: "/wp-includes/css/admin-bar.css" }).appendTo("head");</script>';
    die;
    
    _wp_admin_bar_init();
    wp_admin_bar_render();
    die;

    # This below is here because it KIND OF worked and who knows if it may be needed sometime
    require_once( ABSPATH . WPINC . '/class-wp-admin-bar.php' );
    $admin_bar_class = apply_filters( 'wp_admin_bar_class', 'WP_Admin_Bar' );
    if ( class_exists( $admin_bar_class ) )
        $wp_admin_bar = new $admin_bar_class;
    else
        return false;


    $wp_admin_bar->initialize();
    $wp_admin_bar->add_menus();

    do_action_ref_array( 'admin_bar_menu', array( &$wp_admin_bar ) );
    $wp_admin_bar->render();
    do_action( 'wp_after_admin_bar_render' );

    die;
}
add_action('wp_ajax_varnishsuite_load_admin_bar', 'varnishsuite_load_admin_bar');