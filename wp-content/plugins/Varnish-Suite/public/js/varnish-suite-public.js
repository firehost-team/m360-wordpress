var varnish_suite = {

    load_admin_bar: function() {
        var data = {
            action: 'varnishsuite_load_admin_bar'
        };

        jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
            jQuery('body').append(response);
            if(response) {
                jQuery('body').css('padding-top', '30px')
                jQuery.get(window.location.href, function(response) {
                    jQuery(response).filter('#wpadminbar').appendTo(jQuery('body'));
                });
            }
        });
    }
};



(function( $ ) {
    'use strict';
    varnish_suite.load_admin_bar();
})( jQuery );