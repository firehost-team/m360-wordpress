<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       patrick@trafficfox.no
 * @since      1.0.0
 *
 * @package    Varnish_Suite
 * @subpackage Varnish_Suite/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
