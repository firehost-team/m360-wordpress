<?php
class SUPERKASSE_ADMIN_Message {
    private $_message;

    function __construct( $message ) {
        $this->_message = $message;

        add_action( 'admin_notices', array( $this, 'render' ) );
    }

    function render() {
        printf( '<div class="updated">%s</div>', $this->_message );
    }
}

function m360_superkasse_install_multisite_notice() {
    echo '<div id="message" class="error fade"><p>';
    '<strong>Superkasse</strong></a> requires multisite installation. Please <a href="http://codex.wordpress.org/Create_A_Network">create a network</a> first, or <a href="plugins.php">deactivate Superkasse</a>.';
    echo '</p></div>';
}



if( ! function_exists( 'superkasse_Logger' ) ) {
    function superkasse_Logger($Text){
        $File = fopen(WP_CONTENT_DIR . '/m360_superkasse.log', 'a');
        fwrite($File, date('d-m-y H:i:s') . substr((string)microtime(), 1, 6) . ' - ' . $Text . "\n");
        fclose($File);
    }
}

// API
if(!function_exists('M360_Superkasse_Add')){
    function M360_Superkasse_Add($site_url){
        $data = array("method"=> "pcknettbutikk","url"=>$site_url);
        $result = M360_Superkasse_CallLicenceAPI("POST",json_encode($data));
        superkasse_Logger(__FUNCTION__.' api result: '.print_r($result,true));
        if(!empty($result)){
            if($result->status == 'ok'){
                new SUPERKASSE_ADMIN_Message($result->message);
            }else{
                new SUPERKASSE_ADMIN_Message($result->message);
            }
        }

    }
}
if(!function_exists('M360_Superkasse_Delete')){
    function M360_Superkasse_Delete(){

    }
}

if(!function_exists('M360_Superkasse_CallLicenceAPI')){
    function M360_Superkasse_CallLicenceAPI($method, $data = false){
        $url = 'http://superkasse.no/api/';
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result);
    }
}