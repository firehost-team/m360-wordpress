<?php
/*
 * Plugin Name: M360 Superkasse
 * Plugin URI: http://m360.no
 * Description: sender nettbutikk url-en til superkasse.no når det blir noe endring til nettverk side , oppretting, sletting eller endring status
 * Version: 1.01
 * Requires at least: WordPress 3.0
 * Tested up to: WordPress 4.6.1
 * Author: Ibrahim Qraiqe
 * Author URI: http://m360.no
 * License: GPL v2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Network: true
*/

if( !defined( 'ABSPATH' ) ) {
    exit;
}
define ( 'M360_SUPERKASSE_URL', plugins_url('', __FILE__) );
define ( 'M360_SUPERKASSE_DIR', dirname(__FILE__) );

require_once( M360_SUPERKASSE_DIR . '/inc/functions.php');

class M360_SUPERKASSE{
    public function __construct() {
        if ( !is_multisite() ) {
            add_action( 'admin_notices', 'm360_superkasse_install_multisite_notice' );
            return;
        }

        $this->hooks();
    }
    private function hooks() {
        add_action( 'plugins_loaded', array($this,'m360_superkasse_init') );
    }


    public function m360_superkasse_init() {
        add_action ( 'wpmu_new_blog', array($this,'m360_superkasse_wpmu_new_blog' ));
        add_action( 'update_blog_public', array($this,'m360_superkasse_update_blog_public'), 10, 1 );
        add_action ( 'delete_blog', array($this,'m360_superkasse_delete_blog'), 10, 1);
        add_action ( 'archive_blog', array($this,'m360_superkasse_archive_blog'), 10, 1 );
        add_action ( 'unarchive_blog', array($this,'m360_superkasse_unarchive_blog'), 10, 1 );
        add_action ( 'activate_blog', array($this,'m360_superkasse_activate_blog'), 10, 1 );
        add_action ( 'deactivate_blog', array($this,'m360_superkasse_deactivate_blog'), 10, 1 );
    }


    public function m360_superkasse_wpmu_new_blog($blog_id, $user_id, $domain, $path, $site_id, $meta){
        // når ny nettverk side opprettes
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Add($site_url);
    }

    public function m360_superkasse_update_blog_public( $blog_id, $value ) {
        // når status for nettverkside public endres
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Add($site_url);
    }

    public function m360_superkasse_delete_blog($blog_id, $drop){
        // når ny nettverk side slettes
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Delete($site_url);
    }

    public function m360_superkasse_archive_blog( $blog_id ) {
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Delete($site_url);
    }

    public function m360_superkasse_unarchive_blog( $blog_id ) {
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Add($site_url);
    }

    public function m360_superkasse_activate_blog( $blog_id ) {
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Add($site_url);
    }

    public function m360_superkasse_deactivate_blog( $blog_id ) {
        $site_url = get_site_url($blog_id);
        M360_Superkasse_Add($site_url);
    }
}



new M360_SUPERKASSE();
