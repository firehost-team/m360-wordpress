<?php
/**
 * Plugin Name: Recover Abandoned Cart
 * Plugin URI:
 * Description: Recover Abandoned Cart is a WooCommerce Extension Plugin which will help you Recover the Abandoned Carts and bring more sales.
 * Version: 15.1
 * Author: Fantastic Plugins
 * Author URI:
 */
/*
  Copyright 2014 Fantastic Plugins. All Rights Reserved.
  This Software should not be used or changed without the permission
  of Fantastic Plugins.
 */

require_once 'inc/fp_rac_counter.php';
require_once 'inc/fp_rac_coupon.php';
include 'inc/fp_rac_previous_order.php';
include 'inc/class_fp_rac_cart_data_from_mail.php';
include 'inc/class_fp_rac_preview_email_template.php';
include 'inc/class_fp_rac_polish_product_info.php';
include 'inc/class_fp_rac_email_template.php';
include 'inc/class_fp_rac_send_email_by_woocommerce_mailer.php';
include 'inc/class_fp_rac_segmentation.php';
include 'inc/class_fp_rac_abandon_order_management.php';
include 'inc/class_fp_rac_ajax_fun_response_for_email_template.php';
include 'inc/rac_common_function_for_multi_select_search.php';

/*
 * Tabs
 * 
 */
include 'inc/admin/tabs/class_fp_rac_general_tab.php';
include 'inc/admin/tabs/class_fp_rac_email_tab.php';
include 'inc/admin/tabs/class_fp_rac_cart_list_tab.php';
include 'inc/admin/tabs/class_fp_rac_coupon_tab.php';
include 'inc/admin/tabs/class_fp_rac_mail_log_tab.php';
include 'inc/admin/tabs/class_fp_rac_troubleshoot_tab.php';
include 'inc/admin/tabs/class_fp_rac_reports_tab.php';
include 'inc/admin/tabs/class_fp_rac_recovered_order_tab.php';
include 'inc/admin/tabs/class_fp_rac_shortcode_tab.php';
include 'inc/admin/tabs/class_fp_rac_support_tab.php';

class RecoverAbandonCart {

    public static function fprac_check_woo_active() {

        if (is_multisite()) {
            // This Condition is for Multi Site WooCommerce Installation
            if (!is_plugin_active_for_network('woocommerce/woocommerce.php') && (!is_plugin_active('woocommerce/woocommerce.php'))) {
                if (is_admin()) {
                    $variable = "<div class='error'><p> __('Recover Abandoned Cart will not work until WooCommerce Plugin is Activated. Please Activate the WooCommerce Plugin.','recoverabandoncart') </p></div>";
                    echo $variable;
                }
                return;
            }
        } else {
            // This Condition is for Single Site WooCommerce Installation
            if (!is_plugin_active('woocommerce/woocommerce.php')) {
                if (is_admin()) {
                    $variable = "<div class='error'><p> __('Recover Abandoned Cart will not work until WooCommerce Plugin is Activated. Please Activate the WooCommerce Plugin.','recoverabandoncart') </p></div>";
                    echo $variable;
                }
                return;
            }
        }
    }

    public static function fprac_header_problems() {
        ob_start();
    }

    public static function fprac_settings_tabs($tabs) {
        $tabs['fpracgenral'] = __('General Settings', 'recoverabandoncart');
        $tabs['fpracemail'] = __('Email Settings', 'recoverabandoncart');
        $tabs['fpractable'] = __('Cart List', 'recoverabandoncart');
        $tabs['fpracupdate'] = __('Check Previous Orders', 'recoverabandoncart');
        $tabs['fpraccoupon'] = __('Coupon In Mail', 'recoverabandoncart');
        $tabs['fpracmailog'] = __('Mail Log', 'recoverabandoncart');
        $tabs['fpracdebug'] = __('Troubleshoot', 'recoverabandoncart');
        $tabs['fpracreport'] = __('Reports', 'recoverabandoncart');
        $tabs['fpracrecoveredorderids'] = __('Recovered Orders', 'recoverabandoncart');
        $tabs['fpracshortocde'] = __('Shortcodes', 'recoverabandoncart');
        $tabs['fpracsupport'] = __('Support', 'recoverabandoncart');
        return $tabs;
    }

    public static function fprac_access_woo_script($array_screens) {
        $newscreenids = get_current_screen();
        $array_screens[] = $newscreenids->id;
        return $array_screens;
    }

    public static function fprac_admin_submenu() {
        global $rac_screen_options_tab;
        $rac_screen_options_tab = add_submenu_page('woocommerce', __("Recover Abandoned Cart", "recoverabandoncart"), __("Recover Abandoned Cart", "recoverabandoncart"), 'manage_woocommerce', 'fprac_slug', array('RecoverAbandonCart', 'fprac_admin_settings'));
        add_action("load-$rac_screen_options_tab", array("RecoverAbandonCart", "add_screen_option_tab"));
    }

    //add screen option tab
    public static function add_screen_option_tab() {
        global $rac_screen_options_tab;
        $screen = get_current_screen();
        if (!is_object($screen) || $screen->id != $rac_screen_options_tab || !isset($_REQUEST['tab']) || $_REQUEST['tab'] != 'fpracrecoveredorderids')
            return;
        $args = array(
            'label' => __('Number Of Items Per Page', 'recoverabandoncart'),
            'default' => 10,
            'option' => 'rac_per_page'
        );
        add_screen_option('per_page', $args);
    }

    public static function set_screen_option_value($status, $option, $value) {
        if ('rac_per_page' == $option)
            return $value;
    }

    //Load Email Template
    public static function email_template($subject, $message) {
        global $woocommerce, $woocommerce_settings;

        // load the mailer class
        $mailer = WC()->mailer();
        $email_heading = $subject;
        $message;
        $abstractClass = new ReflectionClass('WC_Email');
        if (!$abstractClass->isAbstract()) {
            $email = new WC_Email();
            // wrap the content with the email template and then add styles
            $fetch_data = $email->style_inline($mailer->wrap_message($email_heading, $message));
        } else {
            $fetch_data = $mailer->wrap_message($email_heading, $message);
        }
        return $fetch_data;
    }

    //Load Email Template
    public static function template_ready($message, $link) {
        global $woocommerce, $woocommerce_settings;
        $data = $link . $message;
        return $data;
    }

    public static function remove_action_hook() {
        remove_action('woocommerce_cart_updated', array('RecoverAbandonCart', 'fp_rac_insert_entry'));
    }

    public static function fprac_admin_settings() {
        global $woocommerce, $woocommerce_settings, $current_section, $current_tab;
        $tabs = "";
        do_action('woocommerce_fprac_settings_start');
        $current_tab = ( empty($_GET['tab']) ) ? 'fpracgenral' : sanitize_text_field(urldecode($_GET['tab']));
        $current_section = ( empty($_REQUEST['section']) ) ? '' : sanitize_text_field(urldecode($_REQUEST['section']));
        if (!empty($_POST['save'])) {
            if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'woocommerce-settings'))
                die(__('Action failed. Please refresh the page and retry.', 'recoverabandoncart'));

            if (!$current_section) {
                switch ($current_tab) {
                    default :
                        if (isset($woocommerce_settings[$current_tab]))
                            woocommerce_update_options($woocommerce_settings[$current_tab]);

                        // Trigger action for tab
                        do_action('woocommerce_update_options_' . $current_tab);
                        break;
                }

                do_action('woocommerce_update_options');

                // Handle Colour Settings
                if ($current_tab == 'general' && get_option('woocommerce_frontend_css') == 'yes') {
                    
                }
            } else {
                // Save section onlys
                do_action('woocommerce_update_options_' . $current_tab . '_' . $current_section);
            }

            // Clear any unwanted data
            delete_transient('woocommerce_cache_excluded_uris');
            // Redirect back to the settings page
            $redirect = esc_url_raw(add_query_arg(array('saved' => 'true')));
            if (isset($_POST['subtab'])) {
                wp_safe_redirect($redirect);
                exit;
            }
        }
        // Get any returned messages
        $error = ( empty($_GET['wc_error']) ) ? '' : urldecode(stripslashes($_GET['wc_error']));
        $message = ( empty($_GET['wc_message']) ) ? '' : urldecode(stripslashes($_GET['wc_message']));

        if ($error || $message) {

            if ($error) {
                echo '<div id="message" class="error fade"><p><strong>' . esc_html($error) . '</strong></p></div>';
            } else {
                echo '<div id="message" class="updated fade"><p><strong>' . esc_html($message) . '</strong></p></div>';
            }
        } elseif (!empty($_GET['saved'])) {

            echo '<div id="message" class="updated fade"><p><strong>' . __('Your settings have been saved.', 'recoverabandoncart') . '</strong></p></div>';
        } elseif (!empty($_GET['resetted'])) {
            echo '<div id="message" class="updated fade"><p><strong>' . __('Your settings have been Restored.', 'recoverabandoncart') . '</strong></p></div>';
        }
        //check cron job define and enabled or disabled.
        if (get_option('rac_cron_troubleshoot_format') != 'server_cron') {
            if (defined('DISABLE_WP_CRON')) {
                if (DISABLE_WP_CRON == 'true') {
                    echo '<div id="message" class="error"><p><strong>' . __("wp_cron is disabled in your Site <br />Recover Abandoned Cart will not be able to send Cart Abandoned Emails to Users Automatically<br />To enable wp_cron, edit the config.php of your Wordpress installation and search for 'define('DISABLE_WP_CRON', 'true');' and set the value as false", "recoverabandoncart") . '</strong></p></div>';
                }
            }
        }
        ?>
        <div class="wrap woocommerce">
            <form method="post" id="mainform" action="" enctype="multipart/form-data">
                <div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div><h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                    <?php
                    $tabs = apply_filters('woocommerce_fprac_settings_tabs_array', $tabs);
                    foreach ($tabs as $name => $label)
                        echo '<a href="' . admin_url('admin.php?page=fprac_slug&tab=' . $name) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';

                    do_action('woocommerce_fprac_settings_tabs');
                    ?>
                </h2>

                <?php
                switch ($current_tab) {
                    case "fpractable":
                        FP_RAC_CartList_Tab::fp_rac_adandoncart_admin_display();
                        break;
                    case "fpracemail":
                        if (!isset($_GET['rac_new_email']) && !isset($_GET['rac_edit_email']) && !isset($_GET['rac_send_email'])) {
                            do_action('woocommerce_fprac_settings_tabs_' . $current_tab); // @deprecated hook
                            do_action('woocommerce_fprac_settings_' . $current_tab);
                            ?>
                            <p class="submit" style="margin-left: 25px;">
                                <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                                    <input name="save" class="button-primary" type="submit" value="<?php _e('Save', 'recoverabandoncart'); ?>" />
                                <?php endif; ?>
                                <input type="hidden" name="subtab" id="last_tab" />
                                <?php wp_nonce_field('woocommerce-settings'); ?>
                            </p>
                            <p><h3><?php _e('Mail Template Settings', 'recoverabandoncart'); ?></h3></p>
                            <?php
                        }
                        //email template lists
                        if (isset($_GET['rac_new_email'])) {
                            FP_RAC_Email_Template::fp_rac_create_new_email_template();
                        } else if (isset($_GET['rac_edit_email']) && !isset($_GET['preview'])) {
                            FP_RAC_Email_Template::fp_rac_edit_email_template();
                        } else if (isset($_GET['rac_send_email'])) {
                            FP_RAC_Email_Template::fp_rac_send_email_template();
                        } elseif (isset($_GET['preview'])) {
                            FP_RAC_Preview_Email_Template::rac_preview_email_template();
                        } else {
                            FP_RAC_Email_Template::fp_rac_display_email_template_table();
                        }
                        ?>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery('.rac_copy_email_template').click(function () {
                                    var row_id = (jQuery(this).attr("data-value"));
                                    var dataparam = ({
                                        action: 'copy_this_template',
                                        row_id: row_id,
                                    });
                                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam, function (response) {
                                        console.log(response);
                                        window.setTimeout(function () {
                                            location.reload();
                                        }, 1000);
                                    });
                                });
                            });</script>
                        <?php
                        break;
                    case "fpracupdate":
                        echo '<table class="form-table"><tr>
                            <th>' . __('Add WC Order which are', 'recoverabandoncart') . ' <td><p><input type = "checkbox" name = "order_status[]" value = "wc-on-hold">' . __("on-hold", "recoverabandoncart") . '</p>
                        <p><input type = "checkbox" name = "order_status[]" value = "wc-pending">' . __("Pending", "recoverabandoncart") . '</p>
                        <p><input type = "checkbox" name = "order_status[]" value = "wc-failed" checked>' . __("Failed", "recoverabandoncart") . '</p>
                        <p><input type = "checkbox" name = "order_status[]" value = "wc-cancelled">' . __("Cancelled", "recoverabandoncart") . '</p></td>
                        </tr><tr>
                        <th>With</th><td><select id = "order_time">
                        <option value = "all">' . __("All time", "recoverabandoncart") . '</option>
                        <option value = "specific">' . __("Specific", "recoverabandoncart") . '</option>
                        </td>
                        </tr>
                        <tr style = "display: none" id = "specific_row">
                        <th>' . __("Specific Time", "recoverabandoncart") . '</th>
                        <td>' . __("From", "recoverabandoncart") . '<input type = "text" name = "from_date" id = "from_time" class = "rac_date"> ' . __('To', 'recoverabandoncart') . ' <input type = "text" id = "to_time" name = "to_date" class = "rac_date"></td>
                </tr>
                <tr>
                <td><input type = "button" class = "button button-primary" name = "update_order" id = "update_order" value = "' . __("Check for Abandoned Cart", "recoverabandoncart") . '"></td>
                <td><img style = "width: 30px;height: 30px;display: none;" class = "perloader_image" src = "' . WP_PLUGIN_URL . '/rac/images/update.gif"/><p id = "update_response"></p></td>
                </tr>
                </table>';
                        //ajax call
                        echo '<script > jQuery(document) . ready(function() {
                            jQuery("#specific_row") . css("display", "none");
                            jQuery("#order_time") . change(function() {
                                        if (jQuery(this) . val() == "specific") {
                                            jQuery("#specific_row") . css("display", "table-row");
                                        } else {
                                            jQuery("#specific_row") . css("display", "none");
                                        }
                                    });
                        });</script>';
                        break;
                    case "fpracmailog":
                        FP_RAC_Mail_Log_Tab::fp_rac_mail_logs_display();
                        break;
                    case "fpraccoupon":
                        do_action('woocommerce_fprac_settings_tabs_' . $current_tab); // @deprecated hook
                        do_action('woocommerce_fprac_settings_' . $current_tab);
                        ?>
                        <p>Use {rac.coupon} to include a coupon code in mail</p>
                        <span class="submit" style="margin-left: 25px;">
                            <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                                <input name="save" class="button-primary" style="margin-top:15px;" type="submit" value="<?php _e('Save', 'recoverabandoncart'); ?>" />
                            <?php endif; ?>
                            <input type="hidden" name="subtab" id="last_tab" />
                            <?php wp_nonce_field('woocommerce-settings'); ?>
                        </span>

                        <?php
                        break;
                    case "fpracdebug":
                        do_action('woocommerce_fprac_settings_tabs_' . $current_tab); // @deprecated hook
                        do_action('woocommerce_fprac_settings_' . $current_tab);
                        ?>
                        <h3><?php _e('Test Mail', 'recoverabandoncart'); ?></h3>
                        <table class="form-table">
                            <tr>
                                <th><?php _e('Test Mail Format', 'recoverabandoncart'); ?></th>
                                <td>
                                    <select name="rac_test_mail_format" id="rac_test_mail_format">
                                        <option value="1"><?php _e('Plain Text', 'recoverabandoncart'); ?></option>
                                        <option value="2"><?php _e('HTML', 'recoverabandoncart'); ?></option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <th><?php _e('Send Test Email to', 'recoverabandoncart'); ?> </th>
                                <td><input type="text" id="testemailto" name="testemailto" value="">
                                    <input type="button" id="senttestmail" class="button button-primary" value=<?php _e('Send Test Email', 'recoverabandoncart'); ?>></td>
                            </tr>
                            <tr>
                                <td colspan="2"><p id="test_mail_result" style="display:none;"></p></td>
                            </tr>
                        </table>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery("#senttestmail").click(function () {
                                    var data = {
                                        action: "rac_send_test_mail",
                                        rac_test_mail_to: jQuery("#testemailto").val(),
                                        rac_plain_or_html: jQuery('#rac_test_mail_format').val(),
                                    };
                                    console.log(data);
                                    var cur_button = jQuery(this);
                                    jQuery(this).prop("disabled", true);
                                    jQuery.ajax({
                                        type: "POST",
                                        url: ajaxurl,
                                        data: data
                                    }).done(function (response) {
                                        jQuery("#test_mail_result").css("display", "block");
                                        if (response == "sent") {
                                            jQuery("#test_mail_result").html("<?php _e("Mail has been Sent, but this doesn\'t mean mail will be delivered Successfully. Check Wordpress Codex for More info on Mail.", "recoverabandoncart") ?>");
                                        } else {
                                            jQuery("#test_mail_result").html("<?php _e("Mail not Sent.", "recoverabandoncart"); ?>");
                                        }
                                        cur_button.prop("disabled", false);
                                    });
                                });
                            });</script>
                        <h3><?php _e('Cron Schedules', 'recoverabandoncart') ?></h3>
                        <table class="widefat">
                            <thead>
                                <tr>
                                    <th><?php _e('Mail Job hook', 'recoverabandoncart') ?></th>
                                    <th><?php _e('Next Mail job', 'recoverabandoncart') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php _e('rac_cron_job', 'recoverabandoncart') ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (wp_next_scheduled('rac_cron_job')) {
                                            date_default_timezone_set('UTC');
                                            echo "UTC time = " . date(get_option('date_format'), wp_next_scheduled('rac_cron_job')) . ' / ' . date(get_option('time_format'), wp_next_scheduled('rac_cron_job')) . '</br>';
                                            @date_default_timezone_set(get_option('timezone_string'));
                                            echo "Local time = " . date(get_option('date_format'), wp_next_scheduled('rac_cron_job')) . ' / ' . date(get_option('time_format'), wp_next_scheduled('rac_cron_job')) . '</br>';
                                        } else {
                                            _e('Cron is not set', 'recoverabandoncart');
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h1><?php _e('Troubleshoot for Unsubscription Link in Footer', 'recoverabandoncart'); ?></h1>
                        <h3>
                            <?php echo 'If Unsubscribe Email Link is not visible footer of email, then kindly consider to use this shortcode {rac.unsubscribe} in text editor of each email templates to make it work.'; ?>
                        </h3>
                        <span class="submit" style="margin-left: 25px;">
                            <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                                <input name="save" class="button-primary" style="margin-top:15px;" type="submit" value="<?php _e('Save', 'recoverabandoncart'); ?>" />
                            <?php endif; ?>
                            <input type="hidden" name="subtab" id="last_tab" />
                            <?php wp_nonce_field('woocommerce-settings'); ?>
                        </span>

                        <?php
                        break;
                    case "fpracreport" :
                        new FP_RAC_Reports_Tab();
                        break;

                    case "fpracrecoveredorderids":
                        FPRacCounter::add_list_table();
                        break;
                    case "fpracshortocde":
                        FP_RAC_Shortcode_Tab::fp_rac_shortcodes_info();
                        break;
                    case "fpracsupport";
                        woocommerce_admin_fields(FP_RAC_Support_tab::fp_rac_support_admin_fields());
                        break;
                    default :
                        do_action('woocommerce_fprac_settings_tabs_' . $current_tab); // @deprecated hook
                        do_action('woocommerce_fprac_settings_' . $current_tab);
                        $admin_url = admin_url('admin.php');
                        $reset_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'rac_reset' => 'reset'), $admin_url));
                        echo '<input class="button-secondary" id="rac_reset" type="button" name="rac_reset" value="' . __("Reset", "recoverabandoncart") . '">';
                        echo '<script type="text/javascript">
                                       jQuery(document).ready(function(){
                                       jQuery("#rac_reset").click(function(){
                                       window.location.replace("' . $reset_url . '");
                                       });
                                       jQuery("#rac_admin_cart_recovered_noti").change(function(){
                                       if(jQuery(this).is(":checked")){
                                       jQuery(".admin_notification").parent().parent().show();
                                       jQuery(".admin_notifi_sender_opt").closest("tr").show();
                                       }else{
                                        jQuery(".admin_notification").parent().parent().hide();
                                        jQuery(".admin_notifi_sender_opt").closest("tr").hide();
                                       }
                                       //sender option should be refereshed as it is inside this
                                       var sender_opt = jQuery("[name=\'rac_recovered_sender_opt\']:checked").val();
                                       console.log(sender_opt);
                                       if(sender_opt == "woo"){
                                       jQuery(".local_senders").parent().parent().hide();
                                       }else{
                                        jQuery(".local_senders").parent().parent().show();
                                       }
                                       });
                                       jQuery("[name=\'rac_recovered_sender_opt\']").change(function(){
                                       var sender_opt = jQuery("[name=\'rac_recovered_sender_opt\']:checked").val();
                                       if(sender_opt == "woo"){
                                       jQuery(".local_senders").parent().parent().hide();
                                       }else{
                                        jQuery(".local_senders").parent().parent().show();
                                       }
                                       });
                                       //on ready event
                                       var sender_opt = jQuery("[name=\'rac_recovered_sender_opt\']:checked").val();
                                       console.log(sender_opt);
                                       if(sender_opt == "woo"){
                                       jQuery(".local_senders").parent().parent().hide();
                                       }else{
                                        jQuery(".local_senders").parent().parent().show();
                                       }
                                       //enable notification event
                                       if(jQuery("#rac_admin_cart_recovered_noti").is(":checked")){
                                       jQuery(".admin_notification").parent().parent().show();
                                       jQuery(".admin_notifi_sender_opt").closest("tr").show();
                                       }else{
                                        jQuery(".admin_notification").parent().parent().hide();
                                        jQuery(".admin_notifi_sender_opt").closest("tr").hide();
                                       }
                                       });</script>';
                        ?>
                        <span class="submit" style="margin-left: 25px;">
                            <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                                <input name="save" class="button-primary" type="submit" value="<?php _e('Save', 'recoverabandoncart'); ?>" />
                            <?php endif; ?>
                            <input type="hidden" name="subtab" id="last_tab" />
                            <?php wp_nonce_field('woocommerce-settings'); ?>
                        </span>
                        <?php
                        break;
                }
                ?>


            </form>
        </div>

        <script type="text/javascript">

            jQuery(document).ready(function () {
                if (jQuery('#rac_remove_carts').is(":checked")) {
                    jQuery('#rac_remove_new').parent().parent().parent().parent().css("display", "table-row");
                    jQuery('#rac_remove_abandon').parent().parent().parent().parent().css("display", "table-row");
                } else {
                    jQuery('#rac_remove_new').parent().parent().parent().parent().css("display", "none");
                    jQuery('#rac_remove_abandon').parent().parent().parent().parent().css("display", "none");
                }

                jQuery('#rac_remove_carts').change(function () {
                    if (this.checked) {
                        jQuery('#rac_remove_new').parent().parent().parent().parent().css("display", "table-row");
                        jQuery('#rac_remove_abandon').parent().parent().parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#rac_remove_new').parent().parent().parent().parent().css("display", "none");
                        jQuery('#rac_remove_abandon').parent().parent().parent().parent().css("display", "none");
                    }
                });
            });</script>


        <?php
    }

    public static function fp_rac_troubleshoot_mailsend() {
        global $woocommerce;
        if (isset($_GET['tab'])) {

            if ($_GET['tab'] == 'fpracdebug') {
                ?>

                <script type="text/javascript">

                    jQuery(document).ready(function () {
                <?php if ((float) $woocommerce->version > (float) ('2.2.0')) { ?>
                            var troubleemail = jQuery('#rac_trouble_mail').val();
                            if (troubleemail === 'mail') {
                                jQuery('.prependedrc').remove();
                                jQuery('#rac_trouble_mail').parent().append('<span class="prependedrc"><?php _e("For WooCommerce 2.3 or higher version mail() function will not load the woocommerce default template. This option will be deprecated", "recoverabandoncart"); ?> </span>');
                            } else {
                                jQuery('.prependedrc').remove();
                            }
                            jQuery('#rac_trouble_mail').change(function () {
                                if (jQuery(this).val() === 'mail') {
                                    jQuery('.prependedrc').remove();
                                    jQuery('#rac_trouble_mail').parent().append('<span class="prependedrc"><?php _e("For WooCommerce 2.3 or higher version mail() function will not load the woocommerce default template. This option will be deprecated", "recoverabandoncart") ?> </span>');
                                } else {
                                    jQuery('.prependedrc').remove();
                                }
                            });
                <?php } ?>

                        if (jQuery('#rac_webmaster_mail').val() == 'webmaster1') {
                            jQuery("#rac_textarea_mail").parent().parent().show();
                        } else {
                            //Hide text box here
                            jQuery("#rac_textarea_mail").parent().parent().hide();
                        }
                        jQuery("#rac_webmaster_mail").change(function () {
                            if (jQuery(this).val() == 'webmaster1') {
                                jQuery("#rac_textarea_mail").parent().parent().show();
                            } else {
                                //Hide text box here
                                jQuery("#rac_textarea_mail").parent().parent().hide();
                            }
                        });
                    });</script>
                <?php
            }
        }
    }

    public static function create_load_table() {
        global $wpdb;
        $currentdbversion = '3.5.6';
        $olddbversion = get_option('rac_db_version');
        if (!$olddbversion) {
            add_option('rac_db_version', '1.0.0');
        }
        if ($currentdbversion != get_option('rac_db_version')) {
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $sql = "CREATE TABLE " . $table_name . "(
             id int(9) NOT NULL AUTO_INCREMENT,
             cart_details LONGTEXT NOT NULL,
             user_id LONGTEXT NOT NULL,
              email_id VARCHAR(255),
             cart_abandon_time BIGINT NOT NULL,
             cart_status LONGTEXT NOT NULL,
             mail_template_id LONGTEXT,
             ip_address LONGTEXT,
             link_status LONGTEXT,
             sending_status VARCHAR(15) NOT NULL DEFAULT 'SEND',
             wpml_lang VARCHAR(10),
             placed_order VARCHAR(20),
             completed VARCHAR(20),
              UNIQUE KEY id (id)
            )DEFAULT CHARACTER SET utf8;";

            dbDelta($sql);

            //Email Template Table
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            $sql_email = "CREATE TABLE " . $table_name_email . "(
             id int(9) NOT NULL AUTO_INCREMENT,
             template_name LONGTEXT NOT NULL,
             sender_opt VARCHAR(10) NOT NULL DEFAULT 'woo',
             from_name LONGTEXT NOT NULL,
             from_email LONGTEXT NOT NULL,
             rac_blind_carbon_copy LONGTEXT NOT NULL,
             subject LONGTEXT NOT NULL,
             anchor_text LONGTEXT NOT NULL,
             message LONGTEXT NOT NULL,
             sending_type VARCHAR(20) NOT NULL,
             sending_duration BIGINT NOT NULL,
             status VARCHAR(10) NOT NULL DEFAULT 'ACTIVE',

             UNIQUE KEY id (id)
            )DEFAULT CHARACTER SET utf8;";
            dbDelta($sql_email);

            //Email Logs
            $table_name_logs = $wpdb->prefix . 'rac_email_logs';
            $sql_email_logs = "CREATE TABLE " . $table_name_logs . "(
             id int(9) NOT NULL AUTO_INCREMENT,
             email_id LONGTEXT NOT NULL,
             date_time LONGTEXT NOT NULL,
             rac_cart_id LONGTEXT NOT NULL,
             template_used LONGTEXT NOT NULL,
              UNIQUE KEY id (id)
            )DEFAULT CHARACTER SET utf8;";
            dbDelta($sql_email_logs);

            $email_temp_check = $wpdb->get_results("SELECT * FROM $table_name_email", OBJECT);
            if (empty($email_temp_check)) {
                $wpdb->insert($table_name_email, array('template_name' => 'Default',
                    'sender_opt' => 'woo',
                    'from_name' => 'Admin',
                    'from_email' => get_option('admin_email'),
                    'rac_blind_carbon_copy' => '',
                    'subject' => 'Recovering Abandon Cart',
                    'anchor_text' => 'Cart Link',
                    'message' => "Hi {rac.firstname},<br><br>We noticed you have added the following Products in your Cart, but haven't completed the purchase. {rac.Productinfo}<br><br>We have captured the Cart for your convenience. Please use the following link to complete the purchase {rac.cartlink}<br><br>Thanks.",
                    'sending_type' => 'days',
                    'sending_duration' => '1'));
            }
            //altering table like below in order to avoid problem with previous table
            $wpdb->query("ALTER TABLE $table_name change mail_sent_to ip_address LONGTEXT");
            $wpdb->query("ALTER TABLE $table_name ADD sending_status varchar(15) NOT NULL DEFAULT 'SEND' AFTER link_status");
            $wpdb->query("ALTER TABLE $table_name ADD email_id VARCHAR(255) AFTER user_id");
            $wpdb->query("ALTER TABLE $table_name ADD wpml_lang varchar(10) NOT NULL DEFAULT 'en' AFTER sending_status");
            //altering for email templates
            $wpdb->query("ALTER TABLE $table_name_email ADD sender_opt varchar(10) NOT NULL DEFAULT 'woo' AFTER template_name");
            $wpdb->query("ALTER TABLE $table_name_email ADD status varchar(10) NOT NULL DEFAULT 'ACTIVE' AFTER sending_duration");
            $wpdb->query("ALTER TABLE $table_name_email ADD mail VARCHAR(10) NOT NULL DEFAULT 'HTML' AFTER status");
            $wpdb->query("ALTER TABLE $table_name_email ADD link LONGTEXT NOT NULL AFTER mail");
            $wpdb->query("ALTER TABLE $table_name_email ADD anchor_text LONGTEXT NOT NULL");
            $wpdb->query("ALTER TABLE $table_name_email ADD rac_blind_carbon_copy LONGTEXT NOT NULL");

            $wpdb->query("ALTER TABLE $table_name ADD old_status LONGTEXT NOT NULL");
            //adding extra column in  Email Template
            fp_rac_add_extra_column_in_email_template();
            //adding extra column in cart table
            fp_rac_add_extra_column_in_cart_list_table();
            //update default values to old rows
            $wpdb->query("UPDATE $table_name_email SET anchor_text='Cart Link' WHERE anchor_text=''");
            update_option('rac_db_version', $currentdbversion);
        }
    }

    public static function sample_productinfo_shortcode() {
        ob_start();
        global $woocommerce;
        if ((float) $woocommerce->version < (float) ('2.4.0')) {
            ?>
            <style type="text/css">
                table .td{
                    border: 1px solid #e4e4e4 !important;
                }

            </style>
        <?php } ?>
        <table class="<?php echo FPRacCron::enable_border() ?>" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', 'Helvetica', 'Roboto', 'Arial', 'sans-serif';" >
            <thead>
        <?php echo FP_RAC_Polish_Product_Info::fp_rac_get_sortable_column_name('en'); ?>
            </thead>
            <tbody>
        <?php
        echo FP_RAC_Polish_Product_Info::fp_split_rac_items_in_cart('Product A', fp_rac_placeholder_img(), '1', FP_List_Table_RAC::format_price(10));
        ?>
            </tbody>
                <?php if (get_option('rac_hide_tax_total_product_info_shortcode') != 'yes') { ?>
                <tfoot>
                    <tr>
                        <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_subtotal', 'en', get_option('rac_product_info_subtotal')); ?></th>
                        <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price(10); ?></td>
                    </tr>
            <?php if (get_option('rac_hide_shipping_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_shipping', 'en', get_option('rac_product_info_shipping')); ?></th>
                            <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price(10); ?></td>
                        </tr>
            <?php } ?>
                    <?php if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'no' && get_option('rac_hide_tax_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_tax', 'en', get_option('rac_product_info_tax')); ?></th>
                            <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price(1); ?></td>
                        </tr>
            <?php } ?>
                    <tr>
                        <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_total', 'en', get_option('rac_product_info_total')); ?></th>
                        <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price(21); ?></td>
                    </tr>

                </tfoot>
        <?php } ?>
        </table>
            <?php
            return ob_get_clean();
        }

        public static function fp_rac_edit_mail_update_data() {
            global $wpdb;
            $row_id = $_POST['id'];
            $email_value = $_POST['email'];
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $last_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$row_id and cart_status NOT IN('trash')", OBJECT);
            $last_cart_key = key($last_cart);
            $user_details = maybe_unserialize($last_cart[$last_cart_key]->cart_details);
            $user_details["visitor_mail"] = $email_value;
            $details = serialize($user_details);
            $wpdb->update($table_name, array('cart_details' => $details), array('id' => $row_id));
            exit();
        }

        public static function restrict_entries_in_cart_list($email) {
            $getrestriction = get_option('custom_restrict');
            if ($getrestriction == 'user_role') {
                $userrolenamemailget = get_option('custom_user_role_for_restrict_in_cart_list');
                $getuserby = get_user_by('email', $email);
                if ($getuserby) {
                    $newto = $getuserby->roles[0];
                } else {
                    $newto = $email;
                }
            } elseif ($getrestriction == 'name') {
                $userrolenamemailget = get_option('custom_user_name_select_for_restrict_in_cart_list');
                $userrolenamemailget = explode(',', $userrolenamemailget);
                $getuserby = get_user_by('email', $email);
                if ($getuserby) {
                    $newto = $getuserby->ID;
                } else {
                    $newto = $email;
                }
            } else {
                $userrolenamemailget = get_option('custom_mailid_for_restrict_in_cart_list');
                $userrolenamemailget = explode("\r\n", $userrolenamemailget);
                $newto = $email;
            }
            if (!empty($userrolenamemailget)) {
                if (!in_array($newto, $userrolenamemailget)) {
                    return 'proceed';
                } else {
                    return 'not proceed';
                }
            } else {
                return 'proceed';
            }
        }

        public static function fp_rac_insert_entry() {
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $current_time = current_time('timestamp');
            $currentuser_lang = fp_rac_get_current_language();
            if (is_user_logged_in() && !is_admin()) {
                $user_id = get_current_user_id();
                $user_details = get_userdata($user_id);
                $user_email = $user_details->user_email;
                $last_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id = $user_id and cart_status IN('NEW','ABANDON') and placed_order IS NULL ORDER BY id DESC LIMIT 1", OBJECT);
                if (!empty($last_cart)) {
                    $last_cart = $last_cart[0];
                }
                $chosen_methods = WC()->session->get('chosen_shipping_methods');
                $chosen_methods = isset($chosen_methods) ? $chosen_methods[0] : '';
                $shipping_cost = isset(WC()->cart->shipping_total) ? WC()->cart->shipping_total : '';
                $shipping_tax_cost = isset(WC()->cart->shipping_tax_total) ? WC()->cart->shipping_tax_total : '';
                $shipping_details = array('shipping_cost' => $shipping_cost, 'shipping_tax_cost' => $shipping_tax_cost, 'shipping_method' => $chosen_methods);
                $cart_persistent = (get_user_meta($user_id, '_woocommerce_persistent_cart'));
                if (!empty($cart_persistent[0]['cart'])) {
                    $cart_content = get_user_meta($user_id, '_woocommerce_persistent_cart');
                    $cart_content['shipping_details'] = $shipping_details;
                    $cart_content = maybe_serialize($cart_content);
                    $cut_off_time = get_option('rac_abandon_cart_time');
                    if (get_option('rac_abandon_cart_time_type') == 'minutes') {
                        $cut_off_time = $cut_off_time * 60;
                    } else if (get_option('rac_abandon_cart_time_type') == 'hours') {
                        $cut_off_time = $cut_off_time * 3600;
                    } else if (get_option('rac_abandon_cart_time_type') == 'days') {
                        $cut_off_time = $cut_off_time * 86400;
                    }
                    if (!empty($last_cart)) {
                        $cut_off_time = $last_cart->cart_abandon_time + $cut_off_time;
                    }
                    if ($current_time > $cut_off_time) {
                        if ((isset($_COOKIE['rac_cart_id'])) || (isset($_GET['abandon_cart']))) {
                            //do nothing. Since this cart is from mail
                        } else {
                            if (!empty($last_cart)) {
                                $wpdb->update($table_name, array('cart_status' => 'ABANDON'), array('id' => $last_cart->id));
                                FPRacCounter::rac_do_abandoned_count();
                            }
                            if (get_option('rac_remove_carts') == 'yes') {
                                if (get_option('rac_remove_new') == 'yes') {
                                    $wpdb->delete($table_name, array('email_id' => $user_email, 'cart_status' => 'NEW'));
                                }
                                if (get_option('rac_remove_abandon') == 'yes') {
                                    $wpdb->delete($table_name, array('email_id' => $user_email, 'cart_status' => 'ABANDON'));
                                }
                            }
                            if (self::restrict_entries_in_cart_list($user_email) == 'proceed') {
                                $wpdb->insert($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $user_email, 'cart_abandon_time' => $current_time, 'cart_status' => 'NEW', 'wpml_lang' => $currentuser_lang));
                            }
                        }
                    } else { //Update the cart details if less than or equal to cut off time
                        if (!empty($last_cart)) {
                            $wpdb->update($table_name, array('cart_details' => $cart_content, 'cart_abandon_time' => $current_time), array('id' => $last_cart->id));
                        }
                    }
                } else {
                    if (!empty($last_cart) && $last_cart->cart_status == 'NEW') {
                        $wpdb->delete($table_name, array('id' => $last_cart->id));
                    }
                }
                // FOR ALL USER STATUS - - UPDATE ONLY
                //Members
                $status_new_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status='NEW' AND user_id != '0'", OBJECT);
                $cut_off_time = get_option('rac_abandon_cart_time');
                if (get_option('rac_abandon_cart_time_type') == 'minutes') {
                    $cut_off_time = $cut_off_time * 60;
                } else if (get_option('rac_abandon_cart_time_type') == 'hours') {
                    $cut_off_time = $cut_off_time * 3600;
                } else if (get_option('rac_abandon_cart_time_type') == 'days') {
                    $cut_off_time = $cut_off_time * 86400;
                }
                foreach ($status_new_list as $status_new) {
                    $cut_off_time = $cut_off_time + $status_new->cart_abandon_time;
                    if ($current_time > $cut_off_time) {
                        $wpdb->update($table_name, array('cart_status' => 'ABANDON'), array('id' => $status_new->id));
                        FPRacCounter::rac_do_abandoned_count();
                    }
                }
                //Guest
                $status_new_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status='NEW' AND user_id='0'", OBJECT);
                $cut_off_time = get_option('rac_abandon_cart_time_guest');
                if (get_option('rac_abandon_cart_time_type_guest') == 'minutes') {
                    $cut_off_time = $cut_off_time * 60;
                } else if (get_option('rac_abandon_cart_time_type_guest') == 'hours') {
                    $cut_off_time = $cut_off_time * 3600;
                } else if (get_option('rac_abandon_cart_time_type_guest') == 'days') {
                    $cut_off_time = $cut_off_time * 86400;
                }
                foreach ($status_new_list as $status_new) {
                    $cut_off_time = $cut_off_time + $status_new->cart_abandon_time;
                    if ($current_time > $cut_off_time) {
                        $wpdb->update($table_name, array('cart_status' => 'ABANDON'), array('id' => $status_new->id));
                        FPRacCounter::rac_do_abandoned_count();
                    }
                }
                // FOR ALL USER STATUS - UPDATE ONLY END
            } else {
                // FOR ALL USER STATUS - UPDATE ONLY
                //Members
                $status_new_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status='NEW' AND user_id!='0'", OBJECT);
                $cut_off_time = get_option('rac_abandon_cart_time');
                if (get_option('rac_abandon_cart_time_type') == 'minutes') {
                    $cut_off_time = $cut_off_time * 60;
                } else if (get_option('rac_abandon_cart_time_type') == 'hours') {
                    $cut_off_time = $cut_off_time * 3600;
                } else if (get_option('rac_abandon_cart_time_type') == 'days') {
                    $cut_off_time = $cut_off_time * 86400;
                }
                foreach ($status_new_list as $status_new) {
                    $cut_off_time = $cut_off_time + $status_new->cart_abandon_time;
                    if ($current_time > $cut_off_time) {
                        $wpdb->update($table_name, array('cart_status' => 'ABANDON'), array('id' => $status_new->id));
                        FPRacCounter::rac_do_abandoned_count();
                    }
                }
                //guest
                $status_new_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status='NEW' AND user_id='0'", OBJECT);
                $cut_off_time = get_option('rac_abandon_cart_time_guest');
                if (get_option('rac_abandon_cart_time_type_guest') == 'minutes') {
                    $cut_off_time = $cut_off_time * 60;
                } else if (get_option('rac_abandon_cart_time_type_guest') == 'hours') {
                    $cut_off_time = $cut_off_time * 3600;
                } else if (get_option('rac_abandon_cart_time_type_guest') == 'days') {
                    $cut_off_time = $cut_off_time * 86400;
                }
                foreach ($status_new_list as $status_new) {
                    $cut_off_time = $cut_off_time + $status_new->cart_abandon_time;
                    if ($current_time > $cut_off_time) {
                        $wpdb->update($table_name, array('cart_status' => 'ABANDON'), array('id' => $status_new->id));
                        FPRacCounter::rac_do_abandoned_count();
                    }
                }
                // FOR ALL USER STATUS - UPDATE ONLY END
            }
        }

        public static function check_is_member_or_guest($to) {

            $get_user_by_email = get_user_by('email', $to);

            if ($get_user_by_email) {
                return true;
            } else {
                return false;
            }
        }

        public static function rac_return_user_id($memberemail) {
            $get_user_by_email = get_user_by('email', $memberemail);

            return $get_user_by_email->ID;
        }

        //Footer Record
        public static function unsubscribed_user_from_rac_mail() {
            if (isset($_COOKIE['un_sub_email_auto'])) {
                ?>
            <style type="text/css">
                p.un_sub_email_css {
                    position: fixed;
                    left: 0;
                    right: 0;
                    margin: 0;
                    width: 100%;
                    font-size: 1em;
                    padding: 1em 0;
                    text-align: center;
                    background-color: #<?php echo get_option('rac_unsubscription_message_background_color') ?>;
                    color: #<?php echo get_option('rac_unsubscription_message_text_color') ?>;
                    z-index: 99998;
                    a {
                        color: 0 1px 1em rgba(0, 0, 0, 0.2);
                    }
                }

                .admin-bar {
                    p.un_sub_email_css {
                        top: 32px;
                    }
                }
            </style>
            <?php
            if ($_COOKIE['un_sub_email_auto'] != "") {
                if (isset($_COOKIE['already_unsubscribed'])) {
                    echo '<p class="un_sub_email_css">' . get_option('rac_already_unsubscribed_text') . '</p>';
                } else {
                    echo '<p class="un_sub_email_css">' . get_option('rac_unsubscribed_successfully_text') . '</p>';
                }
            }
            unset($_COOKIE['un_sub_email_auto']);
            setcookie('un_sub_email_auto', null);
        }
        if (isset($_GET['email']) && isset($_GET['action']) && isset($_GET['_mynonce'])) {
            $to = $_GET['email'];
            if (get_option('rac_unsubscription_type') != '2') {

                // Automatic Unsubscription
                $check = RecoverAbandonCart::check_is_member_or_guest($to);
                if ($check) {
                    // For Member
                    $member_userid = RecoverAbandonCart::rac_return_user_id($to);
                    $check_already = get_user_meta($member_userid, 'fp_rac_mail_unsubscribed', true);
                    if ($check_already == 'yes') {
                        setcookie('already_unsubscribed', 'yes', time() + 3600);
                    } else {
                        unset($_COOKIE['already_unsubscribed']);
                        setcookie('already_unsubscribed', null);
                        update_user_meta($member_userid, 'fp_rac_mail_unsubscribed', 'yes');
                    }
                } else {
                    // For Guest
                    $old_array = array_filter(array_unique((array) get_option('fp_rac_mail_unsubscribed')));
                    $listofemails = (array) $to;
                    $merge_arrays = array_merge($listofemails, $old_array);
                    if (in_array($to, $old_array)) {
                        setcookie('already_unsubscribed', 'yes', time() + 3600);
                    } else {
                        unset($_COOKIE['already_unsubscribed']);
                        setcookie('already_unsubscribed', null);
                        update_option('fp_rac_mail_unsubscribed', $merge_arrays);
                    }
                }
                setcookie('un_sub_email_auto', $to, time() + 3600);
                unset($_COOKIE['un_sub_email_manual']);
                setcookie('un_sub_email_manual', null);
                wp_redirect(get_permalink());
            } else {
                // Manual Unsubscription
                setcookie('un_sub_email_manual', $to, time() + 3600);
                unset($_COOKIE['un_sub_email_auto']);
                setcookie('un_sub_email_auto', null);
                wp_redirect(get_permalink());
            }
        }
    }

    public static function rac_footer_email_customization($message) {
        if (get_option('fp_unsubscription_link_in_email') == 'yes') {
            if (get_option('fp_unsubscription_footer_link_text_option') == '2') {
                $replace_footer_text = rac_replace_shortcode_in_custom_footer_text();
                $replace_footer_text = $message . ' ' . $replace_footer_text;
            } else {
                $replace_footer_text = rac_replace_shortcode_in_custom_footer_text();
            }
            return $replace_footer_text;
        } else {
            return $message;
        }
    }

    public static function rac_unsubscription_shortcode($to, $message) {
        if (get_option('fp_unsubscription_link_in_email') == 'yes') {
            $footer_message = rac_replace_shortcode_in_custom_footer_text($to);
            $message = str_replace('{rac.unsubscribe}', $footer_message, $message);
        }
        return $message;
    }

    public static function response_unsubscribe_option_myaccount() {
        if (isset($_POST['getcurrentuser']) && isset($_POST['dataclicked'])) {
            $userid = $_POST['getcurrentuser'];
            $dataclicked = $_POST['dataclicked'];

            if ($dataclicked == 'false') {
                update_user_meta($userid, 'fp_rac_mail_unsubscribed', 'yes');
                echo "1";
            } else {
                delete_user_meta($userid, 'fp_rac_mail_unsubscribed');
                echo "2";
            }
            exit();
        }
    }

    public static function add_undo_unsubscribe_option_myaccount() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#fp_rac_unsubscribe_option').click(function () {
                    var getcurrentuser = "<?php echo get_current_user_id(); ?>";
                    var dataclicked = jQuery(this).is(':checked') ? 'false' : 'true';
                    var data = {
                        action: 'fp_rac_undo_unsubscribe',
                        getcurrentuser: getcurrentuser,
                        dataclicked: dataclicked
                    };
                    jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data,
                            function (response) {
                                response = response.replace(/\s/g, '');
                                jQuery("p.un_sub_email_css").hide();
                                if (response === '1') {
                                    alert("<?php _e("Successfully Unsubscribed...", "recoverabandoncart"); ?>");
                                } else {
                                    alert("<?php _e("Successfully Subscribed...", "recoverabandoncart"); ?>");
                                }
                            });
                });
            });</script>
        <h3><?php echo get_option('rac_unsub_myaccount_heading'); ?></h3>
        <p><input type="checkbox" name="fp_rac_unsubscribe_option" id="fp_rac_unsubscribe_option" value="yes" <?php checked("yes", get_user_meta(get_current_user_id(), 'fp_rac_mail_unsubscribed', true)); ?>/>    <?php echo get_option('rac_unsub_myaccount_text'); ?></p>
        <?php
    }

    public static function manual_unsubscribe_option() {
        if (isset($_COOKIE['un_sub_email_manual'])) {
            $mail_id_to_unsub = $_COOKIE['un_sub_email_manual'];
            if ($mail_id_to_unsub != '') {
                ?>
                <form method="post" id="manual_unsubscibe_form">
                    <input type="hidden" name="email_id_at_session" id="email_id_at_session" value="<?php echo $mail_id_to_unsub ?>">
                <?php
                $user = get_user_by('email', $mail_id_to_unsub);
                if (isset($user->data->ID)) {
                    $user_id = $user->data->ID;
                    $check_already = get_user_meta($user_id, 'fp_rac_mail_unsubscribed', true);
                    if ($check_already == 'yes') {
                        ?>
                            <div class="unsubscribeContent">
                                <div class="mailSubscribe"><strong><?php echo $mail_id_to_unsub ?></strong><br></div>
                                <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_already_unsubscribed_text'); ?></strong><br>
                                </div>
                            </div>
                        <?php
                    } else {
                        ?>
                            <div class="unsubscribeContent">
                                <div class="mailSubscribe"><strong><?php echo $mail_id_to_unsub ?></strong><br></div>
                            <?php if (!isset($_POST['email_id_at_session'])) { ?>
                                    <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_confirm_unsubscription_text'); ?></strong><br>
                                    </div>
                                    <div style="text-align:center">
                                        <input type="submit" class="unsubscribe_button" id="rac_unsubscribe_manually" value="<?php _e('Unsubscribe', 'recoverabandoncart') ?>">
                                    </div>
                        <?php } else {
                            ?>
                                    <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_unsubscribed_successfully_text'); ?></strong></div>
                                    <?php
                                    update_user_meta($user_id, 'fp_rac_mail_unsubscribed', 'yes');
                                    unset($_COOKIE['un_sub_email_manual']);
                                    setcookie('un_sub_email_manual', null);
                                }
                                ?>
                            </div>
                                <?php
                            }
                        } else {
                            $old_array = array_filter(array_unique((array) get_option('fp_rac_mail_unsubscribed')));
                            if (in_array($mail_id_to_unsub, $old_array)) {
                                ?>
                            <div class="unsubscribeContent">
                                <div class="mailSubscribe"><strong><?php echo $mail_id_to_unsub ?></strong><br></div>
                                <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_already_unsubscribed_text'); ?></strong><br>
                                </div>
                            </div>
                        <?php
                    } else {
                        ?>
                            <div class="unsubscribeContent">
                                <div class="mailSubscribe"><strong><?php echo $mail_id_to_unsub ?></strong><br></div>
                            <?php if (!isset($_POST['email_id_at_session'])) { ?>
                                    <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_confirm_unsubscription_text'); ?></strong><br>
                                    </div>
                                    <div style="text-align:center">
                                        <input type="submit" class="unsubscribe_button" id="rac_unsubscribe_manually" value="<?php _e('Unsubscribe', 'recoverabandoncart') ?>">
                                    </div>
                            <?php
                        } else {
                            ?>
                                    <div class="subsInnerContent"><strong class="msgTitle"><?php echo get_option('rac_unsubscribed_successfully_text'); ?></strong></div>
                                    <?php
                                    $old_array = array_filter(array_unique((array) get_option('fp_rac_mail_unsubscribed')));
                                    $listofemails = (array) $mail_id_to_unsub;
                                    $merge_arrays = array_merge($listofemails, $old_array);
                                    update_option('fp_rac_mail_unsubscribed', $merge_arrays);
                                    unset($_COOKIE['un_sub_email_manual']);
                                    setcookie('un_sub_email_manual', null);
                                }
                                ?>
                            </div>
                                <?php
                            }
                        }
                        ?>
                </form>
                <style type="text/css">
                    .unsubscribeContent {
                        border: 1px solid #d6d4d4;
                        border-radius: 10px;
                        box-sizing: border-box;
                        margin: 25px auto;
                        padding: 45px 60px;
                        text-align: center;
                        width: 600px;
                    }
                    .mailSubscribe {
                        border-bottom: 1px solid #dbdedf;
                        font-size: 18px;
                        line-height: 31px;
                        margin-bottom: 30px;
                        padding-bottom: 30px;
                        color: #<?php echo get_option('rac_unsubscription_email_text_color') ?>;
                    }
                    .msgTitle {
                        color: #<?php echo get_option('rac_confirm_unsubscription_text_color') ?>;
                        display: inline-block;
                        font-size: 36px;
                        margin-bottom: 24px;
                    }
                    .unsubscribe_button {
                        background-color: #FF0000;
                        border: medium none;
                        border-radius: 5px;
                        color: #FFFFFF;
                        display: inline-block;
                        font-size: 25px;
                        padding: 10px 24px;
                        text-align: center;
                        text-decoration: none;
                    }
                </style>
                <?php
            }
        }
    }

    public static function rac_cart_details($each_list) {
        ob_start();
        $cart_array = maybe_unserialize($each_list->cart_details);
        $total = '';
        if (is_array($cart_array) && is_null($each_list->ip_address)) {
            $shipping_total = FP_RAC_Polish_Product_Info::fp_rac_get_shipping_total($cart_array);
            $shipping_tax_cost = FP_RAC_Polish_Product_Info::fp_rac_get_shipping_tax_total($cart_array);
            if (isset($cart_array['shipping_details'])) {
                unset($cart_array['shipping_details']);
            }
            foreach ($cart_array as $cart) {
                foreach ($cart as $inside) {
                    foreach ($inside as $product) {
                        $total += ($product['line_subtotal'] + $product['line_subtotal_tax']);
                        $product_names[] = get_the_title($product['product_id']);
                    }
                }
            }
            $total = $total + $shipping_total + $shipping_tax_cost;
            echo implode(' , ', $product_names);
            echo " / " . FP_List_Table_RAC::format_price($total);
        } elseif (is_array($cart_array)) {
            //for cart captured at checkout(GUEST)
            $shipping_total = FP_RAC_Polish_Product_Info::fp_rac_get_shipping_total($cart_array);
            $shipping_tax_cost = FP_RAC_Polish_Product_Info::fp_rac_get_shipping_tax_total($cart_array);
            unset($cart_array['visitor_mail']);
            unset($cart_array['first_name']);
            unset($cart_array['last_name']);
            if (isset($cart_array['visitor_phone'])) {
                unset($cart_array['visitor_phone']);
            }
            if (isset($cart_array['shipping_details'])) {
                unset($cart_array['shipping_details']);
            }
            foreach ($cart_array as $product) {
                $total += ($product['line_subtotal'] + $product['line_subtotal_tax']);
                $product_names[] = get_the_title($product['product_id']);
            }
            $total = $total + $shipping_total + $shipping_tax_cost;
            echo implode(' , ', $product_names);
            echo " / " . FP_List_Table_RAC::format_price($total);
        } elseif (is_object($cart_array)) { // For Guest
            foreach ($cart_array->get_items() as $item) {
                $total += ($item['line_subtotal'] + $item['line_subtotal_tax']);
                $product_names[] = $item['name'];
            }
            $shipping_tax_cost = $cart_array->get_shipping_tax();
            $shipping_total = $cart_array->get_total_shipping();
            $total = $total + $shipping_total + $shipping_tax_cost;
            echo implode(' , ', $product_names);
            echo " / " . FP_List_Table_RAC::format_price($total);
        }
        return ob_get_clean();
    }

    public static function fp_rac_admin_scritps() {
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'fprac_slug') {
                wp_register_style('footable_css', plugins_url('/css/footable.core.css', __FILE__));
                wp_register_script('fp_validate_text_min_max', plugins_url('/js/fp_rac_validate_text_min_max_field.js', __FILE__), array('jquery'));
                wp_localize_script('fp_validate_text_min_max', 'fp_validate_text_params', array(
                    'rac_warning_message' => __('Please enter a value greater than ', 'enhanced validate', 'recoverabandoncart'),
                ));
                wp_enqueue_style('footable_css');
                wp_register_style('footablestand_css', plugins_url('/css/footable.standalone.css', __FILE__));
                wp_enqueue_style('footablestand_css');

                wp_enqueue_script('footable', plugins_url('/js/footable.js', __FILE__), array('jquery'));
                wp_enqueue_script('footable_sorting', plugins_url('/js/footable.sort.js', __FILE__), array('jquery'));
                wp_enqueue_script('footable_paginate', plugins_url('/js/footable.paginate.js', __FILE__), array('jquery'));
                wp_enqueue_script('footable_filter', plugins_url('/js/footable.filter.js', __FILE__), array('jquery'));
                wp_enqueue_script('footable_initialize', plugins_url('/js/footable_initialize.js', __FILE__), array('jquery'));
                wp_enqueue_script('fp_validate_text_min_max', plugins_url('/js/fp_rac_validate_text_min_max_field.js', __FILE__), array('jquery'));
                wp_enqueue_script('racjscolorpicker', plugins_url('/jscolor/jscolor.js', __FILE__), array('jquery'));
                wp_enqueue_style('jquery_smoothness_ui', plugins_url('/css/jquery_smoothness_ui.css', __FILE__));
                wp_enqueue_script('jquery-ui-datepicker');
                wp_enqueue_script('date_picker_initialize', plugins_url('/js/rac_datepicker.js', __FILE__), array('jquery', 'jquery-ui-datepicker'));

                //graph related external files
                $suffix = ".min";
                wp_register_script('flot', WC()->plugin_url() . '/assets/js/jquery-flot/jquery.flot' . $suffix . '.js', array('jquery'), WC_VERSION);
                wp_register_script('flot-resize', WC()->plugin_url() . '/assets/js/jquery-flot/jquery.flot.resize' . $suffix . '.js', array('jquery', 'flot'), WC_VERSION);
                wp_register_script('flot-time', WC()->plugin_url() . '/assets/js/jquery-flot/jquery.flot.time' . $suffix . '.js', array('jquery', 'flot'), WC_VERSION);
                wp_register_script('flot-pie', WC()->plugin_url() . '/assets/js/jquery-flot/jquery.flot.pie' . $suffix . '.js', array('jquery', 'flot'), WC_VERSION);
                wp_register_script('flot-stack', WC()->plugin_url() . '/assets/js/jquery-flot/jquery.flot.stack' . $suffix . '.js', array('jquery', 'flot'), WC_VERSION);
                wp_enqueue_script('flot');
                wp_enqueue_script('flot-resize');
                wp_enqueue_script('flot-time');
                wp_enqueue_script('flot-pie');
                wp_enqueue_script('flot-stack');
            }
        }
    }

    public static function rac_translate_file() {
        load_plugin_textdomain('recoverabandoncart', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

    public static function fp_rac_checkout_script() {
        if ((get_option('rac_load_script_styles') == 'wp_head') || !get_option('rac_load_script_styles')) {
            wp_register_script('rac_guest_handle', plugins_url('/js/rac_guest.js', __FILE__), array('jquery'), WC_VERSION);
            wp_localize_script('rac_guest_handle', 'rac_guest_params', array(
                'console_error' => __('Not a valid e-mail address', 'recoverabandoncart'),
                'ajax_url' => admin_url('admin-ajax.php'),
                'login_details' => is_user_logged_in(),
                'check_out' => is_checkout()
            ));
            wp_enqueue_script('rac_guest_handle', plugins_url('/js/rac_guest.js', __FILE__));
        } else {
            wp_register_script('rac_guest_handle', plugins_url('/js/rac_guest.js', __FILE__), array('jquery'), WC_VERSION);
            wp_localize_script('rac_guest_handle', 'rac_guest_params', array(
                'console_error' => __('Not a valid e-mail address', 'recoverabandoncart'),
                'ajax_url' => admin_url('admin-ajax.php'),
                'login_details' => is_user_logged_in(),
                'check_out' => is_checkout()
            ));
            wp_enqueue_script('rac_guest_handle', plugins_url('/js/rac_guest.js', __FILE__), '', '', true);
        }
    }

    public static function fp_rac_guest_entry_checkout_ajax() {
        global $woocommerce;
        if (!is_user_logged_in()) {
            if (!isset($_COOKIE['rac_cart_id'])) { //means they didn't come mail
                $currentuser_lang = fp_rac_get_current_language();
                $ip_address = rac_get_client_ip();
                $visitor_mail = isset($_POST['rac_email']) ? $_POST['rac_email'] : '';
                $visitor_first_name = isset($_POST['rac_first_name']) ? $_POST['rac_first_name'] : '';
                $visitor_last_name = isset($_POST['rac_last_name']) ? $_POST['rac_last_name'] : '';
                $visitor_phone = isset($_POST['rac_phone']) ? $_POST['rac_phone'] : '';
                $chosen_methods = WC()->session->get('chosen_shipping_methods');
                $chosen_methods = isset($chosen_methods) ? $chosen_methods[0] : '';
                $shipping_cost = isset(WC()->cart->shipping_total) ? WC()->cart->shipping_total : '';
                $shipping_tax_cost = isset(WC()->cart->shipping_tax_total) ? WC()->cart->shipping_tax_total : '';
                $shipping_details = array('shipping_cost' => $shipping_cost, 'shipping_tax_cost' => $shipping_tax_cost, 'shipping_method' => $chosen_methods);
                if ((float) $woocommerce->version < (float) ('2.3.0')) {
                    WC()->cart->calculate_totals();
                }
                if (function_exists('WC')) {
                    $visitor_cart = WC()->cart->get_cart();
                } else {
                    $visitor_cart = $woocommerce->cart->get_cart();
                }
                $visitor_details = $visitor_cart;
                $visitor_details['visitor_mail'] = $visitor_mail;
                $visitor_details['first_name'] = $visitor_first_name;
                $visitor_details['last_name'] = $visitor_last_name;
                $visitor_details['visitor_phone'] = $visitor_phone;
                $visitor_details['shipping_details'] = $shipping_details;
                global $wpdb;
                $table_name = $wpdb->prefix . 'rac_abandoncart';
                $cart_content = maybe_serialize($visitor_details);
                $user_id = "000";
                $current_time = current_time('timestamp');
                if (get_option('rac_remove_carts') == 'yes') {


                    if (get_option('rac_remove_new') == 'yes') {

                        $wpdb->delete($table_name, array('email_id' => $visitor_mail, 'cart_status' => 'NEW'));
                    }

                    if (get_option('rac_remove_abandon') == 'yes') {

                        $wpdb->delete($table_name, array('email_id' => $visitor_mail, 'cart_status' => 'ABANDON'));
                    }
                }

                //check for duplication
                $check_ip = $wpdb->get_results("SELECT * FROM $table_name WHERE ip_address ='$ip_address' AND cart_status='NEW'");
                $check_email = $wpdb->get_results("SELECT * FROM $table_name WHERE email_id ='$visitor_mail' AND cart_status='NEW'");
                if ((!is_null(@$check_ip[0]->id) && !empty($check_ip[0]->id)) || (!is_null(@$check_email[0]->id) && !empty($check_email[0]->id))) {//update
                    $wpdb->update($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $visitor_mail), array('id' => $check_ip[0]->id));
                } else {//Insert New entry
                    $wpdb->insert($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $visitor_mail, 'cart_abandon_time' => $current_time, 'cart_status' => 'NEW', 'ip_address' => $ip_address, 'wpml_lang' => $currentuser_lang));
                    setcookie("rac_checkout_entry", $wpdb->insert_id, time() + 3600, "/");
                }
            }
        }
        exit();
    }

    public static function delete_all_rac_list() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $rows_to_delete = $_POST['listids'];
        $status = $_POST['deletion'];
        foreach ($rows_to_delete as $row_id) {
            if ($status == 'trash') {
                $select_rows = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash') and id=$row_id", OBJECT);
                foreach ($select_rows as $each_row) {
                    $cart_status_old = $each_row->cart_status;
                    $wpdb->update($table_name, array('old_status' => $cart_status_old), array('id' => $row_id));
                }
                $wpdb->update($table_name, array('cart_status' => 'trash'), array('id' => $row_id));
            } elseif ($status == 'restore') {

                $select_rows = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('trash') and id=$row_id", OBJECT);
                if (!empty($select_rows)) {
                    foreach ($select_rows as $each_row) {
                        $cart_status_old = $each_row->cart_status;
                        $wpdb->update($table_name, array('cart_status' => $each_row->old_status), array('id' => $row_id));
                    }
                }
            } else {
                $wpdb->delete($table_name, array('id' => $row_id));
            }
        }
        echo "1";
        exit();
    }

    public static function delete_individual_rac_list() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $row_id = $_POST['row_id'];
        $status = $_POST['deletion'];

        if ($status == 'trash') {
            $select_rows = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash') and id=$row_id", OBJECT);
            foreach ($select_rows as $each_row) {
                $cart_status_old = $each_row->cart_status;
                $wpdb->update($table_name, array('old_status' => $cart_status_old), array('id' => $row_id));
            }
            $wpdb->update($table_name, array('cart_status' => 'trash'), array('id' => $row_id));
        } elseif ($status == 'restore') {
            $select_rows = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('trash') and id=$row_id", OBJECT);
            foreach ($select_rows as $each_row) {
                $cart_status_old = $each_row->old_status;
                $wpdb->update($table_name, array('cart_status' => $cart_status_old), array('id' => $row_id));
            }
        } else {
            $wpdb->delete($table_name, array('id' => $row_id));
        }
        echo "1";
        exit();
    }

    public static function delete_all_rac_log() {
        global $wpdb;
        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
        $rows_to_delete = $_POST['listids'];
        foreach ($rows_to_delete as $row_id) {
            $wpdb->delete($table_name_logs, array('id' => $row_id));
        }
        exit();
    }

    public static function delete_individual_rac_log() {
        global $wpdb;
        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
        $row_id = $_POST['row_id'];
        $wpdb->delete($table_name_logs, array('id' => $row_id));
        exit();
    }

    public static function fp_rac_settings_link($links) {
        $setting_page_link = '<a href="admin.php?page=fprac_slug">Settings</a>';
        array_unshift($links, $setting_page_link);
        return $links;
    }

    public static function fp_rac_reset_general() {
        if (isset($_GET['rac_reset'])) {
            $admin_url = admin_url('admin.php');
            $reset_true_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracgenral', 'resetted' => 'true'), $admin_url));
            update_option('rac_abandon_cart_time_type', 'hours');
            update_option('rac_abandon_cart_time', '1');
            update_option('rac_abandon_cart_time_type_guest', 'hours');
            update_option('rac_abandon_cart_time_guest', '1');
            update_option('rac_abandon_cart_cron_type', 'hours');
            update_option('rac_abandon_cron_time', '12');
            update_option('rac_admin_cart_recovered_noti', 'no');
            update_option('admin_notifi_sender_opt', 'woo');
            update_option('rac_recovered_email_subject', 'A cart has been Recovered');
            update_option('rac_recovered_email_message', 'A cart has been Recovered. Here is the order ID {rac.recovered_order_id} for Reference.');
            wp_redirect($reset_true_url);
            exit;
        }
    }

    public static function set_mail_sending_opt() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $requesting_state = $_POST['current_state'] == 'SEND' ? 'DONT' : 'SEND';
            $wpdb->update($table_name, array('sending_status' => $requesting_state), array('id' => $_POST['row_id']));
            echo $requesting_state;
        }
        exit();
    }

    public static function rac_manual_recovered() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $wpdb->update($table_name, array('cart_status' => 'RECOVERED', 'completed' => 'completed'), array('id' => $_POST['row_id']));
            echo 1;
        }
        exit();
    }

    public static function rac_locate_template($template, $template_name, $template_path) {
        global $woocommerce;

        $default_template = $template;
        if (!$template_path) {
            $template_path = $woocommerce->template_url;
        }

        if ("woocommerce" == get_option('rac_email_use_temp_plain')) {
            // default
            if (!$default_path) {
                if (function_exists('WC')) {
                    $default_path = WC()->plugin_path() . '/templates/';
                } else {
                    $default_path = $woocommerce->plugin_path() . '/templates/';
                }
            }

            $template = $default_path . $template_name;
        } elseif ("theme" == get_option('rac_email_use_temp_plain')) {
            //theme
            $template = locate_template(
                    array(
                        $template_path . $template_name,
                        $template_name
                    )
            );
            // default
            if (!$template) {
                $template = $default_template;
            }
        }

        // Return what we found
        return $template;
    }

    public static function rac_load_mail_message() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $row_id = $_POST['row_id'];
            $table_name = $wpdb->prefix . 'rac_templates_email';
            $templates = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$row_id", OBJECT);
            $template_send = array("mail_send_opt" => $templates[0]->sender_opt,
                "from_name" => $templates[0]->from_name,
                "from_email" => $templates[0]->from_email,
                "subject" => $templates[0]->subject,
                "message" => $templates[0]->message,
                "cart_link_text" => $templates[0]->anchor_text,
                "mail" => $templates[0]->mail,
                "link" => $templates[0]->link,
            );

            echo json_encode($template_send);
        }
        exit();
    }

    public static function add_styles_in_general_tab() {
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'fpracemail') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery(function () {
                            //this is for segmentation.
                            var current_seg_type = jQuery('#rac_template_seg_type').val();
                            jQuery('.rac_colsh').hide();
                            jQuery('.' + current_seg_type).show();
                            jQuery('#rac_template_seg_type').change(function () {
                                var current_seg_type = jQuery('#rac_template_seg_type').val();
                                jQuery('.rac_colsh').hide();
                                jQuery('.' + current_seg_type).show();
                            });
                            var currentplainhtml = jQuery('.rac_template_mail').val();
                            if (currentplainhtml === 'PLAIN') {
                                jQuery('.rac_logo_link').show();
                                jQuery('.rac_email_sender').show();
                                if (jQuery('#rac_sender_woo').is(':checked')) {
                                    jQuery('.rac_local_senders').hide();
                                } else {
                                    jQuery('.rac_local_senders').show();
                                }
                                jQuery('input[name=rac_sender_opt]').change(function () {
                                    if (jQuery('#rac_sender_woo').is(':checked')) {
                                        jQuery('.rac_local_senders').hide();
                                    } else {
                                        jQuery('.rac_local_senders').show();
                                    }
                                });
                            } else {
                                jQuery('.rac_logo_link').hide();
                                jQuery('.rac_email_sender').show();
                                if (jQuery('#rac_sender_woo').is(':checked')) {
                                    jQuery('.rac_local_senders').hide();
                                } else {
                                    jQuery('.rac_local_senders').show();
                                }
                                jQuery('input[name=rac_sender_opt]').change(function () {
                                    if (jQuery('#rac_sender_woo').is(':checked')) {
                                        jQuery('.rac_local_senders').hide();
                                    } else {
                                        jQuery('.rac_local_senders').show();
                                    }
                                });
                            }
                            jQuery('.rac_template_mail').change(function () {
                                var currentplainhtml = jQuery(this).val();
                                if (currentplainhtml === 'PLAIN') {
                                    jQuery('.rac_logo_link').show();
                                    jQuery('.rac_email_sender').show();
                                    if (jQuery('#rac_sender_woo').is(':checked')) {
                                        jQuery('.rac_local_senders').hide();
                                    } else {
                                        jQuery('.rac_local_senders').show();
                                    }
                                    jQuery('input[name=rac_sender_opt]').change(function () {
                                        if (jQuery('#rac_sender_woo').is(':checked')) {
                                            jQuery('.rac_local_senders').hide();
                                        } else {
                                            jQuery('.rac_local_senders').show();
                                        }
                                    });
                                } else {
                                    jQuery('.rac_logo_link').hide();
                                    jQuery('.rac_email_sender').show();
                                    if (jQuery('#rac_sender_woo').is(':checked')) {
                                        jQuery('.rac_local_senders').hide();
                                    } else {
                                        jQuery('.rac_local_senders').show();
                                    }
                                    jQuery('input[name=rac_sender_opt]').change(function () {
                                        if (jQuery('#rac_sender_woo').is(':checked')) {
                                            jQuery('.rac_local_senders').hide();
                                        } else {
                                            jQuery('.rac_local_senders').show();
                                        }
                                    });
                                }
                            });
                        });
                        //Email sending Method
                        if (jQuery('#rac_mail_template_send_method').val() !== 'template_time') {
                            jQuery('#rac_mail_template_sending_priority').parent().parent().hide();
                        } else {
                            jQuery('#rac_mail_template_sending_priority').parent().parent().show();
                        }

                        jQuery('#rac_mail_template_send_method').change(function () {
                            if (jQuery('#rac_mail_template_send_method').val() !== 'template_time') {
                                jQuery('#rac_mail_template_sending_priority').parent().parent().hide();
                            } else {
                                jQuery('#rac_mail_template_sending_priority').parent().parent().show();
                            }
                        });
                        if (jQuery('#rac_cart_link_options').val() === '3') {
                            jQuery('.racbutton').parent().parent().show();
                            jQuery('.raclink').parent().parent().hide();
                        } else if (jQuery('#rac_cart_link_options').val() === '2') {
                            jQuery('.raclink').parent().parent().hide();
                            jQuery('.racbutton').parent().parent().hide();
                        } else {
                            jQuery('.racbutton').parent().parent().hide();
                            jQuery('.raclink').parent().parent().show();
                        }

                        jQuery('#rac_cart_link_options').change(function () {
                            if (jQuery(this).val() === '3') {
                                jQuery('.racbutton').parent().parent().show();
                            } else if (jQuery(this).val() === '2') {
                                jQuery('.racbutton').parent().parent().hide();
                                jQuery('.raclink').parent().parent().hide();
                            } else {
                                jQuery('.racbutton').parent().parent().hide();
                                jQuery('.raclink').parent().parent().show();
                            }
                        });
                    });</script>
                <?php
            }

            if ($_GET['tab'] == 'fpraccoupon') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        if (jQuery('#rac_prefix_coupon').val() === '1') {
                            jQuery('#rac_manual_prefix_coupon_code').parent().parent().hide();
                        } else {
                            jQuery('#rac_manual_prefix_coupon_code').parent().parent().show();
                        }

                        jQuery('#rac_prefix_coupon').change(function () {
                            if (jQuery(this).val() === '1') {
                                jQuery('#rac_manual_prefix_coupon_code').parent().parent().hide();
                            } else {
                                jQuery('#rac_manual_prefix_coupon_code').parent().parent().show();
                            }
                        });
                    });</script>
                <?php
            }
        }
    }

    public static function rac_cart_link_button_mode($cartlink, $cart_text) {
        ob_start();
        ?>
        <table cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" bgcolor="#<?php echo get_option('rac_cart_button_bg_color'); ?>" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block; padding:0px 10px 0px 10px;">
                    <a href="<?php echo $cartlink; ?>" style="text-decoration: none; width:100%; display:inline-block;line-height:40px;"><span style="color: #<?php echo get_option('rac_cart_button_link_color'); ?>"><?php echo $cart_text; ?></span></a>
                </td>
            </tr>
        </table>
        <?php
        $results = ob_get_clean();
        return $results;
    }

    public static function shortcode_in_subject($firstname, $lastname, $subject) {
        $find_array = array('{rac.firstname}', '{rac.lastname}');
        $replace_array = array($firstname, $lastname);
        $subject = str_replace($find_array, $replace_array, $subject);
        return $subject;
    }

    public static function email_woocommerce_html($mail_template_post, $subject, $message, $logo) {

        if (($mail_template_post == 'HTML')) {
            ob_start();
            if (function_exists('wc_get_template')) {
                wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                wc_get_template('emails/email-footer.php');
            } else {

                woocommerce_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                woocommerce_get_template('emails/email-footer.php');
            }
            $woo_temp_msg = ob_get_clean();
        } elseif ($mail_template_post == 'PLAIN') {

            $woo_temp_msg = $logo . $message;
        } else {

            $woo_temp_msg = $message;
        }

        return $woo_temp_msg;
    }

    public static function rac_send_manual_mail() {
        global $wpdb, $woocommerce, $to;
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $abandancart_table_name = $wpdb->prefix . 'rac_abandoncart';
        $sender_option_post = stripslashes($_POST['rac_sender_option']);
        $mail_template_post = stripslashes($_POST['rac_template_mail']);  // mail plain or html
        $mail_logo_added = stripslashes($_POST['rac_logo_mail']);   // mail logo uploaded
        $from_name_post = stripslashes($_POST['rac_from_name']);
        $from_email_post = stripslashes($_POST['rac_from_email']);
        $message_post = stripslashes($_POST['rac_message']);
        $bcc_post = stripslashes($_POST['rac_blind_carbon_copy']);
        $subject_post = stripslashes($_POST['rac_mail_subject']);
        $anchor_text_post = stripslashes($_POST['rac_anchor_text']);
        $mail_row_ids = stripslashes($_POST['rac_mail_row_ids']);
        $row_id_array = explode(',', $mail_row_ids);
        $mail_template_id_post = isset($_POST['template_id']) ? $_POST['template_id'] : '';
        $table_name_email = $wpdb->prefix . 'rac_templates_email';

        $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>'; // mail uploaded
        ?>
        <style type="text/css">
            table {
                border-collapse: separate;
                border-spacing: 0;
                color: #4a4a4d;
                font: 14px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
            }
            th,
            td {
                padding: 10px 15px;
                vertical-align: middle;
            }
            thead {
                background: #395870;
                background: linear-gradient(#49708f, #293f50);
                color: #fff;
                font-size: 11px;
                text-transform: uppercase;
            }
            th:first-child {
                border-top-left-radius: 5px;
                text-align: left;
            }
            th:last-child {
                border-top-right-radius: 5px;
            }
            tbody tr:nth-child(even) {
                background: #f0f0f2;
            }
            td {
                border-bottom: 1px solid #cecfd5;
                border-right: 1px solid #cecfd5;
            }
            td:first-child {
                border-left: 1px solid #cecfd5;
            }
            .book-title {
                color: #395870;
                display: block;
            }
            .text-offset {
                color: #7c7c80;
                font-size: 12px;
            }
            .item-stock,
            .item-qty {
                text-align: center;
            }
            .item-price {
                text-align: right;
            }
            .item-multiple {
                display: block;
            }
            tfoot {
                text-align: right;
            }
            tfoot tr:last-child {
                background: #f0f0f2;
                color: #395870;
                font-weight: bold;
            }
            tfoot tr:last-child td:first-child {
                border-bottom-left-radius: 5px;
            }
            tfoot tr:last-child td:last-child {
                border-bottom-right-radius: 5px;
            }

        </style>
        <?php
        $compact = array($sender_option_post, $from_name_post, $from_email_post);
        $headers = FPRacCron::rac_format_email_headers($compact, $bcc_post);
        foreach ($row_id_array as $row_id) {

            $cart_row = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$row_id", OBJECT);
            //For Member
            $cart_array = maybe_unserialize($cart_row[0]->cart_details);
            $tablecheckproduct = FP_RAC_Polish_Product_Info::fp_rac_extract_cart_details($cart_row[0]);
            if ($cart_row[0]->user_id != '0' && $cart_row[0]->user_id != 'old_order') {
                $sent_mail_templates = maybe_unserialize($cart_row[0]->mail_template_id);
                if (!is_array($sent_mail_templates)) {
                    $sent_mail_templates = array(); // to avoid mail sent/not sent problem for serialization on store
                }

                $current_time = current_time('timestamp');
                @$cart_url = rac_get_page_permalink_dependencies('cart');
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $cart_row[0]->id, 'email_template' => $mail_template_id_post), $cart_url));
                $url_to_click = fp_rac_wpml_convert_url($url_to_click, $cart_row[0]->wpml_lang);
                if (get_option('rac_cart_link_options') == '1') {
                    $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post) . '</a>';
                } elseif (get_option('rac_cart_link_options') == '2') {
                    $url_to_click = $url_to_click;
                } else {
                    $cart_text = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post);
                    $url_to_click = self::rac_cart_link_button_mode($url_to_click, $cart_text);
                }

                $user = get_userdata($cart_row[0]->user_id);
                $to = $user->user_email;
                $firstname = $user->user_firstname;
                $lastname = $user->user_lastname;
                $date = date_i18n(rac_date_format(), $cart_row[0]->cart_abandon_time);
                $time = date_i18n(rac_time_format(), $cart_row[0]->cart_abandon_time);

                $subject = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_subject', $cart_row[0]->wpml_lang, $subject_post);
                $subject = self::shortcode_in_subject($firstname, $lastname, $subject);

                $message = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_message', $cart_row[0]->wpml_lang, $message_post);
                $message = str_replace('{rac.cartlink}', $url_to_click, $message);
                $message = str_replace('{rac.date}', $date, $message);
                $message = str_replace('{rac.time}', $time, $message);
                $message = str_replace('{rac.firstname}', $firstname, $message);
                $message = str_replace('{rac.lastname}', $lastname, $message);

                $message = str_replace('{rac.Productinfo}', $tablecheckproduct, $message);
                if (strpos($message, "{rac.coupon}")) {
                    $coupon_code = FPRacCoupon::rac_create_coupon($user->user_email, $cart_row[0]->cart_abandon_time);
                    update_option('abandon_time_of' . $cart_row[0]->id, $coupon_code);
                    $message = str_replace('{rac.coupon}', $coupon_code, $message); //replacing shortcode with coupon code
                }
                $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
                add_filter('woocommerce_email_footer_text', array('RecoverAbandonCart', 'rac_footer_email_customization'));
                $message = do_shortcode($message); //shortcode feature
                if ($mail_logo_added == '') {
                    $logo = '';
                } else {
                    $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;" src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>'; // mail uploaded
                }
                // mail send plain or html
                $woo_temp_msg = self::email_woocommerce_html($mail_template_post, $subject, $message, $logo);
                // mail send plain or html

                if ('wp_mail' == get_option('rac_trouble_mail')) {
                    if (FPRacCron::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $mail_template_post, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        //add to mail log
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                } else {
                    if (FPRacCron::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        //add to mail log
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                }
            }
            //End Member
            //FOR Guest at place order
            if ($cart_row[0]->user_id == '0' && is_null($cart_row[0]->ip_address)) {
                $sent_mail_templates = maybe_unserialize($cart_row[0]->mail_template_id);
                if (!is_array($sent_mail_templates)) {
                    $sent_mail_templates = array(); // to avoid mail sent/not sent problem for serialization on store
                }
                $current_time = current_time('timestamp');
                @$cart_url = rac_get_page_permalink_dependencies('cart');
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $cart_row[0]->id, 'email_template' => $mail_template_id_post, 'guest' => 'yes'), $cart_url));
                $url_to_click = fp_rac_wpml_convert_url($url_to_click, $cart_row[0]->wpml_lang);
                if (get_option('rac_cart_link_options') == '1') {
                    $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '" href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post) . '</a>';
                } elseif (get_option('rac_cart_link_options') == '2') {
                    $url_to_click = $url_to_click;
                } else {
                    $cart_text = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post);
                    $url_to_click = self::rac_cart_link_button_mode($url_to_click, $cart_text);
                }

                $order_object = maybe_unserialize($cart_row[0]->cart_details);
                $to = $order_object->billing_email;

                $firstname = $order_object->billing_first_name;
                $lastname = $order_object->billing_last_name;
                $date = date_i18n(rac_date_format(), $cart_row[0]->cart_abandon_time);
                $time = date_i18n(rac_time_format(), $cart_row[0]->cart_abandon_time);

                $subject = self::shortcode_in_subject($firstname, $lastname, $subject_post);
                $message = str_replace('{rac.cartlink}', $url_to_click, $message_post);
                $message = str_replace('{rac.date}', $date, $message);
                $message = str_replace('{rac.time}', $time, $message);
                $message = str_replace('{rac.firstname}', $firstname, $message);
                $message = str_replace('{rac.lastname}', $lastname, $message);
                $message = str_replace('{rac.Productinfo}', $tablecheckproduct, $message);
                if (strpos($message, "{rac.coupon}")) {
                    $coupon_code = FPRacCoupon::rac_create_coupon($order_object->billing_email, $cart_row[0]->cart_abandon_time);
                    update_option('abandon_time_of' . $cart_row[0]->id, $coupon_code);
                    $message = str_replace('{rac.coupon}', $coupon_code, $message); //replacing shortcode with coupon code
                }
                $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
                $message = do_shortcode($message); //shortcode feature

                add_filter('woocommerce_email_footer_text', array('RecoverAbandonCart', 'rac_footer_email_customization'));
                if ($mail_logo_added == '') {
                    $logo = '';
                } else {
                    $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;" src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>'; // mail uploaded
                }
                // mail send plain or html
                $woo_temp_msg = self::email_woocommerce_html($mail_template_post, $subject, $message, $logo);
                // mail send plain or html
                if ('wp_mail' == get_option('rac_trouble_mail')) {
                    if (FPRacCron::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $mail_template_post, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                } else {
                    if (FPRacCron::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($store_template_id);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                }
            }
            //END Guest
            //GUEST Checkout
            if ($cart_row[0]->user_id == '0' && !is_null($cart_row[0]->ip_address)) {
                $sent_mail_templates = maybe_unserialize($cart_row[0]->mail_template_id);
                if (!is_array($sent_mail_templates)) {
                    $sent_mail_templates = array(); // to avoid mail sent/not sent problem for serialization on store
                }
                $current_time = current_time('timestamp');
                @$cart_url = rac_get_page_permalink_dependencies('cart');
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $cart_row[0]->id, 'email_template' => $mail_template_id_post, 'guest' => 'yes', 'checkout' => 'yes'), $cart_url));

                $url_to_click = fp_rac_wpml_convert_url($url_to_click, $cart_row[0]->wpml_lang);
                if (get_option('rac_cart_link_options') == '1') {
                    $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post) . '</a>';
                } elseif (get_option('rac_cart_link_options') == '2') {
                    $url_to_click = $url_to_click;
                } else {
                    $cart_text = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post);
                    $url_to_click = self::rac_cart_link_button_mode($url_to_click, $cart_text);
                }

                $order_object = maybe_unserialize($cart_row[0]->cart_details);
                $to = $order_object['visitor_mail'];
                $date = date_i18n(rac_date_format(), $cart_row[0]->cart_abandon_time);
                $time = date_i18n(rac_time_format(), $cart_row[0]->cart_abandon_time);

                $firstname = $order_object['first_name'];
                $lastname = $order_object['last_name'];
                $message = str_replace('{rac.cartlink}', $url_to_click, $message_post);
                $subject = self::shortcode_in_subject($firstname, $lastname, $subject_post);
                $message = str_replace('{rac.date}', $date, $message);
                $message = str_replace('{rac.time}', $time, $message);
                $message = str_replace('{rac.firstname}', $firstname, $message);
                $message = str_replace('{rac.lastname}', $lastname, $message);
                $message = str_replace('{rac.Productinfo}', $tablecheckproduct, $message);
                if (strpos($message, "{rac.coupon}")) {
                    $coupon_code = FPRacCoupon::rac_create_coupon($order_object['visitor_mail'], $cart_row[0]->cart_abandon_time);
                    update_option('abandon_time_of' . $cart_row[0]->id, $coupon_code);
                    $message = str_replace('{rac.coupon}', $coupon_code, $message); //replacing shortcode with coupon code
                }
                $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
                $message = do_shortcode($message); //shortcode feature
                if ($mail_logo_added == '') {
                    $logo = '';
                } else {
                    $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;" src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>'; // mail uploaded
                }
                // mail send plain or html
                $woo_temp_msg = self::email_woocommerce_html($mail_template_post, $subject, $message, $logo);
                // mail send plain or html
                if ('wp_mail' == get_option('rac_trouble_mail')) {
                    if (FPRacCron::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $mail_template_post, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                } else {
                    if (FPRacCron::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                }
            }
            //END Checkout
            //Order Updated
            if ($cart_row[0]->user_id == 'old_order' && is_null($cart_row[0]->ip_address)) {
                $sent_mail_templates = maybe_unserialize($cart_row[0]->mail_template_id);
                $current_time = current_time('timestamp');
                @$cart_url = rac_get_page_permalink_dependencies('cart');
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $cart_row[0]->id, 'email_template' => $mail_template_id_post, 'old_order' => 'yes'), $cart_url));
                $url_to_click = fp_rac_wpml_convert_url($url_to_click, $cart_row[0]->wpml_lang);
                if (get_option('rac_cart_link_options') == '1') {
                    $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post) . '</a>';
                } elseif (get_option('rac_cart_link_options') == '2') {
                    $url_to_click = $url_to_click;
                } else {
                    $cart_text = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', $cart_row[0]->wpml_lang, $anchor_text_post);
                    $url_to_click = self::rac_cart_link_button_mode($url_to_click, $cart_text);
                }

                $order_object = maybe_unserialize($cart_row[0]->cart_details);
                $to = $order_object->billing_email;
                $date = date_i18n(rac_date_format(), $cart_row[0]->cart_abandon_time);
                $time = date_i18n(rac_time_format(), $cart_row[0]->cart_abandon_time);

                $firstname = $order_object->billing_first_name;
                $lastname = $order_object->billing_last_name;
                $message = str_replace('{rac.cartlink}', $url_to_click, $message_post);
                $subject = self::shortcode_in_subject($firstname, $lastname, $subject_post);
                $message = str_replace('{rac.date}', $date, $message);
                $message = str_replace('{rac.time}', $time, $message);
                $message = str_replace('{rac.firstname}', $firstname, $message);
                $message = str_replace('{rac.lastname}', $lastname, $message);
                $message = str_replace('{rac.Productinfo}', $tablecheckproduct, $message);
                if (strpos($message, "{rac.coupon}")) {
                    $coupon_code = FPRacCoupon::rac_create_coupon($order_object->billing_email, $cart_row[0]->cart_abandon_time);
                    update_option('abandon_time_of' . $cart_row[0]->id, $coupon_code);
                    $message = str_replace('{rac.coupon}', $coupon_code, $message); //replacing shortcode with coupon code
                }
                $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
                $message = do_shortcode($message); //shortcode feature
                if ($mail_logo_added == '') {
                    $logo = '';
                } else {
                    $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;" src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>'; // mail uploaded
                }
                // mail send plain or html
                $woo_temp_msg = self::email_woocommerce_html($mail_template_post, $subject, $message, $logo);
                // mail send plain or html
                if ('wp_mail' == get_option('rac_trouble_mail')) {
                    if (FPRacCron::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $mail_template_post, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        //add to mail log
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                } else {
                    if (FPRacCron::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact)) {
                        $sent_mail_templates[] = $mail_template_id_post;
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id), array('id' => $cart_row[0]->id));
                        //add to mail log
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $template_used = $mail_template_id_post . '- Manual';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $cart_row[0]->id, "template_used" => $template_used));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($mail_template_id_post);
                    }
                }
            }
        }

        exit();
    }

    public static function rac_send_test_mail() {
        $to = $_POST['rac_test_mail_to'];
        $plain_or_html = $_POST['rac_plain_or_html'] == '1' ? "plain" : "html";

        $subject = "Test E-Mail";
        $message = "This is a test E-Mail to Make sure E-Mail are sent successfully from your site.";

        if ($plain_or_html == 'html') {
            ob_start();
            if (function_exists('wc_get_template')) {
                wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                wc_get_template('emails/email-footer.php');
            } else {
                woocommerce_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                woocommerce_get_template('emails/email-footer.php');
            }
            $message = ob_get_clean();
        } else {
            $message = $message;
        }

        if (get_option('rac_mime_mail_header_ts') != 'none') {//check for to aviod header duplication
            $headers = "MIME-Version: 1.0\r\n";
        }
        $headers .= FPRacCron::rac_formatted_from_address_woocommerce();
        if (get_option('rac_replyto_mail_header_ts') != 'none') {//check for to aviod header duplication
            $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
        }
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        if ('wp_mail' == get_option('rac_trouble_mail')) {
            if (FPRacCron::rac_send_wp_mail_test($to, $subject, $message, $headers)) {
                echo "sent";
            }
        } else {
            if (FPRacCron::rac_send_mail_test($to, $subject, $message, $headers)) {
                echo "sent";
            }
        }

        exit();
    }

    public static function get_user_role_jquery() {
        global $woocommerce;
        ?>
        <script type="text/javascript">
            jQuery(function () {
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    jQuery("#custom_user_role").chosen();
                    jQuery("#rac_mailcartlist_change").chosen();
                    jQuery("#rac_template_seg_selected_user_role").chosen();
                    jQuery("#custom_user_role_for_restrict_in_cart_list").chosen();
                    jQuery("#rac_template_seg_selected_product_in_cart").chosen();
                    jQuery("#rac_select_category_to_enable_redeeming").chosen();
                    jQuery("#rac_exclude_category_to_enable_redeeming").chosen();
        <?php } else {
            ?>
                    jQuery("#rac_template_seg_selected_user_role").select2();
                    jQuery("#custom_user_role").select2();
                    jQuery("#custom_user_role_for_restrict_in_cart_list").select2();
                    jQuery("#rac_mailcartlist_change").select2();
                    jQuery("#rac_select_category_to_enable_redeeming").select2();
                    jQuery("#rac_exclude_category_to_enable_redeeming").select2();
        <?php }
        ?>

                var getselectedvalue = jQuery('#custom_exclude').val() || [];
                if (getselectedvalue === 'user_role') {
                    jQuery('#custom_user_role').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_user_role').parent().parent().css("display", "none");
                }
                jQuery('#custom_exclude').change(function () {
                    if (jQuery(this).val() === 'user_role') {
                        jQuery('#custom_user_role').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_user_role').parent().parent().css("display", "none");
                    }
                });
                var getselecteddvalue = jQuery('#custom_exclude').val() || [];
                if (getselecteddvalue === 'name') {
                    jQuery('#custom_user_name_select').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_user_name_select').parent().parent().css("display", "none");
                }
                jQuery('#custom_exclude').change(function () {
                    if (jQuery(this).val() === 'name') {
                        jQuery('#custom_user_name_select').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_user_name_select').parent().parent().css("display", "none");
                    }
                });
                var getselecteddvalue = jQuery('#custom_exclude').val() || [];
                if (getselecteddvalue === 'mail_id') {
                    jQuery('#custom_mailid_edit').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_mailid_edit').parent().parent().css("display", "none");
                }
                jQuery('#custom_exclude').change(function () {
                    if (jQuery(this).val() === 'mail_id') {
                        jQuery('#custom_mailid_edit').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_mailid_edit').parent().parent().css("display", "none");
                    }
                });
                var getselectedvalue_fr_cl = jQuery('#custom_restrict').val() || [];
                if (getselectedvalue_fr_cl === 'user_role') {
                    jQuery('#custom_user_role_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_user_role_for_restrict_in_cart_list').parent().parent().css("display", "none");
                }
                jQuery('#custom_restrict').change(function () {
                    if (jQuery(this).val() === 'user_role') {
                        jQuery('#custom_user_role_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_user_role_for_restrict_in_cart_list').parent().parent().css("display", "none");
                    }
                });
                var getselecteddvalue_fr_cl = jQuery('#custom_restrict').val() || [];
                if (getselecteddvalue_fr_cl === 'name') {
                    jQuery('#custom_user_name_select_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_user_name_select_for_restrict_in_cart_list').parent().parent().css("display", "none");
                }
                jQuery('#custom_restrict').change(function () {
                    if (jQuery(this).val() === 'name') {
                        jQuery('#custom_user_name_select_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_user_name_select_for_restrict_in_cart_list').parent().parent().css("display", "none");
                    }
                });
                var getselecteddvalue_fr_cl = jQuery('#custom_restrict').val() || [];
                if (getselecteddvalue_fr_cl === 'mail_id') {
                    jQuery('#custom_mailid_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                } else {
                    jQuery('#custom_mailid_for_restrict_in_cart_list').parent().parent().css("display", "none");
                }
                jQuery('#custom_restrict').change(function () {
                    if (jQuery(this).val() === 'mail_id') {
                        jQuery('#custom_mailid_for_restrict_in_cart_list').parent().parent().css("display", "table-row");
                    } else {
                        jQuery('#custom_mailid_for_restrict_in_cart_list').parent().parent().css("display", "none");
                    }
                });
                var enable_delete_abandon_carts = jQuery('#enable_remove_abandon_after_x_days').val();
                if (enable_delete_abandon_carts === 'no') {
                    jQuery('#rac_remove_abandon_after_x_days').parent().parent().css("display", "none");
                } else {
                    jQuery('#rac_remove_abandon_after_x_days').parent().parent().css("display", "table-row");
                }
                jQuery('#enable_remove_abandon_after_x_days').change(function () {
                    if (jQuery(this).val() === 'no') {
                        jQuery('#rac_remove_abandon_after_x_days').parent().parent().css("display", "none");
                    } else {

                        jQuery('#rac_remove_abandon_after_x_days').parent().parent().css("display", "table-row");
                    }

                });
                var uploader_open;
                jQuery('.upload_button').click(function (e) {
                    e.preventDefault();
                    if (uploader_open) {
                        uploader_open.open();
                        return;
                    }

                    uploader_open = wp.media.frames.uploader_open = wp.media({
                        title: 'Media Uploader',
                        button: {
                            text: 'Media Uploader'
                        },
                        multiple: false
                    });
                    //When a file is selected, grab the URL and set it as the text field's value
                    uploader_open.on('select', function () {
                        attachment = uploader_open.state().get('selection').first().toJSON();
                        jQuery('#rac_logo_mail').val(attachment.url);
                    });
                    uploader_open.open();
                });
                var checked = jQuery('.rac_cartlist_new_abandon_recover').is(':checked') ? "1" : "0";
                if (checked === '1') {
                    jQuery('.rac_cart_depends_parent_new_abandon_option').parent().parent().parent().parent().show();
                } else {
                    jQuery('.rac_cart_depends_parent_new_abandon_option').parent().parent().parent().parent().hide();
                }
                jQuery(document).on('change', '.rac_cartlist_new_abandon_recover', function () {
                    if (jQuery(this).is(':checked')) {
                        jQuery('.rac_cart_depends_parent_new_abandon_option').parent().parent().parent().parent().show();
                    } else {
                        jQuery('.rac_cart_depends_parent_new_abandon_option').parent().parent().parent().parent().hide();
                    }
                });
                var unsubscription_type = jQuery("input[name='rac_unsubscription_type']:checked").val();
                if (unsubscription_type == '1') {
                    jQuery('#rac_unsubscription_redirect_url').parent().parent().show();
                    jQuery('#rac_unsubscription_message_text_color').closest('tr').show();
                    jQuery('#rac_unsubscription_message_background_color').closest('tr').show();
                    jQuery('#rac_manual_unsubscription_redirect_url').parent().parent().hide();
                    jQuery('#rac_confirm_unsubscription_text').parent().parent().hide();
                    jQuery('#rac_unsubscription_email_text_color').closest('tr').hide();
                    jQuery('#rac_confirm_unsubscription_text_color').parent().parent().hide();
                } else {
                    jQuery('#rac_unsubscription_redirect_url').parent().parent().hide();
                    jQuery('#rac_unsubscription_message_text_color').closest('tr').hide();
                    jQuery('#rac_unsubscription_message_background_color').closest('tr').hide();
                    jQuery('#rac_manual_unsubscription_redirect_url').parent().parent().show();
                    jQuery('#rac_confirm_unsubscription_text').parent().parent().show();
                    jQuery('#rac_unsubscription_email_text_color').closest('tr').show();
                    jQuery('#rac_confirm_unsubscription_text_color').parent().parent().show();
                }
                jQuery("input[name='rac_unsubscription_type']").change(function () {
                    if (jQuery(this).val() == '1') {
                        jQuery('#rac_unsubscription_redirect_url').parent().parent().show();
                        jQuery('#rac_unsubscription_message_text_color').closest('tr').show();
                        jQuery('#rac_unsubscription_message_background_color').closest('tr').show();
                        jQuery('#rac_manual_unsubscription_redirect_url').parent().parent().hide();
                        jQuery('#rac_confirm_unsubscription_text').parent().parent().hide();
                        jQuery('#rac_unsubscription_email_text_color').closest('tr').hide();
                        jQuery('#rac_confirm_unsubscription_text_color').parent().parent().hide();
                    } else {
                        jQuery('#rac_unsubscription_redirect_url').parent().parent().hide();
                        jQuery('#rac_unsubscription_message_text_color').closest('tr').hide();
                        jQuery('#rac_unsubscription_message_background_color').closest('tr').hide();
                        jQuery('#rac_manual_unsubscription_redirect_url').parent().parent().show();
                        jQuery('#rac_confirm_unsubscription_text').parent().parent().show();
                        jQuery('#rac_unsubscription_email_text_color').closest('tr').show();
                        jQuery('#rac_confirm_unsubscription_text_color').parent().parent().show();
                    }
                });
                var product_info_rows_sh_option = jQuery("input[name='rac_hide_tax_total_product_info_shortcode']").is(":checked");
                if (product_info_rows_sh_option == true) {
                    jQuery('#rac_hide_tax_row_product_info_shortcode').closest('tr').hide();
                    jQuery('#rac_hide_shipping_row_product_info_shortcode').closest('tr').hide();
                } else {
                    jQuery('#rac_hide_tax_row_product_info_shortcode').closest('tr').show();
                    jQuery('#rac_hide_shipping_row_product_info_shortcode').closest('tr').show();
                }
                jQuery("input[name='rac_hide_tax_total_product_info_shortcode']").click(function () {
                    var product_info_rows_sh_option = jQuery("input[name='rac_hide_tax_total_product_info_shortcode']").is(":checked");
                    if (product_info_rows_sh_option == true) {
                        jQuery('#rac_hide_tax_row_product_info_shortcode').closest('tr').hide();
                        jQuery('#rac_hide_shipping_row_product_info_shortcode').closest('tr').hide();
                    } else {
                        jQuery('#rac_hide_tax_row_product_info_shortcode').closest('tr').show();
                        jQuery('#rac_hide_shipping_row_product_info_shortcode').closest('tr').show();
                    }
                });
            });</script>
        <?php
    }

// Unset Cookie in RAC when they process placeorder
}

include_once (ABSPATH . 'wp-admin/includes/plugin.php');
add_action('init', array('RecoverAbandonCart', 'fprac_check_woo_active'));
if (isset($_GET['page'])) {
    if (($_GET['page'] == 'fprac_slug')) {
        add_action('admin_head', array('RecoverAbandonCart', 'get_user_role_jquery'));
    }
}

add_action('admin_menu', array('RecoverAbandonCart', 'fprac_admin_submenu'));
add_action('admin_init', array('RecoverAbandonCart', 'fp_rac_reset_general'));

$fp_rac = plugin_basename(__FILE__);
add_filter("plugin_action_links_$fp_rac", array('RecoverAbandonCart', 'fp_rac_settings_link'));
add_filter('woocommerce_fprac_settings_tabs_array', array('RecoverAbandonCart', 'fprac_settings_tabs'));
if (isset($_GET['page'])) {
    if ($_GET['page'] == 'fprac_slug') {
        add_filter('woocommerce_screen_ids', array('RecoverAbandonCart', 'fprac_access_woo_script'), 9, 1);
    }
}

require_once 'inc/fp_rac_cron.php';
register_activation_hook(__FILE__, array('RecoverAbandonCart', 'create_load_table'));
register_activation_hook(__FILE__, array('FP_RAC_Email_Tab', 'fprac_email_default_settings'));
register_activation_hook(__FILE__, array('FP_RAC_Coupon_Tab', 'fprac_coupon_default_settings'));
register_activation_hook(__FILE__, array('FP_RAC_General_Tab', 'fprac_general_default_settings'));
register_activation_hook(__FILE__, array('FP_RAC_Troubleshoot_tab', 'fprac_trouble_default_settings'));

add_action('wp_login', array('RecoverAbandonCart', 'remove_action_hook'), 1);
add_filter('set-screen-option', array('RecoverAbandonCart', 'set_screen_option_value'), 10, 3);

add_action('init', array('RecoverAbandonCart', 'fprac_header_problems'));
add_action('woocommerce_cart_updated', array('RecoverAbandonCart', 'fp_rac_insert_entry'));

add_action('wp_enqueue_scripts', array('RecoverAbandonCart', 'fp_rac_checkout_script'));
add_action('admin_enqueue_scripts', array('RecoverAbandonCart', 'fp_rac_admin_scritps'));

add_action('wp_ajax_deletecartlist', array('RecoverAbandonCart', 'delete_all_rac_list'));
add_action('wp_ajax_rac_delete_individual_list', array('RecoverAbandonCart', 'delete_individual_rac_list'));
add_action('wp_ajax_deletemaillog', array('RecoverAbandonCart', 'delete_all_rac_log'));
add_action('wp_ajax_rac_delete_individual_log', array('RecoverAbandonCart', 'delete_individual_rac_log'));
add_action('wp_ajax_rac_start_stop_mail', array('RecoverAbandonCart', 'set_mail_sending_opt'));
add_action('wp_ajax_rac_load_mail_message', array('RecoverAbandonCart', 'rac_load_mail_message'));
add_action('wp_ajax_rac_manual_mail_ajax', array('RecoverAbandonCart', 'rac_send_manual_mail'));
add_action('wp_ajax_rac_send_test_mail', array('RecoverAbandonCart', 'rac_send_test_mail'));
add_action('wp_ajax_rac_manual_recovered', array('RecoverAbandonCart', 'rac_manual_recovered'));
add_action('wp_ajax_nopriv_rac_preadd_guest', array('RecoverAbandonCart', 'fp_rac_guest_entry_checkout_ajax'));

add_action('wp_ajax_edit_value_update_now', array('RecoverAbandonCart', 'fp_rac_edit_mail_update_data'));
//ASN
add_action('plugins_loaded', array('RecoverAbandonCart', 'rac_translate_file'));

register_activation_hook(__FILE__, array('FPRacCron', 'fp_rac_cron_job_setting'));
register_activation_hook(__FILE__, array('RecoverAbandonCart', 'fprac_header_problems'));


add_filter('cron_schedules', array('FPRacCron', 'fp_rac_add_x_hourly'));
add_action('update_option_rac_abandon_cart_cron_type', array('FPRacCron', 'fp_rac_cron_job_setting_savings'));
add_action('update_option_rac_abandon_cron_time', array('FPRacCron', 'fp_rac_cron_job_setting_savings'));

add_action('rac_cron_job', array('FPRacCron', 'fp_rac_cron_job_mailing'));
add_action('wp_head', array('FPRacCron', 'rac_delete_abandon_carts_after_selected_days'));
add_action('admin_head', array('FPRacCron', 'rac_delete_abandon_carts_after_selected_days'));

add_action('admin_head', array('RecoverAbandonCart', 'fp_rac_troubleshoot_mailsend'));
add_action('wp_head', array('RecoverAbandonCart', 'unsubscribed_user_from_rac_mail'));

if (get_option('rac_unsub_myaccount_option') == 'yes') {
    add_action('woocommerce_before_my_account', array('RecoverAbandonCart', 'add_undo_unsubscribe_option_myaccount'));
}
add_action('wp_ajax_fp_rac_undo_unsubscribe', array('RecoverAbandonCart', 'response_unsubscribe_option_myaccount'));
add_shortcode('rac.unsubscribe_email_manual', array('RecoverAbandonCart', 'manual_unsubscribe_option'));
add_action('admin_head', array('RecoverAbandonCart', 'add_styles_in_general_tab'));


//For Deletion of coupon code
require_once 'inc/fp_rac_coupon_deletion.php';
include 'inc/fp_rac_add_cancelled_order_immediately.php';
//For Backword Compatibility
require_once 'inc/rac_settings_backward_compatibility.php';

//For WPML
function fp_get_wpml_text($option_name, $language, $message) {
    if (function_exists('icl_register_string')) {
        if ($language == 'en') {
            return $message;
        } else {
            global $wpdb;
            $context = 'RAC';

            $res = $wpdb->get_results($wpdb->prepare("
            SELECT s.name, s.value, t.value AS translation_value, t.status
            FROM  {$wpdb->prefix}icl_strings s
            LEFT JOIN {$wpdb->prefix}icl_string_translations t ON s.id = t.string_id
            WHERE s.context = %s
                AND (t.language = %s OR t.language IS NULL)
            ", $context, $language), ARRAY_A);
            foreach ($res as $each_entry) {
                if ($each_entry['name'] == $option_name) {
                    if ($each_entry['translation_value']) {
                        $translated = $each_entry['translation_value'];
                    } else {
                        $translated = $each_entry['value'];
                    }
                }
            }
            return $translated;
        }
    } else {
        return $message;
    }
}

function rac_register_template_for_wpml() {

    if (function_exists('icl_register_string')) {

        global $wpdb;
        $context = 'RAC';
        $template_table = $wpdb->prefix . 'rac_templates_email';
        $re = $wpdb->get_results("SELECT * FROM $template_table");
        foreach ($re as $each_template) {
            $name_msg = 'rac_template_' . $each_template->id . '_message';
            $value_msg = $each_template->message;
            icl_register_string($context, $name_msg, $value_msg); //for registering message
            $name_sub = 'rac_template_' . $each_template->id . '_subject';
            $value_sub = $each_template->subject;
            icl_register_string($context, $name_sub, $value_sub); //for registering subject

            $name_anchortext = 'rac_template_' . $each_template->id . '_anchor_text';
            $getvalue_anchortext = $each_template->anchor_text;
            icl_register_string($context, $name_anchortext, $getvalue_anchortext);

            $productname = 'rac_template_product_name';
            $getvalue_productname = get_option('rac_product_info_product_name');
            icl_register_string($context, $productname, $getvalue_productname);

            $productimage = 'rac_template_product_image';
            $getvalue_productimage = get_option('rac_product_info_product_image');
            icl_register_string($context, $productimage, $getvalue_productimage);


            $productquantity = 'rac_template_product_quantity';
            $getvalue_quantity = get_option('rac_product_info_quantity');
            icl_register_string($context, $productquantity, $getvalue_quantity);

            $productprice = 'rac_template_product_price';
            $getvalue_productprice = get_option('rac_product_info_product_price');
            icl_register_string($context, $productprice, $getvalue_productprice);

            $subtotal = 'rac_template_subtotal';
            $getvalue_subtotal = get_option('rac_product_info_subtotal');
            icl_register_string($context, $subtotal, $getvalue_subtotal);

            $shipping = 'rac_template_shipping';
            $getvalue_shipping = get_option('rac_product_info_shipping');
            icl_register_string($context, $shipping, $getvalue_shipping);

            $tax = 'rac_template_tax';
            $getvalue_tax = get_option('rac_product_info_tax');
            icl_register_string($context, $tax, $getvalue_tax);

            $total = 'rac_template_total';
            $getvalue_total = get_option('rac_product_info_total');
            icl_register_string($context, $total, $getvalue_total);
        }
    }
}

add_action('admin_init', 'rac_register_template_for_wpml');

function rac_date_format() {
    $date_format = get_option('rac_date_format');
    if ($date_format == '') {
        $date_format = 'd:m:y';
    }
    return $date_format;
}

function rac_time_format() {
    $time_format = get_option('rac_time_format');
    if ($time_format == '') {
        $time_format = 'h:i:s';
    }
    return $time_format;
}

function rac_get_client_ip() {
    $ipaddress = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (!empty($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];

    return $ipaddress;
}

function rac_replace_shortcode_in_custom_footer_text($to = '') {
    global $to;
    if (get_option('rac_unsubscription_type') != '2') {
        $siteurl = get_option('rac_unsubscription_redirect_url');
        if ($siteurl) {
            $site_url = $siteurl;
        } else {
            $site_url = get_permalink(wc_get_page_id('myaccount'));
        }
    } else {
        $siteurl = get_option('rac_manual_unsubscription_redirect_url');
        if ($siteurl) {
            $site_url = $siteurl;
        } else {
            $site_url = get_permalink(wc_get_page_id('myaccount'));
        }
    }
    $site_name = get_bloginfo('name'); // Site Name
    $create_nonce = wp_create_nonce('myemail');
    $unsublink = esc_url(add_query_arg(array('email' => $to, 'action' => 'unsubscribe', '_mynonce' => $create_nonce), $site_url));
    $footer_link_text = get_option('fp_unsubscription_footer_link_text');
    $footer_message = get_option('fp_unsubscription_footer_message');
    $find_shortcode = array('{rac_unsubscribe}', '{rac_site}');
    $unsublink = '<a href="' . $unsublink . '">' . $footer_link_text . '</a>';
    $replace_shortcode = array($unsublink, $site_name);
    $footer_message = str_replace($find_shortcode, $replace_shortcode, $footer_message);
    return $footer_message;
}

function fp_rac_wpml_convert_url($url, $lan = null) {
    if (class_exists('SitePress')) {
        global $sitepress;
        $lang_change_url = $sitepress->convert_url($url, $lan);
        return $lang_change_url;
    }
    return $url;
}

function rac_get_page_permalink_dependencies($page) {
    $redirect_url = get_permalink(get_option('woocommerce_' . $page . '_page_id'));
    return $redirect_url;
}

function fp_rac_placeholder_img() {
    $size = array(
        'width' => '90',
        'height' => '90',
        'crop' => 1
    );

    return '<img src="' . wc_placeholder_img_src() . '" alt="Placeholder" width="' . esc_attr($size['width']) . '" height="' . esc_attr($size['height']) . '" />';
}

function fp_rac_get_current_language() {
    if (function_exists('icl_register_string')) {
        $currentuser_lang = isset($_SESSION['wpml_globalcart_language']) ? $_SESSION['wpml_globalcart_language'] : ICL_LANGUAGE_CODE;
    } else {
        $currentuser_lang = 'en';
    }
    return $currentuser_lang;
}

function fp_rac_check_column_exists($table_name, $column_name) {
    global $wpdb;
    $data_base = constant('DB_NAME');
    $column_exists = $wpdb->query("select * from information_schema.columns where table_schema='$data_base' and table_name = '$table_name' and column_name = '$column_name'");
    if ($column_exists <= 0) {
        return true;
    }
    return false;
}

function fp_rac_add_extra_column_in_email_template() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'rac_templates_email';
    if (fp_rac_check_column_exists($table_name, 'segmentation')) {
        $wpdb->query("ALTER TABLE $table_name ADD segmentation LONGTEXT NOT NULL");
    }
}

function fp_rac_add_extra_column_in_cart_list_table() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'rac_abandoncart';
    if (fp_rac_check_column_exists($table_name, 'mail_template_sending_time')) {
        $wpdb->query("ALTER TABLE $table_name ADD mail_template_sending_time LONGTEXT after mail_template_id");
    }
}
