if (!rac_guest_params.login_details && rac_guest_params.check_out) {
    jQuery(document).ready(function () {
        jQuery("#billing_email").on("keyup keypress change focusout", function () {
            fp_rac_common_function_for_checkout_fields();
        });
        jQuery("#billing_first_name").on("change", function () {
            fp_rac_common_function_for_checkout_fields();
        });
        jQuery("#billing_last_name").on("change", function () {
            fp_rac_common_function_for_checkout_fields();
        });
        jQuery("#billing_phone").on("change", function () {
            fp_rac_common_function_for_checkout_fields();
        });
        function fp_rac_common_function_for_checkout_fields() {
            var fp_rac_mail = jQuery("#billing_email").val();
            var atpos = fp_rac_mail.indexOf("@");
            var dotpos = fp_rac_mail.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= fp_rac_mail.length)
            {
                console.log(rac_guest_params.console_error);
                //return false;
            }
            else {
                console.log(fp_rac_mail);
                var fp_rac_first_name = jQuery("#billing_first_name").val();
                var fp_rac_last_name = jQuery("#billing_last_name").val();
                var fp_rac_phone = jQuery("#billing_phone").val();
                var data = {
                    action: "rac_preadd_guest",
                    rac_email: fp_rac_mail,
                    rac_first_name: fp_rac_first_name,
                    rac_last_name: fp_rac_last_name,
                    rac_phone: fp_rac_phone
                }
                jQuery.post(rac_guest_params.ajax_url, data,
                        function (response) {
                            //alert(response);
                            console.log(response);
                        });
            }
        }
    });
}