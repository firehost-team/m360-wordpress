<?php

/*
 * Send Email Via Woocommerce Mailer
 */

class FP_RAC_Send_Email_Woocommerce_Mailer {

    private static $from_email_address;
    private static $from_name;

    public static function send_email_via_woocommerce_mailer($to, $subject, $message, $headers, $compact) {
        if (!empty($compact) && $compact[0] == 'local') {
            self::$from_name = $compact[1];
            self::$from_email_address = $compact[2];
            add_filter('woocommerce_email_from_address', array('FP_RAC_Send_Email_Woocommerce_Mailer', 'alter_from_email_of_woocommerce'), 10, 2);
            add_filter('woocommerce_email_from_name', array('FP_RAC_Send_Email_Woocommerce_Mailer', 'alter_from_name_of_woocommerce'), 10, 2);
            $mailer = WC()->mailer();
            $mailer->send($to, $subject, $message, $headers, '');
            remove_filter('woocommerce_email_from_address', array('FP_RAC_Send_Email_Woocommerce_Mailer', 'alter_from_email_of_woocommerce'), 10, 2);
            remove_filter('woocommerce_email_from_name', array('FP_RAC_Send_Email_Woocommerce_Mailer', 'alter_from_name_of_woocommerce'), 10, 2);
            self::$from_email_address = false;
            self::$from_name = false;
        } else {
            $mailer = WC()->mailer();
            $mailer->send($to, $subject, $message, $headers, '');
        }
    }

    //alter From Name 
    public function alter_from_email_of_woocommerce($from_email, $object) {
        $get_email_address = self::$from_email_address;
        if ($get_email_address) {
            return $get_email_address;
        }
        return $from_email;
    }

    //alter From address
    public function alter_from_name_of_woocommerce($from_name, $object) {
        $get_from_email_name = self::$from_name;
        if ($get_from_email_name) {
            return $get_from_email_name;
        }
        return $from_name;
    }

}
