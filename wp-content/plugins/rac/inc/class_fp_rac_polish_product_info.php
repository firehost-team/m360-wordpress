<?php

class FP_RAC_Polish_Product_Info {

    //Polish the Product Info using Cart Details

    public static function fp_rac_extract_cart_details($each_cart) {
        ob_start();
        global $woocommerce;
        if ((float) $woocommerce->version < (float) ('2.4.0')) {
            ?>
            <style type="text/css">
                table .td{
                    border: 1px solid #e4e4e4 !important;
                }

            </style>
        <?php } ?>
        <table class="<?php echo FPRacCron::enable_border() ?>" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', 'Helvetica', 'Roboto', 'Arial', 'sans-serif';" >
            <thead>
                <?php echo self::fp_rac_get_sortable_column_name($each_cart->wpml_lang); ?>
            </thead>
            <tbody>
                <?php
                $tax = '';
                $subtotal = '';
                $tax_total = '';
                $shipping_total = '';
                $shipping_tax_cost = '';
                $shipping_method_title = '';
                $cart_array = maybe_unserialize($each_cart->cart_details);
                if (is_array($cart_array) && (!empty($cart_array))) {
                    $shipping_total = self::fp_rac_get_shipping_total($cart_array);
                    $shipping_tax_cost = self::fp_rac_get_shipping_tax_total($cart_array);
                    $shipping_method_title = self::fp_rac_get_shipping_method_tilte($cart_array);
                    $shipping = self::fp_rac_get_shipping_details($shipping_total, $shipping_method_title, $shipping_tax_cost);
                    if (isset($cart_array['shipping_details'])) {
                        unset($cart_array['shipping_details']);
                    }
                    if (isset($cart_array[0]['cart'])) {
                        foreach ($cart_array[0]['cart'] as $eachproduct) {
                            $product_name = get_the_title($eachproduct['product_id']);
                            if (get_option('rac_email_product_variation_sh') != 'no') {
                                if (isset($eachproduct['variation']) && (!empty($eachproduct['variation']))) {
                                    $product_name = $product_name . '<br />' . self::fp_rac_get_formatted_variation($eachproduct);
                                }
                            }
                            $productid = $eachproduct['product_id'];
                            $imageurl = "";
                            if ((get_post_thumbnail_id($eachproduct['variation_id']) != "") || (get_post_thumbnail_id($eachproduct['variation_id']) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($eachproduct['variation_id']));
                                $imageurl = $image_urls[0];
                            }
                            if ($imageurl == "") {
                                if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                    $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                    $imageurl = $image_urls[0];
                                } else {
                                    $imageurl = esc_url(wc_placeholder_img_src());
                                }
                            }
                            $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                            $quantity = $eachproduct['quantity'];
                            if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                                $price = $eachproduct['line_subtotal'] + $eachproduct['line_subtotal_tax'];
                                $tax = 0;
                                $subtotal += $eachproduct['line_subtotal'] + $eachproduct['line_subtotal_tax'];
                            } else {
                                $price = $eachproduct['line_subtotal'];
                                $tax += $eachproduct['line_subtotal_tax'];
                                $subtotal += $eachproduct['line_subtotal'];
                            }

                            echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                        }
                    } elseif (is_array($cart_array) && (!empty($cart_array))) {
                        if (isset($cart_array['visitor_mail'])) {
                            unset($cart_array['visitor_mail']);
                        }
                        if (isset($cart_array['first_name'])) {
                            unset($cart_array['first_name']);
                        }
                        if (isset($cart_array['last_name'])) {
                            unset($cart_array['last_name']);
                        }
                        if (isset($cart_array['visitor_phone'])) {
                            unset($cart_array['visitor_phone']);
                        }
                        foreach ($cart_array as $myproducts) {
                            $product_name = get_the_title($myproducts['product_id']);
                            if (get_option('rac_email_product_variation_sh') != 'no') {
                                if (isset($myproducts['variation']) && (!empty($myproducts['variation']))) {
                                    $product_name = $product_name . '<br />' . self::fp_rac_get_formatted_variation($myproducts);
                                }
                            }
                            $productid = $myproducts['product_id'];
                            $imageurl = "";
                            if ((get_post_thumbnail_id($myproducts['variation_id']) != "") || (get_post_thumbnail_id($myproducts['variation_id']) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($myproducts['variation_id']));
                                $imageurl = $image_urls[0];
                            }
                            if ($imageurl == "") {
                                if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                    $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                    $imageurl = $image_urls[0];
                                } else {
                                    $imageurl = esc_url(wc_placeholder_img_src());
                                }
                            }
                            $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                            $quantity = $myproducts['quantity'];
                            if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                                $price = $myproducts['line_subtotal'] + $myproducts['line_subtotal_tax'];
                                $tax = 0;
                                $subtotal += $myproducts['line_subtotal'] + $myproducts['line_subtotal_tax'];
                            } else {
                                $price = $myproducts['line_subtotal'];
                                $tax += $myproducts['line_subtotal_tax'];
                                $subtotal += $myproducts['line_subtotal'];
                            }
                            echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                        }
                    }
                } elseif (is_object($cart_array)) {
                    $order = new WC_Order($cart_array->id);
                    $shipping_tax_cost = $order->get_shipping_tax();
                    $shipping_total = $order->get_total_shipping();
                    $shipping_method_title = $order->get_shipping_method();
                    $shipping = self::fp_rac_get_shipping_details($shipping_total, $shipping_method_title, $shipping_tax_cost);
                    foreach ($order->get_items() as $products) {
                        $product_name = get_the_title($products['product_id']);
                        if (get_option('rac_email_product_variation_sh') != 'no') {
                            if (isset($products['variation_id']) && (!empty($products['variation_id']))) {
                                $product = $order->get_product_from_item($products);
                                $item_meta = new WC_Order_Item_Meta($products, $product);
                                $attributes = $item_meta->display(true, true, '_', '<br />');
                                $product_name = $product_name . '<br />' . $attributes;
                            }
                        }
                        $productid = $products['product_id'];
                        $imageurl = "";
                        if ((get_post_thumbnail_id($products['variation_id']) != "") || (get_post_thumbnail_id($products['variation_id']) != 0)) {
                            $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($products['variation_id']));
                            $imageurl = $image_urls[0];
                        }
                        if ($imageurl == "") {
                            if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                $imageurl = $image_urls[0];
                            } else {
                                $imageurl = esc_url(wc_placeholder_img_src());
                            }
                        }
                        $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                        $quantity = $products['qty'];
                        if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                            $price = $products['line_subtotal'] + $products['line_subtotal_tax'];
                            $tax = 0;
                            $subtotal += $products['line_subtotal'] + $products['line_subtotal_tax'];
                        } else {
                            $price = $products['line_subtotal'];
                            $tax += $products['line_subtotal_tax'];
                            $subtotal += $products['line_subtotal'];
                        }
                        echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                    }
                }
                $tax_total = $tax + $shipping_tax_cost;
                ?>
            </tbody>
            <?php if (get_option('rac_hide_tax_total_product_info_shortcode') != 'yes') { ?>
                <tfoot>
                    <tr>
                        <?php $i = 1; ?>
                        <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_subtotal', $each_cart->wpml_lang, get_option('rac_product_info_subtotal')); ?></th>
                        <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price($subtotal); ?></td>
                    </tr>
                    <?php if ($shipping_method_title != '' && get_option('rac_hide_shipping_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <?php $i = 1; ?>
                            <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo fp_get_wpml_text('rac_template_shipping', $each_cart->wpml_lang, get_option('rac_product_info_shipping')); ?></th>
                            <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo $shipping; ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($tax > 0 && get_option('rac_hide_tax_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <?php
                            $i = 1;
                            ?>
                            <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_tax', $each_cart->wpml_lang, get_option('rac_product_info_tax')); ?></th>
                            <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price($tax_total); ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <?php $i = 1; ?>
                        <th class="<?php echo FPRacCron::enable_border() ?>" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo fp_get_wpml_text('rac_template_total', $each_cart->wpml_lang, get_option('rac_product_info_total')); ?></th>
                        <td class="<?php echo FPRacCron::enable_border() ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo FP_List_Table_RAC::format_price($subtotal + $tax_total + $shipping_total); ?></td>
                    </tr>

                </tfoot>
            <?php } ?>
        </table>
        <?php
        return ob_get_clean();
    }

    public static function fp_split_rac_items_in_cart($product_name, $image, $quantity, $price) {
        ob_start();
        ?><tr>
            <?php
            $default_column = array('product_name', 'product_image', 'product_quantity', 'product_price');
            $sortable_column = get_option('drag_and_drop_product_info_sortable_column');
            $sortable_column = is_array($sortable_column) && !empty($sortable_column) ? $sortable_column : $default_column;
            foreach ($sortable_column as $column_key_name) {
                $product_details = $column_key_name == 'product_name' ? $product_name : ($column_key_name == 'product_image' ? $image : ($column_key_name == 'product_quantity' ? $quantity : $price));
                if (get_option('rac_hide_' . $column_key_name . '_product_info_shortcode') != 'yes') {
                    ?>
                    <td class="<?php echo FPRacCron::enable_border() ?>" colspan="<?php echo fp_rac_get_column_span_count(2); ?>" style="text-align:left; vertical-align:middle; font-family: 'Helvetica Neue', 'Helvetica', 'Roboto', 'Arial', 'sans-serif'; word-wrap:break-word;">
                        <?php echo $product_details; ?>
                    </td>
                    <?php
                }
            }
            ?> </tr><?php
        return ob_get_clean();
    }

    public static function rac_show_cart_products_brief($each_cart) {
        ob_start();
        ?>
        <table class="td" cellspacing="0" cellpadding="6" style="width:50%; font-family: 'Helvetica Neue', 'Helvetica', 'Roboto', 'Arial', 'sans-serif';" border="1" >
            <thead>
                <?php echo self::fp_rac_get_sortable_column_name($each_cart->wpml_lang); ?>
            </thead>
            <tbody>
                <?php
                $tax = '';
                $subtotal = '';
                $tax_total = '';
                $shipping_total = '';
                $shipping_tax_cost = '';
                $shipping_method_title = '';
                $cart_array = maybe_unserialize($each_cart->cart_details);
                if (is_array($cart_array) && (!empty($cart_array))) {
                    $shipping_total = self::fp_rac_get_shipping_total($cart_array);
                    $shipping_tax_cost = self::fp_rac_get_shipping_tax_total($cart_array);
                    $shipping_method_title = self::fp_rac_get_shipping_method_tilte($cart_array);
                    $shipping = self::fp_rac_get_shipping_details($shipping_total, $shipping_method_title, $shipping_tax_cost);
                    if (isset($cart_array['shipping_details'])) {
                        unset($cart_array['shipping_details']);
                    }
                    if (isset($cart_array[0]['cart'])) {
                        foreach ($cart_array[0]['cart'] as $eachproduct) {
                            $product_name = get_the_title($eachproduct['product_id']);
                            if (get_option('rac_email_product_variation_sh') != 'no') {
                                if (isset($eachproduct['variation']) && (!empty($eachproduct['variation']))) {
                                    $product_name = $product_name . '<br />' . self::fp_rac_get_formatted_variation($eachproduct);
                                }
                            }
                            $productid = $eachproduct['product_id'];
                            $imageurl = "";
                            if ((get_post_thumbnail_id($eachproduct['variation_id']) != "") || (get_post_thumbnail_id($eachproduct['variation_id']) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($eachproduct['variation_id']));
                                $imageurl = $image_urls[0];
                            }
                            if ($imageurl == "") {
                                if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                    $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                    $imageurl = $image_urls[0];
                                } else {
                                    $imageurl = esc_url(wc_placeholder_img_src());
                                }
                            }
                            $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                            $quantity = $eachproduct['quantity'];
                            if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                                $price = $eachproduct['line_subtotal'] + $eachproduct['line_subtotal_tax'];
                                $tax = 0;
                                $subtotal += $eachproduct['line_subtotal'] + $eachproduct['line_subtotal_tax'];
                            } else {
                                $price = $eachproduct['line_subtotal'];
                                $tax += $eachproduct['line_subtotal_tax'];
                                $subtotal += $eachproduct['line_subtotal'];
                            }

                            echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                        }
                    } elseif (is_array($cart_array) && (!empty($cart_array))) {
                        if (isset($cart_array['visitor_mail'])) {
                            unset($cart_array['visitor_mail']);
                        }
                        if (isset($cart_array['first_name'])) {
                            unset($cart_array['first_name']);
                        }
                        if (isset($cart_array['last_name'])) {
                            unset($cart_array['last_name']);
                        }
                        if (isset($cart_array['visitor_phone'])) {
                            unset($cart_array['visitor_phone']);
                        }
                        foreach ($cart_array as $myproducts) {
                            $product_name = get_the_title($myproducts['product_id']);
                            if (get_option('rac_email_product_variation_sh') != 'no') {
                                if (isset($myproducts['variation']) && (!empty($myproducts['variation']))) {
                                    $product_name = $product_name . '<br />' . self::fp_rac_get_formatted_variation($myproducts);
                                }
                            }
                            $productid = $myproducts['product_id'];
                            $imageurl = "";
                            if ((get_post_thumbnail_id($myproducts['variation_id']) != "") || (get_post_thumbnail_id($myproducts['variation_id']) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($myproducts['variation_id']));
                                $imageurl = $image_urls[0];
                            }
                            if ($imageurl == "") {
                                if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                    $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                    $imageurl = $image_urls[0];
                                } else {
                                    $imageurl = esc_url(wc_placeholder_img_src());
                                }
                            }
                            $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                            $quantity = $myproducts['quantity'];
                            if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                                $price = $myproducts['line_subtotal'] + $myproducts['line_subtotal_tax'];
                                $tax = 0;
                                $subtotal += $myproducts['line_subtotal'] + $myproducts['line_subtotal_tax'];
                            } else {
                                $price = $myproducts['line_subtotal'];
                                $tax += $myproducts['line_subtotal_tax'];
                                $subtotal += $myproducts['line_subtotal'];
                            }
                            echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                        }
                    }
                } elseif (is_object($cart_array)) {
                    $order = new WC_Order($cart_array->id);
                    $shipping_tax_cost = $order->get_shipping_tax();
                    $shipping_total = $order->get_total_shipping();
                    $shipping_method_title = $order->get_shipping_method();
                    $shipping = self::fp_rac_get_shipping_details($shipping_total, $shipping_method_title, $shipping_tax_cost);
                    foreach ($order->get_items() as $products) {
                        $product_name = get_the_title($products['product_id']);
                        if (get_option('rac_email_product_variation_sh') != 'no') {
                            if (isset($products['variation_id']) && (!empty($products['variation_id']))) {
                                $product = $order->get_product_from_item($products);
                                $item_meta = new WC_Order_Item_Meta($products, $product);
                                $attributes = $item_meta->display(true, true, '_', '<br />');
                                $product_name = $product_name . '<br />' . $attributes;
                            }
                        }
                        $productid = $products['product_id'];
                        $imageurl = "";
                        if ((get_post_thumbnail_id($products['variation_id']) != "") || (get_post_thumbnail_id($products['variation_id']) != 0)) {
                            $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($products['variation_id']));
                            $imageurl = $image_urls[0];
                        }
                        if ($imageurl == "") {
                            if ((get_post_thumbnail_id($productid) != "") || (get_post_thumbnail_id($productid) != 0)) {
                                $image_urls = wp_get_attachment_image_src(get_post_thumbnail_id($productid));
                                $imageurl = $image_urls[0];
                            } else {
                                $imageurl = esc_url(wc_placeholder_img_src());
                            }
                        }
                        $image = '<img src="' . $imageurl . '" alt="' . get_the_title($productid) . '" height="90" width="90" />';
                        $quantity = $products['qty'];
                        if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') == 'yes') {
                            $price = $products['line_subtotal'] + $products['line_subtotal_tax'];
                            $tax = 0;
                            $subtotal += $products['line_subtotal'] + $products['line_subtotal_tax'];
                        } else {
                            $price = $products['line_subtotal'];
                            $tax += $products['line_subtotal_tax'];
                            $subtotal += $products['line_subtotal'];
                        }
                        echo self::fp_split_rac_items_in_cart($product_name, $image, $quantity, FP_List_Table_RAC::format_price($price));
                    }
                }
                $tax_total = $tax + $shipping_tax_cost;
                ?>
            </tbody>
            <?php if (get_option('rac_hide_tax_total_product_info_shortcode') != 'yes') { ?>
                <tfoot>
                    <tr>
                        <?php $i = 1; ?>
                        <th class="td" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo fp_get_wpml_text('rac_template_subtotal', $each_cart->wpml_lang, get_option('rac_product_info_subtotal')); ?></th>
                        <td class="td" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo FP_List_Table_RAC::format_price($subtotal); ?></td>
                    </tr>
                    <?php if ($shipping_method_title != '' && get_option('rac_hide_shipping_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <?php $i = 1; ?>
                            <th class="td" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo fp_get_wpml_text('rac_template_shipping', $each_cart->wpml_lang, get_option('rac_product_info_shipping')); ?></th>
                            <td class="td" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo $shipping; ?></td>
                        </tr>
                    <?php } ?> 
                    <?php if ($tax > 0 && get_option('rac_hide_tax_row_product_info_shortcode') != 'yes') { ?>
                        <tr>
                            <?php
                            $i = 1;
                            ?>
                            <th class="td" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo fp_get_wpml_text('rac_template_tax', $each_cart->wpml_lang, get_option('rac_product_info_tax')); ?></th>
                            <td class="td" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo FP_List_Table_RAC::format_price($tax_total); ?></td>

                        </tr>
                    <?php } ?>
                    <tr>
                        <?php $i = 1; ?>
                        <th class="td" scope="row" colspan="<?php echo fp_rac_get_column_span_count(); ?>" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo fp_get_wpml_text('rac_template_total', $each_cart->wpml_lang, get_option('rac_product_info_total')); ?></th>
                        <td class="td" style="text-align:left; <?php if ($i == 1) echo 'border-top-width: 1px;'; ?>"><?php echo FP_List_Table_RAC::format_price($subtotal + $tax_total + $shipping_total); ?></td>
                    </tr>

                </tfoot>
            <?php } ?>
        </table>
        <?php
        return ob_get_clean();
    }

    /**
     * Get the formatted Attribute variations.
     * @param  Object Variations.
     * @return String
     */
    public static function fp_rac_get_formatted_variation($variations) {
        $product_id = $variations['product_id'];
        $product = get_product($product_id);
        $formatted_attributes = '';
        $attributes = explode(',', wc_get_formatted_variation($variations['variation'], true));
        foreach ($attributes as $each_attribute) {
            $explode_data = explode(':', $each_attribute);
            if (isset($explode_data[0]) && isset($explode_data[1])) {
                $formatted_attributes.= wc_attribute_label($explode_data[0], $product) . ':' . $explode_data[1] . '<br />';
            }
        }
        return $formatted_attributes;
    }

    /**
     * Get the Shipping Total.
     * @param  array CartContents.
     * @return float
     */
    public static function fp_rac_get_shipping_total($cart_array) {
        if (isset($cart_array['shipping_details']['shipping_cost'])) {
            $shipping_total = $cart_array['shipping_details']['shipping_cost'] != '' ? $cart_array['shipping_details']['shipping_cost'] : (float) 0;
            return $shipping_total;
        }
        return '';
    }

    /**
     * Get the Shipping Tax Total.
     * @param  array CartContents.
     * @return float
     */
    public static function fp_rac_get_shipping_tax_total($cart_array) {
        if (isset($cart_array['shipping_details']['shipping_tax_cost'])) {
            $shipping_tax_cost = $cart_array['shipping_details']['shipping_tax_cost'] != '' ? $cart_array['shipping_details']['shipping_tax_cost'] : (float) 0;
            return $shipping_tax_cost;
        }
        return '';
    }

    /**
     * Get the Shipping method Title.
     * @param  array CartContents.
     * @return string
     */
    public static function fp_rac_get_shipping_method_tilte($cart_array) {
        if (isset($cart_array['shipping_details']['shipping_method'])) {
            $current_chosen_method = $cart_array['shipping_details']['shipping_method'];
            $shipping_method_title = self::fp_rac_api_get_shipping_method_title($current_chosen_method);
            return $shipping_method_title;
        }
        return '';
    }

    /**
     * Get the Shipping method Title.
     * @param  boolean $current_chosen_method
     * @return string
     */
    public static function fp_rac_api_get_shipping_method_title($current_chosen_method) {
        if ($current_chosen_method != '') {
            $explode_shipping_method_id = explode(':', $current_chosen_method);
            $method_id = $explode_shipping_method_id[0];
            $instance_id = isset($explode_shipping_method_id[1]) ? $explode_shipping_method_id[1] : '';
            $wc_shipping = WC_Shipping::instance();
            if (method_exists('WC_Shipping', 'load_shipping_methods')) {
                $allowed_classes = $wc_shipping->load_shipping_methods();
            } else {
                $allowed_classes = $wc_shipping->get_shipping_method_class_names();
            }
            if (!empty($method_id) && in_array($method_id, array_keys($allowed_classes))) {
                $class_name = $allowed_classes[$method_id];
                if (is_object($class_name)) {
                    $class_name = get_class($class_name);
                }
                $method_object = new $class_name($instance_id);
                if (is_object($method_object)) {
                    $shipping_method_title = $method_object->title;
                    return $shipping_method_title;
                }
            }
        }
        return '';
    }

    /**
     * Get the Shipping cost Details.
     * @param  string $total
     * @param  string $method_title
     * @return string
     */
    public static function fp_rac_get_shipping_details($total, $method_title, $shipping_tax_cost) {
        if ($total > 0) {
            if (get_option('rac_inc_tax_with_product_price_product_info_shortcode') != 'no') {
                $total = $shipping_tax_cost + $total;
            }
            return $method_title . ': ' . FP_List_Table_RAC::format_price($total);
        } else {
            return $method_title;
        }
    }

    /**
     * Get sortable coulmn name.
     * @param  string $lang
     * @return string
     */
    public static function fp_rac_get_sortable_column_name($lang) {
        ob_start();
        $default_column = array('product_name', 'product_image', 'product_quantity', 'product_price');
        $sortable_column = get_option('drag_and_drop_product_info_sortable_column');
        $sortable_column = is_array($sortable_column) && !empty($sortable_column) ? $sortable_column : $default_column;
        foreach ($sortable_column as $column_key_name) {
            if ($column_key_name == 'product_name') {
                $product_details = get_option('rac_product_info_product_name');
            } elseif ($column_key_name == 'product_image') {
                $product_details = get_option('rac_product_info_product_image');
            } elseif ($column_key_name == 'product_quantity') {
                $product_details = get_option('rac_product_info_quantity');
            } elseif ($column_key_name == 'product_price') {
                $product_details = get_option('rac_product_info_product_price');
            }
            if (get_option('rac_hide_' . $column_key_name . '_product_info_shortcode') != 'yes') {
                ?>
                <th class="<?php echo FPRacCron::enable_border() ?>" colspan="<?php echo fp_rac_get_column_span_count(2); ?>" scope="col" style="text-align:left;">
                    <?php echo fp_get_wpml_text('rac_template_' . $column_key_name, $lang, $product_details); ?>
                </th>
                <?php
            }
        }
        return ob_get_clean();
    }

}

function fp_rac_get_column_span_count($value = '') {
    $i = 3;
    $i = get_option('rac_hide_product_name_product_info_shortcode') == 'yes' ? $i - 1 : $i;
    $i = get_option('rac_hide_product_image_product_info_shortcode') == 'yes' ? $i - 1 : $i;
    $i = get_option('rac_hide_product_quantity_product_info_shortcode') == 'yes' ? $i - 1 : $i;
    $i = get_option('rac_hide_product_price_product_info_shortcode') == 'yes' ? $i - 1 : $i;
    if ($i <= 0 && $value != '') {
        return $value;
    }
    if ($value == '') {
        return $i;
    } else {
        return '';
    }
}
