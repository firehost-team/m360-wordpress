<?php

class FPRacCron {

    public static function fp_rac_cron_job_setting() {
        wp_clear_scheduled_hook('rac_cron_job');
        if (wp_next_scheduled('rac_cron_job') == false) {
            wp_schedule_event(time(), 'xhourly', 'rac_cron_job');
        }
    }

    //on save clear and set the cron again
    public static function fp_rac_cron_job_setting_savings() {
        wp_clear_scheduled_hook('rac_cron_job');
        if (wp_next_scheduled('rac_cron_job') == false) {
            wp_schedule_event(time(), 'xhourly', 'rac_cron_job');
        }
    }

    public static function fp_rac_add_x_hourly($schedules) {

        $interval = get_option('rac_abandon_cron_time');
        if (get_option('rac_abandon_cart_cron_type') == 'minutes') {
            $interval = $interval * 60;
        } else if (get_option('rac_abandon_cart_cron_type') == 'hours') {
            $interval = $interval * 3600;
        } else if (get_option('rac_abandon_cart_cron_type') == 'days') {
            $interval = $interval * 86400;
        }
        $schedules['xhourly'] = array(
            'interval' => $interval,
            'display' => 'X Hourly'
        );
        return $schedules;
    }

    public static function get_rac_formatprice($price) {
        if (function_exists('woocommerce_price')) {
            return woocommerce_price($price);
        } else {
            if (function_exists('wc_price')) {
                return wc_price($price);
            }
        }
    }

    public static function mailing() {
        foreach ($email_templates as $emails) {
            
        }
    }

    public static function email_woocommerce_html($html_template, $subject, $message, $logo) {
        if (($html_template == 'HTML')) {
            ob_start();
            if (function_exists('wc_get_template')) {
                wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                wc_get_template('emails/email-footer.php');
            } else {

                woocommerce_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                woocommerce_get_template('emails/email-footer.php');
            }
            $woo_temp_msg = ob_get_clean();
        } else {

            $woo_temp_msg = $logo . $message;
        }

        return $woo_temp_msg;
    }

    public static function rac_delete_abandon_carts_after_selected_days() {
        global $wpdb;
        $abandancart_table_name = $wpdb->prefix . 'rac_abandoncart';
        $abandon_carts = $wpdb->get_results("SELECT * FROM $abandancart_table_name WHERE cart_status='ABANDON'");
        if (get_option('enable_remove_abandon_after_x_days') == 'yes') {
            foreach ($abandon_carts as $each_cart) {
                $duration = get_option('rac_remove_abandon_after_x_days') * 86400;
                $cut_off_time = $each_cart->cart_abandon_time + $duration;
                $current_time = strtotime(date_i18n('Y-m-d h:i:s'));
                if ($current_time >= $cut_off_time) {
                    $wpdb->query("DELETE FROM $abandancart_table_name WHERE id=$each_cart->id");
                }
            }
        }
    }

    public static function fp_rac_cron_job_mailing() {
        global $wpdb;
        $emailtemplate_table_name = $wpdb->prefix . 'rac_templates_email';
        $abandancart_table_name = $wpdb->prefix . 'rac_abandoncart';
        if (get_option('rac_mail_template_send_method') == 'template_time') {
            if (get_option('rac_mail_template_sending_priority') != 'mailsequence') {
                $email_templates = $wpdb->get_results("SELECT * FROM $emailtemplate_table_name ORDER BY sending_type DESC ,sending_duration ASC");
            } else {
                $email_templates = $wpdb->get_results("SELECT * FROM $emailtemplate_table_name ORDER BY id ASC");
            }
        } else {
            $email_templates = $wpdb->get_results("SELECT * FROM $emailtemplate_table_name ORDER BY id ASC");
        }
        //all email templates
        // For Members
        if (get_option('rac_email_use_members') == 'yes') {
            $abandon_carts = $wpdb->get_results("SELECT * FROM $abandancart_table_name WHERE cart_status='ABANDON' AND user_id NOT IN('0','old_order')  AND placed_order IS NULL AND completed IS NULL ORDER BY id ASC"); //Selected only cart which are not completed
            foreach ($abandon_carts as $each_cart) {
                foreach ($email_templates as $emails) {
                    if ($emails->status == "ACTIVE") {
                        if (FP_RAC_Segmentation::check_send_mail_based_on($each_cart, $emails, $each_cart->user_id, $each_cart->email_id)) {
                            $find_user = 'member';
                            self::send_mail_by_mail_sending_option($each_cart, $emails, $find_user);
                        }
                    }
                }
            }
        }
        // FOR GUEST
        if (get_option('rac_email_use_guests') == 'yes') {
            $abandon_carts = $wpdb->get_results("SELECT * FROM $abandancart_table_name WHERE cart_status='ABANDON' AND user_id='0' AND placed_order IS NULL and ip_address IS NULL AND completed IS NULL ORDER BY id ASC"); //Selected only cart which are not completed
            foreach ($abandon_carts as $each_cart) {
                foreach ($email_templates as $emails) {
                    if ($emails->status == "ACTIVE") {
                        if (FP_RAC_Segmentation::check_send_mail_based_on($each_cart, $emails, $each_cart->user_id, $each_cart->email_id)) {
                            $find_user = 'guest1';
                            self::send_mail_by_mail_sending_option($each_cart, $emails, $find_user);
                        }
                    }
                }
            }
            //FOR Guest Captured in chcekout page
            $abandon_carts = $wpdb->get_results("SELECT * FROM $abandancart_table_name WHERE cart_status='ABANDON' and placed_order IS NULL  AND user_id='0' AND ip_address IS NOT NULL AND completed IS NULL ORDER BY id ASC"); //Selected only cart which are not completed
            foreach ($abandon_carts as $each_cart) {
                foreach ($email_templates as $emails) {
                    $cart_array = maybe_unserialize($each_cart->cart_details);
                    if ($emails->status == "ACTIVE") {
                        if (FP_RAC_Segmentation::check_send_mail_based_on($each_cart, $emails, $each_cart->user_id, $each_cart->email_id)) {
                            $find_user = 'guest2';
                            self::send_mail_by_mail_sending_option($each_cart, $emails, $find_user);
                        }
                    }
                }
            }
        }
        // FOR ORDER UPDATED FROM OLD
        $abandon_carts = $wpdb->get_results("SELECT * FROM $abandancart_table_name WHERE cart_status='ABANDON' AND user_id='old_order' AND placed_order IS NULL AND ip_address IS NULL AND completed IS NULL ORDER BY id ASC"); //Selected only cart which are not completed
        foreach ($abandon_carts as $each_cart) {
            foreach ($email_templates as $emails) {
                $cart_array = maybe_unserialize($each_cart->cart_details);
                $id = $cart_array->id;
                $order_obj = new WC_Order($id);
                $main_check = '0';
                $user_id = get_post_meta($id, '_customer_user', true);
                $email_id = get_post_meta($id, '_billing_email', true);
                if ($order_obj->user_id != '') {
                    if (get_option('rac_email_use_members') == 'yes') {
                        // For Controlling Email Id for Member/Guest
                        $main_check = '1';
                    }
                } else {
                    if (get_option('rac_email_use_guests') == 'yes') {
                        $main_check = '1';
                    }
                }
                if ($emails->status == "ACTIVE") {
                    if ($main_check != '0') {
                        if (FP_RAC_Segmentation::check_send_mail_based_on($each_cart, $emails, $user_id, $email_id)) {
                            $find_user = 'old_order';
                            self::send_mail_by_mail_sending_option($each_cart, $emails, $find_user);
                        }
                    }
                }
            }
        }
    }

    public static function send_mail_by_mail_sending_option($each_cart, $emails, $find_user) {
        global $wpdb, $to;
        $sent_mail_templates = '';
        $abandancart_table_name = $wpdb->prefix . 'rac_abandoncart';
        $tablecheckproduct = FP_RAC_Polish_Product_Info::fp_rac_extract_cart_details($each_cart);
        fp_rac_add_extra_column_in_cart_list_table(); //check and add mail_template_sending_time column in cart list table
        $abandon_carts_check = $wpdb->get_results("SELECT mail_template_id,mail_template_sending_time FROM $abandancart_table_name WHERE cart_status='ABANDON' AND id=$each_cart->id"); //Selected only cart which are not completed
        $duration = self::sending_duration($emails);
        $current_time = current_time('timestamp');
        $abandon_multiple_emails_check = $abandon_carts_check[0]->mail_template_id;
        $store_template_sending_time = $abandon_carts_check[0]->mail_template_sending_time;
        $store_sending_time = maybe_unserialize($store_template_sending_time);
        $sent_mail_templates = maybe_unserialize($abandon_multiple_emails_check);
        $date = date_i18n(rac_date_format(), $each_cart->cart_abandon_time);
        $time = date_i18n(rac_time_format(), $each_cart->cart_abandon_time);
        //check for sending time duration and duplicate template id
        if (is_null($abandon_multiple_emails_check)) { // IF EMPTY IT IS NOT SENT FOR ANY SINGLE TEMPLATE
            $cut_off_time = $each_cart->cart_abandon_time + $duration;
        } elseif (!is_null($abandon_multiple_emails_check)) {// IF EMPTY IT IS NOT SENT FOR ANY SINGLE TEMPLATE END
            if (!in_array($emails->id, (array) $sent_mail_templates)) {
                if (get_option('rac_mail_template_send_method') == 'template_time') {
                    if (is_null($store_sending_time)) {
                        $cut_off_time = $each_cart->cart_abandon_time + $duration;
                    } else {
                        $cut_off_time = end($store_sending_time) + $duration;
                    }
                } else {
                    $cut_off_time = $each_cart->cart_abandon_time + $duration;
                }
            } else {
                $cut_off_time = $current_time + 1;
            }
        }//end

        if ($current_time > $cut_off_time) {
            @$cart_url = rac_get_page_permalink_dependencies('cart');
            if ($find_user == 'member') {
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $each_cart->id, 'email_template' => $emails->id), $cart_url));
                $user = get_userdata($each_cart->user_id);
                $to = $user->user_email;
                $firstname = $user->user_firstname;
                $lastname = $user->user_lastname;
            } elseif ($find_user == 'guest1') {
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $each_cart->id, 'email_template' => $emails->id, 'guest' => 'yes'), $cart_url));
                @$order_object = maybe_unserialize($each_cart->cart_details);
                $to = $order_object->billing_email;
                $subject = fp_get_wpml_text('rac_template_' . $emails->id . '_subject', $each_cart->wpml_lang, $emails->subject);
                $firstname = $order_object->billing_first_name;
                $lastname = $order_object->billing_last_name;
            } elseif ($find_user == 'guest2') {
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $each_cart->id, 'email_template' => $emails->id, 'guest' => 'yes'), $cart_url));
                @$order_object = maybe_unserialize($each_cart->cart_details);
                $to = $order_object['visitor_mail'];
                $subject = fp_get_wpml_text('rac_template_' . $emails->id . '_subject', $each_cart->wpml_lang, $emails->subject);
                $firstname = $order_object['first_name'];
                $lastname = $order_object['last_name'];
            } elseif ($find_user == 'old_order') {
                $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => $each_cart->id, 'email_template' => $emails->id, 'old_order' => 'yes'), $cart_url));
                @$cart_array = maybe_unserialize($each_cart->cart_details);
                $id = $cart_array->id;
                $order_object = new WC_Order($id);
                $to = $order_object->billing_email;
                $subject = fp_get_wpml_text('rac_template_' . $emails->id . '_subject', $each_cart->wpml_lang, $emails->subject);
                $firstname = $order_object->billing_first_name;
                $lastname = $order_object->billing_last_name;
            }

            $url_to_click = fp_rac_wpml_convert_url($url_to_click, $each_cart->wpml_lang);
            if (get_option('rac_cart_link_options') == '1') {
                $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $emails->id . '_anchor_text', $each_cart->wpml_lang, $emails->anchor_text) . '</a>';
            } elseif (get_option('rac_cart_link_options') == '2') {
                $url_to_click = $url_to_click;
            } else {
                $cart_Text = fp_get_wpml_text('rac_template_' . $emails->id . '_anchor_text', $each_cart->wpml_lang, $emails->anchor_text);
                $url_to_click = RecoverAbandonCart::rac_cart_link_button_mode($url_to_click, $cart_Text);
            }

            $subject = fp_get_wpml_text('rac_template_' . $emails->id . '_subject', $each_cart->wpml_lang, $emails->subject);
            $message = fp_get_wpml_text('rac_template_' . $emails->id . '_message', $each_cart->wpml_lang, $emails->message);
            $subject = RecoverAbandonCart::shortcode_in_subject($firstname, $lastname, $subject);
            $message = str_replace('{rac.cartlink}', $url_to_click, $message);
            $message = str_replace('{rac.date}', $date, $message);
            $message = str_replace('{rac.time}', $time, $message);
            $message = str_replace('{rac.firstname}', $firstname, $message);
            $message = str_replace('{rac.lastname}', $lastname, $message);
            $message = str_replace('{rac.Productinfo}', $tablecheckproduct, $message);
            if (strpos($message, "{rac.coupon}")) {
                if ($find_user == 'member') {
                    $coupon_code = FPRacCoupon::rac_create_coupon($user->user_email, $each_cart->cart_abandon_time);
                } elseif ($find_user == 'guest1' || $find_user == 'old_order') {
                    $coupon_code = FPRacCoupon::rac_create_coupon($order_object->billing_email, $each_cart->cart_abandon_time);
                } elseif ($find_user == 'guest2') {
                    $coupon_code = FPRacCoupon::rac_create_coupon($order_object['visitor_mail'], $each_cart->cart_abandon_time);
                }
                update_option('abandon_time_of' . $each_cart->id, $coupon_code);
                $message = str_replace('{rac.coupon}', $coupon_code, $message); //replacing shortcode with coupon code
            }
            $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
            add_filter('woocommerce_email_footer_text', array('RecoverAbandonCart', 'rac_footer_email_customization'));
            $message = do_shortcode($message); //shortcode feature

            $html_template = $emails->mail; // mail send plain or html

            if ($emails->link == '') {
                $logo = '';
            } else {
                $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;"  src="' . esc_url($emails->link) . '" /></p></td></tr></table>'; // mail uploaded
            }

            $woo_temp_msg = self::email_woocommerce_html($html_template, $subject, $message, $logo); // mail send plain or html
            $compact = array($emails->sender_opt, $emails->from_name, $emails->from_email);
            $headers = self::rac_format_email_headers($compact, $emails->rac_blind_carbon_copy);
            if ($each_cart->sending_status == 'SEND') {//condition to check start/stop mail sending
                if ('wp_mail' == get_option('rac_trouble_mail')) {
                    if (self::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $html_template, $compact)) {
                        $sent_mail_templates[] = $emails->id;
                        $store_sending_time[$emails->id] = $current_time;
                        $serialize_sending_time = maybe_serialize($store_sending_time);
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id, 'mail_template_sending_time' => $serialize_sending_time), array('id' => $each_cart->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $each_cart->id, "template_used" => $emails->id));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($emails->id);
                    }
                } else {
                    if (self::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact)) {
                        $sent_mail_templates[] = $emails->id;
                        $store_sending_time[$emails->id] = $current_time;
                        $serialize_sending_time = maybe_serialize($store_sending_time);
                        $store_template_id = maybe_serialize($sent_mail_templates);
                        $wpdb->update($abandancart_table_name, array('mail_template_id' => $store_template_id, 'mail_template_sending_time' => $serialize_sending_time), array('id' => $each_cart->id));
                        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
                        $wpdb->insert($table_name_logs, array("email_id" => $to, "date_time" => $current_time, "rac_cart_id" => $each_cart->id, "template_used" => $emails->id));
                        FPRacCounter::rac_do_mail_count();
                        FPRacCounter::email_count_by_template($emails->id);
                    }
                }
            }
        }
    }

    public static function rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $html_template, $compact) {
        global $woocommerce;
        $getdesiredoption = get_option('custom_exclude');
        if ($getdesiredoption == 'user_role') {
            $userrolenamemailget = get_option('custom_user_role');
            $getuserby = get_user_by('email', $to);
            if ($getuserby) {
                $newto = $getuserby->roles[0];
            } else {
                $newto = $to;
            }
        } elseif ($getdesiredoption == 'name') {
            $userrolenamemailget = get_option('custom_user_name_select');
            $getuserby = get_user_by('email', $to);
            if ($getuserby) {
                $newto = $getuserby->ID;
            } else {
                $newto = $to;
            }
        } else {
            $userrolenamemailget = get_option('custom_mailid_edit');
            $userrolenamemailget = explode("\r\n", $userrolenamemailget);
            $newto = $to;
        }

        $check_member_guest = RecoverAbandonCart::check_is_member_or_guest($to);
        $proceed = '1';

        if ($check_member_guest) {
            // for member
            $userid = RecoverAbandonCart::rac_return_user_id($to);
            $status = get_user_meta($userid, 'fp_rac_mail_unsubscribed', true);

            if ($status != 'yes') {
                $proceed = '1';
            } else {
                $proceed = '2';
            }
        } else {
            // for guest
            $needle = $to;
            if (!in_array($needle, (array) get_option('fp_rac_mail_unsubscribed'))) {
                $proceed = '1';
            } else {
                $proceed = '2';
            }
        }

        if ($proceed == '1') {
            if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                if (!in_array($newto, (array) $userrolenamemailget)) {

                    if (get_option('rac_webmaster_mail') == 'webmaster1') {
                        return wp_mail($to, $subject, $woo_temp_msg, $headers, '-f ' . get_option('rac_textarea_mail'));
                    } else {
                        return wp_mail($to, $subject, $woo_temp_msg, $headers);
                    }
                }
            } else {

                if (!in_array($newto, (array) $userrolenamemailget)) {
                    if ($html_template == 'HTML') {
                        FP_RAC_Send_Email_Woocommerce_Mailer::send_email_via_woocommerce_mailer($to, $subject, $woo_temp_msg, $headers, $compact);
                        return "1";
                    } else {
                        wp_mail($to, $subject, $woo_temp_msg, $headers);
                        return "1";
                    }
                }
            }
        }
    }

    public static function rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact) {
        global $woocommerce;
        $getdesiredoption = get_option('custom_exclude');
        if ($getdesiredoption == 'user_role') {
            $userrolenamemailget = get_option('custom_user_role');
            $getuserby = get_user_by('email', $to);
            if ($getuserby) {
                $newto = $getuserby->roles[0];
            } else {
                $newto = $to;
            }
        } elseif ($getdesiredoption == 'name') {
            $userrolenamemailget = get_option('custom_user_name_select');
            $getuserby = get_user_by('email', $to);
            if ($getuserby) {
                $newto = $getuserby->ID;
            } else {
                $newto = $to;
            }
        } else {
            $userrolenamemailget = get_option('custom_mailid_edit');
            $userrolenamemailget = explode("\r\n", $userrolenamemailget);
            $newto = $to;
        }



        $check_member_guest = RecoverAbandonCart::check_is_member_or_guest($to);
        $proceed = '1';
        if ($check_member_guest) {
            // for member
            $userid = RecoverAbandonCart::rac_return_user_id($to);
            $status = get_user_meta($userid, 'fp_rac_mail_unsubscribed', true);
            if ($status != 'yes') {
                $proceed = '1';
            } else {
                $proceed = '2';
            }
        } else {
            // for guest
            $needle = $to;
            if (!in_array($needle, (array) get_option('fp_rac_mail_unsubscribed'))) {
                $proceed = '1';
            } else {
                $proceed = '2';
            }
        }
        if ($proceed == '1') {
            if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                if (!in_array($newto, (array) $userrolenamemailget)) {

                    if (get_option('rac_webmaster_mail') == 'webmaster1') {
                        return mail($to, $subject, $woo_temp_msg, $headers, '-f ' . get_option('rac_textarea_mail'));
                    } else {
                        return mail($to, $subject, $woo_temp_msg, $headers);
                    }
                }
            } else {
                if (!in_array($newto, (array) $userrolenamemailget)) {
                    FP_RAC_Send_Email_Woocommerce_Mailer::send_email_via_woocommerce_mailer($to, $subject, $woo_temp_msg, $headers, $compact);
                    return "1";
                }
            }
        }
    }

    // For Test Mail Function
    public static function rac_send_wp_mail_test($to, $subject, $woo_temp_msg, $headers, $compact) {
        global $woocommerce;
        if ((float) $woocommerce->version <= (float) ('2.2.0')) {
            if (get_option('rac_webmaster_mail') == 'webmaster1') {
                return wp_mail($to, $subject, $woo_temp_msg, $headers, '-f ' . get_option('rac_textarea_mail'));
            } else {
                return wp_mail($to, $subject, $woo_temp_msg, $headers);
            }
        } else {
            $mailer = WC()->mailer();
            $mailer->send($to, $subject, $woo_temp_msg, $headers, '');
            return "1";
        }
    }

    public static function rac_send_mail_test($to, $subject, $woo_temp_msg, $headers, $compact) {
        global $woocommerce;
        if ((float) $woocommerce->version <= (float) ('2.2.0')) {
            if (get_option('rac_webmaster_mail') == 'webmaster1') {
                return mail($to, $subject, $woo_temp_msg, $headers, '-f ' . get_option('rac_textarea_mail'));
            } else {
                return mail($to, $subject, $woo_temp_msg, $headers);
            }
        } else {
            $mailer = WC()->mailer();
            $mailer->send($to, $subject, $woo_temp_msg, $headers, '');
            return "1";
        }
    }

    public static function rac_formatted_from_address_local($fromname, $fromemail) {
        if (get_option('rac_webmaster_mail') == 'webmaster1') {
            return "From: " . $fromname . " <" . $fromemail . ">" . "-f " . get_option('rac_textarea_mail') . "\r\n";
        } else {
            return "From: " . $fromname . " <" . $fromemail . ">\r\n";
        }
    }

    public static function rac_formatted_from_address_woocommerce() {
        if (get_option('rac_webmaster_mail') == 'webmaster1') {
            return "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">" . "-f " . get_option('rac_textarea_mail') . "\r\n";
        } else {
            return "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
        }
    }

    public static function enable_border() {
        $enable_border = get_option('rac_enable_border_for_productinfo_in_email');
        if ($enable_border != 'no') {
            return 'td';
        } else {
            return '';
        }
    }

    public static function sending_duration($emails) {
        if ($emails->sending_type == 'hours') {
            $duration = $emails->sending_duration * 3600;
        } else if ($emails->sending_type == 'minutes') {
            $duration = $emails->sending_duration * 60;
        } else if ($emails->sending_type == 'days') {
            $duration = $emails->sending_duration * 86400;
        }//duration is finished
        return $duration;
    }

    // format email header
    public static function rac_format_email_headers($compact, $bcc = false) {
        $sender_opt = $compact[0];
        $from_name = $compact[1];
        $from_email = $compact[2];
        //header MIME version
        if (get_option('rac_mime_mail_header_ts') != 'none') {//check for to aviod header duplication
            $headers = "MIME-Version: 1.0\r\n";
        }
        //header charset
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        //header from and Reply-to
        if ($sender_opt == 'local') {
            $headers .= self::rac_formatted_from_address_local($from_name, $from_email);
            if (get_option('rac_replyto_mail_header_ts') != 'none') {//check for to aviod header duplication
                $headers .= "Reply-To: " . $from_name . " <" . $from_email . ">\r\n";
            }
        } else {
            $headers .= self::rac_formatted_from_address_woocommerce();
            if (get_option('rac_replyto_mail_header_ts') != 'none') {//check for to aviod header duplication
                $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . "<" . get_option('woocommerce_email_from_address') . ">\r\n";
            }
        }
        //header BCC.
        if ($bcc) {
            $headers .= "Bcc: " . $bcc . "\r\n";
        }
        return $headers;
    }

}

function decode_labels_for_non_english_sites($label, $name, $product = null) {
    return rawurldecode($label);
}

add_filter('woocommerce_attribute_label', 'decode_labels_for_non_english_sites', 10, 2);
