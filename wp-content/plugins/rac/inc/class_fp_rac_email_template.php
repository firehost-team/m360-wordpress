<?php

class FP_RAC_Email_Template {

    public static function fp_rac_create_new_email_template() {
        global $wp_roles, $woocommerce;
        $editor_id = "rac_email_template_new";
        $settings = array('textarea_name' => 'rac_email_template_new');
        $admin_url = admin_url('admin.php');
        $template_list_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail'), $admin_url));
        foreach ($wp_roles->role_names as $key => $value) {
            $userrole[] = $key;
            $username[] = $value;
        }
        $user_role = array_combine((array) $userrole, (array) $username);
        $guest_role = array('rac_guest' => __('Guest', 'woocommerce'));
        $seg_user_role = array_merge($user_role, $guest_role);
        $option = '';
        $content = "Hi {rac.firstname},<br><br>We noticed you have added the following Products in your Cart, but haven't completed the purchase. {rac.Productinfo}<br><br>We have captured the Cart for your convenience. Please use the following link to complete the purchase {rac.cartlink}<br><br>Thanks.";
        echo '<table class="widefat"><tr><td>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.cartlink} to insert the Cart Link in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.date} to insert the Abandoned Cart Date in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.time} to insert the Abandoned Cart Time in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.firstname} to insert Reciever First Name in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.lastname} to insert Receiver Last Name in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.Productinfo} to insert Product Information in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.coupon} to insert Coupon Code in the mail</strong></span></td></tr><tr><td>';
        echo '<tr><td>' . __('Template Name', 'recoverabandoncart') . ': </td><td><input type="text" name="rac_template_name" id="rac_template_name"></td></tr>';
        echo '<tr><td>' . __('Template Status', 'recoverabandoncart') . ':</td><td> <select name="rac_template_status" id="rac_template_status">
                                <option value="ACTIVE">' . __("Activated", "recoverabandoncart") . '</option>
                                <option value="NOTACTIVE">' . __("Deactivated", "recoverabandoncart") . '</option>
                                </select></td></tr>';
        echo '<tr><td>' . __('Segmentation', 'recoverabandoncart') . ':</td><td><select name="rac_template_seg_type" id="rac_template_seg_type">
                                <option value="rac_template_seg_odrer_count">' . __("Order Total", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_odrer_amount">' . __("Order Amount", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_total">' . __("Cart Total", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_date">' . __("Cart Abandon Date", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_quantity">' . __("Cart quantity", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_user_role">' . __("User Role", "recoverabandoncart") . '</option>    
                                <option value="rac_template_seg_cart_product">' . __("Product in cart", "recoverabandoncart") . '</option>
                                </select></td></tr>';
        foreach ($seg_user_role as $key => $value) {
            $option.= "<option value=" . $key . ">" . $value . "</option>";
        }
        echo '<tr class="rac_colsh rac_template_seg_odrer_count"><td></td><td><label>' . __('Minimum Order Total  : ', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_count_min" id="rac_template_seg_odrer_count_min" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_count"><td></td><td><label>' . __('Maximum Order Total  : ', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_count_max" id="rac_template_seg_odrer_count_max" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_amount"><td></td><td><label>' . __('Minimum Order Amount :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_amount_min" id="rac_template_seg_odrer_amount_min" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_amount"><td></td><td><label>' . __('Maximum Order Amount :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_amount_max" id="rac_template_seg_odrer_amount_max" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_total"><td></td><td><label>' . __('Minimum Cart Amount :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_total_min" id="rac_template_seg_cart_total_min" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_total"><td></td><td><label>' . __('Maximum Cart Amount :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_total_max" id="rac_template_seg_cart_total_max" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_date"><td></td><td><label>' . __('From Abadon Date :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_from_date" id="rac_template_seg_cart_from_date" value="*"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_date"><td></td><td><label>' . __('To Abadon Date :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_to_date" id="rac_template_seg_cart_to_date" value="*"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_quantity"><td></td><td><label>' . __('Minimum Cart Quantity :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_quantity_min" id="rac_template_seg_cart_quantity_min" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_quantity"><td></td><td><label>' . __('Maximum Cart Quantity :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_quantity_max" id="rac_template_seg_cart_quantity_max" class="fp_text_min_max" value="*" data-min="1"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_user_role"><td></td><td><label>' . __('User Role :', 'recoverabandoncart') . '<label><select multiple="multiple" name="rac_template_seg_selected_user_role[]" id="rac_template_seg_selected_user_role" placeholder="' . __('Serach a User Role', 'recoverabandoncart') . '" style="width:250px;">' . $option . '</select></td></tr>';
        echo fp_rac_common_function_for_search_products('rac_template_seg_selected_product_in_cart');
        // mail plain or html
        echo '<tr><td>' . __('Email Template Type', 'recoverabandoncart') . ':</td><td><select name="rac_template_mail" class="rac_template_mail">
                                 <option value="HTML">' . __("Woocommerce Template", "recoverabandoncart") . '</option>
                                 <option value="PLAIN">' . __("HTML Template", "recoverabandoncart") . '</option>
                                 </select></td></tr>';
        // mail plain or html
        // mail logo upload
        echo '<tr class="rac_logo_link"><td>' . __('Header Image For HTML Template', 'recoverabandoncart') . ':</td><td><input type="text" size="40" name="rac_logo_mail" id="rac_logo_mail"><input class="upload_button" id="image_uploader" type="submit" value="' . __("Media Uploader.") . '" /></td></tr>';
        // mail logo upload
        echo '<tr class = "rac_email_sender"><td>' . __('Email Sender Option', 'recoverabandoncart') . ': </td><td><input type="radio" name="rac_sender_opt" id="rac_sender_woo" value="woo" class="rac_sender_opt">' . __("woocommerce", "recoverabandoncart") . ' <input type="radio" name="rac_sender_opt" id="rac_sender_local" value="local" class="rac_sender_opt">' . __("local", "recoverabandoncart") . '</td></tr>';
        echo '<tr class="rac_local_senders"><td>' . __('From Name', 'recoverabandoncart') . ': </td><td><input type="text" name="rac_from_name"  id="rac_from_name"></td></tr>';
        echo '<tr class="rac_local_senders"><td>' . __('From Email', 'recoverabandoncart') . ': </td><td><input type="text" name="rac_from_email"  id="rac_from_email"></td></tr>';

        echo '<tr><td>' . __('Bcc', 'recoverabandoncart') . ':</td><td> <input type="textarea" name="rac_blind_carbon_copy" id="rac_blind_carbon_copy"></td></tr>';

        echo '<tr><td>' . __('Subject', 'recoverabandoncart') . ':</td><td> <input type="text" name="rac_subject" id="rac_subject"></td></tr>';
        echo '<tr><td>' . __('Duration to Send Mail After Abandoned Cart', 'recoverabandoncart') . ':<select name="rac_duration_type" id="rac_duration_type">
                                <option value="minutes">' . __("Minutes", "recoverabandoncart") . '</option>
                                <option value="hours">' . __("Hours", "recoverabandoncart") . '</option>
                                <option value="days">' . __("Days", "recoverabandoncart") . '</option>
                                </select></td>';
        echo '<td><span><input type="text" name="rac_mail_duration" id="rac_duration"></span></td></tr>';
        echo '<tr><td>' . __('Cart Link Anchor Text', 'recoverabandoncart') . ': </td><td><input type="text" name="rac_anchor_text" value="Cart Link" id="rac_anchor_text"></td></tr>';
        echo '<tr><td> ' . __('Message', 'recoverabandoncart') . ':</td>';
        echo '<td>';
        wp_editor($content, $editor_id, $settings);
        echo '</td></tr>';
        echo '<tr><td><input type="button" name="rac_save_new_template" class="button button-primary button-large" id="rac_save_new_template" value="' . __("Save", "recoverabandoncart") . '">&nbsp;';
        echo '<a href="' . $template_list_url . '"><input type="button" class="button" name="returntolist" value="' . __("Return to Mail Templates", "recoverabandoncart") . '"></a>&nbsp;';
        echo '</td></tr>';
        echo '</table>';
        ?>
        <script>
            function get_tinymce_content() {
                if (jQuery("#wp-rac_email_template_new-wrap").hasClass("tmce-active")) {
                    //rac_email_template_new
                    return tinyMCE.get('rac_email_template_new').getContent();
                } else {
                    return jQuery("#rac_email_template_new").val();
                }
            }
            function fp_validate_text_field_msg(min_total, max_total, min_element, max_element) {
                if (min_total == "" && max_total == "") {
                    var error = "Please Enter any Value";
                    jQuery(document.body).triggerHandler("fp_common_error_tip", [min_element, error]);
                    jQuery(document.body).triggerHandler("fp_common_error_tip", [max_element, error]);
                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                    window.location.hash = "#rac_template_seg_type";
                    return false;
                }
                if (min_total == "") {
                    var error = "Please Enter any Value";
                    jQuery(document.body).triggerHandler("fp_common_error_tip", [min_element, error]);
                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                    window.location.hash = "#rac_template_seg_type";
                    return false;
                }
                if (max_total == "") {
                    var error = "Please Enter any Value";
                    jQuery(document.body).triggerHandler("fp_common_error_tip", [max_element, error]);
                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                    window.location.hash = "#rac_template_seg_type";
                    return false;
                }
                return true;
            }
            jQuery(document).ready(function () {
                jQuery("#rac_template_name").val("Default");
                jQuery("#rac_from_name").val("Admin");
                jQuery("#rac_sender_woo").attr("checked", "checked");
                jQuery(".rac_sender_opt").change(function () {
                    if (jQuery("#rac_sender_woo").is(":checked")) {
                        jQuery(".rac_local_senders").css("display", "none");
                    } else {
                        jQuery(".rac_local_senders").css("display", "table-row");
                    }
                });
                jQuery("#rac_subject").val("Recover Abandon Cart");
                jQuery("#rac_from_email").val("<?php echo get_option('admin_email'); ?>");
                jQuery("#rac_duration_type").val("days");
                jQuery("#rac_template_status").val("ACTIVE");
                jQuery(".rac_template_mail").val("HTML");
                jQuery("#rac_logo_mail").val("<?php echo $admin_url; ?>");
                jQuery("#rac_duration").val("1");
                jQuery("#rac_template_seg_type").val("rac_template_seg_odrer_count");
                var content = "Hi {rac.firstname},<br><br>We noticed you have added the following Products in your Cart, but haven't completed the purchase. {rac.Productinfo}<br><br>We have captured the Cart for your convenience. Please use the following link to complete the purchase {rac.cartlink}<br><br>Thanks.";
                jQuery("#rac_email_template_new").val(content);
                jQuery("#rac_duration_type").change(function () {
                    jQuery("span#rac_duration").html(jQuery("#rac_duration_type").val());
                });
                jQuery("#rac_save_new_template").click(function () {
                    var rac_template_name = jQuery("#rac_template_name").val();
                    var rac_template_status = jQuery("#rac_template_status").val();
                    var rac_sender_option = jQuery("input:radio[name=rac_sender_opt]:checked").val();
                    var rac_from_name = jQuery("#rac_from_name").val();
                    var rac_from_email = jQuery("#rac_from_email").val();
                    var rac_blind_carbon_copy = jQuery("#rac_blind_carbon_copy").val();
                    var rac_subject = jQuery("#rac_subject").val();
                    var rac_anchor_text = jQuery("#rac_anchor_text").val();
                    var rac_message = get_tinymce_content();
                    var rac_duration_type = jQuery("#rac_duration_type").val();
                    var rac_mail_duration = jQuery("span #rac_duration").val();
                    var rac_template_mail = jQuery(".rac_template_mail").val(); // mail plain or html
                    var rac_logo_mail = jQuery("#rac_logo_mail").val(); // mail logo upload

                    console.log(jQuery("#rac_email_template_new").val());
                    var rac_template_seg_type = jQuery("#rac_template_seg_type").val();
                    if (rac_template_seg_type == "rac_template_seg_odrer_count") {
                        var rac_seg_min_order_total = jQuery("#rac_template_seg_odrer_count_min").val();
                        var rac_seg_max_order_total = jQuery("#rac_template_seg_odrer_count_max").val();
                        var validation_msg = fp_validate_text_field_msg(rac_seg_min_order_total, rac_seg_max_order_total, jQuery("#rac_template_seg_odrer_count_min"), jQuery("#rac_template_seg_odrer_count_max"));
                        if (!(validation_msg)) {
                            return false;
                        }
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_odrer_count_min": rac_seg_min_order_total, "rac_template_seg_odrer_count_max": rac_seg_max_order_total};
                    } else if (rac_template_seg_type == "rac_template_seg_odrer_amount") {
                        var rac_seg_min_order_amount = jQuery("#rac_template_seg_odrer_amount_min").val();
                        var rac_seg_min_order_amount = jQuery("#rac_template_seg_odrer_amount_max").val();
                        var validation_msg = fp_validate_text_field_msg(rac_seg_min_order_amount, rac_seg_min_order_amount, jQuery("#rac_template_seg_odrer_amount_min"), jQuery("#rac_template_seg_odrer_amount_max"));
                        if (!(validation_msg)) {
                            return false;
                        }
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_odrer_amount_min": rac_seg_min_order_amount, "rac_template_seg_odrer_amount_max": rac_seg_min_order_amount};
                    } else if (rac_template_seg_type == "rac_template_seg_cart_total") {
                        var rac_seg_min_cart_amount = jQuery("#rac_template_seg_cart_total_min").val();
                        var rac_seg_max_cart_amount = jQuery("#rac_template_seg_cart_total_max").val();
                        var validation_msg = fp_validate_text_field_msg(rac_seg_min_cart_amount, rac_seg_max_cart_amount, jQuery("#rac_template_seg_cart_total_min"), jQuery("#rac_template_seg_cart_total_max"));
                        if (!(validation_msg)) {
                            return false;
                        }
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_total_min": rac_seg_min_cart_amount, "rac_template_seg_cart_total_max": rac_seg_max_cart_amount};
                    } else if (rac_template_seg_type == "rac_template_seg_cart_date") {
                        var rac_seg_from_cart_date = jQuery("#rac_template_seg_cart_from_date").val();
                        var rac_seg_to_cart_date = jQuery("#rac_template_seg_cart_to_date").val();
                        var validation_msg = fp_validate_text_field_msg(rac_seg_from_cart_date, rac_seg_to_cart_date, jQuery("#rac_template_seg_cart_from_date"), jQuery("#rac_template_seg_cart_to_date"));
                        if (!(validation_msg)) {
                            return false;
                        }
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_from_date": rac_seg_from_cart_date, "rac_template_seg_cart_to_date": rac_seg_to_cart_date};
                    } else if (rac_template_seg_type == "rac_template_seg_cart_quantity") {
                        var rac_seg_min_cart_quantity = jQuery("#rac_template_seg_cart_quantity_min").val();
                        var rac_seg_max_cart_quantity = jQuery("#rac_template_seg_cart_quantity_max").val();
                        var validation_msg = fp_validate_text_field_msg(rac_seg_min_cart_quantity, rac_seg_max_cart_quantity, jQuery("#rac_template_seg_cart_quantity_min"), jQuery("#rac_template_seg_cart_quantity_max"));
                        if (!(validation_msg)) {
                            return false;
                        }
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_quantity_min": rac_seg_min_cart_quantity, "rac_template_seg_cart_quantity_max": rac_seg_max_cart_quantity};
                    } else if (rac_template_seg_type == "rac_template_seg_user_role") {
                        var rac_seg_user_role = jQuery("#rac_template_seg_selected_user_role").val();
                        var rac_segmentation_array = {"rac_template_seg_user_role": rac_template_seg_type, "rac_template_seg_selected_user_role": rac_seg_user_role};
                    } else {
                        var rac_seg_products = jQuery("#rac_template_seg_selected_product_in_cart").val();
                        var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_selected_product_in_cart": rac_seg_products};
                    }
                    jQuery(this).prop("disabled", true);
                    var data = {
                        action: "rac_new_template",
                        rac_sender_option: rac_sender_option,
                        rac_template_name: rac_template_name,
                        rac_template_status: rac_template_status,
                        rac_from_name: rac_from_name,
                        rac_from_email: rac_from_email,
                        rac_blind_carbon_copy: rac_blind_carbon_copy,
                        rac_subject: rac_subject,
                        rac_anchor_text: rac_anchor_text,
                        rac_message: rac_message,
                        rac_duration_type: rac_duration_type,
                        rac_mail_duration: rac_mail_duration,
                        rac_template_segmentation: rac_segmentation_array,
                        rac_template_mail: rac_template_mail, // mail plain or html
                        rac_logo_mail: rac_logo_mail  // mail logo upload
                    };
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl,
                        data: data
                    }).done(function (response) {
                        jQuery("#rac_save_new_template").prop("disabled", false);
                        window.location.replace("<?php echo $template_list_url; ?>");
                    });
                    console.log(data);
                });
                // mail logo upload
                var uploader_open;
                jQuery('.upload_button').click(function (e) {
                    e.preventDefault();
                    if (uploader_open) {
                        uploader_open.open();
                        return;
                    }

                    uploader_open = wp.media.frames.uploader_open = wp.media({
                        title: 'Media Uploader',
                        button: {
                            text: 'Media Uploader'
                        },
                        multiple: false
                    });
                    //When a file is selected, grab the URL and set it as the text field's value
                    uploader_open.on('select', function () {
                        attachment = uploader_open.state().get('selection').first().toJSON();
                        jQuery('#rac_logo_mail').val(attachment.url);
                    });
                    uploader_open.open();
                });
                //mail logo upload
            });</script>
        <style>
            .rac_local_senders{
                display:none;
            }
            #image_uploader {
                color: blueviolet;
            }
            //code for multi select choice diaplay problem in table 
            <?php if ((float) $woocommerce->version > (float) ('3.0')) { ?>
                .widefat td, .widefat td ul{
                    overflow: visible !important;
                }
                .chosen-container{
                    border:1px solid #ddd;
                }
            <?php } ?>
        </style>
        <?php
    }

    public static function fp_rac_edit_email_template() {
        global $wpdb, $wp_roles, $woocommerce;
        $template_id = $_GET['rac_edit_email'];
        $table_name = $wpdb->prefix . 'rac_templates_email';
        $edit_templates = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$template_id", OBJECT);
        $edit_templates = $edit_templates[0]; // get array 0 value mutidimensional method
        $admin_url = admin_url('admin.php');
        $template_list_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail'), $admin_url));
        $editor_id = "rac_email_template_edit";
        $content = $edit_templates->message;
        $settings = array('textarea_name' => 'rac_email_template_edit');
        $template_seg_order_count = '';
        $template_seg_order_amount = '';
        $template_seg_cart_total = '';
        $template_seg_cart_date = '';
        $template_seg_cart_quantity = '';
        $template_seg_user_role = '';
        $template_seg_product_cart = '';
        ?>
        <style type="text/css">
            table #rac_template_seg_typ {
                border: 10px;
            }
        </style>
        <?php
        foreach ($wp_roles->role_names as $key => $value) {
            $userrole[] = $key;
            $username[] = $value;
        }
        $user_role = array_combine((array) $userrole, (array) $username);
        $guest_role = array('rac_guest' => __('Guest', 'woocommerce'));
        $seg_user_role = array_merge($user_role, $guest_role);
        $option = '';
        echo '<table class="widefat"><tr><td>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.cartlink} to insert the Cart Link in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.date} to insert the Abandoned Cart Date in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.time} to insert the Abandoned Cart Time in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.firstname} to insert Reciever First Name in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.lastname} to insert Reciever Last Name in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.Productinfo} to insert Product Information in the mail</strong></span></td></tr>';
        echo '<tr><td colspan="2"><span><strong>Use {rac.coupon} to insert Coupon Code in the mail</strong></span></td></tr><tr><td>';
        _e('Template Name', 'recoverabandoncart') . ':</td>';
        echo '<td><input type="text" name="rac_template_name" id="rac_template_name" value="' . $edit_templates->template_name . '"></td></tr>';
        $template_active = selected($edit_templates->status, 'ACTIVE', false);
        $template_not_active = selected($edit_templates->status, 'NOTACTIVE', false);
        echo '<tr><td>' . __('Template Status', 'recoverabandoncart') . ':</td><td> <select name="rac_template_status" id="rac_template_status">
                                <option value="ACTIVE" ' . $template_active . '>' . __("Activated", "recoverabandoncart") . '</option>
                                <option value="NOTACTIVE" ' . $template_not_active . '>' . __("Deactivated", "recoverabandoncart") . '</option>
                                </select></td></tr>';
        $segmentation_details = maybe_unserialize($edit_templates->segmentation);
        if (isset($segmentation_details['rac_template_seg_type'])) {
            $template_seg_order_count = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_odrer_count', false);
            $template_seg_order_amount = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_odrer_amount', false);
            $template_seg_cart_total = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_cart_total', false);
            $template_seg_cart_date = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_cart_date', false);
            $template_seg_cart_quantity = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_cart_quantity', false);
            $template_seg_user_role = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_user_role', false);
            $template_seg_product_cart = selected($segmentation_details['rac_template_seg_type'], 'rac_template_seg_cart_product', false);
        }
        echo '<tr><td>' . __('Segmentation', 'recoverabandoncart') . ':</td><td><select name="rac_template_seg_type" id="rac_template_seg_type">
                                <option value="rac_template_seg_odrer_count" ' . $template_seg_order_count . '>' . __("Order Total", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_odrer_amount" ' . $template_seg_order_amount . '>' . __("Order Amount", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_total" ' . $template_seg_cart_total . '>' . __("Cart Total", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_date" ' . $template_seg_cart_date . '>' . __("Cart Abandon Date", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_cart_quantity" ' . $template_seg_cart_quantity . '>' . __("Cart quantity", "recoverabandoncart") . '</option>
                                <option value="rac_template_seg_user_role" ' . $template_seg_user_role . '>' . __("User Role", "recoverabandoncart") . '</option>    
                                <option value="rac_template_seg_cart_product" ' . $template_seg_product_cart . '>' . __("Product in cart", "recoverabandoncart") . '</option>
                                </select></td></tr>';
        $rac_template_seg_odrer_count_min = isset($segmentation_details['rac_template_seg_odrer_count_min']) ? $segmentation_details['rac_template_seg_odrer_count_min'] : '*';
        $rac_template_seg_odrer_count_max = isset($segmentation_details['rac_template_seg_odrer_count_max']) ? $segmentation_details['rac_template_seg_odrer_count_max'] : '*';
        $rac_template_seg_odrer_amount_min = isset($segmentation_details['rac_template_seg_odrer_amount_min']) ? $segmentation_details['rac_template_seg_odrer_amount_min'] : '*';
        $rac_template_seg_odrer_amount_max = isset($segmentation_details['rac_template_seg_odrer_amount_max']) ? $segmentation_details['rac_template_seg_odrer_amount_max'] : '*';
        $rac_template_seg_cart_total_min = isset($segmentation_details['rac_template_seg_cart_total_min']) ? $segmentation_details['rac_template_seg_cart_total_min'] : '*';
        $rac_template_seg_cart_total_max = isset($segmentation_details['rac_template_seg_cart_total_max']) ? $segmentation_details['rac_template_seg_cart_total_max'] : '*';
        $rac_template_seg_cart_from_date = isset($segmentation_details['rac_template_seg_cart_from_date']) ? $segmentation_details['rac_template_seg_cart_from_date'] : '*';
        $rac_template_seg_cart_to_date = isset($segmentation_details['rac_template_seg_cart_to_date']) ? $segmentation_details['rac_template_seg_cart_to_date'] : '*';
        $rac_template_seg_cart_quantity_min = isset($segmentation_details['rac_template_seg_cart_quantity_min']) ? $segmentation_details['rac_template_seg_cart_quantity_min'] : '*';
        $rac_template_seg_cart_quantity_max = isset($segmentation_details['rac_template_seg_cart_quantity_max']) ? $segmentation_details['rac_template_seg_cart_quantity_max'] : '*';
        foreach ($seg_user_role as $key => $value) {
            $selected_roles = isset($segmentation_details['rac_template_seg_selected_user_role']) ? $segmentation_details['rac_template_seg_selected_user_role'] : '';
            if (in_array($key, (array) $selected_roles)) {
                $selected = "selected=selected";
            } else {
                $selected = '';
            }
            $option.= "<option value=" . $key . " $selected>" . $value . "</option>";
        }
        echo '<tr class="rac_colsh rac_template_seg_odrer_count"><td></td><td><label>' . __('Minimum Order Total  : ', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_count_min" id="rac_template_seg_odrer_count_min" class="fp_text_min_max"  data-min="1" value="' . $rac_template_seg_odrer_count_min . '" ></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_count"><td></td><td><label>' . __('Maximum Order Total  : ', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_count_max" id="rac_template_seg_odrer_count_max" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_odrer_count_max . '" ></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_amount"><td></td><td><label>' . __('Minimum Order Amount  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_amount_min" id="rac_template_seg_odrer_amount_min" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_odrer_amount_min . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_odrer_amount"><td></td><td><label>' . __('Maximum Order Amount  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_odrer_amount_max" id="rac_template_seg_odrer_amount_max" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_odrer_amount_max . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_total"><td></td><td><label>' . __('Minimum Cart Amount  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_total_min" id="rac_template_seg_cart_total_min" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_cart_total_min . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_total"><td></td><td><label>' . __('Maximum Cart Amount  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_total_max" id="rac_template_seg_cart_total_max" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_cart_total_max . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_date"><td></td><td><label>' . __('From Abadon Date  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_from_date" id="rac_template_seg_cart_from_date" value="' . $rac_template_seg_cart_from_date . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_date"><td></td><td><label>' . __('To Abadon Date  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_to_date" id="rac_template_seg_cart_to_date" value="' . $rac_template_seg_cart_to_date . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_quantity"><td></td><td><label>' . __('Minimum Cart Quantity  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_quantity_min" id="rac_template_seg_cart_quantity_min" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_cart_quantity_min . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_cart_quantity"><td></td><td><label>' . __('Maximum Cart Quantity  :', 'recoverabandoncart') . '</label><input type="text" name="rac_template_seg_cart_quantity_max" id="rac_template_seg_cart_quantity_max" class="fp_text_min_max" data-min="1" value="' . $rac_template_seg_cart_quantity_max . '"></input></td></tr>';
        echo '<tr class="rac_colsh rac_template_seg_user_role"><td></td><td><label>' . __('User Role :', 'recoverabandoncart') . '</label><p><select multiple="multiple" name="rac_template_seg_selected_user_role[]" id="rac_template_seg_selected_user_role" placeholder="' . __('Serach a User Role', 'recoverabandoncart') . '" style="width:250px;">' . $option . '</select></p></td></tr>';
        $selected_products = isset($segmentation_details['rac_template_seg_selected_product_in_cart']) ? $segmentation_details['rac_template_seg_selected_product_in_cart'] : '';
        update_option('rac_template_seg_selected_product_in_cart', $selected_products);
        echo fp_rac_common_function_for_search_products('rac_template_seg_selected_product_in_cart');
        // mail plain or html
        $template_html = selected($edit_templates->mail, 'HTML', false);
        $template_plain = selected($edit_templates->mail, 'PLAIN', false);
        echo '<tr><td>' . __('Email Template Type', 'recoverabandoncart') . ':</td><td><select name="rac_template_mail" class="rac_template_mail">
                                 <option value="HTML" ' . $template_html . '>' . __("Woocommerce Template", "recoverabandoncart") . '</option>
                                 <option value="PLAIN" ' . $template_plain . '>' . __("HTML Template", "recoverabandoncart") . '</option>
                                  </select></td></tr>';
        // mail plain or html
        // mail logo upload
        echo '<tr class="rac_logo_link"><td>' . __('Header Image For HTML Template', 'recoverabandoncart') . ':</td><td><input type="text" size="40" name="rac_logo_mail" id="rac_logo_mail" value="' . $edit_templates->link . '"><input class="upload_button" id="image_uploader" type="submit" value="' . __("Media Uploader", "recoverabandoncart") . '" /></td></tr>';
        // mail logo upload
        $woo_selected = checked($edit_templates->sender_opt, 'woo', false);
        $local_selected = checked($edit_templates->sender_opt, 'local', false);
        echo '<tr class = "rac_email_sender"><td>' . __('Email Sender Option', 'recoverabandoncart') . ': </td><td><input type="radio" name="rac_sender_opt" id="rac_sender_woo" value="woo" ' . $woo_selected . ' class="rac_sender_opt">' . __("woocommerce", "recoverabandoncart") . '
                                <input type="radio" name="rac_sender_opt" id="rac_sender_local" value="local" ' . $local_selected . ' class="rac_sender_opt">' . __("local", "recoverabandoncart") . '</td></tr>';
        echo '<tr class="rac_local_senders"><td>' . __('From Name', 'recoverabandoncart') . ':</td>';
        echo '<td><input type="text" name="rac_from_name" id="rac_from_name" value="' . $edit_templates->from_name . '"></td></tr>';
        echo '<tr class="rac_local_senders"><td>' . __('From Email', 'recoverabandoncart') . ':</td>';
        echo '<td><input type="text" name="rac_from_email" id="rac_from_email" value="' . $edit_templates->from_email . '"></td></tr>';
        echo '<tr><td>' . __('Bcc', 'recoverabandoncart') . ':</td>';
        if (isset($edit_templates->rac_blind_carbon_copy)) {
            $bcc = $edit_templates->rac_blind_carbon_copy;
        } else {
            $bcc = '';
        }
        echo '<td><input type="textarea" name="rac_blind_carbon_copy" id="rac_blind_carbon_copy" value="' . $bcc . '"></td></tr>';
        echo '<tr><td>' . __('Subject', 'recoverabandoncart') . ':</td>';
        echo '<td><input type="text" name="rac_subject" id="rac_subject" value="' . $edit_templates->subject . '"></td></tr>';
        $duration_type = $edit_templates->sending_type;
        echo '<tr><td>' . __('Send Mail Duration', 'recoverabandoncart') . ':<select name="rac_duration_type" id="rac_duration_type">
                                <option value="minutes" ' . selected($duration_type, "minutes", false) . '>' . __("Minutes", "recoverabandoncart") . '</option>
                                <option value="hours" ' . selected($duration_type, "hours", false) . '>' . __("Hours", "recoverabandoncart") . '</option>
                                <option value="days" ' . selected($duration_type, "days", false) . '>' . __("Days", "recoverabandoncart") . '</option>
                                </select>';
        echo '</td><td><span><input type="text" name="rac_mail_duration" id="rac_duration" value="' . $edit_templates->sending_duration . '"></span></td></tr>';
        echo '<tr><td>' . __('Cart Link Anchor Text', 'recoverabandoncart') . ': </td><td><input type="text" name="rac_anchor_text" id="rac_anchor_text" value="' . $edit_templates->anchor_text . '"></td></tr>';
        echo '<tr><td> ' . __('Message', 'recoverabandoncart') . ':</td>';
        echo '<td>';
        wp_editor($content, $editor_id, $settings);
        echo '</td></tr>';
        echo '<tr><td>';
        echo '<input type="button" class="button button-primary button-large" name="rac_save_new_template" id="rac_save_new_template" value="' . __('Save Changes', 'recoverabandoncart') . '">&nbsp;';
        echo '<a href="' . $template_list_url . '"><input type="button" class="button" name="returntolist" value="' . __('Return to Mail Templates', 'recoverabandoncart') . '"></a>&nbsp;';
        echo '</td></tr>';
        echo '<tr><td>';
        echo '<div id="rac_mail_result" style="display:none"> ' . __("Settings Saved", "recoverabandoncart") . '</div>';
        echo '</td></tr>';

        echo '<tr><td>' . __("Test Email", "recoverabandoncart") . ' : ';
        echo '<input type="email" class=rac_send_test_email_for_this_template></td></tr>';
        echo '<tr><td><input type="button" class="button button-primary button-large" name="rac_send_template_preview" id="rac_send_template_preview" value="' . __('Send Test', 'recoverabandoncart') . '">&nbsp;';
        echo '<div id="rac_test_mail_sent" style="display:none"> ' . __("Mail Sent", "recoverabandoncart") . '</div>'
        . '<div class="rac_hide_this_message">' . __('Shortcodes replaced by Sample Data', 'recoverabandoncart') . '</div></td></tr>';

        echo '</table>';
        echo '<script>
                        function get_tinymce_content() {
                                    if (jQuery("#wp-rac_email_template_edit-wrap").hasClass("tmce-active")) {
                                        return tinyMCE.get("rac_email_template_edit").getContent();
                                    } else {
                                        return jQuery("#rac_email_template_edit").val();
                                    }
                                }
                        function fp_validate_text_field_msg(min_total, max_total, min_element, max_element) {
                                if (min_total == "" && max_total == "") {
                                    var error = "Please Enter any Value";
                                    jQuery(document.body).triggerHandler("fp_common_error_tip", [min_element, error]);
                                    jQuery(document.body).triggerHandler("fp_common_error_tip", [max_element, error]);
                                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                                    window.location.hash = "#rac_template_seg_type";
                                    return false;
                                }
                                if (min_total == "") {
                                    var error = "Please Enter any Value";
                                    jQuery(document.body).triggerHandler("fp_common_error_tip", [min_element, error]);
                                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                                    window.location.hash = "#rac_template_seg_type";
                                    return false;
                                }
                                if (max_total == "") {
                                    var error = "Please Enter any Value";
                                    jQuery(document.body).triggerHandler("fp_common_error_tip", [max_element, error]);
                                    jQuery("#rac_vlaidate_error_msg").css("display", "block");
                                    window.location.hash = "#rac_template_seg_type";
                                    return false;
                                }
                                return true;
                        }
                        jQuery(document).ready(function(){
                            jQuery("#rac_duration_type").change(function(){
                                     jQuery("span#rac_duration").html(jQuery("#rac_duration_type").val());
                                });
                                //normal ready event
                                if(jQuery("#rac_sender_woo").is(":checked")){
                                    jQuery(".rac_local_senders").css("display","none");
                                }else{
                                    jQuery(".rac_local_senders").css("display","table-row");
                                }

                            jQuery(".rac_sender_opt").change(function(){
                                if(jQuery("#rac_sender_woo").is(":checked")){
                                    jQuery(".rac_local_senders").css("display","none");
                                }else{
                                    jQuery(".rac_local_senders").css("display","table-row");
                                }
                                });
                            jQuery("#rac_save_new_template").click(function(){
                                var rac_template_name = jQuery("#rac_template_name").val();
                                var rac_template_status = jQuery("#rac_template_status").val();
                                var rac_sender_option = jQuery("input:radio[name=rac_sender_opt]:checked").val();
                                var rac_from_name = jQuery("#rac_from_name").val();
                                var rac_from_email = jQuery("#rac_from_email").val();
                                var rac_blind_carbon_copy = jQuery("#rac_blind_carbon_copy").val();
                                var rac_subject = jQuery("#rac_subject").val();
                                var rac_anchor_text = jQuery("#rac_anchor_text").val();
                                var rac_message = get_tinymce_content();
                                var rac_duration_type = jQuery("#rac_duration_type").val();
                                var rac_mail_duration = jQuery("span #rac_duration").val();
                                var rac_template_mail = jQuery(".rac_template_mail").val();  // mail plain or html
                                var rac_logo_mail = jQuery("#rac_logo_mail").val(); //  mail logo upload
                                var rac_template_id = ' . $template_id . '
                                console.log(jQuery("#rac_email_template_edit").val());
                                
                                var rac_template_seg_type = jQuery("#rac_template_seg_type").val();
                                if (rac_template_seg_type == "rac_template_seg_odrer_count") {
                                    var rac_seg_min_order_total = jQuery("#rac_template_seg_odrer_count_min").val();
                                    var rac_seg_max_order_total = jQuery("#rac_template_seg_odrer_count_max").val();
                                    var validation_msg=fp_validate_text_field_msg(rac_seg_min_order_total,rac_seg_max_order_total,jQuery("#rac_template_seg_odrer_count_min"),jQuery("#rac_template_seg_odrer_count_max"));
                                    if(!(validation_msg)){
                                    return false;
                                    }
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_odrer_count_min": rac_seg_min_order_total, "rac_template_seg_odrer_count_max": rac_seg_max_order_total};
                                } else if (rac_template_seg_type == "rac_template_seg_odrer_amount") {
                                    var rac_seg_min_order_amount = jQuery("#rac_template_seg_odrer_amount_min").val();
                                    var rac_seg_min_order_amount = jQuery("#rac_template_seg_odrer_amount_max").val();
                                    var validation_msg=fp_validate_text_field_msg(rac_seg_min_order_amount,rac_seg_min_order_amount,jQuery("#rac_template_seg_odrer_amount_min"),jQuery("#rac_template_seg_odrer_amount_max"));
                                    if(!(validation_msg)){
                                    return false;
                                    }
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_odrer_amount_min": rac_seg_min_order_amount, "rac_template_seg_odrer_amount_max": rac_seg_min_order_amount};
                                } else if (rac_template_seg_type == "rac_template_seg_cart_total") {
                                    var rac_seg_min_cart_amount = jQuery("#rac_template_seg_cart_total_min").val();
                                    var rac_seg_max_cart_amount = jQuery("#rac_template_seg_cart_total_max").val();
                                    var validation_msg=fp_validate_text_field_msg(rac_seg_min_cart_amount,rac_seg_max_cart_amount,jQuery("#rac_template_seg_cart_total_min"),jQuery("#rac_template_seg_cart_total_max"));
                                    if(!(validation_msg)){
                                    return false;
                                    }
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_total_min": rac_seg_min_cart_amount, "rac_template_seg_cart_total_max": rac_seg_max_cart_amount};
                                } else if (rac_template_seg_type == "rac_template_seg_cart_date") {
                                    var rac_seg_from_cart_date = jQuery("#rac_template_seg_cart_from_date").val();
                                    var rac_seg_to_cart_date = jQuery("#rac_template_seg_cart_to_date").val();
                                    var validation_msg=fp_validate_text_field_msg(rac_seg_from_cart_date,rac_seg_to_cart_date,jQuery("#rac_template_seg_cart_from_date"),jQuery("#rac_template_seg_cart_to_date"));
                                    if(!(validation_msg)){
                                    return false;
                                    }
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_from_date": rac_seg_from_cart_date, "rac_template_seg_cart_to_date": rac_seg_to_cart_date};
                                } else if (rac_template_seg_type == "rac_template_seg_cart_quantity") {
                                    var rac_seg_min_cart_quantity = jQuery("#rac_template_seg_cart_quantity_min").val();
                                    var rac_seg_max_cart_quantity = jQuery("#rac_template_seg_cart_quantity_max").val();
                                    var validation_msg=fp_validate_text_field_msg(rac_seg_min_cart_quantity,rac_seg_max_cart_quantity,jQuery("#rac_template_seg_cart_quantity_min"),jQuery("#rac_template_seg_cart_quantity_max"));
                                    if(!(validation_msg)){
                                    return false;
                                    }
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_cart_quantity_min": rac_seg_min_cart_quantity, "rac_template_seg_cart_quantity_max": rac_seg_max_cart_quantity};
                                } else if (rac_template_seg_type == "rac_template_seg_user_role") {
                                    var rac_seg_user_role = jQuery("#rac_template_seg_selected_user_role").val();
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_selected_user_role": rac_seg_user_role};
                                } else {
                                    var rac_seg_products = jQuery("#rac_template_seg_selected_product_in_cart").val();
                                    var rac_segmentation_array = {"rac_template_seg_type": rac_template_seg_type, "rac_template_seg_selected_product_in_cart": rac_seg_products};
                                }
                                jQuery(this).prop("disabled",true);
                                var data = {
                                action:"rac_edit_template",
                                rac_sender_option:rac_sender_option,
                                rac_template_name:rac_template_name,
                                rac_template_status:rac_template_status,
                                rac_from_name:rac_from_name,
                                rac_from_email:rac_from_email,
                                rac_blind_carbon_copy: rac_blind_carbon_copy,
                                rac_subject:rac_subject,
                                rac_anchor_text:rac_anchor_text,
                                rac_message:rac_message,
                                rac_duration_type:rac_duration_type,
                                rac_mail_duration:rac_mail_duration,
                                rac_template_segmentation:rac_segmentation_array,
                                rac_template_id:rac_template_id,
                                rac_template_mail:rac_template_mail, // mail plain or html
                                rac_logo_mail: rac_logo_mail  // mail logo upload
                                };

                                jQuery.ajax({
                                type:"POST",
                                url:ajaxurl,
                                data:data
                                }).done(function(response){
                                 jQuery("#rac_save_new_template").prop("disabled",false);
                                 jQuery("#rac_mail_result").css("display","block");
                                });
                                console.log(data);
                                });

                                jQuery("#rac_send_template_preview").click(function(){
                                    var rac_to_email = jQuery(".rac_send_test_email_for_this_template").val();
                                    var atpos=rac_to_email.indexOf("@");
                                    var dotpos=rac_to_email.lastIndexOf(".");
                                    if (rac_to_email !== "") {
                                        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=rac_to_email.length){
                                            alert("' . __("Please enter valid email id", "recoverabandoncart") . '");
                                            return false;
                                        }
                                    }
                                    else{
                                        alert("' . __("Please enter email id", "recoverabandoncart") . '");
                                        return false;
                                    }
                                    var rac_template_name = jQuery("#rac_template_name").val();
                                    var rac_template_status = jQuery("#rac_template_status").val();
                                    var rac_sender_option = jQuery("input:radio[name=rac_sender_opt]:checked").val();
                                    var rac_from_name = jQuery("#rac_from_name").val();
                                    var rac_from_email = jQuery("#rac_from_email").val();
                                    var rac_blind_carbon_copy = jQuery("#rac_blind_carbon_copy").val();
                                    var rac_subject = jQuery("#rac_subject").val();
                                    var rac_anchor_text = jQuery("#rac_anchor_text").val();
                                    var rac_message = get_tinymce_content();
                                    var rac_duration_type = jQuery("#rac_duration_type").val();
                                    var rac_mail_duration = jQuery("span #rac_duration").val();
                                    var rac_template_mail = jQuery(".rac_template_mail").val();  // mail plain or html
                                    var rac_logo_mail = jQuery("#rac_logo_mail").val(); //  mail logo upload
                                    var rac_template_id = ' . $template_id . '
                                    console.log(jQuery("#rac_email_template_edit").val());


                                    var data = {
                                    action:"rac_send_template_preview_email",
                                    rac_to_email : rac_to_email,
                                    rac_sender_option:rac_sender_option,
                                    rac_template_name:rac_template_name,
                                    rac_template_status:rac_template_status,
                                    rac_from_name:rac_from_name,
                                    rac_from_email:rac_from_email,
                                    rac_blind_carbon_copy: rac_blind_carbon_copy,
                                    rac_subject:rac_subject,
                                    rac_anchor_text:rac_anchor_text,
                                    rac_message:rac_message,
                                    rac_duration_type:rac_duration_type,
                                    rac_mail_duration:rac_mail_duration,
                                    rac_template_id:rac_template_id,
                                    rac_template_mail:rac_template_mail, // mail plain or html
                                    rac_logo_mail: rac_logo_mail  // mail logo upload
                                };

                                jQuery.ajax({type:"POST",url:ajaxurl,data:data}).done(function(response){

                                        jQuery("#rac_test_mail_sent").css("display","block");
                                        jQuery(".rac_hide_this_message").css("display","none");

                                });
                                console.log(data);
                                });


                                });</script>
                                ';
        ?><style>
            #image_uploader {
                color: blueviolet;
            }
            <?php if ((float) $woocommerce->version >= (float) ('3.0')) { ?>
                .widefat td, .widefat td ul{
                    overflow: visible !important;
                }
                .chosen-container{
                    border:1px solid #ddd;
                }
            <?php } ?>
        </style><?php
    }

    public static function fp_rac_send_email_template() {
        //email template lists
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_templates_email';
        $templates = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC", OBJECT);
        ?>
        <table class="widefat">
            <tr>
                <td><?php _e('Load Message from existing Template'); ?></td>
                <td><select id="rac_load_mail">
                        <?php
                        foreach ($templates as $key => $each_template) {
                            if ($key == 0) {
                                $template_name = $each_template->template_name . '( #' . $each_template->id . ')';
                                echo '<option value=' . $each_template->id . ' selected>' . $template_name . '</option>';
                            } else {
                                $template_name = $each_template->template_name . '( #' . $each_template->id . ')';
                                echo '<option value=' . $each_template->id . '>' . $template_name . '</option>';
                            }
                        }
                        if (isset($templates[0]->rac_blind_carbon_copy)) {
                            $bcc = $templates[0]->rac_blind_carbon_copy;
                        } else {
                            $bcc = '';
                        }
                        ?></select></td>
            </tr>
            <!--mail plain or html-->
            <tr>
                <td><?php _e('Email Template Type'); ?></td>
                <td><select name="rac_template_mail" class="rac_template_mail">
                        <option value="HTML"<?php selected('HTML', $templates[0]->mail); ?>><?php _e("Woocommerce Template", "recoverabandoncart"); ?></option>
                        <option value="PLAIN"<?php selected('PLAIN', $templates[0]->mail); ?>><?php _e("HTML Template", "recoverabandoncart"); ?></option>
                    </select></td>
            </tr>
            <!-- mail plain or html-->

            <!--   mail logo upload -->
            <tr class="rac_logo_link">
                <td><?php _e('Header Image For HTML Template', 'recoverabandoncart'); ?>:</td>
                <td><input type="text" size="40" name="rac_logo_mail" id="rac_logo_mail" value="<?php echo $templates[0]->link ?>"><input class="upload_button" id="image_uploader" type="submit" value="<?php _e("Media Uploader", "recoverabandoncart"); ?>" /></td>
            </tr>
            <!-- mail logo upload-->

            <tr class = "rac_email_sender">
                <td><?php _e('Email Sender Option', 'recoverabandoncart'); ?>: </td>
                <td>
                    <input type="radio" name="rac_sender_opt" id="rac_sender_woo" value="woo" <?php checked('woo', $templates[0]->sender_opt); ?>  class="rac_sender_opt"><?php _e("woocommerce", "recoverabandoncart"); ?>
                    <input type="radio" name="rac_sender_opt" id="rac_sender_local" value="local" <?php checked('local', $templates[0]->sender_opt); ?>  class="rac_sender_opt"><?php _e("local", "recoverabandoncart"); ?>
                </td>
            </tr>


            <tr class="rac_local_senders">
                <td> <?php _e('From Name', 'recoverabandoncart'); ?>:</td>
                <td><input type="text" name="rac_from_name" id="rac_from_name" value="<?php echo $templates[0]->from_name; ?>"></td>
            </tr>
            <tr class="rac_local_senders">
                <td><?php _e('From Email', 'recoverabandoncart'); ?>:</td>
                <td><input type="text" name="rac_from_email" id="rac_from_email" value="<?php echo $templates[0]->from_email; ?>"></td>
            </tr>
            <tr>
                <td><?php _e("Bcc:", "recoverabandoncart"); ?></td>
                <td><input type="textarea" id="rac_blind_carbon_copy" name="rac_blind_carbon_copy" value="<?php echo $bcc; ?>"></td>
            </tr>
            <tr>
                <td><?php _e("Subject:", "recoverabandoncart"); ?></td>
                <td><input type="text" id="rac_mail_subject" name="rac_manual_mail_subject" value="<?php echo $templates[0]->subject; ?>"></td>
            </tr>
            <tr>
                <td><?php _e("Cart Link Anchor Text:", "recoverabandoncart"); ?></td>
                <td><input type="text" id="rac_anchor_text" name="rac_anchor_text" value="<?php echo $templates[0]->anchor_text; ?>"></td>
            </tr>
            <tr>
                <td><?php _e('Message', 'recoverabandoncart'); ?>:</td>
                <?php
                $content = $templates[0]->message;
                $editor_id = "rac_manual_mail";
                $settings = array('textarea_name' => 'rac_manual_mail');
                ?>
                <td><?php wp_editor($content, $editor_id, $settings); ?></td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="rac_cart_row_ids" id="rac_cart_row_ids" value="<?php echo $_GET['rac_send_email']; ?>">
                </td>
            </tr>
            <tr>
                <td><input type="button" class="button-primary" name="rac_mail" id="rac_mail" value="<?php _e("Send Mail Now", "recoverabandoncart"); ?>"> <span id="rac_mail_result" style="display: none;"><?php _e("Mail Sent Successfully", "recoverabandoncart"); ?></span></td>
            </tr>
        </table>
        <script type="text/javascript">
            function set_tinymce_content(value) {
                if (jQuery("#wp-rac_manual_mail-wrap").hasClass("tmce-active")) {
                    return tinyMCE.activeEditor.setContent(value);
                } else {
                    return jQuery("#rac_manual_mail").val(value);
                }
            }
            function get_tinymce_content_value() {
                if (jQuery("#wp-rac_manual_mail-wrap").hasClass("tmce-active")) {
                    return tinyMCE.get('rac_manual_mail').getContent();
                } else {
                    return jQuery("#rac_manual_mail").val();
                }
            }
            jQuery(document).ready(function () {
                var template_id;
                template_id = jQuery('#rac_load_mail').val();
                jQuery('#rac_load_mail').change(function () {
                    if (jQuery('#rac_load_mail').val() != 'no') {
                        console.log(jQuery('#rac_load_mail').val());
                        var row_id = jQuery('#rac_load_mail').val();
                        var data = {
                            action: 'rac_load_mail_message',
                            row_id: row_id
                        }
                        jQuery.post(ajaxurl, data,
                                function (response) {
                                    var template = JSON.parse(response);
                                    console.log(template.message);
                                    set_tinymce_content(template.message);
                                    console.log(jQuery('#rac_manual_mail').val());
                                    jQuery("input[name=rac_sender_opt][value=" + template.mail_send_opt + "]").attr('checked', true);
                                    jQuery("select option[value=" + template.mail + "]").prop('selected', true); // mail plain or html
                                    jQuery("#rac_from_name").val(template.from_name);
                                    jQuery("#rac_logo_mail").val(template.link); //mail logo upload
                                    jQuery("#rac_from_email").val(template.from_email);
                                    jQuery("#rac_blind_carbon_copy").val(template.rac_blind_carbon_copy);
                                    jQuery("#rac_mail_subject").val(template.subject);
                                    jQuery("#rac_anchor_text").val(template.cart_link_text);
                                    template_id = row_id;
                                });
                    }
                });
                //event for sender opt
                if (jQuery('#rac_sender_woo').is(':checked'))
                {
                    jQuery('.rac_local_senders').hide();
                } else {
                    jQuery('.rac_local_senders').show();
                }
                jQuery('input[name=rac_sender_opt]').change(function () {
                    if (jQuery('#rac_sender_woo').is(':checked'))
                    {
                        jQuery('.rac_local_senders').hide();
                    } else {
                        jQuery('.rac_local_senders').show();
                    }
                });
                jQuery('#rac_mail').click(function () {     // send mail now button when you cick trigger this function
                    jQuery("#rac_mail").prop("disabled", true);
                    var rac_message = get_tinymce_content_value();
                    var data = {
                        action: 'rac_manual_mail_ajax',
                        rac_mail_row_ids: jQuery('#rac_cart_row_ids').val(),
                        rac_sender_option: jQuery('input[name=rac_sender_opt]:radio:checked').val(),
                        rac_template_mail: jQuery('select[name=rac_template_mail]').val(), // mail plain or html
                        rac_logo_mail: jQuery('#rac_logo_mail').val(), //mail logo upload
                        rac_anchor_text: jQuery('#rac_anchor_text').val(),
                        rac_message: rac_message,
                        rac_from_name: jQuery('#rac_from_name').val(),
                        rac_from_email: jQuery('#rac_from_email').val(),
                        rac_blind_carbon_copy: jQuery('#rac_blind_carbon_copy').val(),
                        rac_mail_subject: jQuery('#rac_mail_subject').val(),
                        template_id: template_id,
                    }
                    console.log(data);
                    jQuery.post(ajaxurl, data,
                            function (response) {
                                jQuery("#rac_mail").prop("disabled", false);
                                jQuery("#rac_mail_result").css("display", "inline-block");
                            });
                });
            });</script>
        <?php
    }

    public static function fp_rac_display_email_template_table() {
        //email template lists
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_templates_email';
        $templates = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC", OBJECT);
        $admin_url = admin_url('admin.php');
        $new_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail', 'rac_new_email' => 'template'), $admin_url));
        $edit_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail', 'rac_edit_email' => 'template'), $admin_url));

        echo '<a href=' . $new_template_url . '>';
        echo '<input type="button" name="rac_new_email_template" id="rac_new_email_template" class="button" value="' . __("New Template", "recoverabandoncart") . '">';
        echo '</a>';
        echo '&nbsp<span><select id="rac_pagination">';
        for ($k = 1; $k <= 20; $k++) {

            if ($k == 10) {
                echo '<option value="' . $k . '" selected="selected">' . $k . '</option>';
            } else {
                echo '<option value="' . $k . '">' . $k . '</option>';
            }
        }
        echo '</select></span>';
        echo '&nbsp<label>' . __("Search", "recoverabandoncart") . '</label><input type="text" name="rac_temp_search" id="rac_temp_search">';

        echo '<table class="rac_email_template_table table" data-page-size="10" data-filter="#rac_temp_search" data-filter-minimum="1">
	<thead>
		<tr>
			<th data-type="numeric">' . __('ID', 'recoverabandoncart') . '</th>
			<th>' . __('Template Name', 'recoverabandoncart') . '</th>
			<th>' . __('From Name', 'recoverabandoncart') . '</th>
                        <th>' . __('From Email', 'recoverabandoncart') . '</th>
                        <th>' . __('Subject', 'recoverabandoncart') . '</th>
                        <th data-hide="phone">' . __('Message', 'recoverabandoncart') . '</th>
                        <th>' . __('Status', 'recoverabandoncart') . '</th>
                        <th>' . __('Emails Sent', 'recoverabandoncart') . '</th>
                        <th>' . __('Carts Recovered', 'recoverabandoncart') . '</th>
                        <th>' . __('Email Preview', 'recoverabandoncart') . '</th>
                        <th>' . __('Duplicate', 'recoverabandoncart') . '</th>
		</tr>
	</thead>';
        foreach ($templates as $each_template) {
            echo '<tr><td data-value=' . $each_template->id . ' >';
            echo $each_template->id;
            $edit_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail', 'rac_edit_email' => $each_template->id), $admin_url));
            $email_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail', 'rac_edit_email' => $each_template->id, 'preview' => 'true'), $admin_url));
            echo '&nbsp;<span><a href="' . $edit_template_url . '">' . __('Edit', 'recoverabandoncart') . ' </a></span>&nbsp; <span><a href="" class="rac_delete" data-id="' . $each_template->id . '">' . __('Delete', 'recoverabandoncart') . '</a></span>';
            echo '</td><td>';
            echo $each_template->template_name;
            echo '</td><td>';
            if ("local" == $each_template->sender_opt) {
                echo $each_template->from_name;
                echo '</td><td>';
                echo $each_template->from_email;
            } else {
                echo get_option('woocommerce_email_from_name');
                echo '</td><td>';
                echo get_option('woocommerce_email_from_address');
            }
            echo '</td><td>';
            echo $each_template->subject;
            echo '</td><td>';
            $message = strip_tags($each_template->message);
            if (strlen($message) > 80) {
                echo substr($message, 0, 80);
                echo '.....';
            } else {
                echo $message;
            }
            echo '</td>';
            echo '<td>';
            $mail_id = $each_template->id;
            $status = $each_template->status;
            if ($status == 'ACTIVE') {
                echo ' <a href="#" class="button rac_mail_active" data-racmailid="' . $mail_id . '" data-currentstate="ACTIVE">' . __("Deactivate", "recoverabandoncart") . '</a>';
            } else {
                echo ' <a href="#" class="button rac_mail_active" data-racmailid="' . $mail_id . '" data-currentstate="NOTACTIVE">' . __("Activate", "recoverabandoncart") . '</a>';
            }
            echo '</td>';
            echo '<td>';
            $emails_sent = get_option('email_count_of_' . $each_template->id);
            if ($emails_sent > 0) {
                echo $emails_sent;
            } else {
                echo 0;
            }
            echo '</td>';

            echo '<td>';
            $carts_recovered = get_option('rac_recovered_count_of_' . $each_template->id);
            if ($carts_recovered > 0) {
                echo $carts_recovered;
            } else {
                echo 0;
            }
            echo '</td>';
            echo '<td>';
            echo ' <a href="' . $email_template_url . ' "target=_blank"> ' . __("View", "recoverabandoncart") . ' </a>';
            echo '</td>';

            echo '<td>';
            echo '<input type="button" name="rac_copy_email_template" data-value="' . $each_template->id . '" id="rac_copy_email_template' . $each_template->id . '" class="button rac_copy_email_template" value="' . __("Duplicate", "recoverabandoncart") . '">';
            echo '</td></tr>';
        }
        echo '</tbody>
            <tfoot>
		<tr>
			<td colspan="11">
				<div class="pagination pagination-centered hide-if-no-paging"></div>
			</td>
		</tr>
	</tfoot></table>';
    }

}
