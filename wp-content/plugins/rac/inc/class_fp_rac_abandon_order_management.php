<?php
/*
 * Recovered Related Functionality
 * 
 */

class FP_RAC_Abandon_Order_Management {

    public function __construct() {
        add_action('woocommerce_thankyou', array($this, 'clear_cookie'));
        add_action('woocommerce_order_status_completed', array($this, 'fp_rac_check_order_status'));
        add_action('woocommerce_order_status_processing', array($this, 'fp_rac_check_order_status'));
        add_action('woocommerce_checkout_order_processed', array($this, 'fp_rac_insert_guest_entry'));
        add_action('woocommerce_order_status_changed', array($this, 'fp_rac_order_status_guest'), 10, 3);
        add_action('woocommerce_checkout_order_processed', array($this, 'fp_rac_cookies_for_cart_recover'));
        add_action('woocommerce_checkout_order_processed', array($this, 'remove_member_acart_on_orderplaced'));

        $order_list = get_option('rac_mailcartlist_change');
        if (is_array($order_list) && (!empty($order_list))) {
            foreach ($order_list as $each_list) {
                add_action('woocommerce_order_status_' . $each_list, array($this, 'fp_rac_check_order_status'));
            }
        }
    }

    /*
     * Updating for recovered cart which placed order
     * 
     */

    public static function fp_rac_cookies_for_cart_recover($order_id) {
        if (isset($_COOKIE['rac_cart_id'])) {
            $row_id = $_COOKIE['rac_cart_id'];
            global $wpdb;
            $wpdb->update($abandon_cart_table, array('placed_order' => $order_id), array('id' => $row_id));
            update_post_meta($order_id, 'rac_order_placed', $row_id);
        } else {
            $order_placed = '1';
            self::fp_rac_check_cart_list_manual_recovery($order_id, $order_placed);
        }
    }

    /*
     * Change Member cartlist status to recovered based on order status.
     * 
     */

    public static function fp_rac_check_order_status($order_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $rac_order = get_post_meta($order_id, 'rac_order_placed', true);
        if (!empty($rac_order)) {
            $wpdb->update($table_name, array('completed' => 'completed'), array('id' => $rac_order));
            $wpdb->update($table_name, array('cart_status' => 'RECOVERED'), array('id' => $rac_order));

            $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash') AND id IN ($rac_order)", OBJECT);
            if (is_array($abandon_cart_list) && !empty($abandon_cart_list)) {
                if (is_object($abandon_cart_list[0])) {
                    $mail_template_ids = maybe_unserialize($abandon_cart_list[0]->link_status);
                }
                $mail_template_id = $mail_template_ids[0];
            }
            //counter
            if (get_option('already_recovered' . $order_id) != 'yes') {
                FPRacCounter::rac_do_recovered_count();
                FPRacCounter::rac_recovered_count_by_mail_template($mail_template_id);
                update_option('already_recovered' . $order_id, 'yes');
            }
            if (get_option('already_recorded_this_' . $order_id) != 'yes') {
                FPRacCounter::record_order_id_and_cart_id($order_id);
                update_option('already_recorded_this_' . $order_id, 'yes');
            }
            self::fp_rac_mail_admin_cart_recovered($order_id); //mailing admin on order recover
        }
        $order_placed = '2';
        self::fp_rac_check_cart_list_manual_recovery($order_id, $order_placed);
    }

    /*
     * Check Additional More Function to cross check whatever order contain cart products
     * 
     */

    public static function fp_rac_check_cart_list_manual_recovery($order_id, $orderplaced) {
        if (get_option('rac_cartlist_new_abandon_recover', true) == 'yes') {

            $allow_manual_order = get_option('rac_cartlist_new_abandon_recover_by_manual_order', true);
            if ($allow_manual_order == 'no') {
                if (get_post_meta($order_id, '_created_via', true) == 'checkout') {
                    // Run
                } else {
                    return false;
                }
            }
            global $wpdb;
            $order = new WC_Order($order_id);
            $billing_email = $order->billing_email;

            $table_name = $wpdb->prefix . 'rac_abandoncart';
            //Gather Results
            $user_id = $order->user_id; // Previously it was get_current_user_id(); if admin manually recover the cart by making order completed then admin cart will recover (it is a bug) it should be the person cart.
            $user_details = get_userdata($user_id);
            if ($user_details) {
                $user_email = $user_details->user_email;
            } else {
                $user_email = $billing_email;
            }

            $newstatus = get_option('rac_cartlist_change_from_new_to_recover') == 'yes' ? "1" : "0";
            $abandonstatus = get_option('rac_cartlist_change_from_abandon_to_recover') == 'yes' ? "1" : "0";

            if ($newstatus == '1' && $abandonstatus == '1') {
                // If both are true
                $status = array("ABANDON", "NEW");
            } elseif ($newstatus == '1' && $abandonstatus == '0') {
                $status = array("NEW");
            } elseif ($newstatus == '0' && $abandonstatus == '1') {
                $status = array("ABANDON");
            } else {
                $status = array("0");
            }
            $status = '"' . implode('","', $status) . '"';

            if ($user_email != $billing_email) {
                $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `email_id`='$user_email' and `cart_status` IN ($status)", ARRAY_A);
            } else {
                $results = $wpdb->get_results("SELECT * FROM $table_name WHERE `email_id`='$billing_email' and `cart_status` IN ($status)", ARRAY_A);
            }

            if (!empty($results)) {
                foreach ($results as $key => $value) {
                    $rac_order = $value['id'];

                    if ($orderplaced == '1') {
                        $wpdb->update($table_name, array('placed_order' => $order_id), array('id' => $rac_order));
                    }
                    if ($orderplaced == '2') {
                        $order_placed = $value['placed_order'];
                        $order_placed = $order_placed ? $order_placed : $order_id;
                        $wpdb->update($table_name, array('completed' => 'completed'), array('id' => $rac_order));
                        $wpdb->update($table_name, array('cart_status' => 'RECOVERED', 'placed_order' => $order_placed), array('id' => $rac_order));
                    }
                }
            }
        }
    }

    /*
     * clear cookie of cart id after order placed by clicked link on email.
     * 
     */

    public static function clear_cookie($orderid) {
        if (isset($_COOKIE['rac_cart_id'])) {
            unset($_COOKIE['rac_cart_id']);
            setcookie("rac_cart_id", null, -1, "/");
        }
    }

    /*
     * Send Recovered Mail to admin after order placed by clicked link on email 
     * 
     */

    public static function fp_rac_mail_admin_cart_recovered($order_id) {

        if (get_option('rac_admin_cart_recovered_noti') == "yes") {
            $to = get_option('rac_admin_email');
            $subject = get_option('rac_recovered_email_subject');
            $message = get_option('rac_recovered_email_message');
            $from_name = get_option('rac_recovered_from_name');
            $from_email = get_option('rac_recovered_from_email');
            $sender_opt = get_option('rac_recovered_sender_opt');
            $compact = array($sender_opt, $from_name, $from_email);
            $headers = FPRacCron::rac_format_email_headers($compact);
            $html_template = ($sender_opt == "woo") ? 'HTML' : 'PLAIN';
            $message = str_replace('{rac.recovered_order_id}', $order_id, $message); //replacing shortcode for order id
            ob_start();
            $order = new WC_Order($order_id);
            ?>
            <table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1">
                <thead>
                    <tr>
                        <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product', 'woocommerce'); ?></th>
                        <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Quantity', 'woocommerce'); ?></th>
                        <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Price', 'woocommerce'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $order->email_order_items_table(false, true); ?>
                </tbody>
                <tfoot>
                    <?php
                    if ($totals = $order->get_order_item_totals()) {
                        $i = 0;
                        foreach ($totals as $total) {
                            $i++;
                            ?><tr>
                                <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
                                <td style="text-align:left; border: 1px solid #eee; <?php if ($i == 1) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
                            </tr><?php
                        }
                    }
                    ?>
                </tfoot>
            </table>

            <?php
            $newdata = ob_get_clean();

            ob_start();
            $message = str_replace('{rac.order_line_items}', $newdata, $message);
            if (function_exists('wc_get_template')) {
                wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                wc_get_template('emails/email-footer.php');
            } else {
                woocommerce_get_template('emails/email-header.php', array('email_heading' => $subject));
                echo $message;
                woocommerce_get_template('emails/email-footer.php');
            }
            $woo_temp_msg = ob_get_clean();
            if ('wp_mail' == get_option('rac_trouble_mail')) {
                FPRacCron::rac_send_wp_mail($to, $subject, $woo_temp_msg, $headers, $html_template, $compact);
            } else {
                FPRacCron::rac_send_mail($to, $subject, $woo_temp_msg, $headers, $compact);
            }
        }
    }

    /*
     * Add manual entry as Guest in cart list table.
     * 
     */

    public static function fp_rac_insert_guest_entry($order_id) {

        if (!is_user_logged_in()) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $currentuser_lang = fp_rac_get_current_language();
            if (!isset($_COOKIE['rac_cart_id']) && !isset($_REQUEST['token']) && !isset($_COOKIE['rac_checkout_entry'])) { // We can remove cookie check if we want
                $order = new WC_Order($order_id);
                $user_email = $order->billing_email;
                if (get_option('rac_remove_carts') == 'yes') {
                    if (get_option('rac_remove_new') == 'yes') {
                        $wpdb->delete($table_name, array('email_id' => $user_email, 'cart_status' => 'NEW'));
                    }
                    if (get_option('rac_remove_abandon') == 'yes') {
                        $wpdb->delete($table_name, array('email_id' => $user_email, 'cart_status' => 'ABANDON'));
                    }
                }
                $check_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id=0 ORDER BY id DESC LIMIT 1", OBJECT);
                $db_cart_content = maybe_unserialize($check_cart[0]->cart_details);
                if (empty($db_cart_content)) {// IF no previous entry make a new
                    $user_id = "0";
                    $current_time = current_time('timestamp');
                    $cart_content = maybe_serialize($order);
                    if (RecoverAbandonCart::restrict_entries_in_cart_list($user_email) == 'proceed') {
                        $wpdb->insert($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $user_email, 'cart_abandon_time' => $current_time, 'cart_status' => 'NEW', 'wpml_lang' => $currentuser_lang));
                    }
                    update_post_meta($order->id, 'guest_cart', 'yes');
                } else {
                    if (is_object($db_cart_content)) {
                        if ($db_cart_content->id != $order->id) { // don't allow if they refresh again || if already exist
                            $current_time = current_time('timestamp');
                            $cart_content = maybe_serialize($order);
                            $user_id = "0";
                            $wpdb->insert($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $user_email, 'cart_abandon_time' => $current_time, 'cart_status' => 'NEW', 'wpml_lang' => $currentuser_lang));
                            update_post_meta($order->id, 'guest_cart', 'yes');
                        }
                    } else {
                        //create after checkout cart
                        $current_time = current_time('timestamp');
                        $cart_content = maybe_serialize($order);
                        $user_id = "0";
                        $wpdb->insert($table_name, array('cart_details' => $cart_content, 'user_id' => $user_id, 'email_id' => $user_email, 'cart_abandon_time' => $current_time, 'cart_status' => 'NEW', 'wpml_lang' => $currentuser_lang));
                        update_post_meta($order->id, 'guest_cart', 'yes');
                    }
                }
            } elseif (isset($_COOKIE['rac_checkout_entry']) && !isset($_COOKIE['rac_cart_id'])) {
                //Check cookies for deleting cart captured from checkout
                //Delete only if it is not recoverd from mail
                $delete_id = $_COOKIE['rac_checkout_entry'];
                $wpdb->delete($table_name, array('id' => $delete_id));
                //delete entry
            }
        }
    }

    /*
     * Remove Member cart list after order placed by normal.
     * 
     */

    public static function remove_member_acart_on_orderplaced($order_id) {
        if (is_user_logged_in()) {
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $order = new WC_Order($order_id);
            $user_id = $order->user_id;
            if (!empty($user_id)) { // order by members
                $part_user_acart = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='$user_id' AND cart_status='NEW'", OBJECT);
                if (!empty($part_user_acart)) {
                    foreach ($part_user_acart as $each_entry) {
                        $stored_cart = maybe_unserialize($each_entry->cart_details);
                        if (is_array($stored_cart) && !empty($stored_cart)) {
                            if (isset($stored_cart['shipping_details'])) {
                                unset($stored_cart['shipping_details']);
                            }
                            foreach ($stored_cart as $cart_details) {
                                if (isset($cart_details['cart']) && !empty($cart_details['cart'])) {
                                    if (count($cart_details['cart']) <= count($order->get_items())) {
                                        $order_item_product_ids = array();
                                        $rac_cart_product_ids = array();
                                        foreach ($cart_details['cart'] as $product) {
                                            $rac_cart_product_ids[] = $product['product_id'];
                                        }
                                        foreach ($order->get_items() as $items) {
                                            $order_item_product_ids[] = $items['product_id'];
                                        }
                                        $check_array = array_diff($rac_cart_product_ids, $order_item_product_ids);
                                        if (empty($check_array)) {
                                            $wpdb->delete($table_name, array('id' => $each_entry->id));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Delete Guest cartlist based on order status.
     * 
     */

    public static function fp_rac_order_status_guest($order_id, $old, $new_status) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $check_guest_cart = get_post_meta($order_id, 'guest_cart', true);
        if ($check_guest_cart == 'yes') {
            if (get_option('rac_guest_abadon_type_processing') == 'yes') { //option selected by user
                if ($new_status == 'processing') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            } if (get_option('rac_guest_abadon_type_completed') == 'yes') {//option selected by user
                if ($new_status == 'completed') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }
            //rac_guest_abadon_type_pending
            if (get_option('rac_guest_abadon_type_pending') == 'yes') {//option selected by user
                if ($new_status == 'pending') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }
            //failed rac_guest_abadon_type_failed
            if (get_option('rac_guest_abadon_type_failed') == 'yes') {//option selected by user
                if ($new_status == 'failed') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }
            //on-hold  rac_guest_abadon_type_on-hold
            if (get_option('rac_guest_abadon_type_on-hold') == 'yes') {//option selected by user
                if ($new_status == 'on-hold') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }
            //refunded rac_guest_abadon_type_refunded
            if (get_option('rac_guest_abadon_type_refunded') == 'yes') {//option selected by user
                if ($new_status == 'refunded') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }

            // rac_guest_abadon_type_cancelled
            if (get_option('rac_guest_abadon_type_cancelled') == 'yes') {//option selected by user
                if ($new_status == 'cancelled') {
                    $get_list = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id='0'", OBJECT);
                    foreach ($get_list as $each_entry) {
                        $expected_object = maybe_unserialize($each_entry->cart_details);
                        if (is_object($expected_object)) {
                            if ($expected_object->id == $order_id) {
                                $wpdb->delete($table_name, array('id' => $each_entry->id));
                            }
                        }
                    }
                }
            }
        }
    }

}

new FP_RAC_Abandon_Order_Management();
