<?php

class FP_RAC_Cart_Data_From_Mail {

    public function __construct() {
        add_action('wp_head', array($this, 'recover_old_order_rac'));
        add_action('wp_head', array($this, 'fp_rac_guest_cart_recover'));
        add_action('woocommerce_init', array($this, 'fp_rac_add_abandon_cart'));
    }

    /*
     * only perform recover from member mail
     * 
     */

    public static function fp_rac_add_abandon_cart() {
        global $woocommerce;
        //only perform recover from member mail
        if (isset($_GET['abandon_cart']) && !isset($_GET['guest']) && !isset($_GET['checkout']) && !isset($_GET['old_order'])) {
            $abandon_cart_id = $_GET['abandon_cart'];
            $email_template_id = $_GET['email_template'];
            global $wpdb;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $last_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $abandon_cart_id and cart_status IN('NEW','ABANDON') and placed_order IS NULL", OBJECT);
            end($last_cart);

            $last_cart_key = key($last_cart);
            if (isset($last_cart_key)) {
                $user_details = maybe_unserialize($last_cart[$last_cart_key]->cart_details);
                unset($user_details['shipping_details']);
                foreach ($user_details as $cart) {
                    $cart_content = $cart['cart'];
                }

                if (get_option('rac_cart_content_when_cart_link_is_clicked') != 'no') {
                    $new_session_cart = $cart_content;
                } else {
                    $old_session_cart = WC()->session->cart;
                    $cart_content = fp_rac_check_is_array($cart_content);
                    $array_cart_content_filter = array_filter($cart_content);
                    $old_session_cart = fp_rac_check_is_array($old_session_cart);
                    $array_session_cart_filter = array_filter($old_session_cart);
                    $new_session_cart = array_merge($array_cart_content_filter, $array_session_cart_filter);
                }

                if (!isset($_COOKIE['rac_cart_id'])) {
                    if (function_exists('WC')) {
                        WC()->session->cart = $new_session_cart;
                    } else {
                        $woocommerce->session->cart = $new_session_cart;
                    }
                    setcookie("rac_cart_id", $abandon_cart_id, time() + 3600, "/");
                } else {
                    $get_cookie_id = $_COOKIE['rac_cart_id'];
                    if ($get_cookie_id != $abandon_cart_id) {
                        if (function_exists('WC')) {
                            WC()->session->cart = $new_session_cart;
                        } else {
                            $woocommerce->session->cart = $new_session_cart;
                        }
                        setcookie("rac_cart_id", $abandon_cart_id, time() + 3600, "/");
                    }
                }
                if (!empty($last_cart[$last_cart_key]->link_status)) {
                    $email_template_ids_db = maybe_unserialize($last_cart[$last_cart_key]->link_status);
                    if (!in_array($email_template_id, (array) $email_template_ids_db)) { //check for id duplication
                        $email_template_ids_db[] = $email_template_id;
                        $email_template_id_final = $email_template_ids_db;
                    }
                    $email_template_id_final = $email_template_ids_db;
                } else {
                    $email_template_id_final = array($email_template_id);
                }
                $email_template_id_final = maybe_serialize($email_template_id_final);
                $wpdb->update($table_name, array('link_status' => $email_template_id_final), array('id' => $abandon_cart_id));
                FPRacCounter::rac_do_linkc_count($abandon_cart_id, $email_template_id);
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($last_cart[$last_cart_key]->wpml_lang);
            } else {
                $currentuser_lang = fp_rac_get_current_language();
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($currentuser_lang);
                wc_add_notice(__('Seems your cart has been already Recovered/Order Placed', 'recoverabandoncart'), 'error');
            }
            if (!is_user_logged_in()) {
                if (function_exists('WC')) {
                    WC()->session->set_customer_session_cookie(true);
                } else {
                    $woocommerce->session->set_customer_session_cookie(true);
                }
            }
            wp_safe_redirect($redirect_url);
            exit;
        }
    }

    /*
     * only perform recover from guest mail
     * 
     */

    public static function fp_rac_guest_cart_recover() {
        global $wpdb;
        global $woocommerce;
        if (isset($_GET['guest'])) {
            $email_template_id_final = '';
            $abandon_cart_id = $_GET['abandon_cart'];
            $email_template_id = $_GET['email_template'];
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $last_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $abandon_cart_id and cart_status IN('NEW','ABANDON') and placed_order IS NULL", OBJECT);

            $last_cart_key = key($last_cart);
            $expected_object = maybe_unserialize($last_cart[$last_cart_key]->cart_details);
            if (isset($last_cart_key)) {
                if (is_object($expected_object)) {
                    //For Object Recover Abandon Cart
                    $cart_details = $expected_object->get_items();
                    if (get_option('rac_cart_content_when_cart_link_is_clicked') == 'yes') {
                        $woocommerce->cart->empty_cart();
                    }
                    if (is_array($cart_details) && !empty($cart_details)) {
                        foreach ($cart_details as $products) {
                            $product = get_product($products['product_id']);
                            if (!empty($products['variation_id'])) {
                                $variations = array();
                                if (is_array($products['item_meta']) && !empty($products['item_meta'])) {
                                    foreach ($products['item_meta'] as $meta_name => $meta_value) {
                                        $attributes = $product->get_variation_attributes();
                                        $lower_case = array_change_key_case($attributes, CASE_LOWER);
                                        if (!empty($lower_case[$meta_name])) {
                                            if (!is_null($lower_case[$meta_name])) {
                                                $value_true = in_array(strtolower($meta_value[0]), array_map('strtolower', $lower_case[$meta_name]));
                                            } else {
                                                $value_true = false;
                                            }
                                        }

                                        if (in_array(strtolower($meta_name), array_map('strtolower', array_keys($attributes))) && $value_true) {
                                            $variations[$meta_name] = $meta_value[0];
                                        }
                                    }
                                }
                                $products['qty'][0];
                                $woocommerce->cart->add_to_cart($products['product_id'], $products['qty'][0], $products['variation_id'], array_filter($variations));
                            } else {
                                $woocommerce->cart->add_to_cart($products['product_id'], $products['qty']);
                            }
                        }
                    }
                    setcookie("rac_cart_id", $abandon_cart_id, time() + 3600, "/");

                    if (!empty($last_cart[$last_cart_key]->link_status)) {
                        $email_template_ids_db = maybe_unserialize($last_cart[$last_cart_key]->link_status);
                        if (!in_array($email_template_id, (array) $email_template_ids_db)) { //check for id duplication
                            $email_template_ids_db[] = $email_template_id;
                            $email_template_id_final = $email_template_ids_db;
                        }
                    } else {
                        $email_template_id_final = array($email_template_id);
                    }
                } elseif (is_array($expected_object)) {

                    $expected_object = maybe_unserialize($last_cart[$last_cart_key]->cart_details);
                    $cart_details = $expected_object;
                    unset($cart_details['visitor_mail']);
                    unset($cart_details['first_name']);
                    unset($cart_details['last_name']);
                    unset($cart_details['visitor_phone']);
                    unset($cart_details['shipping_details']);
                    if (get_option('rac_cart_content_when_cart_link_is_clicked') == 'yes') {
                        $woocommerce->cart->empty_cart();
                    }
                    if (is_array($cart_details) && !empty($cart_details)) {
                        foreach ($cart_details as $products) {
                            if (!empty($products['variation_id'])) {
                                $variations = array();

                                foreach ($products['variation'] as $attr_name => $attr_val) {
                                    $var_name = str_replace("attribute_", '', $attr_name);
                                    $variations[$var_name] = $attr_val;
                                }

                                $woocommerce->cart->add_to_cart($products['product_id'], $products['quantity'], $products['variation_id'], $variations, $products);
                            } else {
                                $woocommerce->cart->add_to_cart($products['product_id'], $products['quantity']);
                            }
                        }
                    }
                    setcookie("rac_cart_id", $abandon_cart_id, time() + 3600, "/");
                    if (!empty($last_cart[$last_cart_key]->link_status)) {
                        $email_template_ids_db = maybe_unserialize($last_cart[$last_cart_key]->link_status);
                        if (!in_array($email_template_id, (array) $email_template_ids_db)) { //check for id duplication
                            $email_template_ids_db[] = $email_template_id;
                            $email_template_id_final = $email_template_ids_db;
                        }
                    } else {
                        $email_template_id_final = array($email_template_id);
                    }
                }
                $email_template_id_final = maybe_serialize($email_template_id_final);
                $wpdb->update($table_name, array('link_status' => $email_template_id_final), array('id' => $abandon_cart_id));
                FPRacCounter::rac_do_linkc_count($abandon_cart_id, $email_template_id);
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($last_cart[$last_cart_key]->wpml_lang);
            } else {
                $currentuser_lang = fp_rac_get_current_language();
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($currentuser_lang);
                wc_add_notice(__('Seems your cart has been already Recovered/Order Placed', 'recoverabandoncart'), 'error');
            }
            //Redirect again to cart
            wp_safe_redirect($redirect_url);
            exit;
        }
    }

    /*
     * only perform recover from olderorder mail
     * 
     */

    public static function recover_old_order_rac() {
        // old order made as abandoned by update button
        if (isset($_GET['old_order'])) {
            $abandon_cart_id = $_GET['abandon_cart'];
            $email_template_id = $_GET['email_template'];
            global $wpdb;
            global $woocommerce;
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $last_cart = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $abandon_cart_id and cart_status IN('NEW','ABANDON') and placed_order IS NULL", OBJECT);
            end($last_cart);
            $last_cart_key = key($last_cart);
            $expected_object = maybe_unserialize($last_cart[$last_cart_key]->cart_details);
            if (isset($last_cart_key)) {
                if (is_object($expected_object)) {
                    $cart_details = $expected_object->get_items();
                    if (get_option('rac_cart_content_when_cart_link_is_clicked') == 'yes') {
                        $woocommerce->cart->empty_cart();
                    }
                    if (is_array($cart_details) && !empty($cart_details)) {
                        foreach ($cart_details as $products) {
                            $product = get_product($products['product_id']);
                            if (!empty($products['variation_id'])) {
                                $variations = array();
                                if (is_array($products['item_meta']) && !empty($products['item_meta'])) {
                                    foreach ($products['item_meta'] as $meta_name => $meta_value) {
                                        $attributes = $product->get_variation_attributes();
                                        $lower_case = array_change_key_case($attributes, CASE_LOWER);
                                        if (!is_null($lower_case[$meta_name])) {
                                            $value_true = in_array(strtolower($meta_value[0]), array_map('strtolower', $lower_case[$meta_name]));
                                        } else {
                                            $value_true = false;
                                        }
                                        if (in_array(strtolower($meta_name), array_map('strtolower', array_keys($attributes))) && $value_true) {
                                            $variations[$meta_name] = $meta_value[0];
                                        }
                                    }
                                }
                                $woocommerce->cart->add_to_cart($products['product_id'], $products['qty'], $products['variation_id'], $variations);
                            } else {
                                $woocommerce->cart->add_to_cart($products['product_id'], $products['qty']);
                            }
                        }
                    }
                }
                setcookie("rac_cart_id", $abandon_cart_id, time() + 3600, "/");
                if (!empty($last_cart[$last_cart_key]->link_status)) {
                    $email_template_ids_db = maybe_unserialize($last_cart[$last_cart_key]->link_status);
                    if (!in_array($email_template_id, (array) $email_template_ids_db)) { //check for id duplication
                        $email_template_ids_db[] = $email_template_id;
                        $email_template_id_final = $email_template_ids_db;
                    }
                } else {
                    $email_template_id_final = array($email_template_id);
                }
                $email_template_id_final = maybe_serialize($email_template_id_final);
                $wpdb->update($table_name, array('link_status' => $email_template_id_final), array('id' => $abandon_cart_id));
                FPRacCounter::rac_do_linkc_count($abandon_cart_id, $email_template_id);
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($last_cart[$last_cart_key]->wpml_lang);
            } else {
                $currentuser_lang = fp_rac_get_current_language();
                $redirect_url = fp_rac_url_for_checkout_or_cart_with_lan($currentuser_lang);
                wc_add_notice(__('Seems your cart has been already Recovered/Order Placed', 'recoverabandoncart'), 'error');
            }
            wp_safe_redirect($redirect_url);
            exit;
        }
    }

}

function fp_rac_url_for_checkout_or_cart_with_lan($lang_code) {
    if (get_option('rac_cartlink_redirect') == '2') {
        $redirect_url = rac_get_page_permalink_dependencies('checkout');
        if ($lang_code != NULL) {
            $redirect_url = $lang_code == 'en' ? $redirect_url : fp_rac_wpml_convert_url($redirect_url, $lang_code);
        }
    } else {
        $redirect_url = rac_get_page_permalink_dependencies('cart');

        if ($lang_code != NULL) {

            $redirect_url = $lang_code == 'en' ? $redirect_url : fp_rac_wpml_convert_url($redirect_url, $lang_code);
        }
    }
    return $redirect_url;
}

new FP_RAC_Cart_Data_From_Mail();

function fp_rac_check_is_array($array) {
    if (is_array($array)) {
        $array = $array;
    } else {
        $array = explode(',', $array);
    }
    return $array;
}
