<?php

/*
 * ajax response functions for email template.
 * 
 */

class FP_RAC_Ajax_Response_for_Email_Template {

    public function __construct() {
        add_action('wp_ajax_rac_new_template', array($this, 'fp_rac_create_new_email_template'));
        add_action('wp_ajax_rac_edit_template', array($this, 'fp_rac_edit_email_template'));
        add_action('wp_ajax_rac_send_template_preview_email', array($this, 'fp_rac_send_email_template_test_mail'));
        add_action('wp_ajax_copy_this_template', array($this, 'fp_rac_copy_email_template'));
        add_action('wp_ajax_rac_delete_email_template', array($this, 'fp_rac_delete_email_template'));
        add_action('wp_ajax_rac_email_template_status', array($this, 'set_email_template_status'));
    }

    public static function fp_rac_create_new_email_template() {
        if (isset($_POST['rac_template_name'])) {
            global $wpdb;
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            //Adding Extra Column In Email Template
            fp_rac_add_extra_column_in_email_template();
            $template_segmentation = isset($_POST['rac_template_segmentation']) ? maybe_serialize($_POST['rac_template_segmentation']) : '';
            $wpdb->insert($table_name_email, array('template_name' => stripslashes($_POST['rac_template_name']),
                'status' => stripslashes($_POST['rac_template_status']),
                'sender_opt' => stripslashes($_POST['rac_sender_option']),
                'from_name' => stripslashes($_POST['rac_from_name']),
                'from_email' => stripslashes($_POST['rac_from_email']),
                'rac_blind_carbon_copy' => stripslashes($_POST['rac_blind_carbon_copy']),
                'subject' => stripslashes($_POST['rac_subject']),
                'anchor_text' => stripslashes($_POST['rac_anchor_text']),
                'message' => stripslashes($_POST['rac_message']),
                'sending_type' => stripslashes($_POST['rac_duration_type']),
                'segmentation' => stripslashes($template_segmentation),
                'sending_duration' => stripslashes($_POST['rac_mail_duration']),
                'mail' => stripslashes($_POST['rac_template_mail']), // mail plain or html
                'link' => stripslashes($_POST['rac_logo_mail']))           // mail logo upload
            );
        }
        echo $wpdb->insert_id;
        exit();
    }

    public static function fp_rac_edit_email_template() {
        if (isset($_POST['rac_template_id'])) {
            $template_id = $_POST['rac_template_id'];
            global $wpdb;
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            //Adding Extra Column In Email Template
            fp_rac_add_extra_column_in_email_template();
            $template_segmentation = isset($_POST['rac_template_segmentation']) ? maybe_serialize($_POST['rac_template_segmentation']) : '';
            $wpdb->update($table_name_email, array('template_name' => stripslashes($_POST['rac_template_name']),
                'status' => stripslashes($_POST['rac_template_status']),
                'sender_opt' => stripslashes($_POST['rac_sender_option']),
                'from_name' => stripslashes($_POST['rac_from_name']),
                'from_email' => stripslashes($_POST['rac_from_email']),
                'rac_blind_carbon_copy' => stripslashes($_POST['rac_blind_carbon_copy']),
                'subject' => stripslashes($_POST['rac_subject']),
                'anchor_text' => stripslashes($_POST['rac_anchor_text']),
                'message' => stripslashes($_POST['rac_message']),
                'segmentation' => stripslashes($template_segmentation),
                'sending_type' => stripslashes($_POST['rac_duration_type']),
                'mail' => stripslashes($_POST['rac_template_mail']), // mail plain or html
                'link' => stripslashes($_POST['rac_logo_mail']), // mail logo upload
                'sending_duration' => stripslashes($_POST['rac_mail_duration'])), array('id' => $template_id));
        }
        exit();
    }

    public static function fp_rac_delete_email_template() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $row_id = $_POST['row_id'];
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            $wpdb->delete($table_name_email, array('id' => $row_id));
            //removing registered WPML strings
            if (function_exists('icl_unregister_string')) {
                icl_unregister_string('RAC', 'rac_template_' . $row_id . '_message');
                icl_unregister_string('RAC', 'rac_template_' . $row_id . '_subject');
            }
        }
        exit();
    }

    public static function set_email_template_status() {
        if (isset($_POST['row_id'])) {
            global $wpdb;
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            $requesting_state = $_POST['status'] == 'ACTIVE' ? 'NOTACTIVE' : 'ACTIVE';
            $wpdb->update($table_name_email, array('status' => $requesting_state), array('id' => $_POST['row_id']));
            echo $requesting_state;
        }
        exit();
    }

    public static function fp_rac_copy_email_template() {
        global $wpdb;
        $id = $_POST['row_id'];
        $table_name = $wpdb->prefix . 'rac_templates_email';
        //Adding Extra Column In Email Template
        fp_rac_add_extra_column_in_email_template();
        $templates = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$id", OBJECT);
        foreach ($templates as $my_template) {
            $id = $my_template->id;
            $template_name = $my_template->template_name;
            $template_name_copy = $template_name . '-copy';
            $sender_opt = $my_template->sender_opt;
            $from_name = $my_template->from_name;
            $from_email = $my_template->from_email;
            $bcc = $my_template->rac_blind_carbon_copy;
            $subject = $my_template->subject;
            $anchor_text = $my_template->anchor_text;
            $message = $my_template->message;
            $sending_type = $my_template->sending_type;
            $sending_duration = $my_template->sending_duration;
            $segmentation = isset($my_template->segmentation) ? $my_template->segmentation : '';
            $status = $my_template->status;
            $mail = $my_template->mail;
            $link = $my_template->link;
        }
        $wpdb->insert($table_name, array('template_name' => stripslashes($template_name_copy),
            'status' => stripslashes($status),
            'sender_opt' => stripslashes($sender_opt),
            'from_name' => stripslashes($from_name),
            'from_email' => stripslashes($from_email),
            'rac_blind_carbon_copy' => stripslashes($bcc),
            'subject' => stripslashes($subject),
            'anchor_text' => stripslashes($anchor_text),
            'message' => stripslashes($message),
            'segmentation' => $segmentation,
            'sending_type' => stripslashes($sending_type),
            'sending_duration' => stripslashes($sending_duration),
            'mail' => stripslashes($mail), // mail plain or html
            'link' => stripslashes($link))           // mail logo upload
        );
        exit();
    }

    public static function fp_rac_send_email_template_test_mail() {
        global $wpdb, $woocommerce, $to;
        if (isset($_POST['rac_template_id'])) {
            $to = $_POST['rac_to_email'];
            $sender_option_post = stripslashes($_POST['rac_sender_option']);
            $mail_template_post = stripslashes($_POST['rac_template_mail']);  // mail plain or html
            $mail_logo_added = stripslashes($_POST['rac_logo_mail']);   // mail logo uploaded
            $from_name_post = stripslashes($_POST['rac_from_name']);
            $from_email_post = stripslashes($_POST['rac_from_email']);
            $message_post = stripslashes($_POST['rac_message']);
            $bcc_post = stripslashes($_POST['rac_blind_carbon_copy']);
            $subject_post = stripslashes($_POST['rac_subject']);
            $anchor_text_post = stripslashes($_POST['rac_anchor_text']);
            $mail_template_id_post = isset($_POST['rac_template_id']) ? $_POST['rac_template_id'] : '';
            $table_name_email = $wpdb->prefix . 'rac_templates_email';
            $date = date_i18n(rac_date_format());
            $time = date_i18n(rac_time_format());
            ?>
            <style type="text/css">
                table {
                    border-collapse: separate;
                    border-spacing: 0;
                    color: #4a4a4d;
                    font: 14px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
                }
                th,
                td {
                    padding: 10px 15px;
                    vertical-align: middle;
                }
                thead {
                    background: #395870;
                    background: linear-gradient(#49708f, #293f50);
                    color: #fff;
                    font-size: 11px;
                    text-transform: uppercase;
                }
                th:first-child {
                    border-top-left-radius: 5px;
                    text-align: left;
                }
                th:last-child {
                    border-top-right-radius: 5px;
                }
                tbody tr:nth-child(even) {
                    background: #f0f0f2;
                }
                td {
                    border-bottom: 1px solid #cecfd5;
                    border-right: 1px solid #cecfd5;
                }
                td:first-child {
                    border-left: 1px solid #cecfd5;
                }
                .book-title {
                    color: #395870;
                    display: block;
                }
                .text-offset {
                    color: #7c7c80;
                    font-size: 12px;
                }
                .item-stock,
                .item-qty {
                    text-align: center;
                }
                .item-price {
                    text-align: right;
                }
                .item-multiple {
                    display: block;
                }
                tfoot {
                    text-align: right;
                }
                tfoot tr:last-child {
                    background: #f0f0f2;
                    color: #395870;
                    font-weight: bold;
                }
                tfoot tr:last-child td:first-child {
                    border-bottom-left-radius: 5px;
                }
                tfoot tr:last-child td:last-child {
                    border-bottom-right-radius: 5px;
                }

            </style>
            <?php

            $compact = array($sender_option_post, $from_name_post, $from_email_post);
            $headers = FPRacCron::rac_format_email_headers($compact, $bcc_post);
            $cart_url = rac_get_page_permalink_dependencies('cart');
            $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => '00', 'email_template' => $mail_template_id_post), $cart_url));
            if (get_option('rac_cart_link_options') == '1') {
                $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', 'en', $anchor_text_post) . '</a>';
            } elseif (get_option('rac_cart_link_options') == '2') {
                $url_to_click = $url_to_click;
            } else {
                $cart_Text = fp_get_wpml_text('rac_template_' . $mail_template_id_post . '_anchor_text', 'en', $anchor_text_post);
                $url_to_click = RecoverAbandonCart::rac_cart_link_button_mode($url_to_click, $cart_Text);
            }
            $subject_post = RecoverAbandonCart::shortcode_in_subject('First Name', 'Last Name', $subject_post);
            $message_post = str_replace('{rac.cartlink}', $url_to_click, $message_post);
            $message_post = str_replace('{rac.date}', $date, $message_post);
            $message_post = str_replace('{rac.time}', $time, $message_post);
            $message_post = str_replace('{rac.firstname}', 'First Name', $message_post);
            $message_post = str_replace('{rac.lastname}', 'Last Name', $message_post);
            $message_post = str_replace('{rac.Productinfo}', RecoverAbandonCart::sample_productinfo_shortcode(), $message_post);
            $message_post = str_replace('{rac.coupon}', 'testcoupo.n1234567890', $message_post);

            $message_post = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message_post);
            add_filter('woocommerce_email_footer_text', array('RecoverAbandonCart', 'rac_footer_email_customization'));
            $message_post = do_shortcode($message_post); //shortcode feature
            if ($mail_logo_added == '') {
                $logo = '';
            } else {
                $logo = '<table><tr><td align="center" valign="top"><p style="margin-top:0;"><img style="max-height:600px;max-width:600px;" src="' . esc_url($mail_logo_added) . '" /></p></td></tr></table>';
            }
            // woocommerce template
            if ($mail_template_post == "HTML") {
                $woo_temp_msg = RecoverAbandonCart::email_woocommerce_html($mail_template_post, $subject_post, $message_post, $logo);
                FP_RAC_Send_Email_Woocommerce_Mailer::send_email_via_woocommerce_mailer($to, $subject_post, $woo_temp_msg, $headers, $compact);
                echo 1;
                exit();
            } else {
                $woo_temp_msg = RecoverAbandonCart::email_woocommerce_html($mail_template_post, $subject_post, $message_post, $logo);
                wp_mail($to, $subject_post, $woo_temp_msg, $headers);
                echo 1;
                exit();
            }
        }
    }

}

new FP_RAC_Ajax_Response_for_Email_Template();
