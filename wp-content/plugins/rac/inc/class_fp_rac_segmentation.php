<?php

class FP_RAC_Segmentation {

    /**
     * This function is used to check Send Mail Based On 
     * 1. Number of Orders Placed by each User or Guest
     * 2. Total Amount Spent by each User or Guest.
     * 3. Abondon Cart Amount.
     * 4. Abandon Cart Date
     * 5. Abandon Cart Quantity.
     * 6. User Roles.
     * 7. Selected Products.
     */
    public static function check_send_mail_based_on($each_cart, $email_template, $user_id, $email_id) {
        $email_template = maybe_unserialize($email_template->segmentation);
        if (isset($email_template)) {
            $send_mail_option = $email_template['rac_template_seg_type'];
            //Numbers Of orders Placed.
            if ($send_mail_option == 'rac_template_seg_odrer_count') {
                $order_placed_min = $email_template['rac_template_seg_odrer_count_min'] != '*' ? (float) $email_template['rac_template_seg_odrer_count_min'] : '*';
                $order_placed_max = $email_template['rac_template_seg_odrer_count_max'] != '*' ? (float) $email_template['rac_template_seg_odrer_count_max'] : '*';
                if ($order_placed_min == '*' && $order_placed_max == '*') {
                    return true;
                } else {
                    $total_order_placed = rac_get_no_of_orders_placed($user_id, $email_id);
                    return rac_check_status_of_min_max($total_order_placed, $order_placed_min, $order_placed_max);
                }

                //Total Amount Spent each User
            } elseif ($send_mail_option == 'rac_template_seg_odrer_amount') {
                $order_placd_total_min = $email_template['rac_template_seg_odrer_amount_min'] != '*' ? (float) $email_template['rac_template_seg_odrer_amount_min'] : '*';
                $order_placd_total_max = $email_template['rac_template_seg_odrer_amount_max'] != '*' ? (float) $email_template['rac_template_seg_odrer_amount_max'] : '*';
                if ($order_placd_total_min == '*' && $order_placd_total_max == '*') {
                    return true;
                } else {
                    $total_amount_spent = rac_get_total_amount_spent($user_id, $email_id);
                    return rac_check_status_of_min_max($total_amount_spent, $order_placd_total_min, $order_placd_total_max);
                }

                //Abandon Cart Amount Limit.
            } else if ($send_mail_option == 'rac_template_seg_cart_total') {
                $cart_total_min = $email_template['rac_template_seg_cart_total_min'] != '*' ? (float) $email_template['rac_template_seg_cart_total_min'] : '*';
                $cart_total_max = $email_template['rac_template_seg_cart_total_max'] != '*' ? (float) $email_template['rac_template_seg_cart_total_max'] : '*';
                if ($cart_total_min == '*' && $cart_total_max == '*') {
                    return true;
                } else {
                    $cart_total = rac_get_abadon_cart_total($each_cart);
                    return rac_check_status_of_min_max($cart_total, $cart_total_min, $cart_total_max);
                }

                //Abandon Date Range.              
            } else if ($send_mail_option == 'rac_template_seg_cart_date') {
                $cart_abadon_from_date = $email_template['rac_template_seg_cart_from_date'] != '' ? strtotime($email_template['rac_template_seg_cart_from_date'] . '00:00:00') : '';
                $cart_abadon_to_date = $email_template['rac_template_seg_cart_to_date'] != '' ? strtotime($email_template['rac_template_seg_cart_to_date'] . '23:59:59') : '';
                return rac_check_status_of_from_to_date($each_cart->cart_abandon_time, $cart_abadon_from_date, $cart_abadon_to_date);

                //Check Each Cart Quantity Range  
            } else if ($send_mail_option == 'rac_template_seg_cart_quantity') {
                $cart_total_quantity_min = $email_template['rac_template_seg_cart_quantity_min'] != '*' ? (float) $email_template['rac_template_seg_cart_quantity_min'] : '*';
                $cart_total_quantity_max = $email_template['rac_template_seg_cart_quantity_max'] != '*' ? (float) $email_template['rac_template_seg_cart_quantity_max'] : '*';
                if ($cart_total_quantity_min == '*' && $cart_total_quantity_max == '*') {
                    return true;
                } else {
                    $total_qty = rac_get_total_eachcart_qty($each_cart);
                    return rac_check_status_of_min_max($total_qty, $cart_total_quantity_min, $cart_total_quantity_max);
                }

                //Check Selected Roles match with Each cart User Role.             
            } elseif ($send_mail_option == 'rac_template_seg_user_role') {
                $selected_user_roles = $email_template['rac_template_seg_selected_user_role'];
                if (empty($selected_user_roles)) {
                    return false;
                } else {
                    return rac_check_user_roles($user_id, $selected_user_roles);
                }

                //Check Selected Product Match with Each Cart Products. 
            } else {
                $select_products = $email_template['rac_template_seg_selected_product_in_cart'];
                $explode_products = explode(',', $select_products);
                if (empty($select_products)) {
                    return true;
                } else {
                    return rac_check_status_of_products_are_there($each_cart, $explode_products);
                }
            }
        } else {
            return true;
        }
    }

}

function rac_get_no_of_orders_placed($user_id, $email_id) {
    global $wpdb;
    if ($user_id != 0) {
        $count = $wpdb->get_var("SELECT COUNT(*)
                 FROM $wpdb->posts as posts

                 LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id

                 WHERE   meta.meta_key       = '_customer_user'
                 AND     posts.post_type     IN ('" . implode("','", rac_get_order_type_dependencies('order-count')) . "')
                 AND     posts.post_status IN ( '" . implode("','", rac_get_post_status_dependencies()) . " ')
                 AND     meta_value          = $user_id
             ");
    } else {
        $count = $wpdb->get_var("SELECT COUNT(*)
                 FROM $wpdb->posts as posts
                 LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
                 LEFT JOIN {$wpdb->postmeta} AS meta2 ON posts.ID = meta2.post_id    
                 WHERE   posts.post_type     IN ('" . implode("','", rac_get_order_type_dependencies('order-count')) . "')
                 AND     posts.post_status IN ( '" . implode("','", rac_get_post_status_dependencies()) . " ' )
                 AND     meta.meta_value   = $user_id
                 AND     meta.meta_key= '_customer_user'
                 AND     meta2.meta_key= '_billing_email'
                 AND     meta2.meta_value   = '$email_id'
             ");
    }
    return (float) $count;
}

function rac_get_total_amount_spent($user_id, $email_id) {
    global $wpdb;
    if ($user_id != 0) {
        $spent = $spent = $wpdb->get_var("SELECT SUM(meta2.meta_value)
			FROM $wpdb->posts as posts

			LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
			LEFT JOIN {$wpdb->postmeta} AS meta2 ON posts.ID = meta2.post_id

			WHERE   meta.meta_key       = '_customer_user'
			AND     meta.meta_value     = $user_id
			AND     posts.post_type     IN ('" . implode("','", rac_get_order_type_dependencies('reports')) . "')
			AND     posts.post_status   IN ( '" . implode("','", rac_get_post_status_dependencies()) . " ' )
			AND     meta2.meta_key      = '_order_total'
		");
    } else {
        $spent = $wpdb->get_var("SELECT SUM(meta3.meta_value)
			FROM $wpdb->posts as posts

			LEFT JOIN {$wpdb->postmeta} AS meta ON posts.ID = meta.post_id
			LEFT JOIN {$wpdb->postmeta} AS meta2 ON posts.ID = meta2.post_id
                        LEFT JOIN {$wpdb->postmeta} AS meta3 ON posts.ID = meta3.post_id    

			WHERE   meta.meta_key       = '_customer_user'
			AND     meta.meta_value     = $user_id
                        AND     meta2.meta_key      = '_billing_email'
                        AND     meta2.meta_value     = '$email_id'    
			AND     posts.post_type     IN ('" . implode("','", rac_get_order_type_dependencies('reports')) . "')
			AND     posts.post_status   IN ( '" . implode("','", rac_get_post_status_dependencies()) . " ')
			AND     meta3.meta_key      = '_order_total'
		");
    }
    return (float) $spent;
}

function rac_get_abadon_cart_total($each_cart) {
    $cart_array = maybe_unserialize($each_cart->cart_details);
    $total = '';
    if (isset($cart_array['shipping_details'])) {
        unset($cart_array['shipping_details']);
    }
    if (is_array($cart_array) && is_null($each_cart->ip_address)) {
        foreach ($cart_array as $cart) {
            foreach ($cart as $inside) {
                foreach ($inside as $product) {
                    $total += $product['line_subtotal'];
                }
            }
        }
    } elseif (is_array($cart_array)) {
        foreach ($cart_array as $product) {
            if (is_array($product)) {
                $total += $product['line_subtotal'];
            }
        }
    } elseif (is_object($cart_array)) {
        foreach ($cart_array->get_items() as $item) {
            $total += $item['line_subtotal'];
        }
    }
    return (float) $total;
}

function rac_check_status_of_min_max($total, $minimum, $maximum) {
    if ($minimum == '*' && $maximum != '*') {
        if ($total <= $maximum) {
            return true;
        } else {
            return false;
        }
    } elseif ($minimum != '*' && $maximum == '*') {
        if ($total >= $minimum) {
            return true;
        } else {
            return false;
        }
    } else {
        if ($total <= $maximum && $total >= $minimum) {
            return true;
        } else {
            return false;
        }
    }
}

function rac_check_status_of_from_to_date($abandon_time, $from, $to) {
    if ($from == '' && $to == '') {
        return true;
    } else if ($from != '' && $to == '') {
        if ($abandon_time > $from) {
            return true;
        } else {
            return false;
        }
    } else if ($from == '' && $to != '') {
        if ($abandon_time < $to) {
            return true;
        } else {
            return false;
        }
    } else {
        if (($abandon_time > $from) && ($abandon_time < $to)) {
            return true;
        } else {
            return false;
        }
    }
}

function rac_check_status_of_products_are_there($each_cart, $products) {
    $cart_array = maybe_unserialize($each_cart->cart_details);
    if (isset($cart_array['shipping_details'])) {
        unset($cart_array['shipping_details']);
    }
    if (is_array($cart_array) && is_null($each_cart->ip_address)) {
        foreach ($cart_array as $cart) {
            foreach ($cart as $inside) {
                foreach ($inside as $product) {
                    if (check_product_type($product, $products)) {
                        return true;
                    }
                }
            }
        }
    } elseif (is_array($cart_array)) {
        foreach ($cart_array as $product) {
            if (check_product_type($product, $products)) {
                return true;
            }
        }
    } elseif (is_object($cart_array)) {
        foreach ($cart_array->get_items() as $item) {
            if (check_product_type($item, $products)) {
                return true;
            }
        }
    }
    return false;
}

function rac_get_total_eachcart_qty($each_cart) {
    $cart_array = maybe_unserialize($each_cart->cart_details);
    $total = '';
    if (isset($cart_array['shipping_details'])) {
        unset($cart_array['shipping_details']);
    }
    if (is_array($cart_array) && is_null($each_cart->ip_address)) {
        foreach ($cart_array as $cart) {
            foreach ($cart as $inside) {
                foreach ($inside as $product) {
                    $total += $product['quantity'];
                }
            }
        }
    } elseif (is_array($cart_array)) {
        foreach ($cart_array as $product) {
            if (is_array($product)) {
                $total += $product['quantity'];
            }
        }
    } elseif (is_object($cart_array)) {
        foreach ($cart_array->get_items() as $item) {
            $total += $item['qty'];
        }
    }
    return (float) $total;
}

function check_product_type($product, $products) {
    $productid = $product['product_id'];
    $whole_product = get_product($productid);
    if (is_object($whole_product)) {
        if ($whole_product->is_type('simple')) {
            if (in_array($productid, $products)) {
                return true;
            } else {
                return false;
            }
        } else if ($whole_product->is_type('variable')) {
            $variation_id = $product['variation_id'];
            if (in_array($variation_id, $products)) {
                return true;
            } else if ((in_array($productid, $products))) {
                return true;
            } else {
                return false;
            }
        }
    }
}

function rac_check_user_roles($user_id, $selected_user_roles) {
    if ($user_id != '0') {
        $cart_roles = implode(get_userdata($user_id)->roles);
    } else {
        if ($user_id == '0') {
            $cart_roles = 'rac_guest';
        }
    }
    if (in_array($cart_roles, $selected_user_roles)) {
        return true;
    } else {
        return false;
    }
}

function rac_get_post_status_dependencies() {
    if (function_exists('wc_get_order_statuses')) {
        $getpoststatus = array('wc-completed', 'wc-processing');
    } else {
        $getpoststatus = array('publish');
    }
    return $getpoststatus;
}

/*
 * 
 * Get Order type
 * 
 */

function rac_get_order_type_dependencies($parameter) {
    if (function_exists('wc_get_order_types')) {
        $getorderstatus = wc_get_order_types($parameter);
    } else {
        $getorderstatus = array('shop_order');
    }
    return $getorderstatus;
}
