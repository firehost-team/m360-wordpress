<?php

class FP_RAC_Troubleshoot_tab {

    public function __construct() {
        add_action('woocommerce_fprac_settings_tabs_fpracdebug', array($this, 'fp_rac_admin_setting_troubleshoot'));
        add_action('woocommerce_update_options_fpracdebug', array($this, 'fp_rac_update_options_troubleshoot'));
    }

    public static function fp_rac_menu_options_troubleshoot() {
        $defaultval = "webmaster@" . $_SERVER['SERVER_NAME'];
        return apply_filters('woocommerce_fpwcctsingle_settings', array(
            array(
                'name' => __('Troubleshooting', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_troubleshoot',
                'clone_id' => '',
            ),
            array(
                'name' => __('Use Mail Function', 'recoverabandoncart'),
                'desc' => __('Please Select which mail function to use while sending notification', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_trouble_mail',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('mail' => 'mail()', 'wp_mail' => 'wp_mail()'),
                'std' => 'wp_mail',
                'default' => 'wp_mail',
                'clone_id' => 'rac_trouble_mail',
            ),
            array(
                'name' => __('Use Mail Troubleshoot', 'recoverabandoncart'),
                'desc' => __('Please enable this option if you want to send Emails using Fifth Parameter ', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_webmaster_mail',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('webmaster1' => __('Enable', 'recoverabandoncart'), 'webmaster2' => __('Disable', 'recoverabandoncart')),
                'std' => 'webmaster2',
                'default' => 'webmaster2',
                'clone_id' => 'rac_webmaster_mail',
            ),
            array(
                'name' => __('Use Email as Fifth Parameter', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_textarea_mail',
                'std' => $defaultval,
                'default' => $defaultval,
                'type' => 'text',
                'newids' => 'rac_textarea_mail',
                'class' => ''
            ),
            array(
                'name' => __('Include MIME Version 1.0 parameter for Emails send through Recovered Abandoned Cart', 'recoverabandoncart'),
                'id' => 'rac_mime_mail_header_ts',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('block' => __('Enable', 'recoverabandoncart'), 'none' => __('Disable', 'recoverabandoncart')),
                'std' => 'block',
                'default' => 'block',
                'clone_id' => 'rac_mime_mail_header_ts',
            ),
            array(
                'name' => __('Include Reply-To parameter for Emails send through Recovered Abandoned Cart', 'recoverabandoncart'),
                'id' => 'rac_replyto_mail_header_ts',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('block' => __('Enable', 'recoverabandoncart'), 'none' => __('Disable', 'recoverabandoncart')),
                'std' => 'block',
                'default' => 'block',
                'clone_id' => 'rac_replyto_mail_header_ts',
            ),
            array('type' => 'sectionend', 'id' => 'rac_troubleshoot'), //Time Settings END
            array(
                'name' => __('Troubleshoot Performance', 'recoverabandoncart'),
                'desc' => '',
                'type' => 'title',
                'id' => 'rac_troubleshoot_performance',
            ),
            array(
                'name' => __('Load Recover Abandon Cart Script/Styles in ', 'recoverabandoncart'),
                'desc' => __('For Footer of the Site Option is experimental why because if your theme doesn\'t contain wp_footer hook then it won\'t work', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_load_script_styles',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => false,
                'options' => array('wp_head' => __('Header of the Site', 'recoverabandoncart'), 'wp_footer' => __('Footer of the Site (Experimental)', 'recoverabandoncart')),
                'std' => 'wp_head',
                'default' => 'wp_head',
                'clone_id' => 'rac_load_script_styles',
            ),
            array('type' => 'sectionend', 'id' => 'rac_troubleshoot_performance'),
            array(
                'name' => __('Troubleshoot Ajax Chunking', 'recoverabandoncart'),
                'desc' => '',
                'type' => 'title',
                'id' => 'rac_troubleshoot_ajax_chunking',
            ),
            array(
                'name' => __('Chunk Count per Ajax call', 'recoverabandoncart'),
                'desc' => __('Applicable for "Check Previous Orders" tab', 'recoverabandoncart'),
                'id' => 'rac_chunk_count_per_ajax',
                'std' => 10,
                'default' => 10,
                'type' => 'number',
                'step' => 1,
                'newids' => 'rac_chunk_count_per_ajax',
                'desc_tip' => stripslashes("Don't Change the Value unless you need")
            ),
            array('type' => 'sectionend', 'id' => 'rac_troubleshoot_ajax_chunking'),
            array(
                'name' => __('Cron Settings', 'recoverabandoncart'),
                'desc' => '',
                'type' => 'title',
                'id' => 'rac_troubleshoot_cron_format',
            ),
            array(
                'name' => __('What Cron will be used', 'recoverabandoncart'),
                'desc' => '',
                'tip' => '',
                'id' => 'rac_cron_troubleshoot_format',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => false,
                'options' => array('wp_cron' => __('wp_cron', 'recoverabandoncart'), 'server_cron' => __('server_cron', 'recoverabandoncart')),
                'std' => 'wp_cron',
                'default' => 'wp_cron',
                'clone_id' => 'rac_cron_troubleshoot_format',
            ),
            array('type' => 'sectionend', 'id' => 'rac_troubleshoot_cron_format'),
        ));
    }

    public static function fp_rac_admin_setting_troubleshoot() {
        woocommerce_admin_fields(FP_RAC_Troubleshoot_tab::fp_rac_menu_options_troubleshoot());
    }

    public static function fp_rac_update_options_troubleshoot() {
        woocommerce_update_options(FP_RAC_Troubleshoot_tab::fp_rac_menu_options_troubleshoot());
    }

    public static function fprac_trouble_default_settings() {
        global $woocommerce;
        foreach (FP_RAC_Troubleshoot_tab::fp_rac_menu_options_troubleshoot() as $setting)
            if (isset($setting['id']) && isset($setting['std'])) {
                add_option($setting['id'], $setting['std']);
            }
    }

}

new FP_RAC_Troubleshoot_tab();
