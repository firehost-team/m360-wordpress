<?php

class FP_RAC_CartList_Tab {

    public static function fp_rac_adandoncart_admin_display() {
        global $wpdb;
        $array = array();
        $table_name = $wpdb->prefix . 'rac_abandoncart';
        $count_trashed = 0;
        $new = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('trash')", ARRAY_A);
        $count_trashed = count($new);
        $new1 = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash')", ARRAY_A);
        $order_by = get_option('rac_display_cart_list_basedon_asc_desc') != 'no' ? 'ASC' : 'DESC';
        $count_all = count($new1);
        $post_from_date = "";
        $post_to_date = "";
        if (!isset($_GET['extend_cart'])) {
            if (!isset($_GET['section'])) {
                if (isset($_POST['filter_by_date']) || isset($_POST['export_cart_list'])) {
                    $post_from_date = $_POST['rac_from_date'];
                    $post_to_date = $_POST['rac_to_date'];
                    $fromdate = strtotime($post_from_date . ' 00:00:00');
                    $todate = strtotime($post_to_date . ' 23:59:59');
                    if (($post_from_date != "") && ($post_to_date != "")) {
                        $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_abandon_time <=$todate AND cart_status NOT IN('trash') ORDER BY id $order_by", OBJECT);
                        $abandon_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_abandon_time <=$todate AND cart_status IN('ABANDON')", OBJECT));
                        $new_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_abandon_time <=$todate AND cart_status IN('NEW')", OBJECT));
                        $recovered_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_abandon_time <=$todate AND cart_status IN('RECOVERED')", OBJECT));
                    } elseif (($post_from_date != "") && ($post_to_date == "")) {
                        $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_status NOT IN('trash') ORDER BY id $order_by", OBJECT);
                        $abandon_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_status IN('ABANDON')", OBJECT));
                        $new_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_status IN('NEW')", OBJECT));
                        $recovered_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time >=$fromdate AND cart_status IN('RECOVERED')", OBJECT));
                    } elseif (($post_from_date == "") && ($post_to_date != "")) {
                        $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time <=$todate AND cart_status NOT IN('trash') ORDER BY id $order_by", OBJECT);
                        $abandon_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time <=$todate AND cart_status IN('ABANDON')", OBJECT));
                        $new_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time <=$todate AND cart_status IN('NEW')", OBJECT));
                        $recovered_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_abandon_time <=$todate AND cart_status IN('RECOVERED')", OBJECT));
                    } else {
                        $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash') ORDER BY id $order_by", OBJECT);
                        $abandon_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('ABANDON')", OBJECT));
                        $new_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('NEW')", OBJECT));
                        $recovered_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('RECOVERED')", OBJECT));
                    }
                } else {
                    $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status NOT IN('trash') ORDER BY id $order_by", OBJECT);
                    $abandon_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('ABANDON')", OBJECT));
                    $new_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('NEW')", OBJECT));
                    $recovered_count = count($wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('RECOVERED')", OBJECT));
                }
            } else {
                if ($_GET['section'] == 'trash') {
                    $abandon_cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE cart_status IN('trash') ORDER BY id $order_by", OBJECT);
                }
            }
            echo '&nbsp<span><select id="rac_pagination_cart">';
            for ($k = 1; $k <= 20; $k++) {

                if ($k == 10) {
                    echo '<option value="' . $k . '" selected="selected">' . $k . '</option>';
                } else {
                    echo '<option value="' . $k . '">' . $k . '</option>';
                }
            }
            echo '</select></span>';

            echo '&nbsp<label>' . __("Search", "recoverabandoncart") . '</label><input type="text" name="rac_temp_search" id="rac_temp_search">';
            ?>
            <br>
            <br>
            <a href="<?php echo esc_url_raw(remove_query_arg('section', get_permalink())); ?>"><?php _e("All ", 'recoverabandoncart'); ?></a><?php echo "($count_all)"; ?>
            |
            <a href="<?php echo esc_url_raw(add_query_arg('section', 'trash', get_permalink())); ?>"><?php _e("Trash ", 'recoverabandoncart'); ?></a><?php echo "($count_trashed)"; ?>
            <style type="text/css">
                .rac_tool_info .tooltip {
                    background: #1496bb;
                    color: #fff;
                    opacity: 0;
                }
                /* .rac_tool_info:hover .tooltip {
                    opacity: 1;
                }*/

            </style>
            <script type='text/javascript'>
                jQuery(function () {
                    jQuery('.rac_tool_info:not(.rac_content_get)').tipTip({'content': 'Click here to Edit Email ID for Guest'});
                });</script>
            <?php
            if (!isset($_GET['section'])) {

                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for="fromDate">' . __("From date:", "recoverabandoncart") . '</label> <input type="text"  class="rac_date" id="rac_from_date"  name="rac_from_date" value="' . $post_from_date . '" />';

                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for="toDate">' . __("To date:", "recoverabandoncart") . '</label> <input type="text" class="rac_date" id="rac_to_date" name="rac_to_date" value="' . $post_to_date . '" />';

                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="filter_by_date button-primary" id="filter_by_date" name="filter_by_date" value=" ' . __("Filter", "recoverabandoncart") . '" />';

                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="export_cart_list button-primary" id="export_cart_list" name="export_cart_list" value=" ' . __("Export CSV", "recoverabandoncart") . ' " />';

                if (isset($_POST['filter_by_date'])) {
                    echo '<br> ' . __('Results by Date Filter', 'recoverabandoncart') . '<br>'
                    . ' All (' . count($abandon_cart_list) . ')&nbsp';
                    echo ' Abandon (' . $abandon_count . ')&nbsp';
                    echo ' New (' . $new_count . ')&nbsp';
                    echo ' Recovered (' . $recovered_count . ')';
                }
                $array = array();
                foreach ($abandon_cart_list as $each_cart_list) {
                    $each_cart_list1 = (array) $each_cart_list;
                    $new_array = array();
                    foreach ($each_cart_list1 as $key => $cart_list) {
                        if ($key != 'cart_details') {
                            $new_array[$key] = $cart_list;
                        } else {
                            ob_start();
                            echo RecoverAbandonCart::rac_cart_details($each_cart_list);
                            $string = ob_get_clean();
                            $string1 = str_replace(' ', '', html_entity_decode(strip_tags($string)));
                            $new_array[$key] = $string1;
                        }
                    }
                    array_push($array, $new_array);
                }
                if (isset($_POST['export_cart_list'])) {
                    ob_end_clean();
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=rac_cartlist" . date_i18n("Y-m-d H:i:s") . ".csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $output = fopen("php://output", "w");
                    $row_heading = array('id', 'cart_details', 'user_id', 'email_id', 'cart_abandon_time', 'cart_status', 'mail_template_id', 'ip_address', 'link_status', 'sending_status', 'wpml_lang', 'placed_order', 'completed', 'old_status');
                    fputcsv($output, $row_heading); // here you can change delimiter/enclosure
                    foreach ($array as $row) {
                        fputcsv($output, $row); // here you can change delimiter/enclosure
                    }
                    fclose($output);
                    exit();
                }
            }

            echo '<table class="rac_email_template_table_abandon table" data-page-size="10" data-filter="#rac_temp_search" data-filter-minimum="1">';
            echo '<thead>
		<tr>
			<th data-type="numeric">' . __('ID', 'recoverabandoncart') . '</th>
			<th data-hide="phone">' . __('Cart Details / Cart Total', 'recoverabandoncart') . '</th>
			<th>' . __('UserName / First Last Name', 'recoverabandoncart') . '</th>
                        <th>' . __('Email ID / Phone Number', 'recoverabandoncart') . '</th>
                        <th data-type="numeric">' . __('Abandoned Date/Time', 'recoverabandoncart') . '</th>
                        <th>' . __('Status', 'recoverabandoncart') . '</th>
                        <th>' . __('Email Template / Email Status / Cart Link in Email', 'recoverabandoncart') . '</th>
                        <th data-type="numeric">' . __('Recovered Order ID', 'recoverabandoncart') . '</th>
                        <th >' . __('Coupon Used', 'recoverabandoncart') . '</th>
                        <th >' . __('Payment Status', 'recoverabandoncart') . '</th>
                        <th>' . __('Mail Sending', 'recoverabandoncart') . '</th>';

            $main_trash_data = 'trash';
            // Check the Status
            if (!isset($_GET['section'])) {
                $main_trash_data = 'trash';
            } else {
                if ($_GET['section'] == 'trash') {
                    $main_trash_data = 'permanent';
                }
            }

            echo '<th class="rac_small_col" data-sort-ignore="true"><a href="#" id="rac_page_select">' . __('Page Select', 'recoverabandoncart') . '</a>&nbsp/&nbsp<a href="#" id="rac_page_deselect">' . __('Page Deselect', 'recoverabandoncart') . '</a>&nbsp</br>&nbsp<a href="#" id="rac_sel">' . __('Select All', 'recoverabandoncart') . '</a>&nbsp/&nbsp <a href="#" id="rac_desel">' . __('Deselect All', 'recoverabandoncart') . '</a>&nbsp'
            . '<a href="#" class="rac_selected_del button" data-deletion=' . $main_trash_data . '>' . __('Delete Selected', 'recoverabandoncart') . '</a>&nbsp';
            if (!isset($_GET['section'])) {
                echo '<a href="#" id="rac_selected_mail" class="button button-primary">' . __('Send Mail for Selected', 'recoverabandoncart') . '</a>';
            } else {
                ?>
                <a href="#" class="rac_selected_del button" data-deletion="restore"><?php _e("Restore Selected", "recoverabandoncart"); ?></a>
                <?php
            }
            echo '</th>
		</tr>
	</thead>';
            ?>
            <tbody>
                <?php
                foreach ($abandon_cart_list as $each_list) {
                    ?>
                    <tr>
                        <td data-value="<?php echo $each_list->id; ?>"><?php echo $each_list->id; ?></td>
                        <td>
                            <?php
                            $admin_url = admin_url('admin.php');
                            $new_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpractable', 'extend_cart' => $each_list->id), $admin_url));
                            echo '<a style="text-decoration: none;" href="' . $new_template_url . '">' . RecoverAbandonCart::rac_cart_details($each_list) . '</a>';
                            ?>
                        </td>
                        <td>
                            <?php
                            $user_info = get_userdata($each_list->user_id);
                            $cart_array = maybe_unserialize($each_list->cart_details);
                            if (is_object($user_info)) {
                                echo $user_info->user_login;
                                echo " / $user_info->user_firstname $user_info->user_lastname";
                            } elseif ($each_list->user_id == '0') {
                                if (is_array($cart_array)) {
                                    //for cart captured at checkout(GUEST)
                                    $first_name = $cart_array['first_name'];
                                    $last_name = $cart_array['last_name'];
                                    $guest_first_last = " / $first_name $last_name";

                                    unset($cart_array['visitor_mail']);
                                    unset($cart_array['first_name']);
                                    unset($cart_array['last_name']);
                                    if (isset($cart_array['visitor_phone'])) {
                                        unset($cart_array['visitor_phone']);
                                    }
                                    if (isset($cart_array['shipping_details'])) {
                                        unset($cart_array['shipping_details']);
                                    }
                                } elseif (is_object($cart_array)) { // For Guest
                                    $guest_first_last = " / $cart_array->billing_first_name $cart_array->billing_last_name";
                                }
                                _e('Guest', 'recoverabandoncart');
                                echo $guest_first_last;
                            } elseif ($each_list->user_id == 'old_order') {

                                $old_order_cart_ob = maybe_unserialize($each_list->cart_details);
                                if (is_array($old_order_cart_ob)) {
                                    //for cart captured at checkout(GUEST)
                                    $first_name = $old_order_cart_ob['first_name'];
                                    $last_name = $old_order_cart_ob['last_name'];
                                    $guest_first_last = " / $first_name $last_name";

                                    unset($old_order_cart_ob['visitor_mail']);
                                    unset($old_order_cart_ob['first_name']);
                                    unset($old_order_cart_ob['last_name']);
                                    if (isset($old_order_cart_ob['visitor_phone'])) {
                                        unset($old_order_cart_ob['visitor_phone']);
                                    }
                                    if (isset($old_order_cart_ob['shipping_details'])) {
                                        unset($old_order_cart_ob['shipping_details']);
                                    }
                                } elseif (is_object($old_order_cart_ob)) { // For Guest
                                    $guest_first_last = " / $old_order_cart_ob->billing_first_name $old_order_cart_ob->billing_last_name";
                                }
                                $user_inf = get_userdata($old_order_cart_ob->user_id);
                                if (is_object($user_inf)) {
                                    echo $user_inf->user_login;
                                    echo " / $user_inf->user_firstname $user_inf->user_lastname";
                                } else {
                                    _e('Guest', 'recoverabandoncart');
                                    echo $guest_first_last;
                                }
                            }
                            ?></td>
                        <td>
                            <?php
                            if ('0' == $each_list->user_id) {
                                $details = maybe_unserialize($each_list->cart_details);
                                if (is_object($details)) {
                                    ?> <div class="rac_tool_info"><p class="rac_edit_option" data-id="<?php echo $each_list->id; ?>" >
                                    <?php
                                    echo $details->billing_email; // Order Object. Works for both old order and rac captured order
                                    ?></p><div class="tooltip"><?php _e("Double Click to Change an Editable", "recoverabandoncart"); ?></div></div><?php
                                            echo '</br>&nbsp' . $details->billing_phone;
                                        } elseif (is_array($details)) {
                                            ?><div class="rac_tool_info"><p class="rac_edit_option" data-id="<?php echo $each_list->id; ?>">
                                            <?php
                                            echo $details['visitor_mail']; //checkout order
                                            ?></p><div class="tooltip"><?php _e("Double Click to Change an Editable", "recoverabandoncart"); ?></div></div><?php
                                            echo "</br>&nbsp";
                                            if (isset($details['visitor_phone'])) {
                                                echo $details['visitor_phone'];
                                            } else {
                                                echo '-';
                                            }
                                        }
                                    } elseif ($each_list->user_id == 'old_order') {
                                        $old_order_cart_ob = maybe_unserialize($each_list->cart_details);
                                        $user_inf = get_userdata($old_order_cart_ob->user_id);
                                        if (is_object($user_inf)) {
                                            echo $user_inf->user_email;
                                            echo " / $user_inf->billing_phone";
                                        } else {
                                            _e('Guest', 'recoverabandoncart');
                                            if (isset($old_order_cart_ob->billing_email)) {
                                                echo " /" . $old_order_cart_ob->billing_email;
                                            } else {
                                                echo '-';
                                            }
                                        }
                                    } else {
                                        $user_infor = get_userdata($each_list->user_id);

                                        if (is_object($user_infor)) {
                                            echo $user_infor->user_email;
                                            echo '</br> &nbsp' . $user_infor->billing_phone;
                                        }
                                    }
                                    ?>
                        </td>
                        <td data-value="<?php echo $each_list->cart_abandon_time; ?>">
                            <?php echo date(get_option('date_format'), $each_list->cart_abandon_time) . '/' . date(get_option('time_format'), $each_list->cart_abandon_time); ?>
                        </td>
                        <td><?php echo $each_list->cart_status == 'trash' ? __('Trashed', 'recoverabandoncart') : $each_list->cart_status; ?></td>
                        <td>
                            <?php
                            $mail_sent = maybe_unserialize($each_list->mail_template_id);
                            $email_table_name_clicked = $wpdb->prefix . 'rac_templates_email';
                            $email_template_all = $wpdb->get_results("SELECT * FROM $email_table_name_clicked ORDER BY id ASC");
                            foreach ($email_template_all as $check_all_email_temp) {
                                echo $check_all_email_temp->template_name;
                                //Mail Sent
                                if (!is_null($mail_sent)) {
                                    if (in_array($check_all_email_temp->id, (array) $mail_sent)) {
                                        echo ' /' . __("Email Sent", "recoverabandoncart") . '';
                                    } else {
                                        echo ' /' . __("Email Not Sent", "recoverabandoncart") . '';
                                    }
                                } else {
                                    echo ' /' . __("Email Not Sent", "recoverabandoncart") . '';
                                }
                                //Mail Sent END
                                //Link Clicked
                                if (!empty($each_list->link_status)) {
                                    $mails_clicked = maybe_unserialize($each_list->link_status);
                                    if (in_array($check_all_email_temp->id, (array) $mails_clicked)) {
                                        echo ' /' . __("Cart Link Clicked", "recoverabandoncart") . '';
                                        echo '<br>';
                                    } else {
                                        echo ' /' . __("Cart Link Not Clicked", "recoverabandoncart") . '';
                                        echo '<br>';
                                    }
                                } else {
                                    echo ' /' . __("Cart Link Not Clicked", "recoverabandoncart") . '';
                                    echo '<br>';
                                }
                                //Link Clicked END
                            }
                            ?>
                        </td>
                        <td data-value="<?php echo $each_list->placed_order; ?>"><?php echo (!is_null($each_list->placed_order) ? ' #' . $each_list->placed_order . '' : __('Not Yet', 'recoverabandoncart')); ?></td>
                        <td>
                            <?php
                            if ($each_list->cart_status == 'RECOVERED') {
                                $coupon_code = get_option('abandon_time_of' . $each_list->id);
                                $order = new WC_Order($each_list->placed_order);
                                $coupons_used = $order->get_used_coupons();
                                if (!empty($coupons_used)) {
                                    if (in_array($coupon_code, $order->get_used_coupons())) {
                                        echo $coupon_code . ' - ';
                                        _e('Success', 'recoverabandoncart');
                                    } else {
                                        _e('Not Used', 'recoverabandoncart');
                                    }
                                } else {
                                    _e('Not Used', 'recoverabandoncart');
                                }
                            } else {
                                _e('Not Yet', 'recoverabandoncart');
                            }
                            ?>
                        </td>
                        <td><?php echo (!empty($each_list->completed) ? __('Completed', 'recoverabandoncart') : __('Not Yet', 'recoverabandoncart')); ?></td>
                        <td>
                            <?php
                            if ($each_list->cart_status != 'trash') {
                                if (empty($each_list->completed)) {
                                    //check if order completed,if completed don't show mail sending button'
                                    ?>
                                    <a href="#" class="button rac_mail_sending" data-racmoptid="<?php echo $each_list->id; ?>" data-currentsate="<?php echo $each_list->sending_status == 'SEND' ? 'SEND' : 'DONT' ?>"><?php
                                        if ($each_list->sending_status == 'SEND') {
                                            _e('Stop Mailing', 'recoverabandoncart');
                                        } else {
                                            _e('Start Mailing', 'recoverabandoncart');
                                        }
                                        ?></a>
                                    <p style="margin-top:10px;"><a href="#" class="button rac_manual_recovered" data-racmrid="<?php echo $each_list->id; ?>"><?php _e('Mark as Recover', 'recoverabandoncart'); ?></a> </p><?php
                                } else {
                                    _e('Recovered', 'recoverabandoncart');
                                }
                            } else {
                                _e("Trashed", "recoverabandoncart");
                            }
                            ?></td>
                        <td class="bis_mas_small_col">
                            <input type="checkbox" class="rac_checkboxes" data-racid="<?php echo $each_list->id; ?>"/>
                            <a href="#" class="button rac_check_indi" data-deletion="<?php echo $main_trash_data; ?>" data-racdelid="<?php echo $each_list->id; ?>"><?php echo $each_list->cart_status != 'trash' ? __("Delete this Row", "recoverabandoncart") : __("Delete Permanently", "recoverabandoncart"); ?></a>
                            <?php if ($each_list->cart_status == 'trash') { ?>
                                <a href="#" class="button rac_check_indi" data-deletion="restore" data-racdelid="<?php echo $each_list->id; ?>"><?php _e('Restore', 'recoverabandoncart'); ?></a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
                echo '</tbody>
            <tfoot>
		<tr>
			<td colspan="12">
				<div class="pagination pagination-centered hide-if-no-paging"></div>
			</td>
		</tr>
	</tfoot></table><style>.footable > tbody > tr > td,.footable > thead > tr > th, .footable > thead > tr > td{text-align:center;}</style>';
                ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    //Manual Mail redirection
                    jQuery('#rac_selected_mail').click(function (e) {
                        e.preventDefault();
                        var selection_for_mail = new Array();
                        jQuery('.rac_checkboxes').each(function (num) {
                            if (jQuery(this).prop('checked')) {
                                selection_for_mail.push(jQuery(this).data('racid'));
                            }
                        });
                        console.log(selection_for_mail);
                        var url_without_data = "<?php echo esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail'), admin_url('admin.php'))); ?>";
                        var url_data = url_without_data + "&rac_send_email=" + selection_for_mail;
                        console.log(url_without_data);
                        console.log(url_data);
                        if (selection_for_mail.length > 0) {
                            window.location.replace(url_data);
                        } else {
                            alert("<?php _e("Select a row to mail", "recoverabandoncart"); ?>");
                        }

                        //window.location.replace("<?php echo add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracemail', 'rac_send_email' => 'template'), admin_url('admin.php')); ?>");
                    });
                });
                //save editable table
                jQuery(document).ready(function () {
                    jQuery(".rac_edit_option").dblclick(function (e) {
                        jQuery(this).next().remove();
                        jQuery(this).parent().removeAttr('class');
                        var p = jQuery(this).text();
                        var value = jQuery('<div class="raceditemail"><textarea class="rac_content_get" name="one" style="width:200px;height:100px;">' + p + '</textarea></br><input class="rac_save" type="button" value="<?php _e("save", "recoverabandoncart"); ?>"/></div>');
                        var one = jQuery('.rac_content_get').val();
                        var id = jQuery(this).attr('data-id');
                        jQuery('.rac_content_get').parent().html(one);
                        jQuery(this).empty();
                        jQuery(this).append(value);
                        jQuery(".rac_save").click(function () {
                            jQuery(".rac_save").prop("disabled", true);
                            var email = jQuery('.rac_content_get').val();
                            var data = {
                                action: "edit_value_update_now",
                                email: email,
                                id: id
                            }

                            jQuery.ajax({
                                type: "POST",
                                url: ajaxurl,
                                data: data
                            }).done(function (response) {
                                jQuery(".rac_save").prop("disabled", false);
                                var p = jQuery(this).text();
                                var value = jQuery('<div class="raceditemail"><textarea class="rac_content_get" name="one" style="width:200px;height:100px;">' + p + '</textarea></br><input class="rac_save" type="button" value="<?php _e("save", "recoverabandoncart"); ?>"/></div>');
                                var one = jQuery('.rac_content_get').val();
                                var id = jQuery(this).attr('data-id');
                                jQuery('.rac_content_get').parent().html(one);
                                jQuery(this).parent().parent().parent().addClass('rac_tool_info');
                            });
                        });
                    });
                });</script>
            <?php
        } else {
            $cart_id = $_GET['extend_cart'];
            $table_name = $wpdb->prefix . 'rac_abandoncart';
            $cart_list = $wpdb->get_results("SELECT * FROM $table_name WHERE id = $cart_id");
            foreach ($cart_list as $eachlist) {
                echo FP_RAC_Polish_Product_Info::rac_show_cart_products_brief($eachlist);
            }
            $admin_url = admin_url('admin.php');
            if (isset($_SERVER['HTTP_REFERER'])) {
                if (strpos($_SERVER['HTTP_REFERER'], 'section=trash') !== false) {
                    $new_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpractable', 'section' => 'trash'), $admin_url));
                } else {
                    $new_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpractable'), $admin_url));
                }
            } else {
                $new_template_url = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpractable'), $admin_url));
            }
            echo '<br><a href="' . $new_template_url . '" style="text-decoration:none"><input class="button-primary" type="button" value="' . __('Back to Cart List', 'recoverabandoncart') . '"</a>';
        }
    }

}
