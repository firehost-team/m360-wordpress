<?php

class FP_RAC_Coupon_Tab {

    public function __construct() {
        add_action('woocommerce_fprac_settings_tabs_fpraccoupon', array($this, 'fp_rac_admin_setting_coupon'));
        add_action('woocommerce_update_options_fpraccoupon', array($this, 'fp_rac_update_options_coupon'));
        add_action('woocommerce_update_options_fpraccoupon', array($this, 'save_product_to_exclude'));
        add_action('woocommerce_update_options_fpraccoupon', array($this, 'save_product_to_include'));
        add_action('woocommerce_admin_field_rac_coupon_exclude_products', array($this, 'rac_select_product_to_exclude'));
        add_action('woocommerce_admin_field_rac_coupon_include_products', array($this, 'rac_select_product_to_include'));
    }

    public static function fp_rac_menu_options_coupon_gen() {
        $categorylist = array();
        $categoryname = array();
        $categoryid = array();
        $particularcategory = get_terms('product_cat');
        if (!is_wp_error($particularcategory)) {
            if (!empty($particularcategory)) {
                if (is_array($particularcategory)) {
                    foreach ($particularcategory as $category) {
                        $categoryname[] = $category->name;
                        $categoryid[] = $category->term_id;
                    }
                }
                $categorylist = array_combine((array) $categoryid, (array) $categoryname);
            }
        }
        return apply_filters('woocommerce_fpraccoupon_settings', array(
            array(
                'name' => __('Coupon Code', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_coupon',
                'clone_id' => '',
            ),
            array(
                'name' => __('Prefix Text of Coupon Code', 'recoverabandoncart'),
                'desc' => __('Select Prefix Text in Coupon Code', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_prefix_coupon',
                'css' => '',
                'desc_tip' => true,
                'type' => 'select',
                'options' => array(
                    '1' => __('Default', 'recoverabandoncart'),
                    '2' => __('Custom', 'recoverabandoncart'),
                ),
                'std' => '1',
                'default' => '1',
                'clone_id' => 'rac_prefix_coupon',
            ),
            array(
                'name' => __('Custom Prefix Text of Coupon Code', 'recoverabandoncart'),
                'desc' => __('Enter Custom Prefix Text for Coupon Code', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_manual_prefix_coupon_code',
                'css' => 'rac_manual_prefix',
                'desc_tip' => true,
                'type' => 'text',
                'std' => '',
                'default' => '',
                'clone_id' => 'rac_manual_prefix_coupon_code',
            ),
            array(
                'name' => __('Type of Discount', 'recoverabandoncart'),
                'desc' => __('Please Select which type of discount should be applied', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_coupon_type',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('fixed_cart' => __('Amount', 'recoverabandoncart'), 'percent' => __('Percentage', 'recoverabandoncart')),
                'std' => 'fixed_cart',
                'default' => 'fixed_cart',
                'clone_id' => 'rac_coupon_type',
            ),
            array(
                'name' => __('Value', 'recoverabandoncart'),
                'desc' => __('Enter the value to reduce in currency or % based on the Type of Discount Selected without any Symbols', 'recoverabandoncart'),
                'tip' => '',
                'desc_tip' => true,
                'id' => 'rac_coupon_value',
                'std' => "",
                'default' => "",
                'type' => 'text',
                'newids' => 'rac_coupon_value',
                'class' => ''
            ),
            array(
                'name' => __('Validity in Days', 'recoverabandoncart'),
                'desc' => __('Enter a value(days in number) for how long the Coupon should be Active', 'recoverabandoncart'),
                'desc_tip' => true,
                'id' => 'rac_coupon_validity',
                'std' => "7",
                'default' => "7",
                'type' => 'text',
                'newids' => 'rac_coupon_validity',
                'class' => ''
            ),
            array(
                'name' => __('Minimum Amount for Coupon Usage', 'recoverabandoncart'),
                'id' => 'rac_minimum_spend',
                'std' => '',
                'default' => '',
                'type' => 'text',
                'newids' => 'rac_minimum_spend',
                'class' => '',
            ),
            array(
                'name' => __('Maximum Amount for Coupon Usage', 'recoverabandoncart'),
                'id' => 'rac_maximum_spend',
                'std' => '',
                'default' => '',
                'type' => 'text',
                'newids' => 'rac_maximum_spend',
                'class' => '',
            ),
            array(
                'name' => __('Individual use only', 'recoverabandoncart'),
                'id' => 'rac_individual_use_only',
                'desc' => __('Check this box if the coupon cannot be used in conjunction with other coupons.', 'recoverabandoncart'),
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_individual_use_only',
                'class' => '',
            ),
            array(
                'type' => 'rac_coupon_include_products',
            ),
            array(
                'type' => 'rac_coupon_exclude_products',
            ),
            array(
                'name' => __('Select Category', 'recoverabandoncart'),
                'desc' => __('Select the Categories to which the coupons from abandoned cart emails can be applied', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_select_category_to_enable_redeeming',
                'class' => 'rac_select_category_to_enable_redeeming',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'multiselect',
                'newids' => 'rac_select_category_to_enable_redeeming',
                'options' => $categorylist,
                'desc_tip' => true,
            ),
            array(
                'name' => __('Exclude Category', 'recoverabandoncart'),
                'desc' => __('Select the Categories to which the coupons from abandoned cart emails cannot be applied', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_exclude_category_to_enable_redeeming',
                'class' => 'rac_exclude_category_to_enable_redeeming',
                'css' => 'min-width:350px',
                'std' => '',
                'type' => 'multiselect',
                'newids' => 'rac_exclude_category_to_enable_redeeming',
                'options' => $categorylist,
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'rac_coupon'), //Coupon Settings END
            array(
                'name' => __('Coupon Code Deletion', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_coupon_deletion',
                'clone_id' => '',
            ),
            array(
                'name' => __('Delete Coupons after Used', 'recoverabandoncart'),
                'desc' => __('Delete Coupons which are automatically created by Recover Abandoned Cart that are Used', 'recoverabandoncart'),
                'id' => 'rac_delete_coupon_after_use',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_delete_coupon_after_use',
            ),
            array(
                'name' => __('Delete Coupons after Expired', 'recoverabandoncart'),
                'desc' => __('Delete Coupons which are automatically created by Recover Abandoned Cart that are Expired', 'recoverabandoncart'),
                'id' => 'rac_delete_coupon_expired',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_delete_coupon_expired',
            ),
            array('type' => 'sectionend', 'id' => 'rac_coupon_deletion'), //Coupon Settings END
        ));
    }

    public static function fp_rac_admin_setting_coupon() {
        woocommerce_admin_fields(FP_RAC_Coupon_Tab::fp_rac_menu_options_coupon_gen());
    }

    public static function fp_rac_update_options_coupon() {
        woocommerce_update_options(FP_RAC_Coupon_Tab::fp_rac_menu_options_coupon_gen());
    }

    public static function fprac_coupon_default_settings() {
        global $woocommerce;

        foreach (FP_RAC_Coupon_Tab::fp_rac_menu_options_coupon_gen() as $setting)
            if (isset($setting['id']) && isset($setting['std'])) {
                add_option($setting['id'], $setting['std']);
            }
    }

    /*
     * Function to save the selected products to exclude
     */

    public static function save_product_to_exclude() {
        update_option('rac_exclude_products_in_coupon', $_POST['rac_exclude_products_in_coupon']);
    }

    /*
     * Function to save select products to include
     */

    public static function save_product_to_include() {
        update_option('rac_include_products_in_coupon', $_POST['rac_include_products_in_coupon']);
    }

    /*
     * Function to select products to exclude
     */

    public static function rac_select_product_to_exclude() {
        echo rac_common_function_to_multi_select_for_product_search('rac_exclude_products_in_coupon', 'Exclude Products from using Coupon');
    }

    /*
     * Function to select products to include
     */

    public static function rac_select_product_to_include() {

        echo rac_common_function_to_multi_select_for_product_search('rac_include_products_in_coupon', 'Products');
    }

}

new FP_RAC_Coupon_Tab();

