<?php

class FP_RAC_General_Tab {

    public function __construct() {
        add_action('woocommerce_fprac_settings_tabs_fpracgenral', array($this, 'fp_rac_admin_setting_general'));
        add_action('woocommerce_update_options_fpracgenral', array($this, 'fp_rac_update_options_general'));
        add_action('woocommerce_admin_field_rac_exclude_users_list_for_restrict_in_cart_list', array($this, 'rac_selected_users_restrict_option'));
    }

    public static function fp_rac_menu_options_general() {
        global $wp_roles;
        foreach ($wp_roles->role_names as $key => $value) {
            $userrole[] = $key;
            $username[] = $value;
        }

        $user_role = array_combine((array) $userrole, (array) $username);
        $admin_mail = get_option('admin_email');
        global $woocommerce, $product, $wpdb;
        if (function_exists('wc_get_order_statuses')) {
            $order_list_keys = array_keys(wc_get_order_statuses());
            $order_list_values = array_values(wc_get_order_statuses());
            $orderlist_replace = str_replace('wc-', '', $order_list_keys);
            $orderlist_combine = array_combine($orderlist_replace, $order_list_values);
        } else {
            $order_status = (array) get_terms('shop_order_status', array('hide_empty' => 0, 'orderby' => 'id'));
            foreach ($order_status as $value) {
                $status_name[] = $value->name;
                $status_slug[] = $value->slug;
            }
            $orderlist_combine = array_combine($status_slug, $status_name);
        }
        return apply_filters('woocommerce_fpwcctsingle_settings', array(
            array(
                'name' => __('Time Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_time_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Abandon Cart Time Type for Members', 'recoverabandoncart'),
                'desc' => __('Please Select whether the time should be in Minutes/Hours/Days for Members', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cart_time_type',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('minutes' => __('Minutes', 'recoverabandoncart'), 'hours' => __('Hours', 'recoverabandoncart'), 'days' => __('Days', 'recoverabandoncart')),
                'std' => 'hours',
                'default' => 'hours',
                'clone_id' => 'rac_abandon_cart_time_type',
            ),
            array(
                'name' => __('Abandon Cart Time for Members', 'recoverabandoncart'),
                'desc' => __('Please Enter time after which the cart should be considered as abandon for Members', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cart_time',
                'css' => 'min-width:150px;',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '1',
                'default' => '1',
                'clone_id' => 'rac_abandon_cart_time',
            ),
            array(
                'name' => __('Abandon Cart Time Type for Guest', 'recoverabandoncart'),
                'desc' => __('Please Select whether the time should be in Minutes/Hours/Days for Guest', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cart_time_type_guest',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('minutes' => __('Minutes', 'recoverabandoncart'), 'hours' => __('Hours', 'recoverabandoncart'), 'days' => __('Days', 'recoverabandoncart')),
                'std' => 'hours',
                'default' => 'hours',
                'clone_id' => 'rac_abandon_cart_time_type_guest',
            ),
            array(
                'name' => __('Abandon Cart Time for Guest', 'recoverabandoncart'),
                'desc' => __('Please Enter time after which the cart should be considered as abandon for guest', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cart_time_guest',
                'css' => 'min-width:150px;',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '1',
                'default' => '1',
                'clone_id' => 'rac_abandon_cart_time_guest',
            ),
            array('type' => 'sectionend', 'id' => 'rac_time_settings'), //Time Settings END
            array(
                'name' => __('Mail Cron Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_cron_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Mail Cron Time Type', 'recoverabandoncart'),
                'desc' => __('Please Select whether the time should be in Minutes/Hours/Days', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cart_cron_type',
                'css' => 'min-width:150px;',
                'type' => 'select',
                'desc_tip' => true,
                'options' => array('minutes' => __('Minutes', 'recoverabandoncart'), 'hours' => __('Hours', 'recoverabandoncart'), 'days' => __('Days', 'recoverabandoncart')),
                'std' => 'hours',
                'default' => 'hours',
                'clone_id' => 'rac_abandon_cart_cron_type',
            ),
            array(
                'name' => __('Mail Cron Time', 'recoverabandoncart'),
                'desc' => __('Please Enter time after which Email cron job should run', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_abandon_cron_time',
                'css' => 'min-width:150px;',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '12',
                'default' => '12',
                'clone_id' => 'rac_abandon_cron_time',
            ),
            array('type' => 'sectionend', 'id' => 'rac_cron_settings'), //Cron Settings END
            array(
                'name' => __('Recover Status Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_mailcontrol_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('When User\'s place the order change all the New/Abandon Cart List to Recovered Status', 'recoverabandoncart'),
                'desc' => __('Recover Cart List based on Order Status', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cartlist_new_abandon_recover',
                'class' => 'rac_cartlist_new_abandon_recover',
                'css' => '',
                'type' => 'checkbox',
                'std' => 'yes',
                'default' => 'yes',
                'newids' => 'rac_cartlist_new_abandon_recover',
                'clone_id' => 'rac_cartlist_new_abandon_recover',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Allow Manual Orders to Recover Cart List', 'recoverabandoncart'),
                'desc' => __('Enable this Option will help to recover cart list based on manually created orders.', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cartlist_new_abandon_recover_by_manual_order',
                'class' => 'rac_cart_depends_parent_new_abandon_option',
                'css' => '',
                'type' => 'checkbox',
                'std' => 'yes',
                'default' => 'yes',
                'newids' => 'rac_cartlist_new_abandon_recover_by_manual_order',
                'clone_id' => 'rac_cartlist_new_abandon_recover_by_manual_order',
                'desc_tip' => true,
            ),
            array(
                'name' => __('New Status to Recovered Status', 'recoverabandoncart'),
                'desc' => __('Based on Order Status change New Status to Recovered Status', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cartlist_change_from_new_to_recover',
                'class' => 'rac_cart_depends_parent_new_abandon_option',
                'css' => '',
                'type' => 'checkbox',
                'std' => 'no',
                'default' => 'no',
                'newids' => 'rac_cartlist_change_from_new_to_recover',
                'clone_id' => 'rac_cartlist_change_from_new_to_recover',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Abandon Status to Recovered Status', 'recoverabandoncart'),
                'desc' => __('Based on Order Status change Abandon Status to Recovered Status', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cartlist_change_from_abandon_to_recover',
                'class' => 'rac_cart_depends_parent_new_abandon_option',
                'css' => '',
                'type' => 'checkbox',
                'std' => 'yes',
                'default' => 'yes',
                'newids' => 'rac_cartlist_change_from_abandon_to_recover',
                'clone_id' => 'rac_cartlist_change_from_abandon_to_recover',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Cart List become Recovered when Order Status', 'recoverabandoncart'),
                'desc' => __('Based on these Order Status Cart List become Recovered', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_mailcartlist_change',
                'class' => 'rac_mailcartlist_change',
                'css' => 'min-width:153px',
                'type' => 'multiselect',
                'options' => $orderlist_combine,
                'std' => array('completed', 'processing'),
                'default' => array('completed', 'processing'),
                'newids' => 'rac_mailcartlist_change',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'rac_mailcontrol_settings'), // Settings END
            array(
                'name' => __('Notification Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_notification_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Enable Email Notification for Admin when Cart is Recovered', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_admin_cart_recovered_noti',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_admin_cart_recovered_noti',
            ),
            array(
                'name' => __('Admin Email ID', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_admin_email',
                'std' => $admin_mail,
                'default' => $admin_mail,
                'type' => 'text',
                'newids' => 'rac_admin_email',
                'class' => 'admin_notification'
            ),
            array(
                'name' => __('Notification Sender Option', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_recovered_sender_opt',
                'std' => "woo",
                'default' => "woo",
                'type' => 'radio',
                'newids' => 'rac_recovered_sender_opt',
                'class' => 'admin_sender_opt',
                'options' => array('woo' => __('WooCommerce', 'recoverabandoncart'), 'local' => __('Local (HTML Email will be sent)', 'recoverabandoncart')),
                'class' => 'admin_notifi_sender_opt'
            ),
            array(
                'name' => __('Notification From Name', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_recovered_from_name',
                'std' => "",
                'default' => "",
                'type' => 'text',
                'newids' => 'rac_recovered_from_name',
                'class' => 'local_senders admin_notification'
            ),
            array(
                'name' => __('Notification From Email', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_recovered_from_email',
                'std' => "",
                'default' => "",
                'type' => 'text',
                'newids' => 'rac_recovered_from_email',
                'class' => 'local_senders admin_notification'
            ),
            array(
                'name' => __('Notification Email Subject', 'recoverabandoncart'),
                'desc' => '',
                'id' => 'rac_recovered_email_subject',
                'std' => "A cart has been Recovered",
                'default' => "A cart has been Recovered",
                'type' => 'text',
                'newids' => 'rac_recovered_email_subject',
                'class' => 'admin_notification'
            ),
            array(
                'name' => __('Notification Email Message', 'recoverabandoncart'),
                'desc' => '',
                'css' => 'min-height:250px;min-width:400px;',
                'id' => 'rac_recovered_email_message',
                'std' => "A cart has been Recovered. Here is the order ID {rac.recovered_order_id} for Reference and Line Items is here {rac.order_line_items}.",
                'default' => "A cart has been Recovered. Here is the order ID {rac.recovered_order_id} for Reference and Line Items is here {rac.order_line_items}.",
                'type' => 'textarea',
                'newids' => 'rac_recovered_email_message',
                'class' => 'admin_notification'
            ),
            array('type' => 'sectionend', 'id' => 'rac_notification_settings'), //Notification Settings END
            array(
                'name' => __('Cart List Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_cartlist_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Remove NEW and ABANDON Carts Previously by same Users', 'recoverabandoncart'),
                'desc' => __('Enabling this option will remove New and Abandon Carts by same Users', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_remove_carts',
                'clone_id' => 'rac_remove_carts',
            ),
            array(
                'name' => __('Remove Carts with "NEW" Status', 'recoverabandoncart'),
                'desc' => __('Enabling this option will remove New Carts by same Users', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_remove_new',
                'clone_id' => 'rac_remove_new',
            ),
            array(
                'name' => __('Remove Carts with "ABANDON" Status', 'recoverabandoncart'),
                'desc' => __('Enabling this option will remove Abandon Carts by same Users', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_remove_abandon',
                'clone_id' => 'rac_remove_abandon',
            ),
            array(
                'name' => __('Remove Carts with "ABANDON" Status after x Days', 'recoverabandoncart'),
                'desc' => __('Enabling this option will delete Abandon Carts after Days which You Select', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'enable_remove_abandon_after_x_days',
                'css' => 'min-width:153px',
                'type' => 'select',
                'options' => array('yes' => 'Yes', 'no' => 'No'),
                'std' => 'no',
                'default' => 'no',
                'clone_id' => 'enable_remove_abandon_after_x_days',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Days to Delete Abandon Carts', 'recoverabandoncart'),
                'desc' => __('Enter Days to delete Abandon Carts in Cart List', 'recoverabandoncart'),
                'id' => 'rac_remove_abandon_after_x_days',
                'clone_id' => 'rac_remove_abandon_after_x_days',
                'type' => 'number',
                'std' => '30',
                'default' => '30',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom Restrict Settings', 'recoverabandoncart'),
                'desc' => __('Selected User Roles, Names and Email ID To Restrict Entry in Cart List', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'custom_restrict',
                'css' => 'min-width:153px',
                'type' => 'select',
                'options' => array('user_role' => __('User Role', 'recoverabandoncart'), 'name' => __('Name', 'recoverabandoncart'), 'mail_id' => __('Mail ID', 'recoverabandoncart')),
                'std' => 'user_role',
                'default' => 'user_role',
                'clone_id' => 'custom_restrict',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select User Role', 'recoverabandoncart'),
                'desc' => __('Enter the First Three Characters of User Role', 'recoverabandoncart'),
                'id' => 'custom_user_role_for_restrict_in_cart_list',
                'css' => 'min-width:150px',
                'type' => 'multiselect',
                'std' => '',
                'options' => $user_role,
                'clone_id' => 'custom_user_role_for_restrict_in_cart_list',
                'desc_tip' => true,
            ),
            array(
                'name' => __('User Name Selected', 'recoverabandoncart'),
                'desc' => __('Enter the First Three Characters of User Name', 'recoverabandoncart'),
                'id' => 'custom_user_name_select_for_restrict_in_cart_list',
                'css' => 'min-width:400px',
                'std' => '',
                'type' => 'rac_exclude_users_list_for_restrict_in_cart_list',
                'clone_id' => 'custom_user_name_select_for_restrict_in_cart_list',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom Mail ID Selected', 'recoverabandoncart'),
                'desc' => __('Enter Mail ID per line which will be restricted to includes an entry in Cart List', 'recoverabandoncart'),
                'id' => 'custom_mailid_for_restrict_in_cart_list',
                'clone_id' => 'custom_mailid_for_restrict_in_cart_list',
                'type' => 'textarea',
                'css' => 'min-width:500px;min-height:200px',
                'std' => '',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Insert Entry in Cart List with Abandon Status When Order Status is Cancelled', 'recoverabandoncart'),
                'desc' => __('Enabling this option will Insert Entry in Cart List with Abandon Status When Order Status is Cancelled', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_insert_abandon_cart_when_os_cancelled',
                'clone_id' => 'rac_insert_abandon_cart_when_os_cancelled',
            ),
            array(
                'name' => __('Prevent adding "New" cart when order cancelled in cart page', 'recoverabandoncart'),
                'desc' => __('Enabling this option will Prevent adding "New" cart when order cancelled in cart page', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_prevent_entry_in_cartlist_while_order_cancelled_in_cart_page',
                'clone_id' => 'rac_prevent_entry_in_cartlist_while_order_cancelled_in_cart_page',
            ),
            array(
                'name' => __('Cart List Display', 'recoverabandoncart'),
                'desc' => __('This option controls how the captured carts are displayed in the cart list table', 'recoverabandoncart'),
                'type' => 'radio',
                'options' => array('yes' => __('First Captured Cart Displayed first', 'recoverabandoncart'), 'no' => __('Recently Captured Cart Displayed first', 'recoverabandoncart')),
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_display_cart_list_basedon_asc_desc',
                'clone_id' => 'rac_display_cart_list_basedon_asc_desc',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'rac_cartlist_settings'), //Carts List Settings END
            array(
                'name' => __('Guest Cart Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_guestcart_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Pending', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Pending', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_pending',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_pending',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Failed', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Failed', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_failed',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_failed',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to On-Hold', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become On-Hold', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_on-hold',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_on-hold',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Processing', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Processing', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_processing',
                'std' => 'yes',
                'default' => 'yes',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_processing',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Completed', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Completed', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_completed',
                'std' => 'yes',
                'default' => 'yes',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_completed',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Refunded', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Refunded', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_refunded',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_refunded',
            ),
            array(
                'name' => __('Remove Guest Cart when the Order Status Changes to Cancelled', 'recoverabandoncart'),
                'desc' => __('Guest Cart Captured on place order will be in cart list, it will be removed when order become Cancelled', 'recoverabandoncart'),
                'id' => 'rac_guest_abadon_type_cancelled',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'newids' => 'rac_guest_abadon_type_cancelled',
            ),
            array('type' => 'sectionend', 'id' => 'rac_guestcart_settings'), //Cart Abadoned Guest Settings END
            //Mail Log Settings start
            array(
                'name' => __('Mail Log Settings', 'recoverabandoncart'),
                'type' => 'title',
                'id' => '_mail_log_settings',
            ),
            array(
                'name' => __('Mail Log Display', 'recoverabandoncart'),
                'desc' => __('This option controls how the mail log is displayed in the Mail Log table', 'recoverabandoncart'),
                'type' => 'radio',
                'options' => array('yes' => __('Oldest Mail Log Displayed first', 'recoverabandoncart'), 'no' => __('Newest Mail Log Displayed first', 'recoverabandoncart')),
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_display_mail_log_basedon_asc_desc',
                'clone_id' => 'rac_display_mail_log_basedon_asc_desc',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mail_log_settings'), //Mail Log Settings END
            // Reovered Orders Settings start
            array(
                'name' => __('Reovered Orders Settings', 'recoverabandoncart'),
                'type' => 'title',
                'id' => '_recovered_orders_settings',
            ),
            array(
                'name' => __('Reovered Orders Display', 'recoverabandoncart'),
                'desc' => __('This option controls how the recovered orders are displayed in the Recovered Orders table', 'recoverabandoncart'),
                'type' => 'radio',
                'options' => array('yes' => __('Oldest Recovered Order Displayed first', 'recoverabandoncart'), 'no' => __('Newest Recovered Order Displayed first', 'recoverabandoncart')),
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_display_recovered_orders_basedon_asc_desc',
                'clone_id' => 'rac_display_recovered_orders_basedon_asc_desc',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_recovered_orders_settings'), //Mail Log Settings END
            array(
                'name' => __('My Account Settings', 'recoverabandoncart'),
                'type' => 'title',
                'id' => '_rac_myaccount_settings',
            ),
            array(
                'name' => __('Show Unsubscription Option in My Account Page', 'recoverabandoncart'),
                'desc' => __('Turn On to make it visible in My Account Page', 'recoverabandoncart'),
                'id' => 'rac_unsub_myaccount_option',
                'std' => 'no',
                'default' => 'no',
                'type' => 'checkbox',
                'clone_id' => 'rac_unsub_myaccount_option',
                'newids' => 'rac_unsub_myaccount_option',
            ),
            array(
                'name' => __('Customize Unsubscription Heading in My Account Page', 'recoverabandoncart'),
                'desc' => __('Customize the heading appeared in My Account Page', 'recoverabandoncart'),
                'id' => 'rac_unsub_myaccount_heading',
                'std' => 'Unsubscription Settings',
                'default' => 'Unsubscription Settings',
                'type' => 'text',
                'clone_id' => 'rac_unsub_myaccount_heading',
                'newids' => 'rac_unsub_myaccount_heading',
            ),
            array(
                'name' => __('Customize Unsubscription Text in My Account Page', 'recoverabandoncart'),
                'desc' => __('Customize the Message appeared in My Account Page for Subscription', 'recoverabandoncart'),
                'id' => 'rac_unsub_myaccount_text',
                'std' => 'Unsubscribe Here To Receive Email from Abandon Cart',
                'default' => 'Unsubscribe Here to Receiver Email from Abandon Cart',
                'type' => 'textarea',
                'clone_id' => 'rac_unsub_myaccount_text',
                'newids' => 'rac_unsub_myaccount_text',
            ),
            array('type' => 'sectionend', 'id' => '_rac_myaccount_settings'),
        ));
    }

    public static function fp_rac_admin_setting_general() {
        woocommerce_admin_fields(FP_RAC_General_Tab::fp_rac_menu_options_general());
    }

    public static function fp_rac_update_options_general() {
        woocommerce_update_options(FP_RAC_General_Tab::fp_rac_menu_options_general());
    }

    public static function fprac_general_default_settings() {
        global $woocommerce;
        foreach (FP_RAC_General_Tab::fp_rac_menu_options_general() as $setting)
            if (isset($setting['id']) && isset($setting['std'])) {
                add_option($setting['id'], $setting['std']);
            }
    }

    public static function rac_selected_users_restrict_option() {
        echo rac_common_function_to_multi_select_for_customer_search('custom_user_name_select_for_restrict_in_cart_list', 'User Name Selected');
    }

}

new FP_RAC_General_Tab();
