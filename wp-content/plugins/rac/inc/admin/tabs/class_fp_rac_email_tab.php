<?php

class FP_RAC_Email_Tab {

    public function __construct() {
        add_action('woocommerce_fprac_settings_tabs_fpracemail', array($this, 'fp_rac_admin_setting_email'));
        add_action('woocommerce_update_options_fpracemail', array($this, 'fp_rac_update_options_email'));
        add_action('woocommerce_admin_field_rac_exclude_users_list', array($this, 'rac_selected_users_exclude_option'));
        add_action('woocommerce_admin_field_rac_drag_drop_product_info', array($this, 'fp_rac_drag_drop_product_info_column_alignment'));
        add_action('wp_ajax_rac_drag_n_drop_product_info_column', array($this, 'fp_rac_admin_request_from_ajax_sortable'));
    }

    public static function fp_rac_menu_options_email() {
        global $woocommerce;

        global $wp_roles;
        foreach ($wp_roles->role_names as $key => $value) {
            $userrole[] = $key;
            $username[] = $value;
        }

        $user_role = array_combine((array) $userrole, (array) $username);

        return apply_filters('woocommerce_fpracemail_settings', array(
            array(
                'name' => __('Email Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_email_gen_settings',
                'clone_id' => '',
            ),
            array(
                'name' => __('Send Email to Members', 'recoverabandoncart'),
                'desc' => __('Enabling this option will send an Email to Members only', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_email_use_members',
                'clone_id' => 'rac_email_use_members',
            ),
            array(
                'name' => __('Send Email to Guests', 'recoverabandoncart'),
                'desc' => __('Enabling this option will send an Email to Guests only', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_email_use_guests',
                'clone_id' => 'rac_email_use_guests',
            ),
            array(
                'name' => __('Custom Exclude Settings', 'recoverabandoncart'),
                'desc' => __('Select User Roles, Names and Email ID To Stop EMail Sending', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'custom_exclude',
                'css' => 'min-width:153px',
                'type' => 'select',
                'options' => array('user_role' => __('User Role', 'recoverabandoncart'), 'name' => __('Name', 'recoverabandoncart'), 'mail_id' => __('Mail ID', 'recoverabandoncart')),
                'std' => 'user_role',
                'default' => 'user_role',
                'clone_id' => 'custom_exclude',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select User Role', 'recoverabandoncart'),
                'desc' => __('Enter the First Three Characters of User Role', 'recoverabandoncart'),
                'id' => 'custom_user_role',
                'css' => 'min-width:150px',
                'type' => 'multiselect',
                'std' => '',
                'options' => $user_role,
                'clone_id' => 'custom_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('User Name Selected', 'recoverabandoncart'),
                'desc' => __('Enter the First Three Character of User Name', 'recoverabandoncart'),
                'id' => 'custom_user_name_select',
                'css' => 'min-width:400px',
                'std' => '',
                'type' => 'rac_exclude_users_list',
                'clone_id' => 'custom_user_name_select',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Custom Mail ID Selected', 'recoverabandoncart'),
                'desc' => __('Enter Mail ID per line which will be excluded to receive a mail from Recover Abandon Cart', 'recoverabandoncart'),
                'id' => 'custom_mailid_edit',
                'clone_id' => 'custom_mailid_edit',
                'type' => 'textarea',
                'css' => 'min-width:500px;min-height:200px',
                'std' => '',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Email Sending Method', 'recoverabandoncart'),
                'id' => 'rac_mail_template_send_method',
                'css' => 'min-width:153px',
                'type' => 'select',
                'options' => array('abandon_time' => __('Based on Abandoned cart Time', 'recoverabandoncart'), 'template_time' => __('Based on Previous Mail Sent Time', 'recoverabandoncart')),
                'std' => 'abandon_time',
                'default' => 'abandon_time',
                'clone_id' => 'rac_mail_template_send_method',
            ),
            array(
                'name' => __('Mail Sending Priority', 'recoverabandoncart'),
                'id' => 'rac_mail_template_sending_priority',
                'css' => 'min-width:153px',
                'type' => 'select',
                'options' => array('mailduration' => __('Mail Duration', 'recoverabandoncart'), 'mailsequence' => __('Mail Sequence', 'recoverabandoncart')),
                'std' => 'mailduration',
                'default' => 'mailduration',
                'clone_id' => 'rac_mail_template_sending_priority',
            ),
            array('type' => 'sectionend', 'id' => 'rac_email_gen_settings'), //Email Settings END
            array(
                'name' => __('Email Template Cart Link Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_cart_link_customization',
            ),
            array(
                'name' => __('Cart Link', 'recoverabandoncart'),
                'desc' => __('Customize the Cart Link in Email Template', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cart_link_options',
                'css' => '',
                'type' => 'select',
                'desc_tip' => true,
                'std' => '1',
                'default' => '1',
                'options' => array(
                    '1' => __('Hyperlink', 'recoverabandoncart'),
                    '2' => __('URL', 'recoverabandoncart'),
                    '3' => __('Button', 'recoverabandoncart'),
                ),
                'clone_id' => 'rac_cart_link_options',
            ),
            array(
                'name' => __('Button Background Color', 'recoverabandoncart'),
                'desc' => __('Customize Button Background Color in Email', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cart_button_bg_color',
                'class' => 'color racbutton',
                'css' => '',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '000091',
                'default' => '000091',
                'clone_id' => 'rac_cart_button_bg_color',
            ),
            array(
                'name' => __('Button Link Color', 'recoverabandoncart'),
                'desc' => __('Customize Button Link Color', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cart_button_link_color',
                'class' => 'color racbutton',
                'css' => '',
                'type' => 'text',
                'desc_tip' => true,
                'std' => 'ffffff',
                'default' => 'ffffff',
                'clone_id' => 'rac_cart_button_link_color',
            ),
            array(
                'name' => __('Link Color', 'recoverabandoncart'),
                'desc' => __('Customize Link Color in Email Template', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_email_link_color',
                'class' => 'color raclink',
                'css' => '',
                'type' => 'text',
                'desc_tip' => true,
                'std' => '1919FF',
                'default' => '1919FF',
                'clone_id' => 'rac_email_link_color',
            ),
            array(
                'name' => __('Redirect after click cartlink in email', 'recoverabandoncart'),
                'desc' => __('Please Select the page that you want to redirect after clicking the cartlink in an email', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_cartlink_redirect',
                'css' => '',
                'std' => '1',
                'type' => 'radio',
                'options' => array('1' => __('Cart Page', 'recoverabandoncart'), '2' => __('Checkout Page', 'recoverabandoncart')),
                'newids' => 'rac_cartlink_redirect',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'rac_date_time_format_customization'),
            array(
                'name' => __('Shortcode Date and Time format Customization', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '{rac.date} and {rac.time} date time format customization',
                'id' => 'rac_date_time_format_customization',
            ),
            array(
                'name' => __('Date Format', 'recoverabandoncart'),
                'desc' => 'Customize Date Format for {rac.date}',
                'id' => 'rac_date_format',
                'type' => 'text',
                'desc_tip' => true,
                'std' => 'd:m:y',
                'default' => 'd:m:y',
                'clone_id' => 'rac_date_format',
            ),
            array(
                'name' => __('Time Format', 'recoverabandoncart'),
                'desc' => 'Customize Time Format for {rac.time}',
                'id' => 'rac_time_format',
                'type' => 'text',
                'desc_tip' => true,
                'std' => 'h:i:s',
                'default' => 'h:i:s',
                'clone_id' => 'rac_time_format',
            ),
            array('type' => 'sectionend', 'id' => 'rac_cart_link_customization'),
            array(
                'name' => __('Unsubscribe Settings', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => '',
                'id' => 'rac_email_unsubscription',
                'clone_id' => '',
            ),
            array(
                'name' => __('Unsubscription Link in Email', 'recoverabandoncart'),
                'desc' => __('Enable', 'recoverabandoncart'),
                'id' => 'fp_unsubscription_link_in_email',
                'clone_id' => 'fp_unsubscription_link_in_email',
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'desc_tip' => '',
            ),
            array(
                'name' => __("Unsubscription Link Text in Email Template will", 'recoverabandoncart'),
                'desc' => __('Choose how the Unsubscription Link from Recovered Abandon Cart will be displayed in emails ', 'recoverabandoncart'),
                'id' => 'fp_unsubscription_footer_link_text_option',
                'clone_id' => 'fp_unsubscription_footer_link_text_option',
                'type' => 'select',
                'options' => array(
                    '1' => __("Replace WooCommerce Footer Text", "recoverabandoncart"),
                    '2' => __("Append to WooCommerce Footer Text", "recoverabandoncart"),
                ),
                'default' => '1',
                'std' => '1',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Unsubscription Link Text", 'recoverabandoncart'),
                'desc' => __('Enter Unsubscription Anchor Text in Email Footer', 'recoverabandoncart'),
                'id' => 'fp_unsubscription_footer_link_text',
                'clone_id' => 'fp_unsubscription_footer_link_text',
                'type' => 'text',
                'default' => 'Unsubscribe',
                'std' => 'Unsubscribe',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Unsubscription Message', 'recoverabandoncart'),
                'desc' => __('Enter Unsubscription Message which is visible in Email Footer', 'recoverabandoncart'),
                'id' => 'fp_unsubscription_footer_message',
                'clone_id' => 'fp_unsubscription_footer_message',
                'type' => 'textarea',
                'css' => 'height: 60px; width: 320px',
                'default' => 'You can {rac_unsubscribe} to stop Receiving Abandon Cart Mail from {rac_site}',
                'std' => 'You can {rac_unsubscribe} to stop Receiving Abandon Cart Mail from {rac_site}',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Unsubscription Type', 'recoverabandoncart'),
                'desc' => __('Please Select the Unsubscription Type', 'recoverabandoncart'),
                'tip' => '',
                'id' => 'rac_unsubscription_type',
                'class' => 'rac_unsubscription_type',
                'css' => '',
                'std' => '1',
                'default' => '1',
                'type' => 'radio',
                'options' => array('1' => 'Automatic Unsubscription', '2' => 'Manual Unsubscription'),
                'newids' => 'rac_unsubscription_type',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Redirect Url for Automatic Unsubscription", 'recoverabandoncart'),
                'desc' => __('Enter Redirect Url to redirect when click the Automatic unsubscription link', 'recoverabandoncart'),
                'id' => 'rac_unsubscription_redirect_url',
                'clone_id' => 'rac_unsubscription_redirect_url',
                'type' => 'text',
                'default' => get_permalink(wc_get_page_id('myaccount')),
                'std' => get_permalink(wc_get_page_id('myaccount')),
                'desc_tip' => true,
            ),
            array(
                'name' => __("Redirect Url for Manual Unsubscription", 'recoverabandoncart'),
                'desc' => __('Enter Redirect Url to redirect when click the Manual unsubscription link', 'recoverabandoncart'),
                'id' => 'rac_manual_unsubscription_redirect_url',
                'clone_id' => 'rac_manual_unsubscription_redirect_url',
                'type' => 'text',
                'default' => get_permalink(wc_get_page_id('myaccount')),
                'std' => get_permalink(wc_get_page_id('myaccount')),
                'desc_tip' => true,
            ),
            array(
                'name' => __("Already Unsubscribed Text", 'recoverabandoncart'),
                'desc' => __('Enter Already Unsubscribed Text', 'recoverabandoncart'),
                'id' => 'rac_already_unsubscribed_text',
                'clone_id' => 'rac_already_unsubscribed_text',
                'type' => 'text',
                'default' => 'You have already unsubscribed.',
                'std' => 'You have already unsubscribed.',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Unsubscribed Successfully Text", 'recoverabandoncart'),
                'desc' => __('Enter Unsubscribed Successfully Text', 'recoverabandoncart'),
                'id' => 'rac_unsubscribed_successfully_text',
                'clone_id' => 'rac_unsubscribed_successfully_text',
                'type' => 'text',
                'default' => 'You have successfully unsubscribed from Abandoned cart Emails.',
                'std' => 'You have successfully unsubscribed from Abandoned cart Emails.',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Confirm Unsubscription Text", 'recoverabandoncart'),
                'desc' => __('Enter Confirm Unsubscription Text', 'recoverabandoncart'),
                'id' => 'rac_confirm_unsubscription_text',
                'clone_id' => 'rac_confirm_unsubscription_text',
                'type' => 'text',
                'default' => 'To stop receiving Abandoned Cart Emails, Click the Unsubscribe button below',
                'std' => 'To stop receiving Abandoned Cart Emails, Click the Unsubscribe button below',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Unsubscription Message Text color", 'recoverabandoncart'),
                'desc' => __('Choose Unsubscription Message Text color', 'recoverabandoncart'),
                'id' => 'rac_unsubscription_message_text_color',
                'clone_id' => 'rac_unsubscription_message_text_color',
                'type' => 'text',
                'default' => 'fff',
                'std' => 'fff',
                'class' => 'color',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Background color for Unsubscription Message", 'recoverabandoncart'),
                'desc' => __('Choose Background color for Unsubscription Message', 'recoverabandoncart'),
                'id' => 'rac_unsubscription_message_background_color',
                'clone_id' => 'rac_unsubscription_message_background_color',
                'type' => 'text',
                'default' => 'a46497',
                'std' => 'a46497',
                'class' => 'color',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Unsubscription Email Text color", 'recoverabandoncart'),
                'desc' => __('Choose Unsubscription Email Text color', 'recoverabandoncart'),
                'id' => 'rac_unsubscription_email_text_color',
                'clone_id' => 'rac_unsubscription_email_text_color',
                'type' => 'text',
                'default' => '000000',
                'std' => '000000',
                'class' => 'color',
                'desc_tip' => true,
            ),
            array(
                'name' => __("Confirm Unsubscription Text color", 'recoverabandoncart'),
                'desc' => __('Choose Confirm Unsubscription Text color', 'recoverabandoncart'),
                'id' => 'rac_confirm_unsubscription_text_color',
                'clone_id' => 'rac_confirm_unsubscription_text_color',
                'type' => 'text',
                'default' => 'ff3f12',
                'std' => 'ff3f12',
                'class' => 'color',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'rac_email_unsubscription'),
            array(
                'type' => 'rac_drag_drop_product_info',
            ),
            array(
                'name' => __('Customize Caption and Visibility for Product Info in Email Template', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => 'Following Customization options works with the shortcode {rac.Productinfo} in Email Template',
                'id' => 'rac_customize_caption_in_product_info',
                'clone_id' => '',
            ),
            array(
                'name' => __('Enable Border for Product Info in Email Template', 'recoverabandoncart'),
                'desc' => __('Enable Border for Product Info in Email Template', 'recoverabandoncart'),
                'id' => 'rac_enable_border_for_productinfo_in_email',
                'clone_id' => 'rac_enable_border_for_productinfo_in_email',
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'desc_tip' => '',
            ),
            array(
                'name' => __('Display Variations for Variable Product in Email Template', 'recoverabandoncart'),
                'desc' => __('When set to "Show" Variation Informations(Attributes will be display below Product Name in Email Template)', 'recoverabandoncart'),
                'type' => 'select',
                'default' => 'yes',
                'options' => array('yes' => __('Show', 'recoverabandoncart'), 'no' => __('Hide', 'recoverabandoncart')),
                'std' => 'yes',
                'id' => 'rac_email_product_variation_sh',
                'clone_id' => 'rac_email_product_variation_sh',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Customize Product Name Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Product Name Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Product Name',
                'std' => 'Product Name',
                'id' => 'rac_product_info_product_name',
                'clone_id' => 'rac_product_info_product_name',
            ),
            array(
                'name' => __('Customize Product Image Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Product Image Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Product Image',
                'std' => 'Product Image',
                'id' => 'rac_product_info_product_image',
                'clone_id' => 'rac_product_info_product_image',
            ),
            array(
                'name' => __('Customize Product Quantity in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Product Quantity in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Quantity',
                'std' => 'Quantity',
                'id' => 'rac_product_info_quantity',
                'clone_id' => 'rac_product_info_quantity',
            ),
            array(
                'name' => __('Customize Product Price Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Product Price Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Product Price',
                'std' => 'Product Price',
                'id' => 'rac_product_info_product_price',
                'clone_id' => 'rac_product_info_product_price',
            ),
            array(
                'name' => __('Customize Subtotal Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Subtotal Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Subtotal',
                'std' => 'Subtotal',
                'id' => 'rac_product_info_subtotal',
                'clone_id' => 'rac_product_info_subtotal',
            ),
            array(
                'name' => __('Customize Shipping Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Shipping Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Shipping',
                'std' => 'Shipping',
                'id' => 'rac_product_info_shipping',
                'clone_id' => 'rac_product_info_shipping',
            ),
            array(
                'name' => __('Customize Tax Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Tax Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Tax',
                'std' => 'Tax',
                'id' => 'rac_product_info_tax',
                'clone_id' => 'rac_product_info_tax',
            ),
            array(
                'name' => __('Customize Total Caption in Email Template', 'recoverabandoncart'),
                'desc' => __('Customize Total Caption in Email Template', 'recoverabandoncart'),
                'type' => 'text',
                'default' => 'Total',
                'std' => 'Total',
                'id' => 'rac_product_info_total',
                'clone_id' => 'rac_product_info_total',
            ),
            array(
                'name' => __('Hide Product Name Column in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Product Name Column in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_product_name_product_info_shortcode',
                'clone_id' => 'rac_hide_product_name_product_info_shortcode',
            ),
            array(
                'name' => __("Hide Product Image Column in Product Info Shortcode", 'recoverabandoncart'),
                'desc' => __('Hide Product Image Column in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_product_image_product_info_shortcode',
                'clone_id' => 'rac_hide_product_image_product_info_shortcode',
            ),
            array(
                'name' => __('Hide Quantity Column in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Quantity Column in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_product_quantity_product_info_shortcode',
                'clone_id' => 'rac_hide_product_quantity_product_info_shortcode',
            ),
            array(
                'name' => __('Hide Product Price Column in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Product Price Column in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_product_price_product_info_shortcode',
                'clone_id' => 'rac_hide_product_price_product_info_shortcode',
            ),
            array(
                'name' => __('Hide Subtotal, Shipping, Tax, Total Rows in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Subtotal, Shipping, Tax, Total Rows in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_tax_total_product_info_shortcode',
                'clone_id' => 'rac_hide_tax_total_product_info_shortcode',
            ),
            array(
                'name' => __('Hide Shipping Row in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Shipping Row in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_shipping_row_product_info_shortcode',
                'clone_id' => 'rac_hide_shipping_row_product_info_shortcode',
            ),
            array(
                'name' => __('Hide Tax Row in Product Info Shortcode', 'recoverabandoncart'),
                'desc' => __('Hide Tax Row in Product Info Shortcode', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_hide_tax_row_product_info_shortcode',
                'clone_id' => 'rac_hide_tax_row_product_info_shortcode',
            ),
            array(
                'name' => __('Clear the Cart Content When Cart Link is Clicked', 'recoverabandoncart'),
                'desc' => '',
                'type' => 'checkbox',
                'default' => 'yes',
                'std' => 'yes',
                'id' => 'rac_cart_content_when_cart_link_is_clicked',
                'clone_id' => 'rac_cart_content_when_cart_link_is_clicked',
            ),
            array(
                'name' => __('Display Product Price Including Tax in Emails', 'recoverabandoncart'),
                'desc' => __('Display Product Price Including Tax in Emails', 'recoverabandoncart'),
                'type' => 'checkbox',
                'default' => 'no',
                'std' => 'no',
                'id' => 'rac_inc_tax_with_product_price_product_info_shortcode',
                'clone_id' => 'rac_inc_tax_with_product_price_product_info_shortcode',
            ),
            array('type' => 'sectionend', 'id' => 'rac_email_gen_settings'),
        ));
    }

    public static function fp_rac_admin_setting_email() {
        woocommerce_admin_fields(FP_RAC_Email_Tab::fp_rac_menu_options_email());
    }

    public static function fp_rac_update_options_email() {
        woocommerce_update_options(FP_RAC_Email_Tab::fp_rac_menu_options_email());
    }

    public static function fprac_email_default_settings() {
        global $woocommerce;
        foreach (FP_RAC_Email_Tab::fp_rac_menu_options_email() as $setting)
            if (isset($setting['id']) && isset($setting['std'])) {
                add_option($setting['id'], $setting['std']);
            }
    }

    public static function rac_selected_users_exclude_option() {
        echo rac_common_function_to_multi_select_for_customer_search('custom_user_name_select', 'User Name Selected');
    }

    public static function fp_rac_drag_drop_product_info_column_alignment() {
        global $woocommerce;
        ?>
        <script type="text/javascript">
            jQuery(function () {
                jQuery('#add').click(function () {
                    jQuery('#test').val();
                });
                jQuery('table#rac_drag_n_drop_product_info').sortable({
                    axis: "y",
                    items: 'tbody',
                    update: function (event, ui) {
                        var data = jQuery(this).sortable("toArray");
                        // POST to server using $.post or $.ajax
                        jQuery.ajax({
                            data: ({
                                action: 'rac_drag_n_drop_product_info_column',
                                data: data,
                            }),
                            type: 'POST',
                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                            success: function (response) {
                                console.log(response);
                            },
                        });
                    }
                });
            });
        </script>
        <style type="text/css">
            tbody>tr.rac_product_info_drag_n_drop  {
                border: 1px solid #ccc;
            }
            .postbox h3 {
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 8px 12px;
            }
            .form-field label {
                display:table-row;
                font-weight:  bold;
                font-size:14px;

            }
            tbody>tr.rac_product_info_drag_n_drop {
                background:#fff;
                margin-bottom:10px;
                display: table-row-group;
                cursor:move;
            }
            table>tbody>tr.rac_product_info_drag_n_drop:hover {
                background:#dedec6;
            }
        </style>
        <h3><label><?php _e('Product Info Positioning', 'recoverabandoncart') ?></label></h3>
        <table class="form-table" id="rac_drag_n_drop_product_info">
            <?php
            $sortable_column = array('product_name' => __('Product Name', 'recoverabandoncart'), 'product_image' => __('Product Image', 'recoverabandoncart'), 'product_quantity' => __('Quantity', 'recoverabandoncart'), 'product_price' => __('Total', 'recoverabandoncart'));
            $priority_array = get_option('drag_and_drop_product_info_sortable_column', true);
            $priority_array = is_array($priority_array) && !empty($priority_array) ? $priority_array : array_keys($sortable_column);
            foreach ($priority_array as $key) {
                ?><tbody id="<?php echo $key; ?>">
                    <tr class="rac_product_info_drag_n_drop" id="<?php echo $key; ?>">
                        <td style="width:400px;"><?php echo $sortable_column[$key]; ?></td>
                    <tr>
                </tbody>
                <?php
            }
            ?>
        </table>

        <?php
    }

    public static function fp_rac_admin_request_from_ajax_sortable() {
        if (isset($_POST['data'])) {
            update_option('drag_and_drop_product_info_sortable_column', $_POST['data']);
        }
        exit();
    }

}

new FP_RAC_Email_Tab();
