<?php

class FP_RAC_Mail_Log_Tab {

    public static function fp_rac_mail_logs_display() {
        global $wpdb;
        $table_name_logs = $wpdb->prefix . 'rac_email_logs';
        $email_template_table = $wpdb->prefix . 'rac_templates_email';
        $order_by = get_option('rac_display_mail_log_basedon_asc_desc');
        $order = $order_by != 'no' ? "ASC" : "DESC";
        $rac_log_list = $wpdb->get_results("SELECT * FROM $table_name_logs ORDER BY id $order", OBJECT);
        echo '&nbsp<span><select id="rac_pagination_logs">';
        for ($k = 1; $k <= 20; $k++) {

            if ($k == 10) {
                echo '<option value="' . $k . '" selected="selected">' . $k . '</option>';
            } else {
                echo '<option value="' . $k . '">' . $k . '</option>';
            }
        }
        '</select></span>';

        echo '&nbsp<label>' . __('Search', 'recoverabandoncart') . '</label><input type="text" name="rac_temp_search" id="rac_temp_search">';

        echo '<table class="rac_email_logs_table table" data-page-size="10" data-filter="#rac_temp_search" data-filter-minimum="1">';
        echo '<thead>
		<tr>
                <th>' . __('S.No', 'recoverabandoncart') . '</th>
			<th>' . __('Email Id', 'recoverabandoncart') . '</th>
                        <th>' . __('Date/Time', 'recoverabandoncart') . '</th>
			<th>' . __('Template Used', 'recoverabandoncart') . '</th>
                        <th>' . __('Abandoned Cart ID', 'recoverabandoncart') . '</th>
                        <th class="rac_small_col" data-sort-ignore="true"><a href="#" id="rac_sel">' . __('Select All', 'recoverabandoncart') . '</a>&nbsp/&nbsp <a href="#" id="rac_desel">' . __('Deselect All', 'recoverabandoncart') . '</a>&nbsp<a href="#" id="rac_selected_del_log" class="button">' . __('Delete Selected', 'recoverabandoncart') . '</a></th>
		</tr>
	</thead><tbody>';
        $srno = 1;
        foreach ($rac_log_list as $each_log) {
            echo "<tr>";
            echo "<td data-value=" . $srno . ">";
            echo $srno;
            echo "</td>";
            echo "<td>";
            echo $each_log->email_id;
            echo "</td>";
            echo "<td data-value=" . $each_log->date_time . ">";
            echo date(get_option('date_format'), $each_log->date_time) . '/' . date(get_option('time_format'), $each_log->date_time);
            echo "</td>";
            echo "<td>";
            $template_id = $each_log->template_used;
            $manual_mail = strpos($template_id, "Manual");
            if ($manual_mail !== false) {
                $template_id = explode("-", $template_id);
                $template_name = $wpdb->get_results("SELECT template_name FROM $email_template_table WHERE id=$template_id[0]", OBJECT);
                if (!empty($template_name)) {
                    echo $template_name[0]->template_name . '(#' . $each_log->template_used . ')';
                } else {
                    _e('Template Info not Available', 'recoverabandoncart');
                }
            } else {
                $template_name = $wpdb->get_results("SELECT template_name FROM $email_template_table WHERE id=$template_id", OBJECT);
                if (!empty($template_name)) {
                    echo $template_name[0]->template_name;
                } else {
                    _e('Template Info not Available', 'recoverabandoncart');
                }
            }

            echo "</td>";
            echo "<td data-value=" . $each_log->rac_cart_id . ">";
            echo $each_log->rac_cart_id;
            echo "</td>";
            ?>
            <td class="bis_mas_small_col">
                <input type="checkbox" class="rac_checkboxes" data-raclogid="<?php echo $each_log->id; ?>"/>
                <a href="#" class="button rac_check_indi" data-raclogdelid="<?php echo $each_log->id; ?>"><?php _e('Delete this Row', 'recoverabandoncart'); ?></a>

            </td>
            <?php
            $srno++; //for serial number
        }
        echo '</tbody>
            <tfoot>
		<tr>
			<td colspan="6">
				<div class="pagination pagination-centered hide-if-no-paging"></div>
			</td>
		</tr>
	</tfoot></table><style>.footable > tbody > tr > td,.footable > thead > tr > th, .footable > thead > tr > td{text-align:center;}</style>';
    }

}
