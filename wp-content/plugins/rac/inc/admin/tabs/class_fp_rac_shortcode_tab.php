<?php

class FP_RAC_Shortcode_Tab {

    public static function fp_rac_shortcodes_info() {
        $shortcodes_info = array(
            "{rac.cartlink}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Abandoned Cart can be loaded using this link from mail", "recoverabandoncart")
            ),
            "{rac.date}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Abandoned Cart Date", "recoverabandoncart")
            ),
            "{rac.time}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Abandoned Cart Time", "recoverabandoncart")
            ),
            "{rac.firstname}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Receiver First Name", "recoverabandoncart")),
            "{rac.lastname}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Receiver Last Name", "recoverabandoncart")),
            "{rac.recovered_order_id}" => array("mail" => __("Admin Order Recovered Notification Mail", "recoverabandoncart"),
                "usage" => __("Order ID can be inserted in the admin notification mail for Reference", "recoverabandoncart")),
            "{rac.order_line_items}" => array("mail" => __("Admin Order Line Items in Recovered Notification Mail", "recoverabandoncart"),
                "usage" => __("Order Line Items will be displayed in Admin Notification Mail for Information", "recoverabandoncart")),
            "{rac.Productinfo}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Product Information Name Image Amount", "recoverabandoncart")),
            "{rac.coupon}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Copon code will be generated automatically and included in the mail with a Coupon options based on the settings from 'Coupon In Mail' tab", "recoverabandoncart")),
            "{rac.unsubscribe}" => array("mail" => __("Abandoned Cart Mail", "recoverabandoncart"),
                "usage" => __("Shows Unsubscibe Link", "recoverabandoncart")),
            "{rac.unsubscribe_email_manual}" => array("mail" => __("Pages", "recoverabandoncart"),
                "usage" => __("Manual Unsubscription of Abandon Cart emails done in this page", "recoverabandoncart")
        ));
        ?>
        <table class="rac_shortcodes_info">
            <thead>
                <tr>
                    <th>
                        <?php _e('Shortcode', 'recoverabandoncart'); ?>
                    </th>
                    <th>
                        <?php _e('Context where Shortcode is valid', 'recoverabandoncart'); ?>
                    </th>
                    <th>
                        <?php _e('Purpose', 'recoverabandoncart'); ?>
                    </th>
                </tr>
            </thead>
            <?php foreach ($shortcodes_info as $shortcode => $s_info) { ?>
                <tr>
                    <td>
                        <?php echo $shortcode; ?>
                    </td>
                    <td>
                        <?php echo $s_info['mail']; ?>
                    </td>
                    <td>
                        <?php echo $s_info['usage']; ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <style type="text/css">
            .rac_shortcodes_info{
                margin-top:20px;
            }
        </style>
        <?php
    }

}
