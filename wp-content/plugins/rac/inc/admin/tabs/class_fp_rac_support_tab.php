<?php

class FP_RAC_Support_tab {

    public static function fp_rac_support_admin_fields() {
        global $woocommerce;
        return apply_filters('woocommerce_fpracsupport_settings', array(
            array(
                'name' => __('Help & Support', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => __('For support, feature request or any help, please <a href="http://support.fantasticplugins.com/">register and open a support ticket on our site.</a> <br> ', 'recoverabandoncart'),
                'id' => 'rac_support_settings'
            ),
            array(
                'name' => __('Documentation', 'recoverabandoncart'),
                'type' => 'title',
                'desc' => __('Please check the documentation as we have lots of information there. The documentation file can be found inside the documentation folder which you will find when you unzip the downloaded zip file.', 'recoverabandoncart'),
                'id' => 'rac_support_documentation',
            ),
            array('type' => 'sectionend', 'id' => 'rac_support_settings'),
        ));
    }

}
