<?php

class FP_RAC_Reports_Tab {

    public $tickSize;
    public $maillogData;
    public $recoveredData;
    public $abandonData;
    public $minData;
    public $maxData;

    public function __construct() {
        $this->fp_rac_reports();
        $this->rac_get_overall_data();
        $this->rac_get_min_max();
        $this->rac_add_start_end_data();
        $this->fp_rac_display_graph_for_reports();
    }

    public function fp_rac_reports() {
        if (isset($_POST['rac_clear_reports'])) {
            delete_option('rac_abandoned_count');
            delete_option('rac_mail_count');
            delete_option('rac_link_count');
            delete_option('rac_recovered_count');
            delete_option('fp_rac_recovered_order_ids');
        }
        ?>
        <table class="rac_reports form-table">
            <tr>
                <th>
                    <?php _e('Number of Abandoned Carts Captured', 'recoverabandoncart'); ?>
                </th>
                <td>
                    <?php
                    if (get_option('rac_abandoned_count')) {
                        echo get_option('rac_abandoned_count');
                    } else {// if it is boolean false then there is no value. so give 0
                        echo "0";
                    };
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Number of total Emails Sent', 'recoverabandoncart'); ?> 
                </th>
                <td>
                    <?php
                    if (get_option('rac_mail_count')) {
                        echo get_option('rac_mail_count');
                    } else {
                        echo "0";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Number of total Email links clicked', 'recoverabandoncart'); ?>
                </th>
                <td>
                    <?php
                    if (get_option('rac_link_count')) {
                        echo get_option('rac_link_count');
                    } else {
                        echo "0";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Number of Carts Recovered', 'recoverabandoncart'); ?>
                </th>
                <td>
                    <?php
                    if (get_option('rac_recovered_count')) {
                        $admin_url = admin_url('admin.php');
                        $fpracrecoveredorderids = esc_url_raw(add_query_arg(array('page' => 'fprac_slug', 'tab' => 'fpracrecoveredorderids'), $admin_url));
                        echo '<a style="text-decoration:none" href="' . $fpracrecoveredorderids . '">' . get_option('rac_recovered_count') . '</a>&nbsp;';
                    } else {
                        echo "0";
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <th>
                    <?php _e('Total Sales Amount Recovered', 'recoverabandoncart'); ?>
                </th>
                <td>
                    <?php
                    $get_order_ids = array_filter((array) get_option('fp_rac_recovered_order_ids'));
                    $total_sum = array();
                    if (!empty($get_order_ids)) {
                        foreach ($get_order_ids as $key => $value) {
                            $total_sum[] = $value['order_total'];
                        }
                    }
                    $total_sum = array_sum($total_sum);
                    echo FP_List_Table_RAC::format_price($total_sum);
                    ?>
                </td>
            </tr>
        </table>
        <br>
        <input type="submit" name="rac_clear_reports" id="rac_clear_reports" class="rac_clear_reports button-primary" value="<?php _e('Clear Reports', 'recoverabandoncart'); ?>" onclick="return confirm(<?php _e('Are you sure to clear the reports ?', 'recoverabandoncart'); ?>)">
        <style type="text/css">
            .rac_reports {
                width:50%;
                background-color:white;
                border:2px solid #21759b;
                border-collapse:unset;
                border-top: 4px solid #21759b;
                margin-top: 20px !important;

            }
            .rac_reports th{
                padding: 20px;
            }
        </style>
        <?php
    }

    // Display Reports Graph
    public function fp_rac_display_graph_for_reports() {
        $period_formatted_options = '';
        $data_formatted_options = '';
        ?>
        <script type = "text/javascript">
            jQuery(function () {
                var mail_log = <?php echo json_encode($this->maillogData) ?>,
                        abandon_cart = <?php echo json_encode($this->abandonData); ?>,
                        recovered_orders = <?php echo json_encode($this->recoveredData); ?>;
                var plot = jQuery.plot("#rac_each_container_details", [
                    {data: mail_log, label: "Mail Log", color: "#29774a"},
                    {data: abandon_cart, label: "Abandoned Carts", color: "#f00"},
                    {data: recovered_orders, label: "Recovered Orders", color: "#00f"}
                ], {
                    series: {
                        lines: {
                            show: true
                        },
                        points: {
                            radius: 3,
                            fill: true,
                            show: true
                        }
                    },
                    xaxis: {
                        mode: "time",
                        tickSize: [1, '<?php echo $this->tickSize; ?>'],
                        minTickSize: [1, "<?php echo $this->tickSize; ?>"],
                    },
                    yaxis: {
                        min: 0,
                        minTickSize: 1,
                        tickDecimals: 0,
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        borderWidth: 2,
                        backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                    }

                });
                jQuery("<div id='tooltip_commissions'></div>").css({
                    position: "absolute",
                    display: "none",
                    border: "1px solid #fdd",
                    padding: "2px",
                    "background-color": "#fee",
                    opacity: 0.80
                }).appendTo("body");
                jQuery("#rac_each_container_details").bind("plothover", function (event, pos, item) {

                    var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
                    jQuery("#hoverdata").text(str);
                    if (item) {
                        var x = new Date(item.datapoint[0]),
                                y = item.datapoint[1].toFixed(2);
                        var getdate = x.getDate();
                        var getmonth = x.getMonth();
                        getmonth += 1;
                        var getyear = x.getFullYear();
                        var formatted_date = getdate + "-" + getmonth + "-" + getyear;
                        jQuery("#tooltip_commissions").html(item.series.label + "<br />" + formatted_date + " : " + parseInt(y))
                                .css({top: item.pageY + 5, left: item.pageX + 5, color: item.series.color})
                                .fadeIn(200);
                    } else {
                        jQuery("#tooltip_commissions").hide();
                    }

                });
            });
        </script>
        <?php
        $period_options = array('alltime' => 'All Time',
            'last7days' => 'Last 7 Days',
            'thismonth' => 'This Month',
            'lastmonth' => 'Last Month',
            '3months' => '3 Months',
            '6months' => '6 Months',
            'thisyear' => 'This Year',
            'lastyear' => 'Last Year'
        );
        foreach ($period_options as $key => $option) {
            if (isset($_POST['rac_reports_period_selection'])) {
                if ($_POST['rac_reports_period_selection'] == $key) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }
            } else {
                $selected = "";
            }
            $period_formatted_options.='<option value=' . $key . ' ' . $selected . '>' . $option . '</option>';
        }
        $data_options = array('alldata' => 'All Data',
            'abandonedcarts' => 'Abandoned Carts',
            'maillog' => 'Mail Log',
            'recovceredorder' => 'Recovered Orders',
        );
        foreach ($data_options as $key => $option) {
            if (isset($_POST['rac_reports_data_selection'])) {
                if ($_POST['rac_reports_data_selection'] == $key) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }
            } else {
                $selected = "";
            }
            $data_formatted_options.='<option value=' . $key . ' ' . $selected . '>' . $option . '</option>';
        }
        ?>
        <div id="poststuff">
            <div class="postbox ">
                <h3><?php _e('Reports Graph', 'recoverabandoncart'); ?></h3>
                <div class="inside">
                    <div class="rac_selection_area">
                        <p>
                            <select class="rac_reports_data_selection" id="rac_reports_data_selection" name="rac_reports_data_selection"><?php echo $data_formatted_options; ?></select>
                            <select class="rac_reports_period_selection" id="rac_reports_period_selection" name="rac_reports_period_selection"><?php echo $period_formatted_options; ?></select>
                            <input type="submit" value="<?php _e("Filter", 'recoverabandoncart'); ?>" name="rac_submit_view_reports" class="button-secondary"/>
                        </p>
                    </div>
                    <div class="rac_each_container">
                        <div id="rac_each_container_details" class="rac_each_container_details"  style="height:400px;"></div>
                    </div>                     
                </div>
            </div>   
        </div>

        <?php
    }

    public function rac_get_overall_data() {
        if (isset($_POST['rac_reports_data_selection'])) {
            if ($_POST['rac_reports_data_selection'] == 'alldata') {
                $mail_log_format_data = self::rac_get_mail_log_data();
                $abandon_cart_format_data = self::rac_get_abandon_cart_data();
                $recovered_orders_format_data = self::rac_get_reovered_orders_data();
            } elseif ($_POST['rac_reports_data_selection'] == 'abandonedcarts') {
                $abandon_cart_format_data = self::rac_get_abandon_cart_data();
                $recovered_orders_format_data = array();
                $mail_log_format_data = array();
            } elseif ($_POST['rac_reports_data_selection'] == 'maillog') {
                $mail_log_format_data = self::rac_get_mail_log_data();
                $recovered_orders_format_data = array();
                $abandon_cart_format_data = array();
            } else {
                $recovered_orders_format_data = self::rac_get_reovered_orders_data();
                $mail_log_format_data = array();
                $abandon_cart_format_data = array();
            }
        } else {
            $mail_log_format_data = self::rac_get_mail_log_data();
            $abandon_cart_format_data = self::rac_get_abandon_cart_data();
            $recovered_orders_format_data = self::rac_get_reovered_orders_data();
        }

        $this->maillogData = $mail_log_format_data;
        $this->abandonData = $abandon_cart_format_data;
        $this->recoveredData = $recovered_orders_format_data;
    }

    public function rac_get_min_max() {
        $maillog_data = $this->maillogData;
        $abandon_data = $this->abandonData;
        $recovered_data = $this->recoveredData;
        if (is_array($this->maillogData) && !empty($this->maillogData)) {
            $maillog_min = $maillog_data[0];
            $maillog_max = end($maillog_data);
        }
        if (is_array($this->abandonData) && !empty($this->abandonData)) {
            $abandon_min = $abandon_data[0];
            $abandon_max = end($abandon_data);
        }
        if (is_array($this->recoveredData) && !empty($this->recoveredData)) {
            $recovered_min = $recovered_data[0];
            $recovered_max = end($recovered_data);
        }
        if (empty($this->maillogData) && empty($this->recoveredData) && empty($this->abandonData)) {
            $this->minData = array();
            $this->maxData = array();
        } elseif (empty($this->maillogData) && empty($this->recoveredData)) {
            $this->minData = $abandon_min[0];
            $this->maxData = $abandon_max[0];
        } elseif (empty($this->maillogData) && empty($this->abandonData)) {
            $this->minData = $recovered_min[0];
            $this->maxData = $recovered_max[0];
        } elseif (empty($this->recoveredData) && empty($this->abandonData)) {
            $this->minData = $maillog_min[0];
            $this->maxData = $maillog_max[0];
        } elseif (!empty($this->maillogData) && !empty($this->recoveredData) && empty($this->abandonData)) {
            $this->minData = min($maillog_min[0], $recovered_min[0]);
            $this->maxData = max($maillog_max[0], $recovered_max[0]);
        } elseif (!empty($this->maillogData) && empty($this->recoveredData) && !empty($this->abandonData)) {
            $this->minData = min($maillog_min[0], $abandon_min[0]);
            $this->maxData = max($maillog_max[0], $abandon_max[0]);
        } elseif (empty($this->maillogData) && !empty($this->recoveredData) && !empty($this->abandonData)) {
            $this->minData = min($abandon_min[0], $recovered_min[0]);
            $this->maxData = max($abandon_max[0], $recovered_max[0]);
        } else {
            $this->minData = min($abandon_min[0], $recovered_min[0], $maillog_min[0]);
            $this->maxData = max($abandon_max[0], $recovered_max[0], $maillog_max[0]);
        }
    }

    public function rac_add_start_end_data() {
        if (!empty($this->minData)) {
            $mindata = date('Y-m-d', $this->minData / 1000);
            $maxdata = date('Y-m-d', $this->maxData / 1000);
            if ($this->tickSize == 'day') {
                $before_date = strtotime($mindata . '-1 day') * 1000;
                $after_date = strtotime($maxdata . '+1 day') * 1000;
            } else {
                $before_date = strtotime($mindata . '-1 month') * 1000;
                $after_date = strtotime($maxdata . '+1 month') * 1000;
            }

            $first_value = array($before_date, 0);
            $last_value = array($after_date, 0);

            if (is_array($this->maillogData) && !empty($this->maillogData)) {
                array_unshift($this->maillogData, $first_value);
                array_push($this->maillogData, $last_value);
            }
            if (is_array($this->abandonData) && !empty($this->abandonData)) {
                array_unshift($this->abandonData, $first_value);
                array_push($this->abandonData, $last_value);
            }
            if (is_array($this->recoveredData) && !empty($this->recoveredData)) {
                array_unshift($this->recoveredData, $first_value);
                array_push($this->recoveredData, $last_value);
            }
        }
    }

    public function rac_get_mail_log_data() {
        global $wpdb;
        $json_format = array();
        $table_name = $wpdb->prefix . 'rac_email_logs';
        $between = self::rac_get_between();
        if (empty($between)) {
            $mail_logs = $wpdb->get_results(
                    "SELECT "
                    . "UNIX_TIMESTAMP(from_unixtime(date_time,'%Y-%m-%d')) AS Date_Time ,"
                    . "count(date_time) as count "
                    . "FROM $table_name "
                    . "GROUP BY UNIX_TIMESTAMP(from_unixtime(date_time,'%Y-%m-%d'))"
                    . "ORDER BY date_time ASC ", ARRAY_A);
        } else {
            $from_date = $between[0];
            $to_date = $between[1];
            $mail_logs = $wpdb->get_results(
                    "SELECT "
                    . "UNIX_TIMESTAMP(from_unixtime(date_time,'%Y-%m-%d')) AS Date_Time ,"
                    . "count(date_time) as count "
                    . "FROM $table_name "
                    . "WHERE date_time >= $from_date AND date_time<=$to_date "
                    . "GROUP BY UNIX_TIMESTAMP(from_unixtime(date_time,'%Y-%m-%d'))"
                    . "ORDER BY date_time ASC ", ARRAY_A);
        }
        if (!empty($mail_logs)) {
            foreach ($mail_logs as $newkey => $newvalue) {
                $json_format[] = array(($newvalue['Date_Time'] ) * 1000, $newvalue['count']);
            }
        }
        return $json_format;
    }

    public function rac_get_abandon_cart_data() {
        global $wpdb;
        $json_format = array();
        $cart_table_name = $wpdb->prefix . 'rac_abandoncart';
        $between = self::rac_get_between();
        if (empty($between)) {
            $abandon_carts = $wpdb->get_results(
                    "SELECT "
                    . "UNIX_TIMESTAMP(from_unixtime(cart_abandon_time,'%Y-%m-%d')) AS Date_Time ,"
                    . "count(cart_abandon_time) as count "
                    . "FROM $cart_table_name "
                    . "WHERE cart_status='ABANDON'"
                    . "GROUP BY UNIX_TIMESTAMP(from_unixtime(cart_abandon_time,'%Y-%m-%d'))"
                    . "ORDER BY cart_abandon_time ASC ", ARRAY_A);
        } else {
            $from_date = $between[0];
            $to_date = $between[1];
            $abandon_carts = $wpdb->get_results(
                    "SELECT "
                    . "UNIX_TIMESTAMP(from_unixtime(cart_abandon_time,'%Y-%m-%d')) AS Date_Time ,"
                    . "count(cart_abandon_time) as count "
                    . "FROM $cart_table_name "
                    . "WHERE cart_status='ABANDON' "
                    . "AND cart_abandon_time >= $from_date AND cart_abandon_time<=$to_date "
                    . "GROUP BY UNIX_TIMESTAMP(from_unixtime(cart_abandon_time,'%Y-%m-%d'))"
                    . "ORDER BY cart_abandon_time ASC ", ARRAY_A);
        }
        if (!empty($abandon_carts)) {
            foreach ($abandon_carts as $newkey => $newvalue) {
                $json_format[] = array(($newvalue['Date_Time'] * 1000 ), $newvalue['count']);
            }
        }
        return $json_format;
    }

    public function rac_get_reovered_orders_data() {
        global $wpdb;
        $new_array = array();
        $json_format = array();
        $recovered_orders = (array) get_option('fp_rac_recovered_order_ids');
        ksort($recovered_orders);
        if (!empty($recovered_orders)) {
            $array_map = array_map(array('Fp_Rac_Reports_Tab', 'array_recovered_orders_map_function'), $recovered_orders);
            $array_filter = array_filter($array_map);
            if (!empty($array_filter)) {
                $new_format = array_count_values($array_filter);
                $json_format = array();
                if (is_array($new_format) && !empty($new_format)) {
                    foreach ($new_format as $newkey => $newvalue) {
                        if ($newvalue != false) {
                            $json_format[] = array(($newkey * 1000 ), $newvalue);
                        }
                    }
                }
            }
        }
        return $json_format;
    }

    public function array_recovered_orders_map_function($array) {
        $between = self::rac_get_between();
        if (!empty($between)) {
            $from_date = $between[0];
            $todate = $between[1];
            $array = strtotime(date('Y-m-d', strtotime($array['date'])));
            if (($array >= $from_date) && ($array <= $todate)) {
                return $array;
            }
        } else {
            $array = strtotime(date('Y-m-d', strtotime($array['date'])));
            return $array;
        }
    }

    public function rac_get_between() {
        if (isset($_POST['rac_reports_period_selection'])) {
            $period_selection = $_POST['rac_reports_period_selection'];
            if ($period_selection == 'alltime') {
                $between = array();
                $this->tickSize = 'month';
            } elseif ($period_selection == 'last7days') {
                $start_date = strtotime(date("Y-m-d", strtotime('midnight -6 days', current_time('timestamp'))));
                $end_date = strtotime(date("Y-m-d", strtotime('tomorrow midnight', current_time('timestamp'))));
                $between = array($start_date, $end_date);
                $this->tickSize = 'day';
            } elseif ($period_selection == 'thismonth') {
                $start_date = strtotime(date('Y-m-01', current_time('timestamp')));
                $end_date = strtotime(date("Y-m-d", strtotime('tomorrow midnight', current_time('timestamp'))));
                $between = array($start_date, $end_date);
                $this->tickSize = 'day';
            } elseif ($period_selection == 'lastmonth') {
                $first_day_current_month = strtotime(date('Y-m-01', current_time('timestamp')));
                $start_date = strtotime(date('Y-m-01', strtotime('-1 DAY', $first_day_current_month)));
                $end_date = strtotime('midnight', $first_day_current_month) - 1;
                $between = array($start_date, $end_date);
                $this->tickSize = 'day';
            } elseif ($period_selection == '3months') {
                $first_day_current_month = strtotime(date('Y-m-01', current_time('timestamp')));
                $start_date = strtotime(date('Y-m-01', strtotime('-2 months', $first_day_current_month)));
                $end_date = strtotime(date("Y-m-d", strtotime('tomorrow midnight', current_time('timestamp'))));
                $between = array($start_date, $end_date);
                $this->tickSize = 'month';
            } elseif ($period_selection == '6months') {
                $first_day_current_month = strtotime(date('Y-m-01', current_time('timestamp')));
                $start_date = strtotime(date('Y-m-01', strtotime('-5 months', $first_day_current_month)));
                $end_date = strtotime(date("Y-m-d", strtotime('tomorrow midnight', current_time('timestamp'))));
                $between = array($start_date, $end_date);
                $this->tickSize = 'month';
            } elseif ($period_selection == 'thisyear') {
                $start_date = strtotime(date('Y-01-01', current_time('timestamp')));
                $end_date = strtotime(date("Y-m-d", strtotime('tomorrow midnight', current_time('timestamp'))));
                $between = array($start_date, $end_date);
                $this->tickSize = 'month';
            } else {
                $first_day_current_year = strtotime(date('Y-01-01', current_time('timestamp')));
                $start_date = strtotime(date('Y-01-01', strtotime('-1 year', $first_day_current_year)));
                $end_date = strtotime('midnight', $first_day_current_year);
                $between = array($start_date, $end_date);
                $this->tickSize = 'month';
            }
        } else {
            $between = array();
            $this->tickSize = 'month';
        }
        return $between;
    }

}
