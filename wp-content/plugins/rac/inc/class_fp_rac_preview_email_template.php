<?php

class FP_RAC_Preview_Email_Template {

    public static function rac_preview_email_template() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'rac_templates_email';
        $id = $_GET['rac_edit_email'];
        $templates = $wpdb->get_results("SELECT * FROM $table_name WHERE id= $id", ARRAY_A);
        foreach ($templates as $each_template) {
            $mail_logo_added = $each_template['link'];
            $view_template = $each_template['mail'];
            $subject = RecoverAbandonCart::shortcode_in_subject('First Name', 'Last Name', $each_template['subject']);
            $message = self::replace_shortcodes_in_template($each_template);
            if ($mail_logo_added == '') {
                $logo = '';
            } else {
                $logo = '<p style="margin-top:0;"><img src="' . esc_url($mail_logo_added) . '" width="100" height="100"/></a></p>';
            }
            if ($view_template == "HTML") {
                echo self::email_template($subject, $message);
            } else {
                ?>
                <style type="text/css">
                    div.block {
                        background: #ffffff;
                        border-radius: 10px;
                    }
                    div.centered {
                        display: inline-block;
                        width: 2px;
                        height: 350px;
                        padding: 10px 15px;
                        background:#ffffff;
                    }
                </style>
                <div class="block">
                    <div style="margin-left:20px"> <?php echo self::template_ready($message, $logo); ?> </div>
                </div>
                <?php
            }
        }
    }

    //Load Email Template
    public static function email_template($subject, $message) {
        global $woocommerce, $woocommerce_settings;

        // load the mailer class
        $mailer = WC()->mailer();
        $email_heading = $subject;
        $message;
        $abstractClass = new ReflectionClass('WC_Email');
        if (!$abstractClass->isAbstract()) {
            $email = new WC_Email();
            // wrap the content with the email template and then add styles
            $fetch_data = $email->style_inline($mailer->wrap_message($email_heading, $message));
        } else {
            $fetch_data = $mailer->wrap_message($email_heading, $message);
        }
        return $fetch_data;
    }

    //Load Email Template
    public static function template_ready($message, $link) {
        global $woocommerce, $woocommerce_settings;
        $data = $link . $message;
        return $data;
    }

    public static function replace_shortcodes_in_template($each_template) {
        global $to;
        $date = date_i18n(rac_date_format());
        $time = date_i18n(rac_time_format());
        $email_template_id = $each_template['id'];
        $anchor_text_post = $each_template['anchor_text'];
        $message = $each_template['message'];
        $user = get_userdata(get_current_user_id());
        $to = $user->user_email;

        $cart_url = rac_get_page_permalink_dependencies('cart');
        $url_to_click = esc_url_raw(add_query_arg(array('abandon_cart' => '00', 'email_template' => $email_template_id), $cart_url));
        if (get_option('rac_cart_link_options') == '1') {
            $url_to_click = '<a style="color:#' . get_option("rac_email_link_color") . '"  href="' . $url_to_click . '">' . $anchor_text_post . '</a>';
        } elseif (get_option('rac_cart_link_options') == '2') {
            $url_to_click = $url_to_click;
        } else {
            $cart_Text = $anchor_text_post;
            $url_to_click = RecoverAbandonCart::rac_cart_link_button_mode($url_to_click, $cart_Text);
        }
        $message = str_replace('{rac.cartlink}', $url_to_click, $message);
        $message = str_replace('{rac.date}', $date, $message);
        $message = str_replace('{rac.time}', $time, $message);
        $message = str_replace('{rac.firstname}', 'First Name', $message);
        $message = str_replace('{rac.lastname}', 'Last Name', $message);
        $message = str_replace('{rac.Productinfo}', RecoverAbandonCart::sample_productinfo_shortcode(), $message);
        $message = str_replace('{rac.coupon}', 'testcoupo.n1234567890', $message);

        $message = RecoverAbandonCart::rac_unsubscription_shortcode($to, $message);
        add_filter('woocommerce_email_footer_text', array('RecoverAbandonCart', 'rac_footer_email_customization'));
        $message = do_shortcode($message);

        return $message;
    }

}
