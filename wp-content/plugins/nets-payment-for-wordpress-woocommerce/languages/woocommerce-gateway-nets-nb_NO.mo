��    (      \  5   �      p     q     s     �     �     �     �     �  A   �     �     �                <     K     Q     c     x     �     �     �     �     �     �     �               %     8     E  	   J  �   T  B   �  <   "     _     e     |  C   �     �     �  �  �     {	     }	     �	     �	     �	     �	     �	  >   �	     �	     
     
     #
     A
     S
     Y
     f
     z
     �
  $   �
     �
     �
     �
     �
     �
  
     	        %     =  
   M  	   X  |   b  /   �  2        B     I     a  9   ~     �     �        '         &      
                                                          "   !                                  $                   #   (             	                   %    ' API License Key API License email Action Cancel Capture Choose File Connection failed to the License Key API server. Try again later. Debug Denied by 3D Secure. Description Enable Nets Payment Gateway Enable/Disable Image Internal failure. Invalid card number. Language License Deactivation NETS Payment Captured Done  Nets Nets Merchant ID  Nets Payment Gateway Plugin activated.  Plugin license deactivated.  Position Processing... Refused by Issuer. Save Changes Size Test Mode The license could not be deactivated. Use the License Deactivation tab to manually deactivate the license before activating a new license. This controls the description which the user sees during checkout. This controls the title which the user sees during checkout. Title Transaction not found. Unable to make Capture ::  Unable to make payment, Auth Reg Comp Failure / insufficient funds. Unable to make payment. Upload Project-Id-Version: WC Nets Payment Gateway v1.1.6
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-15 20:11+0200
PO-Revision-Date: 2016-06-15 20:12+0200
Last-Translator: Nickolass Jensen <nj@nettpilot.no>
Language-Team: 
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 ' Lisensnøkkel API lisens Epost Handling Avbryt Hente Velg fil Forbindelse til lisens API server sviktet. Prøv igjen senere. Feilsøk Avvist av 3D Secure Beskrivelse Aktivér Nets Payment Gateway Aktiver/Deaktiver Bilde Intern feil. Ugyldig kortnummer. Språk LIsens deaktivering Nets betaling korttrekk gjennomført Nets Nets Merchant ID  Nets Payment Gateway Plugin aktivert Plugin lisens deaktivert  Plassering Behandler Avvist av kortutsteder. Lagre endringer Størrelse Testmodus Lisensen kunne ikke deaktiveres. Benytt lisens deaktiveringefanen for å deaktivere lisensen før du aktiverer en ny lisens. Dette styrer beskrivelsen kunden får i kassen. Dette styrer hvilken tittel brukeren ser i kassen. Tittel Finner ikke transakjon. Kunne ikke trekke på kort : Kan ikke gjennomføre betaling. Kontakt din kortutsteder. Kan ikke foreta betaling. Last opp 