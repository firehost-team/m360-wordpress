<?php
/*
Plugin Name: WC Nets Payment Gateway
Plugin URI: https://www.nettpilot.no/produkt/nets-betalingsmodul-for-wordpress-woocommerce/
Description: Extends WooCommerce. Provides a <a href="https://epayment.bbs.no/Admin/Help/Default.aspx" target="_blank">NETS</a> gateway for WooCommerce.
Version: 1.1.8
Author: Nettpilot
Author URI: www.nettpilot.no
*/

/*  Copyright 2012-2016 Nettpilot */

/**
 * WooCommerce API Manager Integration
 */
if ( ! defined( 'WCNPG_UPRADE_URL' ) ) {
    define( 'WCNPG_UPRADE_URL', 'https://www.nettpilot.no/' );
}

if ( ! defined( 'WCNPG_RENEW_LICENSE_URL' ) ) {
    define( 'WCNPG_RENEW_LICENSE_URL', 'https://www.nettpilot.no/min-konto/' );
}

if ( ! defined( 'WCNPG_SETTINGS_MENU_TITLE' ) ) {
    define( 'WCNPG_SETTINGS_MENU_TITLE', 'Nets Payment Gateway Activation' );
}

if ( ! defined( 'WCNPG_SETTINGS_TITLE' ) ) {
    define( 'WCNPG_SETTINGS_TITLE', 'Nets Payment Gateway License Activation' );
}

if ( ! defined( 'WCNPG_TEXT_DOMAIN' ) ) {
    define( 'WCNPG_TEXT_DOMAIN', 'woocommerce-gateway-nets' );
}

if ( ! defined( 'WCNPG_FILE' ) ) {
    define( 'WCNPG_FILE', __FILE__ );
}

require_once( 'am/am.php' );

// Init Nets Gateway after WooCommerce has loaded
add_action('plugins_loaded', 'init_nets_gateway', 0);
define('JQHFUPLUGINDIRURLNETS',plugin_dir_url(__FILE__));

function init_nets_gateway() {

	// If the WooCommerce payment gateway class is not available, do nothing
    if (!class_exists('WC_Payment_Gateway')) return; 

	// Localisation
	load_plugin_textdomain('woocommerce-gateway-nets', false, dirname( plugin_basename( __FILE__ ) ).'/languages/');

	// Define Plugin Directory
	define('plugin_dir', basename(dirname(__FILE__)));
	
	
	/**
	 * The plugin main class
	 **/
    class WC_nets extends WC_Payment_Gateway {
		
		public function __construct() {
			
			global $woocommerce;

			$this->id 				= 'nets';
            $this->icon 			= plugins_url(plugin_dir . "/img/logo.png");
            $this->has_fields 		= false;
            $this->log 				= new WC_Logger();

            // Load the form fields.
            if ( get_option( WC_Nets_Payment_Gateway_Class::plugin_name() . '_activated' ) == 'Activated' ) {
            	$this->init_form_fields();
			}
			 
            // Load the settings.
            $this->init_settings();

            // Set Variables
            $this->title 				= (isset($this->settings['title'])) ? $this->settings['title'] : '';
            $this->description 			= (isset($this->settings['description'])) ? $this->settings['description'] : '';
            $this->descriptionlogo 		= (isset($this->settings['descriptionlogo'])) ? $this->settings['descriptionlogo'] : '';
            $this->descriptionlogotitle = (isset($this->settings['descriptionlogotitle'])) ? $this->settings['descriptionlogotitle'] : '';
            $this->merchant_id 			= (isset($this->settings['merchant_id'])) ? $this->settings['merchant_id'] : '';
            $this->token 				= html_entity_decode($this->settings['token']);
            $this->language 			= (isset($this->settings['language'])) ? $this->settings['language'] : '';
            $this->capture 			= (isset($this->settings['capture'])) ? $this->settings['capture'] : '';
			
           // $this->autocapture 			= (isset($this->settings['autocapture'])) ? $this->settings['autocapture'] : '';
            //$this->autocaptureadmin 	= (isset($this->settings['autocaptureadmin'])) ? $this->settings['autocaptureadmin'] : '';
            $this->send_name 			= (isset($this->settings['send_name'])) ? $this->settings['send_name'] : '';
            $this->testmode 			= (isset($this->settings['testmode'])) ? $this->settings['testmode'] : ''; 
			$this->mode 				= ($this->testmode != "no") ? 'https://test.epayment.nets.eu/Netaxept.svc?wsdl' : 'https://epayment.bbs.no/Netaxept.svc?wsdl';
			$this->terminal 			= ($this->testmode != "no") ? 'https://test.epayment.nets.eu/terminal/default.aspx' : 'https://epayment.bbs.no/terminal/default.aspx';
            $this->debug 				= (isset($this->settings['debug'])) ? $this->settings['debug'] : '';
			
			$this->selected_currency	= NULL;
            $this->nets_currency 		= array(
											'NOK' => 'NOK',
											'SEK' => 'SEK',
											'DKK' => 'DKK',
											'EUR' => 'EUR',
											'USD' => 'USD',
											'GBP' => 'GBP',
											'AUD' => 'AUD',
											'THB' => 'THB'
										);


			// Subscription support
			$this->supports 			= array(
											'products',
											'subscriptions',
											'subscription_cancellation',
											'subscription_suspension',
											'subscription_reactivation',
											'subscription_amount_changes',
											'subscription_date_changes',
											'subscription_payment_method_change'
										);


			// Multiple currency support
			$this->selected_currency = get_woocommerce_currency();


            // Check if the currency is supported
            if (!isset($this->nets_currency[$this->selected_currency])){
                $this->enabled = "no";
            }
			else{
                $this->enabled = $this->settings['enabled'];
            }

            // Actions
             $this->check_callback();
            //add_action('init', array(&$this, 'check_callback'));

            if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
                add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
            } else {
                add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
            }
						//add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_paymentlogo_tab', 50 );
			//add_action( 'woocommerce_settings_tabs_paymentlogo_tab', __CLASS__ . '::paymentlogo_tab' );
			//add_action( 'woocommerce_update_options_paymentlogo_tab', __CLASS__ . '::update_settings' );
			
			// Subscriptions
			add_action('scheduled_subscription_payment_' . $this->id, array($this, 'scheduled_subscription_payment'), 10, 3);
			add_action('woocommerce_subscriptions_changed_failing_payment_method_' . $this->id, array($this, 'update_failing_payment_method'), 10, 2);
			add_action('valid-nets-callback', array(&$this, 'successful_request'));
            add_action('woocommerce_receipt_nets', array(&$this, 'receipt_page'));

        }
	
		/**
			 * Add a new settings tab to the WooCommerce settings tabs array.
			 *
			 * @param array $paymentlogo_tab Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
			 * @return array $paymentlogo_tab Array of WooCommerce setting tabs & their labels, including the Subscription tab.
			 */
		 public static function add_paymentlogo_tab( $settings_tabs ) {
			$settings_tabs['paymentlogo_tab'] = __( 'Payment Logo', 'woocommerce-gateway-nets' );
			return $settings_tabs;
		}
		
		/**
		 * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
		 *
		 * @uses woocommerce_admin_fields()
		 * @uses self::get_settings()
		 */
		
		public static function paymentlogo_tab() {
			self::jquery_html5_file_upload_hook();
		}
	
		 
		/**
		 * Initiate form fields in backend for merchant
		 */
        public function init_form_fields() {

			$this->form_fields = array(
                'enabled' => array(
								'title' => __('Enable/Disable', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Enable Nets Payment Gateway', 'woocommerce-gateway-nets'),
								'default' => 'yes'
                ),
                'title' => array(
								'title' => __('Title', 'woocommerce-gateway-nets'),
								'type' => 'text',
								'description' => __('This controls the title which the user sees during checkout.', 'woocommerce-gateway-nets'),
								'default' => __('Nets', 'woocommerce-gateway-nets')
                ),
                'description' => array(
								'title' => __('Description', 'woocommerce-gateway-nets'),
								'type' => 'textarea',
								'description' => __('This controls the description which the user sees during checkout.', 'woocommerce-gateway-nets'),
								'default' => __("Pay via nets using credit card or bank transfer.", 'woocommerce-gateway-nets')
                ),
				'merchant_id' => array(
								'title' => __('Nets Merchant ID ', 'woocommerce-gateway-nets'),
								'type' => 'text',
								'description' => __('Please enter your nets Merchant ID; this is needed in order to take payment.', 'woocommerce-gateway-nets'),
								'default' => ''
                ),
                'token' => array(
								'title' => __('Nets Token', 'woocommerce-gateway-nets'),
								'type' => 'text',
								'description' => __('Please enter your Nets Token.', 'woocommerce-gateway-nets'),
								'default' => ''
                ),
                'language' => array(
								'title' => __('Language', 'woocommerce-gateway-nets'),
								'type' => 'select',
								'options' => array(
												'en_GB' => 'English',
												'da_DK' => 'Danish',
												'de_DE' => 'German',
												'fi_FI' => 'Finnish',
												'ru_RU' => 'Russian',
												'no_NO' => 'Norwegian',
												'sv_SE' => 'Swedish'
											),
								'description' => __('Set the language in which the page will be opened when the customer is redirected to nets.', 'woocommerce-gateway-nets'),
								'default' => 'no'
                ),
				'capture' => array(
								'title' => __('Capture', 'woocommerce-gateway-nets'),
								'type' => 'select',
								'options' => array(
												'autocaptureadmin' => 'Direct Capture via WC Admin',
												'autocapture' => 'Auto Capture on checkput'
												
											),
								'description' => __('<b>Auto Capture</b>: Enable nets auto Capture of Payment when order is placed;<br /><b>Direct Capture Admin </b>:  Enable nets Direct Capture of Payment via WooCommerce admin when order status is complete.', 'woocommerce-gateway-nets'),
								'default' => 'autocaptureadmin'
                ), 
               /* 'autocapture' => array(
								'title' => __('Auto Capture', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Enable nets auto Capture of Payment', 'woocommerce-gateway-nets'),
								'default' => 'no'
                ),
				'autocaptureadmin' => array(
								'title' => __('Direct Capture via WC Admin', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Enable nets Direct Capture of Payment via WooCommerce admin when order status is complete', 'woocommerce-gateway-nets'),
								'default' => 'yes'
                ),*/
                'send_name' => array(
								'title' => __('Send name', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Send customers name to Nets along with the order information', 'woocommerce-gateway-nets'),
								'default' => 'no'
                ),
                'testmode' => array(
								'title' => __('Test Mode', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Enable nets Test Mode. ', 'woocommerce-gateway-nets'),
								'default' => 'yes'
                ),
                'debug' => array(
								'title' => __('Debug', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Enable logging (<code>woocommerce/logs/nets.txt</code>)', 'woocommerce-gateway-nets'),
								'default' => 'no'
                ),
				'descriptionlogo' => array(
								'title' => __('Display Logo With Description', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Display Gateway Logos on checkout Description', 'woocommerce-gateway-nets'),
								'default' => 'no'
                ),
				'descriptionlogotitle' => array(
								'title' => __('Display Logo With Title', 'woocommerce-gateway-nets'),
								'type' => 'checkbox',
								'label' => __('Display Gateway Logos With Checkout Title', 'woocommerce-gateway-nets'),
								'default' => 'no'
                )
            );
        }


        /**
         * Admin Panel Options
         * - Options for bits like 'title' and availability on a country-by-country basis
         *
         * @since 1.0.0
         */
        public function admin_options() {
            ?>
            <h3><?php _e('Nets Payment Gateway', 'woocommerce-gateway-nets'); ?></h3>
            <p><?php _e('Nets Payment Gateway works by redirecting user to Nets payment interface to complete the transaction.', 'woocommerce-gateway-nets'); ?></p>
            <?php if ( get_option( WC_Nets_Payment_Gateway_Class::plugin_name() . '_activated' ) != 'Activated' ) : ?>
                <h3 style="color:red;"><?php printf( __('The Nets Payment Gatewway API License Key has not been activated! %sClick here%s to activate the license key and the plugin.', 'woocommerce-gateway-nets'), '<a href="' . esc_url( admin_url( 'options-general.php?page=' . WC_Nets_Payment_Gateway_Class::plugin_name() . '_dashboard' ) ) . '">', '</a>' ) ?></h3>
            <?php endif; ?>
            <table class="form-table">
           	<?php
                if(isset($this->nets_currency[$this->selected_currency])){
                    $this->generate_settings_html();
                }
				else{
			?>
                    <tr valign="top">
                    	<th scope="row" class="titledesc"><?php _e('NETS disabled', 'woocommerce-gateway-nets'); ?></th>
                        <td class="forminp">
                            <fieldset>
                            	<legend class="screen-reader-text"><span><?php _e('NETS disabled', 'woocommerce-gateway-nets'); ?></span></legend>
                                <?php _e('NETS does not support your store currency.', 'woocommerce-gateway-nets'); ?><br>
                            </fieldset>
                    	</td>
                    </tr>
                    <?php
                }
                ?>
            </table><!--/.form-table-->
			<h3><?php _e('Nets Payment Gateway Logo Upload', 'woocommerce-gateway-nets'); ?></h3>
            <?php
			 //add hook to display logo upload GUI on nets gateway
			self::jquery_html5_file_upload_hook();
        }
		
		public static function jqhfu_add_inline_script_nets() {
		?>
		<script type="text/javascript">

		jQuery(function () {
			'use strict';
			
			// Initialize the jQuery File Upload widget:
			jQuery('#mainform').fileupload({
				url: '<?php print(admin_url('admin-ajax.php'));?>'
			});
			
			// Enable iframe cross-domain access via redirect option:
			jQuery('#mainform').fileupload(
				'option',
				'redirect',
				window.location.href.replace(
					/\/[^\/]*$/,
					<?php
					$absoluteurl=str_replace(home_url(),'',JQHFUPLUGINDIRURLNETS);
					print("'".$absoluteurl."cors/result.html?%s'");
					?>
				)
			);
			
			if(jQuery('#mainform')) {
				// Load existing files:
				jQuery('#mainform').addClass('fileupload-processing');
				// Load existing files:
				
				jQuery.ajax({
					// Uncomment the following to send cross-domain cookies:
					//xhrFields: {withCredentials: true},
					url: jQuery('#mainform').fileupload('option', 'url'),
					data : {action: "load_ajax_function"},
					acceptFileTypes: /(\.|\/)(<?php print 'gif|jpeg|jpg|png'; ?>)$/i,
					dataType: 'json',
					context: jQuery('#mainform')[0]
				}).always(function () { 
					jQuery(this).removeClass('fileupload-processing');	    
				}).done(function (result) {
					jQuery(this).fileupload('option', 'done')
								.call(this, jQuery.Event('done'), {result: result});
				});
			}

		});
		</script>
		<?php
		}	
		
		/* Block of code that need to be printed to the form*/
		function jquery_html5_file_upload_hook() {
		?>
		<!-- The file upload form used as target for the file upload widget -->
			<!-- Redirect browsers with JavaScript disabled to the origin page -->
		   <input type="hidden" name="action" value="load_ajax_function" />
			<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
		   <div class="fileupload-buttonbar">
		   <table class="form-table">
					<tr valign="top">
							<th scope="row" class="titledesc"><?php _e('Title', 'woocommerce-gateway-nets'); ?></th>
							<td class="forminp"><input type="text" name="title" multiple class="logotitle" />
								<fieldset>
									<legend class="screen-reader-text"><span><?php _e('File Upload Title', 'woocommerce-gateway-nets'); ?></span></legend>
									<p class="description"><?php _e('File upload title is a required field.', 'woocommerce-gateway-nets'); ?></p>
								</fieldset>
							</td>
					</tr>
					<tr valign="top">
							<th scope="row" class="titledesc"><?php _e('Position', 'woocommerce-gateway-nets'); ?></th>
							<td class="forminp"><input type="text" name="position" multiple class="logoposition" /> 
								<fieldset>
									<legend class="screen-reader-text"><span><?php _e('File Upload Postion', 'woocommerce-gateway-nets'); ?></span></legend>
									<p class="description"><?php _e('Define Position to Display on checkout page. Default will take 0 if you don\'t enter anything. Ex: 1,2,3 etc.', 'woocommerce-gateway-nets'); ?></p>
								</fieldset>
							</td>
					</tr>
					<tr valign="top">
							<th scope="row" class="titledesc"><?php _e('File Upload', 'woocommerce-gateway-nets'); ?></th>
							<td class="forminp">
								<div class="fileupload-buttons">
									<!-- The fileinput-button span is used to style the file input field as button -->
									<label class="jqhfu-file-container">
										<?php _e('Add files...', 'woocommerce-gateway-nets'); ?>
										<input type="file" name="files[]" multiple class="jqhfu-inputfile logofile"> 
									</label>
									<fieldset>
										<legend class="screen-reader-text"><span><?php _e('Choose File', 'woocommerce-gateway-nets'); ?></span></legend>
										<p class="description"><?php _e('You are allowed to upload max 1MB file size and gif,jpeg,jpg,png extension image files.', 'woocommerce-gateway-nets'); ?></p>
									</fieldset> 
									
									<!--<button type="submit" class="start jqhfu-button">Start upload</button>
									<button type="reset" class="cancel jqhfu-button">Cancel upload</button>
									<button type="button" class="delete jqhfu-button">Delete</button> 
									<input type="checkbox" class="toggle">-->
									
									<!-- The global file processing state -->
									<span class="fileupload-process"></span>
								</div>
								
								<br />

								<!-- The global progress state -->
								<div class="fileupload-progress jqhfu-fade" style="display:none;max-width:500px;margin-top:2px;">
									<!-- The global progress bar -->
									<div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
									<!-- The extended global progress state -->
									<div class="progress-extended">&nbsp;</div>
								</div>
								<!-- The table listing the files available for upload/download -->
								<div>&nbsp;</div>
								<div class="jqhfu-upload-download-table">
									<table role="presentation">
										<tbody class="files">
											<tr valign="top">
												<th scope="row"><span class="logotblhead"><?php _e('Image', 'woocommerce-gateway-nets'); ?></span></th>
												 <th scope="row"><span class="logotblhead"><?php _e('Title', 'woocommerce-gateway-nets'); ?></span></th>
												 <th scope="row"><span class="logotblhead"><?php _e('Position', 'woocommerce-gateway-nets'); ?></span></th>
												 <th scope="row"><span class="logotblhead"><?php _e('Size', 'woocommerce-gateway-nets'); ?></span></th>
												 <th scope="row"><span class="logotblhead"><?php _e('Action', 'woocommerce-gateway-nets'); ?></span></th>
											</tr>
										</tbody>
										
									</table>
								</div>		
							</td>
					</tr>
				</table> 
				
			
		</div>
		<!-- The blueimp Gallery widget -->
		<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
			<div class="slides"></div>
			<h3 class="title"></h3>
			<a class="prev"><?php echo "<"; ?></a>
			<a class="next"><?php echo ">"; ?></a>
			<a class="close"><?php echo "x"; ?></a>
			<a class="play-pause"></a>
			<ol class="indicator"></ol>
		</div>
		<!-- The template to display files available for upload -->
		
		<script id="template-upload" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
			<tr class="template-upload jqhfu-fade">
				<td>
					<span class="preview"></span> 
				</td>
				<td>
					<p class="name" style="max-width:190px;overflow-x:hidden;">{%=file.name%}</p>
					<strong class="error"></strong>
				</td>
				<td>
					<p class="position" style="max-width:190px;overflow-x:hidden;">{%=file.position%}</p>
					<strong class="error"></strong>
				</td>
				<td>
					<p class="size"><?php _e('Processing...', 'woocommerce-gateway-nets'); ?></p>
					<div class="progress"></div>
				</td>
				<td>
					{% if (!i && !o.options.autoUpload) { %}
						<button id = "uploadlogobtn" class="start jqhfu-button" disabled><?php _e('Upload', 'woocommerce-gateway-nets'); ?></button>
					{% } %}
					{% if (!i) { %}
						<button class="cancel jqhfu-button"><?php _e('Cancel', 'woocommerce-gateway-nets'); ?></button>
					{% } %}
				</td>
			</tr>
		{% } %}
		</script>
		<!-- The template to display files available for download -->
	
		<script id="template-download" type="text/x-tmpl">
			
		{% for (var i=0, file; file=o.files[i]; i++) { %}
			<tr class="template-download jqhfu-fade">
				<td>
					<span class="preview">
						{% if (file.thumbnailUrl) { %}
							<a href="{%=file.url%}" title="{%=file.title%}" download="{%=file.title%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
						{% } %}
					</span>
				</td>
				<td>
					<p class="name" style="max-width:190px;overflow-x:hidden;">
						<a href="{%=file.url%}" title="{%=file.title%}" download="{%=file.title%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.title%}</a>
					</p>
					{% if (file.error) { %}
						<div><span class="error">Error : {%=file.error%}</span> </div>
					{% } %}
				</td>
				<td>
					<span class="name">{%=file.position%}</span>
				</td>
				<td>
					<span class="size">{%=o.formatFileSize(file.size)%}</span>
				</td>
				<td>
					<button class="delete jqhfu-button" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}&action=load_ajax_function"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>Delete</button>
					<!-- <input type="checkbox" name="delete" value="1" class="toggle"> -->
				</td>
			</tr>
		{% } %}
		</script>
		<?php
		}
		/**
         * There are no payment fields for nets, but we want to show the description if set.
         * */
        public function payment_fields() {
			$descriptiontext = '';
            if($this->description){
			    $descriptiontext.= wpautop(wptexturize($this->description));
            }
			if($this->descriptionlogo == 'yes'){
				$descriptiontext.= $this->nets_gateway_icon( $icon = 'yes', $this->id);
			}
			echo $descriptiontext;
        }

        /**
         * Generate the nets button link
         * */
        public function generate_nets_form($order_id){
			global $woocommerce;

		  	// Initiate WC Order
            $order = new WC_Order($order_id);

		  	// MerchantId provided by Netaxept
            $merchantId = $this->merchant_id;

			// Token provided by Netaxept
            $token = $this->token;

			// WSDL location found in documentation
            $wsdl = $this->mode;

			// Set proper Terminal
			$terminal = $this->terminal;

			// Redirect from Netaxept to your site
            $redirect_url = $order->get_checkout_payment_url(true);

            require_once("classes/ClassOrder.php");
			require_once("classes/ClassRecurring.php");
            require_once("classes/ClassTerminal.php");
            require_once("classes/ClassRegisterRequest.php");
            require_once("classes/ClassEnvironment.php");
            require_once("classes/ClassProcessRequest.php");
            require_once("classes/ClassCustomer.php");

			####  PARAMETERS IN ORDER  ####
            $amount 					= $order->order_total * 100;							// Required
            $currencyCode 				= $this->nets_currency[$this->selected_currency]; 		// Required
            $force3DSecure 				= NULL; 												// Optional
            $orderNumber 				= ltrim( $order->get_order_number(), '#'); 				// Required
            $UpdateStoredPaymentInfo 	= NULL; 												// Optional

			####  PARAMETERS IN ENVIRONMENT  ####
            $Language 					= $this->language; 		// Optional
            $OS 						= NULL; 				// Optional
            $WebServicePlatform 		= 'PHP5'; 				// Required (for Web Services)

			####  PARAMETERS IN TERMINAL  ####
            $autoAuth 					= NULL; 				// Optional
            $paymentMethodList 			= NULL; 				// Optional
            $language	 				= $this->language; 		// Optional
            $orderDescription 			= NULL; 				// Optional
            $redirectOnError 			= $order->get_cancel_order_url(); 				// Optional

			####  PARAMETERS IN REGISTER REQUEST  ####
            $AvtaleGiro 				= NULL; 				// Optional
            $CardInfo 					= NULL; 				// Optional
            $Customer 					= NULL; 				// Optional
            $description 				= NULL; 				// Optional
            $DnBNorDirectPayment	 		= NULL; 				// Optional
            $Environment 				= NULL; 				// Optional
            $MicroPayment 				= NULL; 				// Optional
	    $Recurring					= NULL;					// Optional
            $serviceType 				= "B"; 					// Optional: NULL ==> default = "B" <=> BBS HOSTED
            $transactionId 				= NULL; 				// Optional
            $transactionReconRef 			= NULL; 				// Optional
	    $BankAxess 					= NULL; 				// Optional
            $BankAxessMobile 				= NULL; 				// Optional
            $SwedishBankSwedbank 			= NULL; 				// Optional
            $paypal		 			= NULL; 				// Optional



			//	Check if this is a subscription order
			if(class_exists('WC_Subscriptions_Order') && WC_Subscriptions_Order::order_contains_subscription($order_id)){

				// Check if initial subscription payment is 0, change this to 1
				if( WC_Subscriptions_Order::get_total_initial_payment( $order ) == 0 ) {
					$amount = 100;
				} else {
					$amount = WC_Subscriptions_Order::get_total_initial_payment( $order )*100;
				}

				$product_id = NULL;						// Required
				$order_items = $order->get_items();		// Required

				// Loop through order items to find the subscription
				if(sizeof($order_items) > 0) {

					// Get the Subscription Product ID;
					foreach($order_items as $index => $item){
						$s_period = $order->get_product_from_item($item)->subscription_period; 			// Required

						// Check if Product contains Subscription Period and assign Product ID
						if(!empty($s_period) || $s_period != ""){
							$product_id = $order->get_product_from_item($item)->id;
						}
					}

					// Set Subscription Variables
					$period			= WC_Subscriptions_Product::get_period($product_id);
					$lenght			= WC_Subscriptions_Product::get_length($product_id);
					$expiration		= WC_Subscriptions_Product::get_expiration_date($product_id);

					// Set the Nets Frequence Number
					if(!empty($period) || $period != 0){
						switch($period) :
							case 'day' :
								$subPeriod = "0";
								break;
							case 'week' :
								$subPeriod = "7";
								break;
							case 'month' :
								$subPeriod = "28";
								break;
							case 'year' :
								$subPeriod = "364";
								break;
						endswitch;
					}

					// Set expiration date
					if(!empty($expiration) || $expiration != 0){
						$expDate = date('Ymd', strtotime($expiration));
					}
					else{
						$expDate = (date('Y') + 50) . date('md');
					}

					if(!empty($lenght) || $lenght != 0){
						$subLenght = $lenght;
					}
				}

				####  PARAMETERS IN RECURRING ORDER  ####
				$Type			= "R";					// Optional parameter (unless Pan Hash is supplied, then it is required)
				$Frequency		= $subPeriod; 			// Required (if type "R")
				$ExpiryDate		= $expDate; 			// Required (if type "R")
				$PanHash		= NULL;					// Optional

				####  RECURRING OBJECT  ####
				$Recurring = new Recurring(
								$ExpiryDate,
								$Frequency,
								$Type,
								$PanHash
							);


				if($this->debug == 'yes') :
					$array_info = array(
								  	'Testing Period: ' . WC_Subscriptions_Product::get_period($product_id) . ' Correct Period: ' . $subPeriod,
									'Testing Lenght: ' . WC_Subscriptions_Product::get_length($product_id) . ' Correct Length: ' . $subLenght,
									'Testing Expiration: ' . WC_Subscriptions_Product::get_expiration_date($product_id) . ' Correct Format: ' . $expDate,
									'Testing Paragraph: ' . WC_Subscriptions_Product::get_price_string($product_id)
								  );

                	$this->log->add('nets', var_export($array_info, true));
            	endif;
			} // End If Subscription Check


			####  ENVIRONMENT OBJECT  ####
			$Environment  = new Environment(
								$Language,
								$OS,
								$WebServicePlatform
							);

			####  TERMINAL OBJECT  ####
            $Terminal 	= new Terminal(
								$autoAuth,
								$paymentMethodList,
								$language,
								$orderDescription,
								$redirectOnError,
								$redirect_url
							);

			$ArrayOfItem = NULL; // no goods for Klarna ==> normal transaction

			####  ORDER OBJECT  ####
            $Order 	= new Order(
							$amount,
							$currencyCode,
							$force3DSecure,
							$ArrayOfItem,
							$orderNumber,
							$UpdateStoredPaymentInfo
						);

			####  PARAMETERS IN CUSTOMER  ####
			$customerAddress1                   = null;		// Optional parameter (required if DnBNorDirectPayment)
			$customerAddress2                   = null;		// Optional parameter
			$customerCompanyName                = null;		// Optional parameter
			$customerCompanyRegistrationNumber  = null;		// Optional parameter
			$customerCountry                    = strtoupper($order->billing_country);		// Optional parameter

			// Send name?
			if( $this->send_name == 'yes' ) {
				$customerFirstName                  = $order->billing_first_name;		// Optional parameter (required if DnBNorDirectPayment)
				$customerLastName                   = $order->billing_last_name;		// Optional parameter (required if DnBNorDirectPayment)
			} else {
				$customerFirstName                  = null;		// Optional parameter (required if DnBNorDirectPayment)
				$customerLastName                   = null;		// Optional parameter (required if DnBNorDirectPayment)
			}

			$customerNumber                     = null;		// Optional parameter
			$customerEmail                      = null;		// Optional parameter
			$customerPhoneNumber                = null;		// Optional parameter
			$customerPostcode                   = null;		// Optional parameter (required if DnBNorDirectPayment)
			$customerSocialSecurityNumber       = null;		// Optional parameter
			$customerTown                       = null;		// Optional parameter (required if DnBNorDirectPayment)

			####  CUSTOMER OBJECT  ####
			$Customer = new Customer(
			  $customerAddress1,
			  $customerAddress2,
			  $customerCompanyName,
			  $customerCompanyRegistrationNumber,
			  $customerCountry,
			  $customerNumber,
			  $customerEmail,
			  $customerFirstName,
			  $customerLastName,
			  $customerPhoneNumber,
			  $customerPostcode,
			  $customerSocialSecurityNumber,
			  $customerTown
			);

			####  START REGISTER REQUEST  ####
            $RegisterRequest = new RegisterRequest(
									$AvtaleGiro,
									$CardInfo,
									$Customer,
									$description,
									$DnBNorDirectPayment,
									$Environment,
									$MicroPayment,
									$Order,
									$Recurring,
									$serviceType,
									$Terminal,
									$transactionId,
									$BankAxess,
									$BankAxessMobile,
									$SwedishBankSwedbank,
									$Paypal,
									$transactionReconRef
								);

			####  ARRAY WITH REGISTER PARAMETERS  ####
          	$InputParametersOfRegister 	= array(
                								"token" => $token,
                								"merchantId" => $merchantId,
                								"request" => $RegisterRequest
            								);

			if($this->debug == 'yes') :
                $this->log->add('nets', 'InputParametersOfRegister: ' . var_export($InputParametersOfRegister, true));
            endif;

			####  START REGISTER CALL  ####
            try{
                if(strpos($_SERVER["HTTP_HOST"], 'uapp') > 0){
                    // Creating new client having proxy
                    $client = new SoapClient($wsdl, array('proxy_host' => "isa4", 'proxy_port' => 8080, 'trace' => true, 'exceptions' => true));
                }
				else {
                    // Creating new client without proxy
                    $client = new SoapClient($wsdl, array('trace' => true, 'exceptions' => true));
                }

                $OutputParametersOfRegister = $client->__call('Register', array("parameters" => $InputParametersOfRegister));

				// RegisterResult
                $RegisterResult 			= $OutputParametersOfRegister->RegisterResult;
                $terminal_parameters 		= "?merchantId=" . $merchantId . "&transactionId=" . $RegisterResult->TransactionId;
                $process_parameters 		= "?transactionId=" . $RegisterResult->TransactionId;
                $transactionId 				= $RegisterResult->TransactionId;

				header('Location:' . $terminal . $terminal_parameters);

				if($this->debug == 'yes') :
                	$this->log->add('nets', 'generate_nets_form - RegisterResult: ' . var_export($terminal.$terminal_parameters, true));
            	endif;

				if($this->debug == 'yes') :
                	$this->log->add('nets', 'Output: ' . var_export($OutputParametersOfRegister, true));
            	endif;

            } catch (SoapFault $fault){
                $resError = __('Unable to authenticate merchant.','woocommerce-gateway-nets');

				if(isset($resError)){
					wc_add_notice( $resError, 'error' );
                 }
            }
        }


        /**
         * Process the payment and return the result
         * */
        public function process_payment($order_id) {
            global $woocommerce;

			//setcookie("orid", $order_id, 0, COOKIEPATH, COOKIE_DOMAIN, false, true);

			$order = new WC_Order( $order_id );

			if($this->settings['merchant_id'] == "" || $this->settings['token'] == ""){
				$woocommerce->add_error(__('Nets is not configured properly, Please choose another payment method.','woocommerce-gateway-nets'));
              	return FALSE;
            }

			return array(
					'result' 	=> 'success',
					'redirect' 	=> add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, $order->get_checkout_payment_url( true )))
			);
        }


		/**
         * receipt_page
         * */
        public function receipt_page($order) {
            //echo '<p>' . __('Thank you for your order, Unable to pay with NETS.', 'woocommerce-gateway-nets') . '</p>';
            echo $this->generate_nets_form($order);
        }


		/**
         * Check for NETS Response
         * */
        public function check_callback() {
			global $woocommerce;

			require_once("classes/ClassQueryRequest.php");
            require_once("classes/ClassProcessRequest.php");

			$wsdl = $this->mode;
            $transactionId = "";

			if(isset($_GET['transactionId'])) {
                $transactionId = $_GET['transactionId'];
            }

            $responseCode = "";

			if(isset($_GET['responseCode'])) {
                $responseCode = $_GET['responseCode'];
            }

            $order_id = "";

			if ($transactionId != '' && $responseCode == 'OK'){
			   	$queryAuth 		= $this->CallingQueryProcess($transactionId);
			   	$querySale 		= $this->CallingQueryProcess($transactionId);
			   	$order_id 		= $this->get_order_id( $querySale['orderid'] );
			   	$order 			= new WC_Order( $order_id );
				$orderAmount 	= $order->order_total * 100;


				if(!in_array('Auth', (array) $queryAuth)) {
                    $description = "description of AUTH operation";
                    $operation = "AUTH"; //for authentication only

					if($this->capture != 'autocaptureadmin'){  
                         $operation = "SALE"; // AUTHENTICATION AND CAPTURE AMOUNT AT A SAME TIME
                    }

                    $AuthResult = $this->NetsProcess($transactionId, $operation, $description, $orderAmount);

					if($AuthResult->ResponseCode == "OK"){
						//print 'AUTH-CAPTURE done';


						// Add
						if(isset($querySale['panhash']) && isset($querySale['orderid'])){
							add_post_meta($order_id, 'nets_panhash_id', $querySale['panhash']);
							$order->add_order_note(__('NETS subscription ID (panhash): ', 'woocommerce-gateway-nets') . $querySale['panhash']);
						}

						if($this->debug == 'yes') :
                			$this->log->add('nets', 'Query Panhash: ' . $querySale['panhash'] . ' Order ID: ' . $order_id );
            			endif;

						$_POST = stripslashes_deep($_GET);

                        $this ->successful_request($_GET);
                    }

					$transactionId = $AuthResult->TransactionId;
                }
            }
			elseif($responseCode == 'Cancel'){
				// Return customer to cart page
				$order = new WC_Order( $order_id );
				wp_redirect( $order->get_cancel_order_url() );
				exit;
            }
			else{
				return;
            }
        }


		/**
         * Check for NETS Response for Subscription Order
         * */
        public function check_subscription($tId, $oId, $amount) {
			global $woocommerce;

			require_once("classes/ClassQueryRequest.php");
            require_once("classes/ClassProcessRequest.php");

			$wsdl 				= $this->mode;
            $transactionId 		= "";
			$order_id 			= "";
			$orderAmount	 	= $amount;

			if(isset($tId)) {
                $transactionId = $tId;
            }

			if(isset($oId)) {
                $order_id = $this->get_order_id( $oId );
            }

			if(isset($amount)) {
                $transactionAmount = $amount;
            }

			if ($transactionId != ''){
                $queryAuth = $this->CallingQueryProcess($transactionId);

				if(!in_array('Auth', (array) $queryAuth)) {
                    $description = "description of AUTH operation";
                    $operation = "SALE";

                    $AuthResult = $this->NetsProcess($transactionId, $operation, $description, $orderAmount);

					if($AuthResult->ResponseCode == "OK"){
						$querySale = $this->CallingQueryProcess($transactionId);

						if($this->debug == 'yes') :
                			$this->log->add('nets', 'Query SUBBER: ' . var_export($querySale, true));
            			endif;
                    }

					return;
                }
            }
			else{
				return;
            }
        }



		/**
		 * scheduled_subscription_payment function.
		 *
		 * @param $amount_to_charge float The amount to charge.
		 * @param $order WC_Order The WC_Order object of the order which the subscription was purchased in.
		 * @param $product_id int The ID of the subscription product for which this payment relates.
		 * @access public
		 * @return void
		 */
		function scheduled_subscription_payment($amount_to_charge, $order, $product_id) {
			$result = $this->process_subscription_payment($order, $amount_to_charge);

			if (  $result == false ) {

				// Debug
				if ($this->debug=='yes') $this->log->add( 'nets', 'Scheduled subscription payment failed for order ' . $order->id );
				WC_Subscriptions_Manager::process_subscription_payment_failure_on_order($order, $product_id);

			}
			else{

				// Debug
				if ($this->debug=='yes') $this->log->add( 'nets', 'Scheduled subscription payment succeeded for order ' . $order->id );
				WC_Subscriptions_Manager::process_subscription_payments_on_order($order);

			}
		}



		/**
		* process_subscription_payment function.
		*
		* Since we use a Merchant handled subscription, we need to generate the
		* recurrence request.
		*/
		function process_subscription_payment($order = '', $amount = 0) {
			global $woocommerce;

			// MerchantId provided by Netaxept
            $merchantId = $this->merchant_id;

			// Token provided by Netaxept
            $token = $this->token;

			// WSDL location found in documentation
            $wsdl = $this->mode;

			// Set proper Terminal
			$terminal = $this->terminal;

			// Redirect from Netaxept to your site
            $redirect_url = '';

			// Get PanHash ID for subscription
			$pnHash = get_post_meta($order->id, 'nets_panhash_id', true);

			require_once("classes/ClassOrder.php");
			require_once("classes/ClassRecurring.php");
            require_once("classes/ClassTerminal.php");
            require_once("classes/ClassRegisterRequest.php");
            require_once("classes/ClassEnvironment.php");
            require_once("classes/ClassProcessRequest.php");
            require_once("classes/ClassCustomer.php");

			####  PARAMETERS IN ORDER  ####
            $amount 					= $amount * 100; 							// Required
            $currencyCode 				= $order->get_order_currency(); 			// Required
            $force3DSecure 				= NULL; 									// Optional
            $orderNumber 				= ltrim( $order->get_order_number(), '#'); 	// Required
            $UpdateStoredPaymentInfo 	= NULL; 									// Optional

			####  PARAMETERS IN ENVIRONMENT  ####
            $Language 					= NULL; 				// Optional
            $OS 						= NULL; 				// Optional
            $WebServicePlatform 		= 'PHP5'; 				// Required (for Web Services)

			####  PARAMETERS IN TERMINAL  ####
            $autoAuth 					= NULL; 				// Optional
            $paymentMethodList 			= NULL; 				// Optional
            $language	 				= NULL; 				// Optional
            $orderDescription 			= NULL; 				// Optional
            $redirectOnError 			= $order->get_cancel_order_url(); 				// Optional

			####  PARAMETERS IN REGISTER REQUEST  ####
            $AvtaleGiro 				= NULL; 				// Optional
            $CardInfo 					= NULL; 				// Optional
            $Customer 					= NULL; 				// Optional
            $description 				= NULL; 				// Optional
            $DnBNorDirectPayment	 	= NULL; 				// Optional
            $Environment 				= NULL; 				// Optional
            $MicroPayment 				= NULL; 				// Optional
	    $Recurring					= NULL;					// Optional
            $serviceType 				= "C"; 					// Optional: NULL ==> default = "B" <=> BBS HOSTED
            $transactionId 				= NULL; 				// Optional
            $transactionReconRef 		= NULL; 				// Optional
	    $BankAxess 					= NULL; 				// Optional
            $BankAxessMobile 			= NULL; 				// Optional
            $SwedishBankSwedbank 			= NULL; 				// Optional
            $Paypal		 			= NULL; 				// Optional

			####  PARAMETERS IN RECURRING ORDER  ####
			$Type			= "R";				// Optional parameter (unless Pan Hash is supplied, then it is required)
			$Frequency		= NULL; 			// Required (if type "R")
			$ExpiryDate		= NULL; 			// Required (if type "R")
			$PanHash		= $pnHash;			// Optional

			####  RECURRING OBJECT  ####
			$Recurring = new Recurring(
							$ExpiryDate,
							$Frequency,
							$Type,
							$PanHash
						);


			####  ENVIRONMENT OBJECT  ####
			$Environment  = new Environment(
								$Language,
								$OS,
								$WebServicePlatform
							);

			####  TERMINAL OBJECT  ####
            $Terminal 	= new Terminal(
								$autoAuth,
								$paymentMethodList,
								$language,
								$orderDescription,
								$redirectOnError,
								$redirect_url
							);

			$ArrayOfItem = NULL; // no goods for Klarna ==> normal transaction

			####  ORDER OBJECT  ####
            $nets_order 	= new Order(
							$amount,
							$currencyCode,
							$force3DSecure,
							$ArrayOfItem,
							$orderNumber,
							$UpdateStoredPaymentInfo
						);

  			####  PARAMETERS IN CUSTOMER  ####
			$customerAddress1                   = null;		// Optional parameter (required if DnBNorDirectPayment)
			$customerAddress2                   = null;		// Optional parameter
			$customerCompanyName                = null;		// Optional parameter
			$customerCompanyRegistrationNumber  = null;		// Optional parameter
			$customerCountry                    = strtoupper($order->billing_country);		// Optional parameter

			// Send name?
			if( $this->send_name == 'yes' ) {
				$customerFirstName				= $order->billing_first_name;		// Optional parameter (required if DnBNorDirectPayment)
				$customerLastName				= $order->billing_last_name;		// Optional parameter (required if DnBNorDirectPayment)
			} else {
				$customerFirstName				= null;		// Optional parameter (required if DnBNorDirectPayment)
				$customerLastName				= null;		// Optional parameter (required if DnBNorDirectPayment)
			}

			$customerNumber                     = null;		// Optional parameter
			$customerEmail                      = null;		// Optional parameter
			$customerPhoneNumber                = null;		// Optional parameter
			$customerPostcode                   = null;		// Optional parameter (required if DnBNorDirectPayment)
			$customerSocialSecurityNumber       = null;		// Optional parameter
			$customerTown                       = null;		// Optional parameter (required if DnBNorDirectPayment)

			####  CUSTOMER OBJECT  ####
			$Customer = new Customer(
			  $customerAddress1,
			  $customerAddress2,
			  $customerCompanyName,
			  $customerCompanyRegistrationNumber,
			  $customerCountry,
			  $customerNumber,
			  $customerEmail,
			  $customerFirstName,
			  $customerLastName,
			  $customerPhoneNumber,
			  $customerPostcode,
			  $customerSocialSecurityNumber,
			  $customerTown
			);

			####  START REGISTER REQUEST  ####
            $RegisterRequest = new RegisterRequest(
									$AvtaleGiro,
									$CardInfo,
									$Customer,
									$description,
									$DnBNorDirectPayment,
									$Environment,
									$MicroPayment,
									$nets_order,
									$Recurring,
									$serviceType,
									$Terminal,
									$transactionId,
									$BankAxess,
									$BankAxessMobile,
									$SwedishBankSwedbank,
									$Paypal,
									$transactionReconRef
								);

			####  ARRAY WITH REGISTER PARAMETERS  ####
          	$InputParametersOfRegister 	= array(
                								"token" => $token,
                								"merchantId" => $merchantId,
                								"request" => $RegisterRequest
            								);

			####  START REGISTER CALL  ####
            try{
                if(strpos($_SERVER["HTTP_HOST"], 'uapp') > 0){
                    $client = new SoapClient($wsdl, array('proxy_host' => "isa4", 'proxy_port' => 8080, 'trace' => true, 'exceptions' => true));
                }
				else {
                    $client = new SoapClient($wsdl, array('trace' => true, 'exceptions' => true));
                }

                $OutputParametersOfRegister = $client->__call('Register', array("parameters" => $InputParametersOfRegister));

				// RegisterResult
                $transID		= $OutputParametersOfRegister->RegisterResult->TransactionId;
				$response 		= $this->check_subscription($transID, $orderNumber, $amount);

				if($this->debug == 'yes') :
                	$this->log->add('nets', 'Subscription payment to: ' . $wsdl);
            	endif;

				$order->add_order_note( sprintf(__('Nets transaction ID: %s.', 'woocommerce-gateway-nets'), $transID) );				update_post_meta( $order->id, '_nets_transaction', $transID);
				return true;

            } catch (SoapFault $fault){
                $resError = __('Unable to authenticate merchant.','woocommerce-gateway-nets');

				if(isset($resError)){
					wc_add_notice($resError, 'error');
					$order->add_order_note( sprintf(__('Error when processing recurring payment: %s.', 'woocommerce-gateway-nets'), $fault) );
                 }
            }

			return false;
		}



		/**
		 * Update the customer token IDs for a subscription after a customer used the gateway to successfully complete the payment
		 * for an automatic renewal payment which had previously failed.
		 *
		 * @param WC_Order $original_order The original order in which the subscription was purchased.
		 * @param WC_Order $renewal_order The order which recorded the successful payment (to make up for the failed automatic payment).
		 * @return void
		 */
		function update_failing_payment_method($original_order, $renewal_order){
			update_post_meta($original_order->id, 'nets_panhash_id', get_post_meta($renewal_order->id, 'nets_panhash_id', true));
		}



        /**
         * Successful Payment!
         * */
        public function successful_request($posted) {
            global $woocommerce;
			//$querySale 		= $this->CallingQueryProcess($transactionId);
			$querySale 		= $this->CallingQueryProcess($posted['transactionId']);
			$order_id 		= $this->get_order_id( $querySale['orderid'] );
			$order 			= new WC_Order( $order_id );

			if($this->debug == 'yes') :
				foreach ($posted as $key => $value) {
					$tmp_log .= $key . '=' . $value . "\r\n";
                }

				$this->log->add('nets', 'Returning values from NETS: ' . $tmp_log);
            endif;

			$transactionId = "";

			if(isset($_GET['transactionId'])){
                $transactionId = $_GET['transactionId'];
            }

            $responseCode = "";

			if (isset($_GET['responseCode'])){
                $responseCode = $_GET['responseCode'];
            }



			if($posted['responseCode'] == 'OK'){
				$order = new WC_Order($order_id);
                $order->add_order_note(__('NETS payment completed. NETS transaction ID: ', 'woocommerce-gateway-nets') . $posted['transactionId']);			
				update_post_meta( $order_id, '_nets_transaction', $posted['transactionId']);
				$order->payment_complete();

				// empty cart
                $cart = new WC_Cart();
                $cart->empty_cart();
                $cart = NULL;

				header('Location:' . $this->get_return_url($order));
			}
			else{
				$resError = __('Unable to make payment.','woocommerce-gateway-nets');

				if(isset($resError)){
					wc_add_notice($resError, 'error');
                }

				 return FALSE;
            }

            exit;
        }

        public function CallingQueryProcess($transId) {
            global $woocommerce;

			require_once("classes/ClassQueryRequest.php");

			$resultArry = array();
            $transactionId = $transId;
            $wsdl = $this->mode;
            $QueryRequest = new QueryRequest(
                            	$transactionId
            				);

			####  ARRAY WITH QUERY PARAMETERS  ####
            $InputParametersOfQuery = array(
											"token" => $this->token,
                							"merchantId" => $this->merchant_id,
                							"request" => $QueryRequest
            							);

			####  START QUERY CALL  ####
            try{
                if(strpos($_SERVER["HTTP_HOST"], 'uapp') > 0) {
                    // Creating new client having proxy
                    $client = new SoapClient($wsdl, array('proxy_host' => "isa4", 'proxy_port' => 8080, 'trace' => true, 'exceptions' => true));
                }
				else{
                    // Creating new client without proxy
                    $client = new SoapClient($wsdl, array('trace' => true, 'exceptions' => true));
                }

                $OutputParametersOfQuery = $client->__call('Query', array("parameters" => $InputParametersOfQuery));


				// Add PanHash (for recurring payments) to $resultArry if it exist
				if(isset($OutputParametersOfQuery->QueryResult->CardInformation->PanHash)){
					$resultArry['panhash'] = $OutputParametersOfQuery->QueryResult->CardInformation->PanHash;
				}

				// Add Order ID (for recurring payments) to $resultArry if it exist
				if(isset($OutputParametersOfQuery->QueryResult->OrderInformation->OrderNumber)){
					$resultArry['orderid'] = $OutputParametersOfQuery->QueryResult->OrderInformation->OrderNumber;
				}

				if(empty($OutputParametersOfQuery->QueryResult->History->TransactionLogLine->Operation)) {
					foreach ((array) $OutputParametersOfQuery->QueryResult->History->TransactionLogLine as $value) {
                        $resultArry[] = $value->Operation;
                    }

                    return $resultArry;
                }
				else{
                    $resultArry[] = $OutputParametersOfQuery->QueryResult->History->TransactionLogLine->Operation;
                }

				if($this->debug == 'yes') :
                	$this->log->add('nets', 'Outputter Calling PANHASH Query: ' . var_export($OutputParametersOfQuery, true));
            	endif;

				if($this->debug == 'yes') :
                	$this->log->add('nets', 'Outputter Calling PANHASH Array: ' . var_export($resultArry, true));
            	endif;

                return $resultArry;
            } catch (SoapFault $fault) {
                ## Do some error handling in here...
                $resError = __('Unable to make payment.','woocommerce-gateway-nets');

				if(isset($resError)){
					wc_add_notice($resError, 'error');
                 }

				 return FALSE;
            }
        }

		public function NetsProcess($transId, $myoperation, $operationDesc, $amount) {
            global $woocommerce;

			require_once("classes/ClassProcessRequest.php");

			$description 			= $operationDesc;
            $operation 				= $myoperation;
            $transactionAmount	 	= $amount;
            $transactionReconRef 	= "";
            $transactionId 			= $transId;
            $wsdl 					= $this->mode;

			####  PROCESS OBJECT  ####
            $ProcessRequest = new ProcessRequest(
									$description,
									$operation,
									$transactionAmount,
									$transactionId,
									$transactionReconRef
            					);
            $InputParametersOfProcess = array(
											"token" => $this->token,
											"merchantId" => $this->merchant_id,
											"request" => $ProcessRequest
										);

			try{
                if(strpos($_SERVER["HTTP_HOST"], 'uapp') > 0) {
                    // Creating new client having proxy
                    $client = new SoapClient($wsdl, array('proxy_host' => "isa4", 'proxy_port' => 8080, 'trace' => true, 'exceptions' => true));
                }
				else{
                    // Creating new client without proxy
                    $client = new SoapClient($wsdl, array('trace' => true, 'exceptions' => true));
                }

                $OutputParametersOfProcess = $client->__call('Process', array("parameters" => $InputParametersOfProcess));

				return $OutputParametersOfProcess->ProcessResult;

            } catch (SoapFault $fault) {
                $eResponse = isset($fault->detail->BBSException->Result->ResponseCode) ? $fault->detail->BBSException->Result->ResponseCode :'';
				
				switch($eResponse){
					case "99":
                    	$resError  =  __('Unable to make payment, Auth Reg Comp Failure / insufficient funds.','woocommerce-gateway-nets');
						$resError .=  $fault->faultstring;
						break;
                  	case "14":
                    	$resError = __('Invalid card number.','woocommerce-gateway-nets');
                    	break;
                  	case "25":
                    	$resError = __('Transaction not found.','woocommerce-gateway-nets');
                    	break;
                  	case "30":
                    	$resError = __('KID invalid or Field missing PAN.','woocommerce-gateway-nets');
                    	break;
                  	case "84":
                    	$resError = __('Original transaction rejected.','woocommerce-gateway-nets');
                    	break;
                  	case "86":
                    	$resError = __('Transaction already reversed.','woocommerce-gateway-nets');
                    	break;
                   	case "96":
                    	$resError = __('Internal failure.','woocommerce-gateway-nets');
                    	break;
                   	case "04":
						break;
                   	case "05":
						break;
                   	case "06":
						break;
                   	case "07":
                    	$resError = __('Refused by Issuer.','woocommerce-gateway-nets');
                    	break;
                   	case "MZ":
                    	$resError = __('Denied by 3D Secure.','woocommerce-gateway-nets');
                    	break;
                  	default:
                    	$resError = __('Unable to make payment.','woocommerce-gateway-nets');
                    	break;
                }

				if(isset($resError)){
					wc_add_notice($resError, 'error' );
                }

				return $eResponse;
			}

        }


		/**
		 * Get the order ID. Check to see if SON and SONP is enabled and
		 *
		 * @global type $wc_seq_order_number
		 * @global type $wc_seq_order_number_pro
		 * @param type $order_number
		 * @return type
		 */
		private function get_order_id( $order_number ) {

			// Get Order ID by order_number() if the Sequential Order Number plugin is installed
			if ( class_exists( 'WC_Seq_Order_Number' ) ) {

				global $wc_seq_order_number;

				$order_id = $wc_seq_order_number->find_order_by_order_number( $order_number );

				if ( 0 === $order_id ) {
					$order_id = $order_number;
				}

			// Get Order ID by order_number() if the Sequential Order Number Pro plugin is installed
			} elseif ( class_exists( 'WC_Seq_Order_Number_Pro' ) ) {

				global $wc_seq_order_number_pro;

				$order_id = $wc_seq_order_number_pro->find_order_by_order_number( $order_number );

				if ( 0 === $order_id ) {
					$order_id = $order_number;
				}

			} else {

				$order_id = $order_number;
			}

			return $order_id;

		} // end function
		
		/**
		 * Order Capture function when order complete
		 *
		 * @param type $order_number
		 * @return type
		 */
		
		public function nets_woocommerce_payment_complete($order_id){ 			 
			// Nets payment object for setting retrive.
			$nets 			= new WC_nets();  
			$autocaptureadmin = $nets->capture;  			
			if($autocaptureadmin == 'autocapture') return;
			
			// Get Transaction id from query
			global $wpdb;
			/*$query = "SELECT ordernotes.comment_content
            FROM {$wpdb->prefix}comments AS ordernotes 
            WHERE (ordernotes.comment_content LIKE '%NETS transaction ID%'
                OR ordernotes.comment_content LIKE '%Nets Transaction ID%')
            AND ordernotes.comment_author = 'WooCommerce'
            AND ordernotes.comment_approved = '1'
            AND ordernotes.comment_type = 'order_note'
            AND ordernotes.comment_post_ID = '$order_id' ORDER BY ordernotes.`comment_ID` DESC LIMIT 1
			";*/			$transactionId = get_post_meta( $order_id, '_nets_transaction', true );			 
			/* $orderNotes = $wpdb->get_results($query);			echo '<pre>';			print_R($orderNotes);			exit;  */ 
			/*			if(sizeof($orderNotes) > 0) {
					// Get the Transaction ID;
					$transactionId = '';
					foreach($orderNotes as $index => $notes){
						$ordernotes = $notes->comment_content; 	 
						// From this text : 1. NETS payment completed. NETS transaction ID: 6e75cdc16055475db4a692bfecedc8f3 2. Nets Transaction ID: : f06c4a0e4147481fa2a5c363a1a13c6a.	 	
						$transactionIdArray = explode("ID:",$ordernotes);
						if(sizeof($transactionIdArray) > 0) {
							$transactionId = ltrim($transactionIdArray[1],': ');
						} 
				}
			}		*/
			
			if ($transactionId != ''){
				// Process for Capturing order
				require_once("classes/ClassProcessRequest.php");
				$description = "Description of Capture operation";
                $operation = "CAPTURE"; // CAPTURE AMOUNT AT A SAME TIME
				$transactionReconRef = "";
			
			   	$order 			= new WC_Order( $order_id ); // Order Object
				$transactionAmount 	= $order->order_total * 100;
				$ProcessRequest = new ProcessRequest(
				  $description,
				  $operation,
				  $transactionAmount,
				  $transactionId,
				  $transactionReconRef
				); 
				$InputParametersOfProcess = array(
					  "token"       => $nets->token,
					  "merchantId"  => $nets->merchant_id,
					  "request"     => $ProcessRequest 
				);
				try{
					  if (strpos($_SERVER["HTTP_HOST"], 'uapp') > 0){
					  // Creating new client having proxy
					  $client = new SoapClient($nets->mode, array('proxy_host' => "isa4", 'proxy_port' => 8080, 'trace' => true,'exceptions' => true));
					  }else{
						// Creating new client without proxy
						$client = new SoapClient($nets->mode, array('trace' => true,'exceptions' => true ));
					  }
					  
					  $OutputParametersOfProcess = $client->__call('Process' , array("parameters"=>$InputParametersOfProcess));
					  $ProcessResult = $OutputParametersOfProcess->ProcessResult; 
					  if ($ProcessResult->ResponseCode == "OK"){
						$order->add_order_note(__('NETS Payment Captured Done ', 'woocommerce-gateway-nets'));
						return true; 
					  }
					} // End try
					catch (SoapFault $fault){
						$resError = __('Unable to make Capture :: ','woocommerce-gateway-nets').$fault->faultstring;
						$order->add_order_note($resError);
						return FALSE;
					}
				}
		} // end function
		
		// Add javascript and css for logo upload
		public static function enqueue_scripts_nets($hook) {
			 
			$stylepath=JQHFUPLUGINDIRURLNETS.'css/';
			$scriptpath=JQHFUPLUGINDIRURLNETS.'js/';
			
			wp_enqueue_style ( 'blueimp-gallery-style', $stylepath.'blueimp-gallery.min.css' );
			wp_enqueue_style ( 'jquery.fileupload-style', $stylepath.'jquery.fileupload.css' );

			if(!wp_script_is('jquery')) {
				wp_enqueue_script ( 'jquery', $scriptpath .'jquery.min.js',array(),'',false);
			}
			wp_enqueue_script ( 'jquery-ui-script', $scriptpath . 'jquery-ui.min.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jtmpl-script', $scriptpath . 'tmpl.min.js',array('jquery'),'',true);
			wp_enqueue_script ( 'load-image-all-script', $scriptpath . 'load-image.all.min.js',array('jquery'),'',true);
			wp_enqueue_script ( 'canvas-to-blob-script', $scriptpath . 'canvas-to-blob.min.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-blueimp-gallery-script', $scriptpath . 'jquery.blueimp-gallery.min.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-iframe-transport-script', $scriptpath . 'jquery.iframe-transport.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-script', $scriptpath . 'jquery.fileupload.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-process-script', $scriptpath . 'jquery.fileupload-process.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-image-script', $scriptpath . 'jquery.fileupload-image.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-audio-script', $scriptpath . 'jquery.fileupload-audio.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-video-script', $scriptpath . 'jquery.fileupload-video.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-validate-script', $scriptpath . 'jquery.fileupload-validate.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-ui-script', $scriptpath . 'jquery.fileupload-ui.js',array('jquery'),'',true);
			wp_enqueue_script ( 'jquery-fileupload-jquery-ui-script', $scriptpath . 'jquery.fileupload-jquery-ui.js',array('jquery'),'',true);
			return ;
		}
		// uploade handler to upload logo
		public static function jqhfu_load_ajax_function_nets()
		{
			/* Include the upload handler */
			require 'UploadHandler.php';
			global $current_user;
			get_currentuserinfo();
			$current_user_id=$current_user->ID;
			if(!isset($current_user_id) || $current_user_id=='')
				$current_user_id='guest';
			$upload_handler = new UploadHandler(null,$current_user_id,true,null);
			die(); 
		}
		
		// Chanage icon to display on order checkout for nets payment gateway
		public static function nets_gateway_icon( $icon, $id ) {
			
			if ($id === 'nets') { // Filter Nets Gateway
				$nets 	= new WC_nets();
				$descriptionlogotitle = $nets->descriptionlogotitle;
				if($icon == 'yes')$descriptionlogotitle = 'yes';
				if($descriptionlogotitle == 'yes'){
					$pluginDir = plugin_dir_path( __FILE__ ).'img/logos/thumbnail/'; // Image Directory path
					//$dirLogos = scandir($pluginDir) ; //scandir($pluginDir,1) for shorting 
					$dirLogos = glob($pluginDir.'*.{jpg,jpeg,gif,png}', GLOB_BRACE);
					//echo '<pre>';
					//print_R($dirLogos);
					//exit; 
					if(count($dirLogos) > 0){
						$imgUrl = '';
						foreach ($dirLogos as $key => $value){ // Get logos fro the directory
						  
							  if (!in_array($value,array(".",".."))){ 
								
								//$iconUrl = plugins_url(plugin_dir . "/img/logos/thumbnail/".$value);
								//$iconImg = $pluginDir.$value;
								$iconUrl = plugins_url(plugin_dir . "/img/logos/thumbnail/".basename($value));
								$iconImg = $value;
								    
								$explodeFile =  explode('~',basename($value));
								$fileTitle = 'Nets';
								if(count($explodeFile) > 2){
									$fileTitle = $explodeFile[1];
								}
								$styleblock = '';
								if($icon == 'yes'){
									$styleblock = "display: unset;";
								}
								  if(file_exists($iconImg)){
									$imgUrl .= '<img style="padding-right: 2px;'.$styleblock.'" src="' . $iconUrl . '" alt="' . $fileTitle . '" />';
								  }
								} 
						 
					   } 
					   if(!$imgUrl){
						$imgUrl = $icon;
					   }
					   return $imgUrl;
				  } 
			   }
			   return $icon;
			}
		}
	}

/**
 * Add the gateways to WooCommerce
 **/
function add_nets_gateway($methods) {
	
    $methods[] = 'WC_nets';
    return $methods;
}
	
// Capture when order complete
add_action ('woocommerce_order_status_completed', 'WC_nets::nets_woocommerce_payment_complete');	
//add_action( 'init', 'WC_nets::check_callback' );  

add_filter('woocommerce_payment_gateways', 'add_nets_gateway');
// change gateway icon with payment logoes
add_filter( 'woocommerce_gateway_icon', 'WC_nets::nets_gateway_icon', 10, 2 );

add_action('wp_ajax_load_ajax_function', 'WC_nets::jqhfu_load_ajax_function_nets');		
add_action('wp_ajax_nopriv_load_ajax_function', 'WC_nets::jqhfu_load_ajax_function_nets');	

if(isset($_GET['section']) && isset($_GET['tab'])){	
	if($_GET['section'] == 'nets' && $_GET['tab'] == 'checkout'){		
		/* Add the resources */		
		add_action( 'admin_enqueue_scripts','WC_nets::enqueue_scripts_nets' );				
		/* Load the inline script */		
		add_action( 'admin_footer', 'WC_nets::jqhfu_add_inline_script_nets' );			
	}
}

	
}
