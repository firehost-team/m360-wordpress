

jQuery(document).ready(function($){
   
        var form_data = {};
        $('#mainform').find('input').each(function () {
            form_data[this.name] = $(this).val();
        });
        console.log(form_data);
        $('#mainform').ajaxForm({
            url: ajax_url, // there on the admin side, do-it-yourself on the front-end
            data: form_data,
            type: 'POST',
            contentType: 'json',
            success: function (response) {
                alert(response.message);
            },
            error: function (response) {
                console.log('error');
            }
        });
        
});