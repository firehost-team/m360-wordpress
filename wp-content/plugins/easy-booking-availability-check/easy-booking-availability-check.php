<?php
/*
Plugin Name: Easy Booking: Availability Check
Plugin URI: https://www.easy-booking.me/addon/availability-check/
Description: WooCommerce Easy Booking add-on to manage stocks and availabilities
Version: 1.6.5
Author: @_Ashanna
Author URI: https://easy-booking.me/
Licence : GPL
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Check' ) ) :

class Easy_Booking_Availability_Check {

    protected static $_instance = null;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {

        // Check if WooCommerce Easy Booking is active
        if ( $this->ebac_easy_booking_is_active() ) {

            $plugin = plugin_basename( __FILE__ );

            // Init plugin
            add_action( 'plugins_loaded', array( $this, 'ebac_init' ), 20 );

            // Add plugin settings link
            add_filter( 'plugin_action_links_' . $plugin, array( $this, 'ebac_add_settings_link' ) );

            // Add notices
            add_action( 'admin_notices', array( $this, 'ebac_add_notices') );

            // Add network notices
            add_action( 'network_admin_notices', array($this, 'easy_booking_availability_add_network_notices') );

        }

    }

    /**
    *
    * Check if WooCommerce Easy Booking is active
    *
    * @return bool
    *
    **/
    private function ebac_easy_booking_is_active() {

        // Get active plugins
        $active_plugins = (array) get_option( 'active_plugins', array() );

        if ( is_multisite() ) {
            $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
        }

        // Return true if Easy Booking is active
        return ( array_key_exists( 'woocommerce-easy-booking-system/woocommerce-easy-booking.php', $active_plugins ) || in_array( 'woocommerce-easy-booking-system/woocommerce-easy-booking.php', $active_plugins ) );

    }

    /**
    *
    * Init plugin
    *
    **/
    public function ebac_init() {

        // Don't init plugin if WooCommerce Easy Booking isn't at least version 1.9.
        if ( ! method_exists( WCEB(), 'wceb_get_version' ) ) {
            return;
        }
        
        // Define constants
        $this->ebac_define_constants();

        // load plugin textdomain
        $this->ebac_load_textdomain();

        // Common includes
        $this->ebac_includes();

        // Admin includes
        if ( is_admin() ) {
            $this->ebac_admin_includes(); 
        }
        
        // Frontend includes
        if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
            $this->ebac_frontend_includes();
        }

    }

    /**
    *
    * Define constants
    *
    **/
    private function ebac_define_constants() {
        // Plugin directory path
        define( 'EBAC_PLUGIN_FILE', __FILE__ );
    }

    /**
    *
    * Load plugin textdomain for translations
    *
    **/
    private function ebac_load_textdomain() {
        load_plugin_textdomain( 'easy_booking_availability', false, basename( dirname(__FILE__) ) . '/languages/' );
    }

    /**
    *
    * Common includes
    *
    **/
    private function ebac_includes() {
        include_once( 'easy-booking-availability-check-update.php' );
        include_once( 'includes/ebac-functions.php' );
        include_once( 'includes/ebac-date-functions.php' );
        include_once( 'includes/ebac-product-functions.php' );
        include_once( 'includes/class-ebac-ajax.php' );
        include_once( 'includes/class-ebac-cart.php' );
        include_once( 'includes/admin/class-ebac-order.php' );
        include_once( 'includes/imports/class-ebac-imports.php' );
    }

    /**
    *
    * Admin includes
    *
    **/
    private function ebac_admin_includes() {
        include_once( 'includes/admin/class-ebac-admin-assets.php' );
        include_once( 'includes/settings/class-ebac-settings.php' );
        include_once( 'includes/imports/class-ebac-list-imports.php' );     
        include_once( 'includes/reports/class-ebac-reports.php' );   
    }

    /**
    *
    * Frontend includes
    *
    **/
    private function ebac_frontend_includes() {
        include_once( 'includes/class-ebac-product-view.php' );
        include_once( 'includes/class-ebac-assets.php' );
    }

    /**
    *
    * Add settings link
    *
    **/
    public function ebac_add_settings_link( $links ) {
        $settings_link = '<a href="admin.php?page=easy-booking-availability-check">' . __('Settings', 'easy_booking_availability') . '</a>';
        array_push( $links, $settings_link );

        return $links;
    }

    /**
    *
    * Add notices
    *
    **/
    public function ebac_add_notices() {

        if ( is_multisite() ) {
            return;
        }

        if ( ! method_exists( WCEB(), 'wceb_get_version' ) ) {
            include_once( 'includes/admin/views/html-ebac-notice-update.php' );
            return;
        }

        $settings    = get_option('easy_booking_availability_settings');
        $license_key = isset( $settings['easy_booking_availability_license_key'] ) ? $settings['easy_booking_availability_license_key'] : false;

        if ( empty( $license_key ) ) {
            $license_key = false;
        }

        $screen = get_current_screen();

        // Don't display notices on the plugin settings page
        if ( in_array( $screen->id, array( 'easy-booking_page_easy-booking-availability-check' ) ) ) {
            return;
        }

        // Display a notice to init availabilities
        if ( get_option( 'easy_booking_display_notice_init' ) !== '1' ) {
            include_once( 'includes/admin/views/html-ebac-notice-init.php' );
        }

        if ( get_option( 'easy_booking_display_notice_init' ) == '1' && get_option( 'easy_booking_display_notice_init_after_update' ) !== '1' ) {
            include_once( 'includes/admin/views/html-ebac-notice-init-after-update.php' );
        }

        // Display license key notice
        if ( ! is_multisite() && get_option( 'easy_booking_display_notice_ebac_license' ) !== '1' && ! $license_key ) {
            include_once( 'includes/admin/views/html-ebac-notice-license-key.php' );
        }
    }

    /**
    *
    * Add network notices
    *
    **/
    public function easy_booking_availability_add_network_notices() {
        $settings    = get_option('easy_booking_global_settings');
        $license_key = ! isset( $settings['easy_booking_availability_license_key'] ) || empty( $settings['easy_booking_availability_license_key'] ) ? false : $settings['easy_booking_availability_license_key'];

        $screen = get_current_screen();
        
        // Don't display notices on the plugin settings page
        if ( in_array( $screen->id, array( 'toplevel_page_easy-booking-network' ) ) ) {
            return;
        }

        // Display license key notice
        if ( get_option( 'easy_booking_display_notice_ebac_license' ) !== '1' && ! $license_key ) {
            include_once( 'includes/admin/views/html-ebac-notice-license-key.php' );
        }
    }

}

function EBAC() {
    return Easy_Booking_Availability_Check::instance();
}

EBAC();

endif;