msgid ""
msgstr ""
"Project-Id-Version: Easy Booking : Availability Check\n"
"POT-Creation-Date: 2018-03-16 16:46+0100\n"
"PO-Revision-Date: 2018-03-16 16:47+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;esc_html__;"
"esc_html_e\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SearchPath-0: .\n"

#: easy-booking-availability-check-update.php:70
msgid "Your license key is empty or invalid. "
msgstr ""

#: easy-booking-availability-check-update.php:71
msgid "Save your license key"
msgstr ""

#: easy-booking-availability-check-update.php:72
msgid " and "
msgstr ""

#: easy-booking-availability-check-update.php:73
msgid "Recheck updates."
msgstr ""

#: easy-booking-availability-check.php:171
#: includes/settings/includes/ebac-settings.php:15
#: includes/settings/views/ebac-html-settings.php:14
msgid "Settings"
msgstr ""

#: includes/class-ebac-cart.php:76 includes/class-ebac-cart.php:108
#: includes/class-ebac-cart.php:169
msgid "View Cart"
msgstr ""

#: includes/class-ebac-cart.php:77 includes/class-ebac-cart.php:170
#, php-format
msgid "You cannot add another &quot;%s&quot; to your cart."
msgstr ""

#: includes/class-ebac-cart.php:87 includes/class-ebac-cart.php:180
#, php-format
msgid ""
"You cannot add &quot;%s&quot; to the cart because the product is out of "
"stock."
msgstr ""

#: includes/class-ebac-cart.php:98 includes/class-ebac-cart.php:189
#, php-format
msgid ""
"You cannot add that amount of &quot;%s&quot; to the cart because there is "
"not enough stock (%s remaining)."
msgstr ""

#: includes/class-ebac-cart.php:109
#, php-format
msgid ""
"You cannot add that amount to the cart &mdash; we have %s in stock and you "
"already have %s in your cart."
msgstr ""

#: includes/class-ebac-cart.php:272
msgid "An item which is no longer available was removed from your cart."
msgstr ""

#: includes/class-ebac-product-view.php:123
#: includes/class-ebac-product-view.php:129
#: includes/class-ebac-product-view.php:139
#: includes/class-ebac-product-view.php:297
#: includes/class-ebac-product-view.php:311
#: includes/class-ebac-product-view.php:325
msgid "In stock"
msgstr ""

#: includes/class-ebac-product-view.php:125
#: includes/class-ebac-product-view.php:303
msgid "Out of stock"
msgstr ""

#: includes/class-ebac-product-view.php:131
#: includes/class-ebac-product-view.php:313
#, php-format
msgid "Only %s left in stock"
msgstr ""

#: includes/class-ebac-product-view.php:133
#: includes/class-ebac-product-view.php:141
#: includes/class-ebac-product-view.php:315
#: includes/class-ebac-product-view.php:327
#, php-format
msgid "%s in stock"
msgstr ""

#: includes/admin/views/html-ebac-notice-init-after-update.php:13
msgid ""
"You have just updated Easy Booking: Availability Check to a new version. You "
"need to update the availabilities."
msgstr ""

#: includes/admin/views/html-ebac-notice-init-after-update.php:18
#: includes/admin/views/html-ebac-notice-init.php:18
#: includes/settings/class-ebac-settings.php:251
msgid "Initialize availabilities"
msgstr ""

#: includes/admin/views/html-ebac-notice-init.php:13
msgid ""
"To get started, check the \"Manage stock?\" checkbox on your bookable "
"products and set their stock amount. Then, if you already have orders, "
"initialize availabilities."
msgstr ""

#: includes/admin/views/html-ebac-notice-license-key.php:15
msgid ""
"Save your license key to get automatic updates for Easy Booking : "
"Availability Check."
msgstr ""

#: includes/admin/views/html-ebac-notice-license-key.php:27
#: includes/imports/class-ebac-list-imports.php:89
msgid "Save"
msgstr ""

#: includes/admin/views/html-ebac-notice-update.php:13
msgid ""
"This version of Easy Booking: Availability Check requires at least "
"WooCommerce Easy Booking version 1.9. Please update WooCommerce Easy Booking "
"to its latest version."
msgstr ""

#: includes/imports/class-ebac-imports.php:143
msgid "Import bookings"
msgstr ""

#: includes/imports/class-ebac-imports.php:144
msgid "Add booking"
msgstr ""

#: includes/imports/class-ebac-imports.php:184
msgid "Product is required"
msgstr ""

#: includes/imports/class-ebac-imports.php:189
msgid "Start date is required"
msgstr ""

#: includes/imports/class-ebac-imports.php:194
msgid "Quantity booked is required"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:18
#: includes/settings/views/ebac-html-settings.php:15
msgid "Import"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:19
#: includes/settings/includes/ebac-settings.php:59
msgid "Imports"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:26
msgid "No products found."
msgstr ""

#: includes/imports/class-ebac-list-imports.php:90
msgid "Cancel"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:116
#: includes/imports/class-ebac-list-imports.php:120
#: includes/reports/views/ebac-html-reports.php:13
#: includes/reports/views/ebac-html-reports.php:17
msgid "Search for a product&hellip;"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:178
msgid "Product(s)"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:179
msgid "Start"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:180
msgid "End (optional)"
msgstr ""

#: includes/imports/class-ebac-list-imports.php:181
msgid "Quantity booked"
msgstr ""

#: includes/reports/class-ebac-reports.php:28
#: includes/settings/includes/ebac-settings.php:51
msgid "Availabilities"
msgstr ""

#: includes/reports/views/ebac-html-reports.php:22
msgid "Get availabilities"
msgstr ""

#: includes/settings/class-ebac-settings.php:166
#: includes/settings/includes/ebac-network-settings.php:9
msgid "Easy Booking : Availability Check Settings"
msgstr ""

#: includes/settings/class-ebac-settings.php:184
#: includes/settings/class-ebac-settings.php:270
msgid "Enter your license key"
msgstr ""

#: includes/settings/class-ebac-settings.php:206
msgid ""
"Display or not product availabilities on the front-end. For better "
"rendering, they won't be displayed on mobile and for grouped products. "
"\"Colors\" will display a colored circle, \"Amount\" the amount of stock "
"available, and \"Amount and colors\" the colored amount of stock available. "
"High availabilities are colored in green and low availabilities in orange."
msgstr ""

#: includes/settings/class-ebac-settings.php:208
msgid "None"
msgstr ""

#: includes/settings/class-ebac-settings.php:209
msgid "Colors"
msgstr ""

#: includes/settings/class-ebac-settings.php:210
msgid "Amount"
msgstr ""

#: includes/settings/class-ebac-settings.php:211
msgid "Amount and colors"
msgstr ""

#: includes/settings/class-ebac-settings.php:223
msgid ""
"Make your products unavailable for x days after the end of a booking. The "
"unavailability period will also be applied before the booked dates to "
"prevent other bookings from overlapping."
msgstr ""

#: includes/settings/class-ebac-settings.php:239
msgid ""
"Check to automatically delete manually imported bookings when they are over. "
"Useful to remove unnecessary data and clean the imports page."
msgstr ""

#: includes/settings/includes/ebac-network-settings.php:16
#: includes/settings/includes/ebac-settings.php:25
msgid "License Key"
msgstr ""

#: includes/settings/includes/ebac-settings.php:35
msgid "Display Availabilities?"
msgstr ""

#: includes/settings/includes/ebac-settings.php:43
msgid "Unavailability period after a booking?"
msgstr ""

#: includes/settings/includes/ebac-settings.php:66
msgid "Automatically delete past imported bookings?"
msgstr ""
