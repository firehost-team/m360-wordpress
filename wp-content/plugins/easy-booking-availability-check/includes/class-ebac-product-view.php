<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Product_View' ) ) :

class Easy_Booking_Availability_Product_View {

	public function __construct() {
        add_filter( 'woocommerce_product_backorders_allowed', array( $this, 'ebac_allow_backorders_for_bookable_products' ), 999, 2 );
        add_filter( 'woocommerce_quantity_input_max', array( $this, 'ebac_quantity_input_max' ), 10, 2 );
        add_filter( 'woocommerce_available_variation', array( $this, 'ebac_variation_bookable_attribute' ), 10, 3 );
        add_filter( 'woocommerce_get_availability', array( $this, 'ebac_get_availability' ), 10, 2 );
        add_filter( 'easy_booking_fragments', array( $this, 'ebac_new_stock_fragment' ) );
        add_action( 'woocommerce_grouped_product_list_before_price', array( $this, 'ebac_grouped_product_ids' ), 10, 1 );
    }

    /**
    *
    * Automatically allows backorders for bookbale products, so several products with different dates
    * can be added to cart at the same time
    *
    * @param bool $allowed
    * @param int $product_id
    * @return bool
    *
    **/
    public function ebac_allow_backorders_for_bookable_products( $allowed, $product_id ) {
        $product = wc_get_product( $product_id );

        if ( wceb_is_bookable( $product ) && $product->get_stock_quantity() ) {
            $allowed = true;
        }

        return $allowed;
    }

    /**
    *
    * Sets max quantity input - Fix for WooCommerce 2.7
    *
    * @param int $max
    * @param WC_Product $product
    * @return int $max
    *
    **/
    public function ebac_quantity_input_max( $max, $product ) {
        
        if ( wceb_is_bookable( $product ) && true === $product->backorders_allowed() && $product->get_stock_quantity() ) {
            $max = $product->get_stock_quantity();
        }

        return $max;
    }

    /**
    *
    * Set the variation's availability, regardless if the backorders are allowed
    *
    * @param array $available_variations
    * @param WC_Product $product
    * @param WC_Product_Variation variation
    * @return array $available_variations
    *
    **/
    public function ebac_variation_bookable_attribute( $available_variations, $product, $variation ) {

        if ( ! $variation->managing_stock() ) {
            return $available_variations;
        }
        
        if ( wceb_is_bookable( $variation ) && ( ! $variation->get_stock_quantity() || ! $variation->is_in_stock() ) ) {
            $available_variations['is_in_stock'] = false;
        }

        if ( ! wceb_is_bookable( $variation ) && ! $variation->is_in_stock() && $variation->get_backorders() === 'no' ) {
            $available_variations['backorders_allowed'] = $variation->get_backorders();
            $available_variations['is_in_stock'] = false;
        }
        
        return $available_variations;
    }

    /**
    *
    * Get the variation's availability text, regardless if the backorders are allowed
    *
    * @param array $availability
    * @param WC_Product $product
    * @return array
    *
    **/
    public function ebac_get_availability( $availability, $product ) {

        if ( ! wceb_is_bookable( $product ) ) {
            return $availability;
        }

        $stock_amount = '';

        if ( $product->managing_stock() ) {
            $stock_amount = $product->get_stock_quantity();
        } else {
            $stock_amount = $product->is_in_stock() ? '' : 0;
        }

        return $this->ebac_get_availability_from_stock_amount( $stock_amount );

    }

    private function ebac_get_availability_from_stock_amount( $stock_amount ) {
        return array(
            'availability' => $this->ebac_get_availability_text( $stock_amount ),
            'class'        => $this->ebac_get_availability_class( $stock_amount )
        );
    }

    private function ebac_get_availability_text( $stock_amount ) {

        if ( $stock_amount === '' ) {
            $availability_text = __( 'In stock', 'woocommerce' );
        } else if ( $stock_amount <= 0 ) {
            $availability_text = __( 'Out of stock', 'woocommerce' );
        } else if ( $stock_amount <= get_option( 'woocommerce_notify_low_stock_amount' ) ) {

            if ( get_option( 'woocommerce_stock_format' ) === 'no_amount' ) {
                $availability_text = __( 'In stock', 'woocommerce' );
            } else if ( get_option( 'woocommerce_stock_format' ) === 'low_amount' ) {
                $availability_text = sprintf( __( 'Only %s left in stock', 'woocommerce' ), $stock_amount );
            } else {
                $availability_text = sprintf( __( '%s in stock', 'woocommerce' ), $stock_amount );
            }

        } else {

            if ( get_option( 'woocommerce_stock_format' ) === 'no_amount' || get_option( 'woocommerce_stock_format' ) === 'low_amount' ) {
                $availability_text = __( 'In stock', 'woocommerce' );
            } else {
                $availability_text = sprintf( __( '%s in stock', 'woocommerce' ), $stock_amount );
            }
        
        }

        return apply_filters( 'ebac_availability_text', $availability_text, $stock_amount );
    }

    private function ebac_get_availability_class( $stock_amount ) {

        $class = '';
        
        if ( $stock_amount !== '' && $stock_amount <= 0 ) {
            $class = 'out-of-stock';
        } else {
            $class = 'in-stock';
        }

        return apply_filters( 'ebac_availability_class', $class, $stock_amount );

    }

    /**
    *
    * Adds Ajax fragments - Checks and displays product stock
    *
    * @param array $fragments
    * @return array $fragments
    *
    **/
    public function ebac_new_stock_fragment( $fragments ) {
        $product_id   = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : '';
        $variation_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : '';
        $children     = isset( $_POST['children'] ) ? array_map( 'absint', $_POST['children'] ) : array();

        $id = ! empty( $variation_id ) ? $variation_id : $product_id;

        $product = wc_get_product( $id ); // Product or variation object

        if ( ! $product ) {
            return;
        }

        $managing_stock = $product->managing_stock();

        if ( $product->is_type('grouped') || $product->is_type('bundle') ) {

            // Get children ids
            $product_children = wceb_get_product_children_ids( $product );

            $stocks_left = array();
            if ( $product_children ) foreach ( $product_children as $child_id ) {
                $child = wc_get_product( $child_id );

                if ( ! $child->managing_stock() ) {
                    continue;
                }

                $stocks_left[$child_id] = ebac_get_product_stock( $child_id );
            }

            asort( $stocks_left );
            $stock_left = implode( '-', $stocks_left );

            if ( $product->is_type('bundle') && $managing_stock ) {
                $stocks_left[$id] = ebac_get_product_stock( $product_id );
            }

        } else if ( $managing_stock ) {
            $stock_left = ebac_get_product_stock( $product_id, $variation_id );
        }

        if ( isset( $stock_left ) ) {

            if ( $product->is_type('grouped') || $product->is_type('bundle') ) {

                // Get children ids
                $product_children = wceb_get_product_children_ids( $product );

                if ( $product_children ) foreach ( $product_children as $child_id ) {
                    $child = wc_get_product( $child_id );

                    if ( ! $child->managing_stock() ) {
                        continue;
                    }

                    $stock_text = $this->ebac_get_availability_from_stock_amount( $stocks_left[$child_id] );

                    $stock_info[$child_id] = array(
                        'text'  => $stock_text['availability'],
                        'class' => $stock_text['class'],
                        'qty'   => $stocks_left[$child_id]
                    );

                    ob_start();
                    $fragments['stock_info'] = $stock_info;
                    ob_get_clean();

                }

                // Get main bundle product data
                if ( $product->is_type('bundle') && $managing_stock ) {

                    $min_stock = min( $stocks_left );
                    $stock_text = $this->ebac_get_availability_from_stock_amount( $min_stock );

                    $stock_info[$id] = array(
                        'text'  => $stock_text['availability'],
                        'class' => $stock_text['class'],
                        'qty'   => $min_stock
                    );

                    ob_start();
                    $fragments['stock_info'] = $stock_info;
                    ob_get_clean();

                }
                
            } else {

                $stock_text = $this->ebac_get_availability_from_stock_amount( $stock_left );

                $response = array(
                    'availability' => $stock_text['availability'],
                    'class'        => $stock_text['class']
                );

                ob_start();

                $fragments['p.stock']    = '<p class="stock ' . esc_attr( $response['class'] ) . '">' . esc_html( $response['availability'] ) . '</p>';
                $fragments['stock_left'] = $stock_left;

                ob_get_clean();

            }

        }

        return $fragments;
    }

    /**
    *
    * Gets text to display depending on the stock level
    *
    * @param obj WC_Product or WC_Product_Variation - $product
    * @param int - $stock_left
    * @return array $info
    *
    **/
    private function ebac_get_stock_text( $product, $stock_left ) {

        switch ( $stock_left ) {

            case $stock_left == '' :

                $availability = __( 'In stock', 'woocommerce' );

            break;

            case ( $stock_left <= 0 ) :

                $availability = __( 'Out of stock', 'woocommerce' );
                $class        = 'out-of-stock';

            break;

            case ( $stock_left <= get_option( 'woocommerce_notify_low_stock_amount' ) ) :  

                if ( get_option( 'woocommerce_stock_format' ) === 'no_amount' ) {
                    $availability = __( 'In stock', 'woocommerce' );
                } else if ( get_option( 'woocommerce_stock_format' ) === 'low_amount' ) {
                    $availability = sprintf( __( 'Only %s left in stock', 'woocommerce' ), $stock_left );
                } else {
                    $availability = sprintf( __( '%s in stock', 'woocommerce' ), $stock_left );
                }
                
                $class = 'in-stock';

            break;

            default :

                if ( get_option( 'woocommerce_stock_format' ) === 'no_amount' || get_option( 'woocommerce_stock_format' ) === 'low_amount' ) {
                    $availability = __( 'In stock', 'woocommerce' );
                } else {
                    $availability = sprintf( __( '%s in stock', 'woocommerce' ), $stock_left );
                }

                $class = 'in-stock';
            
            break;

        }

        $info = array(
            'availability' => $availability,
            'class'        => $class
        );

        return $info;
    }

    /**
    *
    * Add markup on grouped products to get each grouped product ID on the frontend
    *
    * @param obj WC_Product or WC_Product_Variation - $product
    *
    **/
    public function ebac_grouped_product_ids( $product ) {

        $product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;
        
        echo '<td class="grouped_product_id" data-product_id="' . $product_id . '"></td>';
    }
}

return new Easy_Booking_Availability_Product_View();

endif;