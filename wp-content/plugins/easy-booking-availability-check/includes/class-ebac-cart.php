<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Cart' ) ) :

class Easy_Booking_Availability_Cart {

    public function __construct() {
        add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'ebac_check_stock_before_add_to_cart' ), 25, 4 );
        add_filter( 'woocommerce_update_cart_validation', array( $this, 'ebac_check_stock_on_cart_update' ), 10, 4 );
        add_action( 'woocommerce_check_cart_items', array( $this, 'ebac_check_dates_on_checkout'), 10, 1 );
    }

    /**
    *
    * Ensures that there is enough stock left when adding product to cart
    *
    * @param bool $passed - passed validation
    * @param int $product_id
    * @param int $quantity - quantity added to cart
    * @param int $variation_id
    * @return bool $passed - whether it passed validation or not
    *
    **/
    public function ebac_check_stock_before_add_to_cart( $passed = true, $product_id, $quantity, $variation_id = '' ) {

        // If validation have already failed before, return false
        if ( $passed === false ) {
            return false;
        }

        // Get product ID
        $id = ! empty( $variation_id ) ? $variation_id : $product_id;

        // Get product
        $product = wc_get_product( $id );

        // If product is bookable and manages stock
        if ( wceb_is_bookable( $product ) && $product->managing_stock() ) {

            // Get booking session
            $booking_session = WC()->session->get( 'booking' );

            // If product booking session is set
            if ( isset( $booking_session[$id] ) && ! empty( $booking_session[$id] ) ) {

                $start = $booking_session[$id]['start']; // Formatted dates
                $end = isset( $booking_session[$id]['end'] ) ? $booking_session[$id]['end'] : $start; // Formatted dates

            } else {
                return false;
            }

            $data = array(
                'product_id'   => $product_id,
                'variation_id' => $variation_id,
                'start'        => $start,
                'end'          => $end
            );

            // Get product stock for the dates
            $stock_left = ebac_get_product_stock( $product_id, $variation_id, $start, $end );

            // Check if dates are already in cart and return the quantity in cart
            $quantity_in_cart = $this->ebac_check_cart_dates( $product, $data );
            
            // If product is sold individually and there is already one in cart with overlapping dates
            if ( $product->is_sold_individually() && $quantity_in_cart > 0 ) {

                wc_add_notice( sprintf(
                    '<a href="%s" class="button wc-forward">%s</a> %s',
                    wc_get_cart_url(),
                    __( 'View Cart', 'woocommerce' ),
                    sprintf( __( 'You cannot add another &quot;%s&quot; to your cart.', 'woocommerce' ), $product->get_title() )
                ), 'error' );
                
                return false;

            }

            // If there isn't enough stock left
            if ( isset( $stock_left ) && $stock_left <= 0 ) {

                wc_add_notice( sprintf( __( 'You cannot add &quot;%s&quot; to the cart because the product is out of stock.', 'woocommerce' ),
                    $product->get_title()
                    ), 'error' );

                $passed = false;

            } elseif ( isset( $stock_left ) && $stock_left < $quantity + $quantity_in_cart ) { // If stock left is inferior to quantity in cart + quantity added

                if ( $quantity_in_cart === 0 ) {

                    wc_add_notice( sprintf(
                        __( 'You cannot add that amount of &quot;%s&quot; to the cart because there is not enough stock (%s remaining).', 'woocommerce' ),
                        $product->get_title(),
                        wc_stock_amount( $stock_left )
                    ), 'error' );

                } else {

                    wc_add_notice( sprintf(
                        '<a href="%s" class="button wc-forward">%s</a> %s',
                        wc_get_cart_url(),
                        __( 'View Cart', 'woocommerce' ),
                        sprintf( __( 'You cannot add that amount to the cart &mdash; we have %s in stock and you already have %s in your cart.', 'woocommerce' ),
                            wc_stock_amount( $stock_left ),
                            wc_stock_amount( $quantity_in_cart ) )
                    ), 'error' );

                }

                $passed = false;
            } else {
                $passed = true;
            }

        }

        return $passed;
        
    }

    /**
    *
    * Ensures that there is enough stock left when updating cart
    *
    * @param bool $passed - passed validation
    * @param string $cart_item_key
    * @param array $values - item information
    * @param int $quantity - quantity added to cart
    * @return bool $passed - whether it passed validation or not
    *
    **/
    public function ebac_check_stock_on_cart_update( $passed = true, $cart_item_key, $values, $quantity ) {

        $product_id     = $values['product_id'];
        $variation_id   = $values['variation_id'];
        $product        = $values['data'];

        if ( wceb_is_bookable( $product ) && $product->managing_stock() ) {

            $start = $values['_ebs_start'];
            $end = isset( $values['_ebs_end'] ) ? $values['_ebs_end'] : $start;
            
            $quantity = $quantity - $values['quantity'];

            $data = array(
                'product_id'   => $product_id,
                'variation_id' => $variation_id,
                'start'        => $start,
                'end'          => $end
            );

            // Get product stock for the dates
            $stock_left = ebac_get_product_stock( $product_id, $variation_id, $start, $end );

            // Check if dates are already in cart and return the quantity in cart
            $quantity_in_cart = $this->ebac_check_cart_dates( $product, $data );

            if ( $product->is_sold_individually() && $quantity_in_cart > 0 ) {

                wc_add_notice( sprintf(
                    '<a href="%s" class="button wc-forward">%s</a> %s',
                    wc_get_cart_url(),
                    __( 'View Cart', 'woocommerce' ),
                    sprintf( __( 'You cannot add another &quot;%s&quot; to your cart.', 'woocommerce' ), $product->get_title() )
                ), 'error' );
                
                return false;

            }

            if ( isset( $stock_left ) && $stock_left <= 0 ) {

                wc_add_notice( sprintf(
                    __( 'You cannot add &quot;%s&quot; to the cart because the product is out of stock.', 'woocommerce' ),
                    $product->get_title()
                ),  'error' );

                $passed = false;

            } elseif ( isset( $stock_left ) && $stock_left < $quantity + $quantity_in_cart ) {

                wc_add_notice( sprintf(
                    __( 'You cannot add that amount of &quot;%s&quot; to the cart because there is not enough stock (%s remaining).', 'woocommerce' ),
                    $product->get_title(),
                    wc_stock_amount( $stock_left )
                ), 'error' );

                $passed = false;
            } else {
                $passed = true;
            }

        }

        return $passed;
        
    }

    /**
    *
    * Gets product quantity already in cart for selected dates
    *
    * @param obj $product
    * @param array $data
    * @return int $quantity_in_cart
    *
    **/
    private function ebac_check_cart_dates( $product, $data ) {

        // Get bookable products, dates and quantities in cart
        $dates_in_cart = ebac_get_booked_dates_in_cart();

        // Get all dates in range
        $dates = ebac_get_dates_from_range( $data['start'], $data['end'] );

        // Invert dates and indexes
        $dates = array_flip( $dates );

        if ( $product->is_type( 'variation' ) && $product->managing_stock() === true ) {
            $item_dates_in_cart = isset( $dates_in_cart[$data['variation_id']] ) ? $dates_in_cart[$data['variation_id']] : array();
        } else {
            $item_dates_in_cart = isset( $dates_in_cart[$data['product_id']] ) ? $dates_in_cart[$data['product_id']] : array();
        }

        // Get common dates
        if ( ! empty( $item_dates_in_cart ) && ! empty( $dates ) ) {
            $common_dates = array_intersect_key( $item_dates_in_cart, $dates );
        }

        // Get quantity in cart for the common dates
        if ( ! empty( $common_dates ) ) {
            $quantity_in_cart = max( $common_dates );
        } else {
            $quantity_in_cart = 0;
        }

        return $quantity_in_cart;
    }

    /**
    *
    * Make sure products are still available for the selected dates on cart and checkout
    *
    * @return bool $return
    *
    **/
    public function ebac_check_dates_on_checkout() {

        $return = true;

        foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {

            $product = $values['data'];
            $product_id = $product->get_id();

            if ( $product->managing_stock() && isset( $values['_ebs_start'] ) ) {

                $start = $values['_ebs_start'];
                $end = isset( $values['_ebs_end'] ) ? $values['_ebs_end'] : $values['_ebs_start'];

                $stock_left = ebac_get_product_stock( $product_id, $product_id, $start, $end );

                if ( isset( $stock_left ) && $stock_left < $values['quantity'] ) {

                    WC()->cart->set_quantity( $cart_item_key, 0 );
                    wc_add_notice( __( 'An item which is no longer available was removed from your cart.', 'woocommerce' ), 'error' );
                    $return = false;
                }

            }
        }

        return $return;
    }
}

return new Easy_Booking_Availability_Cart();

endif;