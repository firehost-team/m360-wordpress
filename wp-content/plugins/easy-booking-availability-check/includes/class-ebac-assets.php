<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Assets' ) ) :

class Easy_Booking_Availability_Assets {

	public function __construct() {
        add_action( 'easy_booking_enqueue_additional_scripts', array( $this, 'ebac_enqueue_scripts' ), 10 );
	}
    
	public function ebac_enqueue_scripts() {
        global $post;

        if ( ! is_product() ) {
            return;
        }

        $post_id = $post->ID;
        $product = wc_get_product( $post_id );

        // Easy booking settings
        $easy_booking_settings = get_option('easy_booking_settings');

        // Calculation mode (Days or Nights)
        $calc_mode = $easy_booking_settings['easy_booking_calc_mode'];
        $mode = $calc_mode;
        
        // Init variables
        $out_of_stock         = array();
        $dates                = array();
        $product_stock        = array();
        $managing_stock       = false;

        if ( wceb_is_bookable( $product ) ) {

            // Availability Check functions
            wp_enqueue_script(
                'ebac',
                wceb_get_file_path( '', 'ebac', 'js', EBAC_PLUGIN_FILE ),
                array('jquery', 'pickadate'),
                '1.0',
                true
            );

            wp_localize_script(
                'ebac',
                'settings',
                array(
                    'calc_mode' => $calc_mode
                )
            );

            // WooCommerce 2.7 compatibility
            if ( is_callable( array( $product, 'get_type' ) ) ) {
                $product_type = $product->get_type();
            } else {
                $product_type = $product->product_type;
            }

            if ( $product_type === 'variable' ) {

                wp_enqueue_script(
                    'easy_booking_availability_picker',
                    wceb_get_file_path( '', 'ebac-picker-variable', 'js', EBAC_PLUGIN_FILE ),
                    array('jquery', 'pickadate'),
                    '1.0',
                    true
                );

                // Get product variations
                $variation_ids = $product->get_children();
                
                $mode = array();
                if ( ! empty( $variation_ids ) ) foreach ( $variation_ids as $variation_id ) {

                    $variation = wc_get_product( $variation_id );

                    if ( ! wceb_is_bookable( $variation ) ) {
                        continue;
                    }

                    $date_format = wceb_get_product_booking_dates( $variation );

                    if ( true === $variation->managing_stock() ) {

                        $out_of_stock[$variation_id]   = ebac_get_out_of_stock_dates( $post_id, $variation_id );
                        $dates[$variation_id]          = ebac_get_product_booked_dates( $post_id, $variation_id );
                        $managing_stock[$variation_id] = true;
                        $product_stock[$variation_id]  = $variation->get_stock_quantity();

                        // If only one date, force "Days" mode
                        $mode[$variation_id] = ( $date_format === 'one' ) ? 'days' : $calc_mode;

                    } else if ( $variation->managing_stock() === 'parent' ) {

                        $out_of_stock[$variation_id]   = ebac_get_out_of_stock_dates( $post_id );
                        $dates[$variation_id]          = ebac_get_product_booked_dates( $post_id );
                        $managing_stock[$variation_id] = true;
                        $product_stock[$variation_id]  = $product->get_stock_quantity();

                        // If only one date, force "Days" mode
                        $mode[$variation_id] = ( $date_format === 'one' ) ? 'days' : $calc_mode;

                    } else {

                        $out_of_stock[$variation_id]   = false;
                        $dates[$variation_id]          = false;
                        $managing_stock[$variation_id] = false;
                        $product_stock[$variation_id]  = false;
                        $mode[$variation_id]           = $calc_mode;

                    }

                }

            } else if ( $product_type === 'grouped' || $product_type === 'bundle' ) {

                wp_enqueue_script(
                    'easy_booking_availability_picker',
                    wceb_get_file_path( '', 'ebac-picker-grouped', 'js', EBAC_PLUGIN_FILE ),
                    array('jquery', 'pickadate'),
                    '1.0',
                    true
                );

                // Get product children
                $children = wceb_get_product_children_ids( $product );

                if ( ! empty( $children ) ) foreach ( $children as $child_id ) {

                    $child = wc_get_product( $child_id );

                    if ( $child->managing_stock() === true ) {

                        $out_of_stock[$child_id]   = ebac_get_out_of_stock_dates( $child_id );
                        $dates[$child_id]          = ebac_get_product_booked_dates( $child_id );
                        $managing_stock[$child_id] = true;
                        $product_stock[$child_id]  = $child->get_stock_quantity();

                    } else if ( $child->managing_stock() === 'parent' ) {

                        $parent_product_id = is_callable( array( $child, 'get_parent_id' ) ) ? $child->get_parent_id() : $child->parent->id;

                        $parent_product = wc_get_product( $parent_product_id );

                        $out_of_stock[$child_id]   = ebac_get_out_of_stock_dates( $parent_product_id );
                        $dates[$child_id]          = ebac_get_product_booked_dates( $parent_product_id );
                        $managing_stock[$child_id] = true;
                        $product_stock[$child_id]  = $parent_product->get_stock_quantity();

                    } else {

                        $out_of_stock[$child_id]   = false;
                        $dates[$child_id]          = false;
                        $managing_stock[$child_id] = false;
                        $product_stock[$child_id]  = false;
                        
                    }

                }

                $date_format = wceb_get_product_booking_dates( $product );

                // If only one date, force "Days" mode
                $mode = ( $date_format === 'one' ) ? 'days' : $calc_mode;

            } else {

                if ( $product->managing_stock() ) {

                    wp_enqueue_script(
                        'easy_booking_availability_picker',
                        wceb_get_file_path( '', 'ebac-picker-simple', 'js', EBAC_PLUGIN_FILE ),
                        array('jquery', 'pickadate'),
                        '1.0',
                        true
                    );

                    // Get out-of-stock dates
                    $out_of_stock = ebac_get_out_of_stock_dates( $post_id );

                    // Get booked dates with quantity booked
                    $dates = ebac_get_product_booked_dates( $post_id );

                    $managing_stock = true;
                    $product_stock = $product->get_stock_quantity();

                    $date_format = wceb_get_product_booking_dates( $product );

                    // If only one date, force "Days" mode
                    $mode = ( $date_format === 'one' ) ? 'days' : $calc_mode;

                }

            } // Product type
            
            // Availability Check settings
            $ebac_settings = get_option('easy_booking_availability_settings');

            // Filter to override this - Return "none", "colors", "amount" or "amount_colors"
            $display_availabilities = apply_filters( 'ebac_display_availabilities', $ebac_settings['easy_booking_availability_display'], $post_id );

            $low_stock = get_option('woocommerce_notify_low_stock_amount');

            wp_localize_script(
                'easy_booking_availability_picker',
                'data',
                array(
                    'calc_mode'              => is_array( $mode ) ? array_map( 'esc_html', $mode ) : esc_html( $mode ),
                    'display_availabilities' => esc_html( $display_availabilities ),
                    'disabled_dates'         => $out_of_stock,
                    'availabilities'         => $dates,
                    'stock'                  => $product_stock,
                    'low_stock'              => absint( $low_stock ),
                    'managing_stock'         => $managing_stock
                )
            );

            // Picker's CSS
            if ( function_exists( 'is_multisite' ) && is_multisite() ) {

                $blog_id = get_current_blog_id();

                wp_register_style(
                    'easy_booking_availability_picker_style',
                    plugins_url( 'assets/css/ebac-frontend-picker.' . $blog_id . '.min.css', EBAC_PLUGIN_FILE ),
                    array( 'picker' ),
                    true
                );

            } else {

                wp_register_style(
                    'easy_booking_availability_picker_style',
                    plugins_url('assets/css/ebac-frontend-picker.min.css', EBAC_PLUGIN_FILE),
                    array( 'picker' ),
                    true
                );

            }

            wp_enqueue_style( 'easy_booking_availability_picker_style' );
            
        }
    }
}

return new Easy_Booking_Availability_Assets();

endif;