<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Easy_Booking_Availability_List_Imports extends WP_List_Table {

	protected $max_items;

	public function __construct() {

		parent::__construct( array(
			'singular'  => __( 'Import', 'woocommerce' ),
			'plural'    => __( 'Imports', 'woocommerce' ),
			'ajax'		=> true
		) );

	}

	public function no_items() {
		esc_html_e( 'No products found.', 'woocommerce' );
	}

	public function single_row( $item ) {
		echo '<tr class="ebac-import-line" data-index="' . esc_attr( $item['index'] ) . '">';
			$this->single_row_columns( $item );
		echo '</tr>';
	}

	protected function single_row_columns( $item ) {
		list( $columns, $hidden, $sortable, $primary ) = $this->get_column_info();

		foreach ( $columns as $column_name => $column_display_name ) {

			if ( ( $column_name === 'edit' || $column_name === 'delete' ) && empty( $item['product_ids'] ) ) {
				continue;
			}

			$classes = "$column_name column-$column_name";
			if ( $primary === $column_name ) {
				$classes .= ' has-row-actions column-primary';
			}

			if ( in_array( $column_name, $hidden ) ) {
				$classes .= ' hidden';
			}

			// Comments column uses HTML in the display name with screen reader text.
			// Instead of using esc_attr(), we strip tags to get closer to a user-friendly string.
			$data = 'data-colname="' . wp_strip_all_tags( $column_display_name ) . '"';

			$attributes = "class='$classes' $data";

			if ( 'cb' === $column_name ) {
				echo '<th scope="row" class="check-column">';
				echo $this->column_cb( $item );
				echo '</th>';
			} elseif ( method_exists( $this, '_column_' . $column_name ) ) {
				echo call_user_func(
					array( $this, '_column_' . $column_name ),
					$item,
					$classes,
					$data,
					$primary
				);
			} elseif ( method_exists( $this, 'column_' . $column_name ) ) {
				echo "<td $attributes>";
				echo call_user_func( array( $this, 'column_' . $column_name ), $item );
				echo $this->handle_row_actions( $item, $column_name, $primary );
				echo "</td>";
			} else {
				echo "<td $attributes>";
				echo $this->column_default( $item, $column_name );
				echo $this->handle_row_actions( $item, $column_name, $primary );
				echo "</td>";
			}
		}
	}

	protected function handle_row_actions( $item, $column_name, $primary ) {
		$display = ! empty( $item['product_ids'] ) ? 'style="display:none;' : '';

		$output = '<div class="import-edit-actions"' . $display . '">
		<button type="button" class="button button-primary save-edit">' . esc_html__( 'Save', 'woocommerce' ) . '</button>
		<button type="button" class="button button-secondary cancel-edit">' . esc_html__( 'Cancel', 'woocommerce' ) . '</button>
		</div>';

		return $column_name === $primary ? $output : '';
 	}

 	public function column_products( $item ) {
 		$product_ids = $item['product_ids'];
		$json_ids    = array();

		foreach ( $product_ids as $product_id ) {

			$product = wc_get_product( $product_id );

			if ( is_object( $product ) ) {
				$json_ids[ $product_id ] = wp_kses_post( $product->get_formatted_name() );
			} else {
				return;
			}

		}

		$disabled = ( isset( $json_ids ) && ! empty( $json_ids ) ) ? 'disabled="disabled"' : '';

		if ( version_compare( WC_VERSION, '2.7', '<' ) ) {

			echo '<input type="hidden" id="add_product_id" name="ebac_product_ids" value="' . implode( ',', array_keys( $json_ids ) ) . '" data-selected="' . esc_attr( json_encode( $json_ids) ) . '" class="wc-product-search" style="width: 100%;" data-action="ebac_search_bookable_products" data-placeholder="' . __( 'Search for a product&hellip;', 'woocommerce' ) . '" data-multiple="true"' . $disabled . ' />';
			
		} else {

			echo '<select class="wc-product-search" style="width:203px;" id="add_product_id" name="ebac_product_ids[]" data-action="ebac_search_bookable_products" data-placeholder="' . __( 'Search for a product&hellip;', 'woocommerce' ) . '" multiple="multiple" ' . $disabled . ' />';

			foreach ( $json_ids as $product_id => $product_name ) {
				$product = wc_get_product( $product_id );
				if ( is_object( $product ) ) {
					echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
				}
			}

			echo '</select>';

		}
 	}

 	public function column_start_date( $item ) {
 		$disabled = ( isset( $item['start'] ) && ! empty( $item['start'] ) ) ? 'disabled="disabled"' : '';
		echo '<input type="text" name="ebac_start" value="" data-value="' . esc_attr( $item['start'] ) . '" class="datepicker datepicker_start" ' . $disabled . '>';
 	}

 	public function column_end_date( $item ) {
 		$disabled = ( isset( $item['end'] ) && ! empty( $item['end'] ) ) ? 'disabled="disabled"' : '';
		echo '<input type="text" name="ebac_end" value="" data-value="' . esc_attr( $item['end'] ) . '" class="datepicker datepicker_end" ' . $disabled . '>';
 	}

 	public function column_qty_booked( $item ) {
 		$disabled = ( isset( $item['qty_booked'] ) && ! empty( $item['qty_booked'] ) ) ? 'disabled="disabled"' : '';
		echo '<input type="number" name="ebac_qty_booked" value="' . esc_attr( $item['qty_booked'] ) . '" class="select_qty_booked" min="1" ' . $disabled . '>';
 	}

 	public function column_edit( $item ) {
		echo '<a href="#" class="edit-import">Edit</a>';
	}

 	public function column_delete( $item ) {
 		echo '<a href="#" class="delete-import">Delete</a>';
 	}

	public function column_default( $item, $column_name ) {

		$disabled = ( isset( $item['product_ids'] ) && ! empty( $item['product_ids'] ) ) ? 'disabled="disabled"' : '';
		
		echo apply_filters(
			'easy_booking_availability_display_custom_column',
			sprintf(
				'<input type="text" name="ebac_%s" value="%s" %s>',
				esc_attr( $column_name ),
				isset( $item[$column_name] ) ? esc_attr( $item[$column_name] ) : '',
				$disabled
			),
			$column_name,
			$item
		);

	}

	public function get_columns() {

		$columns = array(
			'products'   => __( 'Product(s)', 'easy_booking_availability' ),
			'start_date' => __( 'Start', 'easy_booking_availability' ),
			'end_date'   => __( 'End (optional)', 'easy_booking_availability' ),
			'qty_booked' => __( 'Quantity booked', 'easy_booking_availability' ),
			'edit'       => '&nbsp;',
			'delete'     => '&nbsp;'
		);

		$custom_columns = apply_filters( 'easy_booking_availability_imports_columns', array() );

		if ( $custom_columns ) foreach ( $custom_columns as $custom_column ) {

			// Backward compatibility
			if ( array_key_exists('content', $custom_column) && is_array( $custom_column['content'] ) ) {
				foreach ( $custom_column['content'] as $custom_column_id => $custom_column_name ) {
					$custom_column['id'] = $custom_column_id;
					$custom_column['name'] = $custom_column_name;
				}
			}

			// Sanitize
			$id = sanitize_html_class( $custom_column['id'] );
			$custom_column['content'][$id] = esc_html( $custom_column['name'] );

			// Insert custom column at the right place
			$columns = $this->array_insert( $columns, absint( $custom_column['position'] ), $custom_column['content'] );
		}

		return $columns;
	}

	/**
	 * Insert given element at given position in an array
	 *
	 */
	private function array_insert( &$array, $position, $insert_array ) {

	  $first_array = array_splice( $array, 0, $position );
	  $array = array_merge( $first_array, $insert_array, $array );

	  return $array;

	}

	protected function get_sortable_columns() {

		$sortable_columns = array(
			'products'   => array( 'product_id', true ),
			'start_date' => array( 'start_date', false ),
			'end_date'   => array( 'end_date', false ),
			'qty_booked' => array( 'qty', false )
		);

		return $sortable_columns;
	}

	function usort_reorder( $a, $b ) {

		// If no sort, default to product ID
		$orderby = ( ! empty( $_GET['orderby'] ) ) ? sanitize_title( $_GET['orderby'] ) : 'product_id';
		// If no order, default to asc
		$order = ( ! empty($_GET['order'] ) ) ? sanitize_title( $_GET['order'] ) : 'asc';
		
		// Determine sort order
		switch( $orderby ) {

			case 'product_id' :

				$a_lower_id = min( $a['product_ids'] );
				$b_lower_id = min( $b['product_ids'] );

				$result = $a_lower_id - $b_lower_id;
				
			break;

			case 'start_date' :
				$a_start_date = strtotime( $a['start'] );
				$b_start_date = strtotime( $b['start'] );

				$result = $a_start_date - $b_start_date;
			break;

			case 'end_date' :
				$a_end_date = strtotime( $a['end'] );
				$b_end_date = strtotime( $b['end'] );

				$result = $a_end_date - $b_end_date;
			break;

			case 'qty' :
				$result = $a['qty_booked'] - $b['qty_booked'];
			break;

		}
		
		// Send final sort direction to usort
		return ( $order === 'asc' ) ? $result : -$result;
	}

	public function prepare_items() {

		$this->_column_headers = array( $this->get_columns(), array(), $this->get_sortable_columns() );
		$current_page          = absint( $this->get_pagenum() );
		$per_page              = 20;

		$this->get_items( $current_page, $per_page );

		/**
		 * Pagination
		 */
		$this->set_pagination_args( array(
			'total_items' => $this->max_items,
			'per_page'    => $per_page,
			'total_pages' => ceil( $this->max_items / $per_page )
		) );
	}

	public function get_items( $current_page, $per_page ) {
		global $wpdb;

		$this->max_items = 0;
		$this->items     = array();

		$imports = get_option('easy_booking_availability_imports');

		if ( ! $imports )
			return;

		usort( $imports, array( $this, 'usort_reorder' ) );

		$total_items = count( $imports );

		$min = ( $current_page - 1 ) * $per_page;
		$max = $min + $per_page;

		$set_max = $total_items < $max ? $total_items : $max;

		$items = array();
		for ( $i = $min; $i < $set_max; $i++ ) {
			$items[] = $imports[$i];
		}

		$this->items     = $items;
		$this->max_items = $total_items;
	}

}