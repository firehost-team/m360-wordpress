<table class="wp-list-table widefat">

	<?php

	$key_max = ! empty( $this->imports ) && is_array( $this->imports ) ? max( array_keys( $this->imports ) ) : 0;

	if ( ! empty( $this->imports ) ) {
		$key_max += 1;
	}

	$item = array(
		'index'       => $key_max,
		'name'        => '',
		'product_ids' => array(),
		'start'       => '',
		'end'         => '',
		'qty_booked'  => ''
	);
	
	?>

	<thead>

		<tr>
		<?php $column_headers = $wp_list_table->get_columns();
		
		if ( $column_headers ) foreach ( $column_headers as $header => $title ) {

			if ( $header !== 'edit' && $header !== 'delete' ) {
				echo '<th>' . esc_attr__( $title, 'easy_booking_availability' ) . '</th>';
			}

		} ?>

		</tr>
		
	</thead>

	<tbody>

		<?php $wp_list_table->single_row( $item ); ?>

	</tbody>

</table>