<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Imports' ) ) :

class Easy_Booking_Availability_Imports {

	private $imports;

	public function __construct() {

		// Get imported bookings
		$this->imports = get_option( 'easy_booking_availability_imports' );

		// Init option
		if ( ! $this->imports ) {
			add_option( 'easy_booking_availability_imports', array() );
		}

		// Get plugin settings
		$this->settings = get_option( 'easy_booking_availability_settings' );

		add_action( 'admin_init', array( $this, 'ebac_imports_init' ) );
		add_action( 'easy_booking_availability_import_tab', array( $this, 'ebac_display_imports' ), 10 );
		add_action( 'admin_print_scripts-easy-booking_page_easy-booking-availability-check', array( $this, 'ebac_load_import_scripts' ) );
		add_action( 'load-easy-booking_page_easy-booking-availability-check', array( $this, 'ebac_save_imports' ) );

		add_action( 'wceb_update_order_items_booking_statuses', array( $this, 'ebac_update_imports_booking_statuses' ) );
	}

	/**
    *
    * Load imports scripts and styles
    *
    **/
	public function ebac_load_import_scripts() {

		// JS

		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'wc-enhanced-select' );
		
		wp_enqueue_script( 'pickadate' );

		wp_register_script(
			'ebac-import',
			wceb_get_file_path( 'admin', 'ebac-import-functions', 'js', EBAC_PLUGIN_FILE ),
			array( 'jquery', 'pickadate' ),
			'1.0',
			true
		);

		$easy_booking_settings = get_option('easy_booking_settings');
        $calc_mode             = $easy_booking_settings['easy_booking_calc_mode']; // Calculation mode (Days or Nights)

		wp_localize_script(
			'ebac-import',
			'ajax',
            array(
				'calc_mode' => esc_html( $calc_mode ),
				'ajax_url'  => esc_url( admin_url( 'admin-ajax.php' ) )
            )
        );

        wp_enqueue_script( 'ebac-import' );
		wp_enqueue_script( 'datepicker.language' );
		wp_enqueue_script( 'jquery-blockui' );

        // CSS

        wp_enqueue_style(
			'woocommerce_admin_styles',
			WC()->plugin_url() . '/assets/css/admin.css',
			array(),
			WC_VERSION
		);

		wp_enqueue_style( 'picker' );

		wp_register_style(
			'ebac_imports_styles',
			wceb_get_file_path( 'admin', 'ebac-imports', 'css', EBAC_PLUGIN_FILE ),
			array(),
			'1.0'
		);

		wp_enqueue_style( 'ebac_imports_styles' );

	}

	/**
    *
    * Trigger action hook when settings or imports are updated
    *
    **/
	public function ebac_save_imports() {
		if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] ) {
	  		$imports = get_option( 'easy_booking_availability_imports' );
			do_action( 'easy_booking_availability_save_imports', $imports );
	   	}
	}

	/**
    *
    * Init import settings
    *
    **/
	public function ebac_imports_init() {

		register_setting(
			'easy_booking_availability_imports',
			'easy_booking_availability_imports',
			array( $this, 'save_imports' )
		);

		// Backward compatibility
		if ( ! get_option( 'ebac_imports_backward_compatibility' ) ) {
			$updated_imports = $this->save_imports( $this->imports );
			update_option( 'easy_booking_availability_imports', $updated_imports );
			add_option( 'ebac_imports_backward_compatibility', 1 );
		}
		
	}

	/**
    *
    * Import form
    *
    **/
	public function ebac_display_imports() { ?>

	<?php 

		$wp_list_table = new Easy_Booking_Availability_List_Imports();
		$wp_list_table->prepare_items(); ?>

		<div class="imports-wrap">
			
			<h2>
				<?php _e('Import bookings', 'easy_booking_availability'); ?>
				<a href="#" class="add-new-h2 add-booking"><?php _e('Add booking', 'easy_booking_availability'); ?></a>
			</h2>
			
			<div class="ebac-import-table">
				<?php include( 'views/ebac-html-import-form.php' ); ?>
				<?php settings_fields('easy_booking_availability_imports'); ?>
	    	</div>
			
			<?php $wp_list_table->display(); ?>

		</div>

	<?php

	}

	/**
    *
    * Sanitize imports
    *
    **/
	public function save_imports( $imports ) {

		// Get current date
        $current_date = strtotime( date( 'Y-m-d' ) );

		$settings             = get_option( 'easy_booking_settings' );
		$change_start_day     = $settings['easy_booking_start_status_change'];
		$change_completed_day = $settings['easy_booking_completed_status_change'];

        $remove_imports = $this->settings['ebac_remove_imports'];

		$imports_to_save = array();
		if ( $imports ) foreach ( $imports as $index => $import ) {

			if ( empty( $import['product_ids'] ) && empty( $import['start'] ) && empty( $import['end'] ) && empty( $import['qty_booked'] ) ) {
				continue;
			}

			if ( empty( $import['product_ids'] ) ) {
				WC_Admin_Meta_Boxes::add_error( __( 'Product is required', 'easy_booking_availability' ) );
				continue;
			}

			if ( empty( $import['start'] ) ) {
				WC_Admin_Meta_Boxes::add_error( __( 'Start date is required', 'easy_booking_availability' ) );
				continue;
			}

			if ( empty( $import['qty_booked'] ) ) {
				WC_Admin_Meta_Boxes::add_error( __( 'Quantity booked is required', 'easy_booking_availability' ) );
				continue;
			}

			// If end is not set, set it to start
			if ( empty( $import['end'] ) ) {
				$import['end'] = $import['start'];
			}

			$start_time = strtotime( $import['start'] );
        	$end_time = strtotime( $import['end'] );

            $booking_status = isset( $import['status'] ) ? $import['status'] : '';

            // No status and start date in the future = Pending status
            if ( $current_date < $start_time ) {
                $item_status = 'wceb-pending';
            }
            
            // Automatically change to Start status x days before start date (defined in the plugin settings)
            $change_start_status = strtotime( $import['start'] . ' -' . $change_start_day . ' days' );
            if ( $current_date >= $change_start_status ) {
                $item_status = 'wceb-start';
            }

            // Automatically change to Processing status between start and end dates
            if ( $current_date > $start_time && $current_date < $end_time ) {
                $item_status = 'wceb-processing';
            }

            // Automatically change to End status on end date
            if ( $current_date >= $end_time ) {
                $item_status = 'wceb-end';
            }

            // If start and end dates are the same day, set Processing instead of Start/End
            if ( $start_time === $end_time && $current_date === $start_time && $current_date === $end_time ) {
                $item_status = 'wceb-processing';
            }

            // Automatically change to Completed status after end date (or delete if option is checked)
            $change_completed_status = strtotime( $import['end'] . ' +' . $change_completed_day . ' days' );
            if ( $current_date > $change_completed_status ) {

            	if ( isset( $remove_imports ) && $remove_imports === 'on' ) {
            		continue;
            	} else {
            		$item_status = 'wceb-completed';
            	}
                
            }

            if ( isset( $item_status ) && $booking_status !== $item_status ) {
                $import['status'] = $item_status;
            }

			foreach( $import as $name => $value ) {

	            switch ( $name ) {

	            	case 'index' :
	            		$imports_to_save[$index][$name] = absint( $value );
	            	break;

	                case 'product_ids' :
	                    $imports_to_save[$index][$name] = array_map( 'absint', $value );
	                break;

	                case 'qty_booked' :
	                    $imports_to_save[$index][$name] = absint( $value );
	                break;

	                default :
	                    $imports_to_save[$index][$name] = is_array( $value ) ? array_map( 'sanitize_text_field', $value ) : sanitize_text_field( $value );
	                break;

	            }
	            
	        }

       	}

       	// Re-index entries
       	$imports_to_save = array_values( $imports_to_save );

       	foreach ( $imports_to_save as $index => $import ) {
       		$imports_to_save[$index]['index'] = $index;
       	}

		return $imports_to_save;
	}

	/**
    *
    * Update imported bookings when booking statuses are updated
    *
    **/
	public function ebac_update_imports_booking_statuses() {
		$updated_imports = $this->save_imports( $this->imports );
		update_option( 'easy_booking_availability_imports', $updated_imports );
	}

}

return new Easy_Booking_Availability_Imports();

endif;