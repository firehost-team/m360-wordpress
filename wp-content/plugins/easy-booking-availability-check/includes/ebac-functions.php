<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
*
* Gets booked amount of every bookable and stock-managed products ordered
*
* @param bool $past - Get passed dates
* @return array of bookable products id with amount booked
*
**/
function ebac_get_booked_items_from_orders( $past = false, $filter_id = '' ) {

    $ebac_settings  = get_option('easy_booking_availability_settings');
    $unavailability = absint( $ebac_settings['ebac_unavailability_period'] );

    // Query orders
    $args = array(
        'post_type' => 'shop_order',
        'post_status' => apply_filters( 
                            'easy_booking_get_order_statuses',
                            array(
                                'wc-pending',
                                'wc-processing',
                                'wc-on-hold',
                                'wc-completed',
                                'wc-refunded'
                            ) ),
        'posts_per_page' => -1
    );

    if ( ! $past ) {
        $args['meta_query'] = array(
            'relation' => 'OR',
            array(
                'key'     => 'order_booking_status',
                'value'   => 'processing',
                'compare' => '=',
            ),
            array(
                'key'     => 'order_booking_status',
                'compare' => 'NOT EXISTS',
            ),
        );
    }

    $query_orders = new WP_Query( $args );

    // Get an array of booked products managing stocks with the start date, end date and quantity booked
    $products = array();
    foreach ( $query_orders->posts as $post ) :

        $order_id = $post->ID;
        $order    = new WC_Order( $order_id );
        $items    = $order->get_items();

        if ( $items ) foreach ( $items as $item_id => $item ) {

            $product_id   = $item['product_id'];
            $variation_id = $item['variation_id'];

            $product = $order->get_product_from_item( $item );

            if ( ! $product ) {
                continue;
            }

            if ( wceb_is_bookable( $product ) && $product->managing_stock() ) {

                if ( isset( $item['ebs_start_format'] ) ) {
                    
                    $id = empty( $variation_id ) || $variation_id === '0' ? $product_id : $variation_id;

					$start = $item['ebs_start_format'];

                    // Check date format to avoid errors (yyyy-mm-dd)
                    if ( ! preg_match( '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/', $start ) ) {
                        continue;
                    }

                    if ( isset( $item['ebs_end_format'] ) && ! empty( $item['ebs_end_format'] ) ) {

                        $end = $item['ebs_end_format'];

                        // Check date format to avoid errors (yyyy-mm-dd)
                        if ( ! preg_match( '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/', $end ) ) {
                            continue;
                        }

                    } else {
                        $end = $start;
                    }

                    $quantity = intval( $item['qty'] );
                    
                    if ( $unavailability > 0 ) {
                        $start = date( 'Y-m-d', strtotime( $start . ' -' . $unavailability . ' days' ) );
                        $end = date( 'Y-m-d', strtotime( $end . ' +' . $unavailability . ' days' ) );
                    }

                    // If a renfund has been made, get the refunded quantity
                    $refunded_qty = $order->get_qty_refunded_for_item( $item_id );

                    // Remove the refunded quantity from booked quantity
                    if ( $refunded_qty < 0 ) {
                        $quantity = $refunded_qty + $quantity;
                    }

                    // If 0 items are left, return
                    if ( $quantity <= 0 ) {
                        continue;
                    }

                    // For variations, if parent product manages stock, store the parent product aswell
                    if ( $product->managing_stock() === 'parent' ) {

                        $products[] = array(
							'product_id' => $product_id,
							'start'      => $start,
                            'end'        => $end,
							'qty'        => $quantity
                        );

                    }

                    $products[] = array(
						'product_id' => $id,
						'start'      => $start,
                        'end'        => $end,
						'qty'        => $quantity
                    );

                }

            }
        
        }

    endforeach;
    
    // Get manually imported bookings
    $imports = ebac_get_imported_booked_dates();

    // Merge ordered products and imported bookings
    $products = apply_filters( 'ebac_booked_products', array_filter( array_merge( $products, $imports ) ) );

    // Make an array of all dates and merge quantities for the same dates
    $booked = array();
    if ( ! empty ( $products ) ) foreach ( $products as $booked_product ) {

        $product_id = $booked_product['product_id'];
        $quantity   = $booked_product['qty'];

        $product = wc_get_product( $product_id );

        if ( ! $product ) {
            continue;
        }
        
        $date_format = wceb_get_product_booking_dates( $product );

        $offset = $date_format === 'one' ? false : true;

        // Get all dates in range
        $dates = ebac_get_dates_from_range( $booked_product['start'], $booked_product['end'], $past, $offset );

        // New dates
        $incoming_dates = array();
        if ( ! array_key_exists( $product_id, $booked ) ) {

            foreach ( $dates as $date ) {

                if ( $quantity > 0 ) {
                    $incoming_dates[$date] = $quantity;
                }

            }

            $booked[$product_id] = $incoming_dates;
            
        } else {

            foreach ( $dates as $date ) {
                
                if ( array_key_exists( $date, $booked[$product_id] ) ) {

                    $new_quantity = $quantity + $booked[$product_id][$date];

                    if ( $new_quantity > 0 ) {
                        $incoming_dates[$date] = $new_quantity;
                    } else {
                        unset( $booked[$product_id][$date] );
                    }

                } else {
                   $incoming_dates[$date] = $quantity;
                }
            }

            $booked[$product_id] = array_merge( $booked[$product_id], $incoming_dates );
        }

    }

    // Remove product IDs with no dates and sort dates
    foreach ( $booked as $product_id => $data ) {

        if ( empty( $booked[$product_id] ) ) {

            unset( $booked[$product_id] );

        } else {

        	// Sort dates
            uksort( $data, 'ebac_sort_dates' );

            // Group dates in dateranges
            $booked[$product_id] = ebac_group_dates_in_dateranges( $data, true, false );

        }

    }

    // If product ID is being filtered, return only this product's booking data
    if ( ! empty( $filter_id ) ) {

        if ( isset( $booked[$filter_id] ) ) {
            return $booked[$filter_id];
        } else {
            return array();
        }

    } else {
        return array_filter( $booked ); 
    }

}

/**
*
* Sort dates in ascending order
*
* @param str $a - First date
* @param str $b - Second date
* @return bool
*
**/
function ebac_sort_dates( $a, $b ) {
    return strtotime( $a ) - strtotime( $b );
}

/**
*
* Gets all imported booked, sorted by product ID
*
* @return array - $imports
*
**/
function ebac_get_imported_booked_dates( $apply_unavailability_period = true ) {

	// Get manually added imports
    $imports = (array) get_option('easy_booking_availability_imports');

    $ebac_settings  = get_option('easy_booking_availability_settings');
    $unavailability = absint( $ebac_settings['ebac_unavailability_period'] );

    $imported_bookings = array();
    if ( ! empty( $imports ) ) {

        foreach ( $imports as $index => $import ) {
            
            foreach ( $import['product_ids'] as $product_id ) {

                if ( empty( $product_id ) ) {
                    continue;
                }

                // Maybe apply unavailability period
                if ( true === $apply_unavailability_period && $unavailability > 0 ) {
                    $start = date( 'Y-m-d', strtotime( $import['start'] . ' -' . $unavailability . ' days' ) );
                    $end   = date( 'Y-m-d', strtotime( $import['end'] . ' +' . $unavailability . ' days' ) );
                } else {
                    $start = $import['start'];
                    $end   = $import['end'];
                }

                $product = wc_get_product( $product_id );

                if ( ! $product ) {
                    continue;
                }

                // Maybe import bundled items
                if ( $product->is_type( 'bundle' ) ) {

                    if ( method_exists( $product, 'get_bundled_items' ) ) {

                        $bundled_items = $product->get_bundled_items();

                        if ( ! empty( $bundled_items ) ) {
                            
                            foreach ( $bundled_items as $bundled_item ) {

                                $bundled_product_id = $bundled_item->get_product_id();
                                $bundled_product = wc_get_product( $bundled_product_id );

                                if ( ! $bundled_product ) {
                                    continue;
                                }

                                if ( ! $bundled_item->is_optional() && $bundled_product->managing_stock() ) {

                                    $imported_bookings[] = array(
                                        'product_id' => $bundled_product_id,
                                        'start'      => $start,
                                        'end'        => $end,
                                        'qty'        => $import['qty_booked'],
                                        'status'     => $import['status']
                                    );

                                }

                            }
                        }
                    }
                }

                $imported_bookings[] = array(
					'product_id' => $product_id,
					'start'      => $start,
					'end'        => $end,
					'qty'        => $import['qty_booked'],
                    'status'     => $import['status']
                );
                
            }
        }
        
    }

    return $imported_bookings;

}

/**
*
* Gets booked amount of a given product from all orders when updating an order
*
* @param int $id - Product or variation id
* @return array $booked - Amount booked for each date for the given product
*
**/
function ebac_get_booked_item_from_order( $id ) {
    $booked = ebac_get_booked_items_from_orders();

    if ( array_key_exists( $id, $booked ) ) {
        return $booked[$id];
    } else {
        return array();
    }
}

/**
*
* Gets booked amount for a given daterange
*
* @param int $id - Product or variation id
* @param string $start
* @param string $end
* @return int $booked_amount
*
**/
function ebac_get_booked_amount_for_daterange( $id, $start, $end ) {

    // Get Easy Booking settings
    $easy_booking_settings = get_option('easy_booking_settings');

     // Calculation mode (Days or Nights)
    $calc_mode = $easy_booking_settings['easy_booking_calc_mode'];

	// Get product booked dateranges with quantity
    $booked_dates  = get_post_meta( $id, '_booking_days', true );
    $booked_amount = 0;

    // Convert start and end to timestamp
    $start = strtotime( $start );
    $end   = strtotime( $end );

    $product = wc_get_product( $id );
    $date_format = wceb_get_product_booking_dates( $product );

    // If calculation mode is set to "Nights", add one day to start day (as you book the night) (only for 2 dates format)
    if ( $date_format === 'two' && $calc_mode === 'nights' ) {
       $start += 86400;
    }

    $quantity_booked = array( 0 );
    if ( $booked_dates ) foreach ( $booked_dates as $index => $daterange ) {

    	// Convert each booked daterange start and end to timestamp
        $booked_start = strtotime( $daterange[0] );

        // If is a daterange
        if ( isset( $daterange[1] ) ) {
            $booked_end = strtotime( $daterange[1] ); 
        } else {
            $booked_end = $booked_start;
        }

        // If dateranges overlap
        if ( ( $start <= $booked_end ) && ( $end >= $booked_start ) ) {
            $quantity_booked[] = $daterange[2];
        }

    }

    // Get maximum booked quantity
    $booked_amount = max( $quantity_booked );

    return $booked_amount;
    
}

/**
*
* Gets booked dates and quantities in cart
*
* @return array $dates_in_cart
*
**/
function ebac_get_booked_dates_in_cart() {

    // Get cart contents
    $cart_contents = WC()->cart->cart_contents;

    $dates_in_cart = array();
    if ( ! empty( $cart_contents ) ) foreach ( $cart_contents as $cart_item_key => $cart_item ) {

        $_product = $cart_item['data'];

        // If item is not a booking, return
        if ( ! isset( $cart_item['_ebs_start'] ) && ! isset( $cart_item['_ebs_end'] ) ) {
            continue;
        }

        $quantity_in_cart = $cart_item['quantity'];
        $start            = $cart_item['_ebs_start'];
        $end              = isset( $cart_item['_ebs_end'] ) ? $cart_item['_ebs_end'] : $start;

        // Get all dates in range
        $booked_dates = ebac_get_dates_from_range( $start, $end );

        foreach ( $booked_dates as $booked_date ) {

            if ( $_product->is_type( 'variation' ) && true === $_product->managing_stock() ) {

                if ( array_key_exists( $cart_item['variation_id'], $dates_in_cart ) && array_key_exists( $booked_date, $dates_in_cart[ $cart_item['variation_id'] ] ) ) {

                    $dates_in_cart[ $cart_item['variation_id'] ][$booked_date] = $quantity_in_cart + $dates_in_cart[ $cart_item['variation_id'] ][$booked_date];

                } else {

                    $dates_in_cart[ $cart_item['variation_id'] ][$booked_date] = $quantity_in_cart;

                }

            } else {

                if ( array_key_exists( $cart_item['product_id'], $dates_in_cart ) && array_key_exists( $booked_date, $dates_in_cart[ $cart_item['product_id'] ] ) ) {

                    $dates_in_cart[ $cart_item['product_id'] ][$booked_date] = $quantity_in_cart + $dates_in_cart[ $cart_item['product_id'] ][$booked_date];

                } else {

                    $dates_in_cart[ $cart_item['product_id'] ][$booked_date] = $quantity_in_cart;

                }

            }

        }
        
    }

    // Return dates already in cart for the same product
    return $dates_in_cart;
}