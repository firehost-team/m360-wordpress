<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Ajax' ) ) :

class Easy_Booking_Availability_Ajax {

    public function __construct() {

        // Availabilities init
        add_action( 'wp_ajax_easy_booking_availability_stock_init', array( $this, 'ebac_stock_init' ) );
        add_action( 'easy_booking_availability_remove_passed_dates', array( $this, 'ebac_stock_init' ) );
        add_action( 'easy_booking_save_settings', array( $this, 'ebac_stock_init' ), 10 );
        add_action( 'easy_booking_availability_save_imports', array ( $this, 'ebac_stock_init' ), 10 );

        // Search functions
        add_action( 'wp_ajax_ebac_search_bookable_products', array( $this, 'ebac_search_bookable_products' ) );
        add_action( 'wp_ajax_easy_booking_availability_get_stocks', array( $this, 'ebac_get_stocks' ) );

        // Import functions
        add_action( 'wp_ajax_ebac_edit_imports', array( $this, 'ebac_edit_imports' ) );
        add_action( 'wp_ajax_ebac_delete_import', array( $this, 'ebac_delete_import' ) );

        // Init availabilities daily to remove past dates and take less space in the database
        if ( ! wp_next_scheduled( 'easy_booking_availability_remove_passed_dates' ) ) {
            wp_schedule_event( date('Y-m-d'), 'daily', 'easy_booking_availability_remove_passed_dates' );
        }
        
    }

    /**
    *
    * Initializes products stocks by adding post_meta with the amount booked for each date
    *
    **/
    public static function ebac_stock_init() {
        global $wpdb;
        
        // Get all booked items from orders
        $booked = ebac_get_booked_items_from_orders();

        $ids = array();
        if ( ! empty( $booked ) ) {

            foreach ( $booked as $id => $booked_stock ) {

                update_post_meta( $id, '_booking_days', $booked_stock );  
                do_action( 'ebac_stock_init', $id, $booked_stock );

                // Store IDs of booked products in an array
                $ids[] = $id;
                $ids = apply_filters( 'ebac_delete_stock_ids', $ids, $id );

            }

        } else {

            $wpdb->query( "DELETE FROM {$wpdb->postmeta} WHERE `meta_key` LIKE '_booking_days'" );

        }

        // Delete any booking data in the database not corresponding to the booked IDs stored before
        if ( ! empty( $ids ) ) {

            $ids = implode( ',', $ids );
            $wpdb->query( "DELETE FROM {$wpdb->postmeta} WHERE `post_id` NOT IN ( $ids ) AND `meta_key` LIKE '_booking_days'" );

        }

        // Add option to prevent notice display to init availabilities
        if ( get_option( 'easy_booking_display_notice_init' ) != 1 ) {
            update_option( 'easy_booking_display_notice_init', 1 );
        }

        // Add option to prevent notice display to init availabilities
        if ( get_option( 'easy_booking_display_notice_init_after_update' ) != 1 ) {
            update_option( 'easy_booking_display_notice_init_after_update', 1 );
        }

        if ( defined( 'DOING_AJAX' ) ) {
           die(); 
        }
        
    }

    /**
    *
    * Search for bookable and managing stocks products
    *
    **/
    public static function ebac_search_bookable_products() {
        ob_start();

        $term = (string) wc_clean( stripslashes( $_GET['term'] ) );

        // Query
        $args = array(
            'post_type'      => array( 'product', 'product_variation' ),
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'order'          => 'ASC',
            'orderby'        => 'parent title',
            'meta_query'     => array(
                array(
                    'key'   => '_booking_option',
                    'value' => 'yes'
                ),
                array(
                    'key'   => '_manage_stock',
                    'value' => array( 'yes', 'parent' )
                )
            ),
            's'              => $term
        );

        $posts = get_posts( $args );

        $found_products = array();
        if ( $posts ) {
            foreach ( $posts as $post ) {
                $product = wc_get_product( $post->ID );
                $found_products[ $post->ID ] = $product->get_formatted_name();
            }
        }

        wp_send_json( $found_products );

        die();

    }

    /**
    *
    * Get product stocks (reports page)
    *
    **/
    public static function ebac_get_stocks() {

        $product_id = absint( $_POST['id'] );
        $product    = wc_get_product( $product_id );

        if ( ! $product ) {
            return;
        }

        $bookings = ebac_get_booked_items_from_orders( true, $product_id );
        $product_stock = $product->get_stock_quantity();

        if ( ! empty( $bookings ) ) foreach ( $bookings as $index => $booking ) {
            $bookings[$index][2] = intval( $product_stock - $booking[2] );
        }

        $availabilities = array(
            'availabilities' => $bookings,
            'product_stock'  => $product_stock
        );

        wp_send_json( $availabilities );

        die();

    }

    /**
    *
    * Add or edit an import
    *
    **/
    public static function ebac_edit_imports() {
        $new_import      = array();
        $updated_imports =  array();

        parse_str( $_POST['data'], $data );

        $index = ! empty( $_POST['index'] ) ? $_POST['index'] : '';

        if ( $_POST['index'] == 0 ) {
            $index = 0;
        }

        $new_import[$index]['index'] = $index;

        foreach( $data as $name => $value ) {

            // Remove the 'ebac_' part
            $name = substr( $name, 5 );

            if ( $name === 'product_ids' ) {

                // WooCommerce > 3.0 (value is already an array)
                if ( is_array( $value ) ) {
                    $new_import[$index][$name] = ! empty( $value ) ? $value : array(); 
                } else {
                    $new_import[$index][$name] = ! empty( $value ) ? explode( ',', $value ) : array(); 
                }
                
            } else {
                $new_import[$index][$name] = ! empty( $value ) ? $value : '';
            }
            
        }

        // Get already saved imports
        $imports = (array) get_option( 'easy_booking_availability_imports' );

        // Merge arrays
        $imports[$index] = $new_import[$index];

        // Update (and sanitize) option
        update_option( 'easy_booking_availability_imports', $imports );

        die();
    }

    /**
    *
    * Delete an import
    *
    **/
    public static function ebac_delete_import() {
        $index   = absint( $_POST['index'] );
        $imports = get_option( 'easy_booking_availability_imports' );
        
        if ( array_key_exists( $index, $imports ) ) {

            // Remove entry
            unset( $imports[$index] );
        
            // Update (and sanitize) option
            update_option( 'easy_booking_availability_imports', $imports );

        }

        die();
    }

}

return new Easy_Booking_Availability_Ajax();

endif;