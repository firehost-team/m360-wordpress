<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

register_setting(
	'easy_booking_availability_settings',
	'easy_booking_availability_settings', 
	array( $this, 'sanitize_values' )
);

add_settings_section(
	'easy_booking_availability_main_settings',
	__( 'Settings', 'easy_booking_availability' ),
	array( $this, 'easy_booking_availability_section_general' ),
	'easy_booking_availability_settings'
);

// If multisite, save the license key on the network, not the sites.
if ( ! is_multisite() ) {

	add_settings_field(
		'easy_booking_availability_license_key',
		__( 'License Key', 'easy_booking_availability' ),
		array( $this, 'easy_booking_availability_license_key' ),
		'easy_booking_availability_settings',
		'easy_booking_availability_main_settings'
	);

}

add_settings_field(
	'easy_booking_availability_display',
	__( 'Display Availabilities?', 'easy_booking_availability' ),
	array( $this, 'easy_booking_availability_display' ),
	'easy_booking_availability_settings',
	'easy_booking_availability_main_settings'
);

add_settings_field(
	'ebac_unavailability_period',
	__( 'Unavailability period after a booking?', 'easy_booking_availability' ),
	array( $this, 'ebac_unavailability_period' ),
	'easy_booking_availability_settings',
	'easy_booking_availability_main_settings'
);

add_settings_field(
	'easy_booking_availability_init',
	__( 'Availabilities', 'easy_booking_availability' ),
	array( $this, 'easy_booking_availability_init' ),
	'easy_booking_availability_settings',
	'easy_booking_availability_main_settings'
);

add_settings_section(
	'easy_booking_availability_imports_settings',
	__( 'Imports', 'easy_booking_availability' ),
	array( $this, 'easy_booking_availability_section_imports' ),
	'easy_booking_availability_settings'
);

add_settings_field(
	'ebac_remove_imports',
	__( 'Automatically delete past imported bookings?', 'easy_booking_availability' ),
	array( $this, 'easy_booking_availability_remove_imports' ),
	'easy_booking_availability_settings',
	'easy_booking_availability_imports_settings'
);