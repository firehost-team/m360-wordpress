<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div class="wrap">

	<?php $ebac_tabs = apply_filters(
		'easy_booking_availability_tabs',
		array(
			'settings' => __('Settings', 'easy_booking_availability'),
			'import'   => __('Import', 'easy_booking_availability')
		)
	);

	$current_tab = empty( $_GET['tab'] ) ? 'settings' : sanitize_title( $_GET['tab'] ); ?>

	<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">

		<?php foreach ( $ebac_tabs as $tab => $label ) { ?>
			<a href="<?php echo admin_url( 'admin.php?page=easy-booking-availability-check&tab=' . $tab ); ?>" class="nav-tab <?php echo ( $current_tab == $tab ? 'nav-tab-active' : '' ) ?>"><?php echo $label; ?></a>
		<?php } ?>

	</h2>

	<?php do_action( 'easy_booking_availability_' . $current_tab . '_tab' ); ?>

</div>