<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php $settings = new Easy_Booking_Availability_Settings(); ?>

<div class="updated easy-booking-notice">

	<p>

		<?php _e( 'Save your license key to get automatic updates for Easy Booking : Availability Check.', 'easy_booking_availability' ); ?>

		<form method="post" action="<?php echo admin_url(); ?>options.php">

			<?php if ( is_multisite() ) {
				settings_fields( 'easy_booking_global_settings' );
				$settings->easy_booking_availability_multisite_license_key();
			} else {
				settings_fields( 'easy_booking_availability_settings' );
				$settings->easy_booking_availability_license_key();
			} ?>
			 
			<?php submit_button( __('Save', 'easy_booking_availability'), 'button'); ?>

		</form>

	</p>

	<button type="button" class="notice-dismiss easy-booking-notice-close" data-notice="ebac_license"></button>

</div>