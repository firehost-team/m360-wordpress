<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="updated easy-booking-notice">

	<p>

		<?php _e( 'To get started, check the "Manage stock?" checkbox on your bookable products and set their stock amount. Then, if you already have orders, initialize availabilities.', 'easy_booking_availability' ); ?>

		<p>

			<button type="button" class="button" id="easy_booking_availability_stock_init">
				<?php _e( 'Initialize availabilities', 'easy_booking_availability' ); ?>
				<span class="easy_booking_availability_response"></span>
			</button>

		</p>
		
		<button type="button" class="notice-dismiss easy-booking-notice-close" data-notice="init"></button>

	</p>

</div>