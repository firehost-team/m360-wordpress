<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
*
* Gets product's minimum stock from start to end date
*
* @param int $product_id
* @param int $variation_id
* @param (optional) str - $start
* @param (optional) str - $end
* @return int $stock_left
*
**/

function ebac_get_product_stock( $product_id, $variation_id = '', $start = '', $end = '' ) {

    // Get product of variation ID
    $id = ! empty( $variation_id ) ? $variation_id : $product_id;

    // Get product object
    $product = wc_get_product( $id );

    if ( ! $product ) {
        return;
    }

    // Get product stock
    $product_stock = $product->get_stock_quantity();

    // Get booking session
    if ( isset( WC()->session ) ) {
        $booking_session = WC()->session->get( 'booking' );
    }

    // Get product booking session
    if ( ! empty( $booking_session[$id] ) ) {
		$start = $booking_session[$id]['start'];
        $end = isset( $booking_session[$id]['end'] ) ? $booking_session[$id]['end'] : $start;
    }

    if ( ! empty( $start ) && ! empty( $end ) ) {

        // If variable product with parent product managing stock
        if ( $product->managing_stock() === 'parent' ) {
            $qty_booked = ebac_get_booked_amount_for_daterange( $product_id, $start, $end );
        } else {
            $qty_booked = ebac_get_booked_amount_for_daterange( $id, $start, $end );
        }

    } else {
        $qty_booked = 0;
    }

    $stock_left = $product_stock - $qty_booked;

    return $stock_left;

}

/**
*
* Gets booked dates (Unix format) and stock left for a given product
*
* @param int $product_id
* @param int $variation_id
* @return array $dates
*
**/
function ebac_get_product_booked_dates( $product_id, $variation_id = '' ) {

    // Get product or variation ID
    $id = ! empty( $variation_id ) ? $variation_id : $product_id;

    // Get product object
    $product = wc_get_product( $id );

    // Get product stock
    $product_stock = $product->get_stock_quantity();

    // Get booked dateranges and quantity booked
    $booked_dateranges = get_post_meta( $id, '_booking_days', true );

    if ( ! empty( $booked_dateranges ) ) foreach ( $booked_dateranges as $index => $daterange ) {

        // Get quantity booked for this daterange
        $amount_booked = $daterange[2];

        // Get stock left for this daterange
        $stock_left = $product_stock - $amount_booked;

        // If no stock is left, remove from array
        if ( $stock_left <= 0 ) {
            unset( $booked_dateranges[$index] );
            continue;
        }

        // Set quantity to quantity left
        $booked_dateranges[$index][2] = $stock_left;

    }

    return $booked_dateranges;
}

/**
*
* Gets out-of-stock dates for a given product
*
* @param int $product_id
* @param int $variation_id
* @return array $dates
*
**/
function ebac_get_out_of_stock_dates( $product_id, $variation_id = '' ) {
    
    // Get product or variation ID
    $id = ! empty( $variation_id ) ? $variation_id : $product_id;

    // Get product object
    $product = wc_get_product( $id );

    // Get product stock
    $product_stock = $product->get_stock_quantity();

    // Get booked dateranges and quantity booked
    $booked_dateranges = get_post_meta( $id, '_booking_days', true );

    if ( $booked_dateranges ) foreach ( $booked_dateranges as $index => $daterange ) {

        // Get quantity booked for this daterange
        $amount_booked = $daterange[2];

        // Get stock left for this daterange
        $stock_left = $product_stock - $amount_booked;

        // If stock is left, remove from array
        if ( $stock_left > 0 ) {
            unset( $booked_dateranges[$index] );
            continue;
        }

        // Remove quantity booked from array
        unset( $booked_dateranges[$index][2] );

    }

    // Get an array of all dates in range
    $out_of_stock = array();
    if( $booked_dateranges ) foreach ( $booked_dateranges as $daterange ) {

        if ( isset( $daterange[1] ) ) {
            $dates = ebac_get_dates_from_range( $daterange[0], $daterange[1] );  
        } else {
            $dates = (array) $daterange[0];
        }

        foreach ( $dates as $date ) {
            $out_of_stock[] = $date;
        }

    }

    // Group dates in dateranges
    if ( ! empty( $out_of_stock ) ) {
       $out_of_stock = ebac_group_dates_in_dateranges( $out_of_stock ); 
    }

    return $out_of_stock;

}

/**
*
* Updates product stock after checkout or after deleting an item from the order
*
* @param int $id - product or variation id
* @param string $start
* @param string $end
* @param int $quantity
* @return array $booked - Amount of product booked for each booked date
*
**/
function ebac_get_new_stock( $id, $start, $end, $quantity ) {

    // Get product booked dates
    $booked_dates = get_post_meta( $id, '_booking_days', true );

    $overlap = array();
    
    if ( ! empty( $start ) && ! empty( $end ) ) {

        // Convert newly added dates to timestamps
        $Ustart = strtotime( $start );
        $Uend   = strtotime( $end );

        $product = wc_get_product( $id );
        $date_format = wceb_get_product_booking_dates( $product );

        $offset = $date_format === 'one' ? false : true;

        // Get all the added dates from the daterange
        $incDates = ebac_get_dates_from_range( $start, $end, false, $offset );

        if( $incDates ) foreach ( $incDates as $incDate ) {
           $overlap[$incDate] = $quantity;
        }

    }
    
    $found = false;
    $before = array();
    $after = array();
    if ( $booked_dates ) foreach ( $booked_dates as $index => $daterange ) {

        // Convert each booked daterange start and end to timestamp
        $booked_start = strtotime( $daterange[0] );

        if ( isset( $daterange[1] ) ) {
            $booked_end = strtotime( $daterange[1] );
        } else {
            $daterange[1] = $daterange[0];
            $booked_end = strtotime( $daterange[1] );
        }

        // If dateranges overlap
        if ( ( $Ustart <= $booked_end ) && ( $Uend >= $booked_start ) ) {

            if ( ! $found ) {
                $found = true;
            }

            $dates = ebac_get_dates_from_range( $daterange[0], $daterange[1] );

            foreach ( $dates as $date ) {

                // If date already exists, merge the quantities
                if ( array_key_exists( $date, $overlap ) ) {
                    $overlap[$date] = $overlap[$date] + $daterange[2];
                } else {
                    $overlap[$date] = $daterange[2];
                }
               
            }
            
        } else {

            if ( ! $found ) {
                $before[] = $booked_dates[$index];
                continue;
            } else {
                $after = array_slice( $booked_dates, $index );
                break;
            }

        }

    }

    // Sort dates
    uksort( $overlap, 'ebac_sort_dates' );

    // Group dates in daterange
    $new_dateranges = ebac_group_dates_in_dateranges( $overlap, true, false );

    // Insert new dates inside already booked dates
    $booked = array_merge( $before, $new_dateranges, $after );

    return $booked;

}