<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Availability_Reports' ) ) :

class Easy_Booking_Availability_Reports {

	public function __construct() {

		add_filter( 'easy_booking_reports_tabs', array( $this, 'ebac_reports_tab' ), 10, 1 );
		add_action( 'easy_booking_load_report_scripts', array( $this, 'ebac_load_report_scripts' ) );
		add_action( 'easy_booking_reports_availabilities', array( $this, 'ebac_reports' ) );
        add_filter( 'wceb_reports_booked_products', array( $this, 'ebac_add_imports_to_reports' ), 10, 1 );

	}

	/**
    *
    * Add an "Availabilities" tab to the reports page
    * @param array $wceb_tabs
    * @return array $wceb_tabs
    *
    **/
	public function ebac_reports_tab( $wceb_tabs ) {
		$wceb_tabs['availabilities'] = __('Availabilities', 'easy_booking_availability');
		return $wceb_tabs;
	}

	/**
    *
    * Load scripts and styles on the reports page
    *
    **/
	public function ebac_load_report_scripts() {

        // Load scripts only on the "Availabilities" tab
        if ( ( ! isset( $_GET['tab'] ) ) || ( isset( $_GET['tab'] ) && $_GET['tab'] !== 'availabilities' ) ) {
            return;
        }

		// JS

		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'wc-enhanced-select' );
		wp_enqueue_script( 'jquery-blockui' );

		wp_register_script(
            'easy_booking_availability_reports',
            wceb_get_file_path( 'admin', 'ebac-reports-functions', 'js', EBAC_PLUGIN_FILE ),
            array('jquery'),
            '1.0',
            true
        );

        wp_enqueue_script( 'easy_booking_availability_reports' );

        $low_stock    = get_option('woocommerce_notify_low_stock_amount');
        $out_of_stock = get_option('woocommerce_notify_no_stock_amount');

        $settings = get_option('easy_booking_settings');

        // Get last available date
        $last_date = isset( $settings['easy_booking_last_available_date'] ) ? absint( $settings['easy_booking_last_available_date'] ) : '1825';

        $current_date = date( 'Y-m-d' );
        $last_available_date = date( 'Y-m-d', strtotime( $current_date . ' +' . $last_date . ' days' ) );
        
        wp_localize_script(
            'easy_booking_availability_reports',
            'admin_fc',
            array(
                'ajax_url'     => esc_url( admin_url( 'admin-ajax.php' ) ),
                'low_stock'    => $low_stock,
                'out_of_stock' => $out_of_stock,
                'last_date'     => esc_html( $last_available_date )
            )
        );

        // CSS

        $calc_mode = $settings['easy_booking_calc_mode'];

		wp_register_style(
			'easy_booking_availability_reports_picker',
			wceb_get_file_path( 'admin', 'ebac-reports-picker-' . $calc_mode, 'css', EBAC_PLUGIN_FILE ),
			true
		);

        wp_enqueue_style( 'easy_booking_availability_reports_picker' );
        

	}

	/**
    *
    * Display reports
    *
    **/
	public function ebac_reports() {
		include_once( 'views/ebac-html-reports.php' );
	}

    /**
    *
    * Add imported bookings to the reports page
    * @param array $booked_products - Products booked by clients
    * @return array $booked_products - Products booked by clients + manually imported bookings
    *
    **/
    function ebac_add_imports_to_reports( $booked_products ) {
        // Get manually imported bookings
        $imports = ebac_get_imported_booked_dates( false );

        // Merge ordered products and imported bookings
        $booked_products = array_merge( $booked_products, $imports );

        return $booked_products;
    }
}

return new Easy_Booking_Availability_Reports();

endif;