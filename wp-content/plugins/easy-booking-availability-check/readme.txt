==== Easy Booking : Availability Check ====
Contributors: @_Ashanna
Tags: woocommerce, booking, renting, products, stocks, availability
Requires at least: 4.0, WooCommerce 3.0, WooCommerce Easy Booking 2.1.7
Tested up to: 4.9.6, WooCommerce 3.4.2
Stable tag: 1.6.5
License: GPLv2 or later

Easy Booking: Availability Check is an add-on for WooCommerce Easy Booking to manage stocks and availabilities.

== Description ==

This plugins allows stock and availability management for “WooCommerce Easy Booking”.

Enable stocks and set the amount of a bookable bookable item and “Easy Booking: Availability Check” will prevent products from being booked during out-of-stock dates.

It will display the amount left for the chosen date on the client side, and prevent the client from buying the product if there isn’t enough stock.

== Installation ==

Make sure you have installed and activated WooCommerce and WooCommerce Easy Booking.

1. Install the “Easy Booking: Availability Check” Plugin.
2. Activate the plugin.
3. Enter your license key. For multisites, install the plugin and enter the license key on the network. Then, activate it on each sites.
4. On your administration panel, go to the Easy Booking menu, and the Availability Check sub-menu to set the plugin settings.
5. Make sure you enabled stock management in WooCommerce Settings (WooCommerce > Settings > Products > Inventory).
6. On your administration panel, Check “Manage stock?” on the bookable product page, and set the stock amount of the product or variation. Backorders can't be allowed for bookable products.
7. To manually import bookings, go to the "Imports" tab on the plugin settings page (Easy Booking > Availability Check).
8. If stocks are not initialized, or if you experience issues with availabilities, initialize them in Easy Booking tab > Availability Check.
9. And that’s it !

== Changelog ==

= 1.6.5 - 2018-06-08 =

* Fix - Issue with "Imports" when no unavailability period was set.

= 1.6.4 - 2018-06-04 =

* Fix - Issue when manually adding an order with only one date.
* Fix - Issue with Imports and Unavailability periods being too long.
* Fix - Compatibility with previous WooCommerce versions.

= 1.6.3 =

* Fix - Updated deprecated $variation->backorders to $variation->get_backorders().
* Fix - Correctly display "Out of stock" text if the product is out of stock.
* Tweak - Improved filters in the "Imports" page.

= 1.6.2 =

* Fix - Issue with availabilities in different timezones.
* Fix - Deprecated get_cart_url() function.
* Fix - CSS issue when selecting month/year in the "Reports" page.
* Tweak - Improved the "Availabilities" tab in the "Reports" page.
* Tweak - Compatibility with "Last available date".
* Tweak - Compatibility with Easy Booking 2.1.7.

= 1.6.1 =

* Add - Added manually imported bookings to the "Reports" page for a better organization.
* Add - Added booking statuses to manually imported bookings to track them in the "Reports" page.
* Add - Added an option to automatically delete manually imported bookings once they ended.
* Add - Added an animation when initializing availabilities.
* Fix - Fixed an issue with grouped/bundled products (file not minified correctly).
* Fix - Fixed an issue when imported bookings contain products which don't exist anymore.
* Fix - Fixed an issue where availabilities were updated twice when making an order.

= 1.6.0 =

* Fix - Update availabilities when changing order status.
* Fix - Maybe import bundled items when importing a bundle product.

= 1.5.9 =

* Fix - Correctly update availabilities when refunding an item.
* Fix - Issue where products were removed from the cart if not managing stocks.
* Fix - CSS and JS issues with month/year selection in the Reports page.
* Fix - 'ebac_order_processed' action hook, with the right availabilities if variation stocks managed at parent product level.
* Add - 'ebac_availability_text' filter to modify availability text.
* Add - 'ebac_availability_class' filter to modify availability class.

= 1.5.8 =

* Fix - Make sure products are still available for the selected dates on cart and checkout.
* Fix - Possibility to import several products at the same time when manually importing a booking.
* Tweak - Improved "Reports" page in "Nights" mode.

= 1.5.7 =

* Fix - Issue when modifying/creating orders in the admin. Availabilities should now be updated correctly.

= 1.5.6 =

* Fix - Compatibility with WooCommerce 3.0.
* Add - Option to display a colored circle instead of the available stock amount on the product page.
* Add - ebac_display_availabilities filter.

= 1.5.5 =

* Fix - [Admin] Update notification.
* Fix - Compatibility with WooCommerce Product Bundles.
* Tweak - Modified an action hook for compatibility with WPML.

= 1.5.4 =

* Fix - Compatibility with WooCommerce Product Bundles.
* Fix - Limit "Unavailability period" to 30 days, to avoid issues.
* Updated ebac.pot file.
* Removed included language files, please visit http://herownsweetcode.com/easy-booking/documentation/availability-check/localization/ to download available translation files.

= 1.5.3 =

* Fix - [Imports] Issue when manually adding bookings with several products.

= 1.5.2 =

* Fix - [Admin] Issue when generating CSS file on multisites.
* Fix - [Admin] Issue on the reports page when no availability data is available for a product.

= 1.5.1 =

* Add - [Filter] 'ebac_booked_products' filter for compatibilty with Polylang.
* Add - [Hook] 'ebac_order_processed' hook in 'ebac_update_order_product' function for compatibilty with Polylang.
* Fix - [Frontend] Unavailability period is now also applied BEFORE the booked dates.
* Fix - [Admin] Issue on the imports table if a column name is empty.
* Fix - [Admin] Apply unavailability period to imported bookings.

= 1.5.0 =

/!\ Because of the new features added, this is only compatible with WooCommerce Easy Booking 2.0.  Please update it if you have a lower version.

* Add - [Feature] Compatibility with WooCommerce Product Bundles.
* Add - [Feature] One date only compatibility.
* Add - [Feature] Unavailability period after a booking, in case you want to make your products unavailable for x days after the end of a booking.
* Fix - [Frontend] Issue when variation doesn't manage stock.
* Fix - [Admin] Prevent WooCommerce from reducing stock in the database, so items are not set to "Out of stock" which also prevents useless email notifications.

= 1.4 =

* Add     - [Admin] Hooked 'ebac_cancelled_order' to 'woocommerce_order_status_cancelled' to update availabilities after cancelling an order.
* Removed - [Admin] - Product availability report on the admin product page (as it is already on the reports page).
* Fix     - [Frontend] Stock management and backorders.
* Fix     - [Admin] Hooked 'ebac_update_booked_products' into 'woocommerce_reduce_order_stock' instead of 'woocommerce_checkout_order_processed'.
* Fix     - [Admin] Check date format before getting booked items from orders.
* Fix     - [Admin] ajax function to init availabilities.
* Tweak   - [Admin] Store dates in dateranges if possible, instead of individually, before loading them on the frontend to avoid too much data.
* Tweak   - Improved calendar's CSS.
* Tweak   - Reviewed, updated and improved code to optimize the plugin.

= 1.3.6 =

* Fix - [Admin] Issue with the import form not saving all pages.
* Fix - [Admin] Function to init availabilities.

= 1.3.5 =

* Add - [Plugin compatibility] Hooks and filters for compatibility with Polylang.
* Fix - [Admin] Error on the import form.
* Fix - [Admin] Issue on the reports page when a product was deleted.

= 1.3.4 =

* Add   - [Admin][Imports] 'easy_booking_availability_imports_columns' filter to add a custom column to the import form.
* Add   - [Admin][Imports] 'easy_booking_availability_display_custom_column' action hook to display a custom column to the import form.
* Add   - [Admin][Imports] 'easy_booking_availability_save_custom_imports' filter to save a custom column to the import form.
* Add   - [Admin]Load translation file for the admin datepickers.
* Fix   - [Admin][Imports] Wrong ajax function to get bookable and managing stocks products in the imports form.
* Fix   - [Admin][Variable products] Error when there is no availability data when loading variations.
* Fix   - [Admin]Limit to maximum year on the date picker.
* Tweak - [Admin][Imports] Improved the import system.
* Tweak - [Admin]Improved notices.
 
= 1.3.3 =

* Add   - Multisite compatibility.
* Fix   - Display negative stocks on the reports page.
* Fix   - Bug when initializing availabilities if a product was deleted.
* Tweak - Delete database entries for each sites on multisites when uninstalling the plugin.

= 1.3.2 =

* Fix - Priority to enqueue frontend scripts (compatibility with Easy Booking : Disable Dates).

= 1.3.1 =

* Add    - Compatibility with Easy Booking : Disable Dates.
* Add    - Show availability calendar on variable parent product when managing stocks at parent product level on the admin product page.
* Add    - Option to color availabilities on the front-end (green : in stock, orange : low stock).
* Add    - WCEB_PATH and WCEB_SUFFIX constants to load scripts and styles.
* Add    - ebac.pot file for translations.
* Fix    - Admin calendars for variable products.
* Fix    - Stock left fragment on simple product to set max on the quantity input.
* Fix    - License key notice.
* Tweak  - Changed classes of the static picker to avoid conflicts.
* Update - French translation.

= 1.3 =

* Add      - Compatibility with grouped products.
* Fix      - Compatibility with WooCommerce > 2.4.
* Fix      - Update process.
* Improved - Reports page.
* Improved - Notices.

= 1.2 =

* Add   - Possibility to manually import bookings.
* Add   - Possibility to manage stocks at parent product level for variable products.
* Add   - Option to display or not availabilities on the front-end.
* Fix   - CSS generation on multisite.
* Tweak - Improved Javascript for better flexibility and performance.

= 1.1.1 =

* Fix - Automatic update system.
* Fix - Allowing backorders on variations.
* Fix - Getting session when checking variation stock.
* Fix - Disabled dates when in "nights" mode.
* Add - Availability reports page on the admin.

= 1.1 =

This update contains major changes for variable products. You must update "WooCommerce Easy Booking" to 1.5 before in order to have this plugin work, as backward compatibility couldn't be done.

* Add - Variations are now handled individually, instead of inheriting from the parent product.
* Add - Multisite compatibility.
* Add - Booking reports on the plugin settings page.
* Add - Variable to include past dates when getting booked dates.
* Fix - Quantity input "max" attribute after selecting dates.
* Fix - Security update.
* Fix - Wrong $wpdb calls.

= 1.0 =
* Initial Release

== Upgrade Notice ==

= 1.5.0 =

This version requires WooCommerce Easy Booking 2.0.

= 1.4 =

This version requires WooCommerce Easy Booking 1.9, please update it before or it won't work.