(function($) {
	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd = $('.datepicker_end').pickadate();

		// Picker objects.
		var pickerStart = $inputStart.pickadate('picker'),
			pickerEnd = $inputEnd.pickadate('picker');

		var calcMode              = data.calc_mode,
			disabledDates         = data.disabled_dates, // Out of stock dates
			bookedDates           = data.availabilities, // Quantity left for booked dates
			stocks                = data.stock; // Product stock
			lowStock              = data.low_stock, // Low stock threshold
			displayAvailabilities = data.display_availabilities; // Display stock numbers
		
		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;

		var startPickerObject = pickerStart.$root,
			endPickerObject   = pickerEnd.$root;

		var toDisable = ebac.getDates( disabledDates, calcMode );

		var formattedDates      = toDisable.dates, // Array of dates to disable
			picks               = toDisable.times, // Array of timestamps
			formattedStartDates = toDisable.startDates, // Array of dates to disable on the start calendar if in "Nights" mode (-1 day)
			startPicks          = toDisable.startTimes, // Array of timestamps for the start calendar if in "Nights" mode (-1 day)
			booked              = ebac.getTimesWithQty( bookedDates );
		
		$('body').on('pickers_init', function(e, variation) {
			ebac.disableOutOfstock( toDisable, pickerStartItem, pickerEndItem, calcMode );
			return false;
		});

		$('body').on('update_price', function( e, data, response ) {

			if ( $('.cart').find( '.quantity input.qty' ).size() ) {

				var qtyInput = $('.cart').find( '.quantity input.qty' );

				if ( response.fragments && response.fragments.stock_left !== '' ) {

					var qty_selected = qtyInput.val();
					var stock_left = response.fragments.stock_left;

					qtyInput.attr( 'max', stock_left );

					if ( stock_left < qty_selected ) {
						qtyInput.val( stock_left );
					}

				} else {
					qtyInput.removeAttr( 'max' );
				}

			}

		});

		pickerStart.on({
			render: function() {
				ebac.greyOutBookedDates( toDisable, calcMode );

				if ( displayAvailabilities !== 'none' ) {
					ebac.appendStockAmount( startPickerObject, booked, stocks, lowStock, displayAvailabilities, calcMode );
				}
			},
		});

		$('body').on('clear_start_picker', function(e, pickerEndItem) {
			pickerEndItem.disable = formattedDates;
			return false;
		});

		pickerEnd.on({
			render: function() {
				ebac.greyOutBookedDates( toDisable, calcMode );

				if ( displayAvailabilities !== 'none' ) {
					ebac.appendStockAmount( endPickerObject, booked, stocks, lowStock, displayAvailabilities, calcMode );
				}
			}
		});

		$('body').on('clear_end_picker', function(e, pickerStartItem) {
			pickerStartItem.disable = ( calcMode === 'nights' ) ? formattedStartDates : formattedDates;
			return false;
		});
		
	});
})(jQuery);