(function($) {
	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd   = $('.datepicker_end').pickadate(),
			pickerStart = $inputStart.pickadate('picker'),
			pickerEnd   = $inputEnd.pickadate('picker');

		var startPickerObject = pickerStart.$root,
			endPickerObject   = pickerEnd.$root;

		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;
		
		var mode                  = data.calc_mode, // Calc mode ("Days" or "Nights")
			disabledDates         = data.disabled_dates, // Out-of-stock dates for each variation
			bookedDates           = data.availabilities, // Booked dates with quantity booked for each variation
			managingStock         = data.managing_stock, // Variation manages stock or not
			stocks                = data.stock, // Stock quantity for each variation
			lowStock              = data.low_stock, // Low stock threshold
			displayAvailabilities = data.display_availabilities; // Display stock numbers

		// On variation select
		$('body').on( 'pickers_init', function( e, variation ) {

			// Get variation ID
			variationId = variation.variation_id;

			calcMode = mode[variationId];

			// Variation manages stocks?
			variationManagesStock = managingStock[variationId];

			// If variation doesn't manages stock, return
			if ( ! variationManagesStock ) {
				return false;
			}

			// Get variation out-of-stock dates in different formats
			toDisable = ebac.getDates( disabledDates[variationId], calcMode );

			formattedDates = {
				startDates: toDisable.startDates, // Array of dates to disable on the start calendar if in "Nights" mode (-1 day)
				dates     : toDisable.dates // Array of dates to disable
			}

			times = {
				startTimes: toDisable.startTimes, // Array of timestamps for the start calendar if in "Nights" mode (-1 day)
				times     : toDisable.times // Array of timestamps
			}

			// Get variation booked dates with quantity
			booked = ebac.getTimesWithQty( bookedDates[variationId] );

			// Disable dates on pickers
			ebac.disableOutOfstock( formattedDates, pickerStartItem, pickerEndItem, calcMode );

			return false;

		});

		// After price calculation
		$('body').on( 'update_price', function( e, data, response ) {

			if ( $('.variations_form').find( '.quantity input.qty' ).size() ) {

				var qtyInput = $('.variations_form').find( '.quantity input.qty' );

				if ( response.fragments && response.fragments.stock_left !== '' ) {

					var qty_selected = qtyInput.val();
					var stock_left = response.fragments.stock_left;

					qtyInput.attr( 'max', stock_left );

					if ( stock_left < qty_selected ) {
						qtyInput.val( stock_left );
					}

				} else {
					qtyInput.removeAttr( 'max' );
				}

			}

		});

		pickerStart.on({
			render: function() {

				// Display stock amount for each date
				if ( displayAvailabilities !== 'none' && variationManagesStock ) {
					ebac.appendStockAmount( startPickerObject, booked, stocks[variationId], lowStock, displayAvailabilities, calcMode );
				}

				// Grey out out of stock dates
				if ( variationManagesStock ) {
					ebac.greyOutBookedDates( times, calcMode );
				}

			}
		});

		$('body').on( 'clear_start_picker', function( e, pickerEndItem ) {

			if ( ! variationManagesStock ) {
				return false;
			}

			if ( ! formattedDates ) {
				pickerEndItem.disable = [];
				return false;
			}

			pickerEndItem.disable = formattedDates.dates;

			return false;
		});

		pickerEnd.on({
			render: function() {

				// Display stock amount for each date
				if ( displayAvailabilities !== 'none' && variationManagesStock ) {
					ebac.appendStockAmount( endPickerObject, booked, stocks[variationId], lowStock, displayAvailabilities, calcMode );
				}

				// Grey out out of stock dates
				if ( variationManagesStock ) {
					ebac.greyOutBookedDates( times, calcMode );
				}

			}
		});

		$('body').on( 'clear_end_picker', function( e, pickerStartItem ) {

			if ( ! variationManagesStock ) {
				return false;
			}

			if ( ! formattedDates ) {
				pickerStartItem.disable = [];
				return false;
			}

			pickerStartItem.disable = ( calcMode === 'nights' ) ? formattedDates.startDates : formattedDates.dates;

			return false;
		});
		
	});
})(jQuery);