(function($) {
	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd   = $('.datepicker_end').pickadate();

		// Picker objects.
		var pickerStart = $inputStart.pickadate('picker'),
			pickerEnd   = $inputEnd.pickadate('picker');

		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;

		var calcMode      = data.calc_mode,
			disabledDates = data.disabled_dates; // Booked dates
			managingStock = data.managing_stock, // Product manages stock or not

		$('body').on( 'pickers_init', function( e, ids ) {

			formattedDates = {};
			times          = {};

			formattedDates['dates']      = [];
			formattedDates['startDates'] = [];

			times['times']      = [];
			times['startTimes'] = [];

			$.each( ids, function( id, qty ) {

				if ( qty > 0 && managingStock[id] ) {

					var dates = disabledDates[id];

					var thisOutOfStockDates = ebac.getDates( dates, calcMode );

					if ( thisOutOfStockDates.dates.length > 0 ) {

						$.each( thisOutOfStockDates.dates, function( index, date ) {
							formattedDates['dates'].push( date );
						});

					}

					if ( thisOutOfStockDates.startDates.length > 0 ) {

						$.each( thisOutOfStockDates.startDates, function( index, date ) {
							formattedDates['startDates'].push( date );
						});

					}

					if ( thisOutOfStockDates.times.length > 0 ) {

						$.each( thisOutOfStockDates.times, function( index, time ) {
							times['times'].push( time );
						});

					}

					if ( thisOutOfStockDates.startTimes.length > 0 ) {

						$.each( thisOutOfStockDates.startTimes, function( index, time ) {
							times['startTimes'].push( time );
						});

					}

				}
				
			});

			ebac.disableOutOfstock( formattedDates, pickerStartItem, pickerEndItem, calcMode );

			return false;

		});

		$('body').on('update_price', function( e, data, response ) {

			if ( response.fragments && typeof response.fragments.stock_info != 'undefined' ) {

				$.each( response.fragments.stock_info, function( ID, stock_info ) {

					if ( wceb.productType === 'grouped' ) {

						var qtyInput = $('.cart').find( '.quantity input.qty[name="quantity[' + ID + ']"]'),
							qty = qtyInput.val();

						$('form.cart')
							.find('.grouped_product_id[data-product_id="' + ID + '"]')
							.next('.price')
							.children('p.stock')
							.addClass(stock_info.class)
							.html(stock_info.text);

					} else if ( wceb.productType === 'bundle' ) {

						// If is a bundled item
						if ( $('.cart[data-product_id="' + ID + '"]').size() ) {

							var $cart = $('.cart[data-product_id="' + ID + '"]');

							// If it is a variable product, get selected variation stock info instead
							if ( $cart.find('input.variation_id').size() ) {

								var variation_id = $cart.find('input.variation_id').val();

								if ( typeof response.fragments.stock_info[variation_id] !== 'undefined' ) {
									stock_info.class = response.fragments.stock_info[variation_id]['class'];
									stock_info.text  = response.fragments.stock_info[variation_id]['text'];
									stock_info.qty   = response.fragments.stock_info[variation_id]['qty'];
								}

							}

							var bundledItemID = $cart.data('bundled_item_id');
								qtyInput = $cart.find('.quantity input.qty[name="bundle_quantity_' + bundledItemID + '"]'),
								qty = qtyInput.val();

							$cart.find('p.stock')
								 .addClass(stock_info.class)
								 .html(stock_info.text);

						// If is main bundle product
						} else if ( $('.cart.bundle_data[data-bundle_id="' + ID + '"]').size() ) {

							var $cart = $('.cart.bundle_data[data-bundle_id="' + ID + '"]');

							var qtyInput = $cart.find('.bundle_button input.qty'),
								qty = qtyInput.val();

							$cart.find('p.stock')
								 .addClass(stock_info.class)
								 .html(stock_info.text);

						} else {
							return;
						}
						
					}

					if ( typeof qtyInput !== 'undefined' ) {

						qtyInput.attr( 'max', stock_info.qty );

						if ( qty > stock_info.qty ) {
							qtyInput.val(stock_info.qty);
						}

					}

				});

			} else {
				$('.cart').find( '.quantity input.qty' ).removeAttr( 'max' );
			}

		});

		pickerStart.on({
			render: function() {
				ebac.greyOutBookedDates( times, calcMode );
			},
		});

		$('body').on('clear_start_picker', function(e, pickerEndItem) {

			if ( ! formattedDates ) {
				pickerEndItem.disable = [];
				return false;
			}

			pickerEndItem.disable = formattedDates.dates;
			return false;
		});

		pickerEnd.on({
			render: function() {
				ebac.greyOutBookedDates( times, calcMode );
			}
		});

		$('body').on('clear_end_picker', function(e, pickerStartItem) {

			if ( ! formattedDates ) {
				pickerStartItem.disable = [];
				return false;
			}
			
			pickerStartItem.disable = ( calcMode === 'nights' ) ? formattedDates.startDates : formattedDates.dates;
			return false;
		});
		
	});
})(jQuery);