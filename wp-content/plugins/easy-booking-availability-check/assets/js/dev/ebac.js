( function( $, root ) {

	var ebac = {};

	// Get an array of dates / dateranges in timestamps with quantity
	var getTimesWithQty = ebac.getTimesWithQty = function( dates ) {

		var times = [];
		
		if ( typeof dates !== 'undefined' && dates !== '' ) {

				$.each( dates, function( index, data ) {
	 
	 			// Quantity for the date
				var qty = data[2];
	 
	 			// First date
				var startDate = new Date( data[0] + "T00:00:00" );

				// Get time
				var startTime = startDate.getTime();

				// If end is set (daterange)
				if ( typeof data[1] !== 'undefined' ) {

					// Second date
					var endDate = new Date( data[1] + "T00:00:00" );

					// Get time
					var endTime = endDate.getTime();

					// Store data
					var pick = { start: startTime, end: endTime, qty: qty }

				} else { // If only one date

					// Store data
					var pick = { start: startTime, qty: qty }

				}

				// Push data in array
				times.push( pick );
				
			});

		}

		return times;

	}

	// Get an array of out-of-stock dates
	// Dates
	// Timestamps
	// End dates (1 day offset)
	// End timestamps (1 day offset)
	var getDates = ebac.getDates = function( dates, mode ) {

		var toDisable = {
			dates     : [],
			startDates: [],
			times     : [],
			startTimes: []
		}

		if ( typeof dates !== 'undefined' && dates.length > 0 ) {

			$.each( dates, function( index, date ) {

				// Get start date
				startDateRange = date[0];

				// Convert start date into time
				startDate = new Date( startDateRange[0],startDateRange[1],startDateRange[2] );

				startPick = startDate.getTime();

				// If is a daterange
				if ( typeof date[1] !== 'undefined' ) {

					// Get end date
					endDateRange = date[1];

					// Store daterange
					dateToDisable = {
						type: 'booked',
						from: startDateRange,
						to  : endDateRange
					};

					// Convert end date into time
					endDate = new Date( endDateRange[0],endDateRange[1],endDateRange[2] );
					endPick = endDate.getTime();

					// Store daterange in times
					time = {
						start: startPick,
						end  : endPick
					}

				} else { // If is single date

					// Store date
					dateToDisable = startDateRange;

					// Store time
					time = startPick;

					// Store type value to indicate it is an out-of-stock date
					dateToDisable['type'] = 'booked';

				}

				toDisable['dates'].push( dateToDisable ); // Array of dates object (day mode)
				toDisable['times'].push( time ); // Array of dates in milliseconds (day mode)

				// If calculation mode is "nights", add one day to booked dates, to disable them
				// on the start calendar.
				if ( mode === 'nights' ) {

					// Get start date with one more day
					startDisable = startDate.setDate( startDate.getDate() - 1 ); // Remove one day

					// Convert new start date into time
					startDisableDate = new Date( startDisable );
					startDisablePick = startDate.getTime();

					// If is a daterange
					if ( typeof date[1] !== 'undefined' ) {

						// Get end date with one more day
						endDisable = endDate.setDate( endDate.getDate() - 1 ); // Remove one day

						// Convert new end date into time
						endDisableDate = new Date( endDisable );
						endDisablePick = endDisableDate.getTime(); // Get date

						// Store daterange
						formattedStartdate = {
							type: 'booked',
							from: [startDisableDate.getFullYear(),startDisableDate.getMonth(),startDisableDate.getDate()],
							to  : [endDisableDate.getFullYear(),endDisableDate.getMonth(),endDisableDate.getDate()]
						};

						// Store daterange in times
						startTime = {
							start: startDisablePick,
							end  : endDisablePick
						}

					} else { // If is a single date

						// Store date
						formattedStartdate = [startDisableDate.getFullYear(),startDisableDate.getMonth(),startDisableDate.getDate()];

						// Store time
						startTime = startDisablePick;

						// Store type value to indicate it is an out-of-stock date
						formattedStartdate['type'] = 'booked';
					}

					toDisable['startDates'].push( formattedStartdate ); // Array of date objects (night mode)
					toDisable['startTimes'].push( startTime ); // Array of dates in milliseconds (night mode)

				}

			});

		}

		return toDisable;

	}

	// Filter calendar days with they data-pick attribute
	var filterDays = ebac.filterDays = function ( $picker, dateRange ) {

		var $day = $picker.filter( function() {

			var pick = $(this).data('pick');

			if ( typeof dateRange === 'object' ) {
				return pick >= dateRange.start && pick <= dateRange.end;
			} else {
				return pick === dateRange;
			}
			
		});

		return $day;
	}

	// Disable out of stock dates
	var disableOutOfstock = ebac.disableOutOfstock = function( dates, pickerStartItem, pickerEndItem, mode ) {

		var toDisable = dates.dates;

		if ( toDisable.length <= 0 ) {
			pickerStartItem.disable = [];
			pickerEndItem.disable   = [];
			return false;
		}

		pickerStartItem.disable = ( mode === 'nights' ) ? dates.startDates : toDisable;

		// In "Nights" mode, end calendar dates have a one day offset
		pickerEndItem.disable = toDisable;

		return false;
		
	}

	// Apply a CSS class on out of stock dates to differentiate them from disabled dates
	var greyOutBookedDates = ebac.greyOutBookedDates = function( times, mode ) {

		if ( ! times ) {
			return;
		}

		if ( mode === 'nights' ) {

			$.each( times.startTimes, function( index, datePick ) {
				var $day = ebac.filterDays( $('#start_date_table .picker__day'), datePick );
				$day.addClass('booked');
			});

			$.each( times.times, function( index, datePick ) {
				var $day = ebac.filterDays( $('#end_date_table .picker__day'), datePick );
				$day.addClass('booked');
			});

		} else {

			$.each( times.times, function( index, datePick ) {
				var $day = ebac.filterDays( $('.picker__day'), datePick );
				$day.addClass('booked');
			});

		}
		
	}

	// Display quantity available on each date of the calendars
	var appendStockAmount = ebac.appendStockAmount = function( picker, booked, stocks, lowStock, displayAvailabilities, mode ) {

		$pickerDay = picker.find('.picker__day:not(.picker__day--disabled)');

		if ( displayAvailabilities === 'amount' || displayAvailabilities === 'amount_colors' ) {
			$pickerDay.addClass('picker__day--stock');
		} else {
			$pickerDay.addClass('picker__day--status');
		}

		c = '';

		if ( booked && booked.length > 0 ) {

			$.each( booked, function( index, date ) {

				var start = date.start,
					qty = date.qty;

				if ( typeof date.end !== 'undefined' ) {
					var end = date.end;
				}

				// If in "Nights" mode, start calendar dates have a 1 day offset
				if ( mode === 'nights' && true === picker.is('#start_date_root') ) {

					start -= 86400000;

					if ( typeof date.end !== 'undefined' ) {
						end -= 86400000;
					}

				}

				if ( displayAvailabilities === 'colors' || displayAvailabilities === 'amount_colors' ) {
					c = ' in_stock';
				}

				if ( qty <= lowStock && ( displayAvailabilities === 'colors' || displayAvailabilities === 'amount_colors' ) ) {
					c = ' low';
				}

				var $day = $pickerDay.filter( function() {

					var pick = $(this).data('pick');

					if ( typeof date.end !== 'undefined' ) {
						return pick >= start && pick <= end;
					} else {
						return pick === start;
					}
					
				});

				if ( displayAvailabilities === 'amount' || displayAvailabilities === 'amount_colors' ) {
					$day.addClass('available')
						.append('<span class="ebac-stock-amount' + c + '">' + qty + '</span>');
				} else {
					$day.addClass('available')
						.append('<span class="ebac-stock-status' + c + '"></span>');
				}
				
			});

		}

		if ( displayAvailabilities === 'colors' || displayAvailabilities === 'amount_colors' ) {
			c = ' in_stock';
		}

		if ( ( displayAvailabilities === 'amount' || displayAvailabilities === 'amount_colors' ) ) {
			var html = '<span class="ebac-stock-amount' + c + '">' + stocks + '</span>';
		} else {
			var html = '<span class="ebac-stock-status' + c + '"></span>';
		}

		picker
			.find('.picker__day:not(.picker__day--disabled):not(.available)')
			.append( html );
		
	}

	root['ebac'] = ebac;

} ( jQuery, this ) );