(function($) {
	$(document).ready(function() {

		var maxYear = new Date( admin_fc.last_date + 'T00:00:00' );

		$input = $('.ebac_reports_pickadate').pickadate({
			today       : false,
			clear       : false,
			close       : false,
			klass       : {
				picker: 'reports_picker',
				holder: 'reports_picker__holder',
				frame : 'reports_picker__frame',
				wrap  : 'reports_picker__wrap',
				box   : 'reports_picker__box'
			},
			selectYears : true,
  			selectMonths: true,
  			max         : maxYear,
		});

		var picker = $input.pickadate( 'picker' ),
			pickerObject = picker.$root;

		picker.open();

		$('#ebac_reports_search').on( 'click', function() {
			
			id = $('#ebac_product_id').val();

			if ( id ) {
				getProductAvailabilities( id );
			}

		});

		picker.on({
			render: function() {
				if ( id ) {
					appendStockAmount();
				}
			}
		});

		function getProductAvailabilities( id ) {
			var data = {
				action: 'easy_booking_availability_get_stocks',
				id    : id
			};

			$('.reports_picker').fadeTo( '400', '0.6' ).block({
				message: null,
				overlayCSS: {
					background: 'transparent',
					backgroundSize: '16px 16px',
					opacity: 0.6
				} 
			});

			// Ajax request
			$.post( admin_fc.ajax_url, data, function( response ) {

				availabilities = response.availabilities;
				stock          = response.product_stock;

				appendStockAmount();

				// Unblock
				$('.reports_picker').stop(true).css('opacity', '1').unblock();

				picker.open(false);

			});
		}

		picker.on({
		  close: function() {
		    picker.open(false);
		  }
		});

		function appendStockAmount() {
			var lowStock   = parseInt( admin_fc.low_stock ),
				outOfStock = parseInt( admin_fc.out_of_stock );

			var $pickerDay = pickerObject.find('.picker__day');

			if ( stock <= lowStock ) {
				$pickerDay.append( '<span class="stock-amount low-stock">' + stock + '</span>');
			} else {
				$pickerDay.append( '<span class="stock-amount">' + stock + '</span>');
			}

			if ( availabilities.length > 0 ) {

				$.each( availabilities, function( index, data ) {

					var start = new Date( data[0] ),
						qty   = data[2];

					start.setHours(0,0,0,0);

					var startTime = start.getTime();

					if ( typeof data[1] !== 'undefined' ) {

						var end = new Date( data[1] );

						end.setHours(0,0,0,0);

						var endTime = end.getTime();

						var $day = $pickerDay.filter( function() {
							var pick = $(this).data( 'pick' );
							return pick >= startTime && pick <= endTime;
						});

					} else {

						var $day = $pickerDay.filter( function() {
							var pick = $(this).data('pick');
							return pick === startTime;
						});

					}

					if ( qty <= outOfStock ) {
						$day.find( 'span.stock-amount' ).removeClass('low-stock').addClass('no-stock').html( qty );
					} else if ( qty <= lowStock ) {
						$day.find( 'span.stock-amount' ).addClass('low-stock').html( qty );
					} else {
						$day.find( 'span.stock-amount' ).removeClass('low-stock').html( qty );
					}
					
				});

			}
			
		}
		
	});
})(jQuery);