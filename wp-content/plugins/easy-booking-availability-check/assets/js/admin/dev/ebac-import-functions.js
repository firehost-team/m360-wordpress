(function($) {
	$(document).ready(function() {

		$.extend( $.fn.pickadate.defaults, {
			formatSubmit: 'yyyy-mm-dd',
			hiddenName  : true,
			selectYears : true,
  			selectMonths: true
		});

		initPickers = function() { 
			$('.ebac-import-line').each( function() {

				var $this = $(this);

				$inputStart = $this.find('.datepicker_start').pickadate();
				$inputEnd   = $this.find('.datepicker_end').pickadate();

				pickerStart = $inputStart.pickadate('picker');
				pickerEnd   = $inputEnd.pickadate('picker');

				var startDate = pickerStart.get('select'),
					endDate   = pickerEnd.get('select');

				if ( typeof startDate != 'undefined' && startDate != null ) {
					setEndPicker( pickerEnd, startDate.obj );
				}

				if ( typeof endDate != 'undefined' && endDate != null ) {
					setStartPicker( pickerStart, endDate.obj );
				}

				setPickers( pickerStart, pickerEnd );

			});
		};

		initPickers();

		function setEndPicker( picker, startDate ) {

			if ( ajax.calc_mode === 'nights' ) {
				startDate.setDate( startDate.getDate() + 1 );
			}

			picker.set({
				min: startDate
			}, { muted: true });
		}

		function setStartPicker( picker, endDate ) {

			if ( ajax.calc_mode === 'nights' ) {
				endDate.setDate( endDate.getDate() + 1 );
			}

			picker.set({
				max: endDate
			}, { muted: true });
		}

		function setPickers( pickerStart, pickerEnd ) {

			pickerStart.on({
				set: function(startTime) {

					if ( typeof startTime.clear != 'undefined' && startTime.clear == null ) {
						selectedStartDate = false;
					} else {
						selectedStartDate = new Date(startTime.select);
					}
					
					setEndPicker( pickerEnd, selectedStartDate );
				},
				close: function() {
					$(document.activeElement).blur();
				}
			});

			pickerEnd.on({
				set: function(endTime) {

					if ( typeof endTime.clear != 'undefined' && endTime.clear == null ) {
						selectedEndDate = false;
					} else {
						selectedEndDate = new Date(endTime.select);
					}
					
					setStartPicker( pickerStart, selectedEndDate );
				},
				close: function() {
					$(document.activeElement).blur();
				}
			});

		}

		// Add a booking
		$('.add-booking').on('click', function(e) {
			e.preventDefault();
			$('.ebac-import-table').show();
		});

		// Cancel add
		$('.ebac-import-table').on( 'click', '.cancel-edit', function(e) {
			e.preventDefault();
			$('.ebac-import-table').hide();
		});

		// Delete a booking
		$('.imports').on('click', '.delete-import', function(e) {
			e.preventDefault();

			var data = {
				action: 'ebac_delete_import',
				index : $( this ).parents( '.ebac-import-line' ).data( 'index' )
			}

			// Block
			$('.imports-wrap').fadeTo('400', '0.6').block({
				message: null,
				overlayCSS: {
					background: 'transparent',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});
			
			// Ajax request
			$.post( ajax.ajax_url, data, function( response ) {

				// Unblock
				$('.imports-wrap').stop(true).css('opacity', '1').unblock();

				// Reload page
				var updateURL = location.href + ('&settings-updated=true');
				window.location = updateURL;

			});
		});

		// Edit a booking
		$('.imports').on('click', '.edit-import', function(e) {
			e.preventDefault();

			$( '.imports' ).find( '.ebac-import-line' ).removeClass( 'import-editing' );

			var $otherLines = $( '.imports' ).find( '.ebac-import-line:not(.import-editing)' );

			$otherLines.find('input').prop( 'disabled', true );
			$otherLines.find('.import-edit-actions').hide();
			$otherLines.find('.edit-import').show();

			var $line = $( this ).parents( '.ebac-import-line' );

			$line.addClass( 'import-editing' );

			$line.find('.column-edit').addClass('expanded');

			var fields = $line.find('input, select');

			fields.prop( 'disabled', false );
			$line.find('.import-edit-actions').show();
			$(this).hide();

		});

		// Cancel edit
		$('.imports').on( 'click', '.cancel-edit', function(e) {
			var $line = $( this ).parents( '.ebac-import-line' );

			$line.find('input, select').prop( 'disabled', true );
			$line.find('.import-edit-actions').hide();
			$line.find('.column-edit').removeClass('expanded');
			$line.find('.edit-import').show();
		});

		// Save a booking
		$('.ebac-import-line').on( 'click', '.save-edit', function(e) {
			e.preventDefault();

			var $line = $( this ).parents( '.ebac-import-line' );
			
			var data = {
				action  : 'ebac_edit_imports',
				index   : $line.data( 'index' ),
				data    : $line.find( 'input, select' ).serialize()
			}

			// Block
			$('.imports-wrap').fadeTo('400', '0.6').block({
				message: null,
				overlayCSS: {
					background: 'transparent',
					backgroundSize: '16px 16px',
					opacity: 0.6
				}
			});
			
			// Ajax request
			$.post( ajax.ajax_url, data, function( response ) {

				// Unblock
				$('.imports-wrap').stop(true).css('opacity', '1').unblock();

				// Reload page
				var updateURL = location.href + ('&settings-updated=true');
				window.location = updateURL;
				
			});
		});

	});
})(jQuery);