(function($) {
	$(document).ready(function() {

		$('#easy_booking_availability_stock_init').on('click', function(e) {
			e.preventDefault();

			var $this = $(this),
				$response = $('.easy_booking_availability_response');

			var data = {
				action: 'easy_booking_availability_stock_init'
			};

			$this.fadeTo('400', '0.6');

			$response.html('');

			$.post( init.ajax_url, data, function( response ) {

				$response.html('✓');

				$this.stop(true).css('opacity', '1');
				$this.parents('.easy-booking-notice').hide();

			});

		});
		
	});
})(jQuery);