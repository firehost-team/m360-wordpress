
/*
$j(document).ready(function() {
    var outputval = '';
    var updateOutput = function(e) {
        var list   = e.length ? e : $j(e.target), output = list.data('output');
        if (window.JSON) {
            outputval = window.JSON.stringify(list.nestable('serialize'));
        } else {
            outputval = ('JSON browser support required for this demo.');
        }
        alert(outputval);
    };

    $j('#nestable3').nestable().on('change', updateOutput);
});
*/
var $j = jQuery.noConflict();
$j(document).ready(function() {
    if($j('#pck_order_to_edit'.length>0)){
        $j(".edit_product_id").click(function(){
            var new_product_id = prompt("Skriv inn product id");

            if (new_product_id == null || new_product_id == "") {
                alert('produkt id kan ikke ble tomt')
            } else {
                alert('ny produkt id er '+new_product_id);
            }
            /*
            $j.ajax(
                pckjs.ajaxurl, {
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'edit_product_id',
                    },
                    success: function (response) {
                        console.log( response );
                    },
                    error: function ( response ) {
                        console.log( response );
                    }
                }
            );
            */
        });
        /*
        $j('#pck_order_to_edit tbody tr').find('.edit_product_id').each(function() {
            var order_nr = $j("#pck_order_to_edit").attr("data-order_id");
            $j(this).editable({
                type: 'number',
                url: pckjs.ajaxurl,
                mode: 'inline',
                emptytext:'kan ikke ble tomt',
                params: function(params) {
                    //originally params contain pk, name and value
                    params.action = 'ajax_edit_product_id';
                    return params;
                },
                success: function(response, newValue) {
                    console.log('response: ',pckjs.ajaxurl);
                }
            });
        });
        */
    }

    // edit order item cliked
    $j(".pck_edit_item_action").click(function(){
        var order_nr = $j("#pck_order_to_edit").attr("data-order_id");
        var item_id = $j(this).attr("data-item_id");

        alert('order_id: '+order_nr+' item_id '+item_id);
    });

});
