<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
if(!class_exists('M360FIXActionPage')){
    class M360FIXActionPage{

        public function __construct() {

        }


        /**
         * @var string
         */
        protected $action = 'm360_fix_saleprice_background_process';
        protected $start_time = 0;


        /**
         * Handle
         *
         * Override this method to perform any actions required
         * during the async request.
         */
        protected function handle() {

            $args = array(
                'post_type' => 'product',
                'posts_per_page' => -1
            );
            $loop = new WP_Query( $args );
            if ( $loop->have_posts() ) {
                while ( $loop->have_posts() ) : $loop->the_post();
                    $product = wc_get_product($loop->post->ID);
                    $sale_price = $product->get_sale_price();
                    $price = $product->get_price();
                    if($sale_price >= $price){
                        $product->set_sale_price('');
                        $product->save();
                        //echo '<h3>sale price for product: '.$product->get_name().' has been removed</h3>';
                    }
                endwhile;
            } else {
                //WC_Admin_Notices::add_notice( 'No products found.' );
                return 'No products found.';
            }
            wp_reset_postdata();
            return 'Done';
        }




        public function removeSalePriceIfEqualPrice(){

            return $this->handle();
            //WC_Admin_Notices::add_notice( 'Done' );
        }


    }
}