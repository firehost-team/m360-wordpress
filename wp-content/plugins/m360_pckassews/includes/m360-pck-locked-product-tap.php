<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKLockedProductsTap extends M360_PCKasseWS_Settings{

    public function __construct(){

    }

    public function init_lockedProductsTap() {
        register_setting(
            'm360_pck_options_locked_products_group',
            'm360_pck_options_locked_products',
            array( $this, 'sanitise' )
        );

        add_settings_section(
            'm360_pck_locked_products_type_section', // id
            '==================== Låst varer mot vare type endring ====================', // title
            NULL,
            'm360_pck_options_page_locked_products' // page
        );

        add_settings_field(
            'm360_pck_locked_products_type_skus',
            'Skriv inn varer nr du vil låse mot endring',
            array($this,'m360_pck_locked_products_type_skus_html'),
            'm360_pck_options_page_locked_products',
            'm360_pck_locked_products_type_section'
        );

        add_settings_section(
            'm360_pck_locked_products_section', // id
            '==================== Låst varer mot alle endring ====================', // title
            NULL,
            'm360_pck_options_page_locked_products' // page
        );

        add_settings_field(
            'm360_pck_locked_products_skus',
            'Skriv inn varer nr du vil låse mot endring',
            array($this,'m360_pck_locked_products_skus_html'),
            'm360_pck_options_page_locked_products',
            'm360_pck_locked_products_section'
        );
    }

    public function m360_pck_locked_products_skus_html(){
        $lockedProductTabOptions = get_option( 'm360_pck_options_locked_products' );

        m360_pck_tooltip('19000',
            "her du kan kan skrive inn vare nr for varer du vil låse mot endring, seperarer de med komma",
            'm360_pck_locked_products_skus'
        );

        // select control start tag
        printf(
            '<textarea class="short wc_input_decimal" id="m360_pck_locked_products_skus" name="m360_pck_options_locked_products[m360_pck_locked_products_skus]">%s</textarea>',
            isset( $lockedProductTabOptions [ 'm360_pck_locked_products_skus' ] ) ?
                esc_attr( $lockedProductTabOptions [ 'm360_pck_locked_products_skus' ] ) : ''
        );
    }

    public function m360_pck_locked_products_type_skus_html(){
        $lockedProductTabOptions = get_option( 'm360_pck_options_locked_products' );

        m360_pck_tooltip('19001',
            "her du kan kan skrive inn vare nr for varer du vil låse mot endring, seperarer de med komma",
            'm360_pck_locked_products_skus'
        );

        // select control start tag
        printf(
            '<textarea class="short wc_input_decimal" id="m360_pck_locked_products_type_skus" name="m360_pck_options_locked_products[m360_pck_locked_products_type_skus]">%s</textarea>',
            isset( $lockedProductTabOptions [ 'm360_pck_locked_products_type_skus' ] ) ?
                esc_attr( $lockedProductTabOptions [ 'm360_pck_locked_products_type_skus' ] ) : ''
        );
    }


}
