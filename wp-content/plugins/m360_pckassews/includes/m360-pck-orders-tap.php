<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKOrdersTap extends M360_PCKasseWS_Settings{
	public function __construct(){
		
	}
	/**
	 * initialise PCK Orders page 
	 */
	public function init_orders() {
		
		// register Orders options group 
		register_setting(
            'm360_pck_options_orders_group', // group
            'm360_pck_options_orders', // name
            array( $this, 'sanitise' ) // sanitise method
        );
		
		// add Orders page 
		add_settings_section(
            'm360_pck_options_section_orders', // id
            'PCK Orders Options', // title
            array( $this, 'settings_html_m360_pck_orders_message' ), // callback
            'm360_pck_options_page_orders' // page
        );
	
		// Allowing sending orders to PCK
		add_settings_field(
            'm360_pck_allow_sending_order_to_pck', 
            'Allow Sending Orders To PCK', 
            array( $this, 'setting_html_m360_pck_allow_sending_orders_to_pck' ), 
            'm360_pck_options_page_orders', 
            'm360_pck_options_section_orders' 
        );

        //deliveredItemsAndCapturedPaymentInfo
        add_settings_field(
            'deliveredItemsAndCapturedPaymentInfo', // id
            'Warehous options', // title
            array( $this, 'deliveredItemsAndCapturedPaymentInfo_html' ), //callback
            'm360_pck_options_page_orders', // page
            'm360_pck_options_section_orders' // section
        );

		//PCK_ORDER_STATUS_ID
		add_settings_field(
            'm360_pck_order_status_ids', // id 
            'Order Status', // title
            array( $this, 'setting_html_m360_pck_order_status_id' ), //callback
            'm360_pck_options_page_orders', // page
            'm360_pck_options_section_orders'// section
        );
		// Extra costnad label
		add_settings_field(
            'm360_pck_order_extra_coast_label', 
            'Ekstra coast label to PCK', 
            array( $this, 'setting_html_m360_pck_extra_coast_label' ), 
            'm360_pck_options_page_orders', 
            'm360_pck_options_section_orders' 
        );
		
		
		// No orders error message
		add_settings_field(
            'm360_pck_show_no_orders_to_transfeer_message', 
            'Show no orders to transfeer message', 
            array( $this, 'setting_html_m360_pck_show_no_orders_to_transfeer_message' ), 
            'm360_pck_options_page_orders', 
            'm360_pck_options_section_orders' 
        );

        // No orders error message
        add_settings_field(
            'm360_pck_credit_order',
            'Payment method send as credit to pck',
            array( $this, 'setting_html_m360_pck_credit_order' ),
            'm360_pck_options_page_orders',
            'm360_pck_options_section_orders'
        );
        // No orders error message
        add_settings_field(
            'm360_pck_credit_order_status',
            'Credit order status',
            array( $this, 'setting_html_m360_pck_credit_order_status' ),
            'm360_pck_options_page_orders',
            'm360_pck_options_section_orders'
        );
        // No orders error message
        add_settings_field(
            'm360_pck_item_info_in_order_info',
            'Send oreder item info in order info',
            array( $this, 'setting_m360_pck_item_info_in_order_info' ),
            'm360_pck_options_page_orders',
            'm360_pck_options_section_orders'
        );

        if(class_exists('WCCF')){
            add_settings_section(
                'm360_pck_wccf_section', // id
                'Woocommerce custome fields', // title
                array( $this, 'm360_pck_wccf_section_html' ), // callback
                'm360_pck_options_page_orders' // page
            );
            if(post_type_exists('wccf_product_field')){
                add_settings_field(
                    'm360_pck_wccf_products',
                    'Product Fields',
                    array( $this, 'm360_pck_wccf_products_html' ),
                    'm360_pck_options_page_orders',
                    'm360_pck_wccf_section'
                );
            }
            if(post_type_exists('wccf_order_field')){
                add_settings_field(
                    'm360_pck_wccf_orders',
                    'Order Fields',
                    array( $this, 'm360_pck_wccf_orders_html' ),
                    'm360_pck_options_page_orders',
                    'm360_pck_wccf_section'
                );
            }

            if(post_type_exists('wccf_checkout_field')){
                add_settings_field(
                    'm360_pck_wccf_checkout',
                    'Checkout Fields',
                    array( $this, 'm360_pck_wccf_checkout_html' ),
                    'm360_pck_options_page_orders',
                    'm360_pck_wccf_section'
                );
            }
        }
				
	}
    // custome fields starts

    public function m360_pck_wccf_section_html(){
        print '==================================';
    }

    private function select_html_for_custom_fields($post_type,$setting,$empty_message){
        $args = array(
            'post_type'=> $post_type,
            'post_status' => 'publish'
        );
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );

        $posts = get_posts($args);
        $options = array();
        foreach ($posts as $post){
            $label = get_post_meta($post->ID,'label',true);
            $options[$post->ID] = $label;

        }
        if(count($options)>0){
            print '<select name="m360_pck_options_orders['.$setting.'][]" style="width: 150px;" multiple="multiple">';
            foreach( $options as $key => $value ) {
                printf(
                    '<option value="%s"%s>%s</option>',
                    $key,
                    ( in_array($key,(array)$ordersTabOptions[$setting])) ? ' selected' : '',
                    $value
                );
            }

            print '</select>';
        }else{
            print $empty_message;
        }
    }
	public function m360_pck_wccf_products_html(){
        $this->select_html_for_custom_fields('wccf_product_field','m360_pck_wccf_products',"<h3>Du har ingen aktiv custom fields for vare</h3>");
    }
    public function m360_pck_wccf_orders_html(){
        $this->select_html_for_custom_fields('wccf_order_field','m360_pck_wccf_orders',"<h3>Du har ingen aktiv custom fields for ordre</h3>");
    }

    public function m360_pck_wccf_checkout_html(){
        $this->select_html_for_custom_fields('wccf_checkout_field','m360_pck_wccf_checkout',"<h3>Du har ingen aktiv custom fields for checkout</h3>");
    }


    // custome fields ends
    public function setting_html_m360_pck_credit_order_status(){
        $values = array('completed'=>'Completed', 'processing'=>'Processing');
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $saved = isset($ordersTabOptions[ 'm360_pck_credit_order_status'] )?$ordersTabOptions[ 'm360_pck_credit_order_status']:'completed';


        m360_pck_tooltip('16870',
            "Choose the status for credit order",
            'm360_pck_credit_order_status'
        );

        print '<select name="m360_pck_options_orders[m360_pck_credit_order_status]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_m360_pck_item_info_in_order_info(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $saved = isset($ordersTabOptions[ 'm360_pck_item_info_in_order_info'] )?$ordersTabOptions[ 'm360_pck_item_info_in_order_info']:'no';

        m360_pck_tooltip('16370',
            "If yes, items info from order will send in order info",
            'm360_pck_item_info_in_order_info'
        );

        print '<select name="m360_pck_options_orders[m360_pck_item_info_in_order_info]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';

    }


	public function deliveredItemsAndCapturedPaymentInfo_html(){
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $selected = isset($ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ] )?$ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ]:'normal';

        $values = array('normal'=>'Normal','use_warehouses'=>'Use warehouses');

        m360_pck_tooltip('996030',
            "If set, Pckasse will assume that the items are allready processed and flag them as delivered and forward the sale to accounting and statistics.",
            'deliveredItemsAndCapturedPaymentInfo'
        );

        // select control start tag
        print '<select  name="m360_pck_options_orders[deliveredItemsAndCapturedPaymentInfo]" style="width: 150px;">';


        foreach( $values as $key => $value ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ( $selected == $key ) ? ' selected' : '',
                $value
            );
        }

        // select control close tag
        print '</select>';
        print('<br /><sub>if this set to {Use warehouses} order status will be ignored and woocommerce will send just orders with completed status and in pckasse orders will automatic go to fullførte webordre</sub>');
    }
    public function setting_html_m360_pck_credit_order(){
        global $woocommerce;
        $payments = $woocommerce->payment_gateways->get_available_payment_gateways();
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );

        m360_pck_tooltip('6032',
            "If you want to send a payment method as a credit order to pckasse",
            'm360_pck_credit_order'
        );

        print '<select  name="m360_pck_options_orders[m360_pck_credit_order]" style="width: 150px;">';
        printf(
            '<option value="0"%s>Dont use credit</option>',
            ($ordersTabOptions[ 'm360_pck_allow_sending_order_to_pck' ] == 0  || !isset($ordersTabOptions[ 'm360_pck_credit_order' ] ))? ' selected' : '');
        foreach ($payments as $payment) {
            printf(
                '<option value="%s"%s>%s</option>',
                $payment->id,
                ( $ordersTabOptions[ 'm360_pck_credit_order' ] == $payment->id) ? ' selected' : '',
                $payment->get_title()
            );
        }


        print '</select>';

    }

    /**
	 * HTML to display for the Orders page
	 */
	public function settings_html_m360_pck_orders_message() {
		print 'These settings will be used when Orders are automatically raised in PCK...';
	}
	
	/**
	 * HTML to display for Allowing sending orders to PCK
	 */
	 public function setting_html_m360_pck_allow_sending_orders_to_pck() {
		$ordersTabOptions = get_option( 'm360_pck_options_orders' );
		$values = array('send'=>'Send','dont_send'=>'Don\'t send');
		
		m360_pck_tooltip('6030',
				"Choose if WooCommerce Orders will be sent to PCK or not", 
				'm360_pck_allow_sending_orders'
			);
			
			// select control start tag
			print '<select  name="m360_pck_options_orders[m360_pck_allow_sending_order_to_pck]" style="width: 150px;">';	

			
			foreach( $values as $key => $value ) {
				printf(
					'<option value="%s"%s>%s</option>', 
					$key, 
					( $ordersTabOptions[ 'm360_pck_allow_sending_order_to_pck' ] == $key || !isset($ordersTabOptions[ 'm360_pck_allow_sending_order_to_pck' ] )) ? ' selected' : '', 
					$value
				);					
			}
			
			// select control close tag
			print '</select>';		
	}


	/**
	 * HTML to display the PCK_ORDER_STATUS_ID config option
	 */
	public function setting_html_m360_pck_order_status_id() {
		
		m360_pck_tooltip('6030',
			"WooCommerce Orders will have this Order Status when they are raised in PCK", 
			'm360_pckorderstatus'
		);
		
		// select control start tag 
		print '<select name="m360_pck_options_orders[m360_pck_order_status_ids][]" style="width: 150px;" multiple="multiple">';
		$order_status = wc_get_order_statuses();
		//WriteLog(print_r($order_status,true));
		$ordersTabOptions = get_option( 'm360_pck_options_orders' );
		
		// print each select option
		foreach( $order_status as $key => $value ) {
			// print option, mark as selected if needed
			printf(
				'<option value="%s"%s>%s</option>', 
				$key, 
				( in_array($key,(array)$ordersTabOptions['m360_pck_order_status_ids'])) ? ' selected' : '', 
				$value
			);
			
		}
		
		// select control close tag
		print '</select>';
		
	}
	
	public function setting_html_m360_pck_extra_coast_label(){
		$extraCoastLabel = get_option( 'm360_pck_options_orders' );		
		
		m360_pck_tooltip('6080',
				"Set the label to be used as extra coast label", 
				'm360_pck_order_extra_coast_label'
			);
			
			printf(
			'<input type="text" id="m360_pck_order_extra_coast_label" name="m360_pck_options_orders[m360_pck_order_extra_coast_label]" style="width: 250px;" value="%s" />',
			isset( $extraCoastLabel [ 'm360_pck_order_extra_coast_label' ] ) ? 
				esc_attr( $extraCoastLabel [ 'm360_pck_order_extra_coast_label' ] ) : 'Gebyr'
		);
	}

	public function setting_html_m360_pck_show_no_orders_to_transfeer_message(){

		$ordersTabOptions = get_option( 'm360_pck_options_orders' );
		$values = array('show'=>'Show','dont_show'=>'Don\'t Show');
		
		m360_pck_tooltip('6030',
				"Choose if you want to show message if there is no orders to transfeer from Woocommerce", 
				'm360_pck_show_no_orders_message'
			);
			
			// select control start tag
			print '<select  name="m360_pck_options_orders[m360_pck_show_no_orders_to_transfeer_message]" style="width: 150px;">';	

			
			foreach( $values as $key => $value ) {
				printf(
					'<option value="%s"%s>%s</option>', 
					$key, 
					( $ordersTabOptions[ 'm360_pck_show_no_orders_to_transfeer_message' ] == $key || !isset($ordersTabOptions[ 'm360_pck_show_no_orders_to_transfeer_message' ] )) ? ' selected' : '', 
					$value
				);					
			}
			
			// select control close tag
			print '</select>';
	}
	

}