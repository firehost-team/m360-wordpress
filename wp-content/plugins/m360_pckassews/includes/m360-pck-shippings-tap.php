<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKShippingsTap extends M360_PCKasseWS_Settings{

	public function __construct(){
		
	}
		public function init_shipping() {
		
		// register Shipping options group 
		register_setting(
            'm360_pck_options_shipping_group', // group
            'm360_pck_options_shipping', // name
            array( $this, 'sanitise' ) // sanitise method
        );
		
		// add Shipping page 
		add_settings_section(
            'm360_pck_options_section_shipping', // id
            'PCK Shipping Options', // title
            array( $this, 'settings_html_m360_pck_shipping_message' ), // callback
            'm360_pck_options_page_shipping' // page
        );
		
		add_settings_field(
            'm360_pck_shipping_method', 
            'Shipping Methods', 
            array( $this, 'setting_html_m360_pck_shipping' ), 
            'm360_pck_options_page_shipping', 
            'm360_pck_options_section_shipping' 
        );
	}
	
	/** 
     * HTML to display for the Shipping section
     */
    public function settings_html_m360_pck_shipping_message() {
		
        print 'These settings will be used to link PCK products to WooCommerce shipping options...';
		
    }
	
	/**
	 * HTML to display the EASIFY_SHIPPING_MAPPING config option
	 */
	public function setting_html_m360_pck_shipping() {
	m360_pck_tooltip('6050',
			"WooCommerce Orders will have this Shipment Method when they are raised in PCK", 
			'm360_pckshipmentmethod'
		);
		
		// select control start tag
		print '<select  name="m360_pck_options_shipping[m360_pck_shipping_method]" style="width: 150px;">';
		$shipping_methods = WC()->shipping->load_shipping_methods();
		$shippingTapOptions = get_option( 'm360_pck_options_shipping' );
        //WriteLog(print_r($shippingTapOptions,true));
		foreach( $shipping_methods as $method ) {
			if($method->enabled){
				printf(
				'<option value="%s"%s>%s</option>', 
				$method->id, 
				( $shippingTapOptions[ 'm360_pck_shipping_method' ] == $method->id ) || 
					 !isset($shippingTapOptions[ 'm360_pck_shipping_method' ] ) ? ' selected' : '', 
				$method->title
			);
			}					
		}
		
		// select control close tag
		print '</select>';
		
	}

	
}
?>