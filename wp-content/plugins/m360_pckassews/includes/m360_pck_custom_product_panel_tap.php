<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKCustomProductPanelTap{

	public static function DrawHtml(){
		global $post,$woocommerce;
        $product = wc_get_product( $post );
		?>
        <div id="m360_pck" class="panel woocommerce_options_panel">
            <div class="options_group">
                <?php
                PCKArticleID($product->get_id());
                PCKManufacturer($product->get_id());
                PCKHideWhenOutOfStock($product->get_id());
                PCKArticleStatus($product->get_id());
                PCKExternalLink($product->get_id());
                PCKSubtitle($product->get_id());
                PCKCostPrice($product->get_id());
                PCKAlternativePrice($product->get_id());
                PCKAlternativePrice2($product->get_id());
                PCKArticleWebAction($product->get_id());
                PCKShippingType($product->get_id());
                PCKConfirmedDelivery($product->get_id());
                PCKExpectedDeliveryAmount($product->get_id());
                PCKExpectedDeliveryDate($product->get_id());
                PCKExternalGroupId($product->get_id());
                PCKNoDiscount($product->get_id());
                PCKNonStockItem($product->get_id());
                PCKNonStockItemDays($product->get_id());

                ?>
            </div>
        </div>
		
		<?php
	}
}