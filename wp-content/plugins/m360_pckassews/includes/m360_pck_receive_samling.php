<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_service.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_receive.php' );

class TmpProduct{
    public $hideWhenOutOfStock;
    public $stockCcount;
    public $salesPrice;
    public $discount;
    public $discountFrom;
    public $discountTo;
    public $articleId;
}

class M360PCKReceiverSamlin{

    private static function calculate_multi_pckasser_count_samling($ProductId,$count){
        global $current_PCK;
        $kasse_nr = M360PCKReceiver::retriveKasseNr();

        update_post_meta($ProductId, 'm360_stock_for_pck_'.$kasse_nr, $count);
        update_post_meta($ProductId, 'm360_location_for_pck_'.$kasse_nr, $current_PCK->PCKLocation);

        $current_pck_stock = 0;

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $numberOfPCKasser = isset( $setupTabOptions[ 'm360_number_of_pckasser' ] ) ? $setupTabOptions[ 'm360_number_of_pckasser' ]  : 1;

        for($i = 0; $i<$numberOfPCKasser ; $i++){
            $current_pck_stock += get_post_meta( $ProductId, 'm360_stock_for_pck_'.$i, true );
        }
        return $current_pck_stock;
    }

    private static function samlingCreateAttribute($parent_id,$colors=NULL,$sizes=NULL){
        //WriteLog('createAttribute in samling '.$parent_id.' colors: '.print_r($colors,true).' sizes: '.print_r($sizes,true));

        foreach($colors as $color){
            if(strlen($color)<=0)unset($color,$colors);
        }
        foreach($sizes as $size){
            if(strlen($size)<=0)unset($size,$sizes);
        }
        if(count($colors)<=0)$colors = NULL;
        if(count($sizes)<=0)$sizes = NULL;

        $color_attribute = M360PCKReceiver::colorAttribute();
        $size_attribute = M360PCKReceiver::sizeAttribute();

        if ( function_exists( 'wc_bv_update_post_meta' ) ) {
            wc_bv_update_post_meta( $parent_id, '_bv_type', 'matrix' );
        }

        if(count($colors) && $color_attribute != 'no_attribute'){
            wp_set_object_terms($parent_id, $colors, $color_attribute,true);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $parent_id, '_bv_x', $color_attribute );
            }
        }

        if(count($sizes) && $size_attribute != 'no_attribute'){
            wp_set_object_terms($parent_id, $sizes, $size_attribute,true);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $parent_id, '_bv_y', $size_attribute );
            }
        }


        //$attributes 	= get_post_meta( $ProductId, '_product_attributes', true );
        $attributes = array();
        if(count($colors) || count($sizes)){
            if(count($colors) && $color_attribute != 'no_attribute'){
                $attributes[$color_attribute] = array(
                    'name' => $color_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 1
                );
            }

            if(count($sizes) && $size_attribute != 'no_attribute'){
                $attributes[$size_attribute] = array(
                    'name' => $size_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 2
                );

            }
            update_post_meta($parent_id, '_product_attributes', $attributes);
        }
    }

    public static function getSamlingChilds($product_info){
        $digits_to_search_opt = getProductOption('m360_pck_samling_number_of_digits',4);
        //WriteLog('digits_to_search_opt: '.$digits_to_search_opt);
        $digits_to_search = substr($product_info->articleNo, 0, $digits_to_search_opt);
        //WriteLog('digits_to_search: '.$digits_to_search);

        global $wpdb;
        $query = "SELECT * FROM ".$wpdb->postmeta." WHERE meta_key LIKE '_sku' AND meta_value LIKE '%".$digits_to_search."%'";
        $childs = $wpdb->get_results($query,ARRAY_A);
        return $childs;
    }

    public static function updteParent($parent_id,$product_info){
        if(!$product_info->visibleOnWeb){
            $my_post = array();
            $my_post['ID'] = $parent_id;
            $my_post['post_status'] = 'draft';
        }else{
            $my_post = array();
            $my_post['ID'] = $parent_id;
            $my_post['post_status'] = 'publish';
        }

        wp_update_post( $my_post );

        M360PCKReceiver::updatePostMetaForProduct($parent_id,$product_info);
        M360PCKReceiver::addManfacturer($parent_id,$product_info);
        M360PCKReceiver::updateProductDescription($parent_id,$product_info);

        //self::updateImageAlt($ProductId);
        wc_delete_product_transients( $parent_id );
        return true;
    }

    private static function updateStockSmalingParent($product_id,$count,$product_info = false){
        $current_pck_stock = self::calculate_multi_pckasser_count_samling($product_id,$count);
        $backorder = M360PCKReceiver::calculateBackorder($product_id,$product_info);
        $instock = M360PCKReceiver::calculateInStock($backorder,$current_pck_stock);

        $manageStock = 'yes';
        if($current_pck_stock<=0 && $backorder == 'yes'){
            $manageStock = 'no';
        }
        //mail('ibrahim@m360.no',__FUNCTION__,$product_id.' '.$manageStock);
        $wc_product = wc_get_product($product_id);
        if($wc_product){
            $wc_product->set_manage_stock($manageStock);
            $wc_product->set_backorders($backorder);
            $wc_product->set_stock_quantity($current_pck_stock);
            $wc_product->set_stock_status(wc_clean($instock));
            $wc_product->save();

        }
        /*
        update_post_meta($product_id, '_manage_stock', $manageStock);
        update_post_meta($product_id, '_stock', $current_pck_stock);
        update_post_meta($product_id, '_stock_status', wc_clean($instock));
        update_post_meta($product_id, '_backorders',$backorder);
        */
    }

    public static function find_parent_and_create_it_if_not_exists($product_info){
        //WriteLog(__FUNCTION__.' '.print_r($product_info,true));
        $cloned_product_info = $product_info;
        //$childs = self::getSamlingChilds($product_info);
        //WriteLog('CHILDS: '.print_r($childs,true));

        $digits_to_search_opt = getProductOption('m360_pck_samling_number_of_digits',4);
        $digits_to_search = substr($product_info->articleNo, 0, $digits_to_search_opt);

        $compononts = getSamlingAttributesArray();
        $name_key = $compononts['name'];

        $title_component = explode('-',$product_info->name);
        $name = (count($title_component)>=($name_key+1))?$title_component[$name_key]:NULL;
        //WriteLog(__FUNCTION__.' name: '.$name);
        global $current_PCK;
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        $newArticleId = $digits_to_search.'_'.getInitialsFromName($name);
        $parent_id = SKUTOPOSTID('s_'.$newArticleId);//getPostIdFromParentsIdTbl($newArticleId,$kasse_nr);
        //WriteLog(__FUNCTION__.' newArticleID: '.$newArticleId);

        //WriteLog('Name: '.$name);
        //$parent_id = 0;

        /*
        foreach($childs as $child){
            $child_product = wc_get_product($child['post_id']);
            $sku = $child_product->get_sku();
            WriteLog('THE SKU: '.$sku);
            if($sku == 's_'.slugify($child['post_title'])){
                $parent_id = $child['post_id'];
                break;
            }
        }
        */


        $cloned_product_info->articleNo = 's_'.$newArticleId;
        $cloned_product_info->articleId = $newArticleId;
        $cloned_product_info->name = $name;
        $cloned_product_info->sizeColorInUse = false;
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        if($parent_id <= 0){
            $parent_id = M360PCKReceiver::InsertProduct($cloned_product_info);
            if($parent_id){
                //UpdateParentId_samling($parent_id,$cloned_product_info->articleId,$kasse_nr);
                wp_set_object_terms($parent_id, 'variable', 'product_type');
            }else{
                return 'FAILD in find_parent_and_create_it_if_not_exists';
            }
        }else{
            if(!self::updteParent($parent_id,$cloned_product_info)){
                return 'FAILD Update in find_parent_and_create_it_if_not_exists';
            }
        }
        if($parent_id)self::updateStockSmalingParent($parent_id,$product_info->stockCcount);
        return $parent_id;
    }

    public static function checkPostStatus($post_id){
        $post = get_post($post_id);
        if($post){
            $status = get_post_status($post_id);
            if($status != 'publish'){
                wp_publish_post( $post_id );
            }

        }
    }
    public static function updateVariant($variant_id,$product_info,$parent_id){
        //WriteLog(__FUNCTION__.': '.$variant_id);
        self::checkPostStatus($variant_id);
        $compononts = getSamlingAttributesArray();
        $color_key = $compononts['color'];
        $size_key = $compononts['size'];

        $title_component = explode('-',$product_info->name);
        $color = (count($title_component)>=($color_key+1))?trim($title_component[$color_key]):NULL;
        $size = (count($title_component)>=($size_key+1))? trim($title_component[$size_key]):NULL;

        if(!$color ||strlen($color)<= 0)$color = 'unicolor';
        if(!$size ||strlen($size)<= 0)$size = 'unisize';

        self::samlingCreateAttribute($parent_id,array($color),array($size));

        $color_attribute = M360PCKReceiver::colorAttribute();
        $size_attribute = M360PCKReceiver::sizeAttribute();

        if(!empty($product_info->eans) && $product_info->eans > 0){
            update_post_meta($variant_id, 'm360_pck_eans', $product_info->eans);
        }
        update_post_meta($variant_id, 'm360_pck_hideWhenOutOfStock', $product_info->hideWhenOutOfStock);
        $current_pck_stock = self::calculate_multi_pckasser_count_samling($variant_id,$product_info->stockCcount);

        $backorder = M360PCKReceiver::calculateBackorder($variant_id,$product_info);
        $instock = M360PCKReceiver::calculateInStock($backorder,$current_pck_stock);

        if($color && ctype_space($color)){
            $color = NULL;
            //WriteLog('Color is just white space');
        }
        if($size && ctype_space($size)){
            $size = NULL;
            //WriteLog('size is just white space');
        }

        if($color && strlen($color)>0 && strpos($color, M360PCKReceiver::getNoColorText()) === false && $color_attribute != 'no_attribute'){
            $farge_attribute_term = get_term_by('name', $color, $color_attribute);

            if(is_object($farge_attribute_term) && property_exists($farge_attribute_term,'slug') && $farge_attribute_term->slug){
                update_post_meta($variant_id, 'attribute_'.$color_attribute, $farge_attribute_term->slug);
            }
        }
        if($size && strlen($size)>0 && strpos($size, M360PCKReceiver::getNoSizeText()) === false && $size_attribute != 'no_attribute'){
            $storrelse_attribute_term = get_term_by('name', $size, $size_attribute);
            if(is_object($storrelse_attribute_term) && property_exists($storrelse_attribute_term,'slug') && $storrelse_attribute_term->slug){
                update_post_meta($variant_id, 'attribute_'.$size_attribute, $storrelse_attribute_term->slug);
            }
        }


        $woocommerce_synk_pris_checkbox = empty(get_post_meta( $parent_id, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $parent_id, '_m360_checkbox_synk_pris', true );
        if($woocommerce_synk_pris_checkbox == 'yes') {
            M360PCKReceiver::pricing($variant_id, $product_info);
        }

        $manageStock = 'yes';
        if($current_pck_stock<=0 && $backorder == 'yes'){
            $manageStock = 'no';
        }
        $wc_product = wc_get_product($variant_id);
        if($wc_product){
            //$wc_product->set_sku($post_name);
            update_post_meta($variant_id, '_sku', $product_info->articleNo);
            $wc_product->set_virtual('no');
            $wc_product->set_downloadable('no');
            $wc_product->set_manage_stock($manageStock);
            $wc_product->set_stock_quantity($current_pck_stock);
            $wc_product->set_stock_status(wc_clean($instock));
            $wc_product->set_backorders($backorder);
            $wc_product->save();

        }

        $result = '';
        return $result;
    }

    public static function createVariationAndAddItToParent($product_info,$parent_id){
        global $current_PCK;
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        $my_post = array(
            'post_title' => $product_info->name,
            'post_name' => $product_info->name,
            'post_status' => 'publish',
            'post_parent' => $parent_id,
            'post_type' => 'product_variation',
            'guid' => home_url() . '/?product_variation=' . $product_info->name
        );
        $variant_id = getVariationIdFromSizeColorId($product_info->articleId,$kasse_nr);
        if(!$variant_id || $variant_id <= 0){
            $variant_id = SKUTOPOSTID( $product_info->articleNo );
            if($variant_id && $variant_id>0){
                $n_sizeColorId = get_post_meta( $variant_id, '_sizeColorId_'.$kasse_nr, true );
                UpdateSizeColorID($variant_id,$n_sizeColorId,$kasse_nr);
            }else{
                $variant_id = wp_insert_post($my_post);
                //global $current_PCK;
                update_post_meta( $variant_id, '_sizeColorId_'.$kasse_nr, $product_info->articleId );
                UpdateSizeColorID($variant_id,$product_info->articleId,$kasse_nr);
            }

        }
        return self::updateVariant($variant_id,$product_info,$parent_id);
    }

    public static function product_has_title_compononts($product_info){
        $has_title_compononts = true;
        $title_component = explode('-',$product_info->name);
        if(count($title_component)<=1)$has_title_compononts = false;
        return $has_title_compononts;
    }
    /*
    public static function SamlingSendArticle($product_info){
        //WriteLog('SamlingSendArticle: '.print_r($product_info,true));
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $allways_config = 'no';
        if(isset($productTabOptions[ 'setting_html_m360_pck_samling_allways_config'] )){
            $allways_config = $productTabOptions[ 'setting_html_m360_pck_samling_allways_config'];
        }
        if(!self::product_has_title_compononts($product_info) && !$allways_config)return 'continue_creating';
        $parent_id = self::find_parent_and_create_it_if_not_exists(clone $product_info);
        global $current_PCK;
        $variant_id = getVariationIdFromSizeColorId($product_info->articleId,$kasse_nr);
        if(!$variant_id || $variant_id<=0){
            $variant_id = SKUTOPOSTID( $product_info->articleNo );
            $n_sizeColorId = get_post_meta( $variant_id, '_sizeColorId_'.$kasse_nr, true );
            UpdateSizeColorID($post_id,$n_sizeColorId,$kasse_nr);
        }

        if($variant_id<=0){
            return self::createVariationAndAddItToParent($product_info,$parent_id);
        }else {
            $wc_variant_product = wc_get_product($variant_id);
            if(!$wc_variant_product || !$wc_variant_product->get_id() || $wc_variant_product->get_parent_id()!= $parent_id){
                return self::createVariationAndAddItToParent($product_info,$parent_id);
            }
            $child_parent_id = wp_get_post_parent_id($variant_id);
            if($child_parent_id != $parent_id) {
                //WriteLog('No parent found for Variant: ' . $variant_id);
                wp_update_post(
                    array(
                        'ID' => $variant_id,
                        'post_parent' => $parent_id
                    )
                );
            }
            //WriteLog('the parent id for : ' . $variant_id.' is: '.$parent_id);
            UpdateSizeColorID($variant_id,$product_info->articleId,$kasse_nr);
            return self::updateVariant($variant_id,$product_info,$parent_id);
        }

    }
    */
    public static function SamlingSendArticle($product_info){
        //WriteLog('SamlingSendArticle: '.print_r($product_info,true));
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $allways_config = 'no';
        if(isset($productTabOptions[ 'setting_html_m360_pck_samling_allways_config'] )){
            $allways_config = $productTabOptions[ 'setting_html_m360_pck_samling_allways_config'];
        }
        if(!self::product_has_title_compononts($product_info) && !$allways_config)return 'continue_creating';
        $parent_id = self::find_parent_and_create_it_if_not_exists(clone $product_info);
        global $current_PCK;
        $variant_id = getVariationIdFromSizeColorId($product_info->articleId,$kasse_nr);
        //$variant_id = wc_get_product_id_by_sku($product_info->articleNo);

        $create_anew = false;
        $temp = wc_get_product($variant_id);
        if($temp){
            $temp_parent = $temp->get_parent_id();
            //WriteLog('TMP Parent ID: '.$temp_parent.' GG: '.$parent_id);
            if($temp_parent != $parent_id){
                wp_delete_post($variant_id,true);
                deleteSizeColorIdFromDb($variant_id,$kasse_nr);
                $create_anew = true;
                $variant_id = 0;
            }
        }
        if(!$create_anew && (!$variant_id || $variant_id<=0)){
            $variant_id = SKUTOPOSTID( $product_info->articleNo );
            $n_sizeColorId = get_post_meta( $variant_id, '_sizeColorId_'.$kasse_nr, true );
            UpdateSizeColorID($variant_id,$n_sizeColorId,$kasse_nr);
        }

        if($variant_id<=0){
            WriteLog('Create a new');
            return self::createVariationAndAddItToParent($product_info,$parent_id);
        }else {
            $wc_variant_product = wc_get_product($variant_id);
            if(!$wc_variant_product || !$wc_variant_product->get_id() || $wc_variant_product->get_parent_id()!= $parent_id){
                return self::createVariationAndAddItToParent($product_info,$parent_id);
            }
            $child_parent_id = wp_get_post_parent_id($variant_id);
            if($child_parent_id != $parent_id) {
                //WriteLog('No parent found for Variant: ' . $variant_id);
                wp_update_post(
                    array(
                        'ID' => $variant_id,
                        'post_parent' => $parent_id
                    )
                );
            }
            //WriteLog('the parent id for : ' . $variant_id.' is: '.$parent_id);
            UpdateSizeColorID($variant_id,$product_info->articleId,$kasse_nr);
            return self::updateVariant($variant_id,$product_info,$parent_id);
        }

    }

    public static function SamlingUpdateProductInfo($articleId,$Picture,$colorid=false){
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
        //WriteLog('ARTICLEID: '.$articleId);
        //WriteLog('DATA'.print_r($data,true));
        global $wpdb;
        global $current_PCK;

        //WriteLog(__FUNCTION__.' articleId: '.$articleId);

        $current_product_id = getVariationIdFromSizeColorId($articleId,$kasse_nr);
        //$current_product = wc_get_product($current_product_id);
        $parent_id = wp_get_post_parent_id($current_product_id);
        if($parent_id){
            WriteLog(__FUNCTION__.' parent_id: '.$parent_id);
            return M360PCKReceiver::UpdateProductInfo($parent_id, $Picture,true);
        }else{
            WriteLog(__FUNCTION__.'No Paren Id');
            return true;
        }
        /*
        if($current_product && $current_product->is_type( 'simple' )){
            if(!has_post_thumbnail( $parent_id )) {
                return M360PCKReceiver::UpdateProductInfo($parent_id, $Picture,true);
            }else{
                return M360PCKReceiver::UpdateProductInfo($current_product_id,$Picture,true);
            }
        }else if($parent_id>0 && !has_post_thumbnail( $parent_id )){
            return M360PCKReceiver::UpdateProductInfo($parent_id,$Picture,true);
        }else{
            return M360PCKReceiver::UpdateExtraImages($parent_id,$articleId, $Picture,true,$colorid);
        }
        */

    }

    public static function SamlingUpdateProductStockLevel($updateStock){
        //WriteLog('SamlingUpdateProductStockLevel');
        //global $wpdb;
        //global $current_PCK;
        $kasse_nr = M360PCKReceiver::retriveKasseNr();
/*
        $qur = "SELECT post_id FROM " . $wpdb->postmeta . " WHERE meta_key = '_sizeColorId_".$kasse_nr."' AND meta_value = ".$updateStock->articleId." LIMIT 1";
        WriteLog(__FUNCTION__.' '.$qur);

        $ProductId = $wpdb->get_var($wpdb->prepare($qur));
*/
        $ProductId = getVariationIdFromSizeColorId($updateStock->articleId,$kasse_nr);


        //WriteLog('Produckt id in samling updateStock: '.$ProductId);
        if($ProductId <= 0){
            return 'We can not create The new size';
        }else{
            $qty = self::calculate_multi_pckasser_count_samling($ProductId,$updateStock->count);

            $backorder = M360PCKReceiver::calculateBackorder($ProductId);
            $instock = M360PCKReceiver::calculateInStock($backorder,$qty);
            $manageStock = 'yes';
            if($qty<=0 && $backorder == 'yes'){
                $manageStock = 'no';
            }
            //WriteLog('UpdateProductStockLevel ' .$instock);
            try{
                $wc_product = wc_get_product($ProductId);
                if($wc_product){
                    $wc_product->set_stock_quantity($qty);
                    $wc_product->set_backorders($backorder);
                    $wc_product->set_stock_status(wc_clean($instock));
                    $wc_product->set_manage_stock($manageStock);
                    $wc_product->save();
                }
                /*
                update_post_meta($ProductId, '_stock', $qty);
                update_post_meta($ProductId, '_backorders',$backorder);
                update_post_meta($ProductId, '_stock_status', wc_clean($instock));
                update_post_meta($ProductId, '_manage_stock', $manageStock);
                */
                return true;
            }catch(Exception $ex){
                return 'UpdateProductStockLevel '.$ex->getMessage();
            }
        }
    }

}

