<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
if( ! function_exists( 'is_option_set' ) ) {
    function is_option_set( $option, $key ) {
        if ( !isset( $option[$key] ) || empty( $option[$key] ) ) return false;
        return true;
    }
}

if( ! function_exists( 'is_multipck_option_set' ) ) {
    function is_multipck_option_set( $option, $key1,$key2 ) {
        if ( !isset( $option[$key1][$key2] ) || empty( $option[$key1][$key2] ) ) return false;
        return true;
    }
}

if( ! function_exists('m360_pck_tooltip') ) {
    function m360_pck_tooltip($help_id, $tip, $anchor = '' ) {

        // prints a help icon that links to m360_pck support articles and shows a tooltip when hovered over
        printf(
            '<a href="http://www.m360.no/pck/help/hs%s.aspx%s" target="_blank">
			<img class="m360_pck_tip" title="%s" src="' . plugin_dir_url( __FILE__ ) . '../assets/images/help.png" 
				style="height: 16px; width: 16px;"/></a>',
            $help_id, // m360_pck help article id
            !empty($anchor) ? '#' . $anchor : '',
            $tip
        );

    }
}

if( ! function_exists( 'woocommerce_not_installed_notice' ) ) {
    function woocommerce_not_installed_notice(){
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if(!is_plugin_active( 'woocommerce/woocommerce.php')){
            echo '<div class="error notice"><p>M360 PCKasseWS installed but woocommerce is missing</p></div>';
        }
    }
}
add_action('admin_notices', 'woocommerce_not_installed_notice');

if( ! function_exists( 'DebugTrace' ) ) {
    function DebugTrace(){
        // write current stack trace to debug log file
        try {
            $loggingOptions = get_option('m360_pck_options_logging');
            if ($loggingOptions['m360_pck_logging_debugging'] != 'true') return;

            ob_start();
            debug_print_backtrace();
            $Debug = ob_get_contents();
            ob_end_clean();

            // write to log file in the following format: 17-12-2012 10:15:10:000000 - $text \n
            $File = fopen(dirname(dirname(__FILE__)) . '/logs/m360_pck_debug.log', 'a');
            fwrite($File, date('d-m-y H:i:s') . substr((string)microtime(), 1, 6) . "\n" . $Debug . "\n\n");
            fclose($File);
        } catch (Exception $e) {
            //WriteLog('Receive DebugTrace Exception: ' . $e->getMessage() . "\n");
        }
    }
}
if(!function_exists('savePCKasseSession')){
    function savePCKasseSession(){
        $blog_id = get_current_blog_id();

        file_put_contents(WP_CONTENT_DIR.'/pck_session_'.$blog_id,'');
    }
}

if(!function_exists('getTimeForPCKasseSession')){
    function getTimeForPCKasseSession(){
        $blog_id = get_current_blog_id();
        $filename = WP_CONTENT_DIR.'/pck_session_'.$blog_id;
        if (file_exists($filename)) {
            return filemtime($filename);
        }
        return 0;
    }
}


if( ! function_exists( 'WriteLog' ) ) {
    function WriteLog($Text,$to_email = false){
        $loggingOptions = get_option('m360_pck_options_logging');
        if ($loggingOptions['m360_pck_logging_enabled'] != 'true') return;

        // write to log file in the following format: 17-12-2012 10:15:10:000000 - $Text \n
        $File = fopen(WP_CONTENT_DIR . '/m360_pck_log.log', 'a');

        fwrite($File, date('d-m-y H:i:s') . substr((string)microtime(), 1, 6) . ' - ' . $Text . "\n");
        fclose($File);
        if ($to_email){
            mail('ibrahim@m360.no','firehost_pck_debug',$Text);
        }
    }
}

if( ! function_exists( 'HandleException' ) ) {
    function HandleException($Text){
        throw new Exception($Text);
    }
}

if( ! function_exists( 'CreateSlug' ) ) {
    function CreateSlug($Name){
        // trim white spaces at beginning and end of alias and make lowercase
        $String = trim(strtolower($Name));

        // remove any duplicate whitespace, and ensure all characters are alphanumeric
        $String = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', $String);

        // trim dashes at beginning and end of alias
        $String = trim($String, '-');

        // if we are left with an empty string, make a date with random number
        if (trim(str_replace('-', '', $String)) == '') {
            $String = date("Y-m-d-h-i-s") . mt_rand();
        }

        return $String;
    }
}
if( ! function_exists( 'CancelTheOrder' ) ) {
    function CancelTheOrder($WC_order){
        try{//Possible values: processing, on-hold, cancelled, completed
            $WC_order->update_status('cancelled');
            return true;
        }catch(Exception $ex){
            //WriteLog("Deleting order Exception: " . $ex->getMessage() . "\n");
            return false;
        }
    }
}


// DB
// order status

if( ! function_exists( 'addnettbutikkIdKolToOrderStatusIfNotExists' ) ) {
    function addnettbutikkIdKolToOrderStatusIfNotExists($table_name){
        global $wpdb;
        $rowQur = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table_name."' AND column_name = 'nettbutikk_id'"  ;

        $row = $wpdb->get_results($rowQur);
        if(empty($row)){
            $alterQur = "ALTER TABLE ".$table_name." ADD nettbutikk_id INT(11) NOT NULL DEFAULT 0";
            $wpdb->query($alterQur);
        }

    }
}

if( ! function_exists( 'add_total_submitted_col_to_orderstatus_table' ) ) {
    function add_total_submitted_col_to_orderstatus_table(){
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        global $wpdb;
        $rowQur = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table_name."' AND column_name = 'total_submitted'"  ;

        $row = $wpdb->get_results($rowQur);
        if(empty($row)){
            $alterQur = "ALTER TABLE ".$table_name." ADD total_submitted INT(11) NOT NULL DEFAULT 0";
            $wpdb->query($alterQur);
        }

    }
}

if( ! function_exists( 'get_total_submitted' ) ) {
    function get_total_submitted($order_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");
        return  $wpdb->get_var("SELECT total_submitted FROM $table_name WHERE order_id LIKE $order_id");
    }
}
if( ! function_exists( 'addAskForCreditIfNotExist' ) ) {
    function addAskForCreditIfNotExist($table_name){
        global $wpdb;
        $rowQur = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table_name."' AND column_name = 'askForCredit'"  ;

        $row = $wpdb->get_results($rowQur);
        if(empty($row)){
            $alterQur = "ALTER TABLE ".$table_name." ADD askForCredit TINYINT(1) DEFAULT 0";
            $wpdb->query($alterQur);
        }

    }
}

if( ! function_exists( 'CreateOrderStatusTables' ) ) {
    function CreateOrderStatusTables(){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  pckws_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  order_id int(11) NOT NULL,
	  wc_order_total int(11) NOT NULL,
	  wc_order_delivered int(11) NOT NULL,
	  status varchar(50) DEFAULT 'new' NOT NULL,
	  UNIQUE KEY id (pckws_id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

if( ! function_exists( 'CreateItemsDeleveredTables' ) ) {
    function CreateItemsDeleveredTables(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_items_delevered");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  pckws_item_id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  order_id int(11) NOT NULL,
	  item_id int(11) NOT NULL,
	  count int(11) DEFAULT '0' NOT NULL,
	  UNIQUE KEY id (pckws_item_id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

    }
}


// delevery

if( ! function_exists( 'CreateShipmentsTables' ) ) {
    function CreateShipmentsTables(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_shipments");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  send_id int(11) NOT NULL,
	  shipment_id varchar(50) NOT NULL,
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

if( ! function_exists( 'QtyLevert' ) ) {
    function QtyLevert($order_id){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        $qtyLevert = $wpdb->get_var("SELECT wc_order_delivered FROM $table_name WHERE order_id LIKE $order_id");
        if(!$qtyLevert)return 0;
        else return $qtyLevert;
    }
}

if( ! function_exists( 'getNettbutikkIdFromOrder' ) ) {
    function getNettbutikkIdFromOrder($order_id){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        $nettbutikk_id = $wpdb->get_var("SELECT nettbutikk_id FROM $table_name WHERE order_id LIKE $order_id");
        if(!$nettbutikk_id)return 0;
        else return $nettbutikk_id;
    }
}

if( ! function_exists( 'QtyOrdered' ) ) {
    function QtyOrdered($order_id){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        $qtyOrdered = $wpdb->get_var("SELECT wc_order_total FROM $table_name WHERE order_id LIKE $order_id");
        if(!$qtyOrdered)return 0;
        else return $qtyOrdered;
    }
}

if( ! function_exists( 'copyTableFromOldTable' ) ) {
    function copyTableFromOldTable($old_tableName,$kasse_nettbutikk_id){
        /*
        //WriteLog('1: '.$old_tableName);
        global $wpdb;
        if($wpdb->get_var("SHOW TABLES LIKE '$old_tableName'") == $old_tableName) {
            $qurr = "SELECT * FROM ".$old_tableName." WHERE nettbutikk_id = '{$kasse_nettbutikk_id}'";

            $orders = $wpdb->get_results($qurr,OBJECT);
            foreach($orders as $order){
                WriteLog($order->order_id.' copied from old style');
                M360PCKOrderStatus::UpdateOrderStatus($order->order_id,$order->wc_order_total,$order->wc_order_delivered,$order->status,$kasse_nettbutikk_id);
                $wpdb->delete(
                    $old_tableName,
                    array(
                        'pckws_id' =>$order->pckws_id
                    ));
            }
        }
        */
    }
}

if( ! function_exists( 'ITEMSLEVER' ) ) {
    function ITEMSLEVER($order_id,$nettbutikk_id=0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_items_delevered");
        return $wpdb->get_results( "SELECT * FROM {$table_name} WHERE order_id = {$order_id} AND nettbutikk_id = {$nettbutikk_id}",OBJECT);
    }
}

// -- short codes
if( ! function_exists( 'SKUTOPOSTID' ) ) {
    function SKUTOPOSTID($SKU){
        $scaped_sku = esc_sql($SKU);
        // get WOOCOMM product id from PCK SKU
        global $wpdb;
        $table_name = m360_pck_get_table_name("postmeta");

        $result = $wpdb->get_results("SELECT post_id FROM {$table_name} WHERE meta_key = '_sku' AND meta_value LIKE '%{$scaped_sku}%'");
        $post_id_obj = is_array($result)?end($result):$result;
        $post_id = (is_object($post_id_obj))?$post_id_obj->post_id:$post_id_obj;
        return $post_id;
    }
}
if( ! function_exists( 'PCKArticleID' ) ) {
    function PCKArticleID($id){

        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();
            switch_to_blog( $current_blog_id );
            $PCK_articleID = get_post_meta( $id, 'm360_pck_article_id_0',true );
            restore_current_blog();
        }else{
            $PCK_articleID = get_post_meta( $id, 'm360_pck_article_id_0',true );
        }


        ?>
        <?php if(!empty($PCK_articleID)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>PCK ArticleID</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $PCK_articleID; ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKArticleID_shortcode' ) ) {
    function PCKArticleID_shortcode($sku){
        if(isset($sku)){
            $post_id = SKUTOPOSTID($sku);
            if(is_multisite()) {
                $current_blog_id = get_current_blog_id();

                switch_to_blog($current_blog_id);
                echo get_post_meta( $post_id, 'm360_pck_article_id_0',true );
                restore_current_blog();
            }else{
                echo get_post_meta( $post_id, 'm360_pck_article_id_0',true );
            }
        }
    }
}
add_shortcode('m360-pck-article_id', 'PCKArticleID_shortcode');

if( ! function_exists( 'PCKManufacturer' ) ) {
    function PCKManufacturer($id){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $manufacturer = get_post_meta( $id, 'm360_pck_manufacturer', true );
            restore_current_blog();
        }else{
            $manufacturer = get_post_meta( $id, 'm360_pck_manufacturer', true );
        }

        ?>
        <div class="options_group">
            <p class="form-field">
                <label>
                    <?php echo isset( $productTabOptions [ 'm360_pck_produsent_label' ] ) ?
                        esc_attr( $productTabOptions [ 'm360_pck_produsent_label' ] ) : 'Manufacturer'; ?>
                </label>
                <input type="text" class="select2-input"  placeholder="<?php echo isset($manufacturer)?$manufacturer:'-'; ?>" style="width: 212px;" disabled>
                <span class="description">Shortcode [m360-pck-brand SKU] ex. [m360-pck-brand 100042].</span>

            </p>
        </div>

        <?php
    }
}

if( ! function_exists( 'PCKHideWhenOutOfStock' ) ) {
    function PCKHideWhenOutOfStock($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $hideWhenOutOfStock = get_post_meta( $id, 'm360_pck_hideWhenOutOfStock',true ); //hideWhenOutOfStock
            restore_current_blog();
        }else{
            $hideWhenOutOfStock = get_post_meta( $id, 'm360_pck_hideWhenOutOfStock',true ); //hideWhenOutOfStock
        }

        ?>
        <?php if(isset($hideWhenOutOfStock)):?>
            <div class="options_group">not</div>
        <?php endif; ?>
        <?php
    }
}

if( ! function_exists( 'PCKManufacturer_shortcode' ) ) {
    function PCKManufacturer_shortcode($sku){
        if(isset($sku)){
            $post_id = SKUTOPOSTID($sku);
            if(is_multisite()) {
                $current_blog_id = get_current_blog_id();

                switch_to_blog($current_blog_id);
                echo get_post_meta( $post_id, 'm360_pck_manufacturer',true );
                restore_current_blog();
            }else{
                echo get_post_meta( $post_id, 'm360_pck_manufacturer',true );
            }

        }
    }
}
add_shortcode('m360-pck-brand', 'PCKManufacturer_shortcode');

if( ! function_exists( 'PCKArticleStatus' ) ) {
    function PCKArticleStatus($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $articleStatus = get_post_meta( $id, 'm360_pck_articleStatus',true ); //Varestatus
            restore_current_blog();
        }else{
            $articleStatus = get_post_meta( $id, 'm360_pck_articleStatus',true ); //Varestatus
        }

        ?>
        <?php if(isset($articleStatus)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Varestatus</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo M360PCKMissingAttributes::GetArticleStatus($articleStatus); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>

        <?php
    }
}

if( ! function_exists( 'PCKCostPrice' ) ) {
    function PCKCostPrice($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $costPrice = get_post_meta( $id, 'm360_pck_costPrice',true );
            restore_current_blog();
        }else{
            $costPrice = get_post_meta( $id, 'm360_pck_costPrice',true );
        }
        ?>
        <?php if(isset($costPrice)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Kjøppris</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo get_woocommerce_currency_symbol(). ' '.round($costPrice,2); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}

if( ! function_exists( 'PCKExternalLink' ) ) {
    function PCKExternalLink($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $externalLink = get_post_meta( $id, 'm360_pck_externalLink',true ); //Extern Link
            restore_current_blog();
        }else{
            $externalLink = get_post_meta( $id, 'm360_pck_externalLink',true ); //Extern Link
        }
        ?>
        <?php if(isset($externalLink) && !empty($externalLink)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Extern Link</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $externalLink; ?>" style="width: 212px;" disabled>
                    <span class="description">Shortcode [m360-pck-external-link SKU] ex. [m360-pck-external-link 100042].</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}

if( ! function_exists( 'PCKExternalLink_shortcode' ) ) {
    function PCKExternalLink_shortcode($sku){

        if(isset($sku)){
            $post_id =  SKUTOPOSTID($sku);
            if(is_multisite()) {
                $current_blog_id = get_current_blog_id();
                switch_to_blog($current_blog_id);
                echo get_post_meta( $post_id, 'm360_pck_externalLink',true );
                restore_current_blog();
            }else{
                echo get_post_meta( $post_id, 'm360_pck_externalLink',true );
            }

        }
    }
}
add_shortcode('m360-pck-external-link', 'PCKExternalLink_shortcode');

if( ! function_exists( 'PCKSubtitle' ) ) {
    function PCKSubtitle($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $subtitle = get_post_meta( $id, 'm360_pck_subtitle',true ); //subtitle
            restore_current_blog();
        }else{
            $subtitle = get_post_meta( $id, 'm360_pck_subtitle',true ); //subtitle
        }
        ?>
        <?php if(isset($subtitle) && !empty($subtitle)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Udernavn</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $subtitle; ?>" style="width: 212px;" disabled>
                    <span class="description">Shortcode [m360-pck-subtitle SKU] ex. [m360-pck-subtitle 100042]</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKSubtitle_shortcode' ) ) {
    function PCKSubtitle_shortcode($sku){
        if(isset($sku)){
            $post_id = SKUTOPOSTID($sku);
            if(is_multisite()) {
                $current_blog_id = get_current_blog_id();
                switch_to_blog($current_blog_id);
                echo get_post_meta( $post_id, 'm360_pck_subtitle',true ); //subtitle
                restore_current_blog();
            }else{
                echo get_post_meta( $post_id, 'm360_pck_subtitle',true ); //subtitle
            }
        }
    }
}

	add_shortcode('m360-pck-subtitle', 'PCKSubtitle_shortcode');
if( ! function_exists( 'PCKAlternativePrice' ) ) {
    function PCKAlternativePrice($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $alternativePrice = get_post_meta( $id, 'm360_pck_alternativePrice',true );// Alt Utpris
            restore_current_blog();
        }else{
            $alternativePrice = get_post_meta( $id, 'm360_pck_alternativePrice',true );// Alt Utpris
        }
        ?>
        <?php if(isset($alternativePrice)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Alt. Utpris</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo get_woocommerce_currency_symbol(). ' '.round($alternativePrice,2); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKAlternativePrice2' ) ) {
    function PCKAlternativePrice2($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $alternativePrice2 = get_post_meta( $id, 'm360_pck_alternativePrice2',true ); //Takeaway pris
            restore_current_blog();
        }else{
            $alternativePrice2 = get_post_meta( $id, 'm360_pck_alternativePrice2',true ); //Takeaway pris
        }
        ?>
        <?php if(isset($alternativePrice2)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Takeaway pris</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo get_woocommerce_currency_symbol(). ' '.round($alternativePrice2,2); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKArticleWebAction' ) ) {
    function PCKArticleWebAction($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();
            switch_to_blog($current_blog_id);
            $articleWebAction = get_post_meta( $id, 'm360_pck_articleWebAction',true ); //Web Kjøpstype
            restore_current_blog();
        }else{
            $articleWebAction = get_post_meta( $id, 'm360_pck_articleWebAction',true ); //Web Kjøpstype
        }
        ?>
        <?php if(isset($articleWebAction)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Web Kjøpstype</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo M360PCKMissingAttributes::GetArticleWebAction($articleWebAction); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKShippingType' ) ) {
    function PCKShippingType($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $shippingType = get_post_meta( $id, 'm360_pck_shippingType',true ); //Sendingstype
            restore_current_blog();
        }else{
            $shippingType = get_post_meta( $id, 'm360_pck_shippingType',true ); //Sendingstype
        }
        ?>
        <?php if(isset($shippingType)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Sendingstype</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo M360PCKMissingAttributes::GetShippingType($shippingType); ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKConfirmedDelivery' ) ) {
    function PCKConfirmedDelivery($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();
            switch_to_blog($current_blog_id);
            $confirmedDelivery = get_post_meta( $id, 'm360_pck_confirmedDelivery',true ); //confirmedDelivery
            restore_current_blog();
        }else{
            $confirmedDelivery = get_post_meta( $id, 'm360_pck_confirmedDelivery',true ); //confirmedDelivery
        }
        ?>
        <?php if(isset($confirmedDelivery)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Confirmed Delivery</label>
                    <input type="checkbox" class="checkbox" <?php echo ($confirmedDelivery)?'checked':''; ?> disabled>
                    <span class="description">Checked if Expected Delivery Date is confirmed</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKEANS' ) ) {
    function PCKEANS($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $eans = get_post_meta( $id, 'm360_pck_eans',true ); //EANS
            restore_current_blog();
        }else{
            $eans = get_post_meta( $id, 'm360_pck_eans',true ); //EANS
        }
        ?>
        <?php if(isset($eans)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>EANS</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $eans; ?>" style="width: 212px;" disabled>
                    <span class="description">Shortcode [m360-pck-eans SKU] ex. [m360-pck-eans 100042]</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}
if( ! function_exists( 'PCKEANS_shortcode' ) ) {
    function PCKEANS_shortcode($sku){
        if(isset($sku)){
            $post_id = SKUTOPOSTID($sku);
            $current_blog_id = get_current_blog_id();
            if($current_blog_id>0) {
                switch_to_blog($current_blog_id);
                echo get_post_meta( $post_id, 'm360_pck_eans',true );
                restore_current_blog();
            }else{
                echo get_post_meta( $post_id, 'm360_pck_eans',true );
            }

        }
    }
}

	add_shortcode('m360-pck-eans', 'PCKEANS_shortcode');
if( ! function_exists( 'PCKExpectedDeliveryAmount' ) ) {
    function PCKExpectedDeliveryAmount($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $expectedDeliveryAmount = get_post_meta( $id, 'm360_pck_expectedDeliveryAmount',true ); //Expected Delivery Amount
            restore_current_blog();
        }else{
            $expectedDeliveryAmount = get_post_meta( $id, 'm360_pck_expectedDeliveryAmount',true ); //Expected Delivery Amount
        }
        ?>
        <?php if(isset($expectedDeliveryAmount) && !empty($expectedDeliveryAmount)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Expected Delivery Amount</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $expectedDeliveryAmount; ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKExpectedDeliveryDate' ) ) {
    function PCKExpectedDeliveryDate($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $expectedDeliveryDate = get_post_meta( $id, 'm360_pck_expectedDeliveryDate',true ); //Expected Delivery Date
            restore_current_blog();
        }else{
            $expectedDeliveryDate = get_post_meta( $id, 'm360_pck_expectedDeliveryDate',true ); //Expected Delivery Date
        }
        ?>
        <?php if(isset($expectedDeliveryDate) && !empty($expectedDeliveryDate)):?>
            <?php  $date = date('Y-m-d H:i:s', $expectedDeliveryDate/1000); ?>
            <div class="options_group">
                <p class="form-field">
                    <label>Expected Delivery Date</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $date ?>" style="width: 212px;" disabled>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKExternalGroupId' ) ) {
    function PCKExternalGroupId($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $externalGroupId = get_post_meta( $id, 'm360_pck_externalGroupId',true ); //Pckasse ArticleGroupID used by SendDiscount.categoryID
            restore_current_blog();
        }else{
            $externalGroupId = get_post_meta( $id, 'm360_pck_externalGroupId',true ); //Pckasse ArticleGroupID used by SendDiscount.categoryID
        }
        ?>
        <?php if(isset($externalGroupId) && !empty($externalGroupId)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>External GroupId</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $externalGroupId; ?>" style="width: 212px;" disabled>
                    <span class="description">Pckasse ArticleGroupID used by SendDiscount.categoryID</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKNoDiscount' ) ) {
    function PCKNoDiscount($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $noDiscount = get_post_meta( $id, 'm360_pck_noDiscount',true ); //Order discount field should not be used.
            restore_current_blog();
        }else{
            $noDiscount = get_post_meta( $id, 'm360_pck_noDiscount',true ); //Order discount field should not be used.
        }

        ?>
        <?php if(isset($noDiscount)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>No Discount</label>
                    <input type="checkbox" class="checkbox" <?php echo ($noDiscount)?'checked':''; ?> disabled>
                    <span class="description">Order discount field should not be used.</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKNonStockItem' ) ) {
    function PCKNonStockItem($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $nonStockItem = get_post_meta( $id, 'm360_pck_nonStockItem',true ); //This article in external stock (Use special text if zero stock)
            restore_current_blog();
        }else{
            $nonStockItem = get_post_meta( $id, 'm360_pck_nonStockItem',true ); //This article in external stock (Use special text if zero stock)
        }

        ?>
        <?php if(isset($nonStockItem)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Non Stock Item</label>
                    <input type="checkbox" class="checkbox" <?php echo ($nonStockItem)?'checked':''; ?> disabled>
                    <span class="description">This Product in external stock (Use special text if zero stock).</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }

}
if( ! function_exists( 'PCKNonStockItemDays' ) ) {
    function PCKNonStockItemDays($id){
        if(is_multisite()) {
            $current_blog_id = get_current_blog_id();

            switch_to_blog($current_blog_id);
            $nonStockItemDays = get_post_meta( $id, 'm360_pck_nonStockItemDays',true ); //Days to get the article from external stock
            restore_current_blog();
        }else{
            $nonStockItemDays = get_post_meta( $id, 'm360_pck_nonStockItemDays',true ); //Days to get the article from external stock
        }
        ?>
        <?php if(isset($nonStockItemDays) && !empty($nonStockItemDays)):?>
            <div class="options_group">
                <p class="form-field">
                    <label>Non Stock Item</label>
                    <input type="text" class="select2-input"  placeholder="<?php echo $nonStockItemDays; ?>" style="width: 212px;" disabled>
                    <span class="description">Days to get the article from external stock.</span>
                </p>
            </div>
        <?php endif; ?>
        <?php
    }
}


if( ! function_exists( 'br_aapf_get_attributes' ) ) {
	/**
	 * Get all possible woocommerce attribute taxonomies
	 *
	 * @return mixed|void
	 */
	function br_aapf_get_attributes() {
		$attribute_taxonomies = wc_get_attribute_taxonomies();
		$attributes           = array();

		if ( $attribute_taxonomies ) {
			foreach ( $attribute_taxonomies as $tax ) {
				$attributes[ wc_attribute_taxonomy_name( $tax->attribute_name ) ] = $tax->attribute_label;
			}
		}

		return apply_filters( 'berocket_aapf_get_attributes', $attributes );
	}
}

if( ! function_exists( 'CreateSizeColorIDTable' ) ) {
    function CreateSizeColorIDTable(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  post_id int(11) NOT NULL,
	  sizeColor_id int(11) NOT NULL,
	  kasse_nr int(11) NOT NULL DEFAULT 0,
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

}
/* ids */
if( ! function_exists( 'addKassNrKolToSizeColorIdsTableIfNotExists' ) ) {
    function addKassNrKolToSizeColorIdsTableIfNotExists($table_name){
        global $wpdb;
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $rowQur = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '".$table_name."' AND column_name = 'kasse_nr'"  ;

            $row = $wpdb->get_results($rowQur);
            if(empty($row)){
                $alterQur = "ALTER TABLE ".$table_name." ADD kasse_nr INT(11) NOT NULL DEFAULT 0";
                $wpdb->query($alterQur);
            }
        }

    }

}
if( ! function_exists( 'UpdateSizeColorID' ) ) {
    function UpdateSizeColorID($post_id,$sizeColorId,$kasse_nr=0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateSizeColorIDTable();
        }
        addKassNrKolToSizeColorIdsTableIfNotExists($table_name);

        $qur = "SELECT id FROM {$table_name} WHERE sizeColor_id = {$sizeColorId} AND kasse_nr = {$kasse_nr} LIMIT 1";
        $_id = $wpdb->get_var($qur);
        if(!$_id){
            $wpdb->insert(
                $table_name,
                array('post_id'=>$post_id,
                    'sizeColor_id'=>$sizeColorId,
                    'kasse_nr'=> $kasse_nr));
        }else{
            $wpdb->update(
                $table_name,
                array('id'=>$_id,
                    'post_id'=>$post_id,
                    'sizeColor_id'=>$sizeColorId,
                    'kasse_nr'=>$kasse_nr),
                array('id'=>$_id));
        }

    }

}
if( ! function_exists( 'm360_pck_get_table_name' ) ) {
    function m360_pck_get_table_name($for_table){
        global $wpdb;
        $multiStorePlugin = n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')?true:false;
        //$main_blog_id =   $blog_id;

        $table_name = $wpdb->prefix . $for_table;
        if(is_multisite()) {
            if($multiStorePlugin){
                $table_name =  $wpdb->base_prefix.$for_table;
            }else{
                if($for_table == 'm360_pckasse_orderstatus' ||
                    $for_table == 'm360_pckasse_items_delevered' ||
                    $for_table == 'm360_pckasse_shipments' ||
                    $for_table == 'm360_pckasse_customers' ||
                    $for_table == 'm360_pckasse_customers_groups'){
                    $table_name = $wpdb->base_prefix.$for_table;
                }else{
                    $current_blog_id = get_current_blog_id();
                    $table_name =  $wpdb->base_prefix . $current_blog_id . '_'.$for_table;
                }
            }

        }
        return $table_name;
    }
}

if( ! function_exists( 'sizeColorIdToPostId' ) ) {
    function sizeColorIdToPostId($sizeColorId, $kasse_nr = 0){
        global $wpdb;
        $postmeta_table_name = m360_pck_get_table_name("postmeta");
        $sql_postmeta = "SELECT post_id FROM {$postmeta_table_name} WHERE meta_key LIKE '_sizeColorId_{$kasse_nr}' AND meta_value = {$sizeColorId} LIMIT 1";

        $sizeColorIdToPost =  $wpdb->get_var($sql_postmeta);
        if($sizeColorIdToPost>0)return $sizeColorIdToPost;
        else return 0;
    }
}
if( ! function_exists( 'getVariationIdFromSizeColorId' ) ) {
    function getVariationIdFromSizeColorId($sizeColorId,$kasse_nr=0){
        global $wpdb;
        $postsmeta_table = m360_pck_get_table_name("postmeta");

        $post_id = $wpdb->get_var("SELECT post_id FROM {$postsmeta_table} WHERE meta_key = '_sizeColorId_$kasse_nr' AND meta_value LIKE '{$sizeColorId}' LIMIT 1");
        if($post_id && $post_id>0)return $post_id;


        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateSizeColorIDTable();
        }
        $sql = "SELECT post_id FROM {$table_name} WHERE sizeColor_id = {$sizeColorId} AND kasse_nr = {$kasse_nr} LIMIT 1";
        $post_id =  $wpdb->get_var($sql);
        if($post_id)return $post_id;
        else return 0;

    }

}
if(!function_exists('deleteSizeColorIdFromDb')){
    function deleteSizeColorIdFromDb($ProductId,$kasse_nr = 0){
        $wc_product = wc_get_product($ProductId);
        if($wc_product && $wc_product->has_child( )){
            global $wpdb;
            $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");
            foreach ($wc_product->get_children( ) as $child_id){
                $sql = "SELECT id FROM {$table_name} WHERE post_id = {$child_id} AND kasse_nr = {$kasse_nr} LIMIT 1";
                $_id =  $wpdb->get_var($sql);
                if($_id){
                    $wpdb->delete($table_name, array('id' =>$_id));
                }
            }

        }

    }
}


if( ! function_exists( 'getSizeColorIdForPostIdFromKasse' ) ) {
    function getSizeColorIdForPostIdFromKasse($post_id,$kasse_nr=0){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateSizeColorIDTable();
        }
        $sql = "SELECT sizeColor_id FROM {$table_name} WHERE post_id = {$post_id} AND kasse_nr = {$kasse_nr} LIMIT 1";
        $sizeColor_id =  $wpdb->get_var($sql);

        return $sizeColor_id;
    }

}
if( ! function_exists( 'retriveKasseNr' ) ) {
    function retriveKasseNr(){
        global $current_PCK;
        if(is_object($current_PCK)){
            return getRealKasseNrForLocation($current_PCK->PCKLocation);
        }
        //return (is_object($current_PCK) && isset($current_PCK->PCKasseNr))?$current_PCK->PCKasseNr:0;
        return 0;
    }
}


if( ! function_exists( 'CreateParentsIDsTable' ) ) {
    function CreateParentsIDsTable(){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_parentsIds");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  post_id int(11) NOT NULL,
	  parent_id varchar(150) NOT NULL,
	  kasse_nr int(11) NOT NULL DEFAULT 0,
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

}
if( ! function_exists( 'UpdateParentId_samling' ) ) {
    function UpdateParentId_samling($post_id,$parent_id,$kasse_nr){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_parentsIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateParentsIDsTable();
        }

        $qur = "SELECT id FROM {$table_name} WHERE parent_id = {$parent_id} AND kasse_nr = {$kasse_nr} LIMIT 1";
        $_id = $wpdb->get_var($qur);
        if(!$_id){
            $wpdb->insert(
                $table_name,
                array('post_id'=>$post_id,
                    'parent_id'=>$parent_id,
                    'kasse_nr'=> $kasse_nr));
        }else{
            $wpdb->update(
                $table_name,
                array('id'=>$_id,
                    'post_id'=>$post_id,
                    'parent_id'=>$parent_id,
                    'kasse_nr'=>$kasse_nr),
                array('id'=>$_id));
        }
    }

}
if( ! function_exists( 'getPostIdFromParentsIdTbl' ) ) {
    function getPostIdFromParentsIdTbl($parent_id,$kasse_nr=0){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_parentsIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateParentsIDsTable();
        }
        $qur = "SELECT post_id FROM " . $table_name . " WHERE parent_id LIKE '".$parent_id."' AND kasse_nr = ".$kasse_nr." LIMIT 1";
        $post_id =  $wpdb->get_var($qur);

        return $post_id;
    }

}
if( ! function_exists( 'getPCKarticleIdFromParentsIdTbl' ) ) {
    function getPCKarticleIdFromParentsIdTbl($post_id,$kasse_nr=0){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_parentsIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateParentsIDsTable();
        }
        $qur = "SELECT parent_id FROM " . $table_name . " WHERE post_id LIKE ".$post_id." AND kasse_nr = ".$kasse_nr." LIMIT 1";
        $pck_parent_id =  $wpdb->get_var($qur);

        return $pck_parent_id;
    }

}

/* end */

/* options */
if( ! function_exists( 'getProductOption' ) ) {
    function getProductOption($option,$default){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ $option] )?$productTabOptions[ $option]:$default;
        return $saved;
    }

}
if( ! function_exists( 'getSamlingAttributesArray' ) ) {
    function getSamlingAttributesArray(){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_how_the_attributes_entered'] )?$productTabOptions[ 'm360_pck_how_the_attributes_entered']:'navn-farge-størrelse';
        $saved_comp = explode('-',$saved);
        $attributes = array();
        foreach($saved_comp as $key =>$comp){
            if($comp == 'navn')$attributes['name'] = $key;
            if($comp == 'farge')$attributes['color'] = $key;
            if($comp == 'størrelse')$attributes['size'] = $key;
        }
        return $attributes;
    }

}
if( ! function_exists( 'CreatePCKKreditCustomersTable' ) ) {
    function CreatePCKKreditCustomersTable(){
        global $wpdb;
        //$wpdb->show_errors();

        $table_name = m360_pck_get_table_name("m360_pckasse_customers");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  invoiceAddress varchar(100) DEFAULT '',
	  invoiceAddress2 varchar(100) DEFAULT '',
      countryCode INT UNSIGNED NOT NULL DEFAULT 578,
	  contactId int(11) UNSIGNED NOT NULL,
	  email varchar(50) DEFAULT NULL,
	  name varchar(100) DEFAULT '',
	  orgNo varchar(50) DEFAULT '',
	  pckCustomerId int(11) UNSIGNED NOT NULL DEFAULT 0,
	  phone1 varchar(50) DEFAULT '',
	  invoicePostCity varchar(50) DEFAULT '',
	  invoicePostNo varchar(50) DEFAULT '',
	  fax varchar(50) DEFAULT NULL,
	  invoiceOnEmail TINYINT(1) DEFAULT 0,
	  creditApproved TINYINT(1) DEFAULT 0,
	  UNIQUE KEY id (id)
	) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

}
/* customers */
if( ! function_exists( 'UpdatePCKKreditCustomers' ) ) {
    function UpdatePCKKreditCustomers($contactId,$order,$invoiceOnEmail,$creditApproved,$pckCustomerId,$ask_for_credit=false){
        global $wpdb;
        //$wpdb->show_errors();


        $table_name = m360_pck_get_table_name("m360_pckasse_customers");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreatePCKKreditCustomersTable();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);
        addAskForCreditIfNotExist($table_name);

        $email = $order->get_billing_email();
        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE email LIKE '$email'");
        $nettbutikk_id = (is_multisite())?get_current_blog_id():0;

        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'invoiceAddress' =>$order->get_billing_address_1(),
                    'invoiceAddress2'=>$order->get_billing_address_2(),
                    'countryCode'=>0,
                    'contactId'=>$contactId,
                    'email'=>$order->get_billing_email(),
                    'name'=>$order->get_billing_first_name().' '.$order->get_billing_last_name(),
                    'orgNo' => (property_exists($order,'get_billing_orgno'))?$order->get_billing_orgno():'',
                    'pckCustomerId'=>$pckCustomerId,
                    'phone1'=>$order->get_billing_phone(),
                    'invoicePostCity'=>$order->get_billing_city(),
                    'invoicePostNo'=>$order->get_billing_postcode(),
                    'fax'=>$order->get_billing_phone(),
                    'invoiceOnEmail'=>$invoiceOnEmail,
                    'creditApproved'=>$creditApproved,
                    'nettbutikk_id'=>$nettbutikk_id,
                    'askForCredit'=> $ask_for_credit,
                ));
        }else{
            $wpdb->update(
                $table_name,
                array(
                    'invoiceAddress' =>$order->get_billing_address_1(),
                    'invoiceAddress2'=>$order->get_billing_address_2(),
                    'countryCode'=>0,
                    'contactId'=>$contactId,
                    'email'=>$order->get_billing_email(),
                    'name'=>$order->get_billing_first_name().' '.$order->get_billing_last_name(),
                    'orgNo' => (property_exists($order,'get_billing_orgno'))?$order->get_billing_orgno():'',
                    'pckCustomerId'=>$pckCustomerId,
                    'phone1'=>$order->get_billing_phone(),
                    'invoicePostCity'=>$order->get_billing_city(),
                    'invoicePostNo'=>$order->get_billing_postcode(),
                    'fax'=>$order->get_billing_phone(),
                    'invoiceOnEmail'=>$invoiceOnEmail,
                    'creditApproved'=>$creditApproved,
                    'nettbutikk_id'=>$nettbutikk_id,
                    'askForCredit'=> $ask_for_credit,
                ),
                array('id'=>$_id));
        }

    }

}

if( ! function_exists( 'UpdatePCKKreditCustomersFromPCK' ) ) {
    function UpdatePCKKreditCustomersFromPCK($customerInfo,$contact_id=0){

        global $wpdb;
        //$wpdb->show_errors();


        $table_name = m360_pck_get_table_name("m360_pckasse_customers");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreatePCKKreditCustomersTable();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);
        addAskForCreditIfNotExist($table_name);

        $nettbutikk_id = is_numeric($customerInfo->welcomeMessage)?$customerInfo->welcomeMessage:0;

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE email LIKE '$customerInfo->email'");

        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'invoiceAddress' =>$customerInfo->address1,
                    'invoiceAddress2'=>$customerInfo->address2,
                    'countryCode'=>(property_exists($customerInfo,'countryCode'))?$customerInfo->countryCode:'',
                    'contactId'=>($contact_id>0)?$contact_id:$customerInfo->deltaCustomerId,
                    'email'=>$customerInfo->email,
                    'name'=>$customerInfo->name,
                    'orgNo' => $customerInfo->orgNo,
                    'pckCustomerId'=>$customerInfo->pckCustomerId,
                    'phone1'=>$customerInfo->phoneNo,
                    'invoicePostCity'=>$customerInfo->postCity,
                    'invoicePostNo'=>$customerInfo->postNo,
                    'fax'=>(property_exists($customerInfo,'fax'))?$customerInfo->fax:'',
                    'invoiceOnEmail'=>0,
                    'creditApproved'=>(property_exists($customerInfo,'creditApproved'))?$customerInfo->creditApproved:0,
                    'nettbutikk_id'=>$nettbutikk_id,
                    'askForCredit'=> 0,
                ));
        }else{
            $wpdb->update(
                $table_name,
                array(
                    'invoiceAddress' =>$customerInfo->address1,
                    'invoiceAddress2'=>$customerInfo->address2,
                    'countryCode'=>(property_exists($customerInfo,'countryCode'))?$customerInfo->countryCode:'',
                    'email'=>$customerInfo->email,
                    'name'=>$customerInfo->name,
                    'orgNo' => $customerInfo->orgNo,
                    'phone1'=>$customerInfo->phoneNo,
                    'invoicePostCity'=>$customerInfo->postCity,
                    'invoicePostNo'=>$customerInfo->postNo,
                    'fax'=>(property_exists($customerInfo,'fax'))?$customerInfo->fax:'',
                    'invoiceOnEmail'=>0,
                    'creditApproved'=>(property_exists($customerInfo,'creditApproved'))?$customerInfo->creditApproved:0,
                    'askForCredit'=> 0,
                ),
                array('id'=>$_id));
        }

    }

}

if( ! function_exists( 'UpdatePCKKreditCustomersFromUser' ) ) {
    function UpdatePCKKreditCustomersFromUser($user_id){
        global $wpdb;
        $customer = new WC_Customer( $user_id );
        $org_no = get_user_meta( $user_id, 'billing_orgno', true );
        $ask_for_credit = get_user_meta( $user_id, 'billing_askforcredit', true );

        $table_name = m360_pck_get_table_name("m360_pckasse_customers");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreatePCKKreditCustomersTable();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);
        addAskForCreditIfNotExist($table_name);

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE contactid = {$user_id}");
        $nettbutikk_id = (is_multisite())?get_current_blog_id():0;

        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'invoiceAddress' =>$customer->get_billing_address_1(),
                    'invoiceAddress2'=>$customer->get_billing_address_2(),
                    'contactId'=>$user_id,
                    'email'=>$customer->get_billing_email(),
                    'name'=>$customer->get_billing_first_name().' '.$customer->get_billing_last_name(),
                    'orgNo' => $org_no,
                    'phone1'=>$customer->get_billing_phone(),
                    'invoicePostCity'=>$customer->get_billing_city(),
                    'invoicePostNo'=>$customer->get_billing_postcode(),
                    'fax'=>$customer->get_billing_phone(),
                    'nettbutikk_id'=>$nettbutikk_id,
                    'askForCredit'=> $ask_for_credit,
                ));
        }else{
            $wpdb->update(
                $table_name,
                array(
                    'invoiceAddress' =>$customer->get_billing_address_1(),
                    'invoiceAddress2'=>$customer->get_billing_address_2(),
                    'contactId'=>$user_id,
                    'email'=>$customer->get_billing_email(),
                    'name'=>$customer->get_billing_first_name().' '.$customer->get_billing_last_name(),
                    'orgNo' => $org_no,
                    'phone1'=>$customer->get_billing_phone(),
                    'invoicePostCity'=>$customer->get_billing_city(),
                    'invoicePostNo'=>$customer->get_billing_postcode(),
                    'fax'=>$customer->get_billing_phone(),
                    'nettbutikk_id'=>$nettbutikk_id,
                    'askForCredit'=> $ask_for_credit,
                ),
                array('id'=>$_id));
        }

    }

}

if( ! function_exists( 'CreateCustomersGroupsTable' ) ) {
    function CreateCustomersGroupsTable(){
        global $wpdb;
        //$wpdb->show_errors();

        $table_name = m360_pck_get_table_name("m360_pckasse_customers_groups");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  wp_role_name varchar(100) DEFAULT '',
	  customerGroupId int(11) UNSIGNED NOT NULL DEFAULT 0,
	  kasse_nr int(11) NOT NULL,
	  UNIQUE KEY id (id)
	) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

}
/* new */
if( ! function_exists( 'UpdateCustomerGroupsTable' ) ) {
    function UpdateCustomerGroupsTable($customerGroupId,$wp_role_name='',$kasse_nr=0){

        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers_groups");
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateCustomersGroupsTable();
        }
        $_id = $wpdb->get_var("SELECT id FROM {$table_name} WHERE customerGroupId = {$customerGroupId}");
        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'wp_role_name' =>$wp_role_name,
                    'customerGroupId'=>$customerGroupId,
                    'kasse_nr'=>$kasse_nr,
                ));
        }else{
            $wpdb->update(
                $table_name,
                array(
                    'wp_role_name' =>$wp_role_name,
                    'customerGroupId'=>$customerGroupId,
                    'kasse_nr'=>$kasse_nr,
                ),
                array('id'=>$_id));
        }

    }

}

if( ! function_exists( 'getRoleNameByCustomerGroupId' ) ) {
    function getRoleNameByCustomerGroupId($customerGroupId){
        global $wpdb;

        $table_name = m360_pck_get_table_name("m360_pckasse_customers_groups");

        $role_name = $wpdb->get_var("SELECT wp_role_name FROM {$table_name} WHERE customerGroupId = {$customerGroupId} LIMIT 1");
        return ($role_name)?$role_name:false;
    }

}
if( ! function_exists( 'getUserIdByPCKCustomerID' ) ) {
    function getUserIdByPCKCustomerID($pckCustomerId){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");

        $contactId = $wpdb->get_var("SELECT contactId FROM {$table_name} WHERE pckCustomerId = {$pckCustomerId} LIMIT 1");
        return ($contactId)?$contactId:false;
    }

}

if( ! function_exists( 'isCustomerCreditApproved' ) ) {
    function isCustomerCreditApproved($user_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");
        addAskForCreditIfNotExist($table_name);

        $approved = $wpdb->get_var("SELECT creditApproved FROM {$table_name} WHERE contactid = {$user_id} LIMIT 1");
        return ($approved)?$approved:false;
    }

}

if( ! function_exists( 'isAskedForCredit' ) ) {
    function isAskedForCredit($user_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");
        addAskForCreditIfNotExist($table_name);

        $askForCredit = $wpdb->get_var("SELECT askForCredit FROM {$table_name} WHERE contactid = {$user_id} LIMIT 1");
        return ($askForCredit)?$askForCredit:false;
    }

}

if( ! function_exists( 'getUserByEmailAndNettbutikk' ) ) {
    function getUserByEmailAndNettbutikk($email,$nettbutikk_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");
        $qur = "SELECT * FROM {$table_name} WHERE email LIKE '{$email}' AND nettbutikk_id = '{$nettbutikk_id}' LIMIT 1 ";
        return $wpdb->get_row($qur,OBJECT);
    }

}


if( ! function_exists( 'getPCKKreditCustomers' ) ) {
    function getPCKKreditCustomers($email){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");
        addAskForCreditIfNotExist($table_name);

        $contactId = $wpdb->get_var("SELECT contactId FROM {$table_name} WHERE email = '{$email}' LIMIT 1");
        return ($contactId)?$contactId:false;
    }

}
if( ! function_exists( 'getAllPCKKreditCustomers' ) ) {
    function getAllPCKKreditCustomers(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_customers");
        addAskForCreditIfNotExist($table_name);
        $sql = "SELECT * FROM {$table_name} WHERE creditApproved = 0 AND contactId > 0 AND name <> ''";
        return $wpdb->get_results($sql);
    }
}

if( ! function_exists( 'createCustomerFromOrder' ) ) {
    function createCustomerFromOrder($order){
        $email = $order->get_billing_email();
        $user = get_user_by( 'email', $email );

        if(!$user){
            $password = wp_generate_password( 12, true, true );
            //$user_id = wp_create_user(sanitize_email($email),$password,sanitize_email($email));
            //$user = get_user_by( 'email', $email );
            $newusername = sanitize_user( current( explode( '@', $email ) ), true );
            $user_id = wc_create_new_customer( sanitize_email( $email ), wc_clean( $newusername ), $password );
        }else{
            $user_id = $user->ID;
        }

        update_user_meta( $user_id, "first_name", $order->get_billing_first_name() );
        update_user_meta( $user_id, "last_name", $order->get_billing_last_name() );

        update_user_meta( $user_id, "billing_first_name", $order->get_billing_first_name() );
        update_user_meta( $user_id, "billing_last_name", $order->get_billing_last_name() );

        update_user_meta( $user_id, "billing_address_1", $order->get_billing_address_1() );
        update_user_meta( $user_id, "billing_address_2", $order->get_billing_address_2() );
        update_user_meta( $user_id, "billing_city", $order->get_billing_city());
        update_user_meta( $user_id, "billing_postcode", $order->get_billing_postcode() );
        update_user_meta( $user_id, "billing_email", $order->get_billing_email());
        update_user_meta( $user_id, "billing_phone", $order->get_billing_phone() );
        if(property_exists($order,'get_billing_orgno')){
            update_user_meta( $user_id, "billing_orgno", $order->get_billing_orgno() );
        }

        return $user_id;
    }

}
if( ! function_exists( 'CreateMultiKasserTable' ) ) {
    function CreateMultiKasserTable(){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $charset_collate = $wpdb->get_charset_collate();

            $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  kasse_nr int(11) NOT NULL,
	  kasse_loc varchar(100) NOT NULL,
	  kasse_lic varchar(100) NOT NULL,
	  kasse_username varchar(100) NOT NULL,
	  kasse_password varchar(100) NOT NULL,
	  is_default int(1) DEFAULT NULL,
	  UNIQUE KEY id (id)
	) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }

    }

}
// multi
if( ! function_exists( 'kasserInDB' ) ) {
    function kasserInDB(){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        return $wpdb->get_results( "SELECT * FROM {$table_name}",OBJECT);
    }

}
if( ! function_exists( 'getKasseNrForLocation' ) ) {
    function getKasseNrForLocation($location){
        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        $kasse_nr = $wpdb->get_var("SELECT kasse_nr FROM $table_name WHERE kasse_loc LIKE '{$location}'");

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $counter = $setupTabOptions[ 'm360_number_of_pckasser' ];
        if(!$kasse_nr)return $counter-1;
        else return $kasse_nr;
    }

}


if( ! function_exists( 'getRealKasseNrForLocation' ) ) {
    function getRealKasseNrForLocation($location){
        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        return $wpdb->get_var("SELECT kasse_nr FROM $table_name WHERE kasse_loc LIKE '{$location}'");
    }

}

if( ! function_exists( 'getLocationForKasseNr' ) ) {
    function getLocationForKasseNr($kasse_nr){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        return $wpdb->get_var("SELECT kasse_loc FROM $table_name WHERE kasse_nr = {$kasse_nr}");
    }

}

if( ! function_exists( 'getKasseObjectForNr' ) ) {
    function getKasseObjectForNr($kasseNr){
        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        return $wpdb->get_results( "SELECT * FROM {$table_name} WHERE kasse_nr = {$kasseNr} LIMIT 1",OBJECT);
    }

}
if( ! function_exists( 'getDefaultKasse' ) ) {
    function getDefaultKasse(){
        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        return $wpdb->get_results( "SELECT * FROM {$table_name} WHERE is_default = 1 LIMIT 1",OBJECT);
    }

}
if( ! function_exists( 'getKasseNrForCredintals' ) ) {
    function getKasseNrForCredintals($login,$password){
        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        $kasse = $wpdb->get_row("SELECT * FROM $table_name WHERE kasse_username LIKE '{$login}' AND kasse_password LIKE '{$password}' LIMIT 1 ",OBJECT);
        $kasse_nr = 0;
        if($kasse){
            $kasse_nr = $kasse->kasse_nr;
            $wpdb->update(
                $table_name,
                array(
                    'kasse_nr' =>$kasse_nr,
                    'kasse_loc'=>$kasse->kasse_loc,
                    'kasse_lic'=>$kasse->kasse_lic,
                    'kasse_username'=>$kasse->kasse_username,
                    'kasse_password'=>$kasse->kasse_password,
                    'is_default' => $kasse->is_default,
                ),
                array('id'=>$kasse->id));
        }

        return $kasse_nr;
    }

}

if( ! function_exists( 'addKasse' ) ) {
    function addKasse($kasse_nummer,$location,$licence,$login,$password,$is_default){

        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateMultiKasserTable();
        }

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE kasse_nr LIKE '$kasse_nummer'");

        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'kasse_nr' =>$kasse_nummer,
                    'kasse_loc'=>$location,
                    'kasse_lic'=>$licence,
                    'kasse_username'=>$login,
                    'kasse_password'=>$password,
                    'is_default' => $is_default,
                ));
        }else{
            $wpdb->update(
                $table_name,
                array(
                    'kasse_nr' =>$kasse_nummer,
                    'kasse_loc'=>$location,
                    'kasse_lic'=>$licence,
                    'kasse_username'=>$login,
                    'kasse_password'=>$password,
                    'is_default' => $is_default,
                ),
                array('id'=>$_id));
        }

    }

}

if(!function_exists('version_check')){
    function version_check( $version = '3.0' ) {
        if ( class_exists( 'WooCommerce' ) ) {
            global $woocommerce;
            if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
                return true;
            }
        }
        return false;
    }
}

if(!function_exists('checkForPreviosVersions')){
    function checkForPreviosVersions(){
        $setupTabOptions = get_option( 'm360_pck_options_setup' );

        $pck_username = isset( $setupTabOptions[ 'm360_pck_username' ] ) ? esc_attr( $setupTabOptions[ 'm360_pck_username' ] ) : '';
        $pck_password = isset( $setupTabOptions[ 'm360_pck_password' ] ) ? esc_attr( $setupTabOptions[ 'm360_pck_password' ] ) : '';
        $m360_license = isset( $setupTabOptions[ 'm360_pck_license' ] ) ? esc_attr( $setupTabOptions[ 'm360_pck_license' ] ) : '';
        $pck_license = isset( $setupTabOptions[ 'pck_license' ] ) ? esc_attr( $setupTabOptions[ 'pck_license' ] ) : '';
        if(strlen($pck_password)> 0 || strlen($pck_username)>0)
            return array('m360_pck_username'=>$pck_username,'m360_pck_password'=>$pck_password,'m360_pck_license'=>$m360_license,'pck_license'=>$pck_license);
        else return NUlL;
    }
}

if( ! function_exists( 'deleteOldPCK' ) ) {
    function deleteOldPCK($oldPCK){
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        unset($setupTabOptions['m360_pck_username']);
        unset($setupTabOptions['m360_pck_password']);
        unset($setupTabOptions['pck_license']);

        update_option("m360_pck_options_setup",$setupTabOptions);
    }

}

if( ! function_exists( 'writeOldPCK' ) ) {
    function writeOldPCK($oldPCK){

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $counter = isset( $setupTabOptions[ 'm360_number_of_pckasser' ] ) ? esc_attr( $setupTabOptions[ 'm360_number_of_pckasser' ] ) : 1;

        $pckassenr = 0;
        if($counter>1){
            update_option( $setupTabOptions[ 'm360_number_of_pckasser' ], $counter+1 );
            $pckassenr = $counter;
        }
        $key = 'pck_credintal_'.$pckassenr;

        $location = '';
        $pck_license = $oldPCK['pck_license'];
        $username = $oldPCK['m360_pck_username'];
        $password = $oldPCK['m360_pck_password'];


        $is_default = true;


        if(!isset($setupTabOptions[$key]['location']))
            $setupTabOptions[$key]['location'] = $location;

        if(!isset($setupTabOptions[$key]['license']))
            $setupTabOptions[$key]['license'] = $pck_license;

        if(!isset($setupTabOptions[$key]['username']))
            $setupTabOptions[$key]['username'] = $username;

        if(!isset($setupTabOptions[$key]['password']))
            $setupTabOptions[$key]['password'] = $password;

        update_option("m360_pck_options_setup",$setupTabOptions);

        addKasse($pckassenr,$location,$pck_license,$username,$password,$is_default);

        deleteOldPCK($oldPCK);
    }

}
if( ! function_exists( 'setDefaultKasseIfItIsJustOneKasse' ) ) {
    function setDefaultKasseIfItIsJustOneKasse(){
        $setupTabOptions = get_option( 'm360_pck_options_setup' );

        $counter = isset( $setupTabOptions[ 'm360_number_of_pckasser' ] ) ? esc_attr( $setupTabOptions[ 'm360_number_of_pckasser' ] ) : 1;
        if($counter == 1){
            $setupTabOptions['pck_credintal_0']['default'] = true;
            update_option("m360_pck_options_setup",$setupTabOptions);
        }
    }

}
if( ! function_exists( 'toVersion5_1' ) ) {
    function toVersion5_1(){
        $oldPCK = checkForPreviosVersions();
        if($oldPCK && count($oldPCK)>0){
            writeOldPCK($oldPCK);
        }
        setDefaultKasseIfItIsJustOneKasse();
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateSizeColorIDTable();
        }
        addKassNrKolToSizeColorIdsTableIfNotExists($table_name);
    }

}
if( ! function_exists( 'slugify' ) ) {
    function slugify($text){
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}


if( ! function_exists( 'deleteKasse' ) ) {
    function deleteKasse($kasse_nummer){

        global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_multikasser";
        $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE kasse_nr LIKE '$kasse_nummer'");

        if($_id){
            $wpdb->delete(
                $table_name,
                array(
                    'id' =>$_id
                ));
        }

    }

}

if( ! function_exists( 'n_is_plugin_active_for_network' ) ) {
    function n_is_plugin_active_for_network( $plugin ){
        if ( !is_multisite() )
            return false;

        $plugins = get_site_option( 'active_sitewide_plugins');
        if ( isset($plugins[$plugin]) )
            return true;

        return false;
    }

}
if( ! function_exists( 'n_is_plugin_active' ) ) {
    function n_is_plugin_active( $plugin ){
        return in_array( $plugin, (array) get_option( 'active_plugins', array() ) ) || n_is_plugin_active_for_network( $plugin );
    }

}


/* warehouses */
if( ! function_exists( 'CreateWarehousesTable' ) ) {
    function CreateWarehousesTable(){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  sort_id int(11) NOT NULL,
	  warehouseId int(11) NOT NULL,
	  kasse_nr int(11) NOT NULL,
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

if(!function_exists('getWarehousByWarehouseId')){
    function getWarehousByWarehouseId($warehouseId=0,$kasse_nr){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateWarehousesTable();
        }

        return $wpdb->get_row( "SELECT * FROM ".$table_name." WHERE warehouseId = ".$warehouseId." AND kasse_nr = ".$kasse_nr,OBJECT);
    }
}
if( ! function_exists( 'UpdateWarehouses' ) ) {
    function UpdateWarehouses($warehouseId=0,$kasse_nr){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateWarehousesTable();
        }

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE warehouseId = {$warehouseId} AND kasse_nr = {$kasse_nr}");
        $all_warehouses = getAllwarehouses();
        $last_warehouse = (count($all_warehouses)>0)?end($all_warehouses):false;
        $newSort = ($last_warehouse)?$last_warehouse->sort_id+1:1;
        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'sort_id' =>$newSort,
                    'warehouseId'=>$warehouseId,
                    'kasse_nr'=>$kasse_nr,
                ));
        }

    }
}

/* front end warehouses */
if( ! function_exists( 'CreateFrontEndWarehousesTable' ) ) {
    function CreateFrontEndWarehousesTable(){
        global $wpdb;
        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_frontend_warehouses");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  warehouseId int(11) NOT NULL,
	  kasse_nr int(11) NOT NULL,
	  name varchar(100) DEFAULT '',
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

if(!function_exists('getFrontEndWarehousByWarehouseId')){
    function getFrontEndWarehousByWarehouseId($warehouseId=0,$kasse_nr=0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_frontend_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateFrontEndWarehousesTable();
        }

        return $wpdb->get_row( "SELECT * FROM {$table_name} WHERE warehouseId = {$warehouseId} AND kasse_nr = {$kasse_nr} LIMIT 1",OBJECT);
    }
}
if(!function_exists('getAllFrontEndWarehouses')){
    function getAllFrontEndWarehouses($kasse_nr=0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_frontend_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateFrontEndWarehousesTable();
        }

        return $wpdb->get_results( "SELECT * FROM {$table_name} WHERE kasse_nr = {$kasse_nr}",OBJECT);
    }
}
if( ! function_exists( 'UpdateFrontEndWarehouses' ) ) {
    function UpdateFrontEndWarehouses($warehouseId=0,$kasse_nr,$name=''){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_frontend_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateFrontEndWarehousesTable();
        }

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE warehouseId = {$warehouseId} AND kasse_nr = {$kasse_nr}");
        if(!$_id){
            $wpdb->insert(
                $table_name,
                array(
                    'warehouseId'=>$warehouseId,
                    'kasse_nr'=>$kasse_nr,
                    'name' => 'Standard'
                ));
        }else if(strlen($name)>0){
            $wpdb->update(
                $table_name,
                array(
                    'warehouseId'=>$warehouseId,
                    'kasse_nr'=>$kasse_nr,
                    'name' => $name),
                array('id'=>$_id));
        }

    }
}

if(!function_exists('getAllwarehouses')){
    function getAllwarehouses($kasse_nr = false){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateWarehousesTable();
        }
        if($kasse_nr)
            return $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE kasse_nr = ".$kasse_nr,OBJECT);
        else
            return $wpdb->get_results( "SELECT * FROM ".$table_name,OBJECT);
    }
}

if(!function_exists('getWarehousBySortId')){
    function getWarehousBySortId($sort_id,$kasse_nr){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateWarehousesTable();
        }

        $qur = "SELECT * FROM $table_name WHERE sort_id = {$sort_id} AND kasse_nr = {$kasse_nr} LIMIT 1";
        return $wpdb->get_row($qur,OBJECT);
    }
}

if( ! function_exists( 'UpdateWarehousesSort' ) ) {
    function UpdateWarehousesSort($data){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_warehouses");
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateWarehousesTable();
        }
        $wpdb->query("TRUNCATE TABLE {$table_name}");
        foreach($data as $warehouse){
            $wpdb->insert(
                $table_name,
                array(
                    'sort_id' =>$warehouse->sort_id,
                    'warehouseId'=>$warehouse->warehouseId,
                    'kasse_nr'=>$warehouse->kasse_nr,
                ));
        }
    }
}

if(!function_exists('getWarehouseIdFromOrderItem')){
    function getWarehouseIdFromOrderItem($Product){
        //WriteLog(__FUNCTION__.' '.print_r($Product,true));
        $product_id = ($Product['variation_id']>0)?$Product['variation_id']:$Product['product_id'];

        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $stockDetails_v = get_post_meta($product_id,'stockDetails_'.$kasse_nr,true);
        $qty = 0;
        if(is_object($Product)){
            $qty = $Product->get_quantity();
        }else if(array_key_exists('qty',$Product)){
            $qty = $Product['qty'];
        }else if(array_key_exists('quantity',$Product)){
            $qty = $Product['quantity'];
        }

        if(!$stockDetails_v)return array('warehous_id_inorder'=>0,'count_in_order'=>$qty,'post_id'=>$product_id);;

        if(is_object($stockDetails_v)){
            return array('warehous_id_inorder'=>$stockDetails_v->warehouseId,'count_in_order'=>$qty,'post_id'=>$product_id);;
        }

        $all_warehouses = getAllwarehouses();
        $founds_warehouses = array();
        if($qty > 0){
            foreach ($all_warehouses as $current_warehouse){
                if(count($founds_warehouses)>0)break;
                foreach ($stockDetails_v as $stockdetail){
                    if ($stockdetail->warehouseId == $current_warehouse->warehouseId &&
                        $stockdetail->count >= $qty){
                        //WriteLog('1: product_id: '.$product_id.' qty orderd: '.$qty.' warehoue_id: '.$stockdetail->warehouseId);
                        //$warehouseId =  $stockdetail->warehouseId;
                        $founds_warehouses[] = array('warehous_id_inorder'=>$stockdetail->warehouseId,'count_in_order'=>$qty,'post_id'=>$product_id);
                        break;
                    }
                }
            }
        }

        if(count($founds_warehouses)>0)return $founds_warehouses;
        $remaim = $qty;
        if($remaim > 0){
            foreach ($all_warehouses as $current_warehouse){
                if($remaim <= 0)break;
                foreach ($stockDetails_v as $stockdetail){
                    if($remaim <= 0)break;
                    if ($stockdetail->count >= 0 && $remaim > 0){
                        if($remaim>$stockdetail->count){
                            $founds_warehouses[] = array('warehous_id_inorder'=>$stockdetail->warehouseId,'count_in_order'=>$stockdetail->count,'post_id'=>$product_id);
                            $remaim = $remaim-$stockdetail->count;
                        }else{
                            $founds_warehouses[] = array('warehous_id_inorder'=>$stockdetail->warehouseId,'count_in_order'=>$remaim,'post_id'=>$product_id);
                            $remaim = 0;
                        }
                    }
                }
            }
        }
        /*
        WriteLog('FOUND_02: '.print_r($founds_warehouses,true));

        $counts = array();
        foreach ($stockDetails_v as $key => $stock_count) {
            $counts[$key] = $stock_count->count;
        }
        array_multisort($counts, SORT_ASC, $stockDetails_v);

        $last_warehous = end($stockDetails_v);
        $warehouseId = $last_warehous->warehouseId;

        //WriteLog('2: product_id: '.$product_id.' qty orderd: '.$qty.' warehoue_id: '.$stockdetail->warehouseId);
        */
        return $founds_warehouses;
    }
}

if( ! function_exists( 'getInitialsFromName' ) ) {
    function getInitialsFromName($name){
        $words = explode(" ", $name);
        $acronym = "";

        foreach ($words as $w) {
            if(is_string($w)){
                $acronym .= $w;
            }elseif (is_array($w)){
                $acronym .= $w[0];
            }
        }
        return $acronym;
    }
}
/* discounts table */
if( ! function_exists( 'CreateDiscountTable' ) ) {
    function CreateDiscountTable(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_discounts_table");

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name(
	  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	  articleId int(11),
	  category2Id int (11),
	  categoryId int (11),
	  count DECIMAL (11),
	  customerGroupId int (11),
	  customerId int (11),
	  discount DECIMAL,
	  discountId int (11),
	  manufacturerId int (11),
	  priceAdjustment DECIMAL,
	  priceType DECIMAL,
	  validUntil datetime,
	  kasse_nr int(11),
	  UNIQUE KEY id (id)
	) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
}

if( ! function_exists( 'GetDiscountByDiscountId' ) ) {
    function GetDiscountByDiscountId($discountId, $kasse_nr = 0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_discounts_table");

        return $wpdb->get_row("SELECT * FROM $table_name WHERE discountId = {$discountId} AND kasse_nr = {$kasse_nr} LIMIT 1",OBJECT);
    }
}
if( ! function_exists( 'DeleteDiscountFromDiscountsTable' ) ) {
    function DeleteDiscountFromDiscountsTable($discountId){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_discounts_table");
        $wpdb->delete($table_name, array('discountId' =>$discountId));
    }
}
if( ! function_exists( 'UpdateDiscountTable' ) ) {
    function UpdateDiscountTable($disscount,$kasse_nr=0){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_discounts_table");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateDiscountTable();
        }
        $discountObj = GetDiscountByDiscountId($disscount->discountId);
        if($discountObj){
            $wpdb->update(
                $table_name,
                array(
                    'articleId' =>$disscount,
                    'category2Id' =>$disscount->category2Id,
                    'categoryId' =>$disscount->categoryId,
                    'count' =>$disscount->count,
                    'customerGroupId' =>$disscount->customerGroupId,
                    'customerId' =>$disscount->customerId,
                    'discount' =>$disscount->discount,
                    'discountId' =>$disscount->discountId,
                    'manufacturerId' =>$disscount->manufacturerId,
                    'priceAdjustment' =>$disscount->priceAdjustment,
                    'priceType' =>$disscount->priceType,
                    'validUntil' =>$disscount->validUntil,
                    'kasse_nr' =>$kasse_nr,
                ),
                array('id'=>$discountObj->id));
        }else{
            $wpdb->insert(
                $table_name,
                array(
                    'articleId' =>$disscount,
                    'category2Id' =>$disscount->category2Id,
                    'categoryId' =>$disscount->categoryId,
                    'count' =>$disscount->count,
                    'customerGroupId' =>$disscount->customerGroupId,
                    'customerId' =>$disscount->customerId,
                    'discount' =>$disscount->discount,
                    'discountId' =>$disscount->discountId,
                    'manufacturerId' =>$disscount->manufacturerId,
                    'priceAdjustment' =>$disscount->priceAdjustment,
                    'priceType' =>$disscount->priceType,
                    'validUntil' =>$disscount->validUntil,
                    'kasse_nr' =>$kasse_nr,
                )
            );
        }

    }
}
if(!function_exists('safeKommaForSlug')){
    function safeKommaForSlug($text){
        return str_replace(array("'", "\"", ",",".",";"," "), "_", $text);
        //return str_replace(',','_',$text);
    }
}

if(!function_exists('insertTerms')){
    function insertTerms($terms,$term_attribute){
        $term_ids = array();
        if(!is_array($terms))$terms = (array)$terms;
        foreach ($terms as $term){
            //WriteLog('Term: '.print_r($term,true));
            $slug = safeKommaForSlug($term);
            $old_term = get_term_by('slug', $slug, $term_attribute);
            if($old_term){
                $term_ids[] = $old_term->term_id;

            }else{
                $inserted_term = wp_insert_term(
                    $term,
                    $term_attribute,
                    array(
                        'slug'    => $slug
                    )
                );
                $term_ids[] = $inserted_term['term_id'];
            }

        }
        return $term_ids;

    }
}



if(!function_exists('m360_pck_is_default_pck')){
    function m360_pck_is_default_pck($current_PCK){
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $saved = isset($setupTabOptions[ 'm360_pck_limit_to_defult_pck'] )?$setupTabOptions[ 'm360_pck_limit_to_defult_pck']:'yes';
        $is_default = true;
        if($saved == 'yes'){
            if(!$current_PCK->PCKasseIsDefault){
                $is_default = false;
            }
        }
        return $is_default;
    }
}
if(!function_exists('m360_pck_kassenr')){
    function m360_pck_kassenr(){
        global $current_PCK;
        return (is_object($current_PCK) && isset($current_PCK->PCKasseNr))?$current_PCK->PCKasseNr:0;
    }
}
if(!function_exists('m360_pck_check_if_product_found_for_kasse')){
    function m360_pck_check_if_product_found_for_kasse($product_id){
        //WriteLog('1: '.$product_id);
        $result = get_post_meta($product_id,'m360_pck_article_id_'.m360_pck_kassenr(),true);
        //WriteLog('2: '.$result);
        return $result;
    }
}


/* licence api */
if(!function_exists('CallLicenceAPI')){
    function CallLicenceAPI($method, $data = false){
        $url = 'http://superkasse.no/api/';
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }


        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result);
    }
}
if(!function_exists('APICheckLicence')){
    function APICheckLicence(){
        WriteLog('Checking license ');
        $para = "method=check&url=".get_site_url();
        $url = 'http://superkasse.no/api/?'.$para;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        return json_decode($result);
    }
}

if(!function_exists('m360_get_depositom_product_id')){
    function m360_get_depositom_product_id(){

        $depositsTabOptions = get_option( 'm360_pck_options_deposits' );
        if(isset($depositsTabOptions[ 'm360_pck_deposits_active'] ) == 'yes' && isset( $depositsTabOptions [ 'm360_pck_deposits_product_sku' ] )){
            $sku = $depositsTabOptions [ 'm360_pck_deposits_product_sku' ];
            if(strlen($sku)>0){

                //$product_id = wc_get_product_id_by_sku($sku);
                //return $product_id;
                return SKUTOPOSTID($sku);
            }
        }
        return 0;
    }
}

if(!function_exists('m360_get_depositom_product_sku')){
    function m360_get_depositom_product_sku(){

        $depositsTabOptions = get_option( 'm360_pck_options_deposits' );
        if(isset($depositsTabOptions[ 'm360_pck_deposits_active'] ) == 'yes' && isset( $depositsTabOptions [ 'm360_pck_deposits_product_sku' ] )){
            return $depositsTabOptions [ 'm360_pck_deposits_product_sku' ];
        }
        return '';
    }
}


if( ! function_exists( 'M360EmptySizeColorIds' ) ) {
    function M360EmptySizeColorIds(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");
        if($wpdb->query("TRUNCATE TABLE {$table_name}")){
            WriteLog('TRUNCATE m360_pckasse_sizeColorIds');
        }

    }
}

if( ! ( function_exists( 'pck_get_attachment_by_image_url' ) ) ) {
    function pck_get_attachment_by_image_url($image_url) {
        global $wpdb;
        $table_name = m360_pck_get_table_name("posts");

        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $table_name WHERE guid='%s';", $image_url ));
        if($attachment)return $attachment[0];
        return false;
    }
}