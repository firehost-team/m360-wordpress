<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_service.php' );

class pck_image{
    public static function save_image_to_media_library($name,$image_date,$post_title,$post_content=''){
        $wp_upload_dir = wp_upload_dir();

        $image_directory = $wp_upload_dir['path'] . '/';
        $file = $image_directory.$name;
        if(file_exists($file)){
            unlink($file);
        }
        if (!is_dir($image_directory))
            mkdir($image_directory, 0777, true);


        $upload = wp_upload_bits($name, null, $image_date );
        if( !empty( $upload['error'] ) ) {
            WriteLog('Error: '.print_r($upload['error'],true));
        }

        $file_path = $upload['file'];
        $file_name = basename( $file_path );
        $file_type = wp_check_filetype( $file_name, null );


        $guid = $wp_upload_dir['url'] . '/' . $file_name;
        $attach_id = pck_get_attachment_by_image_url($guid);
        if(!$attach_id){
            $post_info = array(
                'guid'				=> $guid,
                'post_mime_type'	=> $file_type['type'],
                'post_title'		=> $post_title,
                'post_content'		=> $post_content,
                'post_status'		=> 'inherit',
            );

            //$image_id = wp_insert_attachment($attachment, $file);
            $attach_id = wp_insert_attachment( $post_info, $file_path );

        }

        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        //$attach_data = wp_generate_attachment_metadata( $image_id, $name );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

        wp_update_attachment_metadata( $attach_id, $attach_data );
        clean_attachment_cache($attach_id);

        return $attach_id;

    }
}


class M360PCKReceiver{


    public static function saveChildBlogProduct($product_id,$to_blog_id){
        update_post_meta($product_id , '_woonet_network_main_product', true);
        $_woonet_publish_to = '_woonet_publish_to_' . $to_blog_id;
        update_post_meta( $product_id, $_woonet_publish_to, 'yes');

        include_once(WOO_MSTORE_PATH . '/include/class.admin.product.php');
        global $blog_id;
        global $wpdb;

        $main_blog_id =   $blog_id;

        switch_to_blog( $main_blog_id );
        $post_data = get_post($product_id);
        $meta_values = get_post_meta( $post_data );

        $product_interface                =   new WOO_MSTORE_admin_product();
        $product_interface->process_product($product_id, $post_data, TRUE);
        //wp_update_post();
        restore_current_blog();

        $table_name = (strlen($wpdb->base_prefix)>0)?$wpdb->base_prefix.$to_blog_id."_postmeta":$wpdb->prefix .$to_blog_id. "postmeta";
        $qur = "SELECT post_id FROM " . $table_name . " WHERE meta_key = '_woonet_network_is_child_product_id' AND meta_value = '".$product_id."'  LIMIT 1";
        $child_id =  $wpdb->get_var($qur);

        if($child_id){
            switch_to_blog( $to_blog_id );

            if(is_array($meta_values)){
                foreach ( $meta_values as $key => $value ) {
                    update_post_meta( $child_id, $key, $value );
                }
            }
            update_post_meta($child_id, '_woonet_child_inherit_updates', 'yes');
            update_post_meta($child_id, '_woonet_child_stock_synchronize', 'yes');
            restore_current_blog();
        }

    }

    public static function updateMultiStore($product_id,$Product){
        if(USE_MULTISORE){
            $vat = 1+($Product->vat/100);
            if(!empty($Product->price6) && $Product->price6>0){
                $to_blog = $Product->price6/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price7) && $Product->price7>0){
                $to_blog = $Product->price7/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price8) && $Product->price8>0){
                $to_blog = $Product->price8/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price9) && $Product->price9>0){
                $to_blog = $Product->price9/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }
        }else{
            include_once(WOO_MSTORE_PATH . '/include/class.admin.product.php');
            global $blog_id;
            $main_blog_id =   $blog_id;
            switch_to_blog( $main_blog_id );
            $post_data = get_post($product_id);
            $product_interface                =   new WOO_MSTORE_admin_product();
            $product_interface->process_product($product_id, $post_data, TRUE);
            //wp_update_post();
            restore_current_blog();
        }



    }

    /* end mult stor */

    public static function IsExistingProduct($articleId,$articleNo=NULL){
        $isExist = false;
        $kasse_nr = retriveKasseNr();

        global $wpdb;
        $table_name = m360_pck_get_table_name("postmeta");

        $ProductId = $wpdb->get_var($wpdb->prepare(
            "SELECT post_id FROM " . $table_name . " WHERE meta_key = 'm360_pck_article_id_".$kasse_nr."' AND meta_value = '%s' LIMIT 1",$articleId
        ));

        if(is_numeric($ProductId)){$isExist = true;}
        if(!$isExist && $articleNo){
            $ProductId = $wpdb->get_var($wpdb->prepare(
                "SELECT post_id FROM " . $table_name . " WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1",$articleNo
            ));
            if(is_numeric($ProductId)){
                add_post_meta($ProductId, 'm360_pck_article_id_'.$kasse_nr, $articleId, true);
                $isExist = true;
            }
        }

        return $isExist;
    }



    private static function GetWOOCOMMProductIdFromPCKArticleId($articleId){
        global $wpdb;
        $kasse_nr = retriveKasseNr();
        /*
                if(is_multisite()) {
                    $current_blog_id = get_current_blog_id();
                    switch_to_blog( $current_blog_id );
                    $table_name = $wpdb->postmeta;
                    restore_current_blog();
                }else{
                    $table_name = $wpdb->postmeta;
                }
        */
        $table_name = m360_pck_get_table_name("postmeta");
        $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id_{$kasse_nr}' AND meta_value = '{$articleId}' LIMIT 1";

        $id = $wpdb->get_var($qur);
        if($id<=0){
            $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id_' AND meta_value = '{$articleId}' LIMIT 1";
            $id = $wpdb->get_var($qur);
            if($id<=0) {
                $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id' AND meta_value = '{$articleId}' LIMIT 1";
                $id = $wpdb->get_var($qur);
            }

            if($id>0){
                delete_post_meta($id, 'm360_pck_article_id');
                add_post_meta($id, 'm360_pck_article_id_'.$kasse_nr, $articleId, true);
            }
        }
        return $id;
    }


    /* new */
    private static function InsertCategoryIntoWOOCOMM($articleGroup){
        $slug = CreateSlug($articleGroup->name);
        $Term = get_term_by('slug', $slug, 'product_cat' );
        if (!$Term->term_id || $Term->term_id == 0) {
            $Term = wp_insert_term($articleGroup->name, 'product_cat', array('description' => ($articleGroup->description)?$articleGroup->description:' ', 'slug' => $slug));
        }

        return $Term->term_id;

    }

    private static function InsertSubCategoryIntoWOOCOMM($articleGroup, $ParentId){
        $Term = term_exists($articleGroup->name, 'product_cat', $ParentId);
        if (!is_wp_error( $Term ) && $Term !== 0 && $Term !== null) {
            return $Term['term_id'];
        }else{
            $Term = wp_insert_term($articleGroup->name, 'product_cat', array('slug' => CreateSlug($articleGroup->name), 'parent' => $ParentId));
            if(is_wp_error($Term)){
                WriteLog('Error: '.print_r($Term,true));
                return false;
            }else{
                if (array_key_exists('error_data', $Term)) {
                    return $Term->error_data['term_exists'];
                }else{
                    return $Term['term_id'];
                }
            }


        }
    }
    /* ---- */

    private static function UpdateDeliveryPrice($SKU, $Price){
        // get WOOCOMM PCK options from WordPress database
        $PCKOptionsShipping = get_option('m360_pck_options_shipping');

        // check each supported type of shipping to see if the SKU matches any that have been mapped in the WordPress PCK options
        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['free_shipping'] == $SKU)
        {
            // update the minimum amount to qualify for free shipping
            $WoocommSetting = get_option('woocommerce_free_shipping_settings');
            $WoocommSetting['min_amount'] = $Price;
            update_option('woocommerce_free_shipping_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['local_delivery'] == $SKU)
        {
            // update the local delivery flat rate fee
            $WoocommSetting = get_option('woocommerce_local_delivery_settings');
            $WoocommSetting['fee'] = $Price;
            update_option('woocommerce_local_delivery_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['flat_rate'] == $SKU)
        {
            // update the flat rate delivery fee
            $WoocommSetting = get_option('woocommerce_flat_rate_settings');
            $WoocommSetting['cost_per_order'] = $Price;
            update_option('woocommerce_flat_rate_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['international_delivery'] == $SKU)
        {
            // update the cost of international delivery
            $WoocommSetting = get_option('woocommerce_international_delivery_settings');
            $WoocommSetting['cost'] = $Price;
            update_option('woocommerce_international_delivery_settings', $WoocommSetting);

            return true;
        }

        // we're not dealing with shipping here, so tell the calling method to continue...
        return false;
    }

    public static function pricing($ProductId,$Product){
        $wc_product = wc_get_product($ProductId);
        if($wc_product->is_type('variable')){
            update_post_meta( $ProductId, '_sale_price_dates_to', '' );
            update_post_meta( $ProductId, '_sale_price_dates_from', '' );
            update_post_meta( $ProductId, '_price', '');
            update_post_meta( $ProductId, '_sale_price','');

            $wc_product->set_date_on_sale_from('');
            $wc_product->set_date_on_sale_to('');
            $wc_product->set_sale_price('');
            $wc_product->set_price('');
            $wc_product->save();

            return;
        }
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $oreavrunding = 'no';
        if(isset($productTabOptions[ 'm360_pck_product_oreavrunding'] )){
            $oreavrunding = $productTabOptions[ 'm360_pck_product_oreavrunding'];
        }

        $use_veil_pris = 'no';
        if(isset($productTabOptions[ 'setting_html_m360_pck_veilpris_i_pris'] )){
            $use_veil_pris = $productTabOptions[ 'setting_html_m360_pck_veilpris_i_pris'];
        }
        $pck_pris = $Product->salesPrice;
        if($use_veil_pris == 'yes' && $Product->suggestedPrice > $Product->salesPrice){
            $pck_pris = $Product->suggestedPrice;
        }


        $date_from = isset( $Product->discountFrom ) ? date('Y-m-d H:i:s', $Product->discountFrom/1000) : get_post_meta( $ProductId, '_sale_price_dates_from', true );
        $date_to   = isset( $Product->discountTo ) ? date('Y-m-d H:i:s', $Product->discountTo/1000) : get_post_meta( $ProductId, '_sale_price_dates_to', true );


        if($oreavrunding == 'yes')$pck_pris = floor($pck_pris);

        $discount_price = self::getDiscountPrice($Product);

        if(self::isTaxtable() != 'taxable' && $Product->vat > 0){
            $precentage = '1.'.intval($Product->vat);
            if($pck_pris>0) $pck_pris = $pck_pris/$precentage;
            if($discount_price>0) $discount_price = $discount_price/$precentage;
        }

        if (method_exists($wc_product, 'set_regular_price')){
            $wc_product->set_regular_price($pck_pris);
            if (strtotime( 'NOW', current_time( 'timestamp' ) ) < strtotime( $date_to ) && $discount_price > 0) {
                $wc_product->set_date_on_sale_from(strtotime( $date_from ));
                $wc_product->set_date_on_sale_to(strtotime( $date_to ));
                $wc_product->set_sale_price($discount_price);
                $wc_product->set_price($discount_price);
            }else{
                $wc_product->set_date_on_sale_from('');
                $wc_product->set_date_on_sale_to('');
                $wc_product->set_sale_price(($Product->salesPrice >= $pck_pris)?'':$Product->salesPrice );
                $wc_product->set_price($pck_pris);
            }
            $wc_product->save();
        } else {
            update_post_meta( $ProductId, '_regular_price', $pck_pris);
            if (strtotime( 'NOW', current_time( 'timestamp' ) ) < strtotime( $date_to ) && $discount_price > 0) {
                update_post_meta( $ProductId, '_sale_price_dates_from', strtotime( $date_from ) );
                update_post_meta( $ProductId, '_sale_price_dates_to', strtotime( $date_to ) );
                update_post_meta( $ProductId, '_sale_price', $discount_price);
                update_post_meta( $ProductId, '_price', $discount_price);
            }else{
                update_post_meta( $ProductId, '_sale_price_dates_to', '' );
                update_post_meta( $ProductId, '_sale_price_dates_from', '' );
                update_post_meta( $ProductId, '_price', $pck_pris);
                update_post_meta( $ProductId, '_sale_price',($Product->salesPrice >= $pck_pris)?'':$Product->salesPrice);
            }
        }


    }

    public static function calculateBackorder($ProductId,$product_info = false){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $backorder = (isset($productTabOptions[ 'm360_pck_standardbackorder']))?$productTabOptions[ 'm360_pck_standardbackorder']:'yes';
        $m360_pck_hideWhenOutOfStock = get_post_meta( $ProductId, 'm360_pck_hideWhenOutOfStock',true);

        if(!empty($m360_pck_hideWhenOutOfStock)){
            $backorder = (isset($productTabOptions[ 'm360_pck_backorder']))?$productTabOptions[ 'm360_pck_backorder']:'no';
        }
        $nonStockItem = get_post_meta($ProductId, 'm360_pck_nonStockItem',true );
        if($nonStockItem)$backorder = 'yes';

        if($product_info){
            if($product_info->nonStockItem){
                update_post_meta($ProductId,'m360_pck_nonStockItem',true);
                $backorder = 'yes';
            }
        }
        return $backorder;
    }

    private static function calculate_multi_pckasser_count($ProductId,$count){
        global $current_PCK;
        $kasse_nr = retriveKasseNr();

        update_post_meta($ProductId, 'm360_stock_for_pck_'.$kasse_nr, $count);
        update_post_meta($ProductId, 'm360_location_for_pck_'.$kasse_nr, $current_PCK->PCKLocation);


        $current_pck_stock = 0;

        //$setupTabOptions = get_option( 'm360_pck_options_setup' );
        //$numberOfPCKasser = isset( $setupTabOptions[ 'm360_number_of_pckasser' ] ) ? $setupTabOptions[ 'm360_number_of_pckasser' ]  : 1;
        $kasserInDB = kasserInDB();

        if(is_array($kasserInDB)){
            foreach($kasserInDB as $kasse){//for($i = 0; $i<$numberOfPCKasser ; $i++){
                $stock = get_post_meta( $ProductId, 'm360_stock_for_pck_'.$kasse->kasse_nr, true );
                if (is_numeric($stock)){
                    $current_pck_stock += (int)$stock;
                }
            }
        }
        return $current_pck_stock;
    }

    private static function handlingWarehousesOptions($ProductId){
        $kasse_nr = retriveKasseNr();
        $stockDetails = get_post_meta($ProductId,'stockDetails_'.$kasse_nr,true);

        if($stockDetails && is_array($stockDetails)){
            foreach ($stockDetails as $key => $stockDetail){
                //$wearhous = getWarehousByWarehouseId($stockDetail->warehouseId,$kasse_nr);
                if($stockDetail && is_object($stockDetail))UpdateWarehouses($stockDetail->warehouseId,$kasse_nr);
            }
        }
        /*
        $all_warehouses = getAllwarehouses($kasse_nr);

        $warehousesOptions = get_option( 'm360_pck_options_warehouses' );
        $warehousesOptions[ 'm360_pck_warehouses' ] = $all_warehouses;

        update_option( 'm360_pck_options_warehouses', $warehousesOptions );
        */
    }
    private static function handlingStockDetails($ProductId,$Product){
        $kasse_nr = retriveKasseNr();
        if(property_exists($Product,'stockDetails')){
            $stockDetails = $Product->stockDetails;
            update_post_meta($ProductId,'stockDetails_'.$kasse_nr,$stockDetails);
            if(is_object($stockDetails)&& property_exists($stockDetails,'warehouseId')){
                UpdateFrontEndWarehouses($Product->stockDetails->warehouseId,$kasse_nr);
            }else if(is_array($stockDetails)){
                foreach ($stockDetails as $stockDetail){
                    if(is_object($stockDetail)&& property_exists($stockDetail,'warehouseId')){
                        UpdateFrontEndWarehouses($stockDetail->warehouseId,$kasse_nr);
                    }
                }
            }
        }

    }
    private static function handling_stock($ProductId,$Product){
        //WriteLog(__FUNCTION__.' '.print_r($Product,true));
        $wc_product = wc_get_product($ProductId);
        if($wc_product){
            $current_pck_stock = self::calculate_multi_pckasser_count($ProductId,$Product->stockCcount);

            update_post_meta($ProductId, 'm360_pck_hideWhenOutOfStock', $Product->hideWhenOutOfStock);

            $backorder = self::calculateBackorder($ProductId);
            $instock = self::calculateInStock($backorder,$current_pck_stock);
            self::handlingStockDetails($ProductId,$Product);

            $manageStock = 'yes';
            if($current_pck_stock<=0){
                $manageStock = 'no';
            }

            if($backorder == 'yes'){
                $instock = 'instock';
            }
            $deposit_sku = m360_get_depositom_product_sku();

            if($deposit_sku == $wc_product->get_sku()){
                $manageStock = 'no';
                $visibility = 'hidden';
                $instock = 'instock';
            }

            if ($wc_product  && !$wc_product->is_type('external')){
                $wc_product->set_catalog_visibility('visible');
                $wc_product->set_manage_stock($manageStock);
                $wc_product->set_downloadable('no');
                $wc_product->set_virtual('no');
                $wc_product->set_sold_individually('');
                $wc_product->set_backorders($backorder);
                $wc_product->set_stock_quantity($current_pck_stock);
                $wc_product->set_stock_status(wc_clean($instock));
                $wc_product->save();
            }


        }

    }

    private static function physical_properties($ProductId,$Product){
        $wc_product = wc_get_product($ProductId);
        if($wc_product){
            $productTabOptions = get_option( 'm360_pck_options_product' );
            $standardWeight = isset( $productTabOptions [ 'm360_pck_default_weight' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_weight' ] ) : '1';
            $standardLength = isset($productTabOptions [ 'm360_pck_default_length' ] ) ?esc_attr( $productTabOptions [ 'm360_pck_default_length' ] ) : '10';
            $standardWidth = isset( $productTabOptions [ 'm360_pck_default_width' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_width' ] ) : '10';
            $standardHeight = isset( $productTabOptions [ 'm360_pck_default_height' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_height' ] ) : '10';

            $weight = ($Product->weight > 0)?$Product->weight:$standardWeight;
            $length = ($Product->length > 0)?$Product->length:$standardLength;
            $width = ($Product->width > 0)?$Product->width:$standardWidth;
            $height = ($Product->height > 0)?$Product->height:$standardHeight;
            $wc_product->set_weight($weight);
            $wc_product->set_length($length);
            $wc_product->set_width($width);
            $wc_product->set_height($height);
            $wc_product->save();
        }
    }

    public static function CheckTheGroups($ProductId,$Product){
        if(wc_get_product($ProductId)){
            $productTabOptions = get_option( 'm360_pck_options_product' );
            if(isset($productTabOptions[ 'm360_pck_sync_categories' ])){
                if($productTabOptions[ 'm360_pck_sync_categories' ] == 'not_sync'){
                    return;
                }
            }

            $articleGroup = $Product->articleGroup;
            $articleGroup2 = $Product->articleGroup2;
            $articleGroup3 = $Product->articleGroup3;

            $SubCategoryIds = array();

            if(is_object($articleGroup)&& strlen($articleGroup->name)>0){
                $CategoryId_1 = self::InsertCategoryIntoWOOCOMM($articleGroup);
                $SubCategoryIds[] = $CategoryId_1;
                if(is_object($articleGroup2)&& strlen($articleGroup2->name)>0 && $CategoryId_1){
                    $CategoryId_2 = self::InsertSubCategoryIntoWOOCOMM($articleGroup2,$CategoryId_1);
                    $SubCategoryIds[] = $CategoryId_2;

                    if(is_object($articleGroup3)&& strlen($articleGroup3->name)>0 && $CategoryId_2){
                        $CategoryId_3 = self::InsertSubCategoryIntoWOOCOMM($articleGroup3,$CategoryId_2);
                        $SubCategoryIds[] = $CategoryId_3;
                    }
                }
            }
            if(count($SubCategoryIds)>0){
                wp_set_post_terms($ProductId, $SubCategoryIds, "product_cat");
            }else{
                $terms = get_the_terms($ProductId, 'product_cat');
                if (is_array($terms)){
                    foreach($terms as $term){
                        wp_remove_object_terms($ProductId, $term->term_id, 'product_cat');
                    }
                }

            }
        }
    }

    private function handelTaxClass($Product){
        $tax_class_to_return = '';
        if($Product->vat < 25 && $Product->vat > 0){
            $tax_class_to_return = 'reduced-rate';
        }else if($Product->vat <= 0){
            $tax_class_to_return = 'zero-rate';
        }

        global $wpdb;
        $table = $wpdb->prefix . 'woocommerce_tax_rates';
        $rates = $wpdb->get_var("select tax_rate_id from $table where tax_rate_country like 'NO' and tax_rate_class like '$tax_class_to_return'");

        if(empty($rates)){
            $tax_rate = array(
                'tax_rate_country'  => 'NO',
                'tax_rate'          => $Product->vat,
                'tax_rate_name'     => 'Moms ('.intval($Product->vat).'%)',
                'tax_rate_priority' => 1,
                'tax_rate_class'    => $tax_class_to_return
            );
            WC_Tax::_insert_tax_rate( $tax_rate );

            $svalbard_tax_rate = array(
                'tax_rate_country'  => 'NO',
                'tax_rate'          => 0,
                'tax_rate_name'     => 'Momsfritt (Svalbard)',
                'tax_rate_priority' => 1,
                'tax_rate_class'    => $tax_class_to_return
            );
            $tax_rate_id = WC_Tax::_insert_tax_rate( $svalbard_tax_rate );
            WC_Tax::_update_tax_rate_postcodes( $tax_rate_id, array(9170,9171,9172,9173,9174,9175,9176,9177,9178));
            WC_Tax::_update_tax_rate_cities( $tax_rate_id, 'Svalbard' );

        }
        return $tax_class_to_return;
    }

    public static function isTaxtable(){
        $taxable = 'taxable';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_product_tax'] )){
            $taxable = ($productTabOptions[ 'm360_pck_product_tax'] == 'with')?'taxable':'none';
        }
        return $taxable;
    }
    public static function updatePostMetaForProduct($ProductId,$Product){
        $wc_product = wc_get_product( absint($ProductId) );
        if ( $wc_product ) {
            self::CheckTheGroups($ProductId,$Product);
            $tax_class = self::handelTaxClass($Product);
            update_post_meta($ProductId, '_sku', $Product->articleNo);
            $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );

            if($woocommerce_synk_pris_checkbox == 'yes') {
                self::pricing($ProductId, $Product);
            }
            self::handling_stock($ProductId,$Product);

            self::physical_properties($ProductId,$Product);

            $feature = ($Product->recommendedProduct)?'yes':'no';

            update_post_meta($ProductId, '_purchase_note', '');

            $wc_product->set_featured($feature);
            $wc_product->set_tax_class($tax_class);
            $wc_product->set_tax_status(self::isTaxtable());
            $wc_product->save();

            $kasse_nr = retriveKasseNr();
            if ($wc_product->is_type( 'variable' ) ) {
                update_post_meta( $ProductId, '_sizeColorId_'.$kasse_nr, $Product->articleId );
            }else{
                add_post_meta($ProductId, 'm360_pck_article_id_'.$kasse_nr, $Product->articleId, true);
            }
            //add_post_meta($ProductId, 'm360_pck_article_id', $Product->articleId, true); // for enkelt

            //update_post_meta($ProductId, '_tax_status', 'self::isTaxtable()');
            //update_post_meta($ProductId, '_tax_class', '');
        }
    }


    public static function getNewProductId($Product){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return SKUTOPOSTID($Product->articleNo);

        $ProductStub = array(
            'post_title' => $Product->name,
            'post_status' => 'publish',
            'post_type' => 'product'
        );

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_description'] )?$productTabOptions[ 'm360_pck_description']:'both_if_empty';

        if($saved == 'long_description' || $saved == 'long_description_if_empty'){
            $ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description' || $saved == 'short_description_if_empty'){
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both' || $saved == 'both_if_empty'){
            $ProductStub['post_content'] = $Product->description;
            $ProductStub['post_excerpt'] = $Product->description;
        }

        $ProductId = wp_insert_post($ProductStub);
        return $ProductId;
    }

    public static function SizeColorSKU($sizeColor,$articleId,$colorTitle,$sizeTitle){
        $sizeColorId = $sizeColor->sizeColorId;

        $sku = $articleId;
        if(strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false){
            $sku .= '-'.$sizeColor->color->colorId;
        }

        if(strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false){
            $sku .= '-' . $sizeColor->size->sizeId;
        }
        $sku .= '-' . $sizeColorId;
        return $sku;
    }

    private static function SizeColorTitle($ProductId,$colorTitle,$sizeTitle){
        $post_title = '';
        if(strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false){
            $post_title .= $colorTitle .'-';
        }

        if(strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false){
            $post_title .= $sizeTitle.'-';
        }
        $post_title .= ' for #' . $ProductId;
        return $post_title;
    }

    private static function createVariableProduct($sizeColor,$ProductId,$Product){
        $color_attribute = self::colorAttribute();
        $size_attribute = self::sizeAttribute();

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $use_eans_as_sku = isset($productTabOptions[ 'm360_pck_use_eans_as_sku_for_varianter'] )?$productTabOptions[ 'm360_pck_use_eans_as_sku_for_varianter']:'yes';

        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';

        $colorTitle = trim($sizeColor->color->name);


        if($always_color == 'yes'){
            if(!$colorTitle || strlen($colorTitle)<= 0 || strpos($colorTitle, self::getNoColorText()) === true ){
                $colorTitle = 'unicolor';
            }
        }

        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';
        $sizeTitle = trim($sizeColor->size->name);

        if($always_size == 'yes'){
            if(!$sizeTitle || strlen($sizeTitle)<= 0 || strpos($sizeTitle, self::getNoColorText()) === true ){
                $sizeTitle = 'unisize';
            }
        }

        // making sure sku generated
        $eans = (property_exists($sizeColor,'eans'))?$sizeColor->eans:'';
        if(is_array($eans))$eans = $eans[0];
        $post_name = (property_exists($sizeColor,'eans'))?$sizeColor->eans.'-'.$ProductId:self::SizeColorSKU($sizeColor,$Product->articleId,$colorTitle,$sizeTitle);

        if($use_eans_as_sku=='yes'){
            $post_name = (strlen($eans)>0)?$eans:self::SizeColorSKU($sizeColor,$Product->articleId,$colorTitle,$sizeTitle);
        }

        if(strlen($post_name)<=0){
            $post_name = $Product->articleId.'-'.$sizeColor->sizeColorId;
        }
        //
        $post_title = self::SizeColorTitle($ProductId,$colorTitle,$sizeTitle);
        $my_post = array(
            'post_title' => $post_title,
            'post_name' => $post_name,
            'post_status' => 'publish',
            'post_parent' => $ProductId,
            'post_type' => 'product_variation',
            'guid' => home_url() . '/?product_variation=' . $post_name
        );
        //WriteLog('myPost: '.print_r($my_post,true));
        //global $wpdb;
        global $current_PCK;

        $kasse_nr = retriveKasseNr();
        //$attID = wc_get_product_id_by_sku($post_name);
        //WriteLog('$attID: '.print_r($attID,true));
        $attID = getVariationIdFromSizeColorId($sizeColor->sizeColorId,$kasse_nr);
        //$attID = sizeColorIdToPostId($sizeColor->sizeColorId, $kasse_nr);

        if(!$attID){
            $attID = wp_insert_post($my_post);
            //WriteLog('Error: '.print_r($attID,true));
            add_post_meta( $attID, '_sizeColorId_'.$kasse_nr, $sizeColor->sizeColorId );
        }

        $_tmp_product = wc_get_product($attID);

        if($_tmp_product && $_tmp_product->get_parent_id() != $ProductId){
            wp_delete_post($attID, true);
            $attID = wp_insert_post($my_post);
            add_post_meta( $attID, '_sizeColorId_'.$kasse_nr, $sizeColor->sizeColorId );
        }

        if(!empty($sizeColor->eans) && $sizeColor->eans > 0){
            update_post_meta($attID, 'm360_pck_eans', $sizeColor->eans);
        }
        $current_pck_stock = self::calculate_multi_pckasser_count($attID,$sizeColor->stockCount);

        update_post_meta( $attID, '_sizeColorId_'.$kasse_nr, $sizeColor->sizeColorId );
        UpdateSizeColorID($attID,$sizeColor->sizeColorId,$kasse_nr);

        update_post_meta($attID, 'm360_pck_hideWhenOutOfStock', $Product->hideWhenOutOfStock);
        $backorder = self::calculateBackorder($ProductId);
        $instock = self::calculateInStock($backorder,$current_pck_stock);
        $manageStock = 'yes';
        if($current_pck_stock<=0 && $backorder == 'yes'){
            $manageStock = 'no';
        }

        self::handlingStockDetails($attID,$sizeColor);
        $wc_product = wc_get_product($attID);

        if(m360_pck_is_default_pck($current_PCK)){
            if($colorTitle && ctype_space($colorTitle))$colorTitle = NULL;
            if($sizeTitle && ctype_space($sizeTitle)) $sizeTitle = NULL;

            if($colorTitle && strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false && $color_attribute != 'no_attribute'){
                $farge_attribute_term = get_term_by('name', $colorTitle, $color_attribute);
                if(is_object($farge_attribute_term) && property_exists($farge_attribute_term,'slug') && $farge_attribute_term->slug){
                    update_post_meta($attID, 'attribute_'.$color_attribute, $farge_attribute_term->slug);
                }
            }
            if($sizeTitle && strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false && $size_attribute != 'no_attribute'){
                $storrelse_attribute_term = get_term_by('name', $sizeTitle, $size_attribute);
                if(is_object($storrelse_attribute_term) && property_exists($storrelse_attribute_term,'slug') && $storrelse_attribute_term->slug){
                    update_post_meta($attID, 'attribute_'.$size_attribute, $storrelse_attribute_term->slug);
                }
            }

            $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );
            if($woocommerce_synk_pris_checkbox == 'yes') {
                self::pricing($attID, $Product);
            }

        }

        if($wc_product){
            try{
                $wc_product->set_sku($post_name);
            } catch (Exception $e) {
                WriteLog("Error inserting product: ". $e->getMessage().' for SKU: '.$post_name);
            }
            if (!$wc_product->is_type('external')){
                $wc_product->set_downloadable('no');
                $wc_product->set_virtual('no');
                $wc_product->set_manage_stock($manageStock);
                $wc_product->set_backorders($backorder);
                $wc_product->set_stock_quantity($current_pck_stock);
                $wc_product->set_stock_status(wc_clean($instock));
                $wc_product->save();
            }

        }
    }

    public static function colorAttribute(){
        $color_attribute = 'no_attribute';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_color_attribute' ] )){
            if($productTabOptions[ 'm360_pck_color_attribute' ]!='no_attribute'){
                $color_attribute = $productTabOptions[ 'm360_pck_color_attribute' ];
            }
        }
        return $color_attribute;
    }

    public static function sizeAttribute(){
        $size_attribute = 'no_attribute';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_size_attribute' ] )){
            if($productTabOptions[ 'm360_pck_size_attribute' ]!='no_attribute'){
                $size_attribute = $productTabOptions[ 'm360_pck_size_attribute' ];
            }
        }
        return $size_attribute;

    }



    private static function createAttribute($ProductId,$colors=NULL,$sizes=NULL){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';

        if (is_array($colors)){
            foreach($colors as $key => $color){
                if(strlen($color)<=0 && $always_color == 'yes'){
                    $colors[$key] = 'unicolor';

                }else if(strlen($color)<=0){
                    unset($color,$colors);
                }
            }
        }
        if (is_array($sizes)){
            foreach($sizes as $key => $size){
                if(strlen($size)<=0 && $always_size == 'yes'){
                    $sizes[$key] = 'unicolor';
                }else if(strlen($size)<=0){
                    unset($size,$sizes);
                }
            }
        }

        if(count($colors)<=0)$colors = NULL;
        if(count($sizes)<=0)$sizes = NULL;


        $color_attribute = self::colorAttribute();
        $size_attribute = self::sizeAttribute();
        if ( function_exists( 'wc_bv_update_post_meta' ) ) {
            wc_bv_update_post_meta( $ProductId, '_bv_type', 'matrix' );
        }

        if(count($colors) && $color_attribute != 'no_attribute'){
            ///wp_set_object_terms($ProductId, $colors, $color_attribute);
            $new_colors = insertTerms($colors, $color_attribute);
            wp_set_object_terms($ProductId, $new_colors, $color_attribute);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $ProductId, '_bv_x', $color_attribute );
            }
        }

        if(count($sizes) && $size_attribute != 'no_attribute'){
            //wp_set_object_terms($ProductId, $sizes, $size_attribute);
            $new_sizes = insertTerms($sizes, $size_attribute);
            wp_set_object_terms($ProductId, $new_sizes, $size_attribute);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $ProductId, '_bv_y', $size_attribute );
            }
        }

        if(count($colors) || count($sizes)){
            $attributes = array();
            if(count($colors) && $color_attribute != 'no_attribute'){
                $attributes[$color_attribute] = array(
                    'name' => $color_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 1
                );
            }

            if(count($sizes) && $size_attribute != 'no_attribute'){
                $attributes[$size_attribute] = array(
                    'name' => $size_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 2
                );

            }
            update_post_meta($ProductId, '_product_attributes', $attributes);

        }
    }

    public static function getNoColorText(){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        /* no color text */
        $noColorTextOption = 'Ingen fargevalg';
        if(!is_wp_error($productTabOptions) && isset($productTabOptions['m360_pck_noColorText'])){
            if(strlen($productTabOptions['m360_pck_noColorText']) >0){
                $noColorTextOption = $productTabOptions['m360_pck_noColorText'];
            }
        }
        return $noColorTextOption;
    }

    public static function getNoSizeText(){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        /* no size text */
        $noSizeTextOption =	'Ingen';
        if(!is_wp_error($productTabOptions) && isset($productTabOptions['m360_pck_noColorText'])){
            if(strlen($productTabOptions['m360_pck_noColorText']) >0){
                $noSizeTextOption = $productTabOptions['m360_pck_noSizeText'];
            }
        }
        return $noSizeTextOption;
    }

    private static function createVariants($ProductId,$sizeColors,$Product){
        $colors = array();
        $sizes = array();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';


        if(is_array($sizeColors)){
            foreach ($sizeColors as $sizeColor){
                if(strlen($sizeColor->color->name)>0 &&
                    strpos($sizeColor->color->name, self::getNoColorText()) === false){
                    if(!in_array(trim($sizeColor->color->name),$colors))$colors[] = trim($sizeColor->color->name);
                }else if($always_color == 'yes'){
                    $colors[] = 'unicolor';
                }

                if(strlen($sizeColor->size->name)>0 &&
                    strpos($sizeColor->size->name, self::getNoSizeText()) === false){
                    if(!in_array(trim($sizeColor->size->name),$sizes))$sizes[] = trim($sizeColor->size->name);
                }else if($always_size == 'yes'){
                    $sizes[] = 'unisize';
                }
            }
        }

        global $current_PCK;
        if(m360_pck_is_default_pck($current_PCK)){
            self::createAttribute($ProductId,$colors,$sizes);
        }

        $WC_product = wc_get_product($ProductId);

        if (!$WC_product->is_type( 'variable' ) ) {
            wp_set_object_terms($ProductId, 'variable', 'product_type');
        }
        if(is_array($sizeColors)){
            foreach ($sizeColors as $sizeColor) {
                self::createVariableProduct($sizeColor,$ProductId,$Product);
            }
        }
    }

    public static function InsertProduct($Product){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK)){
            return $Product->articleNo.' finnes ikke i nettbutikken , og kassen er ikke den hoved kasse';
        }
        try {
            $ProductId = self::getNewProductId($Product);
            if(m360_pck_is_default_pck($current_PCK)){
                M360PCKMissingAttributes::AddM360PCKAttributes($Product,$ProductId);
                self::updatePostMetaForProduct($ProductId,$Product);
            }else{
                self::handling_stock($ProductId,$Product);
            }

            if ($Product->sizeColorInUse) {
                $sizeColors = $Product->sizeColors;
                if(is_object($sizeColors)) {
                    $sizeColors = array($sizeColors);
                }
                self::createVariants($ProductId,$sizeColors,$Product);
            }

            if(m360_pck_is_default_pck($current_PCK)){
                $tax_class = self::handelTaxClass($Product);
                $wc_product = wc_get_product($ProductId);
                $wc_product->set_tax_class($tax_class);
                $wc_product->set_tax_status(self::isTaxtable());
                $wc_product->save();

                self::addManfacturer($ProductId,$Product);

            }

            if(n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')) {
                self::updateMultiStore($ProductId,$Product);
            }
            return $ProductId;
        } catch (Exception $e) {
            return "Error inserting product: ". $e->getMessage();
        }
    }

    public static function updateProductDescription($ProductId,$Product){
        $content_post = get_post($ProductId);
        $content = (is_object($content_post) && property_exists($content_post,'post_content'))?$content_post->post_content:'';
        $excerpt = (is_object($content_post) && property_exists($content_post,'post_excerpt'))?$content_post->post_excerpt:'';

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_description'] )?$productTabOptions[ 'm360_pck_description']:'both_if_empty';

        $ProductStub = array(
            'ID' => $ProductId,
            'post_title' => $Product->name,
            'post_type' => 'product'
        );
        if($saved == 'long_description'){
            $ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description'){
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both'){
            $ProductStub['post_content'] = $Product->description;
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'long_description_if_empty'){
            if(strlen($content)<=0)$ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description_if_empty'){
            if(strlen($excerpt)<=0)$ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both_if_empty'){
            if(strlen($content)<=0)$ProductStub['post_content'] = $Product->description;
            if(strlen($excerpt)<=0)$ProductStub['post_excerpt'] = $Product->description;
        }
        return wp_update_post($ProductStub);

    }

    private static function getExistingProductId($Product){
        $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($Product->articleId);
        $newProductID = self::updateProductDescription($ProductId,$Product);
        return $newProductID;
    }
    public static function getOldManufacturer($ProductId){
        global $wpdb;
        $table_name = m360_pck_get_table_name("postmeta");

        return $wpdb->get_var($wpdb->prepare(
            "SELECT meta_value FROM " . $table_name . " WHERE meta_key = 'm360_pck_manufacturer' AND post_id = '%s' LIMIT 1", $ProductId
        ));
    }

    public static function getDiscountPrice($Product){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_tilbud_pris'] )?$productTabOptions[ 'm360_pck_tilbud_pris']:'discount';

        switch ($saved) {
            case "discount":
                $discount_price = round($Product->discount, 2);
                break;
            case "suggestedPrice":
                $discount_price = round($Product->suggestedPrice, 2);
                break;
            case "alternativePrice":
                $discount_price = round($Product->alternativePrice, 2);
                break;
            case "alternativePrice2":
                $discount_price = round($Product->alternativePrice2, 2);
                break;
            default:
                $discount_price = null;
        }
        return $discount_price;
    }
    public static function CheckThePrice($WC_product,$ProductId,$Product){

        $old_price = (method_exists($WC_product,'get_price'))?$WC_product->get_price():get_post_meta( $ProductId, '_price', true);
        $old_sale_price = (method_exists($WC_product,'get_sale_price'))?$WC_product->get_sale_price():get_post_meta( $ProductId, '_sale_price', true);
        $old_regular_price = (method_exists($WC_product,'get_regular_price'))?$WC_product->get_regular_price():get_post_meta( $ProductId, '_regular_price', true);

        $old_sale_price_dates_from 	= get_post_meta( $ProductId, '_sale_price_dates_from', true );
        $old_sale_price_dates_to 	= get_post_meta( $ProductId, '_sale_price_dates_to', true ) ;

        $date_from = date('Y-m-d H:i:s', $Product->discountFrom/1000);
        $date_to = date('Y-m-d H:i:s', $Product->discountTo/1000);

        $discount_price = self::getDiscountPrice($Product);

        if($old_price != round($Product->salesPrice, 2) ||
            $old_sale_price != $discount_price ||
            $old_regular_price != round($Product->salesPrice, 2)||
            $old_sale_price_dates_from != $date_from ||
            $old_sale_price_dates_to != $date_to){
            $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );

            if($woocommerce_synk_pris_checkbox == 'yes') {
                self::pricing($ProductId, $Product);
            }
        }
    }


    public static function addExtraAttributes($ProductId,$Product){
        if(strlen($Product->productLine->name)>0){
            wp_set_object_terms($ProductId, array($Product->productLine->name), 'product_tag',true);
        }

        //WriteLog(__FUNCTION__.' '.print_r($Product,true));
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $attributes_loop = array(
            'subtitle'=>'undernavn',
            'externalLink'=>'eksterlink',
            'price1'=>'pris1',
            'price2'=>'pris2',
            'price3'=>'pris3',
            'price4'=>'pris4',
            'price5'=>'pris5',
            'productLine'=>'produktserie'
        );

        if (is_array($attributes_loop)){
            foreach ($attributes_loop as $pck_key =>$attribute){
                if (array_key_exists('m360_pck_'.$attribute.'_fields', $productTabOptions)) {
                    if(isset($productTabOptions[ 'm360_pck_'.$attribute.'_fields' ])&& $productTabOptions[ 'm360_pck_'.$attribute.'_fields' ]!= 'no_attribute'){
                        $attibute_field_name = $productTabOptions[ 'm360_pck_'.$attribute.'_fields' ];
                        $attributes = get_post_meta( $ProductId, '_product_attributes',true);

                        if(!taxonomy_exists($attibute_field_name)){
                            WriteLog('$attibute_field_name: '.$attibute_field_name.' not exists');
                            continue;
                        }
                        //WriteLog('$pck_key: '.$pck_key.' value: '.print_r($Product->$pck_key,true));

                        $value = false;
                        if(isset($Product->$pck_key)){
                            if(in_array($pck_key,array('price1','price2','price3','price4','price5'))){
                                if($Product->$pck_key<=0)continue;
                                $vat = 1+($Product->vat/100);
                                $value = $Product->$pck_key/$vat;
                            }else if($pck_key == 'productLine'){
                                $productLine = $Product->$pck_key;
                                $value = $productLine->name;
                            }else{
                                $value = $Product->$pck_key;
                            }
                        }

                        if($value){
                            update_post_meta($ProductId, 'm360_pck_'.$pck_key, str_replace(',', '|', $value));
                            $attibute_field_values = explode(',', $value);
                            wp_add_object_terms($ProductId, $attibute_field_values, $attibute_field_name);
                            $attributes[$attibute_field_name] = array(
                                'name' => $attibute_field_name,
                                'value' => implode('|', $attibute_field_values),
                                'is_visible' => 1,
                                'is_variation' => 0,
                                'is_taxonomy' => 1,
                                'position'  => 4
                            );
                        }else{
                            delete_post_meta($ProductId, 'm360_pck_'.$pck_key);
                        }
                        update_post_meta($ProductId, '_product_attributes', $attributes);

                    }
                }
            }
        }
    }

    public static function addManfacturer($ProductId,$Product){
        $manufacturer = get_post_meta( $ProductId, 'm360_pck_manufacturer',true );// Produsent
        if(!empty($Product->manufacturer->name)){
            wp_set_object_terms($ProductId, array($Product->manufacturer->name), 'product_tag',true);
            $old_manufacturer_name = (isset($manufacturer))?$manufacturer:'';
            $manufacturer_name = (string)$Product->manufacturer->name;

            if($old_manufacturer_name !== $manufacturer_name){
                update_post_meta($ProductId, 'm360_pck_manufacturer', $manufacturer_name);
            }
            $productTabOptions = get_option( 'm360_pck_options_product' );
            if(isset($productTabOptions[ 'm360_pck_produsent_attribute' ] )){
                $man_att = $productTabOptions[ 'm360_pck_produsent_attribute' ];
                if($productTabOptions[ 'm360_pck_produsent_attribute' ]!='no_attribute'){
                    wp_set_object_terms($ProductId, $manufacturer_name, $man_att);
                    $attributes = get_post_meta( $ProductId, '_product_attributes',true );
                    $attributes[$man_att] = array(
                        'name' => $man_att,
                        'value' => '',
                        'is_visible' => 1,
                        'is_taxonomy' => 1,
                        'position'  => 3
                    );
                    update_post_meta($ProductId, '_product_attributes', $attributes);

                    $man_attribute_term = get_term_by('name', $manufacturer_name, $man_att);
                    if($man_attribute_term->slug)update_post_meta($ProductId, 'attribute_'.$man_att, $man_attribute_term->slug);
                }
            }
        }
        self::addExtraAttributes($ProductId,$Product);
    }

    public static function cleanVariations($Product){
        $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($Product->articleId);

        $args = array(
            'post_type'     => 'product_variation',
            'post_status'   => array( 'private', 'publish' ),
            'numberposts'   => -1,
            'post_parent'   => $ProductId
        );
        $variations = get_posts( $args );

        $kasse_nr = retriveKasseNr();

        $sizeColors = $Product->sizeColors;
        if(is_object($sizeColors)) {
            $sizeColors = array($sizeColors);
        }
        $array_of_sizeColorsIds = array();
        if(is_array($sizeColors)){
            foreach($sizeColors as $sizeColor){
                $array_of_sizeColorsIds[] = $sizeColor->sizeColorId;
            }
        }
        if (is_array($variations)){
            foreach($variations as $variation){
                $attID = getVariationIdFromSizeColorId($variation->ID,$kasse_nr);

                if(!in_array($attID,$array_of_sizeColorsIds)){
                    wp_delete_post($variation->ID, true);
                }
            }
        }

    }

    private static function DeleteChildFromSizeColorIdsTable($post_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");
        $query = "DELETE FROM {$table_name} WHERE post_id = {$post_id}";
        $wpdb->query($query);
    }


    public static function changeProductType($wc_product,$product_id,$Product){

        $using_frgstr = ($Product->sizeColorInUse)?true:false;
        $type = $wc_product->get_type();
        if($using_frgstr && is_object($wc_product) && $type != 'variable'){
            $type = 'variable';
        }

        if(!$using_frgstr && is_object($wc_product) && $type != 'simple'){
            if( $wc_product->has_child() ){
                $childrens = $wc_product->get_children( );
                if(is_array($childrens)){
                    foreach($childrens as $child_id){
                        wp_delete_post($child_id, true);
                        self::DeleteChildFromSizeColorIdsTable($child_id);
                    }
                }
            }
            WC_Product_Variable::sync($product_id);
            $type = 'simple';
        }

        wp_set_object_terms($product_id, $type, 'product_type');
        $new_wc_product = wc_get_product($product_id);
        //WriteLog('new type: '.$new_wc_product->get_type());
        return $new_wc_product;
    }
    public static function UpdateProduct($Product){
        global $current_PCK;
        $lockedProductTabOptions = get_option( 'm360_pck_options_locked_products' );
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $visibleOnWeb_settings = isset($productTabOptions[ 'm360_pck_not_visibleOnWeb'] )?$productTabOptions[ 'm360_pck_not_visibleOnWeb']:'private';

        if(m360_pck_is_default_pck($current_PCK)){
            if(isset( $lockedProductTabOptions [ 'm360_pck_locked_products_skus' ])){
                $lockedProductsSkus = explode(',',$lockedProductTabOptions [ 'm360_pck_locked_products_skus' ]);
                if(in_array($Product->articleNo,array_map('trim', $lockedProductsSkus))){
                    //WriteLog(trim($Product->articleNo).' is a locked product');
                    return true;
                }
            }
        }


        if(!m360_pck_is_default_pck($current_PCK)){
            $ProductId = SKUTOPOSTID($Product->articleNo);
        }else{
            $ProductId = self::getExistingProductId($Product);
        }

        WriteLog(__FUNCTION__.' '.print_r($ProductId,true),true);
        if($ProductId<=0){
            $ProductId = wc_get_product_id_by_sku($Product->articleNo);
        }

        $WC_product = wc_get_product( $ProductId );
        if(!$Product->visibleOnWeb ){
            if($WC_product){
                if($visibleOnWeb_settings == 'delete'){
                    return $WC_product->delete(true);
                }else{
                    wp_update_post( array( 'ID' => $WC_product->get_id(), 'post_status' => 'private' ));
                    return true;
                }
            }
            return true;
        }
        if(!$WC_product){
            self::InsertProduct($Product);
            return true;
        }
        if(isset( $lockedProductTabOptions [ 'm360_pck_locked_products_type_skus' ])){
            $lockedProductTypeSkus = explode(',',$lockedProductTabOptions [ 'm360_pck_locked_products_type_skus' ]);
            if(!in_array($Product->articleNo,array_map('trim', $lockedProductTypeSkus))){
                WriteLog(trim($Product->articleNo).' is a not locked against product type change',true);
                $WC_product = self::changeProductType($WC_product,$ProductId,$Product);
            }
        }

        try {
            if(m360_pck_is_default_pck($current_PCK)){
                self::CheckThePrice($WC_product,$ProductId,$Product);
                M360PCKMissingAttributes::AddM360PCKAttributes($Product,$ProductId);
                $feature = ($Product->recommendedProduct)?'yes':'no';
                //update_post_meta($ProductId, '_featured', $feature);

                if(method_exists($WC_product,'set_featured')){
                    $WC_product->set_featured($feature);
                    $WC_product->save();
                }else{
                    update_post_meta($ProductId, '_featured', $feature);
                }


                self::CheckTheGroups($ProductId,$Product);
                self::updatePostMetaForProduct($ProductId,$Product);
            }

            self::handling_stock($ProductId,$Product);

            if ($Product->sizeColorInUse){
                $sizeColors = $Product->sizeColors;
                if(is_object($sizeColors)) {
                    $sizeColors = array($sizeColors);
                }

                $missingColors = array();
                $missingSizes = array();
                $missingColorsSizes = array();
                $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
                $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';

                if(is_array($sizeColors)){
                    foreach($sizeColors as $sizeColor){

                        $colorTitle = trim($sizeColor->color->name);
                        $sizeTitle = trim($sizeColor->size->name);
                        if(strlen($colorTitle)>0  && strpos($colorTitle, self::getNoColorText()) === false){
                            if(!in_array($colorTitle,$missingColors))$missingColors[] = $colorTitle;
                        }else if($always_color == 'yes'){
                            $missingColors[] = 'unicolor';
                        }

                        if(strlen($sizeTitle)>0  && strpos($sizeTitle, self::getNoSizeText()) === false){
                            if(!in_array($sizeTitle,$missingSizes))$missingSizes[] = $sizeTitle;
                        }else if($always_size == 'yes'){
                            $missingSizes[] = 'unisize';
                        }

                        if((strlen($sizeColor->color->name)>0  && strpos($sizeColor->color->name, self::getNoColorText()) === false)
                            ||(strlen($sizeColor->size->name)>0  && strpos($sizeColor->size->name, self::getNoSizeText()) === false)){
                            $missingColorsSizes[] = $sizeColor;
                        }else if($always_color == 'yes' || $always_size == 'yes'){
                            $missingColorsSizes[] = $sizeColor;
                        }
                    }
                }

                if(m360_pck_is_default_pck($current_PCK)){
                    self::createAttribute($ProductId,$missingColors,$missingSizes);
                    if(count($missingColorsSizes)){
                        foreach ($missingColorsSizes as $missingColorSize){
                            //WriteLog('Missing: '.print_r($missingColorSize,true).' ,product id: '.print_r($ProductId,true));
                            self::createVariableProduct($missingColorSize,$ProductId,$Product);
                        }
                    }
                }

            }

            if(m360_pck_is_default_pck($current_PCK)){
                self::addManfacturer($ProductId,$Product);
                if(n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')) {
                    self::updateMultiStore($ProductId,$Product);
                }
            }

            self::handlingWarehousesOptions($ProductId);
            return true;

        } catch (Exception $ex) {
            WriteLog('Caught exception in '.__FUNCTION__.': ',  $ex->getMessage());
            return 'Error '.$ex->getMessage();
        }
    }



    public static function UpdateCompanyLogo($data){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        $Picture = $data->image;
        //$extension = substr($Picture, 1, 3);
		$extension = 'jpg';
        $name = 'company_logo.'.strtolower($extension);
        $post_titel = get_bloginfo('name');

        $post_content = get_bloginfo('description');
        $attachment_id = pck_image::save_image_to_media_library($name,$Picture,$post_titel,$post_content);
        if($attachment_id && $attachment_id >0){
            return true;
        }else{
            return 'feil ve oppdatere firmalogo';
        }

    }



    public static function UpdateProductInfo($ProductArticleId, $Picture,$samling = false,$variant_parent = false,$imageId=false){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        $ProductId = ($samling)?$ProductArticleId:self::GetWOOCOMMProductIdFromPCKArticleId($ProductArticleId);
        $kasse_nr = retriveKasseNr();

        if(!$ProductId)$ProductId = getVariationIdFromSizeColorId($ProductArticleId,$kasse_nr);
        $FileName_uten_ext = ($variant_parent && $variant_parent >0) ? $ProductArticleId ."_".$variant_parent."_".$imageId : $ProductArticleId;

        //$extension = substr($Picture, 1, 3);
		$extension = 'jpg';
        $name = $FileName_uten_ext.'.'.strtolower($extension);
        $post_titel = $FileName_uten_ext;
        $attachment_id = pck_image::save_image_to_media_library($name,$Picture,$post_titel,'');
        if($attachment_id && $attachment_id >0){
            $product = wc_get_product($ProductId);
            if($product){
                $product->set_image_id($attachment_id);
                $product_id = $product->save();
                if(!is_integer($product_id)){
                    WriteLog('Faild to attach the image to the product');
                    return false;
                }else{
                    $productTabOptions = get_option( 'm360_pck_options_product' );
                    $saved = isset($productTabOptions[ 'm360_pck_alt_text_from_produkt_name'] )?$productTabOptions[ 'm360_pck_alt_text_from_produkt_name']:'yes_if_empty';

                    if($saved == 'yes'){
                        update_post_meta($attachment_id, '_wp_attachment_image_alt', $product->get_title());
                    }else if($saved == 'yes_if_empty'){
                        $old_altext = get_post_meta( $attachment_id, '_wp_attachment_image_alt',true);
                        if($product && empty($old_altext)){
                            update_post_meta($attachment_id, '_wp_attachment_image_alt', $product->get_title());
                        }
                    }
                }
            }
        }else{
            return 'feil ve koble bilde til: '.$ProductArticleId;
        }
    }

    public static function removeProductImage($ProductArticleId){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($ProductArticleId);
        if(has_post_thumbnail( $ProductId )){
            $attachment_id = get_post_thumbnail_id( $ProductId );
            $post = wp_delete_attachment($attachment_id, true);
            if(!is_object($post)){
                return 'Feil ved sletting av bilde for :'.$ProductArticleId;
            }
        }
    }

    public static function calculateInStock($backorder,$qty){
        $instock = 'instock';

        if($backorder == 'no' && $qty<=0){
            $instock = 'outofstock';
        }

        return $instock;
    }
    public static function UpdateProductStockLevel($updateStock){
        global $current_PCK;
        $kasse_nr = retriveKasseNr();
        /* new extra check 07.06.2018*/
        if($updateStock->sizeColorId > 0){
            $ProductId = getVariationIdFromSizeColorId($updateStock->sizeColorId,$kasse_nr);
        }else{
            $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($updateStock->articleId);
        }
        /* extra check end */


        if($ProductId <= 0){
            return 'Vi finner ikke varen med pck id '.$updateStock->articleId.' i nettbutikken, sjekk hvis den synlig på web og aktiv';
        }else{
            $qty = self::calculate_multi_pckasser_count($ProductId,$updateStock->count);
            $backorder = self::calculateBackorder($ProductId);
            $instock = self::calculateInStock($backorder,$qty);
            $manageStock = 'yes';

            /*
            $to_log = array(27661,36869,36870,36871,36872,36873);
            if(in_array($ProductId,$to_log)){
                WriteLog(__FUNCTION__);
                WriteLog('qty: '.$qty.' for product id: '.$ProductId);
            }
            */
            if($qty<=0 && $backorder == 'yes'){
                $manageStock = 'no';
            }
            self::handlingStockDetails($ProductId,$updateStock);
            try{
                $wc_product = wc_get_product($ProductId);
                if($wc_product && !$wc_product->is_type('external')){
                    $wc_product->set_manage_stock($manageStock);
                    $wc_product->set_backorders($backorder);
                    $wc_product->set_stock_quantity($qty);
                    $wc_product->set_stock_status(wc_clean($instock));
                    $wc_product->save();
                }

                return true;
            }catch(Exception $ex){
                return 'UpdateProductStockLevel '.$ex->getMessage();
            }
        }
    }

    public static function DeleteProduct($product_info){
        // get the WOOCOMM product id by the PCK SKU
        $ProductId = self::GetWooCommProductIdFromPCKSKU($product_info->articleId);
        $WC_product = wc_get_product( $ProductId );
        return $WC_product->delete(true);
    }

    public static function UpdateExtraImages($articleId,$imageId, $Picture,$samling=false,$colorid = false){

        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        if($colorid && $colorid >0){
            return self::UpdateProductInfo($colorid, $Picture,$samling,$articleId,$imageId);
        }


        $ProductId = ($samling)?$articleId:self::GetWOOCOMMProductIdFromPCKArticleId($articleId);
        $WC_product = wc_get_product( $ProductId );

        //$extension = substr($Picture, 1, 3);
		$extension = 'jpg';
        $FileName_uten_ext = $articleId .'_'.$imageId;

        $image_id = pck_image::save_image_to_media_library($FileName_uten_ext.'.'.$extension,$Picture,$WC_product->get_name());
        if($image_id && $image_id >0){
            $old_images = get_post_meta( $ProductId, '_product_image_gallery' );
            if(!in_array($image_id,$old_images))array_push($old_images, $image_id);

            $productTabOptions = get_option( 'm360_pck_options_product' );
            $saved = isset($productTabOptions[ 'm360_pck_alt_text_from_produkt_name'] )?$productTabOptions[ 'm360_pck_alt_text_from_produkt_name']:'yes_if_empty';

            if($saved == 'yes'){
                if($WC_product)update_post_meta($image_id, '_wp_attachment_image_alt', $WC_product->get_title());
            }else if($saved == 'yes_if_empty'){
                $old_altext = get_post_meta( $image_id, '_wp_attachment_image_alt',true);

                if(empty($old_altext)){
                    if($WC_product)update_post_meta($image_id, '_wp_attachment_image_alt', $WC_product->get_title());
                }
            }

            update_post_meta( $ProductId, '_product_image_gallery', implode(',',$old_images));
        }else{
            return 'feil ve koble bilde til: '.$articleId;
        }
    }
}

