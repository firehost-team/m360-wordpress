<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class M360PCKMissingAttributes{
	const Normal 					= 0;
    const CallToPurchase			= 1;
	const CallForPrice 				= 2;
	const Kampanjevare 				= 3;
	
	const ArticleStatusAktiv 		= 0;
	const ArticleStatusPassive 		= 1;
	const ArticleStatusExpired 		= 2;
	const ArticleStatusBlocked 		= 3;
	
	const Not_specified 				= 0;
	const Cannot_be_shipped 			= 3;
	const Freight_per_article 		= 5;
	
	public static function GetShippingType($id){
		$string = '';
		switch ($id) {
			case self::Not_specified:
				$string = 'Not specified';
				break;
			case self::Cannot_be_shipped:
				$string = 'Cannot be shipped';
				break;
			case self::Freight_per_article:
				$string = 'Freight per article';
				break;
			default:
				$string = 'Not specified';
		}
		return $string;
	}
	
	public static function GetArticleWebAction($id){
		$string = '';
		switch ($id) {
			case self::Normal:
				$string = 'Normal';
				break;
			case self::CallToPurchase:
				$string = 'Ring for bestilling';
				break;
			case self::CallForPrice:
				$string = 'Ring for pris';
				break;
			case self::Kampanjevare:
				$string = 'Kampanjevare';
				break;
			default:
				$string = 'Normal';
		}
		return $string;
	}

	public static function GetArticleStatus($id){
		$string = '';
		switch ($id) {
			case self::ArticleStatusAktiv:
				$string = 'Aktiv';
				break;
			case self::ArticleStatusPassive:
				$string = 'Passive';
				break;
			case self::ArticleStatusExpired:
				$string = 'Expired';
				break;
			case self::ArticleStatusBlocked:
				$string = 'Blocked';
				break;
			default:
				$string = '';
		}
		return $string;
	}
	
	public static function AddM360PCKAttributes($Product,$ProductId){

		
		update_post_meta($ProductId, 'm360_pck_hideWhenOutOfStock', $Product->hideWhenOutOfStock);
		
		$articleStatus_meta = get_post_meta( $ProductId, 'm360_pck_articleStatus',true ); //Varestatus
		if(!empty($Product->articleStatus)||$Product->articleStatus == 0){
			$old_articleStatus = (isset($articleStatus_meta))?$articleStatus_meta:self::ArticleStatusAktiv;
			$articleStatus = $Product->articleStatus;

			if($articleStatus !== $old_articleStatus){
				update_post_meta($ProductId, 'm360_pck_costPrice', $articleStatus);
			}
		}
		
		$costPrice_meta = get_post_meta( $ProductId, 'm360_pck_costPrice',true ); //Kjøppris
		if(!empty($Product->costPrice) || $Product->costPrice == 0){
			$old_costPrice = (isset($costPrice_meta))?$costPrice_meta:0;
			$costPrice = $Product->costPrice;

			if($costPrice !== $old_costPrice){
				update_post_meta($ProductId, 'm360_pck_costPrice', $costPrice);
			}
		}
		
		$alternativePrice_meta = get_post_meta( $ProductId, 'm360_pck_alternativePrice',true );// Alt Utpris
		if(!empty($Product->alternativePrice) ||$Product->alternativePrice == 0){
			$old_alternativePrice = (isset($alternativePrice_meta))?$alternativePrice_meta:0;
			$alternativePrice = $Product->alternativePrice;

			if($alternativePrice !== $old_alternativePrice){
				update_post_meta($ProductId, 'm360_pck_alternativePrice', $alternativePrice);
			}
		}
		
		$alternativePrice2_meta = get_post_meta( $ProductId, 'm360_pck_alternativePrice2',true ); //Takeaway pris
		if(!empty($Product->alternativePrice2) || $Product->alternativePrice2 == 0){
			$old_alternativePrice2 = (isset($alternativePrice2_meta))?$alternativePrice2_meta:0;
			$alternativePrice2 = $Product->alternativePrice2;

			if($alternativePrice2 !== $old_alternativePrice2){
				update_post_meta($ProductId, 'm360_pck_alternativePrice2', $alternativePrice2);
			}
		}
		
		$articleWebAction_meta = get_post_meta( $ProductId, 'm360_pck_articleWebAction',true ); //Web Kjøpstype
		if(!empty($Product->articleWebAction) || $Product->articleWebAction == self::Normal){
			$old_articleWebAction = (isset($articleWebAction_meta))?$articleWebAction_meta:self::Normal;
			$articleWebAction = $Product->articleWebAction;

			if($articleWebAction != $old_articleWebAction){
				update_post_meta($ProductId, 'm360_pck_articleWebAction', $articleWebAction);
				//WriteLog('articleWebAction '.$articleWebAction);
			}
		}
		
		update_post_meta($ProductId, 'm360_pck_confirmedDelivery', $Product->confirmedDelivery);//confirmedDelivery
		
		$eans_meta = get_post_meta( $ProductId, 'm360_pck_eans',true ); //EANS
		if(!empty($Product->eans)){
			$old_eans = (isset($eans_meta))?$eans_meta:'';
			$eans = $Product->eans;

			if($eans !== $old_eans){
				update_post_meta($ProductId, 'm360_pck_eans', $eans);
				//WriteLog('eans '.$eans);
			}
		}
		
		$expectedDeliveryAmount_meta = get_post_meta( $ProductId, 'm360_pck_expectedDeliveryAmount',true ); //Expected Delivery Amount
		if(!empty($Product->expectedDeliveryAmount) || $Product->expectedDeliveryAmount == 0){
			$old_expectedDeliveryAmount = (isset($expectedDeliveryAmount_meta))?$expectedDeliveryAmount_meta:0;
			$expectedDeliveryAmount = $Product->expectedDeliveryAmount;

			if($expectedDeliveryAmount !== $old_expectedDeliveryAmount){
				update_post_meta($ProductId, 'm360_pck_expectedDeliveryAmount', $expectedDeliveryAmount);
				//WriteLog('expectedDeliveryAmount '.$expectedDeliveryAmount);
			}
		}
		
		$expectedDeliveryDate_meta = get_post_meta( $ProductId, 'm360_pck_expectedDeliveryDate',true ); //Expected Delivery Date
		if(!empty($Product->expectedDeliveryDate)){
			$old_expectedDeliveryDate = (isset($expectedDeliveryDate_meta))?$expectedDeliveryDate_meta:'';
			$expectedDeliveryDate = $Product->expectedDeliveryDate;

			if($expectedDeliveryDate !== $old_expectedDeliveryDate){
				update_post_meta($ProductId, 'm360_pck_expectedDeliveryDate', $expectedDeliveryDate);
				//WriteLog('expectedDeliveryDate '.$expectedDeliveryDate);
			}
		}
		
		$externalGroupId_meta = get_post_meta( $ProductId, 'm360_pck_externalGroupId',true ); //Pckasse ArticleGroupID used by SendDiscount.categoryID
		if(!empty($Product->externalGroupId) || $Product->externalGroupId == 0){
			$old_externalGroupId = (isset($externalGroupId_meta))?$externalGroupId_meta:-1;
			$externalGroupId = $Product->externalGroupId;

			if($externalGroupId != $old_externalGroupId){
				update_post_meta($ProductId, 'm360_pck_externalGroupId', $externalGroupId);
				//WriteLog('externalGroupId '.$externalGroupId);
			}
		}
		
		$externalLink_meta = get_post_meta( $ProductId, 'm360_pck_externalLink',true ); //Extern Link
		if(!empty($Product->externalLink)){
			$old_externalLink = (isset($externalLink_meta))?$externalLink_meta:'';
			$externalLink = $Product->externalLink;

			if($externalLink !== $old_externalLink){
				update_post_meta($ProductId, 'm360_pck_externalLink', $externalLink);
				//WriteLog('externalLink '.$externalLink);
			}
		}
		
		update_post_meta($ProductId, 'm360_pck_noDiscount', $Product->noDiscount); //Order discount field should not be used.
		
		update_post_meta($ProductId, 'm360_pck_nonStockItem', $Product->nonStockItem); //This article in external stock (Use special text if zero stock)
		
		$nonStockItemDays_meta = get_post_meta( $ProductId, 'm360_pck_nonStockItemDays',true ); //Days to get the article from external stock
		if(!empty($Product->nonStockItemDays) || $Product->nonStockItemDays == 0){
			$old_nonStockItemDays = (isset($nonStockItemDays_meta))?$nonStockItemDays_meta:0;
			$nonStockItemDays = $Product->nonStockItemDays;

			if($nonStockItemDays !== $old_nonStockItemDays){
				update_post_meta($ProductId, 'm360_pck_nonStockItemDays', $nonStockItemDays);
				//WriteLog('nonStockItemDays '.$nonStockItemDays);
			}
		}
		
		$shippingType_meta = get_post_meta( $ProductId, 'm360_pck_shippingType',true ); //Sendingstype
		if(!empty($Product->shippingType) || $Product->shippingType == 0){
			$old_shippingType = (isset($shippingType_meta))?$shippingType_meta:self::Not_specified;
			$shippingType = $Product->shippingType;

			if($shippingType !== $old_shippingType){
				update_post_meta($ProductId, 'm360_pck_shippingType', $shippingType);
				//WriteLog('shippingType '.$shippingType);
			}
		}


        if(!empty($Product->eans) && strlen($Product->eans)>0){
            update_post_meta($ProductId, 'm360_pck_eans', $Product->eans);
        }

	}
	
	
}
