<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKLoggingTap extends M360_PCKasseWS_Settings{

	public function __construct(){
		
	}
	
	public function init_logging() {
		
		// register Logging options group 
		register_setting(
            'm360_pck_options_logging_group', // group
            'm360_pck_options_logging', // name
            array( $this, 'sanitise' ) // sanitise method
        );
		
		// Logging Options
		add_settings_section(
            'm360_pck_options_section_logging', // id
            'M360 PCKasseWS Logging Options', // title
            array( $this, 'settings_html_m360_pck_logging_message' ), // callback
            'm360_pck_options_page_logging' // page
        );
		
		// m360_pck_LOGGING_ENABLED
		add_settings_field(
            'm360_pck_logging_enabled', // id
            'Enable Logging', // title
            array( $this, 'setting_html_m360_pck_enable_logging' ), // callback
            'm360_pck_options_page_logging', // page
            'm360_pck_options_section_logging' // section
        );
		
		// m360_pck_DEBUG_ENABLED
        add_settings_field(
            'm360_pck_logging_debugging',
            'Enable Debugging',
            array( $this, 'setting_html_m360_pck_enable_debugging' ),
            'm360_pck_options_page_logging',
            'm360_pck_options_section_logging'
        );
		
	}
	
    /** 
     * HTML to display for the Logging section
     */
    public function settings_html_m360_pck_logging_message() {
		
        print 'Configure how you want M36 PCKasseWS to report on its activities and possible errors...';
		
    }
	
    /** 
     * HTML to display the m360_pck_LOGGING_ENABLED config option
     */
    public function setting_html_m360_pck_enable_logging() {
		$loggingOptions = get_option( 'm360_pck_options_logging' );
		
		m360_pck_tooltip('6070',
			"Do you wish to save logging information?&lt;br /&gt;&lt;br /&gt;
			Logging information will be saved here on your web server if this option is enabled:&lt;br /&gt;&lt;br /&gt;
			&lt;code&gt;" . plugin_dir_url( __FILE__ ) . "logs/m360_pck_log.txt&lt;/code&gt;", 
			'm360_pcklogging'
		);
		
		printf (
			'Yes <input type="radio" id="m360_pck_logging_enabled" name="m360_pck_options_logging[m360_pck_logging_enabled]" value="true" %s/>', 
			isset( $loggingOptions[ 'm360_pck_logging_enabled' ] ) && 
			$loggingOptions[ 'm360_pck_logging_enabled' ] == 'true' ? 'checked="checked" ' : ''
		);
		
		printf (
			'No <input type="radio" id="m360_pck_logging_enabled" name="m360_pck_options_logging[m360_pck_logging_enabled]" value="false" %s/>', 
			isset( $loggingOptions[ 'm360_pck_logging_enabled' ] ) &&
			$loggingOptions[ 'm360_pck_logging_enabled' ] == 'true' ? '' : 'checked="checked" '
		);
		
    }
	
    /** 
     * HTML to display the m360_pck_DEBUG_ENABLED config option
     */
    public function setting_html_m360_pck_enable_debugging() {
		$loggingOptions = get_option( 'm360_pck_options_logging' );
		
		m360_pck_tooltip('6070',
			"Do you wish to save debugging information?&lt;br /&gt;&lt;br /&gt;
			Debugging information will be saved here on your web server if this option is enabled:&lt;br /&gt;&lt;br /&gt;
			&lt;code&gt;" . plugin_dir_url( __FILE__ ) . "logs/m360_pck_debug.txt&lt;/code&gt;", 
			'm360_pckdebugging'
		);
		
		printf(
			'Yes <input type="radio" id="m360_pck_logging_debugging" name="m360_pck_options_logging[m360_pck_logging_debugging]" value="true" %s/>', 
			isset( $loggingOptions[ 'm360_pck_logging_debugging' ] ) && 
			$loggingOptions[ 'm360_pck_logging_debugging' ] == 'true' ? 'checked="checked" ' : ''
		);
		
		print '&nbsp;&nbsp;&nbsp;';
		
		printf(
			'No <input type="radio" id="m360_pck_logging_debugging" name="m360_pck_options_logging[m360_pck_logging_debugging]" value="false" %s/>', 
			isset( $loggingOptions[ 'm360_pck_logging_debugging' ] ) &&
			$loggingOptions[ 'm360_pck_logging_debugging' ] == 'true' ? '' : 'checked="checked" '
		);
        //M360EmptySizeColorIds();
    }
}
