== Changelog ==
== 6.623.33 ==
bruker order meta info for kredit

== 6.623.32 ==
* added a new orderstatus for ordre som mangler kredit godekjenelse

== 6.23.31 ==
* included Woocoomerce custome fields

== 6.623.29 ==
* implementer Patrik code to Purge varnish
* fixing categories after if its empty after it wasnt empty
* checking taxes rates


== 6.623.26 ==
* adding extra attributter - produkt serie og pris 1 til 5

== 6.623.22 ==
* new way to create and update products , and new way to sync and update orders , may not be sutibale for multisites webstore.

== 6.623.18 ==
* fixing slett rest linjer


== 6.623.13 ==
* added woocomemrce template and custome templates/woocommerce/single-product/price.php


== 6.623.12 ==
* pass på antall depositum

== 6.623.11 ==
* Sørge at Depositum vare er altid på lager

== 6.623.1 ==
* added deposits function to the system


== 6.623 ==
* changed tooltip to m360_pck_tooltip to prevent conflict
* removed unused classes and files
* added function to show or not show the manufacturer column in products table
* added function to send/not an email to the new customers created by PCKasse in addition to the standared wordpres email.
* checking/fix some known security holes.

== 6.617 ==
Fixing sending faild credit orders again
Testet customer syncing.

== 6.607 ==
More control over warehouses

== 6.606 ==
fixing dimentions sysncing , adding get order info url , get order recipt , get article url

== 6.605 ==
mulighet for å velge status på hentede kredittordre

== 6.603 ==
taking care of customer syncing and extra product images

== 6.48 ==
taking care of the credit order in some webstores like eButikken.

== 6.44 ==
New image method using Imagick
== 6.2 ==
Make sure to update thumbnail image

== 6.1 ==
if found extra image connected to variable product will send to nettbutikk as thumbnail to the variant in the nettbutikk else it will come to nettbutikk as gallery.
if found - eans - in the sizeColor object it will use as sku , else it will generate an sku from articleNr-colorId-sizeId-$sizeColorId

== 5.0 ==
* Adding skaffevare valg

== 4.9 ==
* Adding Credit order option 

== 4.7 ==
* make sure to register the new orders into the database
== 4.6 ==
* added PCK eans to product

== 4.5 ==
* automatic update woo commerce order status when complete, cancel or refund.

== 4.3 ==
* Fixing and enhacment.

== 4.2 ==
* Fixing and enhacment.

== 4.1 ==
* Adding ability to merge a simple products into variable product.

== 4.0 ==
* Pricing option in product general tap

== 3.9 ==
* more enhancmenet for webjournal and returener og synking av ordre.
== 3.8 ==
* rebuild the credit and update order status from scratch again.
* added round down setting 
* added allow delete the product from PCKasse

== 3.7 ==
* Fix an issue when there is just one variation.

== 3.6 ==
* adding ability to change the attribute label for specifier product.

== 3.5 ==
* change the way the product variations created.
* adding some extra functions in product setting in M360 PCKasseWS product settings 

== 3.4 ==
* add choice for description field which if the user want to save it in short, long, or both description or if the user do not want to use PCKasse description.

== 3.3 ==
* adding ability to choose the attribute for color, size, undernavn and manufacturer 
* fix som bugs
* maintain the code 

== 3.2 ==
* Generate product image alt text from product name , and adding setting in m360 pckasse product tab to allow or disallow it.

= 2.5.72 =
* Taking care of some error situations when there is extra space in product attributes.
* Adding function to auto select first variable product.


= 2.5.71b =
Fixing issues with some themes and categories syncing. ref: rattogsanselig.


= 2.5.7b =
* Added a new tab in M360 Extra function to control order status, you can use this tool to fix some issues when the new orders not sendt to PCK
 
= 2.5.6b =
* Fix some issues with webjournal, please make sure that you dont have any orders in Aktive webordre in PCK before you install this one.