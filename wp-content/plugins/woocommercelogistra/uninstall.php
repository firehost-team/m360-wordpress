<?php

//if uninstall not called from WordPress, exit
if (!defined('WP_UNINSTALL_PLUGIN')) {
    exit();
}

// For Single site
if (!is_multisite()) {

        //Drop tables whene plugin in delete
        global $wpdb;
        
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
        $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
        $shippingPackages = $wpdb->prefix . 'LogistraTAShippingPackages';
        
        $sql  = "DROP Table IF EXISTS ".$shippingMethods;
        $sql2 = "DROP Table IF EXISTS ".$shippingOptions;
        $sql3 = "DROP Table IF EXISTS ".$shippingPackages;
         
        $wpdb->query($sql);
        $wpdb->query($sql2);
        $wpdb->query($sql3);
        
    
} else {
    // For Multisite
}