jQuery(document).on('click', '#license_check', function (e) {
    e.preventDefault();
    jQuery('.loader_img').show();
    jQuery.ajax({
        type: 'POST',
        url: licenseAjax.ajaxurl,
        data: {
            action: 'logistra_license_check',
            post_data: jQuery('form.logistra_license_form').serialize(),
        },
        success: function (response) {

            
             jQuery('.loader_img').hide();
            location.reload();
             

             

        }
    });
});