
jQuery(function () {
    if (value = jQuery("input[name='shipping_method[0]']:checked").attr('value')) {
        id = jQuery("input[name='shipping_method[0]']:checked").attr('id');
        if (logistra.is_checkout_page == '0' && jQuery.inArray(value, logistra.logistra_shipping_method) !== -1) {
            _get_shipping_method(id, value);
        }
    }
    function _get_shipping_method(id, value, selectedsingle) {
        jQuery('div.cart_totals,#order_review').block({message: null, overlayCSS: {background: '#fff url(' + logistra.ajax_loader_url + ') no-repeat center', backgroundSize: '16px 16px', opacity: 0.6}});
        dataId = id;
        jQuery.ajax({
            type: 'POST',
            url: logistra.ajax_url,
            dataType: 'json',
            data: {
                action: 'get_shipping_methods',
                id: value,
                post_data: jQuery('form.checkout').serialize(),
                value: value
            },
            success: function (data) {

                if (jQuery.trim(data.listpart)) {
                    jQuery('#submethod').remove();

                    if (selectedsingle)
                    {
                        jQuery('#' + id).parents('tr.shipping').find('td').append(data.listpart);
                        jQuery('#submethod').css("margin-left", "40px");
                    } else
                        jQuery('#' + id).next().after(data.listpart);
                }

                if (jQuery.trim(data.err)) {
                    if (jQuery("form.checkout").prev().hasClass("sub_shipping_error")) {
                        jQuery("form.checkout").prev().remove();
                        jQuery("form.checkout").before("<div class='sub_shipping_error'>" + data.err + "</div>")
                    } else {
                        jQuery("form.checkout").before("<div class='sub_shipping_error'>" + data.err + "</div>")
                    }
                } else {
                    jQuery(".sub_shipping_error").remove();
                }

                jQuery('div.cart_totals,#order_review').unblock();
            }
        });
    }

    jQuery(document).on('change', 'select[name=sub_shipping]', function () {
        jQuery(".shop_table .blockOverlay").css("position", "static");
        var sel_service = jQuery('#submethod').val();

        Cookies.set("selected_sub_shipping_method", sel_service, {path: '/'});
        Cookies.set("servicePartnerID", '0', {path: '/'});
    })

    jQuery('body').on('updated_checkout', function (event) {
        value = jQuery("input[name='shipping_method[0]']:checked").attr('value');
        id = jQuery("input[name='shipping_method[0]']:checked").attr('id');
        // alert("cookie not set");
        value_arr = value.split(':');
        value = value_arr[0];
        console.log(value);
        if (value && id)
        {
            selectedsingle = false;
            Cookies.set("sub_shipping_method", value, {path: '/'});
        } else
        {
            value = jQuery("input[name='shipping_method[0]']").attr('value');
            value_arr = value.split(':');
            value = value_arr[0];

            id = jQuery("input[name='shipping_method[0]']").attr('id');
            Cookies.set("sub_shipping_method", value, {path: '/'});
            selectedsingle = true;
        }
        console.log(value);

        if (Cookies.set("selected_sub_shipping_method") == null) {
            Cookies.set("selected_sub_shipping_method", '0', {path: '/'});
            Cookies.set("servicePartnerID", '0', {path: '/'});

        }


        if (jQuery.inArray(value, logistra.logistra_shipping_method) !== -1) {
            _get_shipping_method(id, value, selectedsingle);
            call_update_checkout = 1;
        }


    });

    jQuery(document).on('click', 'button[name="calc_shipping"]', function () {
        jQuery(document.body).trigger('updated_shipping_method');
    });

    jQuery(document).on('updated_shipping_method', function () {
        id = jQuery("input[name='shipping_method[0]']:checked").attr('id');
        value = jQuery("input[name='shipping_method[0]']:checked").attr('value');
        if (!value) {
            value = jQuery("input[name='shipping_method[0]']").attr('value');
        }
        value_arr = value.split(':');
        value = value_arr[0];
        jQuery.ajax({
            type: 'POST',
            url: logistra.ajax_url,
            dataType: 'json',
            data: {
                action: 'get_shipping_methods_cart',
                country: jQuery("#calc_shipping_country").val(),
                postcode: jQuery("#calc_shipping_postcode").val(),
                value: value
            },
            success: function (data) {

                if (jQuery.trim(data.listpart)) {
                    jQuery('#submethod').remove();

                    if (selectedsingle)
                    {
                        jQuery('#' + id).parents('tr.shipping').find('td').append(data.listpart);
                        jQuery('#submethod').css("margin-left", "40px");
                    } else
                        jQuery('#' + id).next().after(data.listpart);
                }
            }
        });
    });

//    jQuery(document).on('click', '.shipping_method', function() {
//                   
//                   id = jQuery(this).attr('id'); 
//                   value = jQuery(this).attr('value'); 
//                  // if(jQuery.cookie("selected_sub_shipping_method") == null){
//                    jQuery.cookie("selected_sub_shipping_method",'0' , { path: '/' });
//                 //  alert(value);
//                    jQuery.cookie("sub_shipping_method",value, { path: '/' });
//               // }
//          
//            
//                    if(value)
//                    _get_shipping_method(id, value);
//    });

});

jQuery(document).ready(function () {
    value = jQuery("input[name='shipping_method[0]']:checked").attr('value');
    if (!value) {
        value = jQuery("input[name='shipping_method[0]']").attr('value');
    }
    id = jQuery("input[name='shipping_method[0]']:checked").attr('id');
    //
    if (value) {
        console.log(value);
        value_arr = value.split(':');
        value = value_arr[0];
        // alert("cookie not set");

        selectedsingle = false;
        Cookies.set("sub_shipping_method", value, {path: '/'});
    }

    //console.log(value); 


});



