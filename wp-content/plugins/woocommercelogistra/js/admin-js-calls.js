function sendtologistra() {

    jQuery("#logistra_message").html("Requesting.....");

    jQuery.post(ajaxurl, {
        action: "gf_to_logistra",
        methods: jQuery("#logistra_methods").val(),
        id: jQuery("#log_order_id").val(),
        data: jQuery('#logistra_details :input').serialize()

    },
            function (response) {
                jQuery("#logistra_message").html('');
                if(response=='1'){
                    console.log(response);
                    jQuery("#response_message").html('Sent to logistra. Page will reload now.');
                    //location.reload();
                }
                else{
                    jQuery("#response_message").html('Error. Operation failed.');
                }

            }
    );


}

function sendReturRequest(){
    jQuery.post(ajaxurl, {
        action: "retur_request_to_logistra",
        id: jQuery("#log_order_id").val()

    },
            function (response) {
                console.log(response);
                jQuery("#return_message").html('Sending to logistra...');
                if(response=='1'){
                    jQuery("#return_message").html('Sent to logistra.');
                    window.location = window.location.href;
                }
                else{
                    jQuery("#return_message").html('Error. Operation failed.');
                }

            }
    );
}



jQuery(document).ready(function ($) {

	window.ajaxcheck=0;
	  
	  		
jQuery("select[multiple]").multipleSelect({
            multiple: true,
            multipleWidth: 200,
            width: '100%',
            selectAll: false
        });

$("#getSelectsBtn").click(function(event ) {
    event.preventDefault();
   // var data=[];
            //$('.logistra_user_form .form-group').each(function(){

                //var id=$(this).find('select').attr('id');
                /*var name_field=$("#name").multipleSelect("getSelects", "text");
                 var address1_field=$("#address1").multipleSelect("getSelects", "text");*/
                var name_val=$("#name").multipleSelect("getSelects");

               
                var address1_val=$("#address1").multipleSelect("getSelects");
                var address2_val=$("#address2").multipleSelect("getSelects");
                var country_val=$("#country").multipleSelect("getSelects");
                var postcode_val=$("#postcode").multipleSelect("getSelects");
                var city_val=$("#city").multipleSelect("getSelects");
                var email_val=$("#email").multipleSelect("getSelects");
                var phone_val=$("#phone").multipleSelect("getSelects");
                var mobile_val=$("#mobile").multipleSelect("getSelects");
                var contactperson_val=$("#contact_person").multipleSelect("getSelects");
                var send_sku_qty=false;
                

                

               
                if($('#send_sku_qty').is(':checked')){
                    var send_sku_qty = true;
                }
                
                if($('#use_freetext').is(':checked')){
                    var message_val=$("#freetext").val();

                }else{

                    var message_val=$("#message1").multipleSelect("getSelects");
                }
                if($('#use_minamount').is(':checked')){
                    var gratis_val='yes';

                }else{

                    var gratis_val='no';
                }
     
                  jQuery.post(ajaxurl, {
                        action: "add_user_details",
                        
                        data: {name:name_val,address1:address1_val,address2:address2_val,country:country_val,postcode:postcode_val,city:city_val,email:email_val,phone:phone_val,mobile:mobile_val,contact_person:contactperson_val,message1:message_val,minamount:gratis_val,send_sku_qty: send_sku_qty
                            },
success: function (response) {

       jQuery('.success').html('Settings Successfully Saved');

             
                 
                    


            },
            error: function (response) {
             

                jQuery('.error').html('Settings Could not be saved properly. Please try after sometime');
            }
                    }

                    );
          
            
});
    var children1 = $('.cmb_id_senderid').children();
    var children2 = $('.cmb_id_autotransfer_logistra').children();

    $('.cmb_id_senderid').detach();
    $('.cmb_id_autotransfer_logistra').detach();

    $('.cmb_id_apikey').append(children1);
    $('.cmb_id_live_logistra').attr('id', 'first');
    $('.cmb_id_live_logistra').append('<input class="next_btn" name="next" type="button" value="Next">');



    $('.cmb_id_service_address').append(children2);
    $('.cmb_id_service_address').append('<input class="pre_btn" name="previous" type="button" value="Previous"><input class="next_btn" name="next" type="button" value="Next">');
    $('.cmb_id_apikey').append('<input class="pre_btn" name="previous" type="button" value="Previous"><input class="next_btn" name="next" type="button" value="Next"><p class="imp_note">*All your previous settings will be overridden while going to the next step.</p>');



jQuery(".cmb_id_live_logistra .next_btn").click(function () {

if($("input[type='radio'].cmb_option").is(':checked')) {
    var card_type = $("input[type='radio'].cmb_option:checked").val();
   	if(card_type == 'sandbox'){
   	
   		$('.cmb_id_apikey #apikey').val("5455175715b41603edac4e57112fdd3ac22fb683");
   		$('.cmb_id_apikey #senderid').val("1155");

   	}else{
   	
   		//$('.cmb_id_apikey #apikey').val('');
   		//$('.cmb_id_apikey #senderid').val('');

   	}
}

	    });
    jQuery(".cmb_id_apikey .next_btn").click(function () {
    	jQuery('.loader_img').show();
        jQuery.post(ajaxurl, {
            action: "save_methods",
            api_key: jQuery("#apikey").val(),
            id: jQuery("#senderid").val()

        },
                function (response) {
                    response = jQuery.parseJSON(response);
                    //console.log(response);
                    jQuery('.loader_img').hide();
                    jQuery('.cmb_id_shipping_method_select').html('<label for="shipping_method_select">Shipping Methods</label><br/><br/>');
                    i = 0;
                    jQuery.each(response, function (key, value) {
                        jQuery('.cmb_id_shipping_method_select').append('<input type="checkbox" class="cmb_option" name="shipping_method_select[]" id="shipping_method_select' + i + '" value="' + key + '"><label for="shipping_method_select' + i + '">' + value + '</label><br/>');
                        i++;
                    });
                    jQuery('.cmb_id_shipping_method_select').append('<input class="pre_btn" name="previous" type="button" value="Previous"><input class="next_btn" name="next" onclick="save_ajax_here()" type="button" value="Next">');
                    jQuery(".cmb_id_apikey .next_btn").parent().next().fadeIn('slow');
                    jQuery(".cmb_id_apikey .next_btn").parent().css({
                        'display': 'none'
                    });
// Adding Class Active To Show Steps Forward;
                    jQuery('.active').next().addClass('active');

                    /*---------------------------------------------------------*/


                    jQuery(".pre_btn").click(function () { // Function Runs On PREVIOUS Button Click
    	
        jQuery(this).parent().prev().fadeIn('slow');
        jQuery(this).parent().css({
            'display': 'none'
        });
// Removing Class Active To Show Steps Backward;
        jQuery('.active:last').removeClass('active');
    });
                }
        );
    });
    
     jQuery('.cmb_id_shipping_method_select').append('<input class="pre_btn" name="previous" type="button" value="Previous"><input class="next_btn" name="next" onclick="save_ajax_here()" type="button" value="Next">');
    

 
                 
                    //console.log(response);
                    /*jQuery('.loader_img').hide();
                    jQuery('.cmb_id_customer_details').html('<label for="shipping_method_select">Customer Details</label><br/><br/>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Name</label><select id="billing_name" name="billing_name" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Address 1</label><select id="address1" name="address1" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Address 2</label><select id="address2" name="address2" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Country</label><select id="country" name="country" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Post Code</label><select id="postcode" name="postcode" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing City</label><select id="city" name="city" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Email</label><select id="email" name="email" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Phone</label><select id="phone" name="phone" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<label for="billing_name">Billing Contact Person</label><select id="contact_p" name="contact_p" multiple></select>');
                    jQuery('.cmb_id_customer_details').append('<input class="pre_btn" name="previous" type="button" value="Previous"><input class="next_btn" name="next" onclick="save_ajax_here()" type="button" value="Next">');*/
            
// Adding Class Active To Show Steps Forward;
                   

                    /*---------------------------------------------------------*/

             

    

});
function save_ajax_here(){


if(window.ajaxcheck == 0)
{

window.ajaxcheck=1;
	jQuery('.loader_img').show();
            
          
        	
        jQuery.post(ajaxurl, {
            action: "change_method_status",
           	methods: jQuery('input[name="shipping_method_select[]"]').serialize(),
            formval: jQuery('form.cmb-form').serialize()


        },
                function (response) {
                	jQuery('.cmb_id_shipping_method_select .next_btn').parent().next().fadeIn('slow');
            
            jQuery('.cmb_id_shipping_method_select').css({
                'display': 'none'
            });
// Adding Class Active To Show Steps Forward;
            jQuery('.active').next().addClass('active');
                	jQuery('.loader_img').hide();
                    
                }
        );
    }
}

jQuery(document).ready(function () {
    var count = 0; // To Count Blank Fields
    /*------------ Validation Function-----------------*/

    /*---------------------------------------------------------*/
    jQuery(".next_btn").click(function () { // Function Runs On NEXT Button Click
    	
        if (!jQuery(this).parent().hasClass('cmb_id_apikey')) {

            jQuery(this).parent().next().fadeIn('slow');
            
            jQuery(this).parent().css({
                'display': 'none'
            });
// Adding Class Active To Show Steps Forward;
            jQuery('.active').next().addClass('active');
            
        }
        

    });
    jQuery(".pre_btn").click(function () { // Function Runs On PREVIOUS Button Click
 		
        jQuery(this).parent().prev().fadeIn('slow');
       
        jQuery(this).parent().css({
            'display': 'none'
        });
// Removing Class Active To Show Steps Backward;
        jQuery('.active:last').removeClass('active');
    
    });
// Validating All Input And Textarea Fields
    jQuery(".submit_btn").click(function (e) {
        if (jQuery('input').val() == "" || jQuery('textarea').val() == "") {
            alert("*All Fields are mandatory*");
            return false;
        } else {
            return true;
        }
    });
    jQuery(document).on('wc_backbone_modal_before_update',function(e,data){
        if(data === 'wc-modal-shipping-method-settings'){
            jQuery('form input[type="number"]').each(function(){
                //console.log(jQuery(this).attr('name'));
                //jQuery(this).attr('name', jQuery(this).attr('name').replace( '[]', '' ));
                console.log(jQuery(this).attr('name'));
            });
        }
    });
});