<?php
/*
  Plugin Name: Logistra Cargonizer for Woocommerce 3.0.X
  Plugin URI: https://www.logistra4woocommerce.no
  Description: Woocommerce integrasjon med Logistra Cargonizer
  Version: 3.0.0
  Author: EasyWeb Norge
  License: GPLv2 or later
 */
/*
 * Initializeing some of the variables here
 * So that we can access then in any file in project rather than clling
 * functions repeatadly 
 */
ob_start();
define('LogistraAPIPluginPath', plugin_dir_path(__FILE__));
define('LogistraAPIURlPluginPath', plugins_url());
define('LogistraAPIPHomePluginPath', home_url());
define('LogistraAPIVersion', '1.0');
define('PATH_BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('Logpath', dirname(__FILE__).'/logs/logistralog.txt');
define('Logpath1', dirname(__FILE__).'/logs/logistralog1.txt');

wp_enqueue_style('logistra-theme-style', plugins_url('/style.css', __FILE__), '', rand(100, 182));
         wp_enqueue_style('multiselect-style', plugins_url('/multiple-select.css', __FILE__), '', rand(100, 182));

if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    return;
}
/*
 * Initialize the API , 
 */
$start = new LogistraAPI();
/*
 * You can include some of your external files Here
 */
if ($start) {
    require_once LogistraAPIPluginPath . 'include/API-Post.php';
    require_once LogistraAPIPluginPath . 'include/ajax.php';
    require_once LogistraAPIPluginPath . 'include/cargonizer.php';
}

class LogistraAPI {

    function __construct() {
        static $TAShippingMethods = 'LogistraTAShippingMethods';
        static $TAShippingOptions = 'LogistraTAShippingOptions';
        static $TAShippingPackages = 'LogistraTAShippingPackages';

        /*
         * This Code will run on Activating & Deactivating Plugins
         */
        register_activation_hook(__FILE__, array(&$this, 'LogistraActivation'));
        register_deactivation_hook(__FILE__, array(&$this, 'LogistraDeActivation'));

        // END Of Activation Code
        add_action('init', array('API_POST', 'api_initialize'));
        add_action('init', array(&$this, 'register_ready_to_shipping_order_status'));

        add_filter('woocommerce_shipping_methods', array(&$this, 'add_your_shipping_method'));
        add_action('wp_footer', array(&$this, 'logistra_front_script'));
    }

    function logistra_front_script() {
        global $wpdb;
        wp_enqueue_script('logistra_api_script', plugins_url('/js/apijs.js', __FILE__), array('jquery'), rand(), false);
        wp_enqueue_script('custom_api_script', plugins_url('/js/scripts.js', __FILE__), array('jquery'), rand(), false);



        $shiiping_methods = get_shipping_methods();
        if ($shiiping_methods) {
            foreach ($shiiping_methods as $value) {
                $available_shipping_method[] = $value->Classname;
            }
        } else
            $available_shipping_method = '';

        $assets_path = str_replace(array('http:', 'https:'), '', WC()->plugin_url()) . '/assets/';

        wp_localize_script('logistra_api_script', 'logistra', array(
            'ajax_url' => admin_url() . 'admin-ajax.php',
            'logistra_shipping_method' => $available_shipping_method,
            'ajax_loader_url' => apply_filters('woocommerce_ajax_loader_url', $assets_path . 'images/ajax-loader@2x.gif'),
            'is_checkout_page' => (is_checkout()) ? '1' : '0'
                )
        );
        echo '<style type="text/css"> .sub_shipping_error {  border: 2px solid #ff0000;  padding: 5px; margin-bottom: 10px;} .woocommerce ul#shipping_method .amount,.woocommerce ul#shipping_method .TextEstimated{font-weight:700 !important; font-size:15px !important;} #submethod{display:block !important; margin:10px 0;}</style>';
    }

    function LogistraActivation() {
        global $wp_version;

        /*
         * Check the plugin version compatible with wordpress
         * Current installed version
         * It Must be validated and above 3.8 
         */

        if (version_compare($wp_version, "3.8", "<")) {
            deactivate_plugins(basename(__FILE__));
            wp_die("This Plugin Requires Wordpress Version 3.8 or Higher");
        }
        add_option("LogistraAPIVersion", LogistraAPIVersion);
        self::InitializeLogistraDatabase();
    }

    function InitializeLogistraDatabase() {
        global $wpdb;
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
        $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
        $shippingPackages = $wpdb->prefix . 'LogistraTAShippingPackages';
        $logistratashippingservices = $wpdb->prefix . 'logistratashippingservices';
        $sql = "CREATE TABLE " . $shippingMethods . " (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `TAID` int(11) NOT NULL,
        `TADesc` varchar(200) NOT NULL,
        `Cname` varchar(200) NOT NULL,
        `Cidentifier` varchar(200) NOT NULL,
        `ShippingClass` varchar(200) NOT NULL,
         UNIQUE KEY `id` (`id`)
        );";
        $sql1 = "CREATE TABLE " . $shippingOptions . " (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `methodid` int(11) NOT NULL,
        `Pidentifier` varchar(200) NOT NULL,
        `Classname` varchar(200) NOT NULL,
        `Pname` varchar(200) NOT NULL,
        `minItems` int(11) NOT NULL,
        `maxItems` int(11) NOT NULL,
        `maxWeight` varchar(200) NOT NULL,
        `requireWeight` varchar(200)   NOT NULL,
        `dangerousGoods` varchar(200)   NOT NULL,
        `requireTermsDelivery` varchar(200)   NOT NULL,
        `ConsigneeFreightPayer` varchar(200)   NOT NULL,
        `deliveryAddress` varchar(200)   NOT NULL,
        `requiresServicePartner` varchar(200)   NOT NULL,
        `requiresCFreightCustomer` varchar(200)   NOT NULL,
        `pickupaddress` varchar(200)   NOT NULL,
        `requirespickupaddress` varchar(200)   NOT NULL,
        `agentnumberrequired` varchar(200)   NOT NULL,
        `agentnumberpossible` varchar(200)   NOT NULL,
        `Cfreight` varchar(200)   NOT NULL,
        `ThirdPartyfreight` varchar(200)   NOT NULL,
        `Termsdelivery` varchar(200)   NOT NULL,
         UNIQUE KEY `id` (`id`)
        );";
        $sql2 = "CREATE TABLE " . $shippingPackages . " (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `shippingid` int(11) NOT NULL,
        `pack_en` varchar(200) NOT NULL,
        `pack_no` varchar(200) NOT NULL,
        `pack_abber` varchar(200) NOT NULL,
        `pack_id` varchar(200) NOT NULL,
         UNIQUE KEY `id` (`id`)
        );";

        $sql3 = "CREATE TABLE " . $logistratashippingservices . " (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `Pidentifier` varchar(200) NOT NULL,
        `serviceIdentifier` varchar(200) NOT NULL,
        `serviceName` varchar(200) NOT NULL,
        `kid` varchar(10) NOT NULL,
        `amount` varchar(10) NOT NULL,
        `currency` varchar(10) NOT NULL,
        `account_number` varchar(10) NOT NULL,
		`min_total` varchar(10) NOT NULL,
		`description` varchar(250) NOT NULL,
         UNIQUE KEY `id` (`id`)
        );";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        dbDelta($sql1);
        dbDelta($sql2);
        dbDelta($sql3);
    }

    function LogistraDeActivation() {
        global $wpdb;


        delete_option('myprefix_options');
        delete_option('logistra_api_key');
        $shiiping_methods = get_shipping_methods();
        foreach ($shiiping_methods as $method) {
            $class_name = $method->Classname;
            $methodnm = 'woocommerce_' . $class_name . '_settings';
            delete_option($methodnm);
        }

        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
        $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
        $shippingPackages = $wpdb->prefix . 'LogistraTAShippingPackages';
        $logistratashippingservices = $wpdb->prefix . 'logistratashippingservices';

        $sql = "DROP Table IF EXISTS " . $shippingMethods;
        $sql2 = "DROP Table IF EXISTS " . $shippingOptions;
        $sql3 = "DROP Table IF EXISTS " . $shippingPackages;
        $sql4 = "DROP Table IF EXISTS " . $logistratashippingservices;

        $wpdb->query($sql);
        $wpdb->query($sql2);
        $wpdb->query($sql3);
        $wpdb->query($sql4);
    }
    function LogistraTruncate() {
        global $wpdb;


        delete_option('myprefix_options');
        delete_option('logistra_api_key');
        $shiiping_methods = get_shipping_methods(); 
        
        foreach ($shiiping_methods as $method) {
            $class_name = $method->Classname;
            $methodnm = 'woocommerce_' . $class_name . '_settings';
            delete_option($methodnm);
        }

        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
        $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
        $shippingPackages = $wpdb->prefix . 'LogistraTAShippingPackages';
        $logistratashippingservices = $wpdb->prefix . 'logistratashippingservices';

        $sql = "TRUNCATE Table  " . $shippingMethods;
        $sql2 = "TRUNCATE Table " . $shippingOptions;
        $sql3 = "TRUNCATE Table  " . $shippingPackages;
        $sql4 = "TRUNCATE Table  " . $logistratashippingservices;

        $wpdb->query($sql);
        $wpdb->query($sql2);
        $wpdb->query($sql3);
        $wpdb->query($sql4);

    }

    function register_ready_to_shipping_order_status() {
        register_post_status('wc-ready-to-shipping', array(
            'label' => 'Ready to Shipping',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Ready to Shipping <span class="count">(%s)</span>', 'Ready to Shipping <span class="count">(%s)</span>')
        ));
    }

    function add_your_shipping_method($methods) {
        $shiiping_methods = get_shipping_methods();
        if (!empty($shiiping_methods)) {
            //$methods = array();
            foreach ($shiiping_methods as $method):
                $class_name = $method->Classname;
                $methods[$class_name] = $class_name;
            endforeach;
        }
        return $methods;
    }

    function update_status_to_ready_to_shipping($methods, $formdata, $form) {

        // print_r($formdata);exit();

        global $order, $woocommerce, $wpdb;

//            print_r($form);exit();
//
//            $settings = $this->get_form_settings($formdata);
//            print_r($settings);exit();

        $data = get_option('myprefix_options', 'none');
if($data){
    $crg_api_key = $data['apikey'];
    $crg_sender_id = $data['senderid'];
}
else{
    $crg_api_key = '';
    $crg_sender_id = '';
}

$order = new WC_Order($formdata['consignor']);

//        $crg_api_key = '5455175715b41603edac4e57112fdd3ac22fb683';
//        $crg_sender_id = '1155';
        $serviceId = get_post_meta($formdata['consignor'], 'servicePartnerID', true); //shipping services
        $chosen_shipping_id=explode(":",$methods);
        $subShipping = $methods;
        $subShippingInfo = get_shipping($subShipping);
        $link = get_Logistralinks();
        
        
        $send_sku_qty = false;

        if (get_option('woocommerce_send_sku_qty_logistra')) {
            $send_sku_qty = get_option('woocommerce_send_sku_qty_logistra');
        }
        if ($send_sku_qty) {
            $order_items = $order->get_items();

            $send_sku_qty_msg = array();
            foreach ($order_items as $order_item) {
                $product = get_product($order_item['product_id']);
                if ($product->is_type('variable')) {
                    $product = get_product($order_item['variation_id']);
                }
                if ($product->has_weight()) {
                    $weight = $product->get_weight();
                } else {
                    $weight = 0;
                }
                $product_title = $product->get_title();
                for ($i = 0; $i < $order_item[qty]; $i++) {
                    $sku.=$product->get_sku() . ',';
                }
                $wt = $order_item[qty] * $weight;
                $totle_wt = $totle_wt + $wt;
                $product_title = $product->get_title();

                $send_sku_qty_msg[] .= ''.$product->get_sku().'(' . $order_item[qty].')';
            }
            $send_sku_qty_msg = implode(', ', $send_sku_qty_msg);
        }




        /*
         * This is the customer information who buy this products.
         */
        if(get_option('woocommerce_name_logistra')){

    $name_option=get_option('woocommerce_name_logistra');

    for($i=0;$i<sizeof($name_option);$i++)
    {

        

        if($order->{$name_option[$i]})
        $name.=$order->{$name_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_country_logistra')){

    $country_option=get_option('woocommerce_country_logistra');

    for($i=0;$i<sizeof($country_option);$i++)
    {

        

        if($order->{$country_option[$i]})
        $country.=$order->{$country_option[$i]}. ' ';

    }

}


if(get_option('woocommerce_email_logistra')){

    $email_option=get_option('woocommerce_email_logistra');
     for($i=0;$i<sizeof($email_option);$i++)
    {
         if($order->{$email_option[$i]})
        $email .=$order->{$email_option[$i]}. ' ';

    }

}


if(get_option('woocommerce_address1_logistra')){

    $address1_option=get_option('woocommerce_address1_logistra');

     for($i=0;$i<sizeof($address1_option);$i++)
    {

        if($order->{$address1_option[$i]})
        $address1 .=$order->{$address1_option[$i]}. ' ';
       

    }

}

if(get_option('woocommerce_address2_logistra')){

    $address2_option=get_option('woocommerce_address2_logistra');

    for($i=0;$i<sizeof($address2_option);$i++)
    {

        if($order->{$address2_option[$i]})
        $address2 .=$order->{$address2_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_city_logistra')){

    $city_option=get_option('woocommerce_city_logistra');
    for($i=0;$i<sizeof($city_option);$i++)
    {

        if($order->{$city_option[$i]})
        $city .=$order->{$city_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_phone_logistra')){
$phone_option=get_option('woocommerce_phone_logistra');
    for($i=0;$i<sizeof($phone_option);$i++)
    {

        if($order->{$phone_option[$i]})
        $phone .=$order->{$phone_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_postcode_logistra')){

    $postcode_option=get_option('woocommerce_postcode_logistra');
     for($i=0;$i<sizeof($postcode_option);$i++)
    {

        if($order->{$postcode_option[$i]})
        $postcode .=$order->{$postcode_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_contact_person_logistra')){

    $contactp_option=get_option('woocommerce_contact_person_logistra');
     for($i=0;$i<sizeof($contactp_option);$i++)
    {

        if($order->{$contactp_option[$i]})
        $contactp .=$order->{$contactp_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_mobile_logistra')){

    $mobile_option=get_option('woocommerce_mobile_logistra');
     for($i=0;$i<sizeof($mobile_option);$i++)
    {

        if($order->{$mobile_option[$i]})
        $mobile .=$order->{$mobile_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_message1_logistra')){

    $message_option=get_option('woocommerce_message1_logistra');

    if(is_array($message_option)){

     for($i=0;$i<sizeof($message_option);$i++)
    
    {

        if($order->{$message_option[$i]})
        $message .=$order->{$message_option[$i]}.' ';

    }

}else{

if($order->{$message_option})
$message =$order->{$message_option};


}
}

if(!$message){
    $message = "Forsendelse fra ".$site_name;
}
        $consignee = array(
        "name" =>(empty($name))?$order->shipping_first_name . ' ' . $order->shipping_last_name:$name ,
        "address1" => (empty($address1))?$order->shipping_address1:$address1,
        "address2" => (empty($address2))?"":$address2,
        //"country" => ($country == "")?$order->shipping_country:$country,
        "country" => $order->shipping_country,
        //"postcode" => ($postcode == "")?$order->shipping_postcode:$postcode,
        "postcode" => $order->shipping_postcode,
        "city" => (empty($city))?$order->shipping_city:$city,
        "email" => (empty($email))?$order->billing_email:$email,
        "phone" => (empty($phone))?$order->billing_phone:$phone,
        "mobile" => (empty($mobile))?$order->billing_phone:$mobile,
        "contact_person" => (empty($contactp))?$order->shipping_first_name:$contactp,
    );
        
        if ($send_sku_qty) {
            $messages = array(
                "consignee" => $send_sku_qty_msg
            );
        } else {
            $messages = array(
                "consignee" => (empty($message)) ? $order->customer_note : $message
            );
        }
        $references = array(
            "consignor" => $formdata['consignor']
        );


        /*
         * As per of API this URL are needed
         */

        $crg_consignment_url = "{$link}/consignments.xml";
        $crg_transport_url = "{$link}/transport_agreements.xml";
        $debug = 1;


        $crg_xml = new CRG_Xmlwriter();
        $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);

        $Q = "SELECT SM.TAID,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM 
              JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
              WHERE SO.Pidentifier = '$subShippingInfo->Pidentifier'";
        $_get_shipping_service = "SELECT `serviceIdentifier`,`serviceName`,`kid`,`amount`,`currency`,`account_number` FROM {$wpdb->prefix}logistratashippingservices";
        $id = $wpdb->get_row($Q);
        $results = $wpdb->get_results($Q);
        foreach ($results as $id) {
        //print_r($Q);
        
        $requiresServicePartner = $id->requiresServicePartner;
        $id = $id->TAID;
        //print_r($id);
        /*
         * This are the dynamic data which we took to send to API.
         */
        $site_name = get_bloginfo( 'name' );
        $items[] = array(
            "item" => array(
                "_attribs" => array(
                    "amount" => "1",
                    "description" => "Varer fra ".$site_name,
                    "type" => "$subShippingInfo->pack_abber",
                    "weight" => "1"
                )
            )
        );


        $crg_data['consignments'] = array(
            "consignment" => array(
                "_attribs" => array(
                    "transport_agreement" => $id,
                    "cost_estimate" => "true",
                    "print" => "true",
                ),
                "values" => array(
                    "value" => array(
                        "_attribs" => array(
                            "name" => "ordre_id",
                            "value" => $formdata['consignor'],
                        ),
                    ),
                    "value" => array(
                        "_attribs" => array(
                            "name" => "provider",
                            "value" => "EasyWeb Norge",
                        ),
                    ),
                    "value" => array(
                        "_attribs" => array(
                            "name" => "provider-email",
                            "value" => "support@ewn.no",
                        ),
                    ),
                ),
                "product" => $subShippingInfo->Pidentifier,
                "parts" => array(
                    "consignee" => $consignee,
                ),
                "items" => $items,
                "messages" => $messages,
                "references" => $references
            ),
        );
        
        if(strpos($subShippingInfo->Pidentifier, 'bring') !== false){
            $crg_data['consignments']['consignment']['service'] = array(
                "_attribs" => array(
                    "id" => "bring_e_varsle_for_utlevering"
                )
            );
        }
        
        if (strpos($subShippingInfo->CName, 'PostNord') !== false) {
            $crg_data['consignments']['consignment']['services'][] = array(
                "service" =>
                array(
                    "_attribs" => array(
                        "id" => "postnord_notification_sms"
                    ),
                    $order->billing_phone
                )
            );
            $crg_data['consignments']['consignment']['services'][] = array(
                "service" =>
                array(
                    "_attribs" => array(
                        "id" => "postnord_notification_email"
                    ),
                    $order->billing_email
                )
            );
        }

        if ($requiresServicePartner == 'true') {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $consignee[country] . '&postcode=' . $consignee[postcode] . '');
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            $headers = array(
                "X-Cargonizer-Key:" . $crg_api_key,
                "X-Cargonizer-Sender:" . $crg_sender_id,
                "Content-type:application/xml",
                "charset:utf-8"
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($curl);

            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $service_partners = json_decode($json, TRUE);
            $service_partners_info = array();
            if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {

                $service_partner = $service_partners['service-partners']['service-partner'];
                $partner = $service_partner[$serviceId];

                $service_partners_info[] = array(
                    "service-partner" => array(
                        "number" => (isset($partner["number"])) ? $partner["number"] : '',
                        "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                        "name" => (isset($partner["name"])) ? $partner["name"] : '',
                        "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                        "country" => (isset($partner["country"])) ? $partner["country"] : '',
                        "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                        "city" => (isset($partner["city"])) ? $partner["city"] : '',
                    )
                );

                $service_partner = array(
                    "number" => (isset($partner["number"])) ? $partner["number"] : '',
                    "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                    "name" => (isset($partner["name"])) ? $partner["name"] : '',
                    "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                    "country" => (isset($partner["country"])) ? $partner["country"] : '',
                    "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                    "city" => (isset($partner["city"])) ? $partner["city"] : '',
                );



                $crg_data['consignments']["consignment"]["parts"]["service_partner"] = $service_partner;
            }
        }
        $debug = 0;

        $crg->requestConsignment($crg_data, $debug, $crg_consignment_url);

        $result_xml = $crg->getResultXml();
        if ($result_xml->error) {
                if ($result_xml->error == 'Missing transport agreement') {
                    continue;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
//        print_r($crg_data);
//        print_r($crg_consignment_url);
//        print_r($result_xml);
        if ($result_xml->error) {
            $status = 1;
            $message = (string) $result_xml->error;
            writetolog("Error | Order #".$formdata['consignor']." Could not be sent to logistra : ".$message);

        } else {
            $status = 0;
            $message = (string) $result_xml->consignment->{'tracking-url'};
            writetolog("Success | Order #".$formdata['consignor']." Succesfully Sent to Logistra");
        }

        if (isset($result_xml->consignment->{'consignment-pdf'})) {
            update_post_meta($order_id, '_consignment-pdf', (string) $result_xml->consignment->{'consignment-pdf'});
        }
        if (isset($result_xml->consignment->{'waybill-pdf'})) {
            update_post_meta($order_id, '_waybill-pdf', (string) $result_xml->consignment->{'waybill-pdf'});
        }
        if (isset($result_xml->consignment->{'tracking-url'})) {
            update_post_meta($order_id, '_tracking-url', (string) $result_xml->consignment->{'tracking-url'});
        }


//    update_post_meta($order_id, '_servicenumber', $service_partner["number"]);
//    update_post_meta($order_id, '_servic  ecustomerNumber', $service_partner["customer_number"]);
//    update_post_meta($order_id, '_servicename', $service_partner["name"]);
//    update_post_meta($order_id, '_serviceaddress1', $service_partner["address1"]);
//    update_post_meta($order_id, '_servicecountry', $service_partner["country"]);
//    update_post_meta($order_id, '_servicepostcode', $service_partner["postcode"]);
//    update_post_meta($order_id, '_servicecity', $service_partner["city"]);

        return json_encode(array('status' => $status, 'message' => $message, 'xml' =>$crg_data));
    }

}
add_action('admin_init','create_log');

function create_log(){

 if (!file_exists(Logpath)) {
    $myfile = fopen(Logpath, "wb");
}
    
}

add_action('admin_init','check_license_deactivation');



function check_license_deactivation(){
     global $wpdb;
    $opt = unserialize(get_option('logistra_api_key'));
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://ewn.no/license_response.php');
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $data = array(
        'hostname' => $opt['hostname'],
        'license_key' => $opt['license_key']
    );
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
	
    $response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
	
	writetolog1('response form connection -> '.$response.'  connection info -->'.$httpcode);
   
    if($response==1 && $httpcode == 200){//license valid
            $options=array(
                'hostname' => $opt['hostname'],
            'license_key' => $opt['license_key'],
                'status' => 'active'
            );
         //  self::InitializeLogistraDatabase();
            
           if(!get_option('logistra_api_key'))
            add_option('logistra_api_key', serialize($options));
            else
                update_option ('logistra_api_key', serialize($options));
        }
        if($response==0 && $httpcode == 200){

            //license invalid
            $options=array(
                'hostname' => $opt['hostname'],
            'license_key' => $opt['license_key'],
                'status' => 'inactive'
            );
            //delete_option('logistra_api_key');
            //LogistraDeActivation();
           
          
            
            $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
            $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
            $shippingPackages = $wpdb->prefix . 'LogistraTAShippingPackages';
            $logistratashippingservices = $wpdb->prefix . 'logistratashippingservices';

        $sql = "TRUNCATE Table  " . $shippingMethods;
        $sql2 = "TRUNCATE Table " . $shippingOptions;
        $sql3 = "TRUNCATE Table  " . $shippingPackages;
        $sql4 = "TRUNCATE Table  " . $logistratashippingservices;

        $wpdb->query($sql);
        $wpdb->query($sql2);
        $wpdb->query($sql3);
        $wpdb->query($sql4);
           
        delete_option('myprefix_options');
        delete_option('logistra_api_key');
        $shiiping_methods = get_shipping_methods();
        foreach ($shiiping_methods as $method) {
            $class_name = $method->Classname;
            $methodnm = 'woocommerce_' . $class_name . '_settings';
            delete_option($methodnm);
        }

          if(!get_option('logistra_api_key'))
            add_option('logistra_api_key', serialize($options));
            else
                update_option ('logistra_api_key', serialize($options));
        }
}

function get_shipping($ship) {

    global $wpdb;
    $subshipping_info = "SELECT SM.TAID,SM.Cname,SM.ShippingClass,SP.pack_abber,SO.Pidentifier,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM
                JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
                JOIN {$wpdb->prefix}LogistraTAShippingPackages AS SP ON SO.id = SP.shippingid
                WHERE SO.Classname = '$ship'
            ";
    return $wpdb->get_row($subshipping_info);
}

add_action('woocommerce_order_status_completed', 'update_status_to_ready_to_shipping');

function update_status_to_ready_to_shipping($order_id) {

  //print_r("updated");exit(); 

    global $order, $woocommerce, $wpdb;

    /*
     * This ORder ID is required while sending data to API. As this Order ID is dynamic from Woocommerce
     */
    $order = new WC_Order($order_id);



    $order->shipping_first_name = (get_post_meta($order_id, '_shipping_first_name', true) == "")?get_post_meta($order_id, '_billing_first_name', true):get_post_meta($order_id, '_shipping_first_name', true);

    $order->shipping_last_name = (get_post_meta($order_id, '_shipping_last_name', true) == "")?get_post_meta($order_id, '_billing_last_name', true):get_post_meta($order_id, '_shipping_last_name', true);
//$billing_company = get_post_meta($wpo_wcpdf->export->order->id,'_billing_company',true);
    $order->shipping_address_1 = (get_post_meta($order_id, '_shipping_address_1', true) == "")?get_post_meta($order_id, '_billing_address_1', true):get_post_meta($order_id, '_shipping_address_1', true);

    $order->shipping_address_2 = (get_post_meta($order_id, '_shipping_address_2', true) == "")?'':get_post_meta($order_id, '_shipping_address_2', true);

    $order->shipping_city = (get_post_meta($order_id, '_shipping_city', true) == "")?get_post_meta($order_id, '_billing_city', true):get_post_meta($order_id, '_shipping_city', true);

 
    $order->shipping_postcode = (get_post_meta($order_id, '_shipping_postcode', true) == "")?get_post_meta($order_id, '_billing_postcode', true):get_post_meta($order_id, '_shipping_postcode', true);

    $order->shipping_country = "NO";
//$billing_state = get_post_meta($wpo_wcpdf->export->order->id,'_billing_state',true);
    $order->billing_first_name = get_post_meta($order_id, '_billing_first_name', true);
    $order->billing_last_name = get_post_meta($order_id, '_billing_last_name', true);
    $order->billing_address_1 = get_post_meta($order_id, '_billing_address_1', true);
    $order->billing_address_2 = get_post_meta($order_id, '_billing_address_2', true);
    $order->billing_city = get_post_meta($order_id, '_billing_city', true);
    $order->billing_postcode = get_post_meta($order_id, '_billing_postcode', true);

    $order->billing_email = get_post_meta($order_id, '_billing_email', true);
    $order->billing_phone = get_post_meta($order_id, '_billing_phone', true);
//$order->billing_company = get_post_meta($wpo_wcpdf->export->order->id,'_payment_method',true);

    $order_items = $order->get_items();
    $data = get_option('myprefix_options', 'none');

    $crg_api_key = $data['apikey'];
    $crg_sender_id = $data['senderid'];
	// $crg_api_key = '5455175715b41603edac4e57112fdd3ac22fb683';
        // $crg_sender_id = '1155';
    $serviceId = get_post_meta($order->id, 'servicePartnerID', true);
    $subShipping = get_post_meta($order_id, 'sub_shipping_method', true);
    $subShippingInfo = get_shipping($subShipping);
    $link = get_Logistralinks();
    $send_sku_qty = false;
    
    if (get_option('woocommerce_send_sku_qty_logistra')) {
        $send_sku_qty = get_option('woocommerce_send_sku_qty_logistra');
    }

    /*
     * We have took all products/items from this Order in woocommerce to be send to API
     */

    /* Prepare Items */
    $items = array();
    $totle_wt = 0;
    $send_sku_qty_msg = array();
    foreach ($order_items as $order_item) {
        $product = get_product($order_item['product_id']);
        if($product->is_type('variable')){
            $product = get_product($order_item['variation_id']);
        }
        if ($product->has_weight()) {
            $weight = $product->get_weight();
        } else {
            $weight = 0;
        }
        $product_title = $product->get_title();
        for ($i = 0; $i < $order_item[qty]; $i++) {
            $sku.=$product->get_sku() . ',';
        }
        $wt = $order_item[qty] * $weight;
        $totle_wt = $totle_wt + $wt;
        $product_title = $product->get_title();
        if($send_sku_qty){
            $send_sku_qty_msg[] .= $product->get_sku() .'('. $order_item[qty].')';
        }
    }
    $send_sku_qty_msg = implode(',', $send_sku_qty_msg);
    $site_name = get_bloginfo( 'name' );
    $items[] = array(
        "item" => array(
            "_attribs" => array(
                "amount" => "1",
                "description" => "Varer fra ".$site_name,
                "type" => "PK",
                "weight" => "$totle_wt"
            )
        )
    );

    /*
     * This is the customer information who buy this products.
     */

if(get_option('woocommerce_name_logistra')){

    $name_option=get_option('woocommerce_name_logistra');

    for($i=0;$i<sizeof($name_option);$i++)
    {

        

        if($order->{$name_option[$i]})
        $name.=$order->{$name_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_country_logistra')){

    $country_option=get_option('woocommerce_country_logistra');

    for($i=0;$i<sizeof($country_option);$i++)
    {

        

        if($order->{$country_option[$i]})
        $country.=$order->{$country_option[$i]}. ' ';

    }

}


if(get_option('woocommerce_email_logistra')){

    $email_option=get_option('woocommerce_email_logistra');
     for($i=0;$i<sizeof($email_option);$i++)
    {

        
if($order->{$email_option[$i]})
        $email .=$order->{$email_option[$i]}. ' ';

    }

}


if(get_option('woocommerce_address1_logistra')){

    $address1_option=get_option('woocommerce_address1_logistra');

     for($i=0;$i<sizeof($address1_option);$i++)
    {

        
if($order->{$address1_option[$i]})
        $address1 .=$order->{$address1_option[$i]}. ' ';
       

    }

}

if(get_option('woocommerce_address2_logistra')){

    $address2_option=get_option('woocommerce_address2_logistra');

    for($i=0;$i<sizeof($address2_option);$i++)
    {

        
if($order->{$address2_option[$i]})
        $address2 .=$order->{$address2_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_city_logistra')){

    $city_option=get_option('woocommerce_city_logistra');
    for($i=0;$i<sizeof($city_option);$i++)
    {

        
if($order->{$city_option[$i]})
        $city .=$order->{$city_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_phone_logistra')){
$phone_option=get_option('woocommerce_phone_logistra');
    for($i=0;$i<sizeof($phone_option);$i++)
    {

        
if($order->{$phone_option[$i]})
        $phone .=$order->{$phone_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_postcode_logistra')){

    $postcode_option=get_option('woocommerce_postcode_logistra');
     for($i=0;$i<sizeof($postcode_option);$i++)
    {

        
if($order->{$postcode_option[$i]})
        $postcode .=$order->{$postcode_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_contact_person_logistra')){

    $contactp_option=get_option('woocommerce_contact_person_logistra');
     for($i=0;$i<sizeof($contactp_option);$i++)
    {

        
if($order->{$contactp_option[$i]})
        $contactp .=$order->{$contactp_option[$i]}. ' ';

    }

}
if(get_option('woocommerce_mobile_logistra')){

    $mobile_option=get_option('woocommerce_mobile_logistra');
     for($i=0;$i<sizeof($mobile_option);$i++)
    {

        
if($order->{$mobile_option[$i]})
        $mobile .=$order->{$mobile_option[$i]}. ' ';

    }

}

if(get_option('woocommerce_message1_logistra')){

    $message_option=get_option('woocommerce_message1_logistra');

    if(is_array($message_option)){

     for($i=0;$i<sizeof($message_option);$i++)
    
    {

        
if($order->{$message_option[$i]})
        $message .=$order->{$message_option[$i]}.' ';

    }

}else{

if($order->{$message_option})
$message =$order->{$message_option};


}
}

if(!$message){
    $message = "Forsendelse fra ".$site_name;
}

$postcode=substr($postcode, 0, -1);
$country=substr($country, 0, -1);


$consignee = array(
        "name" =>(empty($name))?$order->shipping_first_name . ' ' . $order->shipping_last_name:$name ,
        "address1" => (empty($address1))?$order->shipping_address1:$address1,
        "address2" => (empty($address2))?"":$address2,
        //"country" => ($country == "")?$order->shipping_country:$country,
        "country" => $order->shipping_country,
        //"postcode" => ($postcode == "")?$order->shipping_postcode:$postcode,
        "postcode" => $order->shipping_postcode,
        "city" => (empty($city))?$order->shipping_city:$city,
        "email" => (empty($email))?$order->billing_email:$email,
        "phone" => (empty($phone))?$order->billing_phone:$phone,
        "mobile" => (empty($mobile))?$order->billing_phone:$mobile,
        "contact_person" => (empty($contactp))?$order->shipping_first_name:$contactp,
    );
        if ($send_sku_qty) {
            $messages = array(
                "consignee" => $send_sku_qty_msg
            );
        } else {
            $messages = array(
                "consignee" => (empty($message)) ? $order->customer_note : $message
            );
        }

/* $consignee = array(
        "name" => $order->shipping_first_name . ' ' . $order->shipping_last_name,
        "address1" => $order->shipping_address_1,
        "address2" => $order->shipping_address_2,
        "country" => $order->shipping_country,
        "postcode" => $order->shipping_postcode,
        "city" => $order->shipping_city,
        "email" => $order->billing_email,
        "phone" => $order->billing_phone,
        "contact_person" => $order->shipping_first_name
    );
    $messages = array(
        "consignee" => $order->customer_note
    );*/
    $references = array(
        "consignor" => $order->id
    );


    /*
     * As per of API this URL are needed
     */

    $crg_consignment_url = "{$link}/consignments.xml";
    $crg_transport_url = "{$link}/transport_agreements.xml";
    $debug = 1;


    $crg_xml = new CRG_Xmlwriter();
    $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);

    $Q = "SELECT SM.TAID,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM 
              JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
              WHERE SO.Pidentifier = '$subShippingInfo->Pidentifier'";
    $_get_shipping_service = "SELECT `serviceIdentifier`,`serviceName`,`kid`,`amount`,`currency`,`account_number` FROM {$wpdb->prefix}logistratashippingservices";

    $id = $wpdb->get_row($Q);
    $results = $wpdb->get_results($Q);
        foreach ($results as $id) {
    $requiresServicePartner = $id->requiresServicePartner;
    $id = $id->TAID;

    /*
     * This are the dynamic data which we took to send to API.
     */

    $crg_data['consignments'] = array(
        "consignment" => array(
            "_attribs" => array(
                "transport_agreement" => $id,
                "cost_estimate" => "true",
                "print" => "true",
            ),
            "values" => array(
                "value" => array(
                    "_attribs" => array(
                        "name" => "ordre_id",
                        "value" => "$order_id",
                    ),
                ),
                "value" => array(
                    "_attribs" => array(
                        "name" => "provider",
                        "value" => "EasyWeb Norge",
                    ),
                ),
                "value" => array(
                    "_attribs" => array(
                        "name" => "provider-email",
                        "value" => "support@ewn.no",
                    ),
                ),
            ),
            "product" => $subShippingInfo->Pidentifier,
            "parts" => array(
                "consignee" => $consignee,
            ),
            "items" => $items,
            "messages" => $messages,
            "references" => $references,
            "transfer" => ($data['autotransfer_logistra'] == "on") ? 'true' : 'false'
        ),
    );

    if(strpos($subShippingInfo->Pidentifier, 'bring') !== false){
            $crg_data['consignments']['consignment']['services'] = array(
                "service" => array(
                    "_attribs" => array(
                        "id" => "bring_e_varsle_for_utlevering"
                    )
                )
            );
        }
        if (strpos($subShippingInfo->CName, 'PostNord') !== false) {
            $crg_data['consignments']['consignment']['services'][] = array(
                "service" =>
                array(
                    "_attribs" => array(
                        "id" => "postnord_notification_sms"
                    ),
                    $order->billing_phone
                )
            );
            $crg_data['consignments']['consignment']['services'][] = array(
                "service" =>
                array(
                    "_attribs" => array(
                        "id" => "postnord_notification_email"
                    ),
                    $order->billing_email
                )
            );
        }

    //print_r($consignee);
    if ($requiresServicePartner == 'true') {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $consignee[country] . '&postcode=' . $consignee[postcode] . '');
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $headers = array(
            "X-Cargonizer-Key:" . $crg_api_key,
            "X-Cargonizer-Sender:" . $crg_sender_id,
            "Content-type:application/xml",
            "charset:utf-8"
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $service_partners = json_decode($json, TRUE);
        $service_partners_info = array();
        if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {

            $service_partner = $service_partners['service-partners']['service-partner'];
            $partner = $service_partner[$serviceId];

            $service_partners_info[] = array(
                "service-partner" => array(
                    "number" => (isset($partner["number"])) ? $partner["number"] : '',
                    "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                    "name" => (isset($partner["name"])) ? $partner["name"] : '',
                    "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                    "country" => (isset($partner["country"])) ? $partner["country"] : '',
                    "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                    "city" => (isset($partner["city"])) ? $partner["city"] : '',
                )
            );

            $service_partner = array(
                "number" => (isset($partner["number"])) ? $partner["number"] : '',
                "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                "name" => (isset($partner["name"])) ? $partner["name"] : '',
                "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                "country" => (isset($partner["country"])) ? $partner["country"] : '',
                "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                "city" => (isset($partner["city"])) ? $partner["city"] : '',
            );



            $crg_data['consignments']["consignment"]["parts"]["service_partner"] = $service_partner;
        }
    }
    $debug = 0;
   
    $crg->requestConsignment($crg_data, $debug, $crg_consignment_url);

    $result_xml = $crg->getResultXml();
    if ($result_xml->error) {
                if ($result_xml->error == 'Missing transport agreement') {
                    continue;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
if ($result_xml->error) {
         
            $message = (string) $result_xml->error;
            writetolog("Error | ".$message);

        } elseif($result_xml->errors){
            foreach ($result_xml->errors as $error) {
                $message = (string) $error;
                writetolog("Error | ".$message);
            }
        } else {
        
            $message = (string) $result_xml->consignment->{'tracking-url'};
            writetolog("Success | Order #".$order_id." Succesfully Sent to Logistra");
        }

    update_post_meta($order_id, '_servicenumber', $service_partner["number"]);
    update_post_meta($order_id, '_servicecustomerNumber', $service_partner["customer_number"]);
    update_post_meta($order_id, '_servicename', $service_partner["name"]);
    update_post_meta($order_id, '_serviceaddress1', $service_partner["address1"]);
    update_post_meta($order_id, '_servicecountry', $service_partner["country"]);
    update_post_meta($order_id, '_servicepostcode', $service_partner["postcode"]);
    update_post_meta($order_id, '_servicecity', $service_partner["city"]);

    if (isset($result_xml->consignment->{'consignment-pdf'})) {
        update_post_meta($order_id, '_consignment-pdf', (string) $result_xml->consignment->{'consignment-pdf'});
    }
    if (isset($result_xml->consignment->{'waybill-pdf'})) {
        update_post_meta($order_id, '_waybill-pdf', (string) $result_xml->consignment->{'waybill-pdf'});
    }
    if (isset($result_xml->consignment->{'tracking-url'})) {
        update_post_meta($order_id, '_tracking-url', (string) $result_xml->consignment->{'tracking-url'});
    }
   
}

add_action('woocommerce_admin_order_data_after_billing_address', 'show_logistra_shipping_information', 10, 1);

function show_logistra_shipping_information($order) {
    $order_shipping_method = $order->get_shipping_method();
    $consignment_pdf = get_post_meta($order->id, '_consignment-pdf', true);
    $waybill_pdf = get_post_meta($order->id, '_waybill-pdf', true);
    $tracking_url = get_post_meta($order->id, '_tracking-url', true);

    if ($consignment_pdf || $waybill_pdf || $tracking_url):
        echo '<p><strong>' . __('LOGISTRA CARGONIZER') . ':</strong> '
        . '<br/>' . (( $consignment_pdf ) ? '<a target="_blank" href="' . $consignment_pdf . '"> Last ned etikett (PDF) </a>' : '')
        . '<br/>' . (( $waybill_pdf ) ? '<a target="_blank" href="' . $waybill_pdf . '"> Last ned fraktbrev (PDF) </a>' : '')
        . '<br/>' . (( $tracking_url ) ? '<a target="_blank" href="' . $tracking_url . '"> Spor forsendelse </a>' : '')
        . '</p>';
    endif;

    $number = get_post_meta($order->id, '_servicenumber', true);
    $customernumber = get_post_meta($order->id, '_servicecustomerNumber', true);
    $name = get_post_meta($order->id, '_servicename', true);
    $address = get_post_meta($order->id, '_serviceaddress1', true);
    $country = get_post_meta($order->id, '_servicecountry', true);
    $postcode = get_post_meta($order->id, '_servicepostcode', true);
    $city = get_post_meta($order->id, '_servicecity', true);

    echo "<div>";
    if ($number) {
        echo "<strong>Servicenummer - </strong> $number <br/>";
    }
    if ($customernumber) {
        echo "<strong>Service kundenummer - </strong> $customernumber <br/>";
    }
    if ($name) {
        echo "<strong>Utleveringssted - </strong> $name <br/>";
    }
    if ($address) {
        echo "<strong>Adresse - </strong> $address <br/>";
    }
    if ($country) {
        echo "<strong>Land - </strong> $country <br/>";
    }
    if ($postcode) {
        echo "<strong>Postnnummer - </strong> $postcode <br/>";
    }
    if ($city) {
        echo "<strong>By - </strong> $city";
    }
    echo "</div>";
}

add_action('woocommerce_admin_order_data_after_shipping_address', 'show_logistra_retur_information', 10, 1);

function show_logistra_retur_information($order){
    $retur_options = get_option('logistra_retur_settings');
    if($retur_options['retur_enable']){
        echo '<input type="button" value="Send I retur" onclick="sendReturRequest()"/><div id="return_message"></div>';
    }
    $consignment_pdf = get_post_meta($order->id, '_retur_consignment_pdf', true);
    if ($consignment_pdf) {
        echo '<br/><a target="_blank" href="' . $consignment_pdf . '"> Last ned returetiketten her </a>';
    }
    $tracking_number = get_post_meta($order->id, '_retur_tracking_number', true);
    if ($tracking_number) {
        echo '<br/><a target="_blank" href="' . $tracking_number . '"> Spor returforsendelse </a>';
    }
}

add_action('woocommerce_order_details_after_order_table', 'show_logistra_shipping_info_order_page', 999, 1);

function show_logistra_shipping_info_order_page($order) {
    $tracking_url = get_post_meta($order->id, '_tracking-url', true);
    if ($tracking_url) {
        echo '<div class="sporing">
<a target="_blank" href="' . $tracking_url . '" class="button">Spor forsendelsen din</a><div><br/>';
    }
}

function remove_logistra_shipping_info() {
    if (WC()->cart->get_cart_contents_count() == 0) {
        unset_sub_shipping_data();
        //writetolog("Info | Shipping Info has been removed");
    }
}

add_action('woocommerce_thankyou', 'remove_logistra_shipping_info');
add_action('woocommerce_check_cart_items', 'remove_logistra_shipping_info');
add_action('woocommerce_before_main_content', 'remove_logistra_shipping_info');

function unset_sub_shipping_data() {
    setcookie('sub_shipping_method', '', time() - 3600);
    setcookie('sub_shipping_cost', '', time() - 3600);
    setcookie('sub_shipping_method_pack_abber', '', time() - 3600);
    setcookie('selected_sub_shipping_method', '', time() - 3600);
}

add_action('woocommerce_checkout_update_order_meta', 'save_sub_shipping_method', 10, 2);

function save_sub_shipping_method($order_id) {

    global $wpdb;
    $order = new WC_Order($order_id);
//    $serviceID = get_post_meta($order_id, 'servicePartnerID',true);
//    $subShipping = get_post_meta($order_id, 'sub_shipping_method',true);
//    
//    if(!$serviceID && !$subShipping)
//    {
    //$_COOKIE['selected_sub_shipping_method']
    $shipping=$order->get_items( 'shipping' );
    foreach ($shipping as $methods){
        $method = $methods['method_id'];
    }
    if(isset($_POST['sub_shipping'])){
        $servicePartnerID = $_POST['sub_shipping'];
    }
    else{
        $servicePartnerID = '0';
    }
    update_post_meta($order_id, 'servicePartnerID', $servicePartnerID);
    $method = explode(':', $method);
    update_post_meta($order_id, 'sub_shipping_method', $method[0]);
    writetolog("Info | Order Id".$order_id." Shipping method has been saved");
    // }
    //  unset_sub_shipping_data();
}

class myprefix_Admin {

    private $key = 'myprefix_options';
    protected $option_metabox = array();
    protected $title = '';
    protected $options_page = '';

    public function __construct() {
        $data = get_option('myprefix_options', 'none');
		// print_r($data);
if(isset($data['apikey'])&&($data['senderid'])){
     $crg_api_key = $data['apikey'];
     $crg_sender_id = $data['senderid'];
}
else{
    $crg_api_key = '';
    $crg_sender_id = '';
}
    
        
        $this->title = __('Oppsett og innstallasjon', 'myprefix');
        $this->fields = array(
            array(
                'name' => __('Logistra LIVE eller TESTMODUS (sandbox)', 'myprefix'),
                'desc' => __('Velg live eller testmodus (sandbox)', 'myprefix'),
                'id' => 'live_logistra',
                'type' => 'radio',
                'data-step' =>'step1',
                'options' => array(
                    'live' => __('Live', 'myprefix'),
                    'sandbox' => __('Sandbox', 'myprefix'),
                ),
            ),
            array(


                'name' => __('API Key', 'myprefix'),
                'desc' => __('API Key', 'myprefix'),
                'id' => 'apikey',
                'value' => $crg_api_key,
                'type' => 'text',
                'data-step' =>'step2',
            ),
            array(
                'name' => __('Sender Id', 'myprefix'),
                'desc' => __('Sender Id', 'myprefix'),
                'id' => 'senderid',
                'type' => 'text',
                'data-step' =>'step2',
                'value' => $crg_sender_id,
            ),
            array(
                'name' => __('Valg av servicepartner', 'myprefix'),
                'desc' => __('Brukeren får velge mellom de 5 nærmeste pickup point. Kun tilgjengelig for få fraktalternativer. Kan medføre ekstrakostnad hos transportør.', 'myprefix'),
                'id' => 'service_address',
                'type' => 'radio',
                'data-step' =>'step3',
                'options' => array(
                    'enabled' => __('På', 'myprefix'),
                    'disabled' => __('Av. Anbefalt!', 'myprefix'),

                ),
            ),
            array(
                'name' => __('Automatisk overføring av EDI', 'myprefix'),
                'desc' => __('Etter du har fullført ordre vil Cargonizer automatisk sende beskjed til transportør om at noe skal sendes. Velger du AV må du i Cargonizer sende EDI manuelt får du poster sendingen.', 'myprefix'),
                'id' => 'autotransfer_logistra',
                'type' => 'checkbox',
                'data-step' =>'step3',
            ),
            array(
                'name' => __('Fraktmetoder', 'myprefix'),
                'desc' => __('Velg fraktmetoder du ønsker å skru på med en gang. Du kan skru på flere under fraktinnstillingene i Woocommerce senere.'),
                'id' => 'shipping_method_select',
                'type' => 'multiselect',
                'data-step' =>'step4',
                'options' => array(
                    
                ),
            ),
             
           
            array(
                'name' => __('Gratulerer, oppsettet er klart.', 'myprefix'),
                'desc' => __('Klikk save for å lagre innstillingene.'),
                'id' => 'finish',
                'type' => 'multiselect',
                'data-step' =>'step5',
                'options' => array(
                    
                ),
            ),
        );
    }

    public function hooks() {
        /*wp_register_script('jquerymin', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', array('jquery'), rand(), false);
         wp_enqueue_script('jquerymin');*/
        wp_register_script('multiselect_script', plugins_url('/js/multiple-select.js', __FILE__), array('jquery'),rand(), false);
        wp_enqueue_script('multiselect_script');
        wp_register_script('plugin_admin_script', plugins_url('/js/admin-js-calls.js', __FILE__), array('jquery'), rand(), false);

       
        wp_localize_script('plugin_admin_script', 'myAjax', array('ajaxurl' => admin_url() . 'admin-ajax.php'));
        wp_enqueue_script('plugin_admin_script');
        wp_register_script('license_script', plugins_url('/js/licence-ajax.js', __FILE__), array('jquery'), rand(), false);
        wp_localize_script('license_script', 'licenseAjax', array('ajaxurl' => admin_url() . 'admin-ajax.php'));
        wp_enqueue_script('license_script');
        add_action('admin_init', array($this, 'init'));
        add_action('admin_menu', array($this, 'logistra_license'));
    }

    public function init() {
        register_setting($this->key, $this->key);
    }

    public function logistra_license() {
        add_menu_page('Logistra', 'Logistra', 'manage_options', 'logistra_license_page', array($this, 'logistra_license_page'));
        add_submenu_page('logistra_license_page', 'Logistra License', 'Logistra License', 'manage_options', 'logistra_license_page', array($this, 'logistra_license_page'));
        
        $opt = unserialize(get_option('logistra_api_key'));
        if ($opt && $opt['status'] == 'active'){


            $this->options_page = add_submenu_page('logistra_license_page', $this->title, $this->title, 'manage_options', $this->key, array($this, 'admin_page_display'));
            $this->options_page = add_submenu_page('logistra_license_page', 'Feltmapping', 'Feltmapping', 'manage_options', 'logistra_user_page', array($this, 'logistra_user_page'));
            $this->options_page = add_submenu_page('logistra_license_page', 'Retur', 'Retur', 'manage_options', 'logistra_retur_page', array($this, 'logistra_retur_page'));
            $this->options_page = add_submenu_page('logistra_license_page', 'Logg', 'Logg', 'manage_options', 'logistra_logs_page', array($this, 'logistra_logs_page'));
        }
        
    }

    public function logistra_license_page() {
        ?>

        <form name="logistra_license_form" class="logistra_license_form">
        <img src="<?php echo plugins_url('easyweb.png', __FILE__)?>" class="ewn_logo">
            <table>
                <tr>
                    <td>Hostname</td>
                    <td><?php echo $_SERVER['SERVER_NAME']; ?><input type="hidden" value="<?php echo $_SERVER['SERVER_NAME']; ?>" name="hostname"/></td>
                </tr>
                <tr>
                    <td>Lisensnøkkel</td>
                    <td>
        <?php $opt = unserialize(get_option('logistra_api_key')); ?>
                        <input type="text" name="license" value="<?php echo $opt ? $opt['license_key'] : '' ?>" required/></td>
                    <td><button id="license_check">Aktiver lisens</button></td>
                    <td><img src="<?php echo plugins_url('loader.gif', __FILE__)?>" class='loader_img'></td>
                </tr>
                <tr>
                    <td class="check_message" colspan="2">
                        <?php
                         if($opt['status'] == 'active'){

                            echo 'Din lisens er aktivert. Gå til <b>"Oppsett"</b> i sidemenyen til venstre.';

                         }else{

                            echo 'Feil lisensnøkkel. Prøv igjen eller kontakt EasyWeb Norge på support@ewn.no';
                         }
                         ?>

                    </td>
                </tr>
            </table>
        </form>
        <?php
    }

public function logistra_user_page() {

    global $woocommerce;
// Array of WP_User objects.
    $select_values=array();
/*foreach ( $blogusers as $user ) {
    $usermeta=get_user_meta($user->ID);
   // print_r($usermeta);

    $select_values['billing_first_name'][]=$usermeta['billing_first_name'][0];
    $select_values['billing_last_name'][]=$usermeta['billing_last_name'][0];
    $select_values['billing_address_1'][]=$usermeta['billing_address_1'][0];
    $select_values['billing_address_2'][]=$usermeta['billing_address_2'][0];
    $select_values['billing_postcode'][]=$usermeta['billing_postcode'][0];
    $select_values['billing_city'][]=$usermeta['billing_city'][0];
    $select_values['billing_country'][]=$usermeta['billing_country'][0];
    $select_values['billing_email'][]=$usermeta['billing_email'][0];
    $select_values['billing_phone'][]=$usermeta['billing_phone'][0];

    $select_values['shipping_first_name'][]=$usermeta['shipping_first_name'][0];
    $select_values['shipping_last_name'][]=$usermeta['shipping_last_name'][0];
    $select_values['shipping_address_1'][]=$usermeta['shipping_address_1'][0];
    $select_values['shipping_address_2'][]=$usermeta['shipping_address_2'][0];
    $select_values['shipping_postcode'][]=$usermeta['shipping_postcode'][0];
    $select_values['shipping_city'][]=$usermeta['shipping_city'][0];
    $select_values['shipping_country'][]=$usermeta['shipping_country'][0];
    $select_values['shipping_email'][]=$usermeta['shipping_email'][0];
    $select_values['shipping_phone'][]=$usermeta['shipping_phone'][0];
 

}*/
//var_dump($select_values);
    ?>

<form name="logistra_user_form" class="logistra_user_form">
    <img src="<?php echo plugins_url('easyweb.png', __FILE__)?>" class="ewn_logo">    
        <?php


$field_array = array(
    'name' => 'Name',
    'address1' => 'Address 1',
    'address2' => 'Address 2',
    'country' => 'Country',
    'postcode' => 'Postcode',
    'city' => 'City',
    'email' => 'E-mail',
    'phone' => 'Telephone',
    'mobile' => 'Mobile',
    'contact_person' => 'Contact Person',
    'message1' => 'Message');


foreach($field_array as $key => $field){
$options=array();
?>

<div class="form-group">
<?php

if(get_option('woocommerce_'.$key.'_logistra')){

        $options=get_option('woocommerce_'.$key.'_logistra');
}

   
   // $message=substr($message, 0, -3);


?>
                <label for="<?php echo $key;?>"><?php echo $field;?></label>
                    <select id="<?php echo $key;?>" name="<?php echo $key;?>" multiple>
                        
                        <optgroup label="Billing Details">

                                
                                <option value="billing_company" <?php if(is_array($options) && in_array('billing_company',$options)) {?>selected="selected"<?php }?>>Billing Company</option>
                                <option value="billing_address_1" <?php if(is_array($options) && in_array('billing_address_1',$options)) {?>selected="selected" <?php }?>>Billing Address 1</option>
                                <option value="billing_address_2" <?php if(is_array($options) && in_array('billing_address_2',$options)) {?>selected="selected" <?php }?>>Billing Address 2</option>
                                <option value="billing_first_name" <?php if(is_array($options) && in_array('billing_first_name',$options)) {?>selected="selected" <?php }?>>Billing First Name</option>
                                <option value="billing_last_name" <?php if(is_array($options) && in_array('billing_last_name',$options)) {?>selected="selected" <?php }?>>Billing Last Name</option>
                                <option value="billing_postcode" <?php if(is_array($options) && in_array('billing_postcode',$options)) {?>selected="selected" <?php }?>>Billing Postcode</option>
                                <option value="billing_city" <?php if(is_array($options) && in_array('billing_city',$options)) {?>selected="selected" <?php }?>>Billing City</option>
                                <option value="billing_country" <?php if(is_array($options) && in_array('billing_country',$options)) {?>selected="selected" <?php }?>>Billing Country</option>
                                <option value="billing_email" <?php if(is_array($options) && in_array('billing_email',$options)) {?>selected="selected" <?php }?>>Billing Email</option>
                                <option value="billing_phone" <?php if(is_array($options) && in_array('billing_phone',$options)) {?>selected="selected" <?php }?>>Billing Phone</option>

                                
                        </optgroup>
                        <optgroup label="Shipping Details">

                                
                                <option value="shipping_company" <?php if(is_array($options) && in_array('shipping_company',$options)) {?>selected="selected" <?php }?>>Shipping Company</option>
                                <option value="shipping_address_1" <?php if(is_array($options) && in_array('shipping_address_1',$options)) {?>selected="selected" <?php }?>>Shipping Address 1</option>
                                <option value="shipping_address_2" <?php if(is_array($options) && in_array('shipping_address_2',$options)) {?>selected="selected" <?php }?>>Shipping Address 2</option>
                                <option value="shipping_first_name" <?php if(is_array($options) && in_array('shipping_first_name',$options)) {?>selected="selected" <?php }?>>Shipping First Name</option>
                                <option value="shipping_last_name" <?php if(is_array($options) && in_array('shipping_last_name',$options)) {?>selected="selected" <?php }?>>Shipping Last Name</option>
                                <option value="shipping_postcode" <?php if(is_array($options) && in_array('shipping_postcode',$options)) {?>selected="selected" <?php }?>>Shipping Postcode</option>
                                <option value="shipping_city" <?php if(is_array($options) && in_array('shipping_city',$options)) {?>selected="selected" <?php }?>>Shipping City</option>
                                <option value="shipping_country" <?php if(is_array($options) && in_array('shipping_country',$options)) {?>selected="selected" <?php }?>>Shipping Country</option>
                                <option value="shipping_email" <?php if(is_array($options) && in_array('shipping_email',$options)) {?>selected="selected" <?php }?>>Shipping Email</option>
                                <option value="shipping_phone" <?php if(is_array($options) && in_array('shipping_phone',$options)) {?>selected="selected" <?php }?>>Shipping Phone</option>
                                
                        </optgroup>
                        

                    </select>

                </div>
<?php
}
       $send_sku_qty = false;
    
    if (get_option('woocommerce_send_sku_qty_logistra')) {
        $send_sku_qty = get_option('woocommerce_send_sku_qty_logistra');
    }
        ?>
            
                        <div class="form-group">
                            <label for="send_sku_qty">Send SKU + QTY</label>
                            <input type="checkbox" name="send_sku_qty" id="send_sku_qty" <?php echo $send_sku_qty ? 'checked' : ''; ?>>
                        </div>
                        <div class="form-group">
                            <label for="use_freetext">Bruk annen tekst?</label>
                            <input type="checkbox" name="use_freetext" id="use_freetext">

                        </div>
                        <div class="form-group">
                        <label for="freetext">Annen tekst</label>

                        <?php
                            if(get_option('woocommerce_message1_logistra')){

                                $message=get_option('woocommerce_message1_logistra');
                            }

                        ?>
                            <textarea name="freetext" id="freetext"><?php if(!is_array($message)){echo $message;}?></textarea>
                        </div>
    <div class="form-group">
                <label for="use_freetext">Gratis frakt tekst?</label>
                <input type="checkbox" <?php if(get_option('woocommerce_minamount_logistra') == 'yes') echo 'checked';?> name="use_minamount" id="use_minamount">

            </div>

                  
                 
                 <button id="getSelectsBtn">Lagre</button>  

                    <div class="success"></div>
                        <div class="failure"></div>
              
        </form>


    <?php
}
//    public function add_options_page() {
//        $this->options_page = add_submenu_page('logistra_license_page',$this->title, $this->title, 'manage_options', $this->key, array($this, 'admin_page_display'));
//    }

    public function logistra_retur_page() {
        if(isset($_POST['submit_retur'])){
            $options_array['retur_enable'] = isset($_POST['retur_enable'])?TRUE:FALSE;
            $options_array['retur_id'] = $_POST['retur_id'];
            $options_array['retur_email'] = $_POST['retur_email'];
            $options_array['retur_weight'] = $_POST['retur_weight'];
            update_option('logistra_retur_settings', $options_array);
        }
        $retur_options = get_option('logistra_retur_settings');
        ?>
        <form name="logistra_retur_form" method="post" enctype="multipart/form-data">
            <img src="<?php echo plugins_url('easyweb.png', __FILE__) ?>" class="ewn_logo">  
            <table>
                <tr>
                    <td>Enable Retur</td>
                    <td><input type="checkbox" name="retur_enable" <?php echo $retur_options['retur_enable'] ? 'checked' : ''; ?> value="yes"></td>
                </tr>
                <tr>
                    <td>Provider ID</td>
                    <td>
                        <select id="retur_id" name="retur_id">
                            <option value="mypack_return" <?php if($retur_options['retur_id'] == 'mypack_return') {?>selected="selected" <?php }?>>PostNord Return Drop Off</option>
                            <option value="bring_bedriftspakke_return" <?php if($retur_options['retur_id'] == 'bring_bedriftspakke_return') {?>selected="selected" <?php }?>>Bring Returservice Bedriftspakke</option>
                            <option value="bring_bedriftspakke_ekspress_return" <?php if($retur_options['retur_id'] == 'mypackbring_bedriftspakke_ekspress_return_return') {?>selected="selected" <?php }?>>Returservice Bedriftspakke Ekspress</option>
                            <option value="bring_servicepakke_return" <?php if($retur_options['retur_id'] == 'bring_servicepakke_return') {?>selected="selected" <?php }?>>Returservice Klimanøytral Servicepartner</option>
                            <option value="helthjem_mypack_return" <?php if($retur_options['retur_id'] == 'helthjem_mypack_return') {?>selected="selected" <?php }?>>Helt Hjem retur hent i butikk</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Email text</td>
                    <td>
                        <?php wp_editor(esc_html( __($retur_options['retur_email'])), 'retur_email', $settings);?>
                        <!--<input type="text" name="retur_email" placeholder="" value="<?php echo $retur_options['retur_email'];?>">-->
                    </td>
                </tr>
                <tr>
                    <td>Weight</td>
                    <td><input type="number" name="retur_weight" step="any" placeholder="" value="<?php echo $retur_options['retur_weight'];?>"></td>
                </tr>
                <tr>
                    <td><input type="submit" name="submit_retur" value="Submit"></td>
                    <td></td>
                </tr>
            </table>
            <div class="success"></div>
            <div class="failure"></div>

        </form>
        <?php
    }

    public function logistra_logs_page() {

//echo file_get_contents(Logpath);
    echo "<div class='logs'>";
    echo "<h2 class='log_head'>If you do not see any logs , please ensure log file has write permissions</h2>";
$f = fopen(Logpath, "r") or exit("Unable to open file!");

while(!feof($f))
{
  echo fgets($f)."<br />";
}
echo "</div>";
}

    public function admin_page_display() {
        ?>
        <div class="wrap cmb_options_page <?php echo $this->key; ?>">
            <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
        <?php cmb_metabox_form($this->option_metabox(), $this->key); ?>
        </div>
            <?php
        }

        public function option_metabox() {
            return array(
                'id' => 'option_metabox',
                'show_on' => array('key' => 'options-page', 'value' => array($this->key,),),
                'show_names' => true,
                'fields' => $this->fields,
            );
        }

        public function __get($field) {
            if (in_array($field, array('key', 'fields', 'title', 'options_page'), true)) {
                return $this->{$field};
            }
            if ('option_metabox' === $field) {
                return $this->option_metabox();
            }

            throw new Exception('Invalid property: ' . $field);
        }

    }

    $myprefix_Admin = new myprefix_Admin();
    $myprefix_Admin->hooks();

    function myprefix_get_option($key = '') {
        global $myprefix_Admin;
        return cmb_get_option($myprefix_Admin->key, $key);
    }

    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

        function logistra_shipping_method_init() {
            global $wpdb;
            global $woocommerce;
          
            $shiiping_methods = get_shipping_methods();

          $shipping_classes = get_terms( 'product_shipping_class', 'hide_empty=0' );
          $shipping_arr=array();
          foreach($shipping_classes as $ship){
            
            
            array_push($shipping_arr,$ship->slug);

          }
           
            //print_r(array_values($shipping_arr));
        $arr=implode(",",$shipping_arr);
            if (!empty($shiiping_methods)) {
                $wp_roles = new WP_Roles();
                $allRoles = $wp_roles->get_names();

                foreach ($shiiping_methods as $method):
                    $class_name = $method->Classname;

                    $sub_shipping_methods = array();
                    $sub_shipping_method_string = "array ( 
                                                     'name' => array(
                                                                             'title' 		=> 'Navn',
                                                                             'type' 		=> 'Text',
                                                                             'placeholder'	=> 'Name',
                                                                             'label' 		=>  '{$class_name}',
                                                                             'default'                 => '{$method->Pname}'
                                                                       ),
                                                                       'tax_status' => array(
		'title' 		=> __( 'Tax Status', 'woocommerce' ),
		'type' 			=> 'select',
		'class'         => 'wc-enhanced-select',
		'default' 		=> 'taxable',
		'options'		=> array(
			'taxable' 	=> __( 'Taxable', 'woocommerce' ),
			'none' 		=> _x( 'None', 'Tax status', 'woocommerce' )
		)
	),
        'is_per_item' => array(
                'title' => 'Enable Shipping Cost per item',
                'type' => 'checkbox',
                'label' => 'Enable Shipping Cost per item',
                'default' => 'no'
            ),
                                                     'enable_shipping_class' => array(
                                                                             'title'        => 'Aktiver fraktklasser',
                                                                             'type'         => 'checkbox',
                                                                             'label'        =>  'Aktiver fraktklasse funksjonalitet',
                                                                             'default'      => 'no'


                                                                       ), ";
                    //$shipping_classes = $woocommerce->shipping->get_shipping_classes();
                    if ( ! empty( $shipping_classes ) ) {
                        $sub_shipping_method_string .=" 'class_costs'  => array(
		'title'			 => 'Fraktpris eks mva for fraktklasse',
		'type'			 => 'title',
		'default'        => '',
		'description'    => 'These costs can optionally be added.' 
	),";
                        foreach ( $shipping_classes as $shipping_class ) {
                            if ( ! isset( $shipping_class->term_id ) ) {
                                continue;
                            }
                            $sub_shipping_method_string .="'class_cost_{$shipping_class->term_id}'  => array(
			'title'       =>  '{$shipping_class->name} Shipping Class Cost' ,
			'type'        => 'text',
			'placeholder' => '0',
			'default'     => '' 
		),";
                        }
                        $sub_shipping_method_string .=" 'no_class_cost'  => array(
		'title'       => 'No Shipping Class Cost',
		'type'        => 'text',
		'placeholder' => '0',
		'default'     => ''
	),";
                        $sub_shipping_method_string .=" 'type'  => array(
		'title' 		=> 'Calculation Type',
		'type' 			=> 'select',
		'class'         => 'wc-enhanced-select',
		'default' 		=> 'order',
		'options' 		=> array(
			'class' 	=> 'Per Class: Charge shipping for each shipping class individually',
			'order' 	=> 'Per Order: Charge shipping for the most expensive shipping class'
		)
	),";
                    }
  

                                                     $sub_shipping_method_string .= "'cost' => array(
                                                                             'title' 		=> 'Fraktpris eks. mva',
                                                                             'type' 		=> 'price',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'Cost For  {$class_name}',
                                                                             'default'                 => ''
                                                                       ), 
                    								    'min_total' => array(
                                                                             'title' 		=> 'Gratis frakt ved beløp over',
                                                                             'type' 		=> 'price',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'min For  {$class_name}',
                                                                             'default'                 => ''
                                                                       ),
                    								    'free_shipping_coupon' => array(
                                                                             'title' => 'Tillat fri frakt ved kuppongbruk',
                                                                            'type' => 'checkbox',
                                                                            'label' => 'Tillat fri frakt ved kuppongbruk',
                                                                            'default' => 'no'
                                                                       ),
                    								    'home_delivery_amount' => array(
                                                                             'title' 		=> 'Pris for gratis frakt (la stå blankt for gratis frakt)',
                                                                             'type' 		=> 'price',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'Pris for gratis frakt (la stå blankt for gratis frakt) For  {$class_name}',
                                                                             'default'                 => ''
                                                                       ),
                    								    'weight' => array(
                                                                             'title' 		=> 'Weight options',
                                                                             'type' 		=> 'repeater',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'Weights For  {$class_name}',
                                                                             'default'                 => ''
                                                                       ),
                                                                        'max_weight' => array(
                                                                            'title' => 'Vis denne fraktmetode under X kilo (kg)',
                                                                            'type' => 'text',
                                                                            'placeholder' => '0',
                                                                            'label' => 'Weight limit For {$class_name}',
                                                                            'default' => ''
                                                                        ),
                    									'description' => array(
                                                                             'title' 		=> 'Fraktklasse beskrivelse',
                                                                             'type' 		=> 'Text',
                                                                             'placeholder'	=> 'Description',
                                                                             'label' 		=>  'description For  {$class_name}',
                                                                             'default'                 => ''
                                                                       ),";
                    $sub_shipping_method_string .="'is_role'=>array(  'title' 		=> 'Rollebasert fraktprising',
                                                                             'type' 		=> 'checkbox',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'Skru på rollebasert fraktprising for {$class_name}',
                                                                             'default'                 => ''),";
                    foreach ($allRoles as $key => $value) {
                        $sub_shipping_method_string .="'$key'=>array(  'title' 		=> 'Fraktpris eks. mva for {$value}',
                                                                             'type' 		=> 'price',
                                                                             'placeholder'	=> '0',
                                                                             'label' 		=>  'min For {$value}',
                                                                             'default'                 => ''),";
                    }
                    $sub_shipping_method_string .= ' ) ';

                    if (!class_exists($class_name)) {

                        eval("class {$class_name} extends WC_Shipping_Method {
                            protected \$fee_cost = '';
                        /*public \$sub_shipping_method = \$sub_shipping_method_string;*/
                    public function __construct(\$instance_id = 0) {
                    \$this->id ='{$class_name}';
                    \$this->instance_id 			 = absint( \$instance_id );
                    \$this->method_title = (\$this->get_option('name'))?\$this->get_option('name'):'$method->Pname'; 
                    \$this->method_description = 'something';
                    \$this->supports = array(
			'shipping-zones',
			'instance-settings',
			'instance-settings-modal',
                    );
                    /*\$this->enabled = 'no';*/
                    
                    \$this->init(); 
                    add_action('woocommerce_update_options_shipping_'.\$this->id,array(\$this, 'process_admin_options'));
                    }
                    public function init(){
                    \$this->instance_form_fields = {$sub_shipping_method_string};
                    /*\$this->init_settings();*/
                    \$this->title = (\$this->get_option('name'))?\$this->get_option('name'):'$method->Pname';
		\$this->tax_status           = \$this->get_option( 'tax_status' );
                \$this->is_per_item           = \$this->get_option( 'is_per_item' );
                \$this->free_shipping_coupon  = \$this->get_option( 'free_shipping_coupon' );
		\$this->cost                 = \$this->get_option( 'cost' );
		\$this->type                 = \$this->get_option( 'type', 'order' );
					\$this->min_total = \$this->get_option('min_total');
                    \$this->home_delivery_amount = \$this->get_option('home_delivery_amount');
                                        \$this->max_weight = \$this->get_option('max_weight');
					\$this->description = \$this->get_option('description');
                    /*\$this->enabled		= \$this->get_option( 'enabled' );*/
                    }
                    function init_form_fields(){
                     
                    }
                    protected function evaluate_cost( \$sum, \$args = array() ) {
		

		\$args           = apply_filters( 'woocommerce_evaluate_shipping_cost_args', \$args, \$sum, \$this );
		\$locale         = localeconv();
		\$decimals       = array( wc_get_price_decimal_separator(), \$locale['decimal_point'], \$locale['mon_decimal_point'] );
		\$this->fee_cost = \$args['cost'];

		add_shortcode( 'fee', array( \$this, 'fee' ) );

		\$sum = do_shortcode( str_replace(
			array(
				'[qty]',
				'[cost]'
			),
			array(
				\$args['qty'],
				\$args['cost']
			),
			\$sum
		) );

		remove_shortcode( 'fee', array( \$this, 'fee' ) );

		\$sum = preg_replace( '/\s+/', '', \$sum );

		\$sum = str_replace( \$decimals, '.', \$sum );

		\$sum = rtrim( ltrim( \$sum, '\t\n\r\0\x0B+*/' ), '\t\n\r\0\x0B+-*/' );

		return \$sum ?  \$sum  : 0;
	}
        public function fee( \$atts ) {
		\$atts = shortcode_atts( array(
			'percent' => '',
			'min_fee' => '',
			'max_fee' => '',
		), \$atts );

		\$calculated_fee = 0;

		if ( \$atts['percent'] ) {
			\$calculated_fee = \$this->fee_cost * ( floatval( \$atts['percent'] ) / 100 );
		}

		if ( \$atts['min_fee'] && \$calculated_fee < \$atts['min_fee'] ) {
			\$calculated_fee = \$atts['min_fee'];
		}

		if ( \$atts['max_fee'] && \$calculated_fee > \$atts['max_fee'] ) {
			\$calculated_fee = \$atts['max_fee'];
		}

		return \$calculated_fee;
	}
        public function calculate_shipping( \$package = array() ) {
        global \$wp_roles, \$current_user, \$woocommerce;
		\$rate = array(
			'id'      => \$this->get_rate_id(),
			'label'   => \$this->title,
			'cost'    => 0,
			'package' => \$package,
		);

		\$has_costs = false; 
		\$cost      = \$this->get_option( 'cost' );

		if ( \$cost !== '' ) {
			\$has_costs    = true;
			\$rate['cost'] = \$this->evaluate_cost( \$cost, array(
				'qty'  => \$this->get_package_item_qty( \$package ),
				'cost' => \$package['contents_cost'],
			) );
		}
                
        if(\$this->get_option('weight')){
            \$weight = \$woocommerce->cart->cart_contents_weight;
            foreach (\$this->get_option('weight') as \$index => \$range) {
                if(\$weight >= \$range['from'] && \$weight < \$range['to']){
                    \$rate['cost'] = \$range['cost'];
                }
            }
        }
                if (\$this->get_option('enable_shipping_class') == 'yes') {

		\$found_shipping_classes = \$this->find_shipping_classes( \$package );
		\$highest_class_cost     = 0;

		foreach ( \$found_shipping_classes as \$shipping_class => \$products ) {
			\$shipping_class_term = get_term_by( 'slug', \$shipping_class, 'product_shipping_class' );
			\$class_cost_string   = \$shipping_class_term && \$shipping_class_term->term_id ? \$this->get_option( 'class_cost_' . \$shipping_class_term->term_id, \$this->get_option( 'class_cost_7' )) : \$this->get_option( 'no_class_cost', '' );

			if ( \$class_cost_string === '' ) {
				continue;
			}

			\$has_costs  = true;
			\$class_cost = \$this->evaluate_cost( \$class_cost_string, array(
				'qty'  => array_sum( wp_list_pluck( \$products, 'quantity' ) ),
				'cost' => array_sum( wp_list_pluck( \$products, 'line_total' ) )
			) );

			if ( \$this->type === 'class' ) {
				\$rate['cost'] += \$class_cost;
			} else {
				\$highest_class_cost = \$class_cost > \$highest_class_cost ? \$class_cost : \$highest_class_cost;
			}
		}

		if ( \$this->type === 'order' && \$highest_class_cost ) {
                        \$rate['cost'] = \$rate['cost'] > \$highest_class_cost ? \$rate['cost'] : \$highest_class_cost;
			
		}
                }
                if (is_user_logged_in()) {
            if (\$this->get_option('is_role') == 'yes') {
                \$user = new WP_User(\$current_user->ID);
                \$role = \$user->roles;
                \$rate['cost'] = \$this->get_option(ucfirst(\$role[0]));
            }
        }
        

        \$total = \$woocommerce->cart->subtotal;
        if(\$this->get_option('min_total') != 0 && \$total > \$this->get_option('min_total')){
            \$rate['cost'] = \$this->get_option('home_delivery_amount')?\$this->get_option('home_delivery_amount'):0;
        }
        \$has_coupon = false;
        if ( \$coupons = WC()->cart->get_coupons() ) {
				foreach ( \$coupons as \$code => \$coupon ) {
					if ( \$coupon->is_valid() && \$coupon->get_free_shipping() ) {
						\$has_coupon = true;
						break;
					}
				}
			}
                        if(\$has_coupon && \$this->get_option('free_shipping_coupon') == 'yes'){
                            \$rate['cost'] = 0;
                        }
        \$qty = \$this->get_package_item_qty( \$package );
        if (\$this->get_option('is_per_item') == 'yes') {
            \$rate['cost'] *= \$qty;
        }
        

		if ( \$has_costs ) {
			\$this->add_rate( \$rate );
		}

		
		do_action( 'woocommerce_' . \$this->id . '_shipping_add_rate', \$this, \$rate );
	}
        public function get_package_item_qty( \$package ) {
		\$total_quantity = 0;
		foreach ( \$package['contents'] as \$item_id => \$values ) {
			if ( \$values['quantity'] > 0 && \$values['data']->needs_shipping() ) {
				\$total_quantity += \$values['quantity'];
			}
		}
		return \$total_quantity;
	}
        public function find_shipping_classes( \$package ) {
		\$found_shipping_classes = array();

		foreach ( \$package['contents'] as \$item_id => \$values ) {
			if ( \$values['data']->needs_shipping() ) {
				\$found_class = \$values['data']->get_shipping_class();

				if ( ! isset( \$found_shipping_classes[ \$found_class ] ) ) {
					\$found_shipping_classes[ \$found_class ] = array();
				}

				\$found_shipping_classes[ \$found_class ][ \$item_id ] = \$values;
			}
		}

		return \$found_shipping_classes;
	}
        public function is_available( \$package ) {
        global \$woocommerce;
        \$weight = \$woocommerce->cart->cart_contents_weight;
        if (\$weight > \$this->max_weight && \$this->max_weight != '' && \$this->max_weight != '0')
            \$is_available = false;
        else
            \$is_available = true;
        return apply_filters( 'woocommerce_shipping_' . \$this->id . '_is_available', \$is_available, \$package );
    }
        public function generate_repeater_html( \$key, \$data ) {
		\$field_key = \$this->get_field_key( \$key );
		\$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		\$data = wp_parse_args( \$data, \$defaults );

		\$ranges = \$this->get_option(\$key);
        ob_start();
        ?>
        <tr valign='top'>
            <th scope='row' class='titledesc'>
                <label for=''><?php echo wp_kses_post(\$data['title']); ?></label>
                <?php echo \$this->get_tooltip_html(\$data); ?>
            </th>
            <td class='forminp a111'>
                <table class='weight_range'>
                    <?php
                    if (\$ranges) {
                        foreach (\$ranges as \$index => \$range) {
                            ?>
                            <tr>
                                <td>
                                    <fieldset>
                                        <input class='input-text' type='number' step='any' name='weightfrom[]' value='<?php echo \$range['from']; ?>'/>Vekt fra<br/>
                                        <input class='input-text' type='number' step='any' name='weightto[]'  value='<?php echo \$range['to']; ?>'/>Vekt til<br/>
                                        <input class='input-text' type='number' name='weightcost[]' value='<?php echo \$range['cost']; ?>'/>Pris<br/>
                                        <?php echo \$this->get_description_html(\$data); ?>
                                        <button type='button' class='remove_this'>-</button>
                                    </fieldset>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td>
                                <fieldset>
                                    <input class='input-text' type='number' step='any' name='weightfrom[]' value=''/>Vekt fra<br/>
                                    <input class='input-text' type='number' step='any' name='weightto[]' value=''/>Vekt til<br/>
                                    <input class='input-text' type='number' name='weightcost[]' value=''/>Pris<br/>
                                    <?php echo \$this->get_description_html(\$data); ?>
                                    <button type='button' class='remove_this'>-</button>
                                </fieldset>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <button type='button' class='add_row'>Add</button>
                <script>
                    jQuery('.add_row').on('click', function () {
                    console.log('here');
                        jQuery('.weight_range').append(\"<tr><td><fieldset><input class='input-text' type='number' step='any' name='weightfrom[]' value=''/>Vekt fra<br/><input class='input-text' type='number' step='any' name='weightto[]' value=''/>Vekt til<br/><input class='input-text' type='number' name='weightcost[]' value=''/>Pris<br/><?php echo \$this->get_description_html(\$data); ?></fieldset></td></tr>\");
                    });
                    jQuery(document).on('click', '.remove_this', function () {
                        jQuery(this).closest('tr').remove();
                    });
                </script>
            </td>
        </tr>
        <?php
        return ob_get_clean();
	}
        public function process_admin_options() {
        if (\$this->instance_id) {
            \$this->init_instance_settings();

            \$post_data = \$this->get_post_data();

            foreach (\$this->get_instance_form_fields() as \$key => \$field) {
                if ('repeater' !== \$this->get_field_type(\$field) && 'title' !== \$this->get_field_type(\$field)) {
                    try {
                        \$this->instance_settings[\$key] = \$this->get_field_value(\$key, \$field, \$post_data);
                    } catch (Exception \$e) {
                        \$this->add_error(\$e->getMessage());
                    }
                } elseif ('title' !== \$this->get_field_type(\$field)) {
                    \$weight_ranges = array();
                    foreach (\$post_data['weightcost'] as \$index => \$value) {
                        \$range['from'] = \$post_data['weightfrom'][\$index];
                        \$range['to'] = \$post_data['weightto'][\$index];
                        \$range['cost'] = \$post_data['weightcost'][\$index];
                        \$weight_ranges[] = \$range;
                    }
                    \$this->instance_settings[\$key] = \$weight_ranges;
                }
            }

            return update_option(\$this->get_instance_option_key(), apply_filters('woocommerce_shipping_' . \$this->id . '_instance_settings_values', \$this->instance_settings, \$this));
        } else {
            return parent::process_admin_options();
        }
    }
                    /*function calculate_shipping( \$package ) {
                        \$rate = array(
			'id' 		=> \$this->id,
			'label' 	=> \$this->title,
                                                      'cost' => \$this->cost,  
                                                      'calc_tax' => 'per_order'
                        
                        );
                        \$this->add_rate(\$rate);
                        }*/
                    /*function calculate_shipping( \$package ) {
					\$rate = array(
						'id' => \$this->id,
						'label' => \$this->title,
						'cost' => '10.99',
					); 
                                        
					// Register the rate
                                        
					\$this->add_rate( \$rate );
				}*/
                    }");
                        //print_r($class); echo '<br/><br/>';
                    }



                endforeach;
                //exit;
            }
        }

        add_action('woocommerce_shipping_init', 'logistra_shipping_method_init');
    }

    function get_Logistralinks() {

        $data = get_option('myprefix_options', 'none');

        if ($data && $data != 'none') {
            $logistra = $data['live_logistra'];
            if ($logistra == 'live')
                $link = 'https://cargonizer.no';
            elseif ($logistra == 'sandbox')
                $link = 'https://sandbox.cargonizer.no';
        } else
            $link = 'https://sandbox.cargonizer.no';

        return $link;
    }

    function get_shipping_methods() {
        global $wpdb;
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingOptions';
        $Query = "select `A`.`id`,`A`.`Pidentifier`,`A`.`Classname`,`A`.`Pname`
                         from $shippingMethods as `A`";
        return $wpdb->get_results($Query, OBJECT);
    }
    
    function get_list_shipping_methods() {
        global $wpdb;
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingOptions';
        $Query = "select `A`.`id`,`A`.`Pidentifier`,`A`.`Classname`,`A`.`Pname`
                         from $shippingMethods as `A`";
        $results=$wpdb->get_results($Query, OBJECT);
        $return=array();
        foreach ($results as $result) {
            $return[$result->Classname]=$result->Pname;
        }
        return $return;
//        global $wpdb;
//        $debug = 1;
//        $data = get_option('myprefix_options', 'none');
//        $link = get_Logistralinks();
//
//        $crg_consignment_url = "{$link}/consignments.xml";
//        $crg_transport_url = "{$link}/transport_agreements.xml";
//
//        if ($key && $senderid) {
//            $crg_api_key = $key;
//            $crg_sender_id = $senderid;
//        } else {
//            $crg_api_key = $data['apikey'];
//            $crg_sender_id = $data['senderid'];
//        }
//
//        require_once LogistraAPIPluginPath . 'include/cargonizer.php';
//        $crg_xml = new CRG_Xmlwriter();
//        $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);
//        $xml = $crg->requestTransportAgreements($crg_transport_url);
//        $xmldoc = new SimpleXMLElement($xml);
//        $ResArr = array();
//        $i = 0;
//        $return=array();
//
//        foreach ($xmldoc as $value) {
//            
//            foreach ($value->products->product as $product):
//                $class_name = ucwords(str_replace(array('_', '-'), ' ', $product->identifier));
//                $class_name = (str_replace(array(' '), '', $class_name));
//                $return[$class_name]=$product->name;
//            endforeach;
//        }
//        return $return;
    }

    function getShippingMethods($key, $senderid) {

        global $wpdb;
        $debug = 1;
        $data = get_option('myprefix_options', 'none');
        $link = get_Logistralinks();

        $crg_consignment_url = "{$link}/consignments.xml";
        $crg_transport_url = "{$link}/transport_agreements.xml";

        if ($key && $senderid) {
            $crg_api_key = $key;
            $crg_sender_id = $senderid;
        } else {
            $crg_api_key = $data['apikey'];
            $crg_sender_id = $data['senderid'];
        }

        require_once LogistraAPIPluginPath . 'include/cargonizer.php';
        $crg_xml = new CRG_Xmlwriter();
        $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);
        $xml = $crg->requestTransportAgreements($crg_transport_url);
        $xmldoc = new SimpleXMLElement($xml);
        $ResArr = array();
        $i = 0;

        foreach ($xmldoc as $value) {
            $id = $value->id;
            $desc = $value->description;
            $numb = $value->number;
            $name = $value->carrier->name;
            $class_name = str_replace(array(' ', '/'), '', $name);
            $identify = $value->carrier->identifier;
            $sql_methods = "INSERT INTO $wpdb->prefix" . "LogistraTAShippingMethods(`TAID`,`TADesc`,`Cname`,`Cidentifier`,`ShippingClass`) VALUES( " . $id;

            $sql_methods .= ",'" . $desc . "', '" . $name . "','" . $identify . "','" . $class_name . "')";

            $Query = "Select * from $wpdb->prefix" . "LogistraTAShippingMethods";

            $Res = $wpdb->get_results($Query);

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql_methods);
            $methodid = $wpdb->insert_id;
            foreach ($value->products->product as $product):
                $class_name = ucwords(str_replace(array('_', '-'), ' ', $product->identifier));
                $class_name = (str_replace(array(' '), '', $class_name));
                $sql_shipping = "INSERT INTO $wpdb->prefix" . "LogistraTAShippingOptions(`methodid`,`Pidentifier`,`Classname`,`Pname`,`minItems`,`maxItems`,`maxWeight`,`requireWeight`,`dangerousGoods`,`requireTermsDelivery`,`ConsigneeFreightPayer`,`deliveryAddress`,`requiresServicePartner`,`requiresCFreightCustomer`,`pickupaddress`,`requirespickupaddress`,`agentnumberrequired`,`agentnumberpossible`,`Cfreight`,`ThirdPartyfreight`,`Termsdelivery`) VALUES( " . $methodid;
                $sql_shipping .= ",'" . $product->identifier . "', '" . $class_name . "','" . $product->name . "','" . $product->min_items . "'";
                $sql_shipping .= ",'" . $product->max_items . "', '" . $product->max_weight . "', '" . $product->requires_weight_or_volume . "'";
                $sql_shipping .= ",'" . $product->dangerous_goods_possible . "', '" . $product->requires_terms_of_delivery . "', '" . $product->consignee_freight_payer_possible . "'";
                $sql_shipping .= ",'" . $product->delivery_address_possible . "', '" . $product->requires_service_partner . "', '" . $product->requires_consignee_freight_payer_customer_number . "'";
                $sql_shipping .= ",'" . $product->pickup_address_possible . "', '" . $product->requires_pickup_address . "', '" . $product->agent_number_required . "'";
                $sql_shipping .= ",'" . $product->agent_number_possible . "', '" . $product->consignee_freight_payer_required . "', '" . $product->third_party_freight_payer_required . "'";
                $sql_shipping .= ",'" . $product->terms_of_delivery_possible . "')";

                dbDelta($sql_shipping);
                $shippingid = $wpdb->insert_id;

                foreach ($product->item_types->item_type as $package):
                    $sql_package = "INSERT INTO $wpdb->prefix" . "LogistraTAShippingPackages(`shippingid`,`pack_en`,`pack_no`,`pack_abber`,`pack_id`) VALUES( " . $shippingid;
                    $sql_package .= ",'" . $package['name_en'] . "', '" . $package['name_no'] . "', '" . $package['abbreviation'] . "', '" . $package['id'] . "')";
                    dbDelta($sql_package);
                endforeach;

                $json = json_encode($product);
                $_Porduct_array = json_decode($json, TRUE);

                if (isset($_Porduct_array["services"])) {
                    $is_single = (array_key_exists(0, $_Porduct_array["services"]["service"])) ? TRUE : FALSE;
                    if ($is_single) {
                        $Pidentifier = $_Porduct_array["identifier"];
                        foreach ($_Porduct_array["services"]["service"] as $value) {
                            $kid = $amount = $currency = $account_number = "false";
                            $min_total = 0;
                            $description = '';
                            $serviceIdentifier = $value["identifier"];
                            $serviceName = $value["name"];
                            if (isset($value["attributes"])) {
                                foreach ($value["attributes"]["attribute"] as $at) {
                                    if (in_array($at["identifier"], array("kid", "amount", "currency", "account_number", "min_total", "description"))) {
                                        if ($at["identifier"] == "currency") {
                                            ${$at[identifier]} = $at["values"]["value"];
                                        } else {
                                            ${$at[identifier]} = $at["required"];
                                        }
                                    }
                                }
                            }
                            $sql_services = "INSERT INTO $wpdb->prefix" . "logistratashippingservices (`Pidentifier`,`serviceIdentifier`,`serviceName`,`kid`,`amount`,`currency`,`account_number`,`min_total`,`description`) VALUES ( '$Pidentifier','$serviceIdentifier','$serviceName','$kid','$amount','$currency','$account_number','$min_total','$description')";

                            dbDelta($sql_services);
                        }
                    } else {
                        $Pidentifier = $_Porduct_array["identifier"];
                        $kid = $amount = $currency = $account_number = "false";
                        $min_total = 0;
                        $description = '';
                        $serviceIdentifier = $_Porduct_array["services"]["service"]["identifier"];
                        $serviceName = $_Porduct_array["services"]["service"]["name"];
                        if (isset($_Porduct_array["attributes"])) {
                            foreach ($_Porduct_array["attributes"]["attribute"] as $at) {
                                if (in_array($at["identifier"], array("kid", "amount", "currency", "account_number", "min_total", "description"))) {
                                    if ($at["identifier"] == "currency") {
                                        ${$at[identifier]} = $at["values"]["value"];
                                    } else {
                                        ${$at[identifier]} = $at["required"];
                                    }
                                }
                            }
                        }
                        $sql_services = "INSERT INTO $wpdb->prefix" . "logistratashippingservices (`Pidentifier`,`serviceIdentifier`,`serviceName`,`kid`,`amount`,`currency`,`account_number`,`min_total`,`description`) VALUES ( '$Pidentifier','$serviceIdentifier','$serviceName','$kid','$amount','$currency','$account_number','$min_total','$description')";
                        dbDelta($sql_services);
                    }
                }
            endforeach;
        }
    }

    add_filter('woocommerce_cart_shipping_method_full_label', 'remove_free_label', 10, 2);

    function remove_free_label($full_label, $method) {

        $full_label = str_replace("kr", "0", $full_label);
        $full_label = str_replace("(GRATIS)", "kr 0 ", $full_label);
        $full_label = str_replace("(Gratis)", "kr 0 ", $full_label);

        return $full_label;
    }

    add_action('woocommerce_checkout_process', 'check_sub_shipping_errors');

    function check_sub_shipping_errors() {
        if (WC()->session->get("subshippingerrors")) {
            foreach (WC()->session->get("subshippingerrors") as $e) {
//          wc_add_notice(__($e), 'error');
                 writetolog("Error | ".$e);
            }
        }

//      sub_shipping_method
        $shipping_method = WC()->session->get('chosen_shipping_methods');
        $shipping_method = (!empty($shipping_method) && isset($shipping_method[0])) ? $shipping_method[0] : '';
        global $wpdb;
        $result = $wpdb->get_results("SELECT ShippingClass FROM {$wpdb->prefix}LogistraTAShippingMethods", ARRAY_A);
        if (!empty($result) && !empty($shipping_method)) {
            $available_shipping_method = array();
            foreach ($result as $value) {
                $available_shipping_method[] = $value['ShippingClass'];
            }
        }
        if (!empty($result) && !empty($shipping_method) && in_array($shipping_method, $available_shipping_method) && !$_POST['sub_shipping_method']) {
            wc_add_notice(__("<strong>ERROR: </strong>Invalid shipping method"), 'error');
            writetolog("Error | Invalid shipping method");
        }
    }

    /*
     * method to find if shipping has 
     * roles based prices
     * 
     * @param $method_id -> id of the shipping method
     */

    function is_role_based($method_id) {
        $shippingOptions = get_option('woocommerce_' . $method_id . '_settings');

        if ($shippingOptions['is_role'] == 'yes' && is_user_logged_in()) {

            return true;
        } else {
            return false;
        }
    }

 /*
     * pass the method id to get roles for logged in user
     * 
     * 
     */

    function role_based_cost($method_id) {
        global $wp_roles, $current_user;



        if (is_user_logged_in()) {
            $user = new WP_User($current_user->ID);

            $role = $user->roles;

            $shippingprices = get_option('woocommerce_' . $method_id . '_settings');


            $prices = $shippingprices[ucfirst($role[0])];
            //print_r($method_id);
            //   print_r($shippingprices);exit();
            return $prices;
        } else {

            return false;
        }
    }

     /*
     * method to find if shipping has 
     * shipping classes
     * 
     * @param $method_id -> id of the shipping method
     */

    function is_shippingclass_based($method_id) {

        $shippingOptions = get_option('woocommerce_' . $method_id . '_settings');
        $cart=WC()->cart->get_cart();

        $shipping_classes=array();
         
            foreach($cart as $cart_item){

               if(!in_array($cart_item['data']->get_shipping_class(),$shipping_classes)){
                if($cart_item['data']->get_shipping_class() == ""){
                   // array_push($shipping_classes,'no_shipping');
                }
                else{
                        array_push($shipping_classes,$cart_item['data']->get_shipping_class());
                }
                    
                }

               }
       if(!empty($shipping_classes))  {
          if ($shippingOptions['enable_shipping_class'] == 'yes' && is_user_logged_in())  {

                return true;
            }
            
         else {
            return false;
        }

    }else{

        return false;
    }
    }

     /*
     * pass the method id to get shipping cost
     * 
     * 
     */

    function shippingclass_based_cost($method_id) {
      
$_tax = new WC_Tax();
        if (is_user_logged_in()) {
          //$shipping_classes = get_terms( 'product_shipping_class', 'hide_empty=0' );
          $shipping_arr=array();
          $prices_arr=array();
          $cart=WC()->cart->get_cart();

            $shipping_classes=array();
           
            foreach($cart as $cart_item){

                    
                    
                    $shipping_classes[$cart_item['product_id']]['class']=$cart_item['data']->get_shipping_class();
                    $shipping_classes[$cart_item['product_id']]['quantity']=$cart_item['quantity'];
                
               
               }
         
            //print_r($shipping_classes);

            $shippingprices = get_option('woocommerce_' . $method_id . '_settings');
            
            foreach($shipping_classes as $key =>  $arr){


                if($shipping_classes[$key]['class'] != ""){

                    $cost+=$shipping_classes[$key]['quantity']*$shippingprices[$shipping_classes[$key]['class']];
                    
                }else{

                    $cost+=$shipping_classes[$key]['quantity']*$shippingprices['cost'];
                }


            }
                

               $roi=$_tax -> get_shipping_tax_rates();
            
               $tax = ($roi[1][rate] / 100) * $cost;
              
            

                $prices = $tax + $cost;

 
            
            return $prices;
        } else {

            return false;
        }
    }

   
    

    /* FOR Gratis Frakt
     * 
     * 
     */

    function register_shortcodes() {
        add_shortcode('gratis-frakt', 'check_if_free_shippping');
    }

    add_action('init', 'register_shortcodes');

//add_action('woocommerce_add_to_cart', 'register_shortcodes');
    function check_if_free_shippping() {
        
        if(get_option('woocommerce_minamount_logistra') == 'no'){
            return;
        }

        global $woocommerce;
        $total = $woocommerce->cart->subtotal;
        global $wpdb;
        //$myrow = $wpdb->get_results( "SELECT Value FROM Logistra WHERE Name='free'" );
//        $selection_priority = get_option('woocommerce_shipping_method_selection_priority', array());
//        $prioritized_methods = array();
//        foreach ($selection_priority as $method_id => $method) {
//            $priority = isset($selection_priority[$method_id]) ? absint($selection_priority[$method_id]) : 1;
//            if (empty($prioritized_methods[$priority])) {
//                $prioritized_methods[$priority] = array();
//            }
//            $prioritized_methods[$priority][$method_id] = $method->cost;
//        }
//        ksort($prioritized_methods);
//        $prioritized_methods = current($prioritized_methods);
//        asort($prioritized_methods);

        $chosen_methods = WC()->session->get('chosen_shipping_methods');
        $chosen_shipping = $chosen_methods[0];
//        $shipping_class = current(array_keys($prioritized_methods));

        $x = WC()->shipping->load_shipping_methods();
        $chosen_shipping_id=explode(":",$chosen_shipping);
        
        /*
         * Class name for default woocomm shipping methods hardcoded, since can't be get from id. Differenece in naming convention.
         */
        if($chosen_shipping_id[0] == 'flat_rate'){
            $obj=new WC_Shipping_Flat_Rate($chosen_shipping_id[1]);
        }
        elseif($chosen_shipping_id[0] == 'free_shipping'){
            $obj=new WC_Shipping_Free_Shipping($chosen_shipping_id[1]);
        }
        elseif($chosen_shipping_id[0] == 'local_pickup'){
            $obj=new WC_Shipping_Local_Pickup($chosen_shipping_id[1]);
        }
        else{
            if(class_exists($chosen_shipping_id[0]))
            $obj=new $chosen_shipping_id[0]($chosen_shipping_id[1]);
        }
        if($obj){
            $val = $obj->get_option('min_total');
        }
        else{
            $val=0;
        }

        

        $loader = '<img class="gratisloader" style="display:none;" src="' . get_template_directory_uri() . '/img/gratisloader.GIF">';
        if ($val == 0) {
            return "<div style='color:#000; float:right; ' class='gratis'>Du HAR <span style='color:#25a6d6; font-weight:bold;'> Ingen Gratis levering</span> </div>";
        } else if ($total < $val)
            return "<div style='color:#000; float:right;' class='gratis'>DU HAR <span style='color:#25a6d6; font-weight:bold;'>" . ($val - $total) . "</span> KR IGJEN TIL <span style='color:#25a6d6; font-weight:bold;'>GRATIS FRAKT!</span></div>";
        else if ($total > $val)
            return "<div style='color:#000; float:right; ' class='gratis'>Du HAR <span style='color:#25a6d6; font-weight:bold;'>GRATIS FRAKT!</span> </div>";
        else
            return "<div style='color:#000; float:right; ' class='gratis'>Du HAR <span style='color:#25a6d6; font-weight:bold;'>500</span> KR TIL <span style='color:#25a6d6; font-weight:bold;'>GRATIS FRAKT!</span> </div>";
    }

    /*
     * CHECK IF A METHOD HAS FREE SHIPPING
     * 
     * @param $methodID for per shipping
     */

    function is_free_shipping($methodId) {
        global $woocommerce;

        $total = $woocommerce->cart->subtotal;



        $x = WC()->shipping->load_shipping_methods();

        $val = $x[$methodId]->settings['min_total'];
        // echo $total."|".$val;
        if ($total < $val || $val == 0) {
            return 0; //no free shipping
        } elseif ($total > $val) {
            return 1; //free shipping
        }
    }

    /* Adjust shipping rates based on roles
     * 
     * 
     * 
     */

    function adjust_shipping_rate($rates) {

        $chosen_methods = WC()->session->get('chosen_shipping_methods');

        $chosen_shipping = $chosen_methods[0];

$_tax = new WC_Tax();
print_r($_tax->get_rates());
exit;

        foreach ($rates as $rate) {
            print_r($rate->cost);
            print_r($_tax->calc_tax($rate->cost, $rates, FALSE, FALSE));

//            if (is_free_shipping($rate->id) == 1) {
//              
//                $rate->cost = 0;
//                $rate->taxes[1] = 0;
//            } elseif (is_role_based($rate->id)) {
//
//                $rate->cost = role_based_cost($rate->id) + $rate->taxes[1];
//            }
//            elseif (is_shippingclass_based($rate->id)) {
//               
//                $price=shippingclass_based_cost($rate->id);
//                /*echo $tax= (25 / 100) * $price;
//                echo shippingclass_based_cost($rate->id);*/
//$rate->cost = 0;
//$rate->taxes[1] = 0;
//               $cost = shippingclass_based_cost($rate->id);
//       
//              
//            
//
//                $rate->cost = $cost;
//
//              
//
//            }
        }
        exit;
     
        return $rates;
    }

    //add_filter('woocommerce_package_rates', 'adjust_shipping_rate', 10, 1);

    add_action("wp_ajax_check_call", "check_latest_value");
    add_action("wp_ajax_nopriv_check_call", "check_latest_value");

    function check_latest_value() {
        global $woocommerce;
        // print_r("checkcall");exit();

        ob_start();
        ?>
    <a href="<?php echo $woocommerce->cart->get_cart_url() ?>" class="homecart" id="totalMiniCart">
        <div id="miniCartLeft">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div id="miniCartRight">
            <strong>Handlekurv</strong>
    <?php
    echo $woocommerce->cart->get_cart_total();
    ?>
        </div>
    </a>

    <?php
    //  echo check_if_free_shippping();
    $fragments = ob_get_clean();

    ob_start();
    $index = 0;
    $shipping_methods = WC()->shipping->load_shipping_methods();

    $chosen_methods = WC()->session->get('chosen_shipping_methods');

    $chosen_shipping = $chosen_methods[0];

    foreach ($shipping_methods as $key => $value) {




        $method_id = $key;

        $val = $shipping_methods[$method_id];

        $roleBased = is_role_based($method_id);
        

        $methodLabel = $shipping_methods[$method_id]->method_title;
        $description = fetch_description($method_id);

        if ($val->settings['enabled'] == 'yes') {
            if (is_free_shipping($method_id) == 1) {//IF FREE SHIPPING IS OFFERED
                ?>
                <li>

                    <input type="radio" name="shipping_method[<?php echo $index; ?>]" <?php if ($method_id === $_POST['method']) { ?> checked <?php } ?> data-index="<?php echo $index; ?>" id="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>" value="<?php echo esc_attr($method_id); ?>" <?php checked($method_id, $chosen_method); ?> class="shipping_method" />
                    <label for="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>"><?php echo $methodLabel ?>:<span class="amount">kr 0</span></label><?php echo $description; ?>

                </li>
                <?php
            } elseif (is_role_based($method_id)) {//IF USER LOGGED IN AND PRICE FOR ROLES
                $price = role_based_cost($method_id);
                $total = (25 / 100) * $price;
                $price = $total + $price;
                ?>
                <li>

                    <input type="radio" name="shipping_method[<?php echo $index; ?>]" data-index="<?php echo $index; ?>" <?php if ($method_id === $_POST['method']) { ?> checked <?php } ?> id="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method->id); ?>" value="<?php echo esc_attr($method_id); ?>" <?php checked($method_id, $chosen_method); ?> class="shipping_method" />
                    <label for="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>"><?php echo $methodLabel ?>:<span class="amount"><?php echo ($price > 0) ? 'kr&nbsp;' . floor($price) : '--'; ?></span></label><?php echo $description; ?>

                </li>
                <?php
            }
                elseif (is_shippingclass_based($method_id)) {//IF USER LOGGED IN AND PRICE FOR ROLES
                $price = shippingclass_based_cost($method_id);
               /*$total = (25 / 100) * $price;
                $price = $total + $price;*/
                ?>
                <li>

                    <input type="radio" name="shipping_method[<?php echo $index; ?>]" data-index="<?php echo $index; ?>" <?php if ($method_id === $_POST['method']) { ?> checked <?php } ?> id="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method->id); ?>" value="<?php echo esc_attr($method_id); ?>" <?php checked($method_id, $chosen_method); ?> class="shipping_method" />
                    <label for="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>"><?php echo $methodLabel ?>:<span class="amount"><?php echo ($price > 0) ? 'kr&nbsp;' . floor($price) : '--'; ?></span></label><?php echo $description; ?>

                </li>
                <?php
            }
             else { //NORMAL LOOP   
                $price = $shipping_methods[$method_id]->settings['cost'];
                $total = (25 / 100) * $price;
                $price = $total + $price;
                ?>
                <li>

                    <input type="radio" name="shipping_method[<?php echo $index; ?>]" data-index="<?php echo $index; ?>" <?php if ($method_id === $_POST['method']) { ?> checked <?php } ?> id="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>" value="<?php echo esc_attr($method_id); ?>" <?php checked($method_id, $chosen_method); ?> class="shipping_method" />
                    <label for="shipping_method_<?php echo $index; ?>_<?php echo sanitize_title($method_id); ?>"><?php echo $methodLabel ?>:<span class="amount"> <?php echo ($price > 0) ? 'kr&nbsp;' . floor($price) : '--'; ?></label><?php echo $description; ?>

                </li>
                <?php
            }
            if ($method_id === $_POST['method']) {

                if (is_free_shipping($method_id) == 1) {

                    $val = 0;
                } else {
                    $val = $x[$chosen_shipping]->settings['cost'];
                }
            }
        }
    }
    $shipping_fragments = ob_get_clean();


    $freeshipping = update_gratis_frakt();

    echo json_encode(array('fragments' => trim($fragments), 'gfragments' => check_if_free_shippping(), 'shipping_fragments' => $shipping_fragments, 'chossen' => $chosen_shipping, 'freeshipping' => $freeshipping, 'shipping_cost' => $price));
    exit();
}

function add_our_script() {

    wp_register_script('ajax_url', admin_url('admin-ajax.php'), array('jquery'), '', true);
    wp_localize_script('ajax_url', 'ajax_params', array('ajax_url' => admin_url('admin-ajax.php')));
    wp_enqueue_script('ajax_url');
}

add_action('wp_enqueue_scripts', 'add_our_script');

//add_filter('woocommerce_add_to_cart_fragments', 'update_gratis_frakt');

function update_gratis_frakt() {
    global $woocommerce;
    ob_start();

    echo check_if_free_shippping();

    $fragments['div.gratis'] = ob_get_clean();


    return $fragments;
}

add_action('woocommerce_before_cart_totals','gratis_frakt_text',10,0);
function gratis_frakt_text(){
   echo do_shortcode( '[gratis-frakt]' );
}

/* ADD TRACKING URL TO EMAIL
 * 
 * 
 */



add_action('woocommerce_email_after_order_table', 'wdm_add_shipping_method_to_order_email', 10, 2);

function wdm_add_shipping_method_to_order_email($order, $status) {
    $tracking_url = get_post_meta($order->id, '_tracking-url', true);

    if ($tracking_url) {
        ?>
        <span><a href="<?php echo $tracking_url ?>" target="_blank">Spor forsendelsen din</a></span>
        <?php
    }
}

/*
 * FETCH DESCRIPTION OF SHIPPING METHOD
 * 
 */

function fetch_description($method_id) {
    $x = WC()->shipping->load_shipping_methods();
    $val = $x[$method_id]->settings['description'];
    if ($val != '')
        return '<span id="' . $method_id . '" class="method_desc"><i class="fa fa-question-circle"></i></span><a id="' . $method_id . '_desc" class="method_desc_block" data-toggle="tooltip" data-placement="top" title="' . $val . '" style="display:none;">' . $val . '</a>';
    else
        return '';
}




/*
 * Option to send to logistra manually
 */

add_action('wp_ajax_retur_request_to_logistra', 'retur_request_to_logistra');

function retur_request_to_logistra() {
    global $wpdb;
    $order = new WC_Order($_POST['id']);
    $serviceId = get_post_meta($_POST['id'], 'servicePartnerID', true);
    $retur_options = get_option('logistra_retur_settings');
    $data = get_option('myprefix_options', 'none');
    if ($data) {
        $crg_api_key = $data['apikey'];
        $crg_sender_id = $data['senderid'];
    } else {
        $crg_api_key = '';
        $crg_sender_id = '';
    }
    $subShipping = get_post_meta($order->id, 'sub_shipping_method', true);
    $subShippingInfo = get_shipping($subShipping);
    $link = get_Logistralinks();
    $crg_consignment_url = "{$link}/consignments.xml";
    $crg_transport_url = "{$link}/transport_agreements.xml";

    $crg_xml = new CRG_Xmlwriter();
    $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);

    $Q = "SELECT SM.TAID,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM 
              JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
              WHERE SO.Pidentifier = '$subShippingInfo->Pidentifier'";
    $results = $wpdb->get_results($Q);
    $consignee = array(
        'name' => $order->shipping_company?$order->shipping_company:$order->shipping_first_name.' '.$order->shipping_last_name,
        'country' => $order->shipping_country,
        'postcode' => $order->shipping_postcode,
        'city' => $order->shipping_city,
        "address1" => $order->shipping_address_1,
        "address2" => $order->shipping_address_2,
        "email" => $order->billing_email,
        "phone" => $order->billing_phone,
        "mobile" => $order->billing_phone,
        "contact_person" => $order->shipping_first_name,
    );
    $items[] = array(
        "item" => array(
            "_attribs" => array(
                "type" => "package",
                "amount" => "1",
                "weight" => $retur_options['retur_weight'],
                "description" => "",
            )
        )
    );
    foreach ($results as $id) {
        $requiresServicePartner = $id->requiresServicePartner;
        $id = $id->TAID;
        $crg_data['consignments'] = array(
            "consignment" => array(
                "_attribs" => array(
                    "transport_agreement" => $id,
                ),
                "product" => $retur_options['retur_id'],
                "parts" => array(
                    "consignee" => $consignee,
                ),
                "items" => $items
            ),
        );
        if ($retur_options['retur_id'] == 'mypack_return' || $retur_options['retur_id'] == 'helthjem_mypack_return') {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $consignee[country] . '&postcode=' . $consignee[postcode] . '');
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            $headers = array(
                "X-Cargonizer-Key:" . $crg_api_key,
                "X-Cargonizer-Sender:" . $crg_sender_id,
                "Content-type:application/xml",
                "charset:utf-8"
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($curl);

            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $service_partners = json_decode($json, TRUE);
            $service_partners_info = array();
            if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {

                $service_partner = $service_partners['service-partners']['service-partner'];
                $partner = $service_partner[$serviceId];

                $service_partners_info[] = array(
                    "service-partner" => array(
                        "number" => (isset($partner["number"])) ? $partner["number"] : '',
                        "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                        "name" => (isset($partner["name"])) ? $partner["name"] : '',
                        "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                        "country" => (isset($partner["country"])) ? $partner["country"] : '',
                        "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                        "city" => (isset($partner["city"])) ? $partner["city"] : '',
                    )
                );

                $service_partner = array(
                    "number" => (isset($partner["number"])) ? $partner["number"] : '',
                    "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                    "name" => (isset($partner["name"])) ? $partner["name"] : '',
                    "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                    "country" => (isset($partner["country"])) ? $partner["country"] : '',
                    "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                    "city" => (isset($partner["city"])) ? $partner["city"] : '',
                );



                $crg_data['consignments']["consignment"]["parts"]["service_partner"] = $service_partner;
            }
        }
    }
    $crg->requestConsignment($crg_data, $debug, $crg_consignment_url);
    
    $result_xml = $crg->getResultXml();
    if ($result_xml->errors) {
        $status = 0;
    } else {
        if (isset($result_xml->consignment->{'number'})) {
            update_post_meta($order->id, '_retur_tracking_number', (string) $result_xml->consignment->{'tracking-url'});
        }
        if (isset($result_xml->consignment->{'consignment-pdf'})) {
            $file_array = array();
            $file_array['name'] = 'retur_consignment_pdf' . $result_xml->consignment->{'id'}.'.pdf';
            $file_array['tmp_name'] = download_url($result_xml->consignment->{'consignment-pdf'});
            $overrides = array('test_form'=>false);
            $file = wp_handle_sideload($file_array, $overrides);
            $curlSession = curl_init(); 
                  
		curl_setopt($curlSession, CURLOPT_VERBOSE, 1); 
		curl_setopt($curlSession, CURLOPT_HEADER, 0); 
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1); 
                curl_setopt($curlSession, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($curlSession, CURLOPT_URL, $result_xml->consignment->{'consignment-pdf'});
		curl_setopt($curlSession, CURLOPT_POST, 0);
		curl_setopt($curlSession, CURLOPT_CUSTOMREQUEST, "GET");
		$headers = array(
			"X-Cargonizer-Key:".$crg_api_key,
			"X-Cargonizer-Sender:".$crg_sender_id,
			"Content-type:application/pdf",
			"Content-length:0",
                        "charset:utf-8"
		);
		curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($curlSession);
            curl_close($curlSession);
            $attachment = array(
                'post_mime_type' => $file['type'],
                'post_title' => $file_array['name'],
                'guid' => $url,
                'post_content' => '',
                'post_status' => 'publish'
            );
            $attachment_id = wp_insert_attachment($attachment, $file['file']);
            file_put_contents(get_attached_file( $attachment_id ), $response);
            update_post_meta($order->id, '_retur_consignment_pdf', wp_get_attachment_url( $attachment_id ));
            $consignment_pdf = (string) wp_get_attachment_url( $attachment_id );
        }
        $status = 1;
        
        $body = $retur_options['retur_email'];
        $body .= '<br/><br/><a target="_blank" href="' . $consignment_pdf . '"> Last ned returetiketten her </a>';
        $body .= '<br/><br/>Med vennlig hilsen';
        $body .= '<br/>' . get_bloginfo("name");
        $to = $order->billing_email;
        $subject = __('Returetikett fra ' . get_bloginfo("name"), 'woocommerce');
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From: ' . get_bloginfo("name") . ' <' . get_bloginfo("admin_email") . '>';

        wp_mail($to, $subject, $body, $headers);
    }
    echo $status;
    die();

}

add_action('wp_ajax_gf_to_logistra', 'send_to_logistra');

function send_to_logistra() {
//            error_reporting(-1);
//ini_set('display_errors', 'On');
    $data = parse_str($_POST['data'], $formdata);
    //$meta_value = gform_get_meta($formdata['consignor'],'tracking_link');
    //  print_r($meta_value);exit();
    // print_r($formdata);exit();
    $returndata = LogistraAPI::update_status_to_ready_to_shipping($_POST['methods'], $formdata, $form);
    //print_r($returndata);
    //$returndata=gform_update_meta( $formdata['consignor'], 'tracking_link', $tracinglink );

    $decodeData = json_decode($returndata);


    if ($decodeData->status == '1') {

        $update = '0';
    } else {

        $update = '1';
        if (!add_post_meta($_POST['id'], '_tracking-url', $decodeData->message, true)) {
            update_post_meta($_POST['id'], '_tracking-url', $decodeData->message);
        }
    }


    echo $update;
    die();
}

add_action("wp_ajax_add_user_details", "add_user_details");
    add_action("wp_ajax_nopriv_add_user_details", "add_user_details");
function add_user_details() {
$data=$_REQUEST['data'];
foreach($data as $key => $dat){

if(!get_option("woocommerce_".$key."_logistra")){
 
add_option("woocommerce_".$key."_logistra",$dat);

}else{


update_option("woocommerce_".$key."_logistra",$dat);


}
}

writetolog("Info | User Details Updated");


}
function custom_meta_box_markup() {
    ?><div class="postbox" id="notifications_container">
        <h3 style="cursor:default;"><span><?php _e('Logistra', 'gravityforms'); ?></span></h3>
        <div id="logistra_message"></div>
        <div class="inside">

            <div>
    <?php
    // print_r($lead[22]);exit();
    $notifications = get_shipping_methods();
    $order_id = get_the_ID();
    // print_r($lead);exit();
    if (get_post_meta($order_id, 'sub_shipping_method', true)) {
        ?>

                    <div id="selected_shipping"><?php echo get_post_meta($order_id, 'sub_shipping_method', true) ?></div>





                    <input type="button" value="Send to Logistra" onclick="sendtologistra()"/>
                    <div id="logistra_details">
                        <input type="hidden" id="log_order_id" name="log_order_id" value="<?php echo $order_id; ?>"/> 
                        <input type="hidden" id="logistra_methods" name="logistra_methods" value="<?php echo get_post_meta($order_id, 'sub_shipping_method', true); ?>"/>
                        <input type="hidden" name="first_name" id="first_name" value="<?php echo get_post_meta($order_id, '_billing_first_name', true); ?>"/>
                        <input type="hidden" name="addresss1" id="addresss1" value="<?php echo get_post_meta($order_id, '_billing_address_1', true); ?>"/>
                        <input type="hidden" name="address2" id="address2" value="<?php echo get_post_meta($order_id, '_billing_address_2', true); ?>"/>
                        <input type="hidden" name="country" id="country" value="Norway"/>
                        <input type="hidden" name="postcode" id="postcode" value="<?php echo get_post_meta($order_id, '_billing_postcode', true); ?>"/>
                        <input type="hidden" name="city" id="city" value="<?php echo get_post_meta($order_id, '_billing_city', true); ?>"/>
                        <input type="hidden" name="email" id="email" value="<?php echo get_post_meta($order_id, '_billing_email', true); ?>"/>
                        <input type="hidden" name="phone" id="phone" value="<?php echo get_post_meta($order_id, '_billing_phone', true); ?>"/>
                        <input type="hidden" name="contact_person" id="contact_person" value="<?php echo get_post_meta($order_id, '_billing_first_name', true); ?>"/>
                        <input type="hidden" name="consignee" id="consignee" value=""/>
                        <input type="hidden" name="consignor" value="<?php echo $order_id; ?>"/>
                    </div>
    <?php
    } else {
        ?>
                    <div id="response_message" style="color:red">No shipping method selected.</div>
        <?php
    }
    ?>
            </div>
            <div id="response_message" style="color:red"></div>
        </div>
    </div><?php
            }

            function add_custom_meta_box() {
                add_meta_box("demo-meta-box", "Send to logistra", "custom_meta_box_markup", "shop_order", "side", "low", null);
            }

            add_action("add_meta_boxes", "add_custom_meta_box");



function writetolog($info){

   // $myfile = fopen(Logpath, "wb") or die("Unable to open file!");
$current = file_get_contents(Logpath);
$new=$info. PHP_EOL;
file_put_contents(Logpath,date('Y-m-d &\nb\sp;&\nb\sp; @ &\nb\sp;&\nb\sp;H:i:s')." | ".$new,FILE_APPEND);

//fclose($myfile);
}
function writetolog1($info){

   // $myfile = fopen(Logpath, "wb") or die("Unable to open file!");
$current = file_get_contents(Logpath1);
$new=$info. PHP_EOL;
file_put_contents(Logpath1,date('Y-m-d &\nb\sp;&\nb\sp; @ &\nb\sp;&\nb\sp;H:i:s')." | ".$new,FILE_APPEND);

//fclose($myfile);
}
function add_country_filter_for_servicepartener() {
            if ('no' === get_option('woocommerce_enable_shipping_calc')) {
                ?>
        <tr class="servicepartener">
	<th><?php echo 'Service Parter calculation'; ?></th>
	<td data-title="<?php echo 'Service Parter calculation'; ?>">
                        <p class="form-row form-row-wide" id="calc_shipping_country_field">
                			<select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state" rel="calc_shipping_state">
                				<option value=""><?php _e('Select a country&hellip;', 'woocommerce'); ?></option>
                            <?php
                            foreach (WC()->countries->get_shipping_countries() as $key => $value)
                                echo '<option value="' . esc_attr($key) . '"' . selected(WC()->customer->get_shipping_country(), esc_attr($key), false) . '>' . esc_html($value) . '</option>';
                            ?>
                			</select>
                		</p>
                                <p class="form-row form-row-wide" id="calc_shipping_postcode_field">
        				<input type="text" class="input-text" value="<?php echo esc_attr(WC()->customer->get_shipping_postcode()); ?>" placeholder="<?php esc_attr_e('Postcode / ZIP', 'woocommerce'); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
        			</p>
                                <p><button type="submit" name="calc_shipping" value="1" class="button"><?php _e( 'Update', 'woocommerce' ); ?></button></p>
        </td>
        <?php
    }
}

//add_action( 'woocommerce_cart_totals_after_shipping', 'add_country_filter_for_servicepartener');
