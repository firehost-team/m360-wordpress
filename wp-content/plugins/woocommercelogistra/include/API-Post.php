<?php
class API_POST {
 public static $api_keys = 'apikey';
 
  function api_initialize() {
        //API_POST::register_api_key();       
        API_POST::include_metaboxes_lib();    
    }

    function include_metaboxes_lib() {
        if (!class_exists('cmb_Meta_Box')) {
            require_once('cmb/init.php');
        }
    }

}