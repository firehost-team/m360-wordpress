<?php

class Api_Ajax {

    public function __construct() {
        add_action('wp_ajax_get_shipping_methods', array(__CLASS__, 'get_shipping_methods'));
        add_action('wp_ajax_nopriv_get_shipping_methods', array(__CLASS__, 'get_shipping_methods'));
        add_action('wp_ajax_get_shipping_methods_cart', array(__CLASS__, 'get_shipping_methods_cart'));
        add_action('wp_ajax_nopriv_get_shipping_methods_cart', array(__CLASS__, 'get_shipping_methods_cart'));
        add_action('wp_ajax_get_cost_estimate', array(__CLASS__, 'get_cost_estimate'));
        add_action('wp_ajax_nopriv_get_cost_estimate', array(__CLASS__, 'get_cost_estimate'));
        add_action('wp_ajax_logistra_license_check', array(__CLASS__, 'logistra_license_check'));
        add_action('wp_ajax_nopriv_logistra_license_check', array(__CLASS__, 'logistra_license_check'));
        add_action('wp_ajax_save_methods', array(__CLASS__, 'save_methods'));
        add_action('wp_ajax_save_methods', array(__CLASS__, 'save_methods'));
        add_action('wp_ajax_change_method_status', array(__CLASS__, 'change_method_status'));
        add_action('wp_ajax_change_method_status', array(__CLASS__, 'change_method_status'));
    }

    function logistra_license_check() {
        parse_str($_POST['post_data'], $params);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://ewn.no/license_response.php');
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
                    curl_setopt($curl, CURLOPT_HEADER, 0);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = array(
            'hostname' => $_SERVER['SERVER_NAME'],
            'license_key' => $params['license']
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $response=curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        writetolog1('response form connection -> '.$response.'  connection info -->'.$httpcode);
        if($response==1 && $httpcode == 200){//license valid
            $options=array(
                'hostname' => $_SERVER['SERVER_NAME'],
            'license_key' => $params['license'],
                'status' => 'active'
            );
            if(!get_option('logistra_api_key'))
            add_option('logistra_api_key', serialize($options));
            else
                update_option ('logistra_api_key', serialize($options));
        }
        if($response==0 && $httpcode == 200){
            //license invalid
            $options=array(
                'hostname' => $_SERVER['SERVER_NAME'],
            'license_key' => $params['license'],
                'status' => 'inactive'
            );
            if(!get_option('logistra_api_key'))
            add_option('logistra_api_key', serialize($options));
            else
                update_option ('logistra_api_key', serialize($options));
        }
                exit;
    }
    
    function save_methods(){
       // if(!get_option('myprefix_options'))
        getShippingMethods($_POST['api_key'], $_POST['id']);
        global $wpdb;
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingOptions';
        $Query = "select `A`.`id`,`A`.`Pidentifier`,`A`.`Classname`,`A`.`Pname`
                         from $shippingMethods as `A`";
        $results=$wpdb->get_results($Query, OBJECT);
        $return=array();
        foreach ($results as $result) {
            if(get_option('woocommerce_'.$result->Classname.'_settings')){

                delete_option('woocommerce_'.$result->Classname.'_settings');
            }
            
            $return[$result->Classname]=$result->Pname;
        }
        print_r(json_encode($return));
        exit;
    }

    /**
        Generate user fields

    **/
        function generate_fields(){


            

        }
    /**
     * Save settings
     */
     function save_shipping() {
        global $current_section;

        $wc_shipping = WC_Shipping::instance();

        if ( ! $current_section ) {
            WC_Admin_Settings::save_fields( $this->get_settings() );
            $wc_shipping->process_admin_options();

        } else {
            foreach ( $wc_shipping->get_shipping_methods() as $method_id => $method ) {
                if ( $current_section === sanitize_title( get_class( $method ) ) ) {
                    do_action( 'woocommerce_update_options_' . $this->id . '_' . $method->id );
                }
            }
        }

        // Increments the transient version to invalidate cache
        WC_Cache_Helper::get_transient_version( 'shipping', true );
    }
    
    function change_method_status(){

        parse_str($_POST['methods'], $params);
        parse_str($_POST['formval'], $formval);
        
        $x = WC()->shipping->load_shipping_methods();
        //print_r(get_option('myprefix_options'));

        //print_r($formval);
       
        update_option('myprefix_options',$formval);
    
        foreach ($params['shipping_method_select'] as $param){
           
$title=$x[$param]->title;
           $shipping=array(
            
        'name'=>$title,
        'enabled'=>yes,
        'cost'=>0,
        'min_total'=>0,
        'description'=>0,
        'is_role'=>no,
        'Administrator'=>0,
        'Editor'=>0,
        'Editor'=>0,
        'Contributor'=>0,
        'Subscriber'=>0,
        'Customer'=>0,
        'Shop Manager'=>0 );


           update_option('woocommerce_'.$param.'_settings',$shipping);
writetolog(date('Y-m-d H:i:s')." | Success | Shipping Method ".$title." Succesfully Saved");
           //echo get_option('woocommerce_'.$param.'_settings');
           print_r(serialize($shipping));
           /*exit;*/

           /* print_r($param);
            $x[$param]->settings['enabled']='yes';

            array_push($methods,$x[$param]->settings['enabled']);
           */ 

        }

        /*$saved_settings=WC_Admin_Settings::save_fields( $methods);

        return $saved_settings;*/

        exit;
    }

    function get_shipping_methods_name($classname) {
        global $wpdb;
        $shippingOptions = $wpdb->prefix . 'LogistraTAShippingOptions';
        $shippingMethods = $wpdb->prefix . 'LogistraTAShippingMethods';
        $Query = "select `A`.`id`,`B`.`TAID`,`B`.`TADesc`,`B`.`TADesc`,`B`.`Cname`,`B`.`Cidentifier`,`B`.`ShippingClass`,`A`.`Pidentifier`,`A`.`Classname`,`A`.`Pname` from $shippingOptions as `A`,$shippingMethods as `B` where A.methodid = B.id AND A.Classname = '" . $classname . "'";
        return $wpdb->get_row($Query);
    }

    function get_shipping_cost($ShippingAllData, $post_data) {
        global $wpdb;
        WC()->session->__unset("subshippingerrors");

        if ($ShippingAllData->Classname) {

            $subshipping_info = "SELECT SM.TAID,SP.pack_abber,SO.Pidentifier,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM
                JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
                JOIN {$wpdb->prefix}LogistraTAShippingPackages AS SP ON SO.id = SP.shippingid
                WHERE SM.ShippingClass = '{$ShippingAllData->ShippingClass}' AND SO.Pidentifier = '$ShippingAllData->Pidentifier'
            ";
            $subshipping_info = $wpdb->get_row($subshipping_info);

            if (!empty($subshipping_info)) {

                $data = get_option('myprefix_options', 'none');
                $link = get_Logistralinks();

                $crg_api_key = $data['apikey'];
                $crg_sender_id = $data['senderid'];
                $crg_consignment_url = "{$link}/consignments.xml";
                $crg_consignment_url = "{$link}/consignment_costs.xml";
                $crg_transport_url = "{$link}/transport_agreements.xml";
                $debug = 1;
                require_once LogistraAPIPluginPath . 'include/cargonizer.php';
                $crg_xml = new CRG_Xmlwriter();
                $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);
                $_key = isset($post_data[ship_to_different_address]) ? 'shipping' : 'billing';
                $consignee = array(
                    "name" => $post_data[$_key . "_first_name"] . ' ' . $post_data[$_key . "_last_name"],
                    "address1" => $post_data[$_key . "_address_1"],
                    "address2" => $post_data[$_key . "_address_2"],
                    "country" => $post_data[$_key . "_country"],
                    "postcode" => $post_data[$_key . "_postcode"],
                    "city" => $post_data[$_key . "_city"],
                    "email" => $post_data[billing_email],
                    "phone" => $post_data[billing_phone],
                );
                /* Prepare Items */
                $items = array();
                foreach (WC()->cart->get_cart() as $cart_item) {
                    $product = get_product($cart_item['product_id']);
                    if ($product->has_weight()) {
                        $weight = $product->get_weight();
                    } else {
                        $weight = 1;
                    }

                    $product_title = $product->get_title();

                    for ($i = 0; $i < $cart_item['quantity']; $i++) {
                        $sku .= $product->get_sku() . ',';
                    }
                    $wt = $cart_item['quantity'] * $weight;
                    $totle_wt = $totle_wt + $wt;
                    $product_title = $product->get_title();
                }

                $items[] = array(
                    "item" => array(
                        "_attribs" => array(
                            "amount" => "1",
                            "description" => "$sku",
                            "type" => "$subshipping_info->pack_abber",
                            "weight" => "$totle_wt"
                        )
                    )
                );

                $crg_data['consignments'] = array(
                    "consignment" => array(
                        "_attribs" => array(
                            "transport_agreement" => $subshipping_info->TAID,
                        ),
                        "product" => $subshipping_info->Pidentifier,
                        "parts" => array(
                            "consignee" => $consignee,
                        ),
                        "items" => $items,
                    ),
                );

                if ($subshipping_info->requiresServicePartner == 'true' || $ShippingAllData->Classname=='BringServicepakke') {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $consignee[country] . '&postcode=' . $consignee[postcode] . '');
                    curl_setopt($curl, CURLOPT_VERBOSE, 1);
                    curl_setopt($curl, CURLOPT_HEADER, 0);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                    $headers = array(
                        "X-Cargonizer-Key:" . $crg_api_key,
                        "X-Cargonizer-Sender:" . $crg_sender_id,
                        "Content-type:application/xml"
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    $response = curl_exec($curl);

                    $xml = simplexml_load_string($response);
                    $json = json_encode($xml);
                    $service_partners = json_decode($json, TRUE);
                    $service_partners_info = array();
                    if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {
                        $service_partner = $service_partners['service-partners']['service-partner'];
                        $partner = $service_partner[$_COOKIE['selected_sub_shipping_method']]; //selected service id

                        $service_partners_info[] = array(
                            "service-partner" => array(
                                "number" => (isset($partner["number"])) ? $partner["number"] : '',
                                "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                                "name" => (isset($partner["name"])) ? $partner["name"] : '',
                                "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                                "country" => (isset($partner["country"])) ? $partner["country"] : '',
                                "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                                "city" => (isset($partner["city"])) ? $partner["city"] : '',
                            )
                        );

                        $service_partner = array(
                            "number" => (isset($partner["number"])) ? $partner["number"] : '',
                            "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                            "name" => (isset($partner["name"])) ? $partner["name"] : '',
                            "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                            "country" => (isset($partner["country"])) ? $partner["country"] : '',
                            "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                            "city" => (isset($partner["city"])) ? $partner["city"] : '',
                        );

                        $crg_data['consignments']["consignment"]["parts"]["service_partner"] = $service_partner;
                    }
                }

                $debug = 0;
                $crg->requestConsignment($crg_data, $debug, $crg_consignment_url);
                $result_xml = $crg->getResultXml();

                global $wpdb;
                $error = '';
                if (!$_COOKIE['sub_shipping_method']) {
                    WC()->session->__unset("subshippingerrors");
                }
                if (isset($result_xml->error)) {
                    setcookie('sub_shipping_cost', '', time() + (86400 * 30), "/");
                    $error = (string) $result_xml->error;

                    if ($error != "FreightCalculator::EstimationNotPossible: No username and password") {
                        $error = '<strong>ERROR: </strong>' . $error;
                        WC()->session->set("subshippingerrors", array("<strong>ERROR: </strong>" . $error));
                    }
                } elseif ($result_xml->consignment->errors->error) {

                    setcookie('sub_shipping_cost', '', time() + (86400 * 30), "/");
                    foreach ($result_xml->consignment->errors->error as $error) {
                        $error = (string) $error;
                        $error.= "<br/><strong>ERROR: </strong>$error<br/>";
                        $_shippingErrors[] = "<strong>ERROR: </strong>" . $error;
                    }

                    WC()->session->set("subshippingerrors", $_shippingErrors);
                }
            }
            /* return $error; */
        }
    }

    function get_shipping_methods() {
        global $wpdb;
        $ShippingAllData = self::get_shipping_methods_name($_POST['value']);

        WC()->session->__unset("subshippingerrors");
        parse_str($_POST['post_data'], $post_data);

        $Request['err'] = self::get_shipping_cost($ShippingAllData, $post_data);

        $subshipping_info_Query = "SELECT SM.TAID,SP.pack_abber,SO.Pidentifier,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM
            JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
            JOIN {$wpdb->prefix}LogistraTAShippingPackages AS SP ON SO.id = SP.shippingid
            WHERE SM.ShippingClass = '{$ShippingAllData->ShippingClass}' AND SO.Pidentifier = '$ShippingAllData->Pidentifier'
        ";

        $subshipping_info = $wpdb->get_row($subshipping_info_Query);

        if (!empty($post_data)) {

            $_key = isset($post_data[ship_to_different_address]) ? 'shipping' : 'billing';
            $data = get_option('myprefix_options', 'none');
            $link = get_Logistralinks();

            $crg_api_key = $data['apikey'];
            $crg_sender_id = $data['senderid'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $post_data[$_key . "_country"] . '&postcode=' . $post_data[$_key . "_postcode"] . '');
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            $headers = array(
                "X-Cargonizer-Key:" . $crg_api_key,
                "X-Cargonizer-Sender:" . $crg_sender_id,
                "Content-type:application/xml"
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($curl);

            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $service_partners = json_decode($json, TRUE);
            $service_partners_info = array();
            if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {
                $service_partner = $service_partners['service-partners']['service-partner'];
                $partner = $service_partner[0];

                if ($subshipping_info->requiresServicePartner == 'true' || $_POST['value']=='BringServicepakke') {

                    $enable_value = $data['service_address'];
                    if ($enable_value == 'enabled') {
                        if ($service_partner['number']) {
                        $partlistopt .= "<option value='0'>" . $service_partner['name'] . "</option>";
                    } else {
                        foreach ($service_partner as $i => $data) {

                            if ($_COOKIE['selected_sub_shipping_method'] == $i)
                                $selectedData = "selected='selected'";
                            else
                                $selectedData = "";

                            $partlistopt .= "<option value='" . $i . "' $selectedData>" . $service_partner[$i]['name'] . "</option>";
                        }
                    }
                    }else {
                        $partlistopt .='';
                        $_COOKIE['selected_sub_shipping_method'] = $service_partner[0]['customer-number'];
                    }
                }

                $service_partners_info[] = array(
                    "service-partner" => array(
                        "number" => (isset($partner["number"])) ? $partner["number"] : '',
                        "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                        "name" => (isset($partner["name"])) ? $partner["name"] : '',
                        "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                        "country" => (isset($partner["country"])) ? $partner["country"] : '',
                        "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                        "city" => (isset($partner["city"])) ? $partner["city"] : '',
                    )
                );

                $service_partner = array(
                    "number" => (isset($partner["number"])) ? $partner["number"] : '',
                    "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                    "name" => (isset($partner["name"])) ? $partner["name"] : '',
                    "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                    "country" => (isset($partner["country"])) ? $partner["country"] : '',
                    "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                    "city" => (isset($partner["city"])) ? $partner["city"] : '',
                );
            }
        }

        global $wpdb;
        if (!$ShippingAllData->Pidentifier) {
            WC()->session->__unset("subshippingerrors");
        }


        $class_name = $ShippingAllData->Classname;

        if ($class_name) {
            if ($partlistopt == '')
                $listpart = "";
            else
                $listpart = "<select name='sub_shipping' id='submethod'>" . $partlistopt . "</select>";
        }

        WC()->session->total = WC()->session->sub_total;
        $Request['listpart'] = $listpart;
        echo json_encode($Request);
        die;
    }
    
    function get_shipping_methods_cart() {
        global $wpdb;
        $ShippingAllData = self::get_shipping_methods_name($_POST['value']);

        WC()->session->__unset("subshippingerrors");
        WC()->customer->set_shipping_location($_POST["country"], '', $_POST["postcode"]);

        $subshipping_info_Query = "SELECT SM.TAID,SP.pack_abber,SO.Pidentifier,SO.requiresServicePartner FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM
            JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
            JOIN {$wpdb->prefix}LogistraTAShippingPackages AS SP ON SO.id = SP.shippingid
            WHERE SM.ShippingClass = '{$ShippingAllData->ShippingClass}' AND SO.Pidentifier = '$ShippingAllData->Pidentifier'
        ";

        $subshipping_info = $wpdb->get_row($subshipping_info_Query);

        $data = get_option('myprefix_options', 'none');
        $link = get_Logistralinks();

        $crg_api_key = $data['apikey'];
        $crg_sender_id = $data['senderid'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $link . '/service_partners.xml?country=' . $_POST["country"] . '&postcode=' . $_POST["postcode"] . '');
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $headers = array(
            "X-Cargonizer-Key:" . $crg_api_key,
            "X-Cargonizer-Sender:" . $crg_sender_id,
            "Content-type:application/xml"
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);

        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $service_partners = json_decode($json, TRUE);
        $service_partners_info = array();
        if (is_array($service_partners) && isset($service_partners['service-partners']) && !empty($service_partners['service-partners'])) {
            $service_partner = $service_partners['service-partners']['service-partner'];
            $partner = $service_partner[0];

            if ($subshipping_info->requiresServicePartner == 'true' || $_POST['value']=='BringServicepakke') {

                $enable_value = $data['service_address'];
                if ($enable_value == 'enabled') {
                    if ($service_partner['number']) {
                        $partlistopt .= "<option value='0'>" . $service_partner['name'] . "</option>";
                    } else {
                        foreach ($service_partner as $i => $data) {

                            if ($_COOKIE['selected_sub_shipping_method'] == $i)
                                $selectedData = "selected='selected'";
                            else
                                $selectedData = "";

                            $partlistopt .= "<option value='" . $i . "' $selectedData>" . $service_partner[$i]['name'] . "</option>";
                        }
                    }
                }else {
                    $partlistopt .='';
                    $_COOKIE['selected_sub_shipping_method'] = $service_partner[0]['customer-number'];
                }
            }

            $service_partners_info[] = array(
                "service-partner" => array(
                    "number" => (isset($partner["number"])) ? $partner["number"] : '',
                    "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                    "name" => (isset($partner["name"])) ? $partner["name"] : '',
                    "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                    "country" => (isset($partner["country"])) ? $partner["country"] : '',
                    "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                    "city" => (isset($partner["city"])) ? $partner["city"] : '',
                )
            );

            $service_partner = array(
                "number" => (isset($partner["number"])) ? $partner["number"] : '',
                "customer_number" => (isset($partner["customer-number"])) ? $partner["customer-number"] : '',
                "name" => (isset($partner["name"])) ? $partner["name"] : '',
                "address1" => (isset($partner["address1"])) ? $partner["address1"] : '',
                "country" => (isset($partner["country"])) ? $partner["country"] : '',
                "postcode" => (isset($partner["postcode"])) ? $partner["postcode"] : '',
                "city" => (isset($partner["city"])) ? $partner["city"] : '',
            );
        }

        global $wpdb;
        if (!$ShippingAllData->Pidentifier) {
            WC()->session->__unset("subshippingerrors");
        }


        $class_name = $ShippingAllData->Classname;

        if ($class_name) {
            if ($partlistopt == '')
                $listpart = "";
            else
                $listpart = "<select name='sub_shipping' id='submethod'>" . $partlistopt . "</select>";
        }

        WC()->session->total = WC()->session->sub_total;
        $Request['listpart'] = $listpart;
        echo json_encode($Request);
        die;
    }

    function writetolog($info){

$logpath=dirname(__FILE__).'../logs/logistralog.txt';
$current = file_get_contents($logpath);
$current .=$info. PHP_EOL;
file_put_contents($logpath, $current,FILE_APPEND);


}
      
    function get_cost_estimate() {
        $data = get_option('myprefix_options', 'none');
        $method=  explode(':', $_POST['pid']);

        setcookie('sub_shipping_method', $method[0], time() + (86400 * 30), "/");
        global $wpdb, $woocommerce;
        $data = get_option('myprefix_options', 'none');
        $link = get_Logistralinks();

        $crg_api_key = $data['apikey'];
        $crg_sender_id = $data['senderid'];

        $product_name = $_POST['pid'];
        //get transport_agreement id
        $Q = "SELECT SM.TAID FROM {$wpdb->prefix}LogistraTAShippingMethods AS SM 
              JOIN {$wpdb->prefix}LogistraTAShippingOptions AS SO ON SM.id = SO.methodid
              WHERE SO.Pidentifier = '$product_name'";

        $id = $wpdb->get_row($Q);
        $id = $id->TAID;

        $crg_consignment_url = "{$link}/consignments.xml";
        $crg_consignment_url = "{$link}/consignment_costs.xml";
        $crg_transport_url = "{$link}/transport_agreements.xml";
        $debug = 1;
        require_once LogistraAPIPluginPath . 'include/cargonizer.php';
        $crg_xml = new CRG_Xmlwriter();
        $crg = new cargonizer($crg_api_key, $crg_sender_id, $crg_consignment_url);
        $ResArr = array();
        $Query = "SELECT SP.pack_abber FROM {$wpdb->prefix}LogistraTAShippingMethods as SM
            JOIN {$wpdb->prefix}LogistraTAShippingOptions  AS SO ON SM.id = SO.methodid
            JOIN {$wpdb->prefix}LogistraTAShippingPackages AS SP ON SO.id = SP.shippingid
            WHERE SM.TAID = $id";
        $result = $wpdb->get_row($Query);
        setcookie('sub_shipping_method_pack_abber', $result->pack_abber, time() + (86400 * 30), "/");
        $i = 0;
        /* Prepare Items */
        $items = array();
        foreach (WC()->cart->get_cart() as $cart_item) {
            $product = get_product($cart_item['product_id']);
            if ($product->has_weight()) {
                $weight = $product->get_weight();
            } else {
                $weight = 1;
            }

            $product_title = $product->get_title();

            for ($i = 0; $i < $cart_item['quantity']; $i++) {
                $sku .= $product->get_sku() . ',';
            }
            $wt = $cart_item['quantity'] * $weight;
            $totle_wt = $totle_wt + $wt;
            $product_title = $product->get_title();
        }

        $items[] = array(
            "item" => array(
                "_attribs" => array(
                    "amount" => "1",
                    "description" => "$sku",
                    "type" => "$result->pack_abber",
                    "weight" => "$totle_wt"
                )
            )
        );

        $crg_data['consignments'] = array(
            "consignment" => array(
                "_attribs" => array(
                    "transport_agreement" => $id,
                ),
                "product" => $product_name,
                "parts" => array(
                    "consignee" => array(
                        "name" => 'Stykkgods',
                        "country" => "NO",
                        "postcode" => "1337",
                    ),
                ),
                "items" => $items,
            ),
        );
        $debug = 0;
        $crg->requestConsignment($crg_data, $debug, $crg_consignment_url);


        $result_xml = $crg->getResultXml();

        if (isset($result_xml->error)) {
            setcookie('sub_shipping_cost', '', time() + (86400 * 30), "/");
            echo 'ERROR: ' . $result_xml->error;
        } elseif ($result_xml->consignment->errors->error) {
            setcookie('sub_shipping_cost', '', time() + (86400 * 30), "/");
            echo 'ERROR: ' . $result_xml->consignment->errors->error;
        } elseif ($result_xml->{'estimated-cost'}) {
            setcookie('sub_shipping_cost', $result_xml->{'estimated-cost'}, time() + (86400 * 30), "/");
            echo 'Shipping Estimated Cost: ' . $result_xml->{'estimated-cost'};
        }
        die();
    }

}

//class

new Api_Ajax;
