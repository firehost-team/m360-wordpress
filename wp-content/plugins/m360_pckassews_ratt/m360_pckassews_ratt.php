<?php
/**
* Plugin Name: M360 PCKasseWS Ratt og sanselig
* Plugin URI: http://www.m360.no
* Description: Connect PCKasse POS to wordpress for woocommerce > 3.0 
* Author: Ibrahim Qraiqe
* Text Domain: m360pck
* Domain Path: /languages/
* Version: 6.631.2
* License: GPLv2
*/
// exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
define('M360_PCKASSWS_PLUGIN_PATH',   plugin_dir_path(__FILE__));
define('USE_MULTISORE',   false);

$dir = M360_PCKASSWS_PLUGIN_PATH;

include_once ($dir.'includes/m360_pck_functions.php');
include_once ($dir.'includes/m360-pck-setup-tap.php');
include_once ($dir.'includes/m360-pck-product-tap.php');
include_once ($dir.'includes/m360-pck-orders-tap.php');
include_once ($dir.'includes/m360-pck-shippings-tap.php');
include_once ($dir.'includes/m360-pck-logging-tap.php');
include_once ($dir.'includes/m360-pck-warehouses-tap.php');
include_once ($dir.'includes/m360_pck_order_status.php' );
include_once ($dir.'includes/m360_pck_custom_product_panel_tap.php');
include_once ($dir.'includes/m360_pck_missing_attributes.php');
include_once ($dir.'includes/plugin_extra/add_submenu_page.php');

load_plugin_textdomain( 'm360pck', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

class M360_PCKasseWS{
	/**
	 * __construct function.
	 */
	 static public function plugin_get_version() {
		if ( ! function_exists( 'get_plugins' ) )
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$plugin_folder = get_plugins( '/' . plugin_basename( dirname( __FILE__ ) ) );
		$plugin_file = basename( ( __FILE__ ) );
		return $plugin_folder[$plugin_file]['Version'];
	}
	
	public function __construct() {
		
		// check if WooCommerce is activated
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        if(!is_plugin_active( 'woocommerce/woocommerce.php')) {
            return;
        }
        if((is_multisite() == false)||(is_multisite() == true && is_main_site())){
            CreateMultiKasserTable();
        }
		// start hooks
		$this->hooks();
		
	}
	
	/**
	 * hooks function.
	 *
	 * initialise hooks
	 */
	private function hooks() {
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $setupTabOptions = get_option( 'm360_pck_options_setup' );

        // intercepts web service calls

		add_action( 'parse_request', array( $this, 'receive_from_pck' ) );
		register_activation_hook( __FILE__, array( $this, 'm360_pck_activation' ) );

        //add_action('woocommerce_thankyou', array( $this, 'send_to_pck'));
        add_action( 'woocommerce_new_order',  array( $this, 'send_to_pck' ),  10, 1);


        $show_merker = isset($setupTabOptions[ 'm360_pck_show_merke_in_products_table'] )?$setupTabOptions[ 'm360_pck_show_merke_in_products_table']:'no';
        if($show_merker == 'yes'){
            add_filter('manage_product_posts_columns', array($this,'m360_pck_manufacturer_col_head'));
            add_action( 'manage_product_posts_custom_column', array( $this, 'action_manage_product_posts_custom_column'), 10, 2 );
        }

        add_filter( 'woocommerce_product_data_tabs', array( $this, 'create_admin_tab' ), 10, 1 );
        add_action( 'woocommerce_product_data_panels', array( $this, 'm360_product_write_panels') );

        $multiStorePlugin = n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')?true:false;

        if((is_multisite() == false)||(is_multisite() == true && is_main_site()) || !$multiStorePlugin){
            add_action('admin_menu',array($this,'remove_m360_pck_metaboxes'));
            if(isset($productTabOptions[ 'm360_pck_delete_image_after_deleting_product'] )){
                if($productTabOptions[ 'm360_pck_delete_image_after_deleting_product'] == 'yes'){
                    add_action('before_delete_post', array($this,'delete_post_attachments'));
                }
            }
        }
        add_action('before_delete_post', array($this,'delete_pck_post'));



		/* auto select first variant */
		add_filter( 'woocommerce_dropdown_variation_attribute_options_args', array($this,'m360_attribute_args'), 10, 1 );
		
		add_action('wp_head', array($this,'m360_maintenance_mode'));
		
		//add_action('init', array($this,'m360_add_sizeColor_attributes'));

		/* custom fields */
        // Display Fields
        add_action( 'woocommerce_product_options_general_product_data', array($this,'m360_add_custom_general_fields') );
        // Save Fields
        add_action( 'woocommerce_process_product_meta',  array($this,'m360_add_custom_general_fields_save') );
        // Frontend
        add_filter( 'woocommerce_attribute_label', array($this,'m360_filter_woocommerce_attribute_label'), 10, 3 );

        // add eans to product
        add_action( 'woocommerce_product_options_general_product_data', array( $this, 'm360_variation_eans' ), 10, 3 );

        // add eans to variable
        add_action( 'woocommerce_variation_options', array( $this, 'm360_variation_eans' ), 10, 3 );

        // checkout extra fields
        add_filter( 'woocommerce_checkout_fields' , array( $this, 'm360_pck_override_checkout_fields') );
        add_action('woocommerce_checkout_process', array( $this, 'm360_pck_checkout_field_process'));
        add_action( 'woocommerce_after_checkout_form',  array( $this, 'm360_pck_override_checkout_jscript'));
        add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'm360_pck_checkout_field_update_order_meta' ));
        add_action('woocommerce_admin_order_data_after_shipping_address', array( $this, 'm360_pck_fields_display_admin_order_meta'), 10, 1);

        // customer
        add_action( 'show_user_profile', array( $this, 'fb_add_custom_user_profile_fields') );
        add_action( 'edit_user_profile', array( $this, 'fb_add_custom_user_profile_fields' ));

        add_action( 'personal_options_update', array( $this, 'fb_save_custom_user_profile_fields' ));
        add_action( 'edit_user_profile_update', array( $this, 'fb_save_custom_user_profile_fields' ));

        // Availability
        if ( ! is_admin() ) {
            if(isset($productTabOptions[ 'm360_pck_show_skaffevare'] )){
                if($productTabOptions[ 'm360_pck_show_skaffevare'] == 'yes'){
                    add_filter( 'woocommerce_product_is_in_stock', array($this,'m360_pck_get_product_is_in_stock'), 10, 1 );
                    add_filter( 'woocommerce_get_availability', array($this,'m360_pck_get_availability'), 10, 2 );
                }
            }
        }

        /* after delete product */
        add_action( 'after_delete_post', array($this,'m360_action_after_delete_post'), 10, 1 );

        /* PCK kunde rabatt */

        if (!is_admin() ){
            $price_hooks = array();
            $price_hooks = array_merge( $price_hooks, array(
                'woocommerce_product_get_price',
                'woocommerce_product_get_sale_price',
                //'woocommerce_product_get_regular_price',
            ) );
            $price_hooks = array_merge( $price_hooks, array(
                'woocommerce_product_variation_get_price',
                'woocommerce_product_variation_get_regular_price',
                'woocommerce_product_variation_get_sale_price',
                //'woocommerce_variation_prices_sale_price',
            ) );

            // Hooking...
            foreach ( $price_hooks as $price_hook ) {
                add_filter( $price_hook, array( $this, 'pck_grupper_sale_price' ), PHP_INT_MAX, 2 );
            }
            add_filter( 'woocommerce_variation_prices', array($this, 'pck_grupper_variations_prices'), PHP_INT_MAX, 2 );
            add_filter( 'woocommerce_sale_flash', array($this,'wc_custom_replace_sale_text') );
        }

        add_action( 'wpo_wcpdf_after_item_meta', array($this,'wpo_wcpdf_show_product_attributes'), 10, 3 );
        /* Invoices/Packing Lists */
        add_filter( 'wc_pip_document_table_headers', array($this,'sv_wc_pip_document_table_headers_product_lager') );
        add_filter( 'wc_pip_document_table_row_cells', array($this,'sv_wc_pip_document_table_row_cells_product_lager'), 10, 5 );
        //dd_action( 'wc_pip_styles', array($this,'sv_wc_pip_styles_product_lager'));

        // reduce stock action
    }

    public function action_woocommerce_product_set_stock( $array ) {
        WriteLog(__FUNCTION__.' hamas: '.print_r($array,true));
    }

    /* Invoices/Packing Lists */
    public function sv_wc_pip_document_table_headers_product_lager( $table_headers ) {
        $lager_header = array( 'product_lager' => 'Lager' );
        return array_merge( $lager_header, $table_headers );
    }
    public function sv_wc_pip_document_table_row_cells_product_lager( $table_row_cells, $document_type, $item_id, $item, $product ) {
        $warehouse_id  = wc_get_order_item_meta($item_id,'lager');
        $warehouse_name = '';
        if($warehouse_id > 0){
            $warehouse = getFrontEndWarehousByWarehouseId($warehouse_id);
            $warehouse_name .= (strlen($warehouse->name)>0)?$warehouse->name:'Standard';
            $lager_content = array( 'product_lager' => $warehouse_name );
            return array_merge( $lager_content, $table_row_cells );
        }else{
            return $table_row_cells;

        }

    }
    public function sv_wc_pip_styles_product_lager() {
        echo 'td.product_thumbnail img {
		width: 75px;
		height: auto;
	}';
    }
    /* end Invoices/Packing Lists */
    public function pck_grupper_variations_prices($prices, $product) {

        foreach ($prices["regular_price"] as $variation_id => $variation_price) {


            $variation_sale_price = $prices["sale_price"][$variation_id];
            $prices["sale_price"][$variation_id] = $this->pck_grupper_sale_price($variation_sale_price, $product);

            $variation_price = $prices["price"][$variation_id];
            $prices["price"][$variation_id] = $this->pck_grupper_sale_price($variation_price, $product);
        }
        asort($prices["price"]);
        asort($prices["regular_price"]);
        asort($prices["sale_price"]);

        return $prices;
    }

    function wpo_wcpdf_show_product_attributes ( $template_type, $item, $order ) {
        if(empty($item['product'])) return;
        $warehouse_name = '';
        if ($template_type == 'packing-slip') {
            $warehouse_id  = wc_get_order_item_meta($item['item_id'],'lager');
            if($warehouse_id > 0){
                $warehouse = getFrontEndWarehousByWarehouseId($warehouse_id);
                $warehouse_name .= (strlen($warehouse->name)>0)?$warehouse->name:'Standard';
                printf('<div class="product-attribute">Lager: %s</div>', $warehouse_name);

            }
        }
    }

    public function getUserRabattAndValidTil(){
        $user = wp_get_current_user();

        $rabatt = 0;
        $data_to = '';

        if($user){
            $g_discounts = array();
            foreach($user->roles as $role){
                $role_obj = get_role($role);
                if($role_obj->has_cap( 'pck_group_rabatt' )){
                    $rabatt_cap = $role_obj->capabilities;
                    $g_discounts[$rabatt_cap['pck_group_rabatt']]=$role_obj->name;
                    if($role_obj->has_cap('pck_group_rabatt_valid_until'))$data_to = $rabatt_cap['pck_group_rabatt_valid_until'];
                }

            }

            if(count($g_discounts)>0){
                arsort($g_discounts);
                foreach($g_discounts as $g_rabatt => $role_name){
                    if($g_rabatt>0){
                        $rabatt = $g_rabatt;
                        break;
                    }
                }
            }else{
                $rabatt = get_user_meta($user->get_id(), 'pck_rabatt',true);
                $data_to = get_user_meta($user->get_id(),"pck_rabatt_valid_until",true);
            }

        }
        return array('rabatt'=>$rabatt,'date_to'=>$data_to);
    }

    function wc_custom_replace_sale_text( $html ) {
        $user_rabat_info = $this->getUserRabattAndValidTil();

        $rabatt = $user_rabat_info['rabatt'];
        global $product;
        $new_html = $html;
        if($rabatt>0 && $product->get_sale_price()< $product->get_regular_price()){
            $new_html =  str_replace( __( 'Sale!', 'woocommerce' ), __( 'Medlemspris!', 'woocommerce' ), $html );
        }

        return $new_html;
    }

    public function pck_grupper_sale_price($price, $productd){

	    $regular_price      = get_post_meta($productd->get_id(),'_regular_price',true);
        $sale_price         = get_post_meta($productd->get_id(),'_sale_price',true);
        $sale_date_from     = $productd->get_date_on_sale_from();//get_post_meta($productd->get_id(),'_sale_price_dates_from',true);
        $sale_date_to       = $productd->get_date_on_sale_to();//get_post_meta($productd->get_id(),'_sale_price_dates_to',true);

        if (strtotime('NOW', current_time( 'timestamp' ) > strtotime($sale_date_from)) &&
            strtotime('NOW', current_time( 'timestamp' ) < strtotime($sale_date_to)) &&
            $sale_price > 0 && $sale_price <= $regular_price) {
            return $sale_price;
        }


        //$tax_display = get_option( 'woocommerce_tax_display_shop' );
        $user_rabat_info = $this->getUserRabattAndValidTil();

        $rabatt = $user_rabat_info['rabatt'];
        $date_to = (!empty($user_rabat_info['data_to']))?$user_rabat_info['data_to']:'';


        if($rabatt>0){
            $rabatt_precentage = $rabatt/100;
            $saving_amount = $regular_price*$rabatt_precentage;

            if(strlen($date_to)>0){
                if(strtotime( 'NOW', current_time( 'timestamp' ) ) <= strtotime( $date_to )){
                    $new_price = $regular_price-$saving_amount;
                }else{
                    $new_price = $price;
                }
            }else{
                $new_price = $regular_price-$saving_amount;
            }

            return $new_price;
        }

        return $price;
    }


    function m360_action_after_delete_post( $postid ) {
        global $wpdb;
        $table_name_childs = m360_pck_get_table_name("m360_pckasse_sizeColorIds");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name_childs'") == $table_name_childs) {
            $delete_child_qur = "DELETE FROM {$table_name_childs} WHERE post_id = {$postid}";
            $wpdb->query($delete_child_qur);
        }

        $table_name_parents = m360_pck_get_table_name("m360_pckasse_parentsIds");
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name_parents'") == $table_name_parents) {
            $delete_parent_qur = "DELETE FROM {$table_name_parents} WHERE post_id = {$postid}";
            $wpdb->query($delete_parent_qur);
        }


    }

    function m360_pck_get_product_is_in_stock( $is_in_stock ) {
        global $product;
        if (is_a( $product, 'WC_Product' ) ) {
            $nonStockItem = get_post_meta( $product->get_id(), 'm360_pck_nonStockItem',true );
            if (!$is_in_stock && $nonStockItem) {
                $is_in_stock = true;
            }
        }

        return $is_in_stock;
    }

    function m360_pck_get_availability( $availability, $_product ) {
        $productTabOptions = get_option( 'm360_pck_options_product' );

        $nonStockItem = get_post_meta( $_product->get_id(), 'm360_pck_nonStockItem',true );
        $nonStockItemDays = get_post_meta( $_product->get_id(), 'm360_pck_nonStockItemDays',true );

        $pretext = isset( $productTabOptions [ 'm360_pck_skaffevare_text' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_skaffevare_text' ] ) : 'Fjernlager, leveringstid: {skaffetid} dager';

        $text = $bodytag = str_replace("{skaffetid}", $nonStockItemDays, $pretext);
        if ($nonStockItem && $_product->get_stock_quantity()<=0) {
            if(strlen($text)>0){
                $availability['availability'] = $text;
            }else{
                $availability['availability'] = $availability['availability'].' , skaffevare '.$nonStockItemDays.' dager';
            }
        }

        return $availability;
    }
    // customer
    function fb_add_custom_user_profile_fields( $user ) {
        $creditApproved = isCustomerCreditApproved($user->ID);
        $askedForCredit = isAskedForCredit($user->ID);
        $checked = '';
        if($askedForCredit ||$creditApproved)$checked = 'checked="checked"';
        $disabled = ($creditApproved)?'disabled':'';
        ?>
        <h3>For PCKasse</h3>
        <table class="form-table">
            <tr class="show-admin-bar user-admin-bar-front-wrap">
                <th scope="row">
                    <label for="billing_orgno">OrgNo: </label></th>
                <td>
                    <input type="text" name="billing_orgno" id="billing_orgno" value="<?php echo esc_attr( get_the_author_meta( 'billing_orgno', $user->ID ) ); ?>" class="regular-text" /><br />
                    <!--<span class="description">OrgNo er obligatorisk for kreditordre kunder </span>-->
                </td>
            </tr>
            <tr class="show-admin-bar user-admin-bar-front-wrap">
                <th scope="row">Ask for credit</th>
                <td>
                    <fieldset>
                        <legend class="screen-reader-text"><span>Toolbar</span></legend>
                        <label for="billing_askforcredit">
                            <input name="billing_askforcredit" type="checkbox" id="billing_askforcredit" value="1" <?php echo $checked.' '.$disabled; ?>>
                            Ask for credit
                        </label>
                    </fieldset>
                </td>
            </tr>
        </table>
    <?php }

    function fb_save_custom_user_profile_fields( $user_id ) {

        if ( !current_user_can( 'edit_user', $user_id ) )
            return FALSE;

        update_user_meta( $user_id, 'billing_orgno', $_POST['billing_orgno'] );
        update_user_meta( $user_id, 'billing_askforcredit', $_POST['billing_askforcredit'] );

        $curren_blog_id = (is_multisite())?get_current_blog_id():0;
        if($curren_blog_id>0)switch_to_blog( $curren_blog_id );
        UpdatePCKKreditCustomersFromUser($user_id);
        if($curren_blog_id>0)restore_current_blog();

    }
    // end customer
    function m360_pck_fields_display_admin_order_meta($order) {
	    $orgno = get_post_meta($order->get_id(), '_billing_orgno', true);
	    if(strlen($orgno)>0){
            echo '<p><strong>' . __('OrgNo') . ':</strong><br> ' . $orgno . '</p>';
        }
    }


    function m360_pck_checkout_field_update_order_meta( $order_id ) {
        if ( ! empty( $_POST['orgno'] ) ) {
            update_post_meta( $order_id, '_billing_orgno', sanitize_text_field( $_POST['orgno'] ) );
        }
    }

    function m360_pck_override_checkout_jscript() {
        //var_dump($fields['billing']['orgno']);

        ## TO DO
        $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
        $credit = '';
        $isCredit = false;
        if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
            $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
            if( !is_numeric($isCredit)){
                $credit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
            }
        }
        if($isCredit):?>
            <script>
                jQuery(function(){
                    jQuery( 'body' )
                        .on( 'updated_checkout', function() {
                            usingGateway();
                            jQuery('input[name="payment_method"]').change(function(){
                                usingGateway();

                            });
                        });
                });


                function usingGateway(){
                    if(jQuery('form[name="checkout"] input[name="payment_method"]:checked').val() == "<?php echo $credit; ?>"){
                        jQuery( '#orgno_field' ).show();
                    }else{
                        jQuery( '#orgno_field' ).hide();
                    }
                }

            </script>
        <?php endif;?>
        <?php
    }
    function m360_pck_checkout_field_process() {
        $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
        $credit = '';
        if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
            $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
            if( !is_numeric($isCredit)){
                $credit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
            }
        }
        /*
        if ( ! $_POST['orgno'] && $_POST['payment_method'] == $credit)
            wc_add_notice( __( 'OrgNo er obligatorisk for kreditordre' ), 'error' );
        */
    }
    function m360_pck_override_checkout_fields( $fields ) {
        $user = wp_get_current_user();
        $orgNo = '';
        if($user){
            $orgNo =  esc_attr( get_the_author_meta( 'billing_orgno', $user->ID ) );
        }
        $fields['billing']['orgno'] = array(
                                            'label'     => __('OrgNo', 'woocommerce'),
                                            'placeholder'   => 'OrgNo',
                                            'required'  => false,
                                            'class'     => array('form-row-wide'),
                                            'clear'     => true,
                                            'default'   => $orgNo,
                                            );
        
        
        return $fields;
    }

    function m360_variation_eans() {
	    ?>
        <style>
            .m360_eans{
                margin-left: 10px;
            }
            .m360_eans .left{
                margin-right: 10px;
            }
            .m360_eans .right{
                font-weight: 600;
                text-decoration: underline;
            }
        </style>
        <?php

        global $woocommerce, $post;
        $product = wc_get_product( $post->ID );
        $eans = get_post_meta( $product->get_id(), 'm360_pck_eans');
        if(count($eans)>0){
            $html = '<div class="m360_eans">';
            $html .= '<span class="left">PCK EANS:</span><span class="right">'.$eans[0].'</span>';
            $html .= '</div>';
            echo $html;
        }
    }

    function m360_add_custom_general_fields() {
        ?>
        <style>

            .m360_custom label strong{
                color: red;
            }
        </style>
        <?php
        global $woocommerce, $post;
        //$product = wc_get_product( $post->ID );
        $product = wc_get_product( $post );

        $attributes = $product->get_attributes();
        //var_dump($attributes);
        foreach ( $attributes as $attribute ) {
            $attribute_label = wc_attribute_label( $attribute['name'] );
            $saved_value = 'hamas';
            echo '<div class="options_group">';

            // Text Field
            woocommerce_wp_text_input(
                array(
                    'id'          => '_m360_custom_text_field_'.$attribute['name'],
                    'label'       => 'Oversett <strong>'.$attribute_label.' </strong>til',
                    'wrapper_class'       => 'm360_custom',
                    'placeholder' => $attribute_label,
                    'desc_tip'    => 'true',
                    'description' => __( 'Skriv din egendefinerte attributtet etiketten hvis du ønsker å tilpasse den for dette produktet, la det stå tomt hvis du vil beholde den opprinnelige etiketten', 'woocommerce' )
                )
            );

            echo '</div>';
        }
		$synk_pris = get_post_meta( $post->ID, '_m360_checkbox_synk_pris', true );
        if(empty($synk_pris) ){
            update_post_meta($post->ID, '_m360_checkbox_synk_pris', esc_attr('yes'));
        }

        woocommerce_wp_checkbox(
            array(
                'id'            => '_m360_checkbox_synk_pris',
                'label'         => __('Synk prisen med PCKasse', 'woocommerce' ),
                'wrapper_class'       => 'm360_custom',
                'desc_tip'    => 'true',
                'description'   => __( 'Hvis denne boksen er huket prisen mellom PCKasse og denne produkt vil bli samme', 'woocommerce' ),
            )
        );


    }

    function m360_add_custom_general_fields_save( $post_id ){

        $product = wc_get_product( $post_id );
        $attributes = $product->get_attributes();
        //var_dump($attributes);
        foreach ( $attributes as $attribute ) {
            $woocommerce_text_field = $_POST['_m360_custom_text_field_'.$attribute['name']];
            if( !empty( $woocommerce_text_field ) ) {
                update_post_meta($post_id, '_m360_custom_text_field_' . $attribute['name'], esc_attr($woocommerce_text_field));
            }else{
                delete_post_meta( $post_id, '_m360_custom_text_field_' . $attribute['name'] );
            }
        }

        $woocommerce_synk_pris_checkbox = isset($_POST['_m360_checkbox_synk_pris']) ? 'yes' : 'no';
        update_post_meta($post_id, '_m360_checkbox_synk_pris', esc_attr($woocommerce_synk_pris_checkbox));

    }

    function m360_filter_woocommerce_attribute_label($label, $name) {
        $translated = get_post_meta( get_the_ID(), '_m360_custom_text_field_pa_'.strtolower($name),true);
        if(strlen($translated)>0 && !is_admin()){
            $label = $translated;
        }
        return $label;
    }

	function m360_add_sizeColor_attributes(){
		global $wpdb;
		
		if ( taxonomy_exists( wc_attribute_taxonomy_name('farge') ) ) {
			//WriteLog('farge_attribute Exists');
		}else{
            //WriteLog('farge_attribute Created');
			$farge_attribute = array(
				'attribute_label'   => 'Farge',
				'attribute_name'    => 'farge',
				'attribute_type'    => 'select',
				'attribute_orderby' => 'menu_order',
				'attribute_public'  => 0
			);
			$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $farge_attribute );
			do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $farge_attribute );
	
			flush_rewrite_rules();
			delete_transient( 'wc_attribute_taxonomies' );
		}
		
		if ( taxonomy_exists( wc_attribute_taxonomy_name( 'storrelse')) ) {
			//WriteLog('storrelse_attribute Exists');
		}else{
            //WriteLog('farge_attribute Created');
			$storrelse_attribute = array(
				'attribute_label'   => 'Størrelse',
				'attribute_name'    => 'storrelse',
				'attribute_type'    => 'select',
				'attribute_orderby' => 'menu_order',
				'attribute_public'  => 0
			);
			$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $storrelse_attribute );
			do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $storrelse_attribute );
	
			flush_rewrite_rules();
			delete_transient( 'wc_attribute_taxonomies' );
		}
	}
	
	function m360_maintenance_mode(){
		$setupTabOptions = get_option( 'm360_pck_options_setup' );
		$maintenance = (isset($setupTabOptions[ 'm360_pck_maintenance_mode' ]))?$setupTabOptions[ 'm360_pck_maintenance_mode' ]:'no';

		
		if (!is_user_logged_in() && $maintenance == 'yes'):?>
        
        <style>
		.m360_overlay_container{
			background-color:#000;
			position:fixed;
			top:0;
			left:0;
			width:100%;
			height:100%;
			z-index:9999999;
			color:#fff;
			display:table;
		}
		.m360_overlay_message{
			text-align:center;
			display:table-cell;
			vertical-align: middle;
			font-size:50px;
		}
		.m360_overlay_message p{
		}
		.m360_overlay_message h2{
			color:#fff;
			text-transform:uppercase;
		}
		body{
			overflow:hidden;
		}
		</style>
        <?php
		$type = (isset($setupTabOptions[ 'm360_pck_maintenance_type' ] )) ? $setupTabOptions[ 'm360_pck_maintenance_type' ] : 'text';
		$text = $setupTabOptions [ 'm360_pck_maintenance_text' ];
		$rs = $setupTabOptions [ 'm360_pck_maintenance_rv' ];
		?>
			<div class="m360_overlay_container">
            	<div class="m360_overlay_message">
               <?php
			   		if($type == 'rev_slider'){
						if(strlen($rs)<=0){
							echo '<p>Slider Revolution finnes ikke</p>';
						}else{
							putRevSlider($rs);
						}
					}else{
						echo $text;
					}
			   ?>
                </div>
            </div>
		<?php endif; ?>
        <?php
	}
    
	function m360_attribute_args( $args = array() ) {

		//$product   = $args['product'];
    	//$attribute = $args['attribute'];
		//$product = $args['product'];
		//WriteLog('m360_attribute_args '.print_r($args,true));
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$firstChoise = $productTabOptions[ 'm360_pck_firstchoise'];
		if($firstChoise == 'yes'){
			if(count($args['options'])>0){
				$args['selected'] = $args['options'][0];
			}
		}
		//$args['options'][0] .= ' '.$product->stock_status;
		return $args;

	}
	
	public function delete_pck_post($post_id){
        //WriteLog(__FUNCTION__);
        deleteSizeColorIdFromDb($post_id);
    }
	public function delete_post_attachments($post_id){
	
		global $post_type;   
		if($post_type !== 'product') return;

        //WriteLog(__FUNCTION__);
		global $wpdb;
	
		$args = array(
			'post_type'         => 'attachment',
			'post_status'       => 'any',
			'posts_per_page'    => -1,
			'post_parent'       => $post_id
		);
		$attachments = new WP_Query($args);
		$attachment_ids = array();
		if($attachments->have_posts()) : while($attachments->have_posts()) : $attachments->the_post();
				$attachment_ids[] = get_the_id();
			endwhile;
		endif;
		wp_reset_postdata();
	
		if(!empty($attachment_ids)) :
			$delete_attachments_query = $wpdb->prepare('DELETE FROM %1$s WHERE %1$s.ID IN (%2$s)', $wpdb->posts, join(',', $attachment_ids));
			$wpdb->query($delete_attachments_query);
		endif;
	
	}
	/*
	function custom_process_order($order_id) {
		global $wpdb;
		global $woocommerce;
		$wpdb->query( $wpdb->prepare("INSERT INTO `wp_m360_pckasse_orderstatus` (`pckws_id`, `order_id`, `wc_order_total`, `wc_order_delivered`, `status`) VALUES (NULL, '$order_id', '2', '0', 'new')") );
	}
	*/
	public function m360_add_manefacturer_text($html) {

		$post = get_post();
		$thumbnail = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'full' ) );
		if(!$thumbnail){ $thumbnail = '<img src="'.wc_placeholder_img_src().'" />'; }
		/* end just for bunadruson */
		$manufacturer = get_post_meta( $post->ID, 'm360_pck_manufacturer', true );
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$show = $productTabOptions[ 'm360_pck_produsent_label_on_product_view'];
		$beforeLbl = isset($productTabOptions['m360_pck_before_produsent_label_on_product_view'])?$productTabOptions['m360_pck_before_produsent_label_on_product_view']:'';
		if(strlen($beforeLbl)>0)$beforeLbl .= ' ';
		
		if(isset($productTabOptions[ 'm360_pck_produsent_label_on_product_view'] ) && strlen($manufacturer)>0 ){
			if($show != 'dont show'){
				$style ='style="position: absolute;';
				if($show == 'top left'){
					$style .= 'top:0;left:0;"';
				}else if($show == 'top right'){
					$style .= 'top:0;right:0;"';
				}else if($show == 'bottom left'){
					$style .= 'bottom:0;left:0;"';
				}else if($show == 'bottom right'){
					$style .= 'bottom:0;right:0;"';
				}else{
					$style = '';
				}
				
				
				//$html .= '<span class="m360_manufacturer_text"'.$style.'>'.$manufacturer.'</span>'.PHP_EOL;
				$link = strip_tags( $html, '<a>' );
				$cl = strpos($link, '</a>');
				
				//$img = strip_tags( $html, '<img>' );
				$final = $thumbnail.'<span class="m360_manufacturer_text"'.$style.'>'.$beforeLbl.$manufacturer.'</span>'.'</a>';
				$newHtml = substr_replace($link,$final,$cl);
				return '<div id="m360_manufacturer_text_container">'.$newHtml.'</div>';
			}else{
				return '<div class="m360_single_poduct_image">'.$thumbnail.'</div>';
			}
			
		}else{
			return '<div class="m360_single_poduct_image">'.$thumbnail.'</div>';
		}
	}
	
	public function remove_m360_pck_metaboxes(){
		$productTabOptions = get_option( 'm360_pck_options_product' );
		if(!isset($productTabOptions[ 'm360_pck_hide_custom_fields' ])){
			remove_meta_box('postcustom','product','normal');
		}else{
			if($productTabOptions[ 'm360_pck_hide_custom_fields' ] == 'hide'){
				remove_meta_box('postcustom','product','normal');
			}
		}
		
	}
	
	public function m360_pck_activation(){
        //WriteLog('m360_pck_activation');
		CreateOrderStatusTables();
		CreateShipmentsTables();
	}

    public function is_order_found_in_pckasse_order_status($order_id){
	    //WriteLog('is_order_found_in_pckasse_order_status');
        global $wpdb;
        $found = false;
        $table_name = (strlen($wpdb->base_prefix)>0)?$wpdb->base_prefix."m360_pckasse_orderstatus":$wpdb->prefix . "m360_pckasse_orderstatus";
        $pck_order = $wpdb->get_row( 'SELECT * FROM '.$table_name.' WHERE order_id = '.$order_id, OBJECT );

        $curren_blog_id = (is_multisite())?get_current_blog_id():0;
        if($curren_blog_id>0){
            switch_to_blog( $curren_blog_id );
            $wc_order = wc_get_order($order_id);
            $wc_order_qty = $this->getTotalOrderedFromOrder($wc_order);
            restore_current_blog();
        }else{
            $wc_order = wc_get_order($order_id);
            $wc_order_qty = $this->getTotalOrderedFromOrder($wc_order);
        }

        if($pck_order){
            $found = true;

            if($pck_order->status != 'new'){
                $wpdb->update(
                    $table_name,
                    array('pckws_id'=>$pck_order->pckws_id,
                        'order_id'=>$pck_order->order_id,
                        'wc_order_total'=>$pck_order->wc_order_total,
                        'wc_order_delivered'=>$pck_order->wc_order_delivered,
                        'status'=>'new'),
                    array('pckws_id'=>$pck_order->pckws_id));
            }

            if($pck_order->wc_order_total != $wc_order_qty){
                $wpdb->update(
                    $table_name,
                    array('pckws_id'=>$pck_order->pckws_id,
                        'order_id'=>$pck_order->order_id,
                        'wc_order_total'=>$wc_order_qty,
                        'wc_order_delivered'=>$pck_order->wc_order_delivered,
                        'status'=>'new'),
                    array('pckws_id'=>$pck_order->pckws_id));
            }

        }

        return $found;
    }


    public function regKunde($order_id){
        $order = wc_get_order($order_id);

        $user = get_user_by( 'email', $order->get_billing_email() );
        if(!$user){
            $user_id = createCustomerFromOrder($order);
        }else{
            $user_id =  $user->ID;
        }

        //WriteLog(__FUNCTION__.' '.print_r($order_id,true).' '.print_r($user_id,true));
        if($user_id>0){
            $order->set_customer_id($user_id);
        }

        $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
        $ask_for_credit = false;
        if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
            $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
            $pyment_id = get_post_meta( $order->get_id(), '_payment_method', true );

            if($pyment_id== $isCredit || $order->get_payment_method() == 'wc-booking-gateway'){
                $ask_for_credit = true;
            }
        }

        UpdatePCKKreditCustomers($user_id,$order,0,0,0,$ask_for_credit);

    }

    public function getTotalOrderedFromOrder($wc_order){
        $wc_order_items = $wc_order->get_items();
        //WriteLog('wc_order: '.print_r($wc_order_items,true));

        $wc_order_qty = 0;
        foreach($wc_order_items as $wc_order_item){
            $wc_order_qty += $wc_order_item['qty'];
        }
        //WriteLog(__FUNCTION__.' '.$wc_order_qty);

        return $wc_order_qty;
    }
    public function reduserLager($order_id){
        $order = wc_get_order($order_id);
        if(!$order)return;

        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        foreach ($order->get_items() as $item){
            $product_id = ($item->get_variation_id()>0)?$item->get_variation_id():$item->get_product_id();
            $old_stock_details = get_post_meta($product_id,'stockDetails_'.$kasse_nr,true);
            $warehouses_from_order = getWarehouseIdFromOrderItem($item);
            foreach($warehouses_from_order as $warehouse_from_order){
                $warehouse_id = $warehouse_from_order['warehous_id_inorder'];
                $count = $warehouse_from_order['count_in_order'];

                foreach ($old_stock_details as $key => $old_stock_detail){
                    if($old_stock_detail->warehouseId == $warehouse_id){
                        $new_count = $old_stock_detail->count-$count;
                        $old_stock_detail->count = $new_count;
                        wc_add_order_item_meta($item->get_id(),'lager',$warehouse_id);
                        break;
                    }
                }
            }
            update_post_meta($product_id,'stockDetails_'.$kasse_nr,$old_stock_details);
        }
    }

	public function send_to_pck( $order_id ) {
        $this->regKunde($order_id);
        //WriteLog(__FUNCTION__.' '.$order_id);
        if($this->is_order_found_in_pckasse_order_status($order_id))return;

        $curren_blog_id = (is_multisite())?get_current_blog_id():0;

        if($curren_blog_id>0)switch_to_blog( $curren_blog_id );
        $wc_order = wc_get_order($order_id);
        $wc_order_qty = $this->getTotalOrderedFromOrder($wc_order);
        if($curren_blog_id>0)restore_current_blog();

		M360PCKOrderStatus::UpdateOrderStatus($order_id,$wc_order_qty,0,M360PCKOrderStatus::STATUS_NEW,$curren_blog_id);
		$this->reduserLager($order_id);

        /*
        global $current_PCK;
        $setupOptions = get_option( 'm360_pck_options_setup' );
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $PCK_Licens = $setupOptions['pck_credintal_'.$kasse_nr]['license'];
        if(strlen($PCK_Licens)>0){
            $url = "http://min.pckasse.no/getOrdersPing.aspx?LicenceNo=".$PCK_Licens;
			//file_get_contents('http://min.pckasse.no/getOrdersPing.aspx?LicenceNo='.$PCK_Licens);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
		}
        */
	}

	function m360_pck_manufacturer_col_head($defaults) {
		$productTabOptions = get_option( 'm360_pck_options_product' );
		
		$defaults['manufacturer'] = isset( $productTabOptions [ 'm360_pck_produsent_label' ] ) ? 
				esc_attr( $productTabOptions [ 'm360_pck_produsent_label' ] ) : 'Manufacturer';
		return $defaults;
	}
	
	public function action_manage_product_posts_custom_column( $column, $postid ) {
		if ( $column == 'manufacturer' ) {
			$manufacturer = get_post_meta( $postid, 'm360_pck_manufacturer', true );
			echo isset($manufacturer)?$manufacturer:'';
		}
	}

	/*
	public function create_admin_tab() {
	   echo'<li class="advanced_options advanced_tab"><a href="#m360_pck">M360 PCK</a></li>';
	}
	*/
    public function create_admin_tab( $tabs ){

        $tabs['m360-pck'] = array(
            'label'  => 'M360 PCK',
            'target' => 'm360_pck',
        );

        return $tabs;
    }
	public function m360_product_write_panels(){
		M360PCKCustomProductPanelTap::DrawHtml();
	}
	/**
	 * receive_from_pck function.
	 *
	 * handles incoming pck requests 
	 */
	public function receive_from_pck() {
		
		$pck_web_service = false;
		$pck_wsdl = false;
		
		// traps pck requests like http://localhost/wordpress/pck
		
		foreach($_SERVER as $i => $x)
			if(!is_array($x)){
				if( strpos( isset( $_SERVER[ $i ] ) ? (string)$x : '', '/pckws/api' ) !== false ) 
					$pck_web_service = true;
			}
			
		
		// traps WSDL request like http://localhost/wordpress/pck?wsdl
		if( strpos( isset( $_SERVER[ 'QUERY_STRING' ] ) ? $_SERVER[ 'QUERY_STRING' ] : '', 'wsdl' ) !== false )
			$pck_wsdl = true;
		
		// handle pck requests
		if( $pck_web_service ) {
			
			// handle WSDL request
			if( $pck_wsdl ) {
				
				header( 'Content-type: text/xml; charset=UTF-8' );
				$wsdlContent = file_get_contents( plugin_dir_path( __FILE__ ) . 'includes/wsdl.xml' );
				$tns_path = get_site_url() . '/pckws/api';
				$wsdlContent = str_replace('{{tns.path}}', $tns_path, $wsdlContent);
				
				print $wsdlContent;
				exit; // do not return control to WordPress
				
			}
			
			// handle pck web service calls
			if( isset($_SERVER["HTTP_SOAPACTION"] ) ) {
				if ( !file_exists( ABSPATH . '/wp-config.php' )) {
					require_once( ABSPATH . '../wp-config.php' );
				} else {
					require_once( ABSPATH . '/wp-config.php' );
				}
				
				require_once( ABSPATH . '/wp-admin/includes/image.php' );
				require_once( plugin_dir_path( __FILE__ ) . 'includes/m360_pck_receive.php' );
				require_once( plugin_dir_path( __FILE__ ) . 'includes/m360_pck_service.php' );
			}
			
			exit; // do not return control to WordPress
			
		}
	}
}

$m360_pckassews 	    = new M360_PCKasseWS();
$setupTap 			    = new M360PCKSetupTap();
$productTap 			= new M360PCKProductTap();
$ordersTap 			    = new M360PCKOrdersTap();
$shippingsTap		    = new M360PCKShippingsTap();
$loggingTap 			= new M360PCKLoggingTap();
$warehousesTap          = new M360WarehousesTap();
//$extraTap			= new M360PCKExtraTap();

class M360_PCKasseWS_Settings{
    private $options;
    
    public function __construct() {
		
		// check if WooCommerce is activated
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if(!is_plugin_active( 'woocommerce/woocommerce.php')) {
            return;
        }

		// add pck options menu into WordPress
        add_action( 'admin_menu', array( $this, 'add_m360_pck_plugin_page' ) );
		
		// add pck extra options sub-menu into WordPress
        add_action( 'admin_menu', array( $this, 'add_m360_extra_pck_plugin_options_page' ) );
		
		// initialise pck option pages
        add_action( 'admin_init', array( $this, 'init_m360_pck_pages' ) );
		
		// initialise tooltipster
		add_action( 'admin_head', array( $this, 'init_m360_pck_header' ) );
		
		// initialise pck javascript and css
		add_action( 'admin_enqueue_scripts', array( $this, 'init_scripts' ) );
    }
    public function init_m360_pck_pages() { 
		global $setupTap;
		global $productTap;
		global $loggingTap;
		global $ordersTap;
		global $shippingsTap;
		global $warehousesTap;

		//global $extraTap;
		
		$setupTap->init_setup();
		$productTap->init_product();
		$ordersTap->init_orders();
		$shippingsTap->init_shipping();
		$loggingTap->init_logging();
        $warehousesTap->init_warehouses();
		//$extraTap->init_extraTap();
    }
    
	
    public function init_m360_pck_header(){
        // header callback
       print "<script type='text/javascript'>
		jQuery(document).ready(function() {
            jQuery('.m360_pck_tip').tooltipster({
                contentAsHTML: true
            });
        });
		</script>";
    }
    
    public function init_scripts(){
        // enqueue css and javascripts

        // m360_pck CSS
        $pck_style_file = plugins_url( '/assets/css/m360_pck.css', __FILE__ );
        $pck_style_file_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'assets/css/m360_pck.css' ));
        wp_enqueue_style( 'm360-pck-style',$pck_style_file ,false,$pck_style_file_ver);

		// Tooltipster CSS
        $tooltipser_style_file = plugins_url( '/assets/css/tooltipster.css', __FILE__ );
        $tooltipser_style_file_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'assets/css/tooltipster.css' ));
		wp_enqueue_style( 'tooltipster-style',$tooltipser_style_file,false,$tooltipser_style_file_ver);
        
		// Tooltipster v3.2.6
		wp_enqueue_script( 'tooltipster', plugins_url( '/assets/js/jquery.tooltipster.min.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'nestable', plugins_url( '/assets/js/jquery.nestable.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'pckjs', plugins_url( '/assets/js/jquery.pck.js', __FILE__ ), array( 'jquery' ) );

    }

	public function add_m360_pck_plugin_page() {
		// adding M360 PCK options page under "Settings"
		add_menu_page(
			'M360 PCKasseWS Settings', // Page Title
			'M360 PCKasseWS', // Menu Title
			'manage_options', // Capability
			'm360_pck_options_page', // Menu Slug
			array( $this, 'create_m360_pck_options_page' ) , // Function
            plugin_dir_url(__FILE__).'assets/images/pck.png',//$icon_url
			30.0
		);
		
	}
	
	public function add_m360_extra_pck_plugin_options_page() {
		add_submenu_page('m360_pck_options_page',
						 'M360 PCKasseWS Extra Functions',
						 'Extra functions',
						 'manage_options',
						 'm360_extra_pck_plugin_options_page',
						 array( $this, 'createM360_extra_pck_plugin_options_page' ) );		
	}
    
	public function createM360_extra_pck_plugin_options_page(){
		$extra_options		= new M360PCKExtraOptions();
		$extra_options->drawThePage();
	}
	
    public function create_m360_pck_options_page(){
               // get current tab
		if( isset( $_GET['tab'] ) ) 
			$tab = $_GET['tab'];
		
		// start page wrapper
		print '<div class="wrap">';
		print '<h2>'.__('M360 PCKasse Settings','m360pck').'</h2>';
		
		// get PCK options from WordPress database and store them
        $this->options = array();
		$this->options[ 'm360_pck_options_setup' ] = get_option( 'm360_pck_options_setup' );
		$this->options[ 'm360_pck_options_setup' ] = get_option( 'm360_pck_options_product' );
		$this->options[ 'm360_pck_options_orders' ] = get_option( 'm360_pck_options_orders' );
		$this->options[ 'm360_pck_options_shipping' ] = get_option( 'm360_pck_options_shipping' );
		$this->options[ 'm360_pck_options_logging' ] = get_option( 'm360_pck_options_logging' );
        $this->options[ 'm360_pck_options_warehouses' ] = get_option( 'm360_pck_options_warehouses' );

        // set default tab
		if ( !isset( $tab ) || empty( $tab ) )
			$tab = 'setup';
		
        // draw the tabs, highlighting current tab
		$this->create_m360_pck_options_tabs( $tab );
		
		// start form wrapper 
		print '<form method="post" action="options.php">';
		
		// print hidden setting fields
		settings_fields( 'm360_pck_options_' . $tab . '_group' );   
		
		// print current page 
		do_settings_sections( 'm360_pck_options_page_' . $tab );
		
		// print submit / save button
		submit_button(); 
		
		// close page wrapper 
        print '</form></div>';
    }
    
	public function create_m360_pck_options_tabs( $current = 'setup' ) {
	
		// a list of the m360_pck option pages 
		$tabs = array( 
			'setup' => 'Setup', 
			'product' => 'Product', 
			'orders' => 'Orders', 
			'shipping' => 'Shipping', 
			'logging' => 'Logging',
            'warehouses'=> 'Warehouses'
			//'extra' =>	'Extra'
		);
		
		// print m360_pck icon and start tab wrapper
		print '<div id="icon-themes" class="m360_pck_icon"><a class="m360_pck_link" target="_blank" href="http://www.m360.no/">&nbsp;</a></div>';
		print '<h2 class="nav-tab-wrapper">';
		
		foreach( $tabs as $tab => $name ) {
				
			// if tab represents current page, highlight it
			$class = ( $tab == $current ) ? ' nav-tab-active' : '';
			print "<a class='nav-tab$class' href='?page=m360_pck_options_page&tab=$tab'>$name</a>";
			
		}
		
		// close wrapper
		print '</h2></div>';
	}
	
	function have_option( $option ) {
		if ( !isset( $this->options[ 'm360_pck_options_setup' ][ $option ] ) || 
			empty( $this->options[ 'm360_pck_options_setup' ][ $option ] ) ) return false;
		return true;
	}


    /**
     * sanitize each setting field as needed
     * @param $input
     * @return mixed
     */
    public function sanitise( $input ) {

		return $input;

    }
}

$multiStorePlugin = n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')?true:false;
if((is_multisite() == false)||(is_multisite() == true && is_main_site()) || !$multiStorePlugin){
    require_once( ABSPATH . '/wp-includes/pluggable.php' );
    if ( is_admin())
        $m360_pck_settings = new M360_PCKasseWS_Settings();
}
