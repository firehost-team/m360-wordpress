<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
//include_once (plugin_dir_path(__FILE__).'../m360_pckassews.php');
//include_once (plugin_dir_path(__FILE__).'m360_pck_functions.php');

class M360PCKProductTap extends M360_PCKasseWS_Settings{
    private $_wareHouses;
	public function __construct(){

        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $this->_wareHouses = getAllFrontEndWarehouses($kasse_nr);

        foreach ($this->_wareHouses as $key => $warehouse){
            if(isset($_POST['m360_pck_options_product']['m360_pck_warehouse_'.$warehouse->warehouseId])){
                UpdateFrontEndWarehouses($warehouse->warehouseId,$kasse_nr,$_POST['m360_pck_options_product']['m360_pck_warehouse_'.$warehouse->warehouseId]);
            }

        }


	}
	
	
	public function init_product() {
		
		// register Setup options group 
		register_setting(
            'm360_pck_options_product_group', // group
            'm360_pck_options_product', // name
            array( $this, 'sanitise' ) // sanitise method
        );

        //==================== Product Attributes ====================
        add_settings_section(
            'm360_pck_product_attributes_section', // id
            '==================== Product Attributes ====================', // title
            NULL,
            'm360_pck_options_page_product' // page
        );

        // How varianters created
        add_settings_field(
            'm360_pck_how_varianter_created',
            'Creating av varianters?',
            array( $this, 'setting_html_m360_pck_how_varianter_created' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $creatingType = 'farge_storrelse_i_btuk';
        if(isset($productTabOptions[ 'm360_pck_how_varianter_created'] ))$creatingType = $productTabOptions[ 'm360_pck_how_varianter_created'];
        if($creatingType == 'samling_samme_vare_nr'){
            // Samling extra settings
            // how many numbers the first should test
            add_settings_field(
                'm360_pck_samling_number_of_digits',
                'How many digits to check',
                array( $this, 'setting_html_m360_pck_samling_number_of_digits' ),
                'm360_pck_options_page_product',
                'm360_pck_product_attributes_section'
            );

            // Who the attributes
            add_settings_field(
                'm360_pck_how_the_attributes_entered',
                'How is the attributes in PCKasse',
                array( $this, 'setting_html_m360_pck_how_the_attributes_entered' ),
                'm360_pck_options_page_product',
                'm360_pck_product_attributes_section'
            );
            add_settings_field(
                'm360_pck_samling_allways_config',
                'Allways create variable products',
                array( $this, 'setting_html_m360_pck_samling_allways_config' ),
                'm360_pck_options_page_product',
                'm360_pck_product_attributes_section'
            );
        }


        // Color attribute
        add_settings_field(
            'm360_pck_color_attribute',
            'Choose <span>Color </span>attribute',
            array( $this, 'setting_html_m360_pck_color_attribute' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

        if($creatingType == 'farge_storrelse_i_btuk'){
            add_settings_field(
                'm360_pck_always_create_color_attribute',
                '<span>always create Color attribute </span>',
                array( $this, 'setting_m360_pck_always_create_color_attribute' ),
                'm360_pck_options_page_product',
                'm360_pck_product_attributes_section'
            );

        }


        // Size attribute
        add_settings_field(
            'm360_pck_size_attribute',
            'Choose <span>Size</span> attribute',
            array( $this, 'setting_html_m360_pck_size_attribute' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

        if($creatingType == 'farge_storrelse_i_btuk'){
            add_settings_field(
                'm360_pck_always_create_size_attribute',
                '<span>always create Size attribute</span>',
                array( $this, 'setting_m360_pck_always_create_size_attribute' ),
                'm360_pck_options_page_product',
                'm360_pck_product_attributes_section'
            );
        }

        // Produsent attribute
        add_settings_field(
            'm360_pck_produsent_attribute',
            'Choose <span>produsent</span> attribute',
            array( $this, 'setting_html_m360_pck_produsent_attribute' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

        // undernavn attribute
        add_settings_field(
            'm360_pck_undernavn_fields',
            'Choose <span>undernavn</span> attribute',
            array( $this, 'setting_html_m360_pck_undernavn_attribute_fields' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

        // undernavn attribute
        add_settings_field(
            'm360_pck_eksterlink_fields',
            'Choose <span>exktern link</span> attribute',
            array( $this, 'setting_html_m360_pck_eksterlink_attribute_fields' ),
            'm360_pck_options_page_product',
            'm360_pck_product_attributes_section'
        );

		// Global settings section
		add_settings_section(
            'm360_pck_options_section_product', // id
            '==================== General Product Settings ====================',
            NULL,
            'm360_pck_options_page_product' // page
        );


        // Allow deleting the prodcut
        add_settings_field(
            'm360_pck_allow_product_deleting', // id
            '<span>Allow product deleting</span>',
            array( $this, 'setting_html_m360_pck_allow_product_deleting' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );

        // Synk product images
        add_settings_field(
            'm360_pck_synk_product_images', // id
            'Sync product image',
            array( $this, 'setting_html_m360_pck_synk_product_images' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );

        add_settings_field(
            'm360_pck_use_imagick', // id
            'Use imagick class for image creating',
            array( $this, 'setting_html_m360_pck_use_imagick' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );
        // use veilpris i pris og utslags/nettpris i salg
        add_settings_field(
            'm360_pck_veilpris_i_pris', // id
            'Use Veilpris',
            array( $this, 'setting_html_m360_pck_veilpris_i_pris' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );
        add_settings_field(
            'm360_pck_tilbud_pris', // id
            'Tilbud pris',
            array( $this, 'setting_html_m360_pck_tilbud_pris' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );

        // Delete product image after deleting the product
        add_settings_field(
            'm360_pck_delete_image_after_deleting_product', // id
            '<span>Delete product image</span>',
            array( $this, 'setting_html_m360_pck_delete_image_after_deleting_product' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );


        // m360_pck_product_oreavrunding
        add_settings_field(
            'm360_pck_product_oreavrunding', // id
            'øreavrunding', // title
            array( $this, 'setting_html_m360_pck_product_oreavrunding' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );

        // m360_pck_product_tax
		add_settings_field(
            'm360_pck_product_tax', // id
            'Price with/without tax', // title
            array( $this, 'setting_html_m360_pck_product_tax' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product' // section
        );

		// Produsent label
		add_settings_field(
            'm360_pck_produsent_label', 
            'Set label for produsent attribute:', 
            array( $this, 'setting_html_m360_pck_produsent_label' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

        // PCK Description
        add_settings_field(
            'm360_pck_description',
            'Product discription',
            array( $this, 'setting_html_m360_pck_description' ),
            'm360_pck_options_page_product',
            'm360_pck_options_section_product'
        );

		// Generate alt text
		add_settings_field(
            'm360_pck_alt_text_from_produkt_name', 
            'Generate alt text for product image from product name', 
            array( $this, 'setting_html_m360_pck_alt_text_from_produkt_name' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// produsent label view boolean
		add_settings_field(
            'm360_pck_produsent_label_on_product_view', 
            'View manufacturer in product page', 
            array( $this, 'setting_html_m360_pck_produsent_label_on_product_view' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

        // produsent label view
		add_settings_field(
            'm360_pck_before_produsent_label_on_product_view', 
            'Text before manufacturer', 
            array( $this, 'setting_html_m360_pck_before_produsent_label_on_product_view' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// custome fields
		add_settings_field(
            'm360_pck_hide_custom_fields', 
            'Hide Custom Fields', 
            array( $this, 'setting_html_m360_pck_hide_custom_fields' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// no color text
		add_settings_field(
			'm360_pck_noColorText',
			'Do not create Color attribute if color contains:',
			array( $this, 'setting_html_m360_pck_noColor' ),
			'm360_pck_options_page_product',
			'm360_pck_options_section_product'
		);

		// no size text
		add_settings_field(
			'm360_pck_noSizeText',
			'Do not create Size attribute if size contains:',
			array( $this, 'setting_html_m360_pck_noSize' ),
			'm360_pck_options_page_product',
			'm360_pck_options_section_product'
		);

		// sync categories
		add_settings_field(
            'm360_pck_sync_categories', 
            'Sync categories with PCK', 
            array( $this, 'setting_html_m360_pck_sync_categories' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// skjul fra web
		add_settings_field(
            'm360_pck_backorder', 
            'backorder "Skjul fra web når tomt på lager"', 
            array( $this, 'setting_html_m360_pck_backorder' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// standard backorder
		add_settings_field(
            'm360_pck_standardbackorder', 
            'backorder when "Skjul fra web når tomt på lager" not checked', 
            array( $this, 'setting_html_m360_pck_standardbackorder' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// first choise
		add_settings_field(
            'm360_pck_firstchoise', 
            'First choise of variable', 
            array( $this, 'setting_html_m360_pck_firstchoise' ), 
            'm360_pck_options_page_product', 
            'm360_pck_options_section_product' 
        );

		// Skaffevare settings section
        add_settings_section(
            'm360_pck_options_section_product_skaffevare', // id
            '==================== Skaffevare ====================',
            NULL,
            'm360_pck_options_page_product' // page
        );
        // Show skaffevare
        add_settings_field(
            'm360_pck_show_skaffevare', // id
            'Show Skaffevare',
            array( $this, 'setting_html_m360_show_skaffevare' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product_skaffevare' // section
        );
        $show_skaffe_vare = 'no';
        if(isset($productTabOptions[ 'm360_pck_show_skaffevare'] ))$show_skaffe_vare = $productTabOptions[ 'm360_pck_show_skaffevare'];
        if($show_skaffe_vare == 'yes'){
            // Skaffevare text
            add_settings_field(
                'm360_pck_skaffevare_text', // id
                'Skaffevare text',
                array( $this, 'setting_html_m360_skaffevare_text' ), // callback
                'm360_pck_options_page_product', // page
                'm360_pck_options_section_product_skaffevare' // section
            );
        }


        // Default dimentions
        // Skaffevare settings section
        add_settings_section(
            'm360_pck_options_section_product_default_dimentions', // id
            '==================== Dimentions default values ====================',
            NULL,
            'm360_pck_options_page_product' // page
        );
        //weight
        add_settings_field(
            'm360_pck_default_weight', // id
            'Weight (kg)',
            array( $this, 'setting_html_m360_default_weight' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product_default_dimentions' // section
        );

        //length
        add_settings_field(
            'm360_pck_default_length', // id
            'Length (cm)',
            array( $this, 'setting_html_m360_default_length' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product_default_dimentions' // section
        );

        //width
        add_settings_field(
            'm360_pck_default_width', // id
            'Width (cm)',
            array( $this, 'setting_html_m360_default_width' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product_default_dimentions' // section
        );

        //height
        add_settings_field(
            'm360_pck_default_height', // id
            'Height (cm)',
            array( $this, 'setting_html_m360_default_height' ), // callback
            'm360_pck_options_page_product', // page
            'm360_pck_options_section_product_default_dimentions' // section
        );
        if(count($this->_wareHouses)>0){
            add_settings_section(
                'm360_pck_options_section_product_warehouses', // id
                '==================== Lagrer fra PCKasse ====================',
                NULL,
                'm360_pck_options_page_product' // page
            );
            foreach ($this->_wareHouses as $key => $warehouse){
                $args     = array (
                    'warehouse_id'      => $warehouse->warehouseId
                );
                add_settings_field(
                    'm360_pck_warehouse_'.$warehouse->warehouseId, // id
                    'Lager id '.$warehouse->warehouseId.' :',
                    array( $this, 'setting_html_m360_warehouses' ), // callback
                    'm360_pck_options_page_product', // page
                    'm360_pck_options_section_product_warehouses',
                    $args
                );
            }
        }

	}

	public function setting_html_m360_warehouses(array $args){
        $lager_id = $args['warehouse_id'];
        $warehouse_frontend = getFrontEndWarehousByWarehouseId($lager_id);
        $warehouse_name = (strlen($warehouse_frontend->name)>0)?$warehouse_frontend->name:'Standard';

        printf(
            '<input type="text" id="m360_pck_warehouse_%s" name="m360_pck_options_product[m360_pck_warehouse_%s]" value="%s" />',
            $lager_id,$lager_id,$warehouse_name
        );
    }
    public function setting_html_m360_pck_eksterlink_attribute_fields(){
        $attributes = br_aapf_get_attributes();
        $attributes['no_attribute'] = 'No attribute';
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('60800',
            "Choose the attribute that you want to save the ekstern link field from pckasse into",
            'm360_pck_eksterlink_fields'
        );
        $saved = isset($productTabOptions[ 'm360_pck_eksterlink_fields' ] )?$productTabOptions[ 'm360_pck_eksterlink_fields' ]:'no_attribute';

        print '<select name="m360_pck_options_product[m360_pck_eksterlink_fields]" style="width: 150px;">';
        foreach ( $attributes as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';

    }

	public function setting_m360_pck_always_create_color_attribute(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] )?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';


        m360_pck_tooltip('27270',
            "will create unicolor attribute if no color found",
            'm360_pck_always_create_color_attribute'
        );

        print '<select name="m360_pck_options_product[m360_pck_always_create_color_attribute]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_m360_pck_always_create_size_attribute(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] )?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';

        m360_pck_tooltip('27270',
            "will create unisize attribute if no size found",
            'm360_pck_always_create_size_attribute'
        );

        print '<select name="m360_pck_options_product[m360_pck_always_create_size_attribute]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';

    }

    public function setting_html_m360_pck_synk_product_images(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_synk_product_images'] )?$productTabOptions[ 'm360_pck_synk_product_images']:'yes';


        m360_pck_tooltip('26270',
            "You can set this value to no if you want to manage product images just from woocommerce and not sync it with PCK",
            'm360_pck_synk_product_images'
        );

        print '<select name="m360_pck_options_product[m360_pck_synk_product_images]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_use_imagick(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_use_imagick'] )?$productTabOptions[ 'm360_pck_use_imagick']:'yes';


        m360_pck_tooltip('26290',
            "For advanced image synking we use php imagick module.<br>Please note that not all servers support this module.<br>If you got any error message in your PCKasse webshop kø about imagick please change this setting to no.",
            'm360_pck_use_imagick'
        );

        print '<select name="m360_pck_options_product[m360_pck_use_imagick]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }
    public function setting_html_m360_default_weight(){
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('9001',
            "Defualt weight hvis det 0 i PCK",
            'm360_pck_default_weight'
        );

        // select control start tag
        printf(
            '<input type="text" class="short wc_input_decimal" id="m360_pck_produsent_label" name="m360_pck_options_product[m360_pck_default_weight]" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_default_weight' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_default_weight' ] ) : '1'
        );
    }
    public function setting_html_m360_default_length(){
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('9002',
            "Defualt Length hvis det 0 i PCK",
            'm360_pck_default_length'
        );

        // select control start tag
        printf(
            '<input type="text" class="short wc_input_decimal" id="m360_pck_produsent_label" name="m360_pck_options_product[m360_pck_default_length]" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_default_length' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_default_length' ] ) : '10'
        );
    }
    public function setting_html_m360_default_width(){
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('9003',
            "Defualt Width hvis det 0 i PCK",
            'm360_pck_default_width'
        );

        // select control start tag
        printf(
            '<input type="text" class="short wc_input_decimal" id="m360_pck_default_width" name="m360_pck_options_product[m360_pck_default_width]" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_default_width' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_default_width' ] ) : '10'
        );
    }
    public function setting_html_m360_default_height(){
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('9004',
            "Defualt height hvis det 0 i PCK",
            'm360_pck_default_height'
        );

        // select control start tag
        printf(
            '<input type="text" class="short wc_input_decimal" id="m360_pck_default_height" name="m360_pck_options_product[m360_pck_default_height]" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_default_height' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_default_height' ] ) : '10'
        );
    }

    public function setting_html_m360_skaffevare_text(){
        $productTabOptions = get_option( 'm360_pck_options_product' );

        m360_pck_tooltip('8271',
            "Write what do you want to show as text for skaffevarer , if you want to show skaffetid use {skaffetid}, ex 'Fjernlager, leveringstid: {skaffetid} dager'",
            'm360_pck_produsent_label'
        );

        // select control start tag
        printf(
            '<input type="text" id="m360_pck_produsent_label" name="m360_pck_options_product[m360_pck_skaffevare_text]" style="width: 250px;" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_skaffevare_text' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_skaffevare_text' ] ) : 'Fjernlager, leveringstid: {skaffetid} dager'
        );
    }


    public function setting_html_m360_show_skaffevare(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_show_skaffevare'] )?$productTabOptions[ 'm360_pck_show_skaffevare']:'no';

        m360_pck_tooltip('8270',
            "if you wanto , you can show skaffevarer from pck even you are hiding out of stock varer",
            'm360_pck_show_skaffevare'
        );

        print '<select name="m360_pck_options_product[m360_pck_show_skaffevare]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

	public function setting_html_m360_pck_how_the_attributes_entered(){
        m360_pck_tooltip('1009',
            "Please enter how is the attributes entered in PCKasse , ex: navn-farge-størrelse",
            'm360_pck_how_the_attributes_entered'
        );
        $productTabOptions = get_option( 'm360_pck_options_product' );

        printf(
            '<input type="text" id="m360_pck_how_the_attributes_entered" name="m360_pck_options_product[m360_pck_how_the_attributes_entered]" style="width: 250px;" placeholder="navn-farge-størrelse" value="%s" />',
            isset( $productTabOptions [ 'm360_pck_how_the_attributes_entered' ] ) ?
                esc_attr( $productTabOptions [ 'm360_pck_how_the_attributes_entered' ] ) : 'navn-farge-størrelse'
        );
    }
	public function setting_html_m360_pck_samling_number_of_digits(){

        m360_pck_tooltip('1010',
            "Please enter how many digits you want to use to check if the product is the avarianter for a product",
            'm360_pck_samling_number_of_digits'
        );

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_samling_number_of_digits'] )?$productTabOptions[ 'm360_pck_samling_number_of_digits']:4;

        printf(
            '<input type="number" id="m360_pck_samling_number_of_digits" name="m360_pck_options_product[m360_pck_samling_number_of_digits]" style="width: 50px;" min="4" max="100" step="1" value="%s" onkeydown="return false"/>', $saved
        );
    }
	public function setting_html_m360_pck_how_varianter_created(){
        $values = array('farge_storrelse_i_btuk'=>'Farge Størrelse i bruk', 'samling_samme_vare_nr'=>'Smling av varer med samme vare nr');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_how_varianter_created'] )?$productTabOptions[ 'm360_pck_how_varianter_created']:'farge_storrelse_i_btuk';

        m360_pck_tooltip('1270',
            "Chose how want to create varianter in woocommerce, Farge størrelse i bruk, means that varianter comes fram PCKasse, if you chose samling , products with the same vare nr fram PCK will become varianter for product",
            'm360_pck_how_varianter_created'
        );

        print '<select name="m360_pck_options_product[m360_pck_how_varianter_created]" style="width: 350px;" onchange="saveFunction();" >';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
        ?>
        <script>
            function saveFunction() {
                document.querySelectorAll("input[type=submit]")[0].click();
            }
        </script>
        <?php
    }

    public function setting_html_m360_pck_samling_allways_config(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'setting_html_m360_pck_samling_allways_config'] )?$productTabOptions[ 'setting_html_m360_pck_samling_allways_config']:'no';

        m360_pck_tooltip('118270',
            "all products from pck will be created as a variable product even there is no farge or størrelse",
            'setting_html_m360_pck_samling_allways_config'
        );

        print '<select name="m360_pck_options_product[setting_html_m360_pck_samling_allways_config]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_product_oreavrunding(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_product_oreavrunding'] )?$productTabOptions[ 'm360_pck_product_oreavrunding']:'no';

        m360_pck_tooltip('6270',
            "If yes pris will round down to the narmest amount",
            'm360_pck_product_oreavrunding'
        );

        print '<select name="m360_pck_options_product[m360_pck_product_oreavrunding]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_allow_product_deleting(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_allow_product_deleting'] )?$productTabOptions[ 'm360_pck_allow_product_deleting']:'yes';

        m360_pck_tooltip('6270',
            "If yes, when you delete the product using PCK, woocommerce will detet the product also",
            'm360_pck_allow_product_deleting'
        );

        print '<select name="m360_pck_options_product[m360_pck_allow_product_deleting]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_veilpris_i_pris(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_veilpris_i_pris'] )?$productTabOptions[ 'm360_pck_veilpris_i_pris']:'no';

        m360_pck_tooltip('16290',
            "If yes, pris i nettbutikk skal komme fra veilpris og salg pris skal komme fra utsalgspris eller nettpris",
            'm360_pck_veilpris_i_pris'
        );

        print '<select name="m360_pck_options_product[m360_pck_veilpris_i_pris]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_tilbud_pris(){
        $values = array('discount'=>'Tilbud', 'suggestedPrice'=>'Veiledende pris','alternativePrice'=>'alt. utpris','alternativePrice2'=>'Takeaway');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_tilbud_pris'] )?$productTabOptions[ 'm360_pck_tilbud_pris']:'discount';

        m360_pck_tooltip('16390',
            "Chose where the scheduled tilbud pris should come from",
            'm360_pck_tilbud_pris'
        );

        print '<select name="m360_pck_options_product[m360_pck_tilbud_pris]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_delete_image_after_deleting_product(){
        $values = array('no'=>'No', 'yes'=>'Yes');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_delete_image_after_deleting_product'] )?$productTabOptions[ 'm360_pck_delete_image_after_deleting_product']:'no';

        m360_pck_tooltip('16270',
            "If yes, when you delete the product using PCK or from the backend , the media attached to the product will be deleted as well",
            'm360_pck_delete_image_after_deleting_product'
        );

        print '<select name="m360_pck_options_product[m360_pck_delete_image_after_deleting_product]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

	public function setting_html_m360_pck_description(){
        $values = array('long_description'=>'Long description',
                        'short_description'=>'Short Description',
                        'both'=>'Both',
                        'long_description_if_empty'=>'in Long description if empty',
                        'short_description_if_empty'=>'in Short Description if empty',
                        'both_if_empty'=>'in Both if empty',
                        'no_description'=>'Do not use PCK description');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_description'] )?$productTabOptions[ 'm360_pck_description']:'both_if_empty';

        m360_pck_tooltip('6180',
            "Choose how you want to save Beskrivelse from PCKasse into webshop",
            'm360_pck_description'
        );

        print '<select name="m360_pck_options_product[m360_pck_description]" style="width: 150px;">';
        foreach ( $values as $key => $value) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_color_attribute(){
        $attributes = br_aapf_get_attributes();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $attributes['no_attribute'] = 'No attribute';
        m360_pck_tooltip('6080',
            "Choose the attribute that you want to save the color field from pckasse into",
            'm360_pck_color_attribute'
        );
        $saved = isset($productTabOptions[ 'm360_pck_color_attribute' ] )?$productTabOptions[ 'm360_pck_color_attribute' ]:'no_attribute';

        print '<select name="m360_pck_options_product[m360_pck_color_attribute]" style="width: 150px;">';
        foreach ( $attributes as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_size_attribute(){
        $attributes = br_aapf_get_attributes();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $attributes['no_attribute'] = 'No attribute';
        m360_pck_tooltip('6080',
            "Choose the attribute that you want to save the size field from pckasse into",
            'm360_pck_size_attribute'
        );
        $saved = isset($productTabOptions[ 'm360_pck_size_attribute' ] )?$productTabOptions[ 'm360_pck_size_attribute' ]:'no_attribute';

        print '<select name="m360_pck_options_product[m360_pck_size_attribute]" style="width: 150px;">';
        foreach ( $attributes as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

	public function setting_html_m360_pck_produsent_attribute(){
		$attributes = br_aapf_get_attributes();
		$productTabOptions = get_option( 'm360_pck_options_product' );
        $attributes['no_attribute'] = 'No attribute';
		m360_pck_tooltip('6080',
			"Choose the attribute that you want to save the produsent field from pckasse into",
			'm360_pck_produsent_attribute'
		);
        $saved = isset($productTabOptions[ 'm360_pck_produsent_attribute' ] )?$productTabOptions[ 'm360_pck_produsent_attribute' ]:'no_attribute';

		print '<select name="m360_pck_options_product[m360_pck_produsent_attribute]" style="width: 150px;">';
		foreach ( $attributes as $key => $value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';

	}
	
	public function setting_html_m360_pck_firstchoise(){
		$values = array('no'=>'No', 'yes'=>'Yes');
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$saved = isset($productTabOptions[ 'm360_pck_firstchoise'] )?$productTabOptions[ 'm360_pck_firstchoise']:'no';
		
		m360_pck_tooltip('6270',
			"If yes first alternativ of variable product will be as default choise",
			'm360_pck_firstchoise'
		);

		print '<select name="m360_pck_options_product[m360_pck_firstchoise]" style="width: 150px;">';
		foreach ( $values as $key =>$value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_backorder(){
		$values = array('no'=>'No', 'notify'=>'Allow, but notify customer');
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$saved = isset($productTabOptions[ 'm360_pck_backorder'] )?$productTabOptions[ 'm360_pck_backorder']:'no';

		m360_pck_tooltip('6170',
			"Choose how you want to set backorder for the product",
			'm360_pck_backorder'
		);

		print '<select name="m360_pck_options_product[m360_pck_backorder]" style="width: 150px;">';
		foreach ( $values as $key =>$value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_alt_text_from_produkt_name(){
		$values = array('no'=>'No', 'yes'=>'Yes','yes_if_empty'=>'Yes if empty');
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$saved = isset($productTabOptions[ 'm360_pck_alt_text_from_produkt_name'] )?$productTabOptions[ 'm360_pck_alt_text_from_produkt_name']:'yes_if_empty';

		m360_pck_tooltip('6180',
			"Select if you want the product image alt text comes from product name",
			'm360_pck_alt_text_from_produkt_name'
		);

		print '<select name="m360_pck_options_product[m360_pck_alt_text_from_produkt_name]" style="width: 150px;">';
		foreach ( $values as $key => $value) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_standardbackorder(){
		$values = array('no'=>'No', 'notify'=>'Allow, but notify customer', 'yes'=>'Allow');
		$productTabOptions = get_option( 'm360_pck_options_product' );
		$saved = isset($productTabOptions[ 'm360_pck_standardbackorder'] )?$productTabOptions[ 'm360_pck_standardbackorder']:'yes';


		m360_pck_tooltip('6180',
			"Choose how you want to set backorder for the product when value of 'Skul fra web når tomt på lager' is not checked",
			'm360_pck_core_backorder'
		);

		print '<select name="m360_pck_options_product[m360_pck_standardbackorder]" style="width: 150px;">';
		foreach ( $values as $key => $value) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}

	public function setting_html_m360_pck_product_tax() {
        $values = array('with'=>'With', 'without'=>'Without');
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_product_tax'] )?$productTabOptions[ 'm360_pck_product_tax']:'with';


        m360_pck_tooltip('26280',
            "Please choose if you want at the price comes from PCKasse with or without rax",
            'm360_pck_product_tax'
        );

        print '<select name="m360_pck_options_product[m360_pck_product_tax]" style="width: 150px;">';
        foreach ( $values as $key =>$value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
		
	}

	public function setting_html_m360_pck_produsent_label(){
		$productTabOptions = get_option( 'm360_pck_options_product' );		
		
		m360_pck_tooltip('6080',
				"Set the label to be used as produsent label", 
				'm360_pck_produsent_label'
			);
			
			// select control start tag
			printf(
			'<input type="text" id="m360_pck_produsent_label" name="m360_pck_options_product[m360_pck_produsent_label]" style="width: 250px;" value="%s" />',
			isset( $productTabOptions [ 'm360_pck_produsent_label' ] ) ? 
				esc_attr( $productTabOptions [ 'm360_pck_produsent_label' ] ) : 'Manufacturer'
		);
	}
	
	public function setting_html_m360_pck_produsent_label_on_product_view(){
		$values = array('dont show', 'top left', 'top right', 'bottom left','bottom right','after image');
		$productTabOptions = get_option( 'm360_pck_options_product' );
		if(!isset($productTabOptions[ 'm360_pck_produsent_label_on_product_view'] )){
			update_option($productTabOptions,array('m360_pck_produsent_label_on_product_view'=>'dont show'));		
		}
		
		m360_pck_tooltip('6080',
			"Choose how you want to display the manufacturer on the product page",
			'm360_pck_produsent_label_on_product_view'
		);

		print '<select name="m360_pck_options_product[m360_pck_produsent_label_on_product_view]" style="width: 150px;">';
		foreach ( $values as $value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$value,
				($productTabOptions[ 'm360_pck_produsent_label_on_product_view' ] == $value ) ||
				(!isset($productTabOptions[ 'm360_pck_produsent_label_on_product_view' ] )) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_before_produsent_label_on_product_view(){
		$productTabOptions = get_option( 'm360_pck_options_product' );		
		
		m360_pck_tooltip('6080',
				"Set the label to be used before produsent", 
				'm360_pck_produsent_before_label'
			);
			
			// select control start tag
			printf(
			'<input type="text" id="m360_pck_before_produsent_label_on_product_view" name="m360_pck_options_product[m360_pck_before_produsent_label_on_product_view]" style="width: 250px;" value="%s" />',
			isset( $productTabOptions [ 'm360_pck_before_produsent_label_on_product_view' ] ) ? 
				esc_attr( $productTabOptions [ 'm360_pck_before_produsent_label_on_product_view' ] ) : ''
		);
	}
	
	public function setting_html_m360_pck_hide_custom_fields(){

		$productTabOptions = get_option( 'm360_pck_options_product' );
		
		m360_pck_tooltip('6080',
				"Show or hide the custom fields box from woocommerce admin product page", 
				'm360_pck_hide_custom_fields'
			);
		
		$html = '<select name="m360_pck_options_product[m360_pck_hide_custom_fields]" style="width: 150px;">';
		$html .= '<option value="show" ';
		if(isset($productTabOptions[ 'm360_pck_hide_custom_fields' ])){
			$html .= ($productTabOptions[ 'm360_pck_hide_custom_fields' ] == 'show' )?'selected >':' >';
		}else{
			$html .= ' >';
		}
		$html .= 'Show';
		$html .= '</option>';
	
		$html .= '<option value="hide" ';
		if(isset($productTabOptions[ 'm360_pck_hide_custom_fields' ])){
			$html .= ($productTabOptions[ 'm360_pck_hide_custom_fields' ] == 'hide' )?'selected >':' >';
		}else{
			$html .= 'selected >';
		}
		$html .= 'Hide';
		$html .= '</option>';
			
		$html .= '</select>';
		echo $html;
		
	}

	public function setting_html_m360_pck_undernavn_attribute_fields(){
		$attributes = br_aapf_get_attributes();
        $attributes['no_attribute'] = 'No attribute';
		$productTabOptions = get_option( 'm360_pck_options_product' );

		m360_pck_tooltip('6080',
			"Choose the attribute that you want to save the undernavn field from pckasse into",
			'm360_pck_undernavn_attribute'
		);
        $saved = isset($productTabOptions[ 'm360_pck_undernavn_fields' ] )?$productTabOptions[ 'm360_pck_undernavn_fields' ]:'no_attribute';

		print '<select name="m360_pck_options_product[m360_pck_undernavn_fields]" style="width: 150px;">';
		foreach ( $attributes as $key => $value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';

	}
	
	public function setting_html_m360_pck_noColor(){
		$productTabOptions = get_option( 'm360_pck_options_product' );		
		
		m360_pck_tooltip('6110',
				"if the color from PCK contains this text, woocommerce will not create the Color attribute, text is case sensitive, default text is 'Ingen'", 
				'm360_pck_noColorText'
			);
			
			// select control start tag
			printf(
			'<input type="text" id="m360_pck_noColorText" name="m360_pck_options_product[m360_pck_noColorText]" style="width: 250px;" value="%s" />',
			isset( $productTabOptions [ 'm360_pck_noColorText' ] ) ? 
				esc_attr( $productTabOptions [ 'm360_pck_noColorText' ] ) : 'Ingen'
		);
	}
	
	public function setting_html_m360_pck_noSize(){
		$productTabOptions = get_option( 'm360_pck_options_product' );		
		
		m360_pck_tooltip('6120',
				"if the size from PCK contains this text, woocommerce will not create the Size attribute, text is case sensitive, default text is 'Ingen'", 
				'm360_pck_noColorText'
			);
			
			// select control start tag
			printf(
			'<input type="text" id="m360_pck_noSizeText" name="m360_pck_options_product[m360_pck_noSizeText]" style="width: 250px;" value="%s" />',
			isset( $productTabOptions [ 'm360_pck_noSizeText' ] ) ? 
				esc_attr( $productTabOptions [ 'm360_pck_noSizeText' ] ) : 'Ingen'
		);
	}

	public function setting_html_m360_pck_sync_categories(){

		$productTabOptions = get_option( 'm360_pck_options_product' );
		
		m360_pck_tooltip('6080',
				"If you want to control the categories from woocommerce and not from pck , please choose not to sync categories from pck to woocommerce", 
				'm360_pck_sync_categories'
			);
		
		$html = '<select name="m360_pck_options_product[m360_pck_sync_categories]" style="width: 150px;">';
		$html .= '<option value="sync" ';
		if(isset($productTabOptions[ 'm360_pck_sync_categories' ])){
			$html .= ($productTabOptions[ 'm360_pck_sync_categories' ] == 'sync' )?'selected >':' >';
		}else{
			$html .= 'selected >';
		}
		$html .= 'Sync';
		$html .= '</option>';
	
		$html .= '<option value="not_sync" ';
		if(isset($productTabOptions[ 'm360_pck_sync_categories' ])){
			$html .= ($productTabOptions[ 'm360_pck_sync_categories' ] == 'not_sync' )?'selected >':' >';
		}else{
			$html .= ' >';
		}
		$html .= 'Do not sync';
		$html .= '</option>';
			
		$html .= '</select>';
		echo $html;
		
	}

	
	
	
}