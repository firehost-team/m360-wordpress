<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}

class M360MULTIPCKSETUPTAP{
    private $_setupTabOptions;

    public function number_of_PCKasser(){
        $numberOfKasser = array();
        $counter = isset( $this->_setupTabOptions[ 'm360_number_of_pckasser' ] ) ? esc_attr( $this->_setupTabOptions[ 'm360_number_of_pckasser' ] ) : 1;
        for($i = 0; $i<$counter ; $i++){
            $numberOfKasser[]=$i;
        }
        return $numberOfKasser;
    }

    public function deletePCK($kasse_to_delete){
        if (!is_admin() || !current_user_can('activate_plugins')) return;

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $counter = $setupTabOptions[ 'm360_number_of_pckasser' ];

        $setupTabOptions['m360_number_of_pckasser'] = ($counter-1 >=0)?$counter-1:0;
        $setupTabOptions['pck_credintal_'.$kasse_to_delete] = NULL;

        update_option("m360_pck_options_setup",$setupTabOptions);

        deleteKasse($kasse_to_delete);
    }
    public function __construct(){

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $this->_setupTabOptions = $setupTabOptions;
        toVersion5_1();
        if(isset($_GET['delete_pcknr'])) {
            $kasse_to_delete = $_GET['delete_pcknr'];
            $this->deletePCK($kasse_to_delete);
        }

        foreach($this->number_of_PCKasser() as $pckassenr){
            if(isset($_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr])){
                //WriteLog('KasseNr: '.$pckassenr.' POST: '.print_r($_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr],true));

                $location = $_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['location'];
                $license = $_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['license'];
                $username = $_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['username'];
                $password = $_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['password'];
                $is_default = false;
                if(isset($_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['default'])){
                    $is_default = $_POST['m360_pck_options_setup']['pck_credintal_'.$pckassenr]['default'];
                }
                addKasse($pckassenr,$location,$license,$username,$password,$is_default);
            }
        }
    }

    public function drawNumberOfKasse(){
        // number of PCKasser
        add_settings_field(
            'm360_number_of_pckasser', // id
            'Number of PCKasser', // title
            array( $this, 'setting_html_number_of_pckasser' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );

        /*
        add_settings_field(
            'm360_info_from_default', // id
            'Product info from Default kasse only', // title
            array( $this, 'setting_html_m360_info_from_default' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );
        */
    }


    public function initMulti(){
        foreach($this->number_of_PCKasser() as $pckassenr){
            $args     = array (
                'pckassenr'      => $pckassenr
            );

            add_settings_field(
                'pck_credintal_'.$pckassenr, // id
                'PCK Nummer: '.($pckassenr+1).'<br \><a href="?page=m360_pck_options_page&tab=setup&delete_pcknr='.$pckassenr.'">DELETE</a>', // title
                array( $this, 'setting_html_pck_lcredintal' ), // callback
                'm360_pck_options_page_setup', // page
                'm360_pck_options_header_section_setup' ,// section
                $args
            );
        }


        if(n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')) {
            add_settings_field(
                'pck_multistore',
                'Multistore', // title
                array( $this, 'setting_html_pck_multistore' ), // callback
                'm360_pck_options_page_setup', // page
                'm360_pck_options_header_section_setup' ,// section
                $args
            );
        }


    }


    public function setting_html_pck_multistore(){
        $network_sites = ( function_exists( 'get_sites' ) ) ? get_sites(array('limit'  =>  999)) : wp_get_sites(array('limit'  =>  999));
        echo'<h2>For å bruke multistore fra din pck til woocoomerce bruk pris6,pris7,pris8 eller pris9 fra pck med id for site der du vil sende varen til</h2>';
        echo '<ul>';
        foreach($network_sites as $network_site){
            switch_to_blog( $network_site->blog_id );

            if(!n_is_plugin_active('woocommerce/woocommerce.php') || $network_site->blog_id == BLOG_ID_CURRENT_SITE) {
                restore_current_blog();
                continue;
            }

            echo '<li style="font-weight: 600;padding:10px 0;">'.esc_attr( get_bloginfo( 'name', 'display' ) ).'=> <span style="background-color:#000; color:#fff;padding:5px;border-radius: 2px;">'.$network_site->blog_id .'</span></li>';
            restore_current_blog();
        }
        echo '</ul>';
    }

    public function setting_html_pck_lcredintal(array $args){
        $headers = array('Location','License','Username','Password','Default')
        ?>
        <style>
            .pcksinfo tr td{
                padding: 15px 2px;
                width:100px;
            }

        </style>
        <table border="1" class="pcksinfo">
            <tr>
                <?php foreach($headers as $header):?>
                    <?php if($header == 'Default'):?>
                        <th><?php echo $header; ?></th>
                    <?php else: ?>
                        <th><?php echo $header.' For PCKasse #:'.($args['pckassenr']+1); ?></th>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            <?php
            echo '<tr>';
            echo '<td>'.$this->setting_html_m360_pck_location($args['pckassenr']).'</td>';
            echo '<td>'.$this->setting_html_pck_license($args['pckassenr']).'</td>';
            echo '<td>'.$this->setting_html_m360_pck_username($args['pckassenr']).'</td>';
            echo '<td>'.$this->setting_html_m360_pck_password($args['pckassenr']).'</td>';
            echo '<td>'.$this->setting_html_m360_pck_is_default($args['pckassenr']).'</td>';
            echo '<tr>';

            ?>
        </table>
        <?php

    }

    private function drawPckasserRows($key1,$key2,$blurb,$type){
        $empty = ( !is_multipck_option_set( $this->_setupTabOptions,$key1,$key2) )?true:false;

        $input_style = ($empty)?'style="width:80%;"':'style="width:100%;"';

        $html = '<div class="multipck_input_container" style="width:200px;">';
        $html .= '<input type="'.$type.'" id="'.$key1.'" name="m360_pck_options_setup['.$key1.']['.$key2.']"';
        $html .= isset( $this->_setupTabOptions[$key1][$key2] ) ? 'value="'.esc_attr( $this->_setupTabOptions[$key1][$key2] ).'"' : '';


        if ($empty) {
            $html .= $input_style.'/>';
            $html .='<img style="padding-left: 5px;" src="' . plugin_dir_url( __FILE__ ) . '../assets/images/warning.png" alt="' . $blurb . '" title="' . $blurb . '"/>';
            $html .= '</div>';

        }else{
            $html .= $input_style.'/></div>';

        }

        return $html;
    }
    public function setting_html_pck_license($pckassenr) {
        $key1 = 'pck_credintal_'.$pckassenr.'';
        $key2 = 'license';
        $blurb = "Please enter your PCKasse ".($pckassenr+1)." License";

        return $this->drawPckasserRows($key1,$key2,$blurb,'text');
    }


    public function setting_html_m360_pck_username($pckassenr) {
        $key1 = 'pck_credintal_'.$pckassenr.'';
        $key2 = 'username';
        $blurb = "Please enter the user name as you put it in your PCKasse number ".($pckassenr+1);
        return $this->drawPckasserRows($key1,$key2,$blurb,'text');
    }

    public function setting_html_m360_pck_password($pckassenr) {
        $key1 = 'pck_credintal_'.$pckassenr.'';
        $key2 = 'password';
        $blurb = "Please enter the password as you put it in your PCKasse number ".($pckassenr+1);
        return $this->drawPckasserRows($key1,$key2,$blurb,'password');
    }

    public function setting_html_m360_pck_location($pckassenr){
        $key1 = 'pck_credintal_'.$pckassenr.'';
        $key2 = 'location';
        $blurb = "Please enter the location for PCKasse number ".($pckassenr+1);
        return $this->drawPckasserRows($key1,$key2,$blurb,'text');
    }

    public function setting_html_m360_pck_is_default($pckassenr){



        $key1 = 'pck_credintal_'.$pckassenr.'';
        $key2 = 'default';

        $checked = is_multipck_option_set($this->_setupTabOptions,$key1,$key2);
        ob_start();
        ?>
        <div style="text-align: center;">
            <?php if($checked):?>
                <?php
                    m360_pck_tooltip('11020',
                    "The creation of new products will come just from this PCK",
                    'm360_default_pckasse'
                    );
                ?>
            <?php endif; ?>
        <input type="checkbox" class ="cb" onchange="check(this)" id="pck_credintal_<?php echo $pckassenr; ?>" name="m360_pck_options_setup[pck_credintal_<?php echo $pckassenr; ?>][default]" value="1" <?php checked( 1,$checked , true ) ?>/>
        </div>
        <script>
            function check(element){
                if(element.checked){
                    var checkboxes = document.getElementsByClassName('cb');
                    for(var i=0;i<checkboxes.length;i++){
                        if(checkboxes[i]!=element)
                            checkboxes[i].checked = false;
                    }
                }
            }
        </script>
        <?php
        return ob_get_clean();
    }

    public function setting_html_number_of_pckasser(){
        m360_pck_tooltip('1020',
            "How many PCKasser will connect to this woocommerce store, please click save after you enter the value",
            'm360_number_of_pckasser'
        );

        printf(
            '<input type="text" id="m360_number_of_pckasser" name="m360_pck_options_setup[m360_number_of_pckasser]" style="width: 250px;" value="%s" />',
            count($this->number_of_PCKasser())
        );

    }

    /*
    public function setting_html_m360_info_from_default(){
        m360_pck_tooltip('1120',
            "Set this option to 'YES' if you want to update all info - except stock level - only from the default kasse",
            'm360_info_from_default'
        );

        $values = array('yes' => 'YES','no'=>'NO');

        $saved = 'yes';

        if(isset($this->_setupTabOptions[ 'm360_info_from_default'] )){
            $saved = $this->_setupTabOptions[ 'm360_info_from_default'];
        }


        print '<select name="m360_pck_options_setup[m360_info_from_default]" style="width: 150px;">';
        foreach ( $values as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';

    }
    */
}