<?php 


class M360PCKExtraTap extends M360_PCKasseWS_Settings{

	public function __construct(){
		
	}
	
	
	public function init_extraTap() {
		
		// register Setup options group 
		register_setting(
            'm360_pck_options_extra_group', // group
            'm360_pck_options_extra', // name
            array( $this, 'sanitise' ) // sanitise method
        );
		
		// add Setup page 
		add_settings_section(
            'm360_pck_options_section_extra', // id
            'M360 PCK Extra Options', // title
            array( $this, 'settings_html_m360_pck_extra' ), // callback
            'm360_pck_options_page_extra' // page
        );
		
	}
	
	public function settings_html_m360_pck_extra() {
		?>
        <style>
		.m360_media_library > ul > li{
			display: inline-block;
			max-width: 50px;
		}
		
		.m360_media_library > ul > li img{
			width:100%;
			height:auto;
			max-height:50px;
		}
		</style>
        <?php
			$query_media_args = array(
				'post_type'      => 'attachment',
				'posts_per_page' => - 1,
				'post_parent' => 0,
			);
			
			$query_images = new WP_Query( $query_images_args );
			$html = '<div class="m360_media_library"><ul>';
			$images = array();
			foreach ( $query_images->posts as $image ) {
				$html .= '<li><img src="'.wp_get_attachment_url( $image->ID ).'"/><li>';
			}
			$html .= '</ul></div>';
			echo $html;
			?>
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Clean up media library">
			<p>This will delete all unused images in your media library</p>
			<?php 
    }

}