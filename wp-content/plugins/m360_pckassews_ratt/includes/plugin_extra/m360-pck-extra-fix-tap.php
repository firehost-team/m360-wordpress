<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once ('fix_action_page.php');
$fix_action_page = new M360FIXActionPage();

class M360PCKExtraFixTap{

	public function __construct(){
		
	}
	
	
	public function drawThePage() {
		$this->removeAll_salePriceIfEqualThePriceHtml();
	}

	
	public function removeAll_salePriceIfEqualThePriceHtml() {
		if(count($_POST)){
			global $fix_action_page;
			if(isset($_POST['removeSalePriceIfEqualPrice'])){
                WC_Admin_Notices::add_notice( 'Start' );
                echo '<h3>'.$fix_action_page->removeSalePriceIfEqualPrice().'</h3>';
			}
			
		}
		?>
        <style>
		.submit{
			display: inline-block;
			margin-left:2px;
		}
		.me60_nopadding{
			padding: 0 !important;
		}
		.m360_buttonRed{
			background: #BA0000 !important;
			border-color: #AA0000 #990000 #990000 !important;
			-webkit-box-shadow: 0 1px 0 #990000 !important;
			box-shadow: 0 1px 0 #990000 !important;
			color: #fff !important;
			text-decoration: none;
		}
		.m360_fit{
			width:50%;
			white-space:nowrap;
		}
		</style>
        <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=fix">
        	<table class="form-table">
            	<tbody>
                    <tr style="border-bottom:1px solid #000;">
                    	<td class="me60_nopadding m360_fit">
                        	remove all sale prices from products if its equal to the refular price:
                        </td>
                        <td class="me60_nopadding">
                        	<?php submit_button('Do it', 'primary', 'removeSalePriceIfEqualPrice', true, array( 'id' => 'removeSalePriceIfEqualPrice' )); ?>
                       </td>
					</tr>
	            </tbody>
            </table>
        </form>
        
        <?php
    }
	

}