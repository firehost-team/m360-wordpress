<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKExtraSizeColors{
    private $_kassenr;

    public function __construct(){
        global $current_PCK;
        $this->_kassenr =  (is_object($current_PCK) && isset($current_PCK->PCKasseNr))?$current_PCK->PCKasseNr:0;
    }


    public function drawThePage(){
        ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            .submit {
                display: inline-block;
                margin-left: 2px;
            }

            .me60_nopadding {
                padding: 0 !important;
            }

            .m360_buttonRed {
                background: #BA0000 !important;
                border-color: #AA0000 #990000 #990000 !important;
                -webkit-box-shadow: 0 1px 0 #990000 !important;
                box-shadow: 0 1px 0 #990000 !important;
                color: #fff !important;
                text-decoration: none;
            }

            .m360_fit {
                width: 1%;
                white-space: nowrap;
            }
        </style>
        <?php

        $this->settings_html_m360_pck_order();
        $this->search_form();

        if (count($_POST)) {
            if (isset($_POST['find'])) {
                $this->drawResult($_POST['sku']);
            }
            if (isset($_POST['change'])) {
                $key = $_POST['change'];
                $child_id = $_POST['child_id_'.$key];
                $sizeColorId = $_POST['sizeColorId_'.$key];

                update_post_meta((int)$child_id,'_sizeColorId_'.$this->_kassenr,$sizeColorId);
                UpdateSizeColorID((int)$child_id,$sizeColorId,$this->_kassenr);

                $this->drawResult($_POST['sku']);

            }

            if (isset($_POST['change_all'])) {
                $childrens = unserialize($_POST['change_all']);
                foreach($childrens as $key => $child_id){
                    $sizeColorId = $_POST['sizeColorId_'.$key];
                    update_post_meta((int)$child_id,'_sizeColorId_'.$this->_kassenr,$sizeColorId);
                    UpdateSizeColorID((int)$child_id,$sizeColorId,$this->_kassenr);
                }
                $this->drawResult($_POST['sku']);
            }
        }
    }


    public function settings_html_m360_pck_order(){
        ?>
        <div class="wrap">
            <h1>Check if SizeColors ids is the same in PCKasse</h1>
        </div>
        <?php
    }


    public function search_form(){
        $sku = isset($_POST['sku'])?$_POST['sku']:'';

        ?>
        <form method="post" action="<?php echo get_admin_url() ?>admin.php?page=m360_extra_pck_plugin_options_page&tab=sizeColors">
            <table class="form-table">
                <tbody>
                <tr style="border-bottom:1px solid #000;">
                    <td class="me60_nopadding m360_fit">
                        Find product by sku:
                    </td>
                    <td class="me60_nopadding m360_fit">
                        <span style="padding-left:20px;"><input type="text" name="sku" value="<?php echo $sku; ?>"></span>
                    </td>
                    <td class="me60_nopadding">
                        <?php submit_button('Search', 'primary', 'find', true, array('id' => 'find')); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <?php
    }

    public function drawResult($sku){
        ?>
        <?php $product_id = wc_get_product_id_by_sku($sku); ?>
        <?php if(!$product_id):?>
            <h3>we can not find a product with sku: <?php echo $sku?></h3>
        <?php else:?>
            <?php $product = wc_get_product($product_id);?>
            <h3><?php echo $product->get_name();?></h3>
            <form method="post" action="<?php echo get_admin_url() ?>admin.php?page=m360_extra_pck_plugin_options_page&tab=sizeColors">
            <?php foreach($product->get_children() as $key => $child_id):?>
                <?php $child = wc_get_product($child_id);?>
                <?php $sizeColrId = get_post_meta($child_id,'_sizeColorId_'.$this->_kassenr,true);?>
                    <div class="form-group row">
                        <div class="col-md-8">
                            <?php echo $child->get_name();?>
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="sizeColorId_<?php echo $key; ?>" value="<?php echo $sizeColrId; ?>" class="form-control">
                            <input type="hidden" name="child_id_<?php echo $key; ?>" value="<?php echo $child_id; ?>" class="form-control">
                            <input type="hidden" name="sku" value="<?php echo $sku;?> " class="form-control">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary" name="change" value="<?php echo $key; ?>">Change</button>
                        </div>
                </div>
            <?php endforeach;?>
                <div class="form-group row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2 col-md-offset-10">
                        <input type="hidden" name="sku" value="<?php echo $sku;?> " class="form-control">
                        <button type="submit" class="btn btn-primary" name="change_all" value="<?php echo serialize($product->get_children());?>">Change All</button>
                    </div>
                </div>
            </form>
        <?php endif; ?>
        <?php
    }
}