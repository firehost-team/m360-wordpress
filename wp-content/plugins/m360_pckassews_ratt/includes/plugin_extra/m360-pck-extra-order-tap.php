<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once ('order_action_page.php');
$order_action_page = new M360ORDERPCKActionPage();

class M360PCKExtraOrderTap{

	public function __construct(){
		
	}
	
	
	public function drawThePage() {
		
		$this->settings_html_m360_pck_order();
		$this->order_html();
		
	}
	
	public function settings_html_m360_pck_order() {
			?>
			<div class="wrap">
            	<h1>If you have a problem with sending an order to PCK,  you can use this tool to try to fix it </h1>
            </div>
			<?php
    }
	
	
	public function order_html() {
		if(count($_POST)){
			global $order_action_page;
			if(isset($_POST['find']) && isset($_POST['order_number'])){
				$order_action_page->findTheOrder($_POST['order_number']);
			}
			if(isset($_POST['update'])){
				$order_action_page->updateTheOrder($_POST['order_number_to_update']);
			}
			
			if(isset($_POST['create_new'])){
				$order_action_page->InsertNewOrderIntoOrderStatusTable();
			}
			
		}
		?>
        <style>
		.submit{
			display: inline-block;
			margin-left:2px;
		}
		.me60_nopadding{
			padding: 0 !important;
		}
		.m360_buttonRed{
			background: #BA0000 !important;
			border-color: #AA0000 #990000 #990000 !important;
			-webkit-box-shadow: 0 1px 0 #990000 !important;
			box-shadow: 0 1px 0 #990000 !important;
			color: #fff !important;
			text-decoration: none;
		}
		.m360_fit{
			width:1%;
			white-space:nowrap;
		}
		</style>
        <?php
			$order_number = '';
			
			if(isset($_POST['order_number'])){
				$order_number = $_POST['order_number'];
			}
			
		?>
        <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
        	<table class="form-table">
            	<tbody>
                    <tr style="border-bottom:1px solid #000;">
                    	<td class="me60_nopadding m360_fit">
                        	Find the order by order number:
                        </td>
                        <td class="me60_nopadding m360_fit">
                        	<span style="padding-left:20px;"><input type="text" name="order_number" value="<?php echo $order_number; ?>"></span>
						</td>
                        <td class="me60_nopadding">
                        	<?php submit_button('Search', 'primary', 'find', true, array( 'id' => 'find' )); ?>
                       </td>
					</tr>
	            </tbody>
            </table>
        </form>
        
        <?php
    }
	

}