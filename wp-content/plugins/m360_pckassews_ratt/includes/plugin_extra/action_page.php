<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKActionPage{
	const ONE_PRODUCT_VARS = 'delete_one_product_variable';
  	const ALL_PRODUCTS_VARS = 'delete_all_products_variables';
	
	public function __construct(){

	}
	
	private static function PostIdFromSku($SKU){
		// get WOOCOMM product id from PCK SKU
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare(
			"SELECT post_id FROM " . $wpdb->postmeta . " WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1", $SKU
		));
	}
	
	private static function DeleteFromSizeColorIdsTable($post_id){
		global $wpdb;
		$table_name = $wpdb->prefix . "m360_pckasse_sizeColorIds";
		$query = "DELETE FROM ".$table_name." WHERE `post_id` = ".$post_id;
		$wpdb->query($query);
	}
	
	private static function TruncateSizeColorIdsTable(){
		global $wpdb;
		$table_name = $wpdb->prefix . "m360_pckasse_sizeColorIds";
		$query = "TRUNCATE ".$table_name;
		$wpdb->query($query);
	}
	private static function DeleteProduct($product,$post_id,$one_product = false){
		update_post_meta($post_id, '_product_attributes', NULL);

		if( $product->has_child() ){
			$childrens = $product->get_children( );
			foreach($childrens as $child){
				wp_delete_post($child, true);
				if($one_product){
					self::DeleteFromSizeColorIdsTable($child);
				}
			}  
		}
	}
	
	private static function resetSKU(){
		$sku = '';
		$_POST['sku'] = NULL;
	}
	
	private function kopi_attributter(){
		echo '<strong>kopi funksjonen kommer snart på neste oppdatering</strong>';
	}
	public function doTheDelete(){
		$sku = '';
		$post_id;
		if(isset($_POST['sku'])){
			$sku = $_POST['sku'];
			$post_id = self::PostIdFromSku($sku);
		}
		
		if(isset($_POST['first_delete'])){
			$selected_type = $_POST['delete_type'];
			if($selected_type == self::ONE_PRODUCT_VARS){
				if(strlen($sku)):?>
                	<?php if($post_id):?>
                    	<?php $product = wc_get_product($post_id); ?>
                    	<form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=delete">
                            <p><strong> Are you sure you want to delete all attributers and variables for product: <span style="color:red;font-weight:800; text-decoration:underline;"><?php echo $product->get_title() ?></span></strong></p>
                            <input type="hidden" name="sku" value="<?php echo $sku; ?>">
                            <?php submit_button('Delete', 'm360_buttonRed', 'second_delete_for_one_product', true, array( 'id' => 'second_delete_for_one_product' )); ?>
                            <?php submit_button('Cancel', 'primary', 'cancel', true, array( 'id' => 'cancel' )); ?>
                        </form>
                    <?php else: ?>
                    	 <P><strong> There is no product found for <span style="color:red;font-weight:800; text-decoration:underline;">SKU:<?php echo $sku; ?></span></strong></P>
                    <?php endif; ?>
                    
                <?php else: ?>
                    <P><strong> PLEASE ENTER THE <span style="color:red;font-weight:800; text-decoration:underline;">SKU</span> FOR THE PRODUCT YOU WANT TO DELETE</strong></P>
                <?php endif; ?>
                <?php
			}elseif($selected_type == self::ALL_PRODUCTS_VARS){
				?>
                <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=delete">
                    <p><strong> Are you sure you want to delete all attributers and variables for <span style="color:red;font-weight:800; text-decoration:underline;">ALL PRODUCTS</span> this will take a while</strong></p>
                    <?php submit_button('Delete', 'm360_buttonRed', 'second_delete_for_all_products', true, array( 'id' => 'second_delete_for_all_products' )); ?>
                    <?php submit_button('Cancel', 'primary', 'cancel', true, array( 'id' => 'cancel' )); ?>
                </form>
                <?php
			}
		}elseif(isset($_POST['second_delete_for_one_product'])){
			if($post_id){
				$product = wc_get_product($post_id);
				self::DeleteProduct($product,$post_id,true);
				echo '<p><span style="color:red;font-weight:800; text-decoration:underline;">'.$product->get_title().'</span> Variables and attributes has been deleted</p>';
				self::resetSKU();
			}
		}elseif(isset($_POST['second_delete_for_all_products'])){
			$args = array(
			'posts_per_page'   => -1,
				'post_type'        => 'product',
                //'paged' => 8
			);
			$posts_array = get_posts( $args );
			
			foreach($posts_array as $post){
				$product = wc_get_product( $post->ID );
				self::DeleteProduct($product,$post->ID,false);
			}
			self::TruncateSizeColorIdsTable();
			echo '<p><span style="color:red;font-weight:800; text-decoration:underline;">ALL PRODUCTS</span> Variables and attributes has been deleted</p>';
			self::resetSKU();
		}elseif(isset($_POST['cancel'])){
			self::resetSKU();
		}/*elseif(isset($_POST['kopi_attributter'])){
			self::kopi_attributter();
		}*/
		
	}
		
}
