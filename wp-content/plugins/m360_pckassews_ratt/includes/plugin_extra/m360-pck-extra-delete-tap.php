<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once ('action_page.php');
$action_page = new M360PCKActionPage();

class M360PCKExtraDeleteTap{

	public function __construct(){
		
	}
	
	
	public function drawThePage() {
		
		$this->settings_html_m360_pck_delete();
		$this->delete_product_html();
		
	}
	
	public function settings_html_m360_pck_delete() {
			?>
			<div class="wrap">
            	<h2>this is the DELETE section </h2>
            </div>
			<?php
    }
	
	
	public function delete_product_html() {
		if(count($_POST)){
			global $action_page;
			$action_page->doTheDelete();
		}
		?>
        <style>
		.submit{
			display: inline-block;
			margin-left:2px;
		}
		.me60_nopadding{
			padding: 0 !important;
		}
		.m360_buttonRed{
			background: #BA0000 !important;
			border-color: #AA0000 #990000 #990000 !important;
			-webkit-box-shadow: 0 1px 0 #990000 !important;
			box-shadow: 0 1px 0 #990000 !important;
			color: #fff !important;
			text-decoration: none;
		}
		</style>
        <?php
			$selected_type = '';
			$sku = '';
			
			if(isset($_POST['delete_type'])){
				$selected_type = $_POST['delete_type'];
			}
			
			if(isset($_POST['sku'])){
				$sku = $_POST['sku'];
			}
		?>
        <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=delete">
        	<table class="form-table">
            	<tbody>
                    <tr style="border-bottom:1px solid #000;">
                    	<td class="me60_nopadding">
                        	Deleting Type:
                        </td>
                        <td class="me60_nopadding">
                            <?php $values = array('delete_one_product_variable' => 'Delete one product variable',
												'delete_all_products_variables'=>'Delete all products variables');
												?>
							<select name="delete_type" style="width: 100%;">
								<?php foreach ( $values as $key => $value):?>
									<?php $selected = '';
                                        if($selected_type == $key ){
                                            $selected = ' selected';
                                        }
                                    ?>
									<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
								<?php endforeach; ?>
								</select>
                        </td>
                        <td class="me60_nopadding" style="width:1%;white-space:nowrap;">
                        	<span style="padding-left:20px;">If type is one product please enter the SKU:</span><span style="padding-left:20px;"><input type="text" name="sku" value="<?php echo $sku; ?>"></span>
						</td>
                        <td class="me60_nopadding">
                        	<?php submit_button('Delete', 'm360_buttonRed', 'first_delete', true, array( 'id' => 'first_delete' )); ?>
                       </td>
					</tr>
                    <?php /*?><tr style="border-bottom:1px solid #000;">
                    	<td class="me60_nopadding">
                        	Kopi attributter fra alle produkter:
                        </td>
                        <td class="me60_nopadding">
                        	<?php submit_button('Kopi', 'primary', 'kopi_attributter', true, array( 'id' => 'kopi_attributter' )); ?>
                       </td>
					</tr><?php */?>
	            </tbody>
            </table>
        </form>
        
        <?php
    }
	

}