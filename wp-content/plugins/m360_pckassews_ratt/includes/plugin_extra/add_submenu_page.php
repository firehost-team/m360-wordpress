<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once ('m360-pck-extra-info-tap.php');
include_once ('m360-pck-extra-delete-tap.php');
include_once ('m360-pck-extra-order-tap.php');
include_once ('m360-pck-extra-fix-tap.php');
include_once ('m360-pck-extra-fix-pckasser-tap.php');
include_once ('m360-pck-ekstra-sizecolor.php');

$info_tap = new M360PCKExtraInfoTap();
$delete_tap = new M360PCKExtraDeleteTap();
$order_tap = new M360PCKExtraOrderTap();
$fix_tap =  new M360PCKExtraFixTap();
$fix_pck_tap = new M360PCKExtraFixPckTap();
$sizeColors = new M360PCKExtraSizeColors();

class M360PCKExtraOptions{

	public function __construct(){

	}
	
	public function create_m360_pck_extra_tabs( $current = 'info' ) {
	
		// a list of the m360_pck option pages 
		$tabs = array( 
			'info' => 'Meldinger', 
			'delete' => 'Delete',
			'order' => 'Order',
            'fix' => 'FIX',
            'fixpck'=> 'Fix PCKasser',
            'sizeColors'=> 'Check SizeColorsIds'
		);
		
		// print m360_pck icon and start tab wrapper
		print '<div id="icon-themes" class="m360_pck_icon"><a class="m360_pck_link" target="_blank" href="http://www.m360.no/">&nbsp;</a></div>';
		print '<h2 class="nav-tab-wrapper">';
		
		foreach( $tabs as $tab => $name ) {
				
			// if tab represents current page, highlight it
			$class = ( $tab == $current ) ? ' nav-tab-active' : '';
			print "<a class='nav-tab$class' href='?page=m360_extra_pck_plugin_options_page&tab=$tab'>$name</a>";
			
		}
		
		// close wrapper
		print '</h2></div>';
	}
	
	public function drawTheTap($tab){
		
		global $delete_tap;
		global $info_tap;
		global $order_tap;
		global $fix_tap;
        global $fix_pck_tap;
        global $sizeColors;

		switch ($tab) {
			case 'info':
				$info_tap->drawThePage();
				break;
			case 'delete':
				$delete_tap->drawThePage();
				break;
			case 'order':
				$order_tap->drawThePage();
				break;
            case 'fix':
                $fix_tap->drawThePage();
                break;
            case 'fixpck':
                $fix_pck_tap->drawThePage();
                break;
            case 'sizeColors':
                $sizeColors->drawThePage();
                break;
			default:
				echo 'default';
		}
		
		
	}
	
	public function drawThePage(){
		if( isset( $_GET['tab'] ) ) 
			$tab = $_GET['tab'];
		
		// start page wrapper
		print '<div class="wrap">';
		print '<h2>M360 PCKasse Extra</h2>';
		
		// set default tab
		if ( !isset( $tab ) || empty( $tab ) )
			$tab = 'info';
		
        // draw the tabs, highlighting current tab
		$this->create_m360_pck_extra_tabs( $tab );
		// start form wrapper 		  
		//settings_fields( 'm360_extra_pck_plugin_options_page_' . $tab . '_group' ); 	
		//do_settings_sections( 'm360_extra_pck_plugin_options_page_' . $tab );
		$this->drawTheTap($tab);
		
		
		// close page wrapper 
        print '</div>';
	}
}
