<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360ORDERPCKActionPage{
	public function __construct(){

	}
	
	private function GetOrderIdByOrderNumber($order_number){
		// get WOOCOMM product id from PCK SKU
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare(
			"SELECT post_id FROM " . $wpdb->postmeta . " WHERE meta_key = '_order_number' AND meta_value = '%s' LIMIT 1", $order_number
		));
	}
	
	private function findeTheOrderInOrderStatusesTable($order_id){
		global $wpdb;
        //$table_name = (strlen($wpdb->base_prefix)>0)?$wpdb->base_prefix."m360_pckasse_orderstatus":$wpdb->prefix . "m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        return $wpdb->get_row($wpdb->prepare(
			"SELECT * FROM ".$table_name. " WHERE order_id = '%s'", $order_id
		));
	}
	
	private function StatusArray(){
		$values = array(M360PCKOrderStatus::STATUS_NEW => 'NEW',
											M360PCKOrderStatus::STATUS_SUBMITTED => 'SUBMITTED',
											M360PCKOrderStatus::STATUS_CANCELED => 'CANCELED',
											M360PCKOrderStatus::STATUS_PARTLYDELIVERED => 'PARTLY DELIVERED',
											M360PCKOrderStatus::STATUS_DELIVERED => 'DELIVERED',);
		return $values;
	}
	private function drawUpdateForm($row,$order_number,$updated = false){

		$found_order_status = $row->status;
		if($updated){
			echo '<p>Order <span style="font-weight:800; color:red">#: '.$order_number.'</span> status has been Changed to: <span style="font-weight:800; color:red">'.$found_order_status.'</span></p>';
		}
		?>
		<form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
			<table class="form-table">
			<tbody>
				<tr style="border-bottom:1px solid #000;">
					<td class="me60_nopadding m360_fit">
						Order <span style="font-weight:800; color:red">#:<?php echo $order_number ?></span>:
					</td>
					<td class="me60_nopadding m360_fit">
						<?php $values = $this->StatusArray(); ?>
						<select name="status">
							<?php foreach ( $values as $key => $value):?>
								<?php $selected = '';
									if($found_order_status == $key ){
										$selected = ' selected';
									}
								?>
								<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
							<?php endforeach; ?>
							</select>
                            <input type="hidden" name="order_number_to_update" value="<?php echo $order_number; ?>">
					</td>
					<td class="me60_nopadding">
						<?php submit_button('Update', 'm360_buttonRed', 'update', true, array( 'id' => 'update' )); ?>
				   </td>
				</tr>
			</tbody>
		</table>
		</form>
		<?php
	}
	
	private function drawCreateForm($order_number,$order_id){
		?>
		<form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
			<table class="form-table">
			<tbody>
				<tr style="border-bottom:1px solid #000;">
					<td class="me60_nopadding m360_fit">
						Order# <span style="padding-left:20px;"><input type="text" name="order_number_new" value="<?php echo $order_number; ?>"></span>:
					</td>
					<td class="me60_nopadding m360_fit">
						<?php $values = $this->StatusArray(); ?>
						<select name="status">
							<?php foreach ( $values as $key => $value):?>
								<?php $selected = '';
									if($found_order_status == $key ){
										$selected = ' selected';
									}
								?>
								<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
							<?php endforeach; ?>
							</select>
                            <input type="hidden" name="order_number_to_create" value="<?php echo $order_number; ?>">
                            <input type="hidden" name="order_id_to_create" value="<?php echo $order_id; ?>">
					</td>
                    <td class="me60_nopadding m360_fit">
						Total items ordered: <span style="padding-left:20px;"><input type="text" name="order_total" value=""></span>
					</td>
					<td class="me60_nopadding">
						<?php submit_button('Create', 'm360_buttonRed', 'create_new', true, array( 'id' => 'create_new' )); ?>
				   </td>
				</tr>
			</tbody>
		</table>
		</form>
		<?php
	}
	
	public function InsertNewOrderIntoOrderStatusTable(){
		$order_number = $_POST['order_number_to_create'];
		$order_id = $_POST['order_id_to_create'];
		$wc_order_total = $_POST['order_total'];
		$blog_id = (is_multisite())?get_current_blog_id():0;
		M360PCKOrderStatus::UpdateOrderStatus($order_id,$wc_order_total,0,M360PCKOrderStatus::STATUS_NEW,$blog_id);
		echo '<p>Order <span style="font-weight:800; color:red">#: '.$order_number.'</span> has been inserted into M360 PCkasse order status</p>';
	}
	
	public function findTheOrder($order_number){
		$order_id = $order_number;//$this->GetOrderIdByOrderNumber($order_number);
		if(!$order_id){
			echo '<p>Order <span style="font-weight:800; color:red">#: '.$order_number.'</span> does not exist in woocommerce orders</p>';
		}else{
			$row = $this->findeTheOrderInOrderStatusesTable($order_id);
			if($row){
				$row->status;
				$this->drawUpdateForm($row,$order_number);
			}else{
				$this->drawCreateForm($order_number,$order_id);
			}	
		}
	}
	
	public function updateTheOrder($order_number){
        $blog_id = (is_multisite())?get_current_blog_id():0;

        $selected_status = $_POST['status'];
		$order_id = $order_number;//$this->GetOrderIdByOrderNumber($order_number);
		$row = $this->findeTheOrderInOrderStatusesTable($order_id);
		M360PCKOrderStatus::UpdateOrderStatus($order_id,$row->wc_order_total,$row->wc_order_delivered,$selected_status,$blog_id);
		$row_result = $this->findeTheOrderInOrderStatusesTable($order_id);
		$this->drawUpdateForm($row_result,$order_number);
	}
	
	
}