<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360WarehousesTap extends M360_PCKasseWS_Settings{

    public function __construct(){
        if (isset($_POST['delete_all_warehouses'])){
            global $wpdb;
            $warehouse = m360_pck_get_table_name("m360_pckasse_warehouses");
            $warehouse_frntend = m360_pck_get_table_name("m360_pckasse_frontend_warehouses");

            $wpdb->query("TRUNCATE TABLE {$warehouse}");
            $wpdb->query("TRUNCATE TABLE {$warehouse_frntend}");
        }
    }

    public function init_warehouses(){
        // register Logging options group
        register_setting(
            'm360_pck_options_warehouses_group', // group
            'm360_pck_options_warehouses', // name
            array($this, 'sanitise') // sanitise method
        );

        // Logging Options
        add_settings_section(
            'm360_pck_options_section_warehouses', // id
            'M360 PCKasseWS warehouses Options', // title
            array($this, 'settings_html_m360_pck_warehouses_message'), // callback
            'm360_pck_options_page_warehouses' // page
        );


        add_settings_field(
            'm360_pck_warehouses', // id
            'sort warehouses', // title
            array( $this, 'm360_pck_warehouses_html' ), // callback
            'm360_pck_options_page_warehouses', // page
            'm360_pck_options_section_warehouses' // section
        );

    }

    public function m360_pck_warehouses_html(){
        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $warehouses = getAllwarehouses($kasse_nr);

        if(isset($_POST['list'])){
            //WriteLog('POST: '.print_r($_POST['list'],true));
            $list = $_POST['list'];
            $new = array();
            foreach ($list as $item){
                $new[] = getWarehousBySortId($item['id'],$kasse_nr);
            }
            //WriteLog('NEW: '.print_r($new,true));
            UpdateWarehousesSort($new);
        }
        ?>
        <style>
            .submit{
                display: none;
            }
            #resort_warehouses {
                position: fixed;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                text-align: center;
                background-color:rgba(255,255,255,0.9);
                z-index: 999;
            }
            #resort_warehouses img{
                width: 100px;
                padding: 40px;
                position: absolute;
                top: 50%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                text-align: center;
            }

            #resort_warehouses p{
                width: 400px;
                padding: 40px;
                position: absolute;
                top: 70%;
                left: 50%;
                -webkit-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                text-align: center;
            }
            .m360_pull_right{
                float: right;
            }
            .m360_pull_right a{
                color: red;
            }
        </style>

        <?php if(count($warehouses)<=0):?>
            <p>you dont have any warehouses</p>
        <?php else:?>
            <div class="dd" id="nestable">
                <ol class="dd-list">
                    <?php foreach($warehouses as $warehouse):?>
                        <li class="dd-item dd3-item" data-id="<?php echo $warehouse->sort_id; ?>">
                            <div class="dd-handle dd3-handle">Drag</div>
                            <div class="dd3-content">
                                lager id i pck er { <?php echo $warehouse->warehouseId; ?> }
                            </div>
                        </li>
                    <?php endforeach;?>
                </ol>
            </div>
            <div id='resort_warehouses' style="display: none">
                <img src="<?php echo plugin_dir_url( __FILE__ ) . '../assets/images/loading_spinner.gif' ?>" />
                <p>Please wait while updating warehouse order</p>
            </div>

            <div>
                <form action="" method="post">
                    <input type="submit" value="Slett alle lagrer fra nettbutikk" id="delete_all_warehouses" name="delete_all_warehouses">
                </form>
            </div>

            <script>
                var $j = jQuery.noConflict();
                $j(document).ready(function() {
                    var updateOutput = function (e) {
                        $j('#resort_warehouses').show();
                        var list   = e.length ? e : $j(e.target), output = list.data('output');
                        $j.ajax({
                            method: "POST",
                            data: {
                                list: list.nestable('serialize'),
                            }
                        }).done(function(result){
                            $j( "#resort_warehouses" ).hide();
                        }).fail(function(jqXHR, textStatus, errorThrown){
                            alert("Unable to save new list order: " + errorThrown);
                        });
                    };

                    $j('#nestable').nestable().on('change', updateOutput);
                });


            </script>
        <?php endif;?>
        <?php
    }
    public function settings_html_m360_pck_warehouses_message(){
        print '<p>Warehouses from pckasse , when order send from store to pck , pck will reduce the inventory as it sorting her</p>';
    }


}