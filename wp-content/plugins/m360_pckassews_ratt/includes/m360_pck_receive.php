<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_service.php' );



class M360PCKReceiver{

    public static function retriveKasseNr(){
        global $current_PCK;
        return (is_object($current_PCK) && isset($current_PCK->PCKasseNr))?$current_PCK->PCKasseNr:0;
    }

    public static function purge_product_varnish($product_id) {
        $_pf                = new WC_Product_Factory();
        $_product           = $_pf->get_product($product_id);
        if(!$_product) return FALSE;

        $product_cats_ids   = wc_get_product_term_ids($_product->get_id(), 'product_cat');
        $links              = [get_permalink($product_id)];
        $category_links     = [];

        foreach($product_cats_ids as $cat_id) {
            $link = get_term_link($cat_id, 'product_cat');;
            $category_links[] = $link;
            $links[] = $link;
        }

        $pagination = 0;
        while($pagination < 20) {
            $pagination++;
            foreach($category_links as $link) {
                $links[] = "{$link}page/{$pagination}";
                $links[] = "{$link}page/{$pagination}/";
            }
        }

        foreach($links as $url) {
            $p = wp_parse_url($url);

            $host_headers = $p['host'];
            if(isset($p['port'])) {
                $host_headers .= ':' . $p['port'];
            }

            $headers = apply_filters('varnish_http_purge_headers', array(
                'host'           => $host_headers,
                'X-Purge-Method' => 'default',
            ));

            $response = wp_remote_request($url, array(
                'method'  => 'PURGE',
                'headers' => $headers,
            ));
        }

        return TRUE;
    }

    public static function saveChildBlogProduct($product_id,$to_blog_id){
        update_post_meta($product_id , '_woonet_network_main_product', true);
        $_woonet_publish_to = '_woonet_publish_to_' . $to_blog_id;
        update_post_meta( $product_id, $_woonet_publish_to, 'yes');

        include_once(WOO_MSTORE_PATH . '/include/class.admin.product.php');
        global $blog_id;
        global $wpdb;

        $main_blog_id =   $blog_id;

        switch_to_blog( $main_blog_id );
        $post_data = get_post($product_id);
        $meta_values = get_post_meta( $post_data );

        $product_interface                =   new WOO_MSTORE_admin_product();
        $product_interface->process_product($product_id, $post_data, TRUE);
        //wp_update_post();
        restore_current_blog();

        $table_name = (strlen($wpdb->base_prefix)>0)?$wpdb->base_prefix.$to_blog_id."_postmeta":$wpdb->prefix .$to_blog_id. "postmeta";
        $qur = "SELECT post_id FROM " . $table_name . " WHERE meta_key = '_woonet_network_is_child_product_id' AND meta_value = '".$product_id."'  LIMIT 1";
        $child_id =  $wpdb->get_var($qur);

        if($child_id){
            switch_to_blog( $to_blog_id );

            foreach ( $meta_values as $key => $value ) {
                update_post_meta( $child_id, $key, $value );
            }
            update_post_meta($child_id, '_woonet_child_inherit_updates', 'yes');
            update_post_meta($child_id, '_woonet_child_stock_synchronize', 'yes');
            restore_current_blog();
        }

    }

    public static function updateMultiStore($product_id,$Product){
        if(USE_MULTISORE){
            $vat = 1+($Product->vat/100);
            if(!empty($Product->price6) && $Product->price6>0){
                $to_blog = $Product->price6/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price7) && $Product->price7>0){
                $to_blog = $Product->price7/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price8) && $Product->price8>0){
                $to_blog = $Product->price8/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }

            if(!empty($Product->price9) && $Product->price9>0){
                $to_blog = $Product->price9/$vat;
                self::saveChildBlogProduct($product_id,(int)$to_blog);
            }
        }else{
            include_once(WOO_MSTORE_PATH . '/include/class.admin.product.php');
            global $blog_id;
            $main_blog_id =   $blog_id;
            switch_to_blog( $main_blog_id );
            $post_data = get_post($product_id);
            $product_interface                =   new WOO_MSTORE_admin_product();
            $product_interface->process_product($product_id, $post_data, TRUE);
            //wp_update_post();
            restore_current_blog();
        }



    }

    /* end mult stor */

    public static function IsExistingProduct($articleId,$articleNo=NULL){
        global $wpdb;
        $table_name = m360_pck_get_table_name("postmeta");
        if($articleNo){
            $ProductId = $wpdb->get_var($wpdb->prepare(
                "SELECT post_id FROM $table_name WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1",$articleNo
            ));
        }else{
            $ProductId = $wpdb->get_var($wpdb->prepare(
                "SELECT post_id FROM $table_name WHERE meta_key = 'm360_pck_article_id_".m360_pck_kassenr()."' AND meta_value = '%s' LIMIT 1",$articleId
            ));
        }

        if(is_numeric($ProductId)){
            $status = get_post_status($ProductId);
            if($status == 'trash'){
                wp_untrash_post( $ProductId );
            }

            if($status == 'draft'){
                wp_publish_post($ProductId);
            }
            
            return true;
        }

        return false;
    }



    private static function GetWOOCOMMProductIdFromPCKArticleId($articleId){
        global $wpdb;
        $kasse_nr = self::retriveKasseNr();
        /*
                if(is_multisite()) {
                    $current_blog_id = get_current_blog_id();
                    switch_to_blog( $current_blog_id );
                    $table_name = $wpdb->postmeta;
                    restore_current_blog();
                }else{
                    $table_name = $wpdb->postmeta;
                }
        */
        $table_name = m360_pck_get_table_name("postmeta");
        $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id_{$kasse_nr}' AND meta_value = '{$articleId}' LIMIT 1";

        $id = $wpdb->get_var($qur);
        if($id<=0){
            $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id_' AND meta_value = '{$articleId}' LIMIT 1";
            $id = $wpdb->get_var($qur);
            if($id<=0) {
                $qur = "SELECT post_id FROM {$table_name} WHERE meta_key = 'm360_pck_article_id' AND meta_value = '{$articleId}' LIMIT 1";
                $id = $wpdb->get_var($qur);
            }

            if($id>0){
                delete_post_meta($id, 'm360_pck_article_id');
                add_post_meta($id, 'm360_pck_article_id_'.$kasse_nr, $articleId, true);
            }
        }
        return $id;
    }


    /* new */
    private static function InsertCategoryIntoWOOCOMM($articleGroup){
        $slug = CreateSlug($articleGroup->name);
        $Term = get_term_by('slug', $slug, 'product_cat' );
        if (!$Term->term_id || $Term->term_id == 0) {
            $Term = wp_insert_term($articleGroup->name, 'product_cat', array('description' => ($articleGroup->description)?$articleGroup->description:' ', 'slug' => $slug));
        }

        return $Term->term_id;

    }

    private static function InsertSubCategoryIntoWOOCOMM($articleGroup, $ParentId){
        $Term = term_exists($articleGroup->name, 'product_cat', $ParentId);
        if ($Term !== 0 && $Term !== null) {
            return $Term['term_id'];
        }else{
            $Term = wp_insert_term($articleGroup->name, 'product_cat', array('slug' => CreateSlug($articleGroup->name), 'parent' => $ParentId));
            if (array_key_exists('error_data', $Term)) {
                return $Term->error_data['term_exists'];
            }else{
                return $Term['term_id'];
            }

        }
    }
    /* ---- */

    private static function UpdateDeliveryPrice($SKU, $Price){
        // get WOOCOMM PCK options from WordPress database
        $PCKOptionsShipping = get_option('m360_pck_options_shipping');

        // check each supported type of shipping to see if the SKU matches any that have been mapped in the WordPress PCK options
        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['free_shipping'] == $SKU)
        {
            // update the minimum amount to qualify for free shipping
            $WoocommSetting = get_option('woocommerce_free_shipping_settings');
            $WoocommSetting['min_amount'] = $Price;
            update_option('woocommerce_free_shipping_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['local_delivery'] == $SKU)
        {
            // update the local delivery flat rate fee
            $WoocommSetting = get_option('woocommerce_local_delivery_settings');
            $WoocommSetting['fee'] = $Price;
            update_option('woocommerce_local_delivery_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['flat_rate'] == $SKU)
        {
            // update the flat rate delivery fee
            $WoocommSetting = get_option('woocommerce_flat_rate_settings');
            $WoocommSetting['cost_per_order'] = $Price;
            update_option('woocommerce_flat_rate_settings', $WoocommSetting);

            return true;
        }

        if ($PCKOptionsShipping['m360_pck_shipping_mapping']['international_delivery'] == $SKU)
        {
            // update the cost of international delivery
            $WoocommSetting = get_option('woocommerce_international_delivery_settings');
            $WoocommSetting['cost'] = $Price;
            update_option('woocommerce_international_delivery_settings', $WoocommSetting);

            return true;
        }

        // we're not dealing with shipping here, so tell the calling method to continue...
        return false;
    }

    public static function pricing($ProductId,$Product){
        $wc_product = wc_get_product($ProductId);
        if($wc_product->is_type('variable')){
            update_post_meta( $ProductId, '_sale_price_dates_to', '' );
            update_post_meta( $ProductId, '_sale_price_dates_from', '' );
            update_post_meta( $ProductId, '_price', '');
            update_post_meta( $ProductId, '_sale_price','');

            $wc_product->set_date_on_sale_from('');
            $wc_product->set_date_on_sale_to('');
            $wc_product->set_sale_price('');
            $wc_product->set_price('');
            $wc_product->save();
            wc_delete_product_transients( $ProductId );

            return;
        }
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $oreavrunding = 'no';
        if(isset($productTabOptions[ 'm360_pck_product_oreavrunding'] )){
            $oreavrunding = $productTabOptions[ 'm360_pck_product_oreavrunding'];
        }

        $use_veil_pris = 'no';
        if(isset($productTabOptions[ 'setting_html_m360_pck_veilpris_i_pris'] )){
            $use_veil_pris = $productTabOptions[ 'setting_html_m360_pck_veilpris_i_pris'];
        }
        $pck_pris = $Product->salesPrice;
        if($use_veil_pris == 'yes' && $Product->suggestedPrice > $Product->salesPrice){
            $pck_pris = $Product->suggestedPrice;
        }


        $date_from = isset( $Product->discountFrom ) ? date('Y-m-d H:i:s', $Product->discountFrom/1000) : get_post_meta( $ProductId, '_sale_price_dates_from', true );
        $date_to   = isset( $Product->discountTo ) ? date('Y-m-d H:i:s', $Product->discountTo/1000) : get_post_meta( $ProductId, '_sale_price_dates_to', true );


        if($oreavrunding == 'yes')$pck_pris = floor($pck_pris);

        $discount_price = self::getDiscountPrice($Product);

        if(self::isTaxtable() != 'taxable' && $Product->vat > 0){
            $precentage = '1.'.intval($Product->vat);
            if($pck_pris>0) $pck_pris = $pck_pris/$precentage;
            if($discount_price>0) $discount_price = $discount_price/$precentage;
        }

        if (method_exists($wc_product, 'set_price')){
            $wc_product->set_regular_price($pck_pris);
            if (strtotime( 'NOW', current_time( 'timestamp' ) ) < strtotime( $date_to ) && $discount_price > 0) {
                $wc_product->set_date_on_sale_from(strtotime( $date_from ));
                $wc_product->set_date_on_sale_to(strtotime( $date_to ));
                $wc_product->set_sale_price($discount_price);
                $wc_product->set_price($discount_price);
            }else{
                $wc_product->set_date_on_sale_from('');
                $wc_product->set_date_on_sale_to('');
                $wc_product->set_sale_price(($Product->salesPrice >= $pck_pris)?'':$Product->salesPrice );
                $wc_product->set_price($pck_pris);
            }
            $wc_product->save();
        } else {
            update_post_meta( $ProductId, '_regular_price', $pck_pris);
            if (strtotime( 'NOW', current_time( 'timestamp' ) ) < strtotime( $date_to ) && $discount_price > 0) {
                update_post_meta( $ProductId, '_sale_price_dates_from', strtotime( $date_from ) );
                update_post_meta( $ProductId, '_sale_price_dates_to', strtotime( $date_to ) );
                update_post_meta( $ProductId, '_sale_price', $discount_price);
                update_post_meta( $ProductId, '_price', $discount_price);
            }else{
                update_post_meta( $ProductId, '_sale_price_dates_to', '' );
                update_post_meta( $ProductId, '_sale_price_dates_from', '' );
                update_post_meta( $ProductId, '_price', $pck_pris);
                update_post_meta( $ProductId, '_sale_price',($Product->salesPrice >= $pck_pris)?'':$Product->salesPrice);
            }
        }

        if (strtotime( 'NOW', current_time( 'timestamp' ) ) < strtotime( $date_to ) && $discount_price > 0) {
            update_post_meta( $ProductId, '_price', $discount_price);
        }else{
            update_post_meta( $ProductId, '_sale_price',($Product->salesPrice >= $pck_pris)?'':$Product->salesPrice);
        }

    }

    public static function calculateBackorder($ProductId,$product_info = false){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $backorder = (isset($productTabOptions[ 'm360_pck_standardbackorder']))?$productTabOptions[ 'm360_pck_standardbackorder']:'yes';
        $m360_pck_hideWhenOutOfStock = get_post_meta( $ProductId, 'm360_pck_hideWhenOutOfStock',true);

        if(!empty($m360_pck_hideWhenOutOfStock)){
            $backorder = (isset($productTabOptions[ 'm360_pck_backorder']))?$productTabOptions[ 'm360_pck_backorder']:'no';
        }
        $nonStockItem = get_post_meta($ProductId, 'm360_pck_nonStockItem',true );
        if($nonStockItem)$backorder = 'yes';

        if($product_info){
            if($product_info->nonStockItem){
                update_post_meta($ProductId,'m360_pck_nonStockItem',true);
                $backorder = 'yes';
            }
        }
        return $backorder;
    }

    private static function calculate_multi_pckasser_count($ProductId,$count){
        global $current_PCK;
        $kasse_nr = self::retriveKasseNr();

        update_post_meta($ProductId, 'm360_stock_for_pck_'.$kasse_nr, $count);
        update_post_meta($ProductId, 'm360_location_for_pck_'.$kasse_nr, $current_PCK->PCKLocation);

        $current_pck_stock = 0;

        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $numberOfPCKasser = isset( $setupTabOptions[ 'm360_number_of_pckasser' ] ) ? $setupTabOptions[ 'm360_number_of_pckasser' ]  : 1;

        for($i = 0; $i<$numberOfPCKasser ; $i++){
            $current_pck_stock += get_post_meta( $ProductId, 'm360_stock_for_pck_'.$i, true );
        }
        return $current_pck_stock;
    }

    private static function handlingWarehousesOptions($ProductId){
        $kasse_nr = m360_pck_kassenr();
        $stockDetails = get_post_meta($ProductId,'stockDetails_'.$kasse_nr,true);

        if($stockDetails && is_array($stockDetails)){
            foreach ($stockDetails as $key => $stockDetail){
                //$wearhous = getWarehousByWarehouseId($stockDetail->warehouseId,$kasse_nr);
                if($stockDetail && is_object($stockDetail))UpdateWarehouses($stockDetail->warehouseId,$kasse_nr);
            }
        }
        /*
        $all_warehouses = getAllwarehouses($kasse_nr);

        $warehousesOptions = get_option( 'm360_pck_options_warehouses' );
        $warehousesOptions[ 'm360_pck_warehouses' ] = $all_warehouses;

        update_option( 'm360_pck_options_warehouses', $warehousesOptions );
        */
    }
    private static function handlingStockDetails($ProductId,$Product){
        $kasse_nr = self::retriveKasseNr();
        if(property_exists($Product,'stockDetails')){
            $stockDetails = $Product->stockDetails;
            update_post_meta($ProductId,'stockDetails_'.$kasse_nr,$stockDetails);
            if(is_object($stockDetails)&& property_exists($stockDetails,'warehouseId')){
                UpdateFrontEndWarehouses($Product->stockDetails->warehouseId,$kasse_nr);
            }else if(is_array($stockDetails)){
                foreach ($stockDetails as $stockDetail){
                    if(is_object($stockDetail)&& property_exists($stockDetail,'warehouseId')){
                        UpdateFrontEndWarehouses($stockDetail->warehouseId,$kasse_nr);
                    }
                }
            }
        }

    }
    private static function handling_stock($ProductId,$Product){
        $current_pck_stock = self::calculate_multi_pckasser_count($ProductId,$Product->stockCcount);

        update_post_meta($ProductId, 'm360_pck_hideWhenOutOfStock', $Product->hideWhenOutOfStock);

        $backorder = self::calculateBackorder($ProductId);
        $instock = self::calculateInStock($backorder,$current_pck_stock);
        self::handlingStockDetails($ProductId,$Product);
        $manageStock = 'yes';
        if($current_pck_stock<=0 && $backorder == 'yes'){
            $manageStock = 'no';
        }

        $wc_product = wc_get_product($ProductId);
        if($wc_product){
            $visibility = ($Product->visibleOnWeb)?'visible':'private';
            $wc_product->set_catalog_visibility($visibility);
            $wc_product->set_manage_stock($manageStock);
            $wc_product->set_downloadable('no');
            $wc_product->set_virtual('no');
            $wc_product->set_sold_individually('');
            $wc_product->set_backorders($backorder);
            $wc_product->set_stock_quantity($current_pck_stock);
            $wc_product->set_stock_status(wc_clean($instock));
            $wc_product->save();

        }

    }

    private static function physical_properties($ProductId,$Product){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $standardWeight = isset( $productTabOptions [ 'm360_pck_default_weight' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_weight' ] ) : '1';
        $standardLength = isset($productTabOptions [ 'm360_pck_default_length' ] ) ?esc_attr( $productTabOptions [ 'm360_pck_default_length' ] ) : '10';
        $standardWidth = isset( $productTabOptions [ 'm360_pck_default_width' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_width' ] ) : '10';
        $standardHeight = isset( $productTabOptions [ 'm360_pck_default_height' ] ) ? esc_attr( $productTabOptions [ 'm360_pck_default_height' ] ) : '10';

        $weight = ($Product->weight > 0)?$Product->weight:$standardWeight;
        $length = ($Product->length > 0)?$Product->length:$standardLength;
        $width = ($Product->width > 0)?$Product->width:$standardWidth;
        $height = ($Product->height > 0)?$Product->height:$standardHeight;
        $wc_product = wc_get_product($ProductId);
        $wc_product->set_weight($weight);
        $wc_product->set_length($length);
        $wc_product->set_width($width);
        $wc_product->set_height($height);
        $wc_product->save();

    }

    public static function CheckTheGroups($ProductId,$Product){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_sync_categories' ])){
            if($productTabOptions[ 'm360_pck_sync_categories' ] == 'not_sync'){
                return;
            }
        }

        $articleGroup = $Product->articleGroup;
        $articleGroup2 = $Product->articleGroup2;
        $articleGroup3 = $Product->articleGroup3;

        $SubCategoryIds = array();

        if(is_object($articleGroup)&& $articleGroup->name){
            $CategoryId_1 = self::InsertCategoryIntoWOOCOMM($articleGroup);
            $SubCategoryIds[] = $CategoryId_1;
            if(is_object($articleGroup2)&& $articleGroup2->name && $CategoryId_1){
                $CategoryId_2 = self::InsertSubCategoryIntoWOOCOMM($articleGroup2,$CategoryId_1);
                $SubCategoryIds[] = $CategoryId_2;

                if(is_object($articleGroup3)&& $articleGroup3->name && $CategoryId_2){
                    $CategoryId_3 = self::InsertSubCategoryIntoWOOCOMM($articleGroup3,$CategoryId_2);
                    $SubCategoryIds[] = $CategoryId_3;
                }
            }
            wp_set_post_terms($ProductId, $SubCategoryIds, "product_cat",true);
        }
    }

    public static function handelTaxClass($Product){
        $tax_class_to_return = '';
        if($Product->vat < 25 && $Product->vat > 0){
            $tax_class_to_return = 'reduced-rate';
        }else if($Product->vat <= 0){
            $tax_class_to_return = 'zero-rate';
        }
        $args = wp_parse_args(array(
            'country'   => 'NO',
            'state'     => '',
            'city'      => '',
            'postcode'  => '',
            'tax_class' => $tax_class_to_return
        ) );

        $tax_classes         = WC_Tax::find_rates($args);
        $current_class = array();
        $loop = 0;
        foreach ($tax_classes as $tax_class){
            if($tax_class['rate'] == $Product->vat){
                $current_class = $tax_class;
            }
            $loop++;
        }
        if(empty($current_class)){
            $tax_rate = array(
                'tax_rate_country'  => 'NO',
                'tax_rate'          => $Product->vat,
                'tax_rate_name'     => 'VAT '.intval($Product->vat).'%',
                'tax_rate_priority' => 1,
                'tax_rate_order'    => $loop,
                'tax_rate_class'    => $tax_class_to_return
            );

            WC_Tax::_insert_tax_rate( $tax_rate );
        }

        return $tax_class_to_return;
    }
    public static function isTaxtable(){
        $taxable = 'taxable';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_product_tax'] )){
            $taxable = ($productTabOptions[ 'm360_pck_product_tax'] == 'with')?'taxable':'none';
        }
        return $taxable;
    }
    public static function updatePostMetaForProduct($ProductId,$Product){
        self::CheckTheGroups($ProductId,$Product);
        $tax_class = self::handelTaxClass($Product);
        update_post_meta($ProductId, '_sku', $Product->articleNo);
        $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );

        if($woocommerce_synk_pris_checkbox == 'yes') {
            self::pricing($ProductId, $Product);
        }
        self::handling_stock($ProductId,$Product);

        self::physical_properties($ProductId,$Product);

        $feature = ($Product->recommendedProduct)?'yes':'no';

        update_post_meta($ProductId, '_purchase_note', '');
        $wc_product = wc_get_product( absint($ProductId) );

        if ( $wc_product ) {
            $wc_product->set_featured($feature);
            $wc_product->set_tax_class($tax_class);
            $wc_product->set_tax_status(self::isTaxtable());
            $wc_product->save();

        }

        $kasse_nr = m360_pck_kassenr();

        add_post_meta($ProductId, 'm360_pck_article_id_'.$kasse_nr, $Product->articleId, true);
        //add_post_meta($ProductId, 'm360_pck_article_id', $Product->articleId, true); // for enkelt
        update_post_meta( $ProductId, '_sizeColorId_'.$kasse_nr, $Product->articleId );

        //update_post_meta($ProductId, '_tax_status', 'self::isTaxtable()');
        //update_post_meta($ProductId, '_tax_class', '');
    }


    public static function getNewProductId($Product){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return SKUTOPOSTID($Product->articleNo);

        $ProductStub = array(
            'post_title' => $Product->name,
            'post_status' => 'publish',
            'post_type' => 'product'
        );

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_description'] )?$productTabOptions[ 'm360_pck_description']:'both_if_empty';

        if($saved == 'long_description' || $saved == 'long_description_if_empty'){
            $ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description' || $saved == 'short_description_if_empty'){
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both' || $saved == 'both_if_empty'){
            $ProductStub['post_content'] = $Product->description;
            $ProductStub['post_excerpt'] = $Product->description;
        }

        $ProductId = wp_insert_post($ProductStub);
        return $ProductId;
    }

    public static function SizeColorSKU($sizeColor,$articleNo,$colorTitle,$sizeTitle){
        $sizeColorId = $sizeColor->sizeColorId;

        $sku = $articleNo;
        if(strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false){
            $sku .= '-'.$sizeColor->color->colorId;
        }

        if(strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false){
            $sku .= '-' . $sizeColor->size->sizeId;
        }
        $sku .= '-' . $sizeColorId;
        return $sku;
    }

    private static function SizeColorTitle($ProductId,$colorTitle,$sizeTitle){
        $post_title = '';
        if(strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false){
            $post_title .= $colorTitle .'-';
        }

        if(strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false){
            $post_title .= $sizeTitle.'-';
        }
        $post_title .= ' for #' . $ProductId;
        return $post_title;
    }

    private static function createVariableProduct($sizeColor,$ProductId,$Product){
        $color_attribute = self::colorAttribute();
        $size_attribute = self::sizeAttribute();

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';

        $colorTitle = trim($sizeColor->color->name);
        if($always_color == 'yes'){
            if(!$colorTitle || strlen($colorTitle)<= 0 || strpos($colorTitle, self::getNoColorText()) === true ){
                $colorTitle = 'unicolor';
            }
        }

        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';
        $sizeTitle = trim($sizeColor->size->name);
        if($always_size == 'yes'){
            if(!$sizeTitle || strlen($sizeTitle)<= 0 || strpos($sizeTitle, self::getNoColorText()) === true ){
                $sizeTitle = 'unisize';
            }
        }

        $post_name = (property_exists($sizeColor,'eans'))?$sizeColor->eans:self::SizeColorSKU($sizeColor,$Product->articleNo,$colorTitle,$sizeTitle);
        if(is_array($post_name) && count($post_name)>0)$post_name = $post_name[0];

        $post_title = self::SizeColorTitle($ProductId,$colorTitle,$sizeTitle);
        $my_post = array(
            'post_title' => $post_title,
            'post_name' => $post_name,
            'post_status' => 'publish',
            'post_parent' => $ProductId,
            'post_type' => 'product_variation',
            'guid' => home_url() . '/?product_variation=' . $post_name
        );
        //global $wpdb;
        global $current_PCK;

        $kasse_nr = m360_pck_kassenr();
        $attID = getVariationIdFromSizeColorId($sizeColor->sizeColorId,$kasse_nr);

        //if(!m360_pck_is_default_pck($current_PCK) && !$attID)$attID = SKUTOPOSTID($post_name);
        $_tmp_product = wc_get_product($attID);
        $parent_product = wc_get_product($ProductId);
        if($_tmp_product && $_tmp_product->get_id()>0 && !in_array($_tmp_product->get_id(),$parent_product->get_children())){
            wp_delete_post($attID,true);
            $_tmp_product = false;
        }


        if (!$_tmp_product || !$_tmp_product->is_type('variation')) {
            $attID = SKUTOPOSTID( $post_name );
            if($attID && $attID >0){
                $n_sizeColorId = get_post_meta( $attID, '_sizeColorId_'.$kasse_nr, true );
                UpdateSizeColorID($attID,$n_sizeColorId,$kasse_nr);
            }else{
                $attID = wp_insert_post($my_post);
                add_post_meta( $attID, '_sizeColorId_'.$kasse_nr, $sizeColor->sizeColorId );
                UpdateSizeColorID($attID,$sizeColor->sizeColorId,$kasse_nr);
            }
        }
        $_tmp_product = wc_get_product($attID);
        
        if($_tmp_product && $_tmp_product->get_parent_id() != $ProductId){
            $attID = wp_insert_post($my_post);
            add_post_meta( $attID, '_sizeColorId_'.$kasse_nr, $sizeColor->sizeColorId );
            UpdateSizeColorID($attID,$sizeColor->sizeColorId,$kasse_nr);
        }

        if(!empty($sizeColor->eans) && $sizeColor->eans > 0){
            update_post_meta($attID, 'm360_pck_eans', $sizeColor->eans);
        }
        $current_pck_stock = self::calculate_multi_pckasser_count($attID,$sizeColor->stockCount);

        update_post_meta($attID, 'm360_pck_hideWhenOutOfStock', $Product->hideWhenOutOfStock);
        $backorder = self::calculateBackorder($ProductId);
        $instock = self::calculateInStock($backorder,$current_pck_stock);
        $manageStock = 'yes';
        if($current_pck_stock<=0 && $backorder == 'yes'){
            $manageStock = 'no';
        }

        self::handlingStockDetails($attID,$sizeColor);
        $wc_product = wc_get_product($attID);

        if(m360_pck_is_default_pck($current_PCK)){
            if($colorTitle && ctype_space($colorTitle))$colorTitle = NULL;
            if($sizeTitle && ctype_space($sizeTitle)) $sizeTitle = NULL;


            if($colorTitle && strlen($colorTitle)>0 && strpos($colorTitle, self::getNoColorText()) === false && $color_attribute != 'no_attribute'){
                $farge_attribute_term = get_term_by('name', $colorTitle, $color_attribute);

                if(is_object($farge_attribute_term) && property_exists($farge_attribute_term,'slug') && $farge_attribute_term->slug){
                    update_post_meta($attID, 'attribute_'.$color_attribute, $farge_attribute_term->slug);
                }
            }
            if($sizeTitle && strlen($sizeTitle)>0 && strpos($sizeTitle, self::getNoSizeText()) === false && $size_attribute != 'no_attribute'){
                $storrelse_attribute_term = get_term_by('name', $sizeTitle, $size_attribute);
                if(is_object($storrelse_attribute_term) && property_exists($storrelse_attribute_term,'slug') && $storrelse_attribute_term->slug){
                    update_post_meta($attID, 'attribute_'.$size_attribute, $storrelse_attribute_term->slug);
                }
            }


            $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );
            if($woocommerce_synk_pris_checkbox == 'yes') {
                self::pricing($attID, $Product);
            }
            if($wc_product){
                update_post_meta($attID, '_sku', $post_name);
                $wc_product->set_downloadable('no');
                $wc_product->set_virtual('no');
            }
        }

        if($wc_product){
            $wc_product->set_manage_stock($manageStock);
            $wc_product->set_backorders($backorder);
            $wc_product->set_stock_quantity($current_pck_stock);
            $wc_product->set_stock_status(wc_clean($instock));
            $wc_product->save();
        }
    }

    public static function colorAttribute(){
        $color_attribute = 'no_attribute';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_color_attribute' ] )){
            if($productTabOptions[ 'm360_pck_color_attribute' ]!='no_attribute'){
                $color_attribute = $productTabOptions[ 'm360_pck_color_attribute' ];
            }
        }
        return $color_attribute;
    }

    public static function sizeAttribute(){
        $size_attribute = 'no_attribute';
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_size_attribute' ] )){
            if($productTabOptions[ 'm360_pck_size_attribute' ]!='no_attribute'){
                $size_attribute = $productTabOptions[ 'm360_pck_size_attribute' ];
            }
        }
        return $size_attribute;

    }

    private static function createAttribute($ProductId,$colors=NULL,$sizes=NULL){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';

        foreach($colors as $key => $color){
            if(strlen($color)<=0 && $always_color == 'yes'){
                $colors[$key] = 'unicolor';

            }else if(strlen($color)<=0){
                unset($color,$colors);
            }
        }
        foreach($sizes as $key => $size){
            if(strlen($size)<=0 && $always_size == 'yes'){
                $sizes[$key] = 'unicolor';
            }else if(strlen($size)<=0){
                unset($size,$sizes);
            }
        }

        if(count($colors)<=0)$colors = NULL;
        if(count($sizes)<=0)$sizes = NULL;


        $color_attribute = self::colorAttribute();
        $size_attribute = self::sizeAttribute();
        if ( function_exists( 'wc_bv_update_post_meta' ) ) {
            wc_bv_update_post_meta( $ProductId, '_bv_type', 'matrix' );
        }

        if(count($colors) && $color_attribute != 'no_attribute'){
            wp_set_object_terms($ProductId, $colors, $color_attribute);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $ProductId, '_bv_x', $color_attribute );
            }
        }

        if(count($sizes) && $size_attribute != 'no_attribute'){
            wp_set_object_terms($ProductId, $sizes, $size_attribute);
            if ( function_exists( 'wc_bv_update_post_meta' ) ) {
                wc_bv_update_post_meta( $ProductId, '_bv_y', $size_attribute );
            }
        }

        if(count($colors) || count($sizes)){
            $attributes = array();
            if(count($colors) && $color_attribute != 'no_attribute'){
                $attributes[$color_attribute] = array(
                    'name' => $color_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 1
                );
            }

            if(count($sizes) && $size_attribute != 'no_attribute'){
                $attributes[$size_attribute] = array(
                    'name' => $size_attribute,
                    'value' => '',
                    'is_visible' => 1,
                    'is_variation' => 1,
                    'is_taxonomy' => 1,
                    'position'  => 2
                );

            }
            update_post_meta($ProductId, '_product_attributes', $attributes);

        }
    }

    public static function getNoColorText(){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        /* no color text */
        $noColorTextOption = 'Ingen fargevalg';
        if(isset($productTabOptions['m360_pck_noColorText'])){
            if(strlen($productTabOptions['m360_pck_noColorText']) >0){
                $noColorTextOption = $productTabOptions['m360_pck_noColorText'];
            }
        }
        return $noColorTextOption;
    }

    public static function getNoSizeText(){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        /* no size text */
        $noSizeTextOption =	'Ingen';
        if(isset($productTabOptions['m360_pck_noColorText'])){
            if(strlen($productTabOptions['m360_pck_noColorText']) >0){
                $noSizeTextOption = $productTabOptions['m360_pck_noSizeText'];
            }
        }
        return $noSizeTextOption;
    }

    private static function createVariants($ProductId,$sizeColors,$Product){
        $colors = array();
        $sizes = array();
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
        $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';


        foreach ($sizeColors as $sizeColor){
            if(strlen($sizeColor->color->name)>0 &&
                strpos($sizeColor->color->name, self::getNoColorText()) === false){
                if(!in_array(trim($sizeColor->color->name),$colors))$colors[] = trim($sizeColor->color->name);
            }else if($always_color == 'yes'){
                $colors[] = 'unicolor';
            }

            if(strlen($sizeColor->size->name)>0 &&
                strpos($sizeColor->size->name, self::getNoSizeText()) === false){
                if(!in_array(trim($sizeColor->size->name),$sizes))$sizes[] = trim($sizeColor->size->name);
            }else if($always_size == 'yes'){
                $sizes[] = 'unisize';
            }
        }

        global $current_PCK;
        if(m360_pck_is_default_pck($current_PCK)){
            self::createAttribute($ProductId,$colors,$sizes);
        }

        $WC_product = wc_get_product($ProductId);

        if (!$WC_product->is_type( 'variable' ) ) {
            wp_set_object_terms($ProductId, 'variable', 'product_type');
        }

        foreach ($sizeColors as $sizeColor) {
            self::createVariableProduct($sizeColor,$ProductId,$Product);
        }


    }

    public static function InsertProduct($Product){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK)){
            return $Product->articleNo.' finnes ikke i nettbutikken , og kassen er ikke den hoved kasse';
        }
        try {
            $ProductId = self::getNewProductId($Product);
            if(m360_pck_is_default_pck($current_PCK)){
                M360PCKMissingAttributes::AddM360PCKAttributes($Product,$ProductId);
                self::updatePostMetaForProduct($ProductId,$Product);
            }else{
                self::handling_stock($ProductId,$Product);
            }

            if ($Product->sizeColorInUse) {
                $sizeColors = $Product->sizeColors;
                if(is_object($sizeColors)) {
                    $sizeColors = array($sizeColors);
                }
                self::createVariants($ProductId,$sizeColors,$Product);
            }

            if(m360_pck_is_default_pck($current_PCK)){
                $tax_class = self::handelTaxClass($Product);
                $wc_product = wc_get_product($ProductId);
                $wc_product->set_tax_class($tax_class);
                $wc_product->set_tax_status(self::isTaxtable());
                $wc_product->save();

                self::addManfacturer($ProductId,$Product);

                wc_delete_product_transients( $ProductId );
            }



            self::purge_product_varnish($ProductId);

            if(n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')) {
                self::updateMultiStore($ProductId,$Product);
            }
            return $ProductId;
        } catch (Exception $e) {
            return "Error inserting product: ". $e->getMessage();
        }
    }

    public static function updateProductDescription($ProductId,$Product){
        $content_post = get_post($ProductId);
        $content = (is_object($content_post) && property_exists($content_post,'post_content'))?$content_post->post_content:'';
        $excerpt = (is_object($content_post) && property_exists($content_post,'post_excerpt'))?$content_post->post_excerpt:'';

        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_description'] )?$productTabOptions[ 'm360_pck_description']:'both_if_empty';

        $ProductStub = array(
            'ID' => $ProductId,
            'post_title' => $Product->name,
            'post_type' => 'product'
        );
        if($saved == 'long_description'){
            $ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description'){
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both'){
            $ProductStub['post_content'] = $Product->description;
            $ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'long_description_if_empty'){
            if(strlen($content)<=0)$ProductStub['post_content'] = $Product->description;
        }else if($saved == 'short_description_if_empty'){
            if(strlen($excerpt)<=0)$ProductStub['post_excerpt'] = $Product->description;
        }else if($saved == 'both_if_empty'){
            if(strlen($content)<=0)$ProductStub['post_content'] = $Product->description;
            if(strlen($excerpt)<=0)$ProductStub['post_excerpt'] = $Product->description;
        }
        return wp_update_post($ProductStub);

    }

    private static function getExistingProductId($Product){
        $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($Product->articleId);
        $newProductID = self::updateProductDescription($ProductId,$Product);
        return $newProductID;
    }
    public static function getOldManufacturer($ProductId){
        global $wpdb;
        $table_name = m360_pck_get_table_name("postmeta");

        return $wpdb->get_var($wpdb->prepare(
            "SELECT meta_value FROM " . $table_name . " WHERE meta_key = 'm360_pck_manufacturer' AND post_id = '%s' LIMIT 1", $ProductId
        ));
    }

    public static function getDiscountPrice($Product){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $saved = isset($productTabOptions[ 'm360_pck_tilbud_pris'] )?$productTabOptions[ 'm360_pck_tilbud_pris']:'discount';

        switch ($saved) {
            case "discount":
                $discount_price = round($Product->discount, 2);
                break;
            case "suggestedPrice":
                $discount_price = round($Product->suggestedPrice, 2);
                break;
            case "alternativePrice":
                $discount_price = round($Product->alternativePrice, 2);
                break;
            case "alternativePrice2":
                $discount_price = round($Product->alternativePrice2, 2);
                break;
            default:
                $discount_price = null;
        }
        return $discount_price;
    }
    public static function CheckThePrice($WC_product,$ProductId,$Product){

        $old_price = (method_exists($WC_product,'get_price'))?$WC_product->get_price():get_post_meta( $ProductId, '_price', true);
        $old_sale_price = (method_exists($WC_product,'get_sale_price'))?$WC_product->get_sale_price():get_post_meta( $ProductId, '_sale_price', true);
        $old_regular_price = (method_exists($WC_product,'get_regular_price'))?$WC_product->get_regular_price():get_post_meta( $ProductId, '_regular_price', true);

        $old_sale_price_dates_from 	= get_post_meta( $ProductId, '_sale_price_dates_from', true );
        $old_sale_price_dates_to 	= get_post_meta( $ProductId, '_sale_price_dates_to', true ) ;

        $date_from = date('Y-m-d H:i:s', $Product->discountFrom/1000);
        $date_to = date('Y-m-d H:i:s', $Product->discountTo/1000);

        $discount_price = self::getDiscountPrice($Product);

        if($old_price != round($Product->salesPrice, 2) ||
            $old_sale_price != $discount_price ||
            $old_regular_price != round($Product->salesPrice, 2)||
            $old_sale_price_dates_from != $date_from ||
            $old_sale_price_dates_to != $date_to){
            $woocommerce_synk_pris_checkbox = empty(get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true ))?'yes':get_post_meta( $ProductId, '_m360_checkbox_synk_pris', true );

            if($woocommerce_synk_pris_checkbox == 'yes') {
                self::pricing($ProductId, $Product);
            }
        }
    }

    public static function addExternlink($ProductId,$Product){
        if(!empty($Product->externalLink)){
            update_post_meta($ProductId, 'm360_pck_eksternlink', str_replace(',', '|', $Product->externalLink));
        }else{
            delete_post_meta($ProductId, 'm360_pck_eksternlink');
        }

        $productTabOptions = get_option( 'm360_pck_options_product' );
        if (array_key_exists('m360_pck_eksterlink_fields', $productTabOptions)) {
            if(isset($productTabOptions[ 'm360_pck_eksterlink_fields' ])&& $productTabOptions[ 'm360_pck_eksterlink_fields' ]!= 'no_attribute'){
                $savedAttribute = $productTabOptions[ 'm360_pck_eksterlink_fields' ];
                $attributes = get_post_meta( $ProductId, '_product_attributes',true );

                if(isset($Product->externalLink) && strlen($Product->externalLink)>0){
                    $eksternlinks = explode(',', $Product->externalLink);
                    wp_add_object_terms($ProductId, $eksternlinks, $savedAttribute);

                    $attributes[$savedAttribute] = array(
                        'name' => $savedAttribute,
                        'value' => implode('|', $eksternlinks),
                        'is_visible' => 1,
                        'is_variation' => 1,
                        'is_taxonomy' => 1,
                        'position'  => 5
                    );

                }
                update_post_meta($ProductId, '_product_attributes', $attributes);

            }
        }
    }
    public static function addSubtitle($ProductId,$Product){
        if(!empty($Product->subtitle)){
            update_post_meta($ProductId, 'm360_pck_subtitle', str_replace(',', '|', $Product->subtitle));
        }else{
            delete_post_meta($ProductId, 'm360_pck_subtitle');
        }

        $productTabOptions = get_option( 'm360_pck_options_product' );
        if (array_key_exists('m360_pck_undernavn_fields', $productTabOptions)) {
            if(isset($productTabOptions[ 'm360_pck_undernavn_fields' ])&& $productTabOptions[ 'm360_pck_undernavn_fields' ]!= 'no_attribute'){
                $savedAttribute = $productTabOptions[ 'm360_pck_undernavn_fields' ];
                $attributes = get_post_meta( $ProductId, '_product_attributes',true);

                if(isset($Product->subtitle) && strlen($Product->subtitle)>0 ){
                    $subtitles = explode(',', $Product->subtitle);
                    wp_add_object_terms($ProductId, $subtitles, $savedAttribute);

                    $attributes[$savedAttribute] = array(
                        'name' => $savedAttribute,
                        'value' => implode('|', $subtitles),
                        'is_visible' => 1,
                        'is_taxonomy' => 1,
                        'position'  => 4
                    );

                }
                update_post_meta($ProductId, '_product_attributes', $attributes);
            }
        }
        self::addExternlink($ProductId,$Product);
    }
    public static function addManfacturer($ProductId,$Product){
        $manufacturer = get_post_meta( $ProductId, 'm360_pck_manufacturer',true );// Produsent
        if(!empty($Product->manufacturer->name)){
            wp_set_object_terms($ProductId, array($Product->manufacturer->name), 'product_tag');
            $old_manufacturer_name = (isset($manufacturer))?$manufacturer:'';
            $manufacturer_name = (string)$Product->manufacturer->name;

            if($old_manufacturer_name !== $manufacturer_name){
                update_post_meta($ProductId, 'm360_pck_manufacturer', $manufacturer_name);
            }
            $productTabOptions = get_option( 'm360_pck_options_product' );
            if(isset($productTabOptions[ 'm360_pck_produsent_attribute' ] )){
                $man_att = $productTabOptions[ 'm360_pck_produsent_attribute' ];
                if($productTabOptions[ 'm360_pck_produsent_attribute' ]!='no_attribute'){
                    wp_set_object_terms($ProductId, $manufacturer_name, $man_att);
                    $attributes = get_post_meta( $ProductId, '_product_attributes',true );
                    $attributes[$man_att] = array(
                        'name' => $man_att,
                        'value' => '',
                        'is_visible' => 1,
                        'is_taxonomy' => 1,
                        'position'  => 3
                    );
                    update_post_meta($ProductId, '_product_attributes', $attributes);

                    $man_attribute_term = get_term_by('name', $manufacturer_name, $man_att);
                    if($man_attribute_term->slug)update_post_meta($ProductId, 'attribute_'.$man_att, $man_attribute_term->slug);
                }
            }
        }
        self::addSubtitle($ProductId,$Product);
    }

    public static function cleanVariations($Product){
        $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($Product->articleId);

        $args = array(
            'post_type'     => 'product_variation',
            'post_status'   => array( 'private', 'publish' ),
            'numberposts'   => -1,
            'post_parent'   => $ProductId
        );
        $variations = get_posts( $args );

        $kasse_nr = m360_pck_kassenr();

        $sizeColors = $Product->sizeColors;
        if(is_object($sizeColors)) {
            $sizeColors = array($sizeColors);
        }
        $array_of_sizeColorsIds = array();
        foreach($sizeColors as $sizeColor){
            $array_of_sizeColorsIds[] = $sizeColor->sizeColorId;
        }
        foreach($variations as $variation){
            $attID = getVariationIdFromSizeColorId($variation->ID,$kasse_nr);

            if(!in_array($attID,$array_of_sizeColorsIds)){
                wp_delete_post($variation->ID, true);
            }
        }
    }

    private static function DeleteChildFromSizeColorIdsTable($post_id){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_sizeColorIds");
        $query = "DELETE FROM {$table_name} WHERE post_id = {$post_id}";
        $wpdb->query($query);
    }


    public static function changeProductType($wc_product,$product_id,$Product){

        $using_frgstr = ($Product->sizeColorInUse)?true:false;
        $type = $wc_product->get_type();
        if($using_frgstr && is_object($wc_product) && $type != 'variable'){
            $type = 'variable';
        }

        if(!$using_frgstr && is_object($wc_product) && $type != 'simple'){
            if( $wc_product->has_child() ){
                $childrens = $wc_product->get_children( );
                foreach($childrens as $child_id){
                    wp_delete_post($child_id, true);
                    self::DeleteChildFromSizeColorIdsTable($child_id);
                }
            }
            WC_Product_Variable::sync($product_id);
            $type = 'simple';
        }

        wp_set_object_terms($product_id, $type, 'product_type');
        $new_wc_product = wc_get_product($product_id);
        //WriteLog('new type: '.$new_wc_product->get_type());
        return $new_wc_product;
    }
    public static function UpdateProduct($Product){
        global $current_PCK;


        if(!m360_pck_is_default_pck($current_PCK)){
            $ProductId = SKUTOPOSTID($Product->articleNo);
        }else{
            $ProductId = self::getExistingProductId($Product);
        }

        if($ProductId<=0){
            $ProductId = wc_get_product_id_by_sku($Product->articleNo);
        }

        $WC_product = wc_get_product( $ProductId );
        if(!$Product->visibleOnWeb ){
            if($WC_product)return $WC_product->delete(true);
            else return true;
        }
        if(!$WC_product){
            self::InsertProduct($Product);
            return true;
        }
        $WC_product = self::changeProductType($WC_product,$ProductId,$Product);

        try {
            if(m360_pck_is_default_pck($current_PCK)){
                self::CheckThePrice($WC_product,$ProductId,$Product);
                M360PCKMissingAttributes::AddM360PCKAttributes($Product,$ProductId);
                $feature = ($Product->recommendedProduct)?'yes':'no';
                //update_post_meta($ProductId, '_featured', $feature);

                if(method_exists($WC_product,'set_featured')){
                    $WC_product->set_featured($feature);
                    $WC_product->save();
                }else{
                    update_post_meta($ProductId, '_featured', $feature);
                }


                self::CheckTheGroups($ProductId,$Product);
                self::updatePostMetaForProduct($ProductId,$Product);
            }

            self::handling_stock($ProductId,$Product);

            if ($Product->sizeColorInUse){
                $sizeColors = $Product->sizeColors;
                if(is_object($sizeColors)) {
                    $sizeColors = array($sizeColors);
                }

                $missingColors = array();
                $missingSizes = array();
                $missingColorsSizes = array();
                $productTabOptions = get_option( 'm360_pck_options_product' );
                $always_color = (isset($productTabOptions[ 'm360_pck_always_create_color_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_color_attribute']:'no';
                $always_size = (isset($productTabOptions[ 'm360_pck_always_create_size_attribute'] ))?$productTabOptions[ 'm360_pck_always_create_size_attribute']:'no';


                foreach($sizeColors as $sizeColor){

                    $colorTitle = trim($sizeColor->color->name);
                    $sizeTitle = trim($sizeColor->size->name);
                    if(strlen($colorTitle)>0  && strpos($colorTitle, self::getNoColorText()) === false){
                        if(!in_array($colorTitle,$missingColors))$missingColors[] = $colorTitle;
                    }else if($always_color == 'yes'){
                        $missingColors[] = 'unicolor';
                    }

                    if(strlen($sizeTitle)>0  && strpos($sizeTitle, self::getNoSizeText()) === false){
                        if(!in_array($sizeTitle,$missingSizes))$missingSizes[] = $sizeTitle;
                    }else if($always_size == 'yes'){
                        $missingSizes[] = 'unisize';
                    }

                    if((strlen($sizeColor->color->name)>0  && strpos($sizeColor->color->name, self::getNoColorText()) === false)
                        ||(strlen($sizeColor->size->name)>0  && strpos($sizeColor->size->name, self::getNoSizeText()) === false)){
                        $missingColorsSizes[] = $sizeColor;
                    }else if($always_color == 'yes' || $always_size == 'yes'){
                        $missingColorsSizes[] = $sizeColor;
                    }
                }

                if(m360_pck_is_default_pck($current_PCK)){
                    self::createAttribute($ProductId,$missingColors,$missingSizes);
                }

                if(count($missingColorsSizes)){
                    foreach ($missingColorsSizes as $missingColorSize){
                        self::createVariableProduct($missingColorSize,$ProductId,$Product);
                    }
                }

            }

            if(m360_pck_is_default_pck($current_PCK)){
                self::addManfacturer($ProductId,$Product);
                if(n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')) {
                    self::updateMultiStore($ProductId,$Product);
                }
                wc_delete_product_transients( $ProductId );
            }

            self::handlingWarehousesOptions($ProductId);self::purge_product_varnish($ProductId);

            return true;

        } catch (Exception $ex) {
            return false;
        }
    }
    public static function createJPEGImage($picture,$name){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        $use_imagick = 'yes';
        if(isset($productTabOptions[ 'm360_pck_use_imagick'] )){
            $use_imagick = $productTabOptions[ 'm360_pck_use_imagick'];
        }
        $output_file = $name."_flatten.jpg";

        if ($use_imagick == 'yes'){
            $im = new Imagick();
            $im->readimageblob($picture);
            if($im->getImageAlphaChannel()){
                $im->setImageAlphaChannel(Imagick::VIRTUALPIXELMETHOD_WHITE);
                $im->setCompression(Imagick::COMPRESSION_JPEG);
            }
            if ($im->getImageColorspace() == Imagick::COLORSPACE_CMYK) {
                $profiles = $im->getImageProfiles('*', false);

                $has_icc_profile = (array_search('icc', $profiles) !== false);
                if ($has_icc_profile === false) {
                    $icc_cmyk = file_get_contents(dirname(__FILE__).'/USWebUncoated.icc');
                    $im->profileImage('icc', $icc_cmyk);
                    unset($icc_cmyk);
                }
                $icc_rgb = file_get_contents(dirname(__FILE__).'/sRGB_v4_ICC_preference.icc');
                $im->profileImage('icc', $icc_rgb);
                unset($icc_rgb);
                $im->setCompressionQuality(100);
                $im->setImageFormat("jpeg");
            }
        }
        array_map('unlink', glob($name."_flatten-*.*"));

        if ($use_imagick == 'yes'){
            $im->writeImage($output_file);
        }
        else{
            file_put_contents($output_file, $picture);
        }

        return $output_file;
    }


    public static function UpdateCompanyLogo($data){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        try {
            $Picture = $data->image;

            // get WordPress current image upload path
            $upload_dir = wp_upload_dir();
            $ImageDirectory = $upload_dir['path'] . '/';

            // save directory on the web server if required
            if (!is_dir($ImageDirectory))
                mkdir($ImageDirectory, 0777, true);

            // create image file name and save image byte array
            $FileName_uten_ext = $ImageDirectory . "pck_logo";
            $FileName = self::createJPEGImage($Picture,$FileName_uten_ext);

            if(file_exists($FileName)){
                $UploadFolder = $upload_dir['url'] . '/';
                $Attachment = array(
                    'guid'           => $UploadFolder . basename($FileName),
                    'post_mime_type' => 'image/jpeg',
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($FileName)),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );

                // insert product image and get WOOCOMM attachment id
                $AttachmentId = wp_insert_attachment($Attachment, $FileName);

                // generate the meta data for the attachment, and update the database record.
                $AttachData = wp_generate_attachment_metadata($AttachmentId, $FileName);

                // insert product image meta data
                wp_update_attachment_metadata($AttachmentId, $AttachData);
            }


            return true;
        } catch (Exception $e) {
            return false;
        }
    }



    public static function UpdateProductInfo($ProductArticleId, $Picture,$samling = false,$variant_parent = false,$imageId=false){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;
        try {

            $upload_dir = wp_upload_dir();
            $ImageDirectory = $upload_dir['path'] . '/';

            if (!is_dir($ImageDirectory))
                wp_mkdir_p($ImageDirectory);

            $ProductId = ($samling)?$ProductArticleId:self::GetWOOCOMMProductIdFromPCKArticleId($ProductArticleId);
            if(has_post_thumbnail( $ProductId )){
                $attachment_id = get_post_thumbnail_id( $ProductId );
                wp_delete_attachment($attachment_id, true);
            }
            $kasse_nr = m360_pck_kassenr();

            if(!$ProductId)$ProductId = getVariationIdFromSizeColorId($ProductArticleId,$kasse_nr);
            $FileName_uten_ext = ($variant_parent && $variant_parent >0)?$ImageDirectory . $ProductArticleId ."_".$variant_parent."_".$imageId:$ImageDirectory . $ProductArticleId;

            $FileName_uten_ext = $FileName_uten_ext.'_'.m360_pck_kassenr();
            $FileName = self::createJPEGImage($Picture,$FileName_uten_ext);

            $UploadFolder = $upload_dir['url'] . '/';
            $image_src = $UploadFolder . basename($FileName);


            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            $Attachment = array(
                'guid'           => $image_src,
                'post_mime_type' => 'image/jpeg',
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($FileName)),
                'post_content'   => '',
                'post_excerpt'   => '',
                'post_status'    => 'inherit'
            );

            $AttachmentId = wp_insert_attachment($Attachment, $FileName, $ProductId);

            if ($attach_data = wp_generate_attachment_metadata( $AttachmentId, $FileName)) {
                wp_update_attachment_metadata($AttachmentId, $attach_data);
            }


            $productTabOptions = get_option( 'm360_pck_options_product' );
            $saved = isset($productTabOptions[ 'm360_pck_alt_text_from_produkt_name'] )?$productTabOptions[ 'm360_pck_alt_text_from_produkt_name']:'yes_if_empty';

            $WC_product = wc_get_product( $ProductId );
            if($WC_product && $saved == 'yes'){
                update_post_meta($AttachmentId, '_wp_attachment_image_alt', $WC_product->get_title());
            }else if($saved == 'yes_if_empty'){
                $old_altext = get_post_meta( $AttachmentId, '_wp_attachment_image_alt',true);

                if($WC_product && empty($old_altext)){
                    update_post_meta($AttachmentId, '_wp_attachment_image_alt', $WC_product->get_title());
                }
            }
            update_post_meta($ProductId, '_thumbnail_id', $AttachmentId);
            wc_delete_product_transients( $ProductId );


            return true;
        } catch (Exception $e) {
            //WriteLog("Error uploading image: " . $e->getMessage() . "\n");
            return false;
        }
    }

    public static function removeProductImage($ProductArticleId){
        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        try {

            // get WOOCOMM product id from PCK SKU
            $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($ProductArticleId);
            if(has_post_thumbnail( $ProductId )){
                $attachment_id = get_post_thumbnail_id( $ProductId );
                wp_delete_attachment($attachment_id, true);
            }
            /*
            $args = array(
                'post_type'   => 'attachment',
                'numberposts' => -1,
                'post_status' => 'any',
                'post_parent' => $ProductId,
            );

            $attachments = get_posts( $args );

            if ( $attachments ) {
                foreach ( $attachments as $attachment ) {
                    wp_delete_attachment( $attachment->ID, true );
                }
            }
            */
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function calculateInStock($backorder,$qty){
        $instock = 'instock';

        if($backorder == 'no' && $qty<=0){
            $instock = 'outofstock';
        }

        return $instock;
    }

    public static function UpdateProductStockLevel($updateStock){
        global $current_PCK;
        $kasse_nr = self::retriveKasseNr();
        /* new extra check 07.06.2018*/
        if($updateStock->sizeColorId > 0){
            $ProductId = getVariationIdFromSizeColorId($updateStock->sizeColorId,$kasse_nr);
        }else{
            $ProductId = self::GetWOOCOMMProductIdFromPCKArticleId($updateStock->articleId);
        }
        /* extra check end */


        if($ProductId <= 0){
            return 'Vi finner ikke varen med pck id '.$updateStock->articleId.' i nettbutikken, sjekk hvis den synlig på web og aktiv';
        }else{
            $qty = self::calculate_multi_pckasser_count($ProductId,$updateStock->count);
            $backorder = self::calculateBackorder($ProductId);
            $instock = self::calculateInStock($backorder,$qty);
            $manageStock = 'yes';
            /*
            $to_log = array(27661,36869,36870,36871,36872,36873);
            if(in_array($ProductId,$to_log)){
                WriteLog(__FUNCTION__);
                WriteLog('qty: '.$qty.' for product id: '.$ProductId);
            }
            */
            if($qty<=0 && $backorder == 'yes'){
                $manageStock = 'no';
            }
            self::handlingStockDetails($ProductId,$updateStock);
            try{
                $wc_product = wc_get_product($ProductId);
                if($wc_product){
                    $wc_product->set_manage_stock($manageStock);
                    $wc_product->set_backorders($backorder);
                    $wc_product->set_stock_quantity($qty);
                    $wc_product->set_stock_status(wc_clean($instock));
                    $wc_product->save();
                }

                return true;
            }catch(Exception $ex){
                return 'UpdateProductStockLevel '.$ex->getMessage();
            }
        }
    }

    public static function DeleteProduct($product_info){
        // get the WOOCOMM product id by the PCK SKU
        $ProductId = self::GetWooCommProductIdFromPCKSKU($product_info->articleId);
        $WC_product = wc_get_product( $ProductId );
        self::purge_product_varnish($ProductId);
       self::purge_product_varnish($ProductId);return $WC_product->delete(true);
    }

    public static function UpdateExtraImages($articleId,$imageId, $Picture,$samling=false,$colorid = false){

        global $current_PCK;
        if(!m360_pck_is_default_pck($current_PCK))return true;

        if($colorid && $colorid >0){
            return self::UpdateProductInfo($colorid, $Picture,$samling,$articleId,$imageId);
        }
        try {
            $upload_dir = wp_upload_dir();
            $ImageDirectory = $upload_dir['path'] . '/';

            // save directory on the web server if required
            if (!is_dir($ImageDirectory))
                mkdir($ImageDirectory, 0777, true);

            // get WOOCOMM product id from PCK SKU
            $ProductId = ($samling)?$articleId:self::GetWOOCOMMProductIdFromPCKArticleId($articleId);
            $WC_product = wc_get_product( $ProductId );

            $FileName_uten_ext = $ImageDirectory . $articleId .'_'.$imageId;
            $FileName_uten_ext = $FileName_uten_ext.'_'.m360_pck_kassenr();
            $FileName = self::createJPEGImage($Picture,$FileName_uten_ext);

            global $wpdb;
            $image_src = $upload_dir['baseurl'] . '/' . _wp_relative_upload_path( $FileName );
            $AttachmentId = $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'");

            $UploadFolder = $upload_dir['url'] . '/';

            // create a WOOCOMM stub for product image / post attachment
            $Attachment = array(
                'guid'           => $UploadFolder . basename($FileName),
                'post_mime_type' => 'image/jpeg',
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($FileName)),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            if(!$AttachmentId){
                $AttachmentId = wp_insert_attachment($Attachment, $FileName, $ProductId);
            }

            if ($attach_data = wp_generate_attachment_metadata( $AttachmentId, $FileName)) {
                wp_update_attachment_metadata($AttachmentId, $attach_data);
            }


            $old_images = get_post_meta( $ProductId, '_product_image_gallery' );
            if(!in_array($AttachmentId,$old_images))array_push($old_images, $AttachmentId);

            $productTabOptions = get_option( 'm360_pck_options_product' );
            $saved = isset($productTabOptions[ 'm360_pck_alt_text_from_produkt_name'] )?$productTabOptions[ 'm360_pck_alt_text_from_produkt_name']:'yes_if_empty';

            if($saved == 'yes'){
                if($WC_product)update_post_meta($AttachmentId, '_wp_attachment_image_alt', $WC_product->get_title());
            }else if($saved == 'yes_if_empty'){
                $old_altext = get_post_meta( $AttachmentId, '_wp_attachment_image_alt',true);

                if(empty($old_altext)){
                    if($WC_product)update_post_meta($AttachmentId, '_wp_attachment_image_alt', $WC_product->get_title());
                }
            }

            update_post_meta( $ProductId, '_product_image_gallery', implode(',',$old_images));
            wc_delete_product_transients( $ProductId );

            return true;
        }
        catch (Exception $e) {
            M360PCKasseServices::setErrorMessage('rror uploading Extra image: '.$e->getMessage());
            return false;
        }
    }
}

