<?php
header('Content-Type: text/xml');

echo '<info>';
echo '<version>' . ADDWISH_VERSION . '</version>';
echo '<woocommerce_version>' . ADDWISH_WC_VERSION . '</woocommerce_version>';
echo '<wordpress_version>' . ADDWISH_WP_VERSION . '</wordpress_version>';
echo '</info>';
