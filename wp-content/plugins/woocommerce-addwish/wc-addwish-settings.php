<?php
class WC_Settings_Addwish_Plugin extends WC_Settings_Page {
    /**
     * Constructor
     */
    public function __construct() {
        $this->id = 'addwish';
        add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 50 );
        add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );
        add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
        add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
    }

    /**
     * Add plugin options tab
     *
     * @return array
     */
    public function add_settings_tab( $settings_tabs ) {
        $settings_tabs[$this->id] = __( 'Addwish', 'woocommerce-addwish-settings-tab' );
        return $settings_tabs;
    }

    /**
     * Get sections
     *
     * @return array
     */
    public function get_sections() {
        $sections = array(
            'section-0' => __( 'Plugin Options', 'woocommerce-addwish-settings-tab' )
        );
        return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
    }

    /**
     * Get sections
     *
     * @return array
     */
    public function get_settings() {
                $settings = array(
                    'section_title' => array(
                        'name'     => __( 'Settings', 'woocommerce-addwish-settings-tab' ),
                        'type'     => 'title',
                        'desc'     => '',
                        'id'       => 'wc_addwish-settings_tab_title_section-1'
                    ),
                    'title' => array(
                        'name' => __( 'Addwish partner ID', 'woocommerce-addwish-settings-tab' ),
                        'desc_tip' => __( 'This is your unique addwish partner ID', 'woocommerce-addwish-settings-tab' ),
                        'type' => 'text',
                        'desc' => __( 'You personal addwish partner ID.', 'woocommerce-addwish-settings-tab' ),
                        'id'   => 'wc_addwish_partner_id'
                    ),
                    'section_end' => array(
                         'type' => 'sectionend',
                         'id' => 'wc_addwish-settings_tabend-section-1'
                    ),
                    'section_title_2' => array(
                        'name'     => __( 'Product feed', 'woocommerce-addwish-settings-tab' ),
                        'type'     => 'title',
                        'desc'     => 'Your product feed: <code>'.get_home_url().'/?feed=addwish_feed</code>',
                        'id'       => 'wc_addwish-settings_tab_title_section-2'
                    ),
                     'section_end_2' => array(
                         'type' => 'sectionend',
                         'id' => 'wc_addwish-settings_tabend-section-2'
                    )
                );
        return apply_filters( 'wc_addwish_settings_tab_settings', $settings, $section );
    }

    /**
     * Output the settings
     */
    public function output() {
        global $current_section;
        $settings = $this->get_settings( $current_section );
        WC_Admin_Settings::output_fields( $settings );
    }

    /**
     * Save settings
     */
    public function save() {
        global $current_section;
        $settings = $this->get_settings( $current_section );
        WC_Admin_Settings::save_fields( $settings );
    }
}
return new WC_Settings_Addwish_Plugin();

