<?php
header( 'Content-Type: text/xml' );

function addwish_xml_tag( $tag, $value, $wrapInCdata = false ) {
	if ( is_bool( $value ) ) {
		$value = $value ? 'true' : 'false';
	}

	// Replace a few special characters that can't exist in xml, even inside CDATA:
	// All of them are replaced with space
	$value = str_replace(
		array(
			// The C0 control block, except a few specifically allowed characters.
			"\x00", "\x01", "\x02", "\x03", "\x04", "\x05", "\x06", "\x07",
			"\x08", "\x0B", "\x0C", "\x0E", "\x0F", "\x10", "\x11", "\x12",
			"\x13", "\x14", "\x15", "\x16", "\x17", "\x18", "\x19", "\x1A",
			"\x1B", "\x1C", "\x1D", "\x1E", "\x1F",
		),
		" ",
		$value);

	if ( $wrapInCdata ) {
		return "<$tag><![CDATA[" . $value . "]]></$tag>";
	}

	return "<$tag>" . $value . "</$tag>";
}

// Implementation of get_term_parents_list for versions of wordpress that misses it.
function addwish_get_term_parents_list_legacy( $id, $taxonomy, $options = array() ) {
	$chain  = '';
	$parent = get_term( $id, $taxonomy );
	if ( array_key_exists( "visited", $options ) ) {
		$visited = $options['visited'];
	} else {
		$visited = array();
	}

	if ( is_wp_error( $parent ) ) {
		return $parent;
	}

	$name = $parent->name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && ! in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		$chain     .= addwish_get_term_parents_list_legacy( $parent->parent, $taxonomy, array(
			'visited'   => $visited,
			'separator' => $options['separator']
		) );
	}

	$chain .= $name . $options['separator'];

	return $chain;
}

// Printing out categories
function addwish_get_categories( $id ) {
	$result = '<hierarchies>';
	$cats   = get_the_terms( $id, 'product_cat' );

	if ( version_compare( ADDWISH_WP_VERSION, "4.8", ">=" ) ) {
		$cat_fn = get_term_parents_list;
	} else {
		$cat_fn = addwish_get_term_parents_list_legacy;
	}

	foreach ( $cats as $cat ) {
		$result .= '<hierarchy>';

		$all_cats = explode( "\0", rtrim( $cat_fn( $cat->term_taxonomy_id, 'product_cat', array(
			'separator' => "\0",
			'link'      => false,
			'format'    => 'name'
		) ), "\0" ) );

		foreach ( $all_cats as $key => $value ) {
			$result .= addwish_xml_tag( 'category', $value );
		}

		$result .= '</hierarchy>';
	}
	$result .= '</hierarchies>';

	return $result;
}

echo '<products version="' . ADDWISH_VERSION . '">';
$args = array( 'post_type' => 'product', 'posts_per_page' => - 1 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
	$_product     = wc_get_product( get_the_ID() );
	$image        = false;
	$thumb        = false;
	$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
	if ( $thumbnail_id ) {
		$image = wp_get_attachment_image_src( $thumbnail_id, 'single-post-thumbnail' );
		if ( ! empty( $image ) ) {
			$image = $image[0];
		}
		$thumb = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail' );
		if ( ! empty( $thumb ) ) {
			$thumb = $thumb[0];
		}
	} else {
		$image = $thumb = wc_placeholder_img_src();
	}

	echo '<product>';
	echo addwish_xml_tag( "title", get_the_title() );
	echo addwish_xml_tag( "url", get_permalink() );
	echo addwish_xml_tag( "productnumber", get_the_ID() );
	$price     = number_format( $_product->get_price(), 2, '.', '' );
	$old_price = $_product->get_regular_price();
	if ( $old_price !== "" ) {
		$old_price = number_format( $old_price, 2, '.', '' );
	}
	echo addwish_xml_tag( "price", $price );
	if ( $old_price === "" ) {
		$old_price = $price;
	}
	echo addwish_xml_tag( "oldPrice", $old_price );
	echo addwish_xml_tag( "instock", $_product->is_in_stock() );
	echo addwish_xml_tag( "description", $_product->get_description(), true );
	if ( $image ) {
		echo addwish_xml_tag( "imgurl", $image );
	}
	if ( $thumb ) {
		echo addwish_xml_tag( "thumbnail", $thumb );
	}
	echo addwish_xml_tag( "keywords", strip_tags( wc_get_product_tag_list( $_product->get_id(), " " ) ), true );
	echo addwish_get_categories( get_the_ID() );
	echo '</product>';
endwhile;
echo '</products>';


