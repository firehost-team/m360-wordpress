=== addwish ===
Contributors: addwish
Tags: addwish, recommendations, search, personalisation, addwish business, wishlist, wish list
Stable tag: 1.1.3
WC requires at least: 3.0.0
WC tested up to: 3.2.5
Requires at least: 4.4.1
Tested up to: 4.9.1

Official addwish business plugin for WooCommerce. Personalise the shopping experience and increase basket size and conversion rate in your webshop.

== Description ==

This is the official addwish business solution plugin for the WooCommerce e-commerce platform.

The addwish business solution will personalise the shopping experience for your visitors as well as increase your basket size and conversion rate across your webshop.

This plugin will:

*	Add the addwish business partner script to your WooCommerce webshop.
*	Add the addwish business conversion tracking to your WooCommerce webshop.
*	Add the addwish business product feed, for addwish business to index your product catalogue.

== Screenshots ==

1. Simple confirguration interface, to enable your addwish business account in WooCommerce.

== Installation ==

1. Upload the entire 'woocommerce-addwish' folder to the '/wp-content/plugins/' directory or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' menu in WordPress.

== Changelog ==

= 1.0.4 =
* Encoding fix
* supporting oldPrice

= 1.0.3 =
* Cart span added

= 1.0.2 =
* Fixed feed limit

= 1.0.1 =
* Info endpoint added

= 1.0.0 =
* Initial Release

== Upgrade Notice ==

= 1.0.3 =
Please upgrade, to ensure all your cart are synced with the addwish business solution.

= 1.0.2 =
Please upgrade, to ensure all your products are synced with the addwish business solution.
