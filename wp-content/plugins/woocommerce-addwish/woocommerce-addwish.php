<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
// Also change it below.
define( 'ADDWISH_VERSION', '1.1.3' );

/**
 * Plugin Name: addwish business integration
 * Description: addwish plugin to ease the integration with WooCommerce.
 * Author: addwish
 * Author URI: http://business.addwish.com/
 * Version: 1.1.3
 * WC requires at least: 3.0.0
 * WC tested up to: 3.2.5
 * Requires at least: 4.4.1
 * Tested up to: 4.9.1
 *
 * Copyright: (c) 2017 addwish
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	// Store the WooCommerce version. We need it for checks later...
	// https://wpbackoffice.com/get-current-woocommerce-version-number/

	// If get_plugins() isn't available, require it
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_folder = get_plugins( '/' . 'woocommerce' );
	$plugin_file   = 'woocommerce.php';

	// If the plugin version number is set, return it
	if ( isset( $plugin_folder[ $plugin_file ]['Version'] ) ) {
		define( 'ADDWISH_WC_VERSION', $plugin_folder[ $plugin_file ]['Version'] );
	} else {
		define( 'ADDWISH_WC_VERSION', 'unknown' );
	}

	include( ABSPATH . 'wp-includes/version.php' );
	define( 'ADDWISH_WP_VERSION', $wp_version );

	/**
	 * Adding the addwish JavaScript
	 **/
	function add_addwish() {
		?>
		<script type="text/javascript">
			(function () {
				var aws = document.createElement('script');
				aws.type = 'text/javascript';
				if (typeof(aws.async) != "undefined") {
					aws.async = true;
				}
				aws.src = (window.location.protocol == 'https:' ? 'https://d1pna5l3xsntoj.cloudfront.net' : 'http://cdn.addwish.com') + '/scripts/company/awAddGift.js#<?php echo get_option( 'wc_addwish_partner_id' )?>';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(aws, s);
			})();
		</script>
		<?php
		global $woocommerce;

		echo '<span class="addwish-cart" style="display: none;" data-total="' . $woocommerce->cart->cart_contents_total . '" data-email="' . wp_get_current_user()->user_email . '">';
		$items = $woocommerce->cart->get_cart();
		foreach ( $items as $item => $values ) {
			echo '<span class="addwish-product" data-url="' . get_permalink( $values['product_id'] ) . '" data-productnumber="' . $values['product_id'] . '"></span>';
		}
		echo '</span>';
	}

	add_action( 'wp_footer', 'add_addwish' );

	add_filter( 'woocommerce_get_settings_pages', 'wc_addwish_add_settings_page' );
	function wc_addwish_add_settings_page( $settings ) {
		$settings[] = include( 'wc-addwish-settings.php' );

		return $settings;
	}

	/**
	 * Adding the addwish product feed
	 **/
	function addwish_feed_init() {
		add_feed( 'addwish_feed', 'addwish_product_feed' );
		// Will make the feed abailable on domain.com/?feed=my_custom_feed or domain.com/feed/my_custom_feed
	}

	add_action( 'init', 'addwish_feed_init' );

	function addwish_product_feed() {
		include( 'wc-addwish-feed.php' );
	}

	/**
	 * Adding the addwish info endpoint
	 **/
	function addwish_info_init() {
		add_feed( 'addwish_info', 'addwish_info_endpoint' );
		// Will make the info endpoint abailable on domain.com/?feed=my_custom_feed or domain.com/feed/my_custom_feed
	}

	add_action( 'init', 'addwish_info_init' );

	function addwish_info_endpoint() {
		include( 'wc-addwish-info.php' );
	}

	/**
	 * Adding the addwish conversion tracking (span)
	 **/
	function addwish_conversion_tracking( $order_id ) {
		// Get order from the id
		$order = new WC_Order( $order_id );
		// Print the wrapping span
		echo '<span class="addwish-conversion" style="display:none;"';
		echo ' data-ordernumber="' . $order_id . '"';
		echo ' data-total="' . $order->get_total() . '"';
		echo ' data-email="' . $order->billing_email . '">';
		// Get all items purchased
		$order_items = $order->get_items();
		foreach ( $order_items as $item ) {
			// Print all order items
			echo '<span class="addwish-product"';
			echo ' data-quantity="' . $item['qty'] . '"';
			echo ' data-productnumber="' . $item['product_id'] . '">';
			echo '</span>';
		}
		echo '</span>';
	}

	add_action( 'woocommerce_thankyou', 'addwish_conversion_tracking' );

	if ( ! class_exists( 'WC_Addwish' ) ) {
		/**
		 * Localisation
		 **/
		load_plugin_textdomain( 'wc_addwish', false, dirname( plugin_basename( __FILE__ ) ) . '/' );

		class WC_Addwish {
			public function __construct() {
			}
		}

		// instantiate our plugin class and add it to the set of globals
		$GLOBALS['wc_addwish'] = new WC_Addwish();
	}
}
