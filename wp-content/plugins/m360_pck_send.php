<?php

include_once(dirname(__FILE__) . '/m360_pck_functions.php'); 

class M360PCKSender {
	public function __construct(){
		
	}
	
	private static function getPCKArticleIDFromSKU($articleId,$articleNo=NULL,$nettbutikk=0){
        global $wpdb;
        global $current_PCK;
        if($nettbutikk>0)switch_to_blog( $nettbutikk );

        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $PCK_articleID = get_post_meta( $articleId, 'm360_pck_article_id_'.$kasse_nr,true );

        if(is_numeric($PCK_articleID)){
            if($nettbutikk>0)switch_to_blog( $nettbutikk );
            return $PCK_articleID;

        }
        /* ekstra 6.42 */
        $PCK_articleID = get_post_meta( $articleId, 'm360_pck_article_id',true );

        if(is_numeric($PCK_articleID)){
            if($nettbutikk>0)switch_to_blog( $nettbutikk );
            return $PCK_articleID;
        }


        $table_name = m360_pck_get_table_name("postmeta");
        $qur = "SELECT meta_value FROM " . $table_name . " WHERE meta_key = 'm360_pck_article_id_{$kasse_nr}' AND post_id = $articleId LIMIT 1";
        $ProductId = $wpdb->get_var($qur);
        $isExist = false;

        if(is_numeric($ProductId)){$isExist = true;}

        if(!$isExist && $articleNo){
            $qur2 = "SELECT post_id FROM " . $table_name . " WHERE meta_key = '_sku' AND meta_value = '".$articleNo."' LIMIT 1";
            $ProductId = $wpdb->get_var($qur2);
            if(is_numeric($ProductId)){
                add_post_meta($ProductId, 'm360_pck_article_id_'.$kasse_nr, $articleId, true);
            }
        }

        return $ProductId;
    }
	
	private static function getSKUFromProductID($product_id){
		global $wpdb;
		// get WOOCOMM product id from PCK SKU
		return $wpdb->get_var($wpdb->prepare(
			"SELECT meta_value FROM " . $wpdb->postmeta . " WHERE meta_key = '_sku' AND post_id = '%s' LIMIT 1", $product_id
		));
	}
	
	public function get_billing_address($order){
		$billing_address = array(
								'first_name' => $order->get_billing_first_name(),
								'last_name'  => $order->get_billing_last_name(),
								'company'    => $order->get_billing_company(),
								'address_1'  => $order->get_billing_address_1(),
								'address_2'  => $order->get_billing_address_2(),
								'city'       => $order->get_billing_city(),
								'postcode'   => $order->get_billing_postcode(),
								'country'    => $order->get_billing_country(),
								'email'      => $order->get_billing_email(),
								'phone'      => $order->get_billing_phone(),
							  );
		return $billing_address;
	}
	
	public function get_shipping_address($order){
		$shipping_address = array(
								'first_name' => $order->get_shipping_first_name(),
								'last_name'  => $order->get_shipping_last_name(),
								'company'    => $order->get_shipping_company(),
								'address_1'  => $order->get_shipping_address_1(),
								'address_2'  => $order->get_shipping_address_2(),
								'city'       => $order->get_shipping_city(),
								'postcode'   => $order->get_shipping_postcode(),
								'country'    => $order->get_shipping_country(),
							  );
		return $shipping_address;
		
	}
	
	public function getSizeColorIdFromVariationId($varID,$nettbutikk_id){
        global $wpdb;
        global $current_PCK;

        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        //WriteLog(__FUNCTION__.' kasse. '.$kasse_nr.' varID: '.$varID);

        $table_name = m360_pck_get_table_name("postmeta");
        //echo '<h3>'.$table_name.'</h3>';

        $key = '_sizeColorId_'.$kasse_nr;

        $qur = "SELECT meta_value FROM $table_name WHERE meta_key = '{$key}' AND post_id = {$varID}";
        $sizeColorId = $wpdb->get_var($qur);
        //echo '<h3>sizeColorId: '.$sizeColorId.'</h3>';

        if(!$sizeColorId){
            $qur_2 = "SELECT meta_value FROM $table_name WHERE meta_key = '_sizeColorId' AND post_id = {$varID}";
            $sizeColorId = $wpdb->get_var($qur_2);
        }
        //echo '<h3>sizeColorId: '.$sizeColorId.'</h3>';

        //WriteLog('sizeColorID: '.$sizeColorId.' qur: '.$qur);

        return (integer)$sizeColorId;
	}
	
	public function tmpC(){
		global $woocommerce;
		global $wpdb;
		$order = wc_get_order(3192);
		$OrderDetails = $order->get_items();
		
		foreach ($OrderDetails as $key => $Product) {
			//WriteLog('order 3129 '.print_r($Product,true));
			$customFieldPrice = 0;
			if(!empty($Product['wccf'])){
				$fields = unserialize($Product['wccf']);
				$customFieldMessage = '';
				foreach ($fields as $field){
					$customFieldPrice += $field['pricing']['value'];
				}
			}
			$price = (($Product['line_subtotal']+$Product['line_subtotal_tax'])/ ($Product['qty'] == 0 ? 1 : $Product['qty']))-$customFieldPrice;
			//WriteLog('price: '.$price);
		}
	}


	public function getWarehouseIdFromOrderItem($Product){
	    $amount = $Product['qty'];
        $warehouseId = 0;
        $product_id = ($Product['variation_id']>0)?$Product['variation_id']:$Product['product_id'];
        //WriteLog('product_id : '.$product_id);

        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $stockDetails_v = get_post_meta($product_id,'stockDetails_'.$kasse_nr,true);
        //WriteLog('sd: '.print_r($stockDetails_v,true));

        $all_warehouses = getAllwarehouses();
        if(count($all_warehouses)>0){
            $found = false;
            foreach($all_warehouses as $warehouseA){
                foreach ($stockDetails_v as $stockDetail){
                    if($stockDetail->warehouseId == $warehouseA->warehouseId){
                        //WriteLog('stockDetail->warehouseId: '.$stockDetail->warehouseId.' '.print_r($stockDetail,true). ' amount: '.$amount);
                        if($stockDetail->count >= $amount){
                            $warehouseId = $stockDetail->warehouseId;
                            $found = true;
                        }
                    }
                }
                if($found)break;
            }
            if($warehouseId <= 0)$warehouseId = $all_warehouses[0]->warehouseId;
        }
        //WriteLog('warehouse id: '.$warehouseId);
        return $warehouseId;
    }
    private function checkOrderStatusTable(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateOrderStatusTables();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);

    }
    public function getOrderFromWC($update_order_status=true){
        $this->checkOrderStatusTable();
        //$this->tmpC();
        //WriteLog('getOrderFromWC');
        //global $woocommerce;

        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $sending_type = isset($ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ] )?$ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ]:'normal';
        $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');


        global $wpdb;
        $ordersArray = array();
        $totalAmount = 0.0;
        $totalQty = 0;

        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $allowd_status = $ordersTabOptions['m360_pck_order_status_ids'];
        //WriteLog('allowd_status: '.print_r($allowd_status,true));

        $kasse_nettbutikk_id = get_current_blog_id();
        $old_table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        $multiStorePlugin = n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')?true:false;
        if($multiStorePlugin){
            $qurr = "SELECT * FROM ".$table_name." WHERE status in ('new','credit','faild')";
        }else{
            copyTableFromOldTable($old_table_name,$kasse_nettbutikk_id);
            if(is_multisite()){
                $qurr = "SELECT * FROM ".$table_name." WHERE status in ('new','credit','faild') AND nettbutikk_id = '{$kasse_nettbutikk_id}'";
            }else{
                $qurr = "SELECT * FROM ".$table_name." WHERE status in ('new','credit','faild')";
            }
        }

        //WriteLog('qurr: '.$qurr);

        $orders = $wpdb->get_results($qurr,OBJECT);
        //WriteLog(__FUNCTION__.' '.print_r($orders,true));
        foreach($orders as $WP_order){
            $order = NULL;
            $nettbutikk_id = (property_exists($WP_order,'nettbutikk_id'))?$WP_order->nettbutikk_id:0;
            if($nettbutikk_id>0)switch_to_blog($nettbutikk_id);
            $order = wc_get_order($WP_order->order_id);

            if(!$order){
                //WriteLog('no order #'.$WP_order);
                continue;
            }

            //WriteLog('order '.print_r($order,true));
            if($sending_type == 'use_warehouses'){
                //WriteLog('ststus: '.$order->get_status().' for order #'.$WP_order->order_id);
                if($order->get_status() != 'completed'){
                    //WriteLog('ststus: '.$order->get_status.' for order #'.$WP_order->order_id);
                    continue;
                }
            }else if(!in_array('wc-'.$order->get_status(),$allowd_status) ){
                //WriteLog('ststus: '.'wc-'.$order->get_status().' for order #'.$WP_order->order_id . ' is not allowed');
                continue;
            }

            $OrderDetails = $order->get_items();
            $shipping_cost = $order->calculate_shipping()+$order->get_shipping_tax();
            $billing_address = $this->get_billing_address($order);
            $shipping_address = $this->get_shipping_address($order);




            if (!$shipping_address || empty($shipping_address)) {
                $shipping_address = $billing_address;
            }
            $order_to_pck = array();
            $orderLines = array();

            $extraCost = 0;
            $extraCoastDesc = '';
            $ekstra_product_info = '';

            foreach ($OrderDetails as $key => $Product) {
                $totalQty += $Product['qty'];
                $customFieldMessage = '';
                $customFieldPrice = 0;
                if(array_key_exists('wccf',$Product)){
                    $fields = unserialize($Product['wccf']);
                    foreach ($fields as $field){
                        $extraCoastDesc .= $field['label'].' ';
                        $customFieldMessage .= ' '.$field['label'].': '.$field['value'].' ';
                        $customFieldPrice += $field['pricing']['value'];
                    }
                }
                $sizeColorId = $this->getSizeColorIdFromVariationId($Product['variation_id'],$nettbutikk_id);

                $warehouseId = $this->getWarehouseIdFromOrderItem($Product);

                //$price = ($order->get_line_subtotal( $Product,true,false )/ ($Product['qty'] == 0 ? 1 : $Product['qty']))-$customFieldPrice;
                //WriteLog('warehouseId: '.$warehouseId);
                //WriteLog('PRICE: '.$price);

                $percentage = ( ( $order->get_line_subtotal( $Product,true,false ) - $order->get_line_total( $Product,true,false ) ) / $order->get_line_subtotal( $Product,true,false ) ) * 100 ;
                $totalAmount += $order->get_line_total( $Product,true,false );
                //$price = $price-($price*($percentage/100));

                //WriteLog('TOTAL: '.$totalAmount);

                //$extraCost += ($customFieldPrice*$Product['qty']);
                //$percentage = ( ( $Product['line_subtotal'] - $Product['line_total'] ) / $Product['line_subtotal'] ) * 100 ;
                //WriteLog('LineSubTotal: '.$order->get_line_subtotal( $Product,true,false )/$Product['qty']);

                $ordet_subtotal = $order->get_line_subtotal( $Product,true,false )/$Product['qty'];

                $wc_prod = wc_get_product($Product['product_id']);
                //echo '<h3>product_id: '.$Product['product_id'].'product: '.print_r($wc_prod,true).'</h3>';

                $p_id = (method_exists($wc_prod,'get_id'))?$wc_prod->get_id():$Product['product_id'];
                $p_sku = (method_exists($wc_prod,'get_sku'))?$wc_prod->get_sku():get_post_meta( $p_id, '_sku', true);
                $article_id_to_send = (integer)self::getPCKArticleIDFromSKU($p_id,$p_sku,$nettbutikk_id);//PCK id

                if(is_object($wc_prod) && $wc_prod->get_type() == 'yith_bundle'){
                    continue;
                }

                if($ordet_subtotal<=0 && isset($Product['_wcpdf_regular_price'])){
                    $price_data = $Product['_wcpdf_regular_price'];
                    $ordet_subtotal = $price_data['excl'];
                }
                $meta = get_post_meta($p_id,'stockDetails_0',true);
                if(is_array($meta)){
                    $meta_obj = end($meta);
                    $warehouse = getFrontEndWarehousByWarehouseId($meta_obj->warehouseId);
                }else{
                    $warehouse = getFrontEndWarehousByWarehouseId($meta->warehouseId);
                }

                if($warehouse){
                    $warehouse_name = ($warehouse->name && strlen($warehouse->name)>0)?$warehouse->name:'Lager id: '.$warehouse->warehouseId;
                    $ekstra_product_info .= ' || Plukklisteinfo for: '.$wc_prod->get_name().' Skal hentes fra: '.$warehouse_name.' || ';
                }
                if ($creation_type == 'samling_samme_vare_nr')$article_id_to_send = $sizeColorId;
                $orderLines[] = array(
                    'articleId'     => $article_id_to_send,
                    'count'         => (integer)$Product['qty'],
                    'orderLineId'   => $key,
                    'price'         => $ordet_subtotal,
                    'sizeColorId'   => $sizeColorId,
                    'discount'	    => is_nan($percentage)?0:$percentage,
                    'info'		    => $customFieldMessage,
                    'warehouseId'   => $warehouseId
                    );

            }


            $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
            $paymentMethod = '';
            //WriteLog('payment_method: '.print_r($order->payment_method,true));
            //WriteLog('saved: '.$m360_pck_options_orders[ 'm360_pck_credit_order' ]);

            if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
                $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
                $pyment_id = get_post_meta( $order->get_id(), '_payment_method', true );
                //WriteLog('isCredit: '.$isCredit.' paymentMethod: '.$pyment_id);
                if($pyment_id == $isCredit || $order->get_payment_method() == 'wc-booking-gateway'){
                    $paymentMethod = 3;
                }
            }
            //WriteLog('Payment: '.$paymentMethod);
            //WriteLog('order Payment: '.print_r($order->payment_method,true));

            $label = esc_attr( $m360_pck_options_orders [ 'm360_pck_order_extra_coast_label' ] );

            /*
            WriteLog('orderLines: '.print_r($orderLines,true));

            WriteLog('calculated: '.$order->get_total());
            WriteLog('totalmount: '.$totalAmount);
            WriteLog('shipping: '.$shipping_cost);
            WriteLog('totaldiscount: '.$order->get_total_discount());
            */

            $extraCost = $order->get_total()-($totalAmount+$shipping_cost);


            //WriteLog('ExtraCoast: '.$extraCost);

            if(strlen($extraCoastDesc)<=0){
                if($extraCost>0){
                    $extraCoastDesc = (strlen($label)>0)?$label:'Gebyr';
                }
            }

            $contactAddressline1 = $billing_address['address_1'];
            $contactAddressline2 = $billing_address['address_2'];

            $billing_first_name = $billing_address['first_name'];
            $billing_last_name = $billing_address['last_name'];

            $contactName = $billing_first_name.' '.$billing_last_name;
            $contactPostCity = $billing_address['city'];
            $contactPostNo = $billing_address['postcode'];


            $deliveryAddressLine1 = (strlen($shipping_address['address_1'])>0)?$shipping_address['address_1']:$contactAddressline1;
            $deliveryAddressLine2 = (strlen($shipping_address['address_2'])>0)?$shipping_address['address_2']:$contactAddressline2;

            $shipping_first_name = (strlen($shipping_address['first_name'])>0)?$shipping_address['first_name']:$billing_first_name;
            $shipping_last_name = (strlen($shipping_address['last_name'])>0)?$shipping_address['last_name']:$billing_last_name;

            $deliveryName = $shipping_first_name.' '.$shipping_last_name;
            $deliveryPostCity = (strlen($shipping_address['city'])>0)?$shipping_address['city']:$contactPostCity;
            $deliveryPostNo = (strlen($shipping_address['postcode'])>0)?$shipping_address['postcode']:$contactPostNo;



            if($multiStorePlugin && $paymentMethod == 3){
                $user = getUserByEmailAndNettbutikk($billing_address['email'],$nettbutikk_id);
                //WriteLog('THE USER: '.print_r($user,true));
                if($user){
                    $user_id = $user->contactId;
                    if(strlen($contactAddressline1)<=0 || ctype_space($contactAddressline1))
                        $contactAddressline1 = $user->invoiceAddress;

                    if(strlen($contactAddressline2)<=0 || ctype_space($contactAddressline2))
                        $contactAddressline2 = $user->invoiceAddress2;

                    if(strlen($contactName)<=0 || ctype_space($contactName))
                        $contactName = $user->name;

                    if(strlen($contactPostCity)<=0 || ctype_space($contactPostCity))
                        $contactPostCity = $user->invoicePostCity;

                    if(strlen($contactPostNo )<=0 || ctype_space($contactPostNo))
                        $contactPostNo = $user->invoicePostNo;

                    if(strlen($deliveryAddressLine1)<=0 || ctype_space($deliveryAddressLine1))
                        $deliveryAddressLine1 = $user->invoiceAddress;

                    if(strlen($deliveryAddressLine2)<=0 || ctype_space($deliveryAddressLine2))
                        $deliveryAddressLine2 = $user->invoiceAddress2;

                    if(strlen($deliveryName)<=0 || ctype_space($deliveryName))
                        $deliveryName = $user->name;

                    if(strlen($deliveryPostCity)<=0 || ctype_space($deliveryPostCity))
                        $deliveryPostCity = $user->invoicePostCity;

                    if(strlen($deliveryPostNo)<=0 || ctype_space($deliveryPostNo))
                        $deliveryPostNo = $user->invoicePostNo;

                }
            }
            else{
                $user = get_user_by( 'email', $billing_address['email'] );
                if(!$user){
                    $user_id = createCustomerFromOrder($order);
                    update_post_meta( $order->get_id(), '_customer_user', $user_id );

                }else{
                    $user_id =  $user->ID;
                }
            }
            $user_info = get_userdata($user_id);
            if(!in_array('customer',$user_info->roles)){
                $role = get_role('customer');
                if($role&&$user){
                    $user->add_role($role->name);
                }
            }

            $costumer_id_from_order = get_post_meta( $order->get_id(), '_customer_user', true );
            if(empty($costumer_id_from_order) || $costumer_id_from_order != $user_id){
                update_post_meta( $order->get_id(), '_customer_user', $user_id );
            }

            $kunde_mesage = (strlen($order->get_customer_note())>0)?'Melding fra kunde: '.$order->get_customer_note():'';
            $the_message_ink_plukklisteinfo = $ekstra_product_info.' '.$kunde_mesage;
            if(strlen($contactName)<=0 || ctype_space($contactName))
                $contactName = $billing_address['email'];

            if(strlen($deliveryName)<=0 || ctype_space($deliveryName))
                $deliveryName = $billing_address['email'];

            $order_to_pck['contactAddressline1'] = $contactAddressline1;
            $order_to_pck['contactAddressline2'] = $contactAddressline2;
            $order_to_pck['contactId'] = $user_id;
            $order_to_pck['contactName'] = $contactName;
            $order_to_pck['contactPostCity'] = $contactPostCity;
            $order_to_pck['contactPostNo'] = $contactPostNo;

            $order_to_pck['deliveryAddressLine1'] = $deliveryAddressLine1;
            $order_to_pck['deliveryAddressLine2'] = $deliveryAddressLine2;
            $order_to_pck['deliveryName'] = $deliveryName;
            $order_to_pck['deliveryPostCity'] = $deliveryPostCity;
            $order_to_pck['deliveryPostNo'] = $deliveryPostNo;

            $order_to_pck['deltaOrderId'] = $WP_order->order_id;

            $order_to_pck['email'] = $billing_address['email'];
            $order_to_pck['extraCost'] = 0.0;//round($extraCost,2);
            $order_to_pck['extraCostDescription'] = $extraCoastDesc;
            $order_to_pck['freightCost'] = round((integer)$shipping_cost,2);

            $order_to_pck['orderLines'] = $orderLines;
            $order_to_pck['paymentMethod']=$paymentMethod;
            $order_to_pck['phone'] = $billing_address['phone'];

            $order_to_pck['freightCostDescription'] = $order->get_shipping_method();//$this->cleanFreightDescription($magentoOrder->getShippingDescription());
            $order_to_pck['message'] = $the_message_ink_plukklisteinfo;
            $order_to_pck['storePickup'] = ($order->get_shipping_method() == 'Hent i butikken')?true:false;

            if($sending_type == 'use_warehouses'){
                $paymentMethod_title = $order->get_payment_method_title();
                $authorzationId = get_post_meta($WP_order->order_id, '_order_key',true);

                $order_to_pck['deliveredItemsAndCapturedPaymentInfo'] = array(
                    'amount' => $order->get_total(),
                    'authorzationId'=> $authorzationId,
                    'extraCost' => 0.0,
                    'freightCost'=>round((integer)$shipping_cost,2),
                    'paymentMethod'=>$paymentMethod_title
                );
            }

            $ordersArray[] = $order_to_pck;

            //WriteLog('orderLines '.print_r($orderLines,true));
            //WriteLog('OrderDetails '.print_r($OrderDetails,true));
            //WriteLog('shipping_address '.print_r($shipping_address,true));
            //WriteLog('billing_address '.print_r($billing_address,true));


            if($sending_type != 'use_warehouses')$order->update_status( 'processing','order '.$WP_order->order_id.' has been sendt to PCK' );
            if($nettbutikk_id>0)restore_current_blog();

            if($update_order_status)M360PCKOrderStatus::UpdateOrderStatus($WP_order->order_id,$totalQty,0,M360PCKOrderStatus::STATUS_SUBMITTED,$nettbutikk_id);

        }
        //WriteLog('ordersArray '.print_r($ordersArray,true));
        //return false;
        return $ordersArray;
    }
}
