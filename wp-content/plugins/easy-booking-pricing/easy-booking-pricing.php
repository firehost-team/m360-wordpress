<?php
/*
Plugin Name: Easy Booking: Pricing
Plugin URI: https://www.easy-booking.me/addon/pricing/
Description: Custom pricing for Easy Booking.
Version: 1.0.4
Author: @_Ashanna
Author URI: https://www.easy-booking.me/
Licence : GPLv2 or later
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Pricing' ) ) :

class Easy_Booking_Pricing {

    protected static $_instance = null;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {


        if ( $this->ebp_easy_booking_is_active() ) {

            $plugin = plugin_basename( __FILE__ );
            
            // Init plugin
            add_action( 'plugins_loaded', array( $this, 'ebp_init' ), 24 );

            // Add settings link
            add_filter( 'plugin_action_links_' . $plugin, array( $this, 'ebp_add_settings_link' ) );

            // Add notices
            add_action( 'admin_notices', array( $this, 'ebp_add_notices' ) );

            // Add network notices
            add_action( 'network_admin_notices', array( $this, 'ebp_add_network_notices' ) );

        }

    }

    /**
    *
    * Check if WooCommerce Easy Booking is active
    *
    * @return bool
    *
    **/
    private function ebp_easy_booking_is_active() {

        // Get active plugins
        $active_plugins = (array) get_option( 'active_plugins', array() );

        if ( is_multisite() ) {
            $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
        }

        // Return true if Easy Booking is active
        return ( array_key_exists( 'woocommerce-easy-booking-system/woocommerce-easy-booking.php', $active_plugins ) || in_array( 'woocommerce-easy-booking-system/woocommerce-easy-booking.php', $active_plugins ) );

    }

    /**
    *
    * Init plugin
    *
    **/
    public function ebp_init() {

        // Don't init plugin if WooCommerce Easy Booking isn't at least version 1.9.
        if ( ! method_exists( WCEB(), 'wceb_get_version' ) ) {
            return;
        }
        
        // Define constants
        $this->ebp_define_constants();

        // load plugin textdomain
        $this->ebp_load_textdomain();

        // Common includes
        $this->ebp_includes();

        // Admin includes
        if ( is_admin() ) {
            $this->ebp_admin_includes();
        }
        
        // Frontend includes
        if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
            $this->ebp_frontend_includes();
        }

    }

    /**
    *
    * Define constants
    *
    **/
    private function ebp_define_constants() {
        // Plugin directory path
        define( 'EBP_PLUGIN_FILE', __FILE__ );
    }

    /**
    *
    * Load plugin textdomain for translations
    *
    **/
    private function ebp_load_textdomain() {
        load_plugin_textdomain( 'easy_booking_pricing', false, basename( dirname(__FILE__) ) . '/languages/' );
    }

    /**
    *
    * Common includes
    *
    **/
    private function ebp_includes() {
        include_once( 'easy-booking-pricing-update.php' );
        include_once( 'includes/ebp-functions.php' );
    }

    /**
    *
    * Admin includes
    *
    **/
    private function ebp_admin_includes() {
        include_once( 'includes/admin/class-ebp-admin-assets.php' );
        include_once( 'includes/settings/class-ebp-settings.php' );
        include_once( 'includes/admin/class-ebp-admin-product.php' );
    }

    /**
    *
    * Frontend includes
    *
    **/
    private function ebp_frontend_includes() {
        include_once( 'includes/class-ebp-product-view.php' );
        include_once( 'includes/class-ebp-assets.php' );
    }

    /**
    *
    * Add settings link
    *
    **/
    public function ebp_add_settings_link( $links ) {
        $settings_link = '<a href="admin.php?page=easy-booking-pricing">' . __( 'Settings', 'easy_booking_pricing' ) . '</a>';
        array_push( $links, $settings_link );

        return $links;
    }

    /**
    *
    * Add notices
    *
    **/
    public function ebp_add_notices() {

        if ( is_multisite() ) {
            return;
        }

        if ( ! method_exists( WCEB(), 'wceb_get_version' ) ) {
            include_once( 'includes/admin/views/html-ebp-notice-update.php' );
            return;
        }

        $settings = get_option( 'ebp_settings' );
        $license_key = isset( $settings['ebp_license_key'] ) ? $settings['ebp_license_key'] : false;

        if ( empty( $license_key ) ) {
            $license_key = false;
        }

        $screen = get_current_screen();

        // Don't display notices on the plugin settings page
        if ( in_array( $screen->id, array( 'easy-booking_page_easy-booking-pricing' ) ) ) {
            return;
        }

        // Display license key notice
        if ( ! is_multisite() && get_option( 'easy_booking_display_notice_ebp_license' ) !== '1' && ! $license_key ) {
            include( 'includes/admin/views/html-ebp-notice-license-key.php' );
        }

    }

    /**
    *
    * Add network notices
    *
    **/
    public function ebp_add_network_notices() {
        $settings = get_option('easy_booking_global_settings');
        $license_key = ! isset( $settings['ebp_license_key'] ) || empty( $settings['ebp_license_key'] ) ? false : $settings['ebp_license_key'];

        $screen = get_current_screen();
        
        // Don't display notices on the plugin settings page
        if ( in_array( $screen->id, array( 'toplevel_page_easy-booking-network' ) ) ) {
            return;
        }

        // Display license key notice
        if ( get_option( 'easy_booking_display_notice_ebp_license' ) !== '1' && ! $license_key ) {
            include( 'includes/admin/views/html-ebp-notice-license-key.php' );
        }
    }

}

function ebp() {
    return Easy_Booking_Pricing::instance();
}

ebp();

endif;