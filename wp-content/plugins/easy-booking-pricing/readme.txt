==== Easy Booking: Pricing ====
Contributors: @_Ashanna
Tags: woocommerce, booking, renting, products, book, rent, e-commerce, prices, pricing
Requires at least: 4.2, WooCommerce 2.5, WooCommerce Easy Booking 2.1.7
Tested up to: 4.9.4, WooCommerce 3.3.1
Stable tag: 1.0.4
License: GPLv2 or later

Easy Booking: Pricing is an add-on for WooCommerce Easy Booking to set different prices depending on a day, date or daterange.

== Description ==

This add-on allows to set different prices for WooCommerce Easy Booking.

You will be able to set different prices depending on :

- Weekdays
- Specific dates
- Dateranges

Specific dates and dateranges can repeat every year (until it reaches the limit year).

== Installation ==

Make sure you have installed WooCommerce and WooCommerce Easy Booking.

1. Install the “Easy Booking: Pricing” Plugin.
2. Activate the plugin.
3. Enter your license key. For multisites, install the plugin and enter the license key on the network. Then, activate it on each sites.
4. In your administration panel, go to the Easy Booking menu, and the Pricing sub-menu to set the plugin settings.
5. In your administration panel, go to the bookable product page and set the prices for each product or variation in the "General" tab (and below the price inputs on the "Variations" tab for variable products). Save the product.
6. And that’s it !

== Changelog ==

= 1.0.4 =

* Add - Compatibility with "Last available date".
* Add - Added 'easy_booking_pricing_get_daily_price_html' filter to change " / day" " / night" texts in some situations.

= 1.0.3 =

* Fix - Compatibility with WooCommerce 3.0 and WooCommerce Product Bundles 5.2.0.
* Fix - Array to string conversion error when saving a custom price.

= 1.0.2 =

* Fix - Compatibility with WooCommerce 3.0.

= 1.0.1 =

* Fix - Fixed an issue with one date selection where price returned was always 1.

= 1.0.0 =

* Initial Release

== Upgrade Notice ==