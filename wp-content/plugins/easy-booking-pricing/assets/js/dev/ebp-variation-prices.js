(function($) {
	$(document).ready(function() {

		$('body').on( 'found_variation', '.variations_form', function( e, variation ) {

			if ( ! variation.is_purchasable || ! variation.is_in_stock || ! variation.variation_is_visible || ! variation.is_bookable ) {
				$('.ebp_pricing').html('').slideUp(200);
			} else {
				var variation_id = variation.variation_id;
				display_product_prices( variation_id );
			}

		});

		$('body').on( 'reset_image', '.variations_form', function() {
			$('.ebp_pricing').slideUp( 200 );
		});

		function display_product_prices( variation_id ) {
			var output = ebp.prices[variation_id];
			$('p.ebp_pricing').html( output ).slideDown( 200 );
		}
		
	});
})(jQuery);