(function($) {
	$(document).ready(function() {

		$.extend($.fn.pickadate.defaults,{
			formatSubmit: 'yyyy-mm-dd',
			hiddenName: true
		});

		$('.product_data').on('click', '.ebp-pricing a.add-price',function() {

			$(this).closest('.ebp-pricing').find('.ebp-table').append( $(this).data( 'row' ) );

			var index = $('.price-row').length,
				addedRow = $('.price-row').last();

			addedRow.find('.ebp_select_type').change();

			var span = addedRow.find('.repeat_row'),
				attr = 'date_every_year[' + index + ']';

			span.find('label').attr('for', attr);
			span.find('.repeat').attr('id', attr);

			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');

			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );

			return false;

		});

		function initPickers( datepicker, start, end ) {

			if ( typeof datepicker === 'undefined' ) {
				var datepicker = $('.ebp_datepicker');
			}

			var $input = datepicker.pickadate(),
				picker = $input.pickadate('picker');

			if ( typeof start === 'undefined' ) {
				start = $('.ebp_price_daterange_start');
			}

			if ( typeof end === 'undefined' ) {
				end = $('.ebp_price_daterange_end');
			}

			start.each( function() {
				initPickersOnLoad( $(this), 'min' );
			});
			
			end.each( function() {
				initPickersOnLoad( $(this), 'max' );
			});
			
		}

		function initPickersOnLoad( input, thingToSet ) {

			var $daterangeInput = input.pickadate(),
				daterangePicker = $daterangeInput.pickadate('picker');

			setMinOrMax( daterangePicker, thingToSet );

			daterangePicker.on('set', function() {
				setMinOrMax( daterangePicker, thingToSet );
			});

			return false;

		}

		function setMinOrMax( picker, thingToSet ) {
			var select = picker.get('select');

			if ( ! select ) {
			 	select = Infinity;
			}

			var $sibling = picker.$node.siblings('.ebp_datepicker').pickadate();
			
			var sliblingPicker = $sibling.pickadate('picker'),
				sliblingItem = sliblingPicker.component.item;

			if ( thingToSet === 'min' ) {
				sliblingItem.min = select;
			} else if ( thingToSet === 'max') {
				sliblingItem.max = select;
			}
			
			sliblingPicker.render();

		}

		initPickers();

		$( '#woocommerce-product-data' ).on( 'woocommerce_variations_loaded', function() {
			initPickers();
		});

		$('.product_data').on('change', '.ebp-pricing .ebp_select_type', function() {

			var selectedType = $(this).val(),
				parentRow = $(this).parents('.form-row'),
				selectLine = parentRow.find('.date_to_set_price'),
				repeatLine = parentRow.find('.repeat_row');

			if ( ! selectedType ) {
				return;
			}

			selectLine.find('span').hide();

			var line = selectLine.find('span[data-type="' + selectedType + '"]'),
				datepicker = line.find('.ebp_datepicker');

			if ( selectedType === 'ebp_day' ) {
				repeatLine.hide();
			} else {
				repeatLine.show();
			}

			line.show();

			if ( selectedType === 'ebp_daterange' ) {
				var startPicker = line.find('.ebp_price_daterange_start'),
					endPicker = line.find('.ebp_price_daterange_end');

				initPickers( datepicker, startPicker, endPicker );
			} else {
				initPickers( datepicker );
			}

		});

		$('.product_data').on('change', '.ebp-pricing .repeat', function() {

			if ( $(this).is(':checked') ) {
				$(this).next('input[type="hidden"]').prop('disabled', true);
			} else {
				$(this).next('input[type="hidden"]').prop('disabled', false);
			}

		});

		$('.product_data').on('click','.ebp-pricing a.delete-price', function() {

			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');
			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );
			$(this).closest('tr').remove();
			return false;

		});

	});
})(jQuery);
