<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'EBP_Product_View' ) ) :

class EBP_Product_View {

    public function __construct() {
        // Get plugin options values
        $this->options = get_option( 'ebp_settings' );

        add_action( 'woocommerce_single_product_summary', array( $this, 'ebp_display_booking_pricing' ), 10 );
        add_action( 'woocommerce_after_bundled_item_cart_details', array( $this, 'ebp_display_booking_pricing' ), 6, 1 );
        add_filter( 'woocommerce_get_price_html', array( $this, 'ebp_display_grouped_prices' ), 20, 2 );
        // add_filter( 'easy_booking_display_price', array( $this, 'ebp_display_price' ), 10, 2 );
        add_filter( 'easy_booking_display_average_price', array( $this, 'ebp_display_average_price' ), 12, 2 );
    }

    /**
    *
    * Display prices on the product page (simple et bundled products)
    * @param (optional) WC_Bundle_Item - $bundled_item
    *
    * @return str
    *
    **/
    public function ebp_display_booking_pricing( $bundled_item = false ) {
        
        global $product;

        // Avoid conflicts
        $_product = $product;

        $product_id = is_callable( array( $_product, 'get_id' ) ) ? $_product->get_id() : $_product->id;

        // WooCommerce Product Bundles compatibility
        if ( $bundled_item && ! empty( $bundled_item ) ) {
            
            $product_id = $bundled_item->product_id;

            if ( ! $bundled_item->is_priced_individually() ) {
                return;
            }

            if ( $bundled_item->product->is_type( 'variable' ) ) {
                echo '<p class="ebp_pricing"></p>';
                return;
            }

        }

        // If product is not bookable or discounts are not displayed, return
        if ( ! wceb_is_bookable( $_product ) || empty( $this->options['ebp_display_prices'] ) ) {
            return;
        }

        echo '<p class="ebp_pricing">';
        $parent = is_callable( array( $_product, 'get_parent_id' ) ) ? $_product->get_parent_id() : $_product->get_parent();
        
        if ( ( $_product->is_type( 'simple' ) && ! $parent ) || $_product->is_type( 'bundle' ) ) {
            echo ebp_get_display_booking_prices( $product_id );
        }

        echo '</p>';
        
    }

    /**
    *
    * Display prices on the product page (grouped products)
    * @param str - $content
    * @param WC_Product - $product
    *
    * @return str - $content
    *
    **/
    public function ebp_display_grouped_prices( $content, $product ) {

        // If product has no parent product, return normal price
        $parent = is_callable( array( $product, 'get_parent_id' ) ) ? $product->get_parent_id() : $product->get_parent();

        if ( ! $parent ) {
            return $content;
        }

        // If not on the product page, return normal price
        if ( ! is_product() ) {
            return $content;
        }
        
        // If product is not bookable, return normal price
        if ( ! wceb_is_bookable( $product ) || empty( $this->options['ebp_display_prices'] ) ) {
            return $content;
        }
        
        $product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;

        $prices = ebp_get_display_booking_prices( $product_id );
        $content .= '<p class="ebp_discounts">' . $prices . '</p>';

        return $content;
    }

    /**
    *
    * Changes the normal "xx $" price into a "From xx $" price if custom prices are set
    * Currently not active, still requires some thinking
    * @param str - $display_price
    * @param WC_Product - $product
    *
    * @return str - $display_price
    *
    **/
    function ebp_display_price( $display_price, $product ) {

        $product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;
        
        // Get pricing data
        $product_prices = get_post_meta( $product_id, '_ebp_product_prices', true );
        $product_price = wceb_get_product_price( $product ); // Product base price

        $prices = array( $product_price );
        if ( ! empty( $product_prices ) ) {

            foreach ( $product_prices as $product_price ) {
                $prices[] = isset( $product_price['sale_price'] ) ? $product_price['sale_price'] : $product_price['price'];
            }

            // Get lower price
            $lower_price = min( $prices );
            $price_text = wceb_get_price_html( $product );

            $display_price = 'From ' . wc_price( $lower_price ) . '<span class="wceb-price-format">' . esc_html( $price_text ) . '</span>';
        }

        return $display_price;
    }

    /**
    *
    * Displays the average price / day / week / custom period after selecting the dates
    * @param bool - $display
    * @param int - $id
    *
    * @return bool - $display_price
    *
    **/
    public function ebp_display_average_price( $display, $id ) {

        // Get product prices
        $product_prices = get_post_meta( $id, '_ebp_product_prices', true );

        // If the product has custom prices, return the average price
        if ( ! empty( $product_prices ) ) {
            $display = true;
        }

        return $display;
    }

}

return new EBP_Product_View();

endif;