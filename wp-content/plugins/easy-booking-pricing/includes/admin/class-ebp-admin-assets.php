<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Pricing_Admin_Assets' ) ) :

class Easy_Booking_Pricing_Admin_Assets {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'ebp_admin_scripts' ), 50 );
    }

    public function ebp_admin_scripts() {
        $screen = get_current_screen();

        wp_register_script(
            'ebp-admin-product-functions',
            wceb_get_file_path( 'admin', 'ebp-admin-product-functions', 'js', EBP_PLUGIN_FILE ),
            array( 'jquery', 'pickadate' ),
            '1.0',
            true
        );

        wp_register_style(
            'ebp-settings',
            wceb_get_file_path( 'admin', 'ebp-settings', 'css', EBP_PLUGIN_FILE ),
            array(),
            '1.0'
        );

        if ( in_array( $screen->id, array( 'product' ) ) ) {

            // JS

            if ( ! wp_script_is( 'pickadate', 'enqueued' ) ) {

                if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
                    wp_enqueue_script( 'picker' );
                    wp_enqueue_script( 'legacy' );
                    wp_enqueue_script( 'pickadate' );
                } else {
                    wp_enqueue_script( 'pickadate' );
                }

            }
            
            wp_enqueue_script( 'ebp-admin-product-functions' );

            // Calendar translation
            if ( ! wp_script_is( 'datepicker.language', 'enqueued' ) ) {
                wp_enqueue_script( 'datepicker.language' );
            }

            // CSS

            // Calendar CSS
            if ( ! wp_style_is( 'default-picker', 'enqueued' ) ) {
                wp_enqueue_style( 'default-picker' );
            }

            wp_enqueue_style( 'ebp-settings' );

        }

	}

}

return new Easy_Booking_Pricing_Admin_Assets();

endif;