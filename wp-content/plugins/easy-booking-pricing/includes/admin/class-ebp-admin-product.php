<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Pricing_Admin_Product' ) ) :

class Easy_Booking_Pricing_Admin_Product {

	public function __construct() {

		// Filter product types
        if ( WCEB()->allowed_types ) foreach ( WCEB()->allowed_types as $type ) {

            // If product is grouped, prices are set on the children (simple) products
            if ( $type !== 'grouped' ) {
				add_action( 'woocommerce_process_product_meta_' . $type, array( $this, 'ebp_save_product_pricing' ), 10, 1);
			}

		}

		add_action( 'woocommerce_product_options_pricing', array( $this, 'ebp_admin_product_pricing' ), 10 );
		add_action( 'woocommerce_variation_options_pricing', array( $this, 'ebp_admin_variation_pricing' ), 10, 3 );

		// Save variation pricing
		add_action( 'woocommerce_save_product_variation', array( $this, 'ebp_save_variation_pricing' ), 15, 2 );

	}

	/**
	 *
	 * Product type disabled dates form
	 *
	 */
	public function ebp_admin_product_pricing() {
		global $post;

		$prices = get_post_meta( $post->ID, '_ebp_product_prices', true );

		echo '<div class="options_group show_if_bookable">';
			include( 'views/html-ebp-admin-product.php' );
		echo '</div>';
	}

	/**
	 *
	 * Variation disabled dates form
	 *
	 */
	public function ebp_admin_variation_pricing( $loop, $variation_data, $variation ) {

		$variation_id = is_callable( array( $variation, 'get_id' ) ) ? $variation->get_id() : $variation->ID;
		$prices = get_post_meta( $variation_id, '_ebp_product_prices', true );

		echo '<div class="show_if_variation_bookable">';
			include( 'views/html-ebp-admin-product.php' );
		echo '</div>';

	}

	/**
	 *
	 * Save single, variable and other product type disabled dates
	 *
	 */
	public function ebp_save_product_pricing( $post_id ) {

		// Get booking prices
		$prices = isset( $_POST['ebp_settings'] ) ? $_POST['ebp_settings'] : array();

		// Format and sanitize values
		$ebp_product_prices = $this->ebp_sanitize_prices( $prices );

		// Update post meta
		update_post_meta( $post_id, '_ebp_product_prices', $ebp_product_prices );

	}

	/**
	 *
	 * Save variation disabled dates
	 *
	 */
	public function ebp_save_variation_pricing( $variation_id, $v ) {

		// Get booking prices
		$settings = isset( $_POST['ebp_settings'] ) ? $_POST['ebp_settings'] : array();

		$prices = array();

		$prices_options = array(
			'var_ebp_date_type',
			'var_ebp_price_day',
			'var_ebp_price_date',
			'var_ebp_price_daterange_start',
			'var_ebp_price_daterange_end',
			'var_ebp_repeat',
			'var_ebp_price',
			'var_ebp_sale_price',
			'var_ebp_unit'
		);

		// Get booking prices for the variation
		foreach ( $prices_options as $option ) {
			$name = str_replace( 'var_', '', $option );
			$prices[$name] = isset( $settings[$option][$v] ) ? $settings[$option][$v] : '';
		}

		// Format and sanitize values
		$ebp_product_prices = $this->ebp_sanitize_prices( $prices );
		
		// Update post meta
		update_post_meta( $variation_id, '_ebp_product_prices', $ebp_product_prices );

	}

	/**
	*
	* Formats and sanitizes prices before saving
	*
	* @param array $prices
	* @return array $ebp_prices
	*
	**/
	private function ebp_sanitize_prices( array $prices ) {

	    $ebp_prices = array();

	    if ( ! isset( $prices['ebp_date_type'] ) ) {
	        return $ebp_prices;
	    }

	    // Count prices
	    $prices_count = count( $prices['ebp_date_type'] );
	    
	    for ( $i = 0; $i < $prices_count; $i++ ) {

			$price = isset( $prices['ebp_price'][$i] ) ? wc_clean( $prices['ebp_price'][$i] ) : '';
			$type  = isset( $prices['ebp_date_type'][$i] ) ? sanitize_text_field( $prices['ebp_date_type'][$i] ) : '';
			$unit  = isset( $prices['ebp_unit'][$i] ) ? sanitize_text_field( $prices['ebp_unit'][$i] ) : 'ebp_single';
			
	        // If price or type is not set, return
	        if ( ( empty( $price ) && $price !== '0' ) || empty( $type ) ) {
	            continue;
	        }

	        $date_to_set_price[$i] = '';

	        switch ( $type ) {

	            case 'ebp_day' :

	                $date_to_set_price[$i]  = isset( $prices['ebp_price_day'][$i] ) ? sanitize_text_field( $prices['ebp_price_day'][$i] ) : '';
	                $set_price_every_year[$i] = 'off';

	            break;
	            case 'ebp_date' :

	                $date_to_set_price[$i]    = isset( $prices['ebp_price_date'][$i] ) ? sanitize_text_field( $prices['ebp_price_date'][$i] ) : '';
	                $set_price_every_year[$i] = isset( $prices['ebp_repeat'][$i] ) ? sanitize_text_field( $prices['ebp_repeat'][$i] ) : 'off';

	            break;
	            case 'ebp_daterange' :

	                // Convert dates to times
	                $start = isset( $prices['ebp_price_daterange_start'][$i] ) ? strtotime( $prices['ebp_price_daterange_start'][$i] ) : '';
	                $end   = isset( $prices['ebp_price_daterange_end'][$i] ) ? strtotime( $prices['ebp_price_daterange_end'][$i] ) : '';

	                $date_to_set_price[$i] = array(
	                    sanitize_text_field( $prices['ebp_price_daterange_start'][$i] ),
	                    sanitize_text_field( $prices['ebp_price_daterange_end'][$i] )
	                );
	                
	                // If one of the date is missing or start is superior to end, remove dates
	                if ( empty( $start ) || empty( $end ) || $start > $end ) {
	                    $date_to_set_price[$i] = array();
	                }

	                $set_price_every_year[$i] = isset( $prices['ebp_repeat'][$i] ) ? sanitize_text_field( $prices['ebp_repeat'][$i] ) : 'off';

	            break;
	            
	        }

	        // If no date(s) is (are) set, return
	        if ( empty( $date_to_set_price[$i] ) ) {
	            continue;
	        }

	        $ebp_prices[$i] = array(
	            'type'     => $type,
	            'date'     => $date_to_set_price[$i],
	            'repeat'   => $set_price_every_year[$i],
	            'price'    => wc_format_decimal( $price ),
	            'unit'     => $unit
	        );

	        // Maybe store sale price
			if ( isset( $prices['ebp_sale_price'][$i] ) ) {

				$sale_price = (string) wc_clean( $prices['ebp_sale_price'][$i] );

				if ( ! empty( $sale_price ) || $sale_price === '0' ) {

					// Check that sale price is inferior to regular price
					if ( $sale_price < $price ) {
						$ebp_prices[$i]['sale_price'] = wc_format_decimal( $sale_price );
					}

				}
				
			}

	    }

	    return $ebp_prices;

	}

}

return new Easy_Booking_Pricing_Admin_Product();

endif;