<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="updated easy-booking-notice">

	<p>

		<?php _e( 'This version of Easy Booking: Pricing requires at least WooCommerce Easy Booking version 2.0.8. Please update WooCommerce Easy Booking to its latest version.', 'easy_booking_pricing' ); ?>

	</p>

</div>