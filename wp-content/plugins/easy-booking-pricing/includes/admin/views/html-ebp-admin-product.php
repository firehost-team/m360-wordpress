<?php $file = isset( $variation ) ? 'html-ebp-variation-pricing' : 'html-ebp-pricing'; ?>
<div class="form-field downloadable_files ebp-pricing">
		
	<p><label><?php _e( 'Booking prices', 'easy_booking_pricing' ); ?>:</label></p>

	<table class="widefat ebp-table ebp-pricing-single">

		<thead>

			<tr>
				<th width="1%">&nbsp;</th>
				<th width="20%"><?php _e( 'Type', 'easy_booking_pricing' ); ?></th>
				<th width="28%"><?php _e( 'Date(s)', 'easy_booking_pricing' ); ?></th>
				<th width="10%"><?php _e( 'Repeat every year?', 'easy_booking_pricing' ); ?></th>
				<th width="10%"><?php echo __( 'Regular price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')'; ?></th>
				<th width="10%"><?php echo __( 'Sale price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')'; ?></th>
				<th width="20%" class="show_if_two_dates">
					<?php _e( 'Per' ); ?>
					<span class="tips" data-tip="<?php _e( 'Choose whether to apply the price daily or per week/custom period (depending on the booking duration you selected). Day and single date prices should usually be per day/night whereas daterange prices should be per week/custom period.', 'easy_booking_pricing' ); ?>">[?]</span>
				</th>
				<th width="1%">&nbsp;</th>
			</tr>

		</thead>

		<tbody>

			<?php

			$i = 0;

			if ( $prices ) foreach ( $prices as $price ) {
				$i++;
				include( $file . '.php' );
			}

			?>

		</tbody>

		<tfoot>

			<tr>

				<th colspan="8">
					<a href="#" class="button add-price" data-row="<?php
                        $price = array(
							'type'       => 'ebp_day',
							'date'       => '',
							'repeat'     => '',
							'price'      => '',
							'sale_price' => '',
							'unit'       => 'ebp_single'
                        );
                        ob_start();
                        include( $file . '.php' );
                        echo esc_attr( ob_get_clean() );
                    ?>">
                    <?php _e( 'Add price', 'easy_booking_pricing' ); ?>
                    </a>
				</th>

			</tr>

		</tfoot>

	</table>
	
</div>