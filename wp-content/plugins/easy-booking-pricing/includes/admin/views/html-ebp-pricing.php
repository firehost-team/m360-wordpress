<tr class="form-row price-row">

	<td class="sort" width="1%"></td>

	<td class="date_type" width="20%">

		<?php $selected_type = esc_attr( $price['type'] ); ?>

		<select class="ebp_select_type" name="ebp_settings[ebp_date_type][]">
			<option value="ebp_day" <?php selected( $selected_type, 'ebp_day', true ) ?>>
				<?php _e( 'Day', 'easy_booking_pricing' ); ?>
			</option>
			<option value="ebp_date" <?php selected( $selected_type, 'ebp_date', true ) ?>>
				<?php _e( 'Date', 'easy_booking_pricing' ); ?>
			</option>
			<option value="ebp_daterange" <?php selected( $selected_type, 'ebp_daterange', true ) ?>>
				<?php _e( 'Date Range', 'easy_booking_pricing' ); ?>
			</option>
		</select>

	</td>

	<td class="date_to_set_price" width="28%">

	<?php $selected = is_array( $price['date'] ) ? array_map( 'esc_attr', $price['date'] ) : esc_attr( $price['date'] );

		if ( $selected_type === 'ebp_day' ) {

			$selected_day = $selected;
			$selected_date = '';
			$selected_daterange[0] = '';
			$selected_daterange[1] = '';
			
		} else if ( $selected_type === 'ebp_date' ) {

			$selected_date = $selected;
			$selected_day = '1';
			$selected_daterange[0] = '';
			$selected_daterange[1] = '';

		} else if ( $selected_type === 'ebp_daterange' ) {

			$selected_daterange[0] = $selected[0];
			$selected_daterange[1] = $selected[1];
			$selected_day = '1';
			$selected_date = '';

		}

	?>

		<span data-type="ebp_day" style="display:<?php echo $selected_type === 'ebp_day' ? 'block' : 'none'; ?>;">
			<select name="ebp_settings[ebp_price_day][]">
				<option value="1" <?php selected( $selected_day, '1', true ) ?>><?php _e( 'Monday', 'easy_booking_pricing' );?></option>
				<option value="2" <?php selected( $selected_day, '2', true ) ?>><?php _e( 'Tuesday', 'easy_booking_pricing' );?></option>
				<option value="3" <?php selected( $selected_day, '3', true ) ?>><?php _e( 'Wednesday', 'easy_booking_pricing' );?></option>
				<option value="4" <?php selected( $selected_day, '4', true ) ?>><?php _e( 'Thursday', 'easy_booking_pricing' );?></option>
				<option value="5" <?php selected( $selected_day, '5', true ) ?>><?php _e( 'Friday', 'easy_booking_pricing' );?></option>
				<option value="6" <?php selected( $selected_day, '6', true ) ?>><?php _e( 'Saturday', 'easy_booking_pricing' );?></option>
				<option value="7" <?php selected( $selected_day, '7', true ) ?>><?php _e( 'Sunday', 'easy_booking_pricing' );?></option>
			</select>
		</span>

		<span data-type="ebp_date" style="display:<?php echo $selected_type === 'ebp_date' ? 'block' : 'none'; ?>;">
			<input type="text" name="ebp_settings[ebp_price_date][]" class="ebp_datepicker ebp_price_datepicker" data-value="<?php echo $selected_type === 'ebp_date' ? $selected_date : ''; ?>" value="">	
		</span>

		<span data-type="ebp_daterange" style="display:<?php echo $selected_type === 'ebp_daterange' ? 'block' : 'none'; ?>;">
			<input type="text" name="ebp_settings[ebp_price_daterange_start][]" class="ebp_datepicker ebp_price_daterange_start" data-value="<?php echo $selected_type === 'ebp_daterange' ? $selected_daterange[0] : ''; ?>" value="">
			<input type="text" name="ebp_settings[ebp_price_daterange_end][]" class="ebp_datepicker ebp_price_daterange_end" data-value="<?php echo $selected_type === 'ebp_daterange' ? $selected_daterange[1] : ''; ?>" value="">
		</span>

	</td>

	<td width="10%">

		<?php $repeat = esc_attr( $price['repeat'] ); ?>

		<span class="repeat_row" style="display:<?php echo $selected_type === 'ebp_day' ? 'none' : 'block'; ?>;">
			<input type="checkbox" id="date_every_year[<?php echo $i; ?>]" name="ebp_settings[ebp_repeat][]" class="repeat" <?php checked( $price['repeat'], 'on' ) ?>>
			<input type="hidden" name="ebp_settings[ebp_repeat][]" value="off" <?php echo $repeat === 'on' ? 'disabled' : ''; ?>>
		</span>

	</td>

	<td width="10%">

		<?php $booking_price = isset( $price['price'] ) ? esc_attr( $price['price'] ) : ''; ?>
		<input type="text"  name="ebp_settings[ebp_price][]" class="wc_input_price" value="<?php echo wc_format_localized_price( $booking_price ); ?>">

	</td>

	<td width="10%">

		<?php $booking_sale_price = isset( $price['sale_price'] ) ? esc_attr( $price['sale_price'] ) : ''; ?>
		<input type="text"  name="ebp_settings[ebp_sale_price][]" class="wc_input_price" value="<?php echo wc_format_localized_price( $booking_sale_price ); ?>">

	</td>

	<td class="show_if_two_dates" width="20%">

		<?php $selected_unit = esc_attr( $price['unit'] ); ?>

		<select class="ebp_select_unit" name="ebp_settings[ebp_unit][]">
			<option value="ebp_single" <?php selected( $selected_unit, 'ebp_single', true ) ?>>
				<?php _e( 'Day/Night', 'easy_booking_pricing' ); ?>
			</option>
			<option value="ebp_multi" <?php selected( $selected_unit, 'ebp_multi', true ) ?>>
				<?php _e( 'Week/Custom period', 'easy_booking_pricing' ); ?>
			</option>
		</select>

	</td>

	<td width="1%" class="delete">

		<a href="#" class="delete-price">
			<?php _e( 'Delete', 'woocommerce' ); ?>
		</a>

	</td>

</tr>