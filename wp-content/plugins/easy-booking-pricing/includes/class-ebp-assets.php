<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Pricing_Assets' ) ) :

class Easy_Booking_Pricing_Assets {

	public function __construct() {
        add_action( 'wp_enqueue_scripts', array( $this, 'ebp_enqueue_scripts' ), 20 );
	}

	public function ebp_enqueue_scripts() {
        global $post;

        // Return if not on a single product page
        if ( ! is_product() ) {
            return;
        }

        // Get plugin settings
        $ebp_settings = get_option( 'ebp_settings' );

        // Return if prices are not displayed
        if ( empty( $ebp_settings['ebp_display_prices'] ) ) {
            return;
        }

        $post_id = $post->ID;
        $product = wc_get_product( $post_id );

        // Enqueue script only if product is bookable and variable
        if ( wceb_is_bookable( $product ) && $product->is_type( 'variable' ) ) {

            wp_enqueue_script( 'easy_booking_variation_prices', wceb_get_file_path( '', 'ebp-variation-prices', 'js', EBP_PLUGIN_FILE ), array( 'jquery' ), '1.0', true );

            $variation_ids = $product->get_children();

            $prices = array();
            if ( $variation_ids ) foreach ( $variation_ids as $variation_id ) {
                $prices[$variation_id] = ebp_get_display_booking_prices( $variation_id );
            }
            
            wp_localize_script( 'easy_booking_variation_prices', 'ebp', array(
                    'prices' => $prices
                )
            );

        }

        if ( wceb_is_bookable( $product ) && $product->is_type( 'bundle' ) ) {

            wp_enqueue_script( 'easy_booking_bundle_prices', wceb_get_file_path( '', 'ebp-bundle-variable-prices', 'js', EBP_PLUGIN_FILE ), array( 'jquery' ), '1.0', true );

            $prices = array();
            if ( true === $product->contains( 'priced_individually' ) ) {

                $bundled = $product->get_bundled_item_ids();

                if ( $bundled ) foreach ( $bundled as $bundled_item_id ) {

                    $bundled_item = $product->get_bundled_item( $bundled_item_id );
                    $_product = $bundled_item->product;

                    if ( $bundled_item->is_priced_individually() && $_product->is_type( 'variable' ) ) {
                        
                        if ( $_product->get_children() ) foreach ( $_product->get_children() as $child ) {
                            $prices[$child] = ebp_get_display_booking_prices( $child );
                        }
                        
                    }
                    
                }
            }

            wp_localize_script( 'easy_booking_bundle_prices', 'ebp', array(
                    'prices' => $prices
                )
            );

        }
    }
}

return new Easy_Booking_Pricing_Assets();

endif;