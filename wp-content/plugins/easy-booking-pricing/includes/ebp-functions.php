<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Easy Booking 2.1.0 new filters
add_filter( 'easy_booking_one_date_price', 'ebp_get_one_date_booking_price', 10, 5 );
add_filter( 'easy_booking_two_dates_price', 'ebp_get_booking_price', 5, 5 );

/**
*
* Calculates new price depending on the dates selected
*
* @param str - $new_price
* @param WC_Product - $product
* @param WC_Product - $_product - Product or variation
* @param array - $booking_data - Booking data, including booking duration
* @param str - $price_type - Price or regular price
* @return str - $new_price
*
**/
function ebp_get_booking_price( $new_price, $product, $_product, $booking_data, $price_type ) {

    $easy_booking_settings = get_option('easy_booking_settings');
    $calc_mode = $easy_booking_settings['easy_booking_calc_mode']; // Calculation mode (Days or Nights)

    $duration = $booking_data['duration'];

    $product_prices = array();

    if ( ! $_product ) {
        return;
    }

    // Get product or variation ID
    if ( $_product->is_type('variation') ) {
        $id = is_callable( array( $_product, 'get_id' ) ) ? $_product->get_id() : $_product->variation_id;
    } else {
        $id = is_callable( array( $_product, 'get_id' ) ) ? $_product->get_id() : $_product->id;
    }

    // Get pricing data
    $product_prices = get_post_meta( $id, '_ebp_product_prices', true );

    $pricing = array();
    if ( ! empty( $product_prices ) ) {

        $start = strtotime( $booking_data['start'] ); // Booking start time
        $end   = strtotime( $booking_data['end'] ); // Booking end time

        // Get an array of selected dates
        $dates = array();

        // Get an array of weekdays associated to each selected date
        $days = array();

        // In "Nights" mode, first day is not counted
        if ( $calc_mode === 'nights' ) {
            $start = strtotime( "+1 day", $start );
        }

        // Get all days and dates inside selected daterange
        for ( $date = $start; $date <= $end; $date = strtotime( "+1 day", $date ) ) {
            $day = date( 'w', $date );

            if ( $day === '0' ) {
                $day = '7';
            }

            $dates[] = $date;
            $days[$day][] = $date;
        }

        // If grouped or bundled, get the parent product's booking duration
        if ( $product->is_type( 'grouped' ) || $product->is_type( 'bundle' ) ) {
            $booking_duration = wceb_get_product_custom_booking_duration( $product );
        } else {
            $booking_duration = wceb_get_product_custom_booking_duration( $_product );
        }

        // Get booking duration in days/nights
        if ( $booking_duration != '1' ) {
            $duration *= $booking_duration;
        }
        
        foreach ( $product_prices as $product_price ) {

            $price = isset( $product_price['sale_price'] ) ? $product_price['sale_price'] : $product_price['price'];

            if ( $price_type === 'regular_price' && isset( $product_price['sale_price'] ) ) {
                $price = $product_price['price'];
            }

            // If price is per week/custom duration, divide it by booking duration
            if ( $product_price['unit'] === 'ebp_multi' && $booking_duration != '1' ) {
                $price /= $booking_duration;
            }

            switch ( $product_price['type'] ) {

                case 'ebp_day' : // Weekday

                    $weekday = $product_price['date'];

                    if ( array_key_exists( $weekday, $days ) ) {

                        foreach ( $days[$weekday] as $day_number => $date ) {
                            $pricing[$date] = $price;
                        }
                        
                    }

                break;

                case 'ebp_date' : // Date

                    if ( $product_price['repeat'] === 'on' ) {
                        
                        $current_year = date( 'Y' );
                        $last_date = isset( $easy_booking_settings['easy_booking_last_available_date'] ) ? absint( $easy_booking_settings['easy_booking_last_available_date'] ) : '1825';
                        $max_year     = absint( $last_date / 365 );
                        $last_year    = strval( $current_year + $max_year );

                        $day = explode( '-', $product_price['date'] );

                        // Get an array of all years from current year to limit year set in the settings
                        for ( $i = $current_year; $i <= $last_year; $i++ ) {

                            $next_date = strtotime( $i . '-' . $day[1] . '-' . $day[2] );

                            // Check if date overlap with selected dates
                            if ( $next_date >= $start && $next_date <= $end ) {
                                $pricing[$next_date] = $price;
                            }

                        }

                    } else {

                        $date = strtotime( $product_price['date'] );

                        // Check if date overlap with selected dates
                        if ( $date >= $start && $date <= $end ) {
                            $pricing[$date] = $price;
                        }

                    }
                    

                break;

                case 'ebp_daterange' : // Daterange

                    $prices = array();
                    if ( $product_price['repeat'] === 'on' ) {

                        // Get selected start date year
                        $start_year = explode( '-', $booking_data['start'] );
                        $start_year = $start_year[0];

                        // Get selected end date year
                        $end_year = explode( '-', $booking_data['end'] );
                        $end_year = $end_year[0];

                        // Get special price date without year
                        $price_start_date = explode( '-', $product_price['date'][0] );
                        $price_end_date   = explode( '-', $product_price['date'][1] );

                        if ( $start_year === $end_year ) {

                            // If year are different
                            if ( $price_start_date[0] !== $price_end_date[0] ) {
                                $difference = $price_end_date[0] - $price_start_date[0];
                                $start_year -= $difference;
                            }

                            $prices[] = array(
                                'price_start' => strtotime( $start_year . '-' . $price_start_date[1] . '-' . $price_start_date[2] ),
                                'price_end'   => strtotime( $end_year . '-' . $price_end_date[1] . '-' . $price_end_date[2] )
                            );

                        } else {

                            for ( $i = $start_year; $i <= $end_year; $i++ ) {

                                $j = $i;

                                // If year are different
                                if ( $price_start_date[0] !== $price_end_date[0] ) {
                                    $difference = $price_end_date[0] - $price_start_date[0];
                                    $j -= $difference;
                                }

                                $prices[] = array(
                                    'price_start' => strtotime( $j . '-' . $price_start_date[1] . '-' . $price_start_date[2] ),
                                    'price_end'   => strtotime( $i . '-' . $price_end_date[1] . '-' . $price_end_date[2] )
                                );

                                $j = $i;
                                
                            }

                        }

                    } else {
                        $prices[] = array(
                            'price_start' => strtotime( $product_price['date'][0] ),
                            'price_end'   => strtotime( $product_price['date'][1] )
                        );
                    }

                    foreach ( $prices as $single_price ) {

                        // In "Nights" mode, first day is not counted
                        if ( $calc_mode === 'nights' ) {
                            $single_price['price_start'] = strtotime( "+1 day", $single_price['price_start'] );
                        }

                        // Check if dateranges overlap - http://stackoverflow.com/a/325964
                        if ( ( $start <= $single_price['price_end'] ) and ( $end >= $single_price['price_start'] ) ) {

                            $price_dates = array();

                            // Get all dates inside daterange
                            for ( $date = $single_price['price_start']; $date <= $single_price['price_end']; $date = strtotime( "+1 day", $date ) ) {
                                $price_dates[] = $date;
                            }

                            // Get overlapping dates
                            $overlap = array_intersect( $dates, $price_dates );

                            foreach ( $overlap as $date_overlapping ) {
                                $pricing[$date_overlapping] = $price;
                            }

                        } else {
                            continue;
                        }

                    }

                break;

            }
        }

    }

    if ( ! empty( $pricing ) ) {

        $new_price = 0;

        // Get number of "special" priced dates
        $count_prices = count( $pricing );

        // Apply "special" price to these days
        foreach ( $pricing as $date => $price ) {
            $new_price += $price;
        }

        // Get number of days without "special" price applied (regular price)
        $base_price_days = absint( $duration - $count_prices );

        // Apply normal price to these days
        if ( $base_price_days > 0 ) {

            $price = wceb_get_product_price( $_product );

            if ( $booking_duration != '1' ) {
                $price /= $booking_duration ;
            }

            $new_price += ( $price * $base_price_days );

        }

    }

    return $new_price;
}

/**
*
* Formats and translate a date, and tries to remove year if necessary
*
* @param str $date (yyyy-mm-dd)
* @param bool $year - Whether to keep year or not.
* @return str $date
*
**/
function ebp_format_date( $date, $year = true ) {

    if ( ! ebp_check_date_format( $date, 'yyyy-mm-dd' ) ) {
        return false;
    }

    $date_format = get_option( 'date_format' );

    // Try to remove year from date format if repeating every year
    if ( ! $year ) {
        $year_format = array('/Y', '/Y', 'Y-', '-Y', ',Y', ', Y', 'Y,', 'Y ,', '/y', '/y', 'y-', '-y', ',y', ', y', 'y,', 'y ,', ' y', 'y ', ' Y', 'Y ', 'Y', 'y', 'o'  );
        $date_format = str_replace( $year_format, '', $date_format );
    }

    $date = date_i18n( $date_format, strtotime( $date ) );

    return $date;
}

/**
*
* Checks date format
*
* @param str $date
* @param str $format - Date format (default: 'yyyy-mm-dd')
* @return bool
*
**/
function ebp_check_date_format( $date, $format = 'yyyy-mm-dd' ) {

    if ( $format = 'yyyy-mm-dd' && preg_match( '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/', $date ) ) {
        return true;
    } else if ( $format = 'yyyy/mm/dd' && preg_match( '/^([0-9]{4}\,[0-9]{2}\,[0-9]{2})$/', $date ) ) {
        return true;
    } else {
        return false;
    }

}

/**
*
* Gets booking price for one date only
*
* @param str - $new_price
* @param WC_Product - $product
* @param WC_Product - $_product - Product or variation
* @param array - $booking_data - Booking data, including booking duration
* @param str - $price_type - Price or regular price
* @return str - $new_price
*
**/
function ebp_get_one_date_booking_price( $new_price, $product, $_product, $booking_data, $price_type ) {

    $easy_booking_settings = get_option('easy_booking_settings');

    // Get product or variation ID
    if ( $_product->is_type('variation') ) {
        $id = is_callable( array( $_product, 'get_id' ) ) ? $_product->get_id() : $_product->variation_id;
    } else {
        $id = is_callable( array( $_product, 'get_id' ) ) ? $_product->get_id() : $_product->id;
    }

    // Get pricing data
    $product_prices = get_post_meta( $id, '_ebp_product_prices', true );

    if ( empty( $product_prices ) ) {
        return $new_price;
    }

    // Get selected date
    $selected_date = strtotime( $booking_data['start'] );

    // Get selected date's day
    $selected_day = date( 'w', $selected_date );

    if ( $selected_day === '0' ) {
        $selected_day = '7';
    }

    foreach ( $product_prices as $product_price ) {

        $price = isset( $product_price['sale_price'] ) ? $product_price['sale_price'] : $product_price['price'];

        if ( $price_type === 'regular_price' && isset( $product_price['sale_price'] ) ) {
            $price = $product_price['price'];
        }

        switch ( $product_price['type'] ) {

            case 'ebp_day' : // Weekday

                $weekday = $product_price['date'];

                if ( $weekday === $selected_day ) {
                    $new_price = $price;
                }

            break;

            case 'ebp_date' : // Date

                if ( $product_price['repeat'] === 'on' ) {
                    
                    $current_year = date( 'Y' );
                    $last_date = isset( $easy_booking_settings['easy_booking_last_available_date'] ) ? absint( $easy_booking_settings['easy_booking_last_available_date'] ) : '1825';
                    $max_year     = absint( $last_date / 365 );
                    $last_year    = strval( $current_year + $max_year );

                    $date = explode( '-', $product_price['date'] );
                    
                    // Get an array of all years from current year to limit year set in the settings
                    for ( $i = $current_year; $i <= $last_year; $i++ ) {

                        $next_date = strtotime( $i . '-' . $date[1] . '-' . $date[2] );

                        // Check if date is equal to selected date
                        if ( $next_date === $selected_date ) {
                            $new_price = $price;
                        }

                    }

                } else {

                    $date = strtotime( $product_price['date'] );

                    // Check if date is equal to selected date
                    if ( $date === $selected_date ) {
                        $new_price = $price;
                    }

                }
                

            break;

            case 'ebp_daterange' : // Daterange

                $prices = array();
                if ( $product_price['repeat'] === 'on' ) {

                    // Get selected date year
                    $selected_year = explode( '-', $booking_data['start'] );
                    $start_year = $selected_year[0];

                    // Get special price date without year
                    $price_start_date = explode( '-', $product_price['date'][0] );
                    $price_end_date   = explode( '-', $product_price['date'][1] );

                    $end_year = $start_year;
                    
                    // If year are different
                    if ( $price_start_date[0] !== $price_end_date[0] ) {
                        $difference = $price_end_date[0] - $price_start_date[0];
                        $start_year -= $difference;
                    }

                    $prices[] = array(
                        'price_start' => strtotime( $start_year . '-' . $price_start_date[1] . '-' . $price_start_date[2] ),
                        'price_end'   => strtotime( $end_year . '-' . $price_end_date[1] . '-' . $price_end_date[2] )
                    );

                } else {

                    $prices[] = array(
                        'price_start' => strtotime( $product_price['date'][0] ),
                        'price_end'   => strtotime( $product_price['date'][1] )
                    );

                }

                foreach ( $prices as $single_price ) {

                    // Check if selected date is inside daterange
                    if ( ( $selected_date <= $single_price['price_end'] ) and ( $selected_date >= $single_price['price_start'] ) ) {

                        $new_price = $price;

                    } else {
                        continue;
                    }

                }

            break;

        }

    }

    return $new_price;
    
}

/**
*
* Gets html to display prices on the product page
*
* @param int - $product_id
* @return str - $output
*
**/
function ebp_get_display_booking_prices( $product_id ) {

    $output = '';

    $product = wc_get_product( $product_id );

    // Get pricing data
    $product_prices = get_post_meta( $product_id, '_ebp_product_prices', true );
    $booking_duration = wceb_get_product_custom_booking_duration( $product );

    $plugin_settings  = get_option('easy_booking_settings');
    $calc_mode        = $plugin_settings['easy_booking_calc_mode'];

    $price_html = wceb_get_price_html( $product );

    if ( ! empty( $product_prices ) ) {

        foreach ( $product_prices as $product_price ) {

            $output .= apply_filters(
                'easy_booking_pricing_single_price',
                ebp_get_display_booking_price( $product_price, $booking_duration, $calc_mode, $price_html, $product ),
                $product_id,
                $product_price
            );

        }

    }

    return wp_kses_post( $output );
}

/**
*
* Gets html to display a single price
*
* @param array - $product_price
* @param int - $booking_duration
* @param str - calc_mode ("Days" or "Nights")
* @param str - $price_html ("/ day", "/ week", etc.)
* @return str - $display_price
*
**/
function ebp_get_display_booking_price( $product_price, $booking_duration, $calc_mode, $price_html, $product ) {

    $display_price = '';

    // If price is per week/custom duration, divide it by booking duration
    if (
        $product_price['type'] !== 'ebp_daterange'
        &&
        $product_price['unit'] === 'ebp_multi'
        &&
        $booking_duration != '1'
    ) {

        $product_price['price'] /= $booking_duration;

        if ( isset( $product_price['sale_price'] ) ) {
            $product_price['sale_price'] /= $booking_duration;
        }

    }

    if ( isset( $product_price['sale_price'] ) ) {
        $price = '<del>' . ( ( is_numeric( $product_price['price'] ) ) ? wc_price( $product_price['price'] ) : esc_html( $product_price['price'] ) ) . '</del> ' . ( ( is_numeric( $product_price['sale_price'] ) ) ? wc_price( $product_price['sale_price'] ) : esc_html( $product_price['sale_price'] ) );
    } else {
        $price = wc_price( $product_price['price'] );
    }

    switch ( $product_price['type'] ) {

        case 'ebp_day' :

            switch ( $product_price['date'] ) {

                case '1' :
                    $day = __( 'Monday', 'easy_booking_pricing' );
                break;
                case '2' :
                    $day = __( 'Tuesday', 'easy_booking_pricing' );
                break;
                case '3' :
                    $day = __( 'Wednesday', 'easy_booking_pricing' );
                break;
                case '4' :
                    $day = __( 'Thursday', 'easy_booking_pricing' );
                break;
                case '5' :
                    $day = __( 'Friday', 'easy_booking_pricing' );
                break;
                case '6' :
                    $day = __( 'Saturday', 'easy_booking_pricing' );
                break;
                case '7' :
                    $day = __( 'Sunday', 'easy_booking_pricing' );
                break;

            }

            $display_price = sprintf(
                esc_html__( '%s: %s', 'easy_booking_pricing' ),
                $day,
                $price . '<br />'
            );
            

        break;

        case 'ebp_date' :

            $year = true;

            // Try to remove year from date format if repeating every year
            if ( $product_price['repeat'] === 'on' ) {
                $year = false;
            }

            $date = ebp_format_date( $product_price['date'], $year );

            $display_price = sprintf(
                esc_html__( '%s: %s', 'easy_booking_pricing' ),
                $date,
                $price . '<br />'
            );

        break;

        case 'ebp_daterange' :

            $year = true;

            // Try to remove year from date format if repeating every year
            if ( $product_price['repeat'] === 'on' ) {
                $year = false;
            }

            $start_date = ebp_format_date( $product_price['date'][0], $year );
            $end_date   = ebp_format_date( $product_price['date'][1], $year );

            if ( $product_price['unit'] === 'ebp_single' && $booking_duration != '1' ) {
                $price_html = ( $calc_mode === 'nights' ) ? __(' / night', 'easy_booking') : __(' / day', 'easy_booking');
                $price_html = apply_filters( 'easy_booking_pricing_get_daily_price_html', $price_html, $product, $booking_duration, $product_price );
            }

            $display_price = sprintf(
                esc_html__( '%s-%s: %s', 'easy_booking_pricing' ),
                $start_date, $end_date, $price . $price_html . '<br />'
            );

        break;

    }
    
    return $display_price;

}