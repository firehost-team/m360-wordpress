<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

add_settings_section(
	'ebp_multisite_settings',
	__( 'Easy Booking: Pricing Settings', 'easy_booking_pricing' ),
	array( $this, 'ebp_multisite_settings' ),
	'easy_booking_global_settings'
);

add_settings_field(
	'ebp_license_key',
	__( 'License Key', 'easy_booking_pricing' ),
	array( $this, 'ebp_multisite_license_key' ),
	'easy_booking_global_settings',
	'ebp_multisite_settings'
);