<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

register_setting(
	'ebp_settings',
	'ebp_settings', 
	array( $this, 'sanitize_values' )
);

add_settings_section(
	'ebp_main_settings',
	__( 'Settings', 'easy_booking_pricing' ),
	array( $this, 'ebp_section_general' ),
	'ebp_settings'
);

// If multisite, save the license key on the network, not the sites.
if ( ! is_multisite() ) {

	add_settings_field(
		'ebp_license_key',
		__( 'License Key', 'easy_booking_pricing' ),
		array( $this, 'ebp_license_key' ),
		'ebp_settings',
		'ebp_main_settings'
	);

}

add_settings_field(
	'ebp_display_prices',
	__( 'Display prices?', 'easy_booking_pricing' ),
	array( $this, 'ebp_display_prices' ),
	'ebp_settings',
	'ebp_main_settings'
);