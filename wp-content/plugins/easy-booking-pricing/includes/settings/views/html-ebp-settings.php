<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div class="wrap">

	<h2><?php _e( 'Easy Booking: Pricing Settings', 'easy_booking_pricing' ); ?></h2>

	<form method="post" id="ebp-settings" action="options.php">

		<?php settings_fields( 'ebp_settings' ); ?>
		<?php do_settings_sections( 'ebp_settings' ); ?>
		 
		<?php submit_button(); ?>

	</form>

</div>