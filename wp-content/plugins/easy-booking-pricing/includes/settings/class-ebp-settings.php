<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Ebp_Settings' ) ) :

class Ebp_Settings {

	private $settings;
	private $global_settings;

	public function __construct() {

		// get plugin options values
		$this->settings = get_option( 'ebp_settings' );

		// initialize options the first time
		if ( ! $this->settings ) {
		
		    $this->settings = array(
				'ebp_license_key'    => '',
				'ebp_display_prices' => ''
		    );

		    add_option( 'ebp_settings', $this->settings );

		}

		if ( is_multisite() ) {
			
			$this->global_settings = get_option( 'easy_booking_global_settings' );

			if ( ! isset( $this->global_settings['ebp_license_key'] ) ) {
				$this->global_settings['ebp_license_key'] = '';
			}

			update_option( 'easy_booking_global_settings', $this->global_settings );

		}

		// Add settings page
		add_action( 'admin_menu', array( $this, 'ebp_add_setting_page' ), 20 );

		// Init plugin settings
		add_action( 'admin_init', array( $this, 'ebp_settings_init' ) );

		$license_set = ! isset( $this->settings['ebp_license_key'] ) || empty( $this->settings['ebp_license_key'] ) ? false : true;

		if ( get_option( 'easy_booking_display_notice_ebp_license' ) !== '1' && ! $license_set ) {
			update_option( 'easy_booking_display_notice_ebp_license', 0 );
		} else {
			update_option( 'easy_booking_display_notice_ebp_license', '1' );
		}

	}

	/**
	 *
	 * Plugin settings page
	 *
	 */
	public function ebp_add_setting_page() {
		$option_page = add_submenu_page(
			'easy-booking',
			'Pricing',
			'Pricing',
			apply_filters( 'easy_booking_settings_capability', 'manage_options' ),
			'easy-booking-pricing',
			array( $this, 'ebp_option_page' )
		);

		// Load scripts on this page only
		add_action( 'admin_print_scripts-'. $option_page, array( $this, 'ebp_load_scripts' ) );
	}

	/**
	 *
	 * Load scripts and styles
	 *
	 */
	public function ebp_load_scripts() {
	}

	/**
	 *
	 * Init settings
	 *
	 */
	public function ebp_settings_init() {

		include_once( 'includes/ebp-settings.php' );

		// Multisite settings
		if ( is_multisite() ) {
			include_once( 'includes/ebp-network-settings.php' );
		}

	}

	/**
	 *
	 * Display settings page content
	 *
	 */
	public function ebp_option_page() {
		include_once( 'views/html-ebp-settings.php' );
	}

	/**
	 *
	 * Main settings section
	 *
	 */
	public function ebp_section_general() {
		echo '';
	}

	/**
	 *
	 * License key field
	 *
	 */
	public function ebp_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'ebp_license_key',
			'name'              => 'ebp_settings[ebp_license_key]',
			'value'             => $this->settings['ebp_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_pricing' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	public function ebp_display_prices() {
		wceb_settings_checkbox( array(
			'id'          => 'ebp_display_prices',
			'name'        => 'ebp_settings[ebp_display_prices]',
			'description' => __( 'Display prices on the product page.', 'easy_booking_pricing' ),
			'value'       => isset( $this->settings['ebp_display_prices'] ) ? $this->settings['ebp_display_prices'] : '',
			'cbvalue'     => 'on'
		));
	}

	/**
	 *
	 * Multisite settings section
	 *
	 */
	public function ebp_multisite_settings() {
		do_settings_sections('ebp_multisite_settings');
	}

	/**
	 *
	 * Multisite license key field
	 *
	 */
	public function ebp_multisite_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'ebp_license_key',
			'name'              => 'easy_booking_global_settings[ebp_license_key]',
			'value'             => $this->global_settings['ebp_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_pricing' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	/**
	 *
	 * Sanitize options
	 *
	 */
	public function sanitize_values( $settings ) {

		// Sanitize other settings
		foreach ( $settings as $key => $value ) {
			$settings[$key] = esc_html( $value );
		}

		return $settings;

	}

}

return new Ebp_Settings();

endif;