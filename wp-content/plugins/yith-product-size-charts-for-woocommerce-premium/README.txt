=== YITH Product Size Charts for WooCommerce ===

== Changelog ==

= Version 1.1.0 - Released: 20 Mar 2017 =

* New - support to WooCommerce 3.0.0-RC1
* Fix - number of size charts shown in shortcode select
* Fix - apostrophe issue
* Tweak - improved size chart button style
* Tweak - allow opening popup with different styles and effects through href
* Tweak - added possibility to set popup style and effect in link
* Dev - replaced chosen with select2 (since WooCommerce 3.0.0)

= Version 1.0.10 =

* Fix - number of size charts showed in quick-edit/bulk-edit

= Version 1.0.9 =

* New - possibility to set the text for Size Chart button

= Version 1.0.8 =

* Fix - js script bug on plugin settings page
* Fix - memory bug

= Version 1.0.7 =

* New - support to WooCommerce 2.5 BETA 3
* Tweak - fixed minor bug

= Version 1.0.6 =

* Tweak - fixed compatibility with YITH WooCommerce Multi Vendor

= Version 1.0.5 =

* Tweak - fixed compatibility with YITH WooCommerce Multi Vendor

= Version 1.0.4 =

* New - compatibility with YITH WooCommerce Multi Vendor 1.7.1
* New - possibility to choose Size Chart tab position

= Version 1.0.3 =

* New - support for AJAX page loading

= Version 1.0.2 =

* New - Italian Language
* Fix - minor bug

= Version 1.0.1 =

* New - Responsive size charts

= Version 1.0.0 =

* Initial release