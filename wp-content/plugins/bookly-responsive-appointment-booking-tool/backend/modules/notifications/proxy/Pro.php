<?php
namespace Bookly\Backend\Modules\Notifications\Proxy;

use Bookly\Lib;
use Bookly\Backend\Modules\Notifications\Forms;

/**
 * Class Pro
 * @package Bookly\Backend\Modules\Notifications\Proxy
 *
 * @method static void renderCustomEmailNotifications( Forms\Notifications $form ) Render custom email notifications.
 */
abstract class Pro extends Lib\Base\Proxy
{
}