<?php
namespace Bookly\Backend\Modules\Appearance\Proxy;

use Bookly\Lib;

/**
 * Class ChainAppointments
 * @package Bookly\Backend\Modules\Frontend\Proxy
 *
 * @method static void renderChain() render a 'plus' for chain appointments on service step
 */
abstract class ChainAppointments extends Lib\Base\Proxy
{
}