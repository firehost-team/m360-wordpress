<?php
namespace Bookly\Backend\Components\Dialogs\Customer\Proxy;

use Bookly\Lib;

/**
 * @since Bookly 16.2
 * @deprecated
 * Proxy file was moved
 */
abstract class CustomerGroups extends Lib\Base\Proxy
{

}