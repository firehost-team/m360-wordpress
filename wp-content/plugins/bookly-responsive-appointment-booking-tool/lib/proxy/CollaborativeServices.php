<?php
namespace Bookly\Lib\Proxy;

use Bookly\Lib;

/**
 * Class CollaborativeServices
 * @package Bookly\Lib\Proxy
 *
 * @method static void cancelAppointment( Lib\Entities\CustomerAppointment $customer_appointment ) Cancel collaborative appointment.
 */
abstract class CollaborativeServices extends Lib\Base\Proxy
{

}