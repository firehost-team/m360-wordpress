<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
define( 'M360_STADION_BILDER_PLUGIN_NAME',' Stadion Bilder');
define( 'M360_STADION_BILDER_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'M360_STADION_BILDER_PLUGIN_URL', plugin_dir_url(__FILE__));

define( 'STADION_TEXT_API_URL', 'http://api.stadion.no/_dbservicephp/_api/tekster/get.php');
define( 'STADION_TEXT_API_MEDIA_ID_URL', 'http://api.stadion.no/_dbservicephp/_api/media/getmediaid.php');
define( 'STADION_TEXT_API_MEDIA_URL', 'http://api.stadion.no/_dbservicephp/_api/media/get.php');

/**
 * Plugin Name:         Stadion Bilder
 * Plugin URI:          https://m360.no
 * Description:         Tilgang til tekst og bilde på Stadion API
 * Author:              M360 Digitalbyrå
 * Author URI:          https://m360.no/
 *
 * Version:             1.0.1
 * Tested up to:        4.9.4
 *
 * WC tested up to: 3.2.6
 *
 * Text Domain:         m360-stadion-bilder
 * Domain Path:         /languages
 *
 * @package             WooCommerce
 * @author              M360 Digitalbyrå
 */
class M360_Stadion_Bilder {
    const VERSION = '1.0.0';
    const TEXT_DOMAIN = 'm360-stadion-bilder';

    static function init() {
        if ( ! class_exists( 'WooCommerce' ) ) {
            return;
        }

        load_plugin_textdomain(self::TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

        add_action( 'admin_enqueue_scripts', 'M360_Stadion_Bilder::stadion_enqueue_admin_scripts');

        require_once 'classes/function.php';
        require_once 'classes/class-settings.php';

        $settings = new M360_Stadion_Bilder_Settings();
        $settings->draw_the_settings();

        //add_filter('wp_get_attachment_url', 'M360_Stadion_Bilder::wp_stadion_bilder_get_attachment_url'); // change image url
        //add_filter( 'woocommerce_locate_template','M360_Stadion_Bilder::stadion_plugin_template', 1, 3 );

        // add sync button to main product
        add_action('add_meta_boxes', 'M360_Stadion_Bilder::stadion_sync_meta_box',30);

        // add sync button to variations
        add_action( 'woocommerce_variation_options', 'M360_Stadion_Bilder::stadion_sync_button', 10, 3 );

        // bulk action
        add_filter( 'bulk_actions-edit-product', 'M360_Stadion_Bilder::register_my_bulk_actions',10,1);
        add_filter( 'handle_bulk_actions-edit-product', 'M360_Stadion_Bilder::stadion_bulk_sync', 10, 3 );

        self::filter_description();
    }

    static function stadion_bulk_sync( $redirect_to, $action, $post_ids ) {
        //StadionLogger(__FUNCTION__.' '.$redirect_to.' action: '.$action.' post_ids: '.print_r($post_ids,true));
        if($action == 'bulk_image_sync_with_stadion'){
            foreach ($post_ids as $post_id){
                $main_product = wc_get_product($post_id);
                $main_product_media_id = CallStadionAPIGetMediaID(stadion_gtin($post_id));
                if($main_product_media_id)add_image_to_media_library($post_id,$main_product_media_id);
                foreach ($main_product->get_children() as $child_id){
                    //$child_product = wc_get_product($child_id);
                    $child_media_id = CallStadionAPIGetMediaID(stadion_gtin($child_id));
                    if($child_media_id)add_image_to_media_library($child_id,$child_media_id);
                }
            }
        }
        return $redirect_to;
    }

    static function register_my_bulk_actions( $bulk_actions ) {
        $bulk_actions['bulk_image_sync_with_stadion'] = __( 'Sync with Stadion Images',self::TEXT_DOMAIN );
        return $bulk_actions;
    }

    static function stadion_sync_button($loop, $variation_data, $variation){
        $product = wc_get_product( $variation->ID );
        $media_ids = CallStadionAPIGetMediaID(stadion_gtin($product->get_id()));
        if($product && $media_ids){
            foreach ($media_ids as $media_id){
                echo stadion_get_product_variation_img_html($product,$variation->ID,$media_id);
                echo $html;
            }

        }
    }
    static function sync_main_product_image(){
        if(isset($_POST['post_id']) && $_POST['media_id']){
            $post_id = $_POST['post_id'];
            $img = $_POST['media_id'];
            $result = add_image_to_media_library($post_id,$img);
            if($result > 0){
                wp_send_json_success( array('message'=>'Product image synced successfully') );
            }else{
                wp_send_json_error(array('message'=>'Faild to attach the image'));
            }
        }else{
            wp_send_json_error(array('message'=>'No post_id or no product_img'));
        }
    }
    static function stadion_sync_meta_box(){
        add_meta_box("stadion-main-meta-box", __('Stadion API',self::TEXT_DOMAIN), "M360_Stadion_Bilder::stadion_meta_box_for_main_product_html", "product", "side", "low");
    }
    static function stadion_meta_box_for_main_product_html(){
        global $post_id;
        $product = wc_get_product($post_id);
        $media_ids = CallStadionAPIGetMediaID(stadion_gtin($post_id));
        $no_image_found = '<p>'.stadion_translate('No image found for this product').'</p>';
        if($product && $media_ids){
            foreach ($media_ids as $media_id){
                echo stadion_get_product_img_html($img_raw,$product,$post_id,$media_id);
            }

        }else{
            echo $no_image_found;
        }
    }
    /**
     * @param $template
     * @param $template_name
     * @param $template_path
     * @return string
     */
    static function stadion_plugin_template($template, $template_name, $template_path ) {
        global $woocommerce;
        $_template = $template;
        if ( ! $template_path )
            $template_path = $woocommerce->template_url;

        $plugin_path  = untrailingslashit( plugin_dir_path( __FILE__ ) )  . '/templates/woocommerce/';

        // Look within passed path within the theme - this is priority
        $template = locate_template(
            array(
                $template_path . $template_name,
                $template_name
            )
        );

        if( ! $template && file_exists( $plugin_path . $template_name ) )
            $template = $plugin_path . $template_name;

        if ( ! $template )
            $template = $_template;

        return $template;
    }

    static function filter_description(){
        $saved = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_text','no_description');
        if($saved != 'no_description'){
            if($saved == 'short_description' || $saved == 'short_description_if_empty'){
                add_filter( 'woocommerce_short_description', 'M360_Stadion_Bilder::wp_stadion_short_description' ); // change product excerpt
            }else if($saved == 'content' || $saved == 'content_if_empty'){
                add_filter( 'the_content', 'M360_Stadion_Bilder::wp_stadion_content' ); // change product content
            }else{
                add_filter( 'the_content', 'M360_Stadion_Bilder::wp_stadion_content'); // change product content
                add_filter( 'woocommerce_short_description', 'M360_Stadion_Bilder::wp_stadion_short_description' ); // change product excerpt
            }

        }
    }

    /**
     * @return bool
     */
    static function if_text_is_empty(){
        $saved = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_text','no_description');
        $saved_comp = explode('_',$saved);
        $if_empty = (end($saved_comp)=='empty')?true:false;
        return $if_empty;
    }

    /**
     * @param $product
     * @param $org_text
     * @return string
     */
    static function get_text_for_prodcut_from_api($product, $org_text){
        $text = '';
        $result = CallStadionAPIForText(stadion_gtin($product->get_id()));

        if($result->status == 'success'){
            if(count($result->result)>0){
                $text = '<ul>';
                foreach ($result->result as $result){
                    $text .= '<li>'.$result->Tekst_Stadion.'</li>';
                }
                $text .= '</ul>';
                return $text;
            }
        }
        return (strlen($text)>0)?$text:$org_text;
    }

    /**
     * @param $content
     * @return string
     */
    static function wp_stadion_content($content ) {
        global $product;
        if($product){
            if(self::if_text_is_empty() && strlen($content)>0){
                return $content;
            }
            return M360_Stadion_Bilder::get_text_for_prodcut_from_api($product,$content);
        }

        return $content;
    }


    /**
     * @param $excerpt
     * @return string
     */
    static function wp_stadion_short_description($excerpt ) {
        global $product;
        if($product){
            if(self::if_text_is_empty() && strlen($excerpt)>0){
                return $excerpt;
            }
            return M360_Stadion_Bilder::get_text_for_prodcut_from_api($product,$excerpt);
        }
        return $excerpt;
    }

    /**
     * @param $url
     * @return string
     */
    static function wp_stadion_bilder_get_attachment_url($url) {
        global $product;
        if($product){
            //return 'https://m360.no/wp-content/uploads/2018/01/Serviceavtale-standard.jpg';
            $media_ids = CallStadionAPIGetMediaID(stadion_gtin($product->get_id()));
            if($media_ids){
                return CallStadionAPIForMedia($url,$media_ids[0]);
            }
        }
        return $url;
    }

    /**
     *
     */
    static function stadion_enqueue_admin_scripts(){
        //wp_enqueue_script( 'stadion-bilder-admin-script', M360_STADION_BILDER_PLUGIN_URL . 'assets/js/admin_script.js', array( 'jquery' ));

        $js_file = M360_STADION_BILDER_PLUGIN_URL.'assets/js/admin_script.js';
        $js_file_ver  = date("ymd-Gis", filemtime( M360_STADION_BILDER_PLUGIN_PATH.'assets/js/admin_script.js'));

        wp_register_script('stadion-bilder-admin-script',$js_file,array('jquery'),$js_file_ver,true);

        wp_enqueue_script('stadion-bilder-admin-script');
        wp_localize_script( 'stadion-bilder-admin-script', 'stadion', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )) );

        $css_file = M360_STADION_BILDER_PLUGIN_URL.'assets/css/admin_style.css';
        $css_file_ver  = date("ymd-Gis", filemtime( M360_STADION_BILDER_PLUGIN_PATH.'assets/css/admin_style.css'));
        wp_enqueue_style( 'stadion-bilder-admin-style',$css_file ,false,$css_file_ver);
    }
}

add_action( 'plugins_loaded', 'M360_Stadion_Bilder::init' );
add_action( 'wp_ajax_sync_main_product_image', 'M360_Stadion_Bilder::sync_main_product_image' );
