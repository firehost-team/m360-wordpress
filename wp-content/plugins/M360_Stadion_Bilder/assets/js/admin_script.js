/**
 * Created by ibrahimqraiqe on 08.03.2018.
 */
;(function ($, window, document, undefined) {

})(jQuery, window, document);
function sync_img(d) {
    var post_id = d.getAttribute("data-id");
    var stadion_media_id = d.getAttribute("data-mediaid");
    $.ajax({
        type: "POST",
        url: stadion.ajaxurl,
        data: {
            'action':'sync_main_product_image',
            "post_id": post_id,
            "media_id":stadion_media_id,
        },
        success: function(result) {
            alert(result.data.message);
            window.location.reload();
        },
        error: function(result) {
            alert(result.data.message);
        }
    });
}

jQuery( document ).ready( function ($) {
    var doTheSyncing = function doTheSyncing(page) {
        var posts_per_page  = ( $('#stadion_products_per_page').val() == '' ) ? '10' :  $('#stadion_products_per_page').val();

        $.ajax(
            stadion.ajaxurl, {
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'start_syncing_all_products',
                    paged: parseInt(page)+1,
                    posts_per_page: parseInt(posts_per_page)
                },
                beforeSend: function(){
                    $( '.loading_container' ).show();
                },
                success: function (response) {
                    $( ".loading" ).empty();
                    $( ".loading" ).append('<p>'+response.data.append+'</p>');
                    if(response.data.message == 'continue'){
                        doTheSyncing(response.data.page);
                    }else{
                        $('#stadion_sync_all_current_page').val(0);
                        $( '.loading_container' ).hide();
                    }
                },
                error: function ( response ) {
                    console.log( response );
                    $( '.loading_container' ).hide();
                }
            }
        );
    };

    $( document ).on('click', '#stadion_start_sync_all_products', function(event) {
        event.preventDefault();
        //doTheExporting();
        if(confirm('Er du sikkert at du vil synke alle varer med Stadion API? , den kan ta tid')){
            doTheSyncing(0);
        }
    });
});