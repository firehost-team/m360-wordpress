<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
require_once 'class-setup-view.php';
require_once 'class-syncing-view.php';
require_once 'class-debugging-view.php';

class M360_Stadion_Bilder_Settings{
    public $syncing_tab;
    public function draw_the_settings() {
        if ( ! class_exists( 'WooCommerce' ) ) {
            return;
        }
        add_action( 'admin_menu', array( $this, 'add_stadion_admin_page' ) );
        add_action( 'admin_init', array( $this, 'init_settings' ) );
    }

    public function init_settings(){
        $setup_tab = new M360_Stadion_Bilder_Settings_Setup_Tab();
        $setup_tab->init_fields();

        $this->syncing_tab = new M360_Stadion_Bilder_Settings_Syncing_Tab();
        add_action( 'wp_ajax_start_syncing_all_products', array($this->syncing_tab,'start_syncing_all_products'));


        $debugging_tab = new M360_Stadion_Bilder_Settings_Debugging_Tab();
        $debugging_tab->init_fields();
    }
    public function add_stadion_admin_page() {

        add_menu_page(
            'Stadion Bilder',
            'Stadion Bilder',
            'manage_options',
            'stadion_bilder_page',
            array( $this, 'create_stadion_bilder_setting_page' ) ,
            M360_STADION_BILDER_PLUGIN_URL.'assets/images/stadion_logo.png',
            31
        );
    }

    public function create_stadion_bilder_setting_page(){
        $tab = isset( $_GET['tab'])?$_GET['tab']:'setup';

        print '<div class="wrap">';
        print '<h2><img src="'.M360_STADION_BILDER_PLUGIN_URL.'assets/images/stadionlogo.png" class="stadion_admin_logo"/>'.__('Bilder Api',M360_Stadion_Bilder::TEXT_DOMAIN).'</h2>';
        require_once 'class-settings-main-view.php';
        new M360_Stadion_Bilder_Settings_Main_View();
        print '<form method="post" action="options.php">';
        settings_fields( 'stadion_bilder_options_' . $tab . '_group' );
        do_settings_sections( 'stadion_bilder_options_page_' . $tab );
        if($tab == 'syncing'){
            $this->syncing_tab->draw_the_page();
        }else{
            submit_button();
        }
        print '</form></div>';


    }
}