<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
if(!function_exists('stadion_bilder_sanitise')){
    function stadion_bilder_sanitise( $input = '' ) {
        return $input;
    }
}

if(!function_exists('stadion_field_seperator')){
    function stadion_field_seperator(){
        echo '<div class="stadion_bilder_seperator"></div>';
    }
}

if(!function_exists('stadion_bilder_get_settings')){
    function stadion_bilder_get_settings($page,$key,$default = ''){
        $option = get_option($page);
        $value = isset($option[$key])?$option[$key]:$default;
        return $value;
    }
}
if(!function_exists('get_setting_input_html')){
    function get_setting_input_html($type,$page,$key,$default = ''){
        $saved = stadion_bilder_get_settings($page,$key,$default);
        printf('<input type="%s" id="stadion_bilder_security_key" name="stadion_bilder_options_setup[stadion_bilder_security_key]" value="%s"/>',$type, $saved
        );
    }
}

if(!function_exists('CallStadionAPIGetMediaID')){
    function CallStadionAPIGetMediaID($streke_kode){
        StadionLogger('streke_kode:  '.print_r($streke_kode,true));

        $secret_key = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_security_key','');
        $para = "?gt_in={$streke_kode}&from=supplering&security_key={$secret_key}";
        $url = STADION_TEXT_API_MEDIA_ID_URL.$para;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        if($result->status == 'success'){
            StadionLogger('ids: '.print_r($result,true));
            return $result->result;
        }else{
            return false;
        }
    }
}


if(!function_exists('CallStadionAPIForText')){
    function CallStadionAPIForText($streke_kode){
        $secret_key = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_security_key','');
        $para = "?gt_in={$streke_kode}&security_key={$secret_key}";
        $url = STADION_TEXT_API_URL.$para;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}

if(!function_exists('CallStadionAPIForMedia')){
    function CallStadionAPIForMedia($media_id,$just_url = false){
        $secret_key = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_security_key','');
        $para = "?mediaid={$media_id}&width=900&security_key={$secret_key}";
        $url = STADION_TEXT_API_MEDIA_URL.$para;
        if($just_url)return $url;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        $picture = curl_exec($ch);
        curl_close($ch);

        if($picture){
            return $picture;
        }

        return false;
    }
}

if( ! function_exists( 'stadion_translate' ) ) {
    function stadion_translate($Text){
        return __($Text,M360_Stadion_Bilder::TEXT_DOMAIN);
    }
}
if(!function_exists('stadion_temp_upload')){
    function stadion_temp_upload() {
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/stadion_temp_upload/';
        if (! is_dir($upload_dir)) {
            mkdir( $upload_dir, 0700 );
        }
        return $upload_dir;
    }
}


if(!function_exists('add_image_to_media_library')){
    function add_image_to_media_library($post_id,$media_id){
        $img_url = CallStadionAPIForMedia($media_id,true);
        $product = wc_get_product($post_id);

        $post = get_post($post_id);
        $file_name = $post->post_name.'.png';

        if( !class_exists( 'WP_Http' ) )
            include_once( ABSPATH . WPINC . '/class-http.php' );

        $http = new WP_Http();
        $response = $http->request( $img_url );
        if( $response['response']['code'] != 200 ) {
            return false;
        }
        //$tmp_file = stadion_temp_upload().$file_name;
        //file_put_contents($tmp_file, file_get_contents($img_url));
        $upload = wp_upload_bits($file_name, null, $response['body'] );
        if( !empty( $upload['error'] ) ) {
            return false;
        }

        $file_path = $upload['file'];
        StadionLogger('2: '.$file_path);

        $file_type = wp_check_filetype( $file_name, null );
        $attachment_title = $product->get_name();
        $wp_upload_dir = wp_upload_dir();

        $post_info = array(
            'guid'				=> $wp_upload_dir['url'] . '/' . $file_name,
            'post_mime_type'	=> $file_type['type'],
            'post_title'		=> $attachment_title,
            'post_content'		=> '',
            'post_status'		=> 'inherit',
        );

        $attach_id = wp_insert_attachment( $post_info, $file_path, $post_id );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

        wp_update_attachment_metadata( $attach_id,  $attach_data );
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
        wc_delete_product_transients( $post_id );

        //unlink($tmp_file);
        return $attach_id;
    }
}

if(!function_exists('stadion_get_product_img_html')){
    function stadion_get_product_img_html($img_raw,$product,$post_id,$media_id){
        $img = CallStadionAPIForMedia($media_id,true);
        //$img = 'data:image/jpg;base64,'.base64_encode($img_raw);
        $html = '<p class="hide-if-no-js" style=" text-align: center ;">';
        $html .= '<span><img src="'.$img.'" class="stadion_admin_main_product_image" alt="'.$product->get_name().'" /></span>';
        //$html .= '<input name="stadion_sync_main" type="submit" class="button button-primary button-large" id="stadion_sync_main" value="Sync with Stadion">';
        $html .= '<span><a href="#" class="button button-primary button-large" data-id="'.$post_id.'" data-mediaid="'.$media_id.'" onclick="sync_img(this);">';
        $html .= stadion_translate('Set as main product image').'</a></span></p>';
        echo $html;
    }
}


if(!function_exists('stadion_get_product_variation_img_html')){
    function stadion_get_product_variation_img_html($product,$post_id,$media_id){
        $img_url = CallStadionAPIForMedia($media_id,true);
        $html = '<div>';
        $html .= '<p><a href="'.$img_url.'" target="_blank">'.stadion_translate('Show Stadion image').'</a></p>';
        $html .= '<p><a href="#" class="button button-primary button-large" data-id="'.$post_id.'" data-mediaid="'.$media_id.'" onclick="sync_img(this);">';
        $html .= stadion_translate('Sync with stadion').'</a></p>';
        $html .= '</div>';
        echo $html;
    }
}
if(!function_exists('stadion_random_product_id')){
    function stadion_random_product_id(){
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => 1,
            'paged' => 1,
        );

        $loop = new WP_Query( $args );
        $post_id = 0;
        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
                $post_id = $loop->post->ID;
            endwhile;
        }
        wp_reset_postdata();
        return $post_id;
    }
}
if(!function_exists('stadion_get_meta_values')){
    function stadion_get_meta_values() {
        $myvals = get_post_meta(stadion_random_product_id());
        $keys = array();
        foreach($myvals as $key=>$val) {
            $name = str_replace('_',' ',$key);
           $keys[$key] = trim($name);
        }
        return $keys;

    }
}

if(!function_exists('stadion_gtin')){
    function stadion_gtin($post_id){
        $first = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_gtin_attribute','m360_pck_eans');
        $value = get_post_meta($post_id,$first,true);
        if(!$value){
            $second = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_gtin_second_attribute','_sku');
            $value = get_post_meta($post_id,$second,true);
        }
        StadionLogger(__FUNCTION__.' '.$value);

        return $value;
    }
}

if( ! function_exists( 'StadionLogger' ) ) {
    function StadionLogger($Text){
        $loggingOptions = get_option( 'stadion_bilder_options_debugging' );
        if ($loggingOptions['stadion_bilder_debugging_enabled'] != 'true') return;

        $File = fopen(WP_CONTENT_DIR . '/Stadion_Log.log', 'a');
        fwrite($File, date('d-m-y H:i:s') . substr((string)microtime(), 1, 6) . ' - ' . $Text . "\n");
        fclose($File);
    }
}