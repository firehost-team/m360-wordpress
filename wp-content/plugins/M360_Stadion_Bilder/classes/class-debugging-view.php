<?php
/**
 * Created by PhpStorm.
 * User: ibrahimqraiqe
 * Date: 09.03.2018
 * Time: 08.41
 */

if( !defined( 'ABSPATH' ) ) {
    exit;
}

class M360_Stadion_Bilder_Settings_Debugging_Tab{

    public function init_fields(){
        register_setting(
            'stadion_bilder_options_debugging_group',
            'stadion_bilder_options_debugging',
            stadion_bilder_sanitise()
        );
        add_settings_section(
            'm360_stadion_bilder_section_debugging',
            __(M360_STADION_BILDER_PLUGIN_NAME.' v.: ',M360_Stadion_Bilder::TEXT_DOMAIN).M360_Stadion_Bilder::VERSION,
            array( $this, 'm360_stadion_bilder_section_setup_html' ),
            'stadion_bilder_options_page_debugging'
        );

        add_settings_field(
            'stadion_bilder_debugging_enabled',
            stadion_translate('Enable Debugging'),
            array( $this, 'stadion_bilder_debugging_enabled_html' ),
            'stadion_bilder_options_page_debugging',
            'm360_stadion_bilder_section_debugging'
        );


    }

    public function stadion_bilder_debugging_enabled_html() {
        $loggingOptions = get_option( 'stadion_bilder_options_debugging' );

        printf (
            'Yes <input type="radio" id="stadion_bilder_debugging_enabled" name="stadion_bilder_options_debugging[stadion_bilder_debugging_enabled]" value="true" %s/>',
            isset( $loggingOptions[ 'stadion_bilder_debugging_enabled' ] ) &&
            $loggingOptions[ 'stadion_bilder_debugging_enabled' ] == 'true' ? 'checked="checked" ' : ''
        );

        printf (
            'No <input type="radio" id="stadion_bilder_debugging_enabled" name="stadion_bilder_options_debugging[stadion_bilder_debugging_enabled]" value="false" %s/>',
            isset( $loggingOptions[ 'stadion_bilder_debugging_enabled' ] ) &&
            $loggingOptions[ 'stadion_bilder_debugging_enabled' ] == 'true' ? '' : 'checked="checked" '
        );

    }
}
