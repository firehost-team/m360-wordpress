<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class M360_Stadion_Bilder_Settings_Main_View{
    public $tabs;

    function __construct(){
        $this->tabs = array(
            'setup' => stadion_translate('Setup'),
            'syncing'=> stadion_translate('Sync'),
            'debugging'=> stadion_translate('Debugging')
        );

        $this->draw_the_page();
    }
    public function draw_the_page(){
        $current = isset( $_GET['tab'])?$_GET['tab']:'setup';

        print '<div id="icon-themes" class="m360_stadion_bilder_icon"><a class="m360_stadion_bilder_link" target="_blank" href="https://www.m360.no/">&nbsp;</a></div>';
        print '<h2 class="nav-tab-wrapper">';

        foreach( $this->tabs as $tab => $name ) {
            $class = ( $tab == $current ) ? ' nav-tab-active' : '';
            print "<a class='nav-tab$class' href='?page=stadion_bilder_page&tab=$tab'>$name</a>";

        }
        print '</h2></div>';
    }
}
