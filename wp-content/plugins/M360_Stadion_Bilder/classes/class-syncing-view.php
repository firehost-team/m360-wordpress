<?php
/**
 * Created by PhpStorm.
 * User: ibrahimqraiqe
 * Date: 09.03.2018
 * Time: 08.42
 */

class M360_Stadion_Bilder_Settings_Syncing_Tab{

    public function start_syncing_all_products(){
        $paged = (isset($_POST['paged'])) ? $_POST['paged'] : 0;
        $posts_per_page = (isset($_POST['posts_per_page'])) ? $_POST['posts_per_page'] : 10;

        $append = stadion_translate('Syncing page# ').$paged;

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $posts_per_page,
            'paged' => $paged,
        );

        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
                $post_id = $loop->post->ID;
                $main_product = wc_get_product($post_id);
                $main_product_media_id = CallStadionAPIGetMediaID(stadion_gtin($post_id));
                if($main_product_media_id)add_image_to_media_library($post_id,$main_product_media_id);
                foreach ($main_product->get_children() as $child_id){
                    //$child_product = wc_get_product($child_id);
                    $child_media_id = CallStadionAPIGetMediaID(stadion_gtin($child_id));
                    if($child_media_id)add_image_to_media_library($child_id,$child_media_id);
                }
            endwhile;
        }

        wp_reset_postdata();

        if($paged < $loop->max_num_pages){
            wp_send_json_success( array('message'=>'continue','append'=>$append,'page'=>$paged) );
        }else{
            wp_send_json_success( array('message'=>'finished','append'=>$append) );
        }
        wp_die();
    }
    public function draw_the_page(){
        ?>
        <div class="wrap">
            <h2><?php echo stadion_translate('Sync all products with Stadion API')?></h2>
            <span><?php echo stadion_translate('Products per page: ')?> </span><input id="stadion_products_per_page" type="number" value="10" style="width: 70px;" min="1">
            <div id="export_result"></div>
            <button type="button" class="button button-primary" id="stadion_start_sync_all_products"><?php echo stadion_translate('Sync!');?></button>
        </div>
        <?php
        $this->draw_loading();
    }

    private function draw_loading(){
        ?>
        <div class="loading_container">
            <div class="loading"><p><?php echo stadion_translate('Syncing page# 1');?></p></div>
        </div>

        <?php
    }
}