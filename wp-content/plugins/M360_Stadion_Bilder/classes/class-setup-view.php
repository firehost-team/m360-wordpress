<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}

class M360_Stadion_Bilder_Settings_Setup_Tab{

    public function init_fields(){
        register_setting(
            'stadion_bilder_options_setup_group',
            'stadion_bilder_options_setup',
            stadion_bilder_sanitise()
        );
        add_settings_section(
            'm360_stadion_bilder_section_setup',
            __(M360_STADION_BILDER_PLUGIN_NAME.' v.: ',M360_Stadion_Bilder::TEXT_DOMAIN).M360_Stadion_Bilder::VERSION,
            array( $this, 'm360_stadion_bilder_section_setup_html' ),
            'stadion_bilder_options_page_setup'
        );

        add_settings_field(
            'stadion_bilder_security_key',
            __('Security key',M360_Stadion_Bilder::TEXT_DOMAIN),
            array( $this, 'stadion_bilder_security_key_html' ),
            'stadion_bilder_options_page_setup',
            'm360_stadion_bilder_section_setup'
        );

        add_settings_field(
            'stadion_bilder_gtin_attribute',
            __('Which attribute to use',M360_Stadion_Bilder::TEXT_DOMAIN),
            array( $this, 'stadion_bilder_gtin_attribute_html' ),
            'stadion_bilder_options_page_setup',
            'm360_stadion_bilder_section_setup'
        );
        add_settings_field(
            'stadion_bilder_gtin_second_attribute',
            __('Second attribute if first faild',M360_Stadion_Bilder::TEXT_DOMAIN),
            array( $this, 'stadion_bilder_gtin_second_attribute_html' ),
            'stadion_bilder_options_page_setup',
            'm360_stadion_bilder_section_setup'
        );

        add_settings_field(
            'stadion_bilder_text',
            __('Product description',M360_Stadion_Bilder::TEXT_DOMAIN),
            array( $this, 'stadion_bilder_text_html' ),
            'stadion_bilder_options_page_setup',
            'm360_stadion_bilder_section_setup'
        );



    }

    public function m360_stadion_bilder_section_setup_html(){
        stadion_field_seperator();
    }

    public function stadion_bilder_security_key_html(){
        get_setting_input_html('text','stadion_bilder_options_setup','stadion_bilder_security_key','');
    }

    public function stadion_bilder_gtin_second_attribute_html(){
        $values = stadion_get_meta_values();

        $saved = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_gtin_second_attribute','_sku');

        print '<select name="stadion_bilder_options_setup[stadion_bilder_gtin_second_attribute]">';
        foreach ( $values as $key => $value) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }
    public function stadion_bilder_gtin_attribute_html(){
        $values = stadion_get_meta_values();

        $saved = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_gtin_attribute','m360_pck_eans');

        print '<select name="stadion_bilder_options_setup[stadion_bilder_gtin_attribute]">';
        foreach ( $values as $key => $value) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function stadion_bilder_text_html(){
        $values = array(
            'content'=>'Content',
            'short_description'=>'Short Description',
            'both'=>'Both',
            'content_if_empty'=>'in Content if empty',
            'short_description_if_empty'=>'in Short Description if empty',
            'both_if_empty'=>'in Both if empty',
            'no_description'=>'Do not use Stadion API');

        $saved = stadion_bilder_get_settings('stadion_bilder_options_setup','stadion_bilder_text','no_description');

        print '<select name="stadion_bilder_options_setup[stadion_bilder_text]">';
        foreach ( $values as $key => $value) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }
}
