<?php

/*
Plugin Name: M360 Ekstra funksjoner
Plugin URI: http://m360.no
Description: Ekstra nyttig funksjoner
Version: 1.69
Author: ibrahim-2015
Author URI: http://m360.no
License: A "Slug" license name e.g. GPL2
*/

$m360_ekstra_dir = plugin_dir_path(__FILE__);
$m360_ekstra_plugin_url = plugin_dir_url(__FILE__);
define("M360_EKSTRA_PLUGIN", "M360_Ekstra/m360_ekstra.php");

include_once ($m360_ekstra_dir.'inc/functions.php');
include_once ($m360_ekstra_dir.'inc/wp_hooks.php');

include_once ($m360_ekstra_dir.'inc/price.php');
include_once ($m360_ekstra_dir.'inc/product.php');
include_once ($m360_ekstra_dir.'inc/menus.php');
include_once ($m360_ekstra_dir.'inc/menus/menus.php');

$price_page = new M360_EKSTRA_PLUGINS_PRICE();
$product_page = new M360_EKSTRA_PLUGINS_PRODUCT();
$menus_page = new M360_EKSTRA_PLUGINS_MENUS();


if( !defined( 'ABSPATH' ) ) {
    exit;
}

class M360_EKSTRA_MainClass{
    public function __construct(){

        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        $this->hooks();

    }


    public function menu_image_add_inline_style_action() {
        $menus_options = get_option( 'm360_ekstra_functions_menus_options' );
        if(isset($menus_options[ 'add_icons_upload_function_to_menus_switch'] )){
            if($menus_options[ 'add_icons_upload_function_to_menus_switch'] == 'on'){
                wp_enqueue_style( 'menu-image', plugins_url( '/assets/css/menu-image.css', __FILE__ ) );
            }
        }
    }

    public function hooks(){
        register_activation_hook( __FILE__, array( $this, 'm360_ekstra_plugin_activation' ) );

        add_action( 'admin_menu', array( $this, 'add_m360_page' ) );
        add_action( 'admin_menu', array( $this, 'add_m360_ekstra_submenu' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'init_m360_scripts_admin' ) );

        add_action( 'wp_loaded', array($this,'register_all_scripts') );

        add_action( 'admin_init', array( $this, 'init_m360_ekstra_plugin_pages' ) );

        add_action( 'get_footer', array( $this, 'init_footer_scripts' ) );


        $wp_hooks_class = new M360_EKSTRA_PLUGIN_WP_HOOKS();
        $wp_hooks_class->m360_wp_hooks();

    }

    public function init_footer_scripts(){
        $product_options = get_option( 'm360_ekstra_functions_product_options' );

        if(isset($product_options[ 'm360_product_limit_line'] )){
            if(intval($product_options[ 'm360_product_limit_line']) > 0){
                wp_register_script('dotdotdot', plugins_url( '/assets/js/jquery.dotdotdot.js', __FILE__ ), array('jquery') );
                wp_enqueue_script('dotdotdot');
            }
        }
    }

    public function m360_ekstra_plugin_activation(){

    }

    public function init_m360_ekstra_plugin_pages() {
        global $price_page;
        global $product_page;
        global $menus_page;

        $price_page->drawPriceSection();
        $product_page->drawProductSection();
        $menus_page->drawProductSection();
    }

    public function register_all_scripts(){
        $frontend_css_file = plugins_url( '/assets/css/frontend.css', __FILE__ );
        $frontend_css_file_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'assets/css/frontend.css' ));


        $js_file = plugins_url( '/assets/js/m360.js', __FILE__ );
        //$js_file_mtime = filemtime($js_file);

        wp_enqueue_style( 'm360-ekstra-plugin-frontend-style', $frontend_css_file,false, $frontend_css_file_ver);

        wp_register_script('m360scripts', $js_file, array('jquery') );
        wp_enqueue_script('m360scripts');

        $this->menu_image_add_inline_style_action();

    }
    public function init_m360_scripts_admin(){
        $css_file = plugins_url( '/assets/css/style.css', __FILE__ );
        $css_file_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'assets/css/style.css' ));

        wp_enqueue_style( 'm360-ekstra-plugin-admin-style', $css_file,false,$css_file_ver );
    }

    public function add_m360_page() {
        if ( empty ( $GLOBALS['admin_page_hooks']['m360_plugins'] ) ){
            add_menu_page(
                '',
                'M360 Plugins',
                'manage_options',
                'm360_plugins',
                '',
                m360_ekstra_plugin_asset_url('images/m360_hvit_25.png'),//$icon_url
                31.0
            );
        }

    }

    public function create_m360_ekstra_plugins_page(){
        if( isset( $_GET['tab'] ) )
            $tab = $_GET['tab'];

        print '<div class="wrap">';
        print '<h2>M360 Ekstra funksjoner</h2>';

        if ( !isset( $tab ) || empty( $tab ) )
            $tab = 'price';

        $this->create_m360_ekstra_plugins_tabs( $tab );

        print '<form method="post" action="options.php">';


        settings_fields( 'm360_ekstra_functions_' . $tab . '_group' );
        do_settings_sections( 'm360_ekstra_functions_page_' . $tab );

        submit_button();

        // close page wrapper
        print '</form></div>';
    }
    public function add_m360_ekstra_submenu() {
        add_submenu_page(
            'm360_plugins',
            'M360 Ekstra plugins: v.'.plugin_get_version(M360_EKSTRA_PLUGIN),
            'Ekstra',
            'manage_options',
            'm360_plugins',
            array( $this, 'create_m360_ekstra_plugins_page' )
        );


    }

    public function create_m360_ekstra_plugins_tabs( $current = 'price' ) {

        $tabs = array(
            'price' => 'Price',
            'product' => 'Product',
            'menus' => 'Menus'
        );

        print '<div id="icon-themes" class="m360_pck_icon"><a class="m360_pck_link" target="_blank" href="http://www.m360.no/">&nbsp;</a></div>';
        print '<h2 class="nav-tab-wrapper">';

        foreach( $tabs as $tab => $name ) {
            $class = ( $tab == $current ) ? ' nav-tab-active' : '';
            print "<a class='nav-tab$class' href='?page=m360_plugins&tab=$tab'>$name</a>";

        }
        print '</h2></div>';
    }

    public function sanitise( $input ) {

        return $input;

    }
}

$mainClass = new M360_EKSTRA_MainClass();
