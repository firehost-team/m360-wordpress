var $j = jQuery.noConflict();

var getCoockieFromDocument = function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            //alert(cname+' '+c.substring(name.length, c.length));
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var price_with_tax = getCoockieFromDocument('m360_show_price_with_tax');
var prisMedMoms = $j( ".pris_med_moms" );
var prisUtenMoms = $j( ".pris_uten_moms" );
//var button_cat = $j("#m360_tax_switcher_button_cat");



/*
var hideTaxInHeaderSettings = function() {
    if ($j('#m360_wowtax_setting').is(':checked')) {
        $j( "#show_price_with_and_without_tax_in_header_switcher_container" ).css( "display", "inline-block" );
    } else {
        $j( "#show_price_with_and_without_tax_in_header_switcher_container" ).css( "display", "none" );
    };
};
*/

var hideThumbnailClassSettings = function() {
    if ($j('#m360_aspect_ratio_switcher').is(':checked')) {
        $j( ".m360_ascpect_ration_class_in_admin" ).css( "display", "block" );
    } else {
        $j( ".m360_ascpect_ration_class_in_admin" ).css( "display", "none" );
    };
};

var hideProductImageBorderSettings = function() {
    if ($j('#m360_add_border_to_product_image').is(':checked')) {
        $j( ".m360_product_image_admin_fields" ).css( "display", "block" );
    } else {
        $j( ".m360_product_image_admin_fields" ).css( "display", "none" );
    };
};

var setTaxCoockies = function(value){
    document.cookie = "m360_show_price_with_tax=" + escape(value) + ";";
};

var hidPrisMedMoms = function(){
    //alert('hidPrisMedMoms');
    prisMedMoms.css( "display", "none" );
    prisUtenMoms.css( "display", "block" );
}

var hidPrisUtenMoms = function(){
    //alert('hidPrisUtenMoms');
    prisMedMoms.css( "display", "none" );
    prisUtenMoms.css( "display", "block" );
}

var checkTaxSettings = function(){
    if(price_with_tax == 'no'){
        hidPrisMedMoms();
    }else{
        hidPrisUtenMoms();
    }
};

$j(document).ready(function() {
    var m360_cat_description_under_header_toggle = $j('.m360_cat_description_under_header #toggle');
    var m360CatDescriptionHeight = $j('#m360_cat_description_section').height();
    var m360_cat_description_expand_org_margin_bottom = $j('#m360_cat_description_expand').css( "margin-bottom" );

    m360_cat_description_under_header_toggle.click(function() {
        if($j('#m360_cat_description_expand').css( "height" ) == '0px'){
            $j( "#m360_cat_description_expand" ).css( "height", m360CatDescriptionHeight+"px" );
            $j( "#m360_cat_description_expand" ).css( "margin-bottom", "20px" );
        }else{
            $j( "#m360_cat_description_expand" ).css( "height", "0px" );
            $j( "#m360_cat_description_expand" ).css( "margin-bottom", m360_cat_description_expand_org_margin_bottom);
        }
    });


    //hideTaxInHeaderSettings();
    hideThumbnailClassSettings();
    hideProductImageBorderSettings();

    /*
    $j('#m360_wowtax_setting').change(function() {
        if(this.checked) {
            $j( "#show_price_with_and_without_tax_in_header_switcher_container" ).css( "display", "inline-block" );
        }else{
            $j( "#show_price_with_and_without_tax_in_header_switcher_container" ).css( "display", "none" );
        }
    });
    */

    $j('#m360_aspect_ratio_switcher').change(function() {
        if(this.checked) {
            $j( ".m360_ascpect_ration_class_in_admin" ).css( "display", "block" );
        }else{
            $j( ".m360_ascpect_ration_class_in_admin" ).css( "display", "none" );
        }
    });

    $j('#m360_add_border_to_product_image').change(function() {
        if(this.checked) {
            $j( ".m360_product_image_admin_fields" ).css( "display", "block" );
        }else{
            $j( ".m360_product_image_admin_fields" ).css( "display", "none" );
        }
    });


    /* tax for cats */
    $j('#priceInclVAT').change(function() {
        if(this.checked){
            setTaxCoockies('yes');
        }
        $j('#m360SetTax').submit();
    });

    $j('#priceExclVAT').change(function() {
        if(this.checked){
            setTaxCoockies('no');
        }
        $j('#m360SetTax').submit();
    });

});

