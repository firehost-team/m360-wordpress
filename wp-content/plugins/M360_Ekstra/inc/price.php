<?php
/**
 * Created by PhpStorm.
 * User: ibrahim-2015
 * Date: 25.08.2017
 * Time: 13.55
 */



if(!class_exists('M360_EKSTRA_PLUGINS_PRICE')){
    class M360_EKSTRA_PLUGINS_PRICE{

        function __construct(){
            $this->price_options = array();

            $price_options = get_option( 'm360_ekstra_functions_price_options' );
            if(isset($price_options[ 'hide_price_switch'] ))
                $this->price_options['hide_price_switch'] = ' checked';
            else
                $this->price_options['hide_price_switch'] = '';

            if(isset($price_options[ 'hide_price_message'] ))
                $this->price_options['hide_price_message'] = $price_options[ 'hide_price_message'];
            else
                $this->price_options['hide_price_message'] = 'Du må logge deg inn for å kunne kjøpe';

            if(isset($price_options[ 'show_price_with_and_without_tax'] ))
                $this->price_options['show_price_with_and_without_tax'] =  ' checked';
            else
                $this->price_options['show_price_with_and_without_tax'] = '';

            /*
            if(isset($price_options[ 'show_price_with_and_without_tax_in_header_switcher'] ))
                $this->price_options['show_price_with_and_without_tax_in_header_switcher'] =  ' checked';
            else
                $this->price_options['show_price_with_and_without_tax_in_header_switcher'] =  '';
            */
        }
        public function sanitise( $input ) {
            return $input;
        }


        public function drawPriceSection(){

            register_setting(
                'm360_ekstra_functions_price_group', // group
                'm360_ekstra_functions_price_options', // name
                array( $this, 'sanitise' ) // sanitise method
            );

            add_settings_section(
                'm360_ekstra_functions_price_section',
                'Price functions',
                '',
                'm360_ekstra_functions_page_price'
            );

            add_settings_field(
                'hide_price_switch', // id
                'Hide price for not logging customer', // title
                array( $this, 'hide_price_switch_html' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );

            add_settings_field(
                'hide_price_message', // id
                'Message to the customer', // title
                array( $this, 'hide_price_message_html' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );

            add_settings_field(
                'show_price_with_and_without_tax', // id
                'Show the price with and without tax', // title
                array( $this, 'show_price_with_and_without_tax_html' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );
            /*
            add_settings_field(
                'show_price_with_and_without_tax_in_header_switcher', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );
            */
        }

        public function emptyFunction(){

        }

        public function hide_price_message_html(){
            $hide_price_message = $this->price_options[ 'hide_price_message'];
            printf(
                '<input type="text" id="hide_price_message" name="m360_ekstra_functions_price_options[hide_price_message]" style="width: 250px;" value="%s" />',
                $hide_price_message);
        }

        public function hide_price_switch_html(){
            $checked = $this->price_options[ 'hide_price_switch'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_price_options[hide_price_switch]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function show_price_with_and_without_tax_html(){

            $checked = $this->price_options[ 'show_price_with_and_without_tax'];
            printf(
                '<label class="switch">
                <input id="m360_wowtax_setting" type="checkbox" name="m360_ekstra_functions_price_options[show_price_with_and_without_tax]" %s />
                <span class="slider round"></span>
             </label>',
                $checked
            );
            //$this->show_price_with_and_without_tax_in_header_switcher_html();
        }
        /*
            public function show_price_with_and_without_tax_in_header_switcher_html(){
                $checked = $this->price_options[ 'show_price_with_and_without_tax_in_header_switcher'];

                printf(
                    '<span id="show_price_with_and_without_tax_in_header_switcher_container">
                        <label for=""m360_ekstra_functions_price_options[show_price_with_and_without_tax_in_header_switcher]">
                        <input type="checkbox" name="m360_ekstra_functions_price_options[show_price_with_and_without_tax_in_header_switcher]" %s />
                        Show switcher in header</label>
                     </span>',
                    $checked
                );

            }
        */

    }
}