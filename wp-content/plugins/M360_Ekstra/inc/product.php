<?php
/**
 * Created by PhpStorm.
 * User: ibrahim-2015
 * Date: 29.08.2017
 * Time: 15.10
 */

if(!class_exists('M360_EKSTRA_PLUGINS_PRODUCT')){
    class M360_EKSTRA_PLUGINS_PRODUCT{
        public $product_options;
        function __construct(){
            $this->product_options = array();
            /*
             * sku
             */
            $product_options = get_option( 'm360_ekstra_functions_product_options' );
            if(isset($product_options[ 'show_product_sku_on_product_page_switch'] ))
                $this->product_options['show_product_sku_on_product_page_switch'] = ' checked';
            else
                $this->product_options['show_product_sku_on_product_page_switch'] = '';

            if(isset($product_options[ 'show_product_sku_on_product_category_switch'] ))
                $this->product_options['show_product_sku_on_product_category_switch'] = ' checked';
            else
                $this->product_options['show_product_sku_on_product_category_switch'] = '';

            if(isset($product_options[ 'show_product_sku_on_product_details_switch'] ))
                $this->product_options['show_product_sku_on_product_details_switch'] = ' checked';
            else
                $this->product_options['show_product_sku_on_product_details_switch'] = '';

            /*
             * limit product title height
             */
            if(isset($product_options[ 'm360_product_limit_line'] ))
                $this->product_options['m360_product_limit_line'] = $product_options[ 'm360_product_limit_line'];
            else
                $this->product_options['m360_product_limit_line'] = '0px';

            if(isset($product_options[ 'm360_product_limit_line_class_name'] ))
                $this->product_options['m360_product_limit_line_class_name'] = $product_options[ 'm360_product_limit_line_class_name'];
            else
                $this->product_options['m360_product_limit_line_class_name'] = 'product-title';

            /*
             * category description and image
             */
            if(isset($product_options[ 'm360_show_product_category_description'] ))
                $this->product_options['m360_show_product_category_description'] = ' checked';
            else
                $this->product_options['m360_show_product_category_description'] = '';

            /*
             * remove some product attributes from product page
             */
            if(isset($product_options[ 'm360_remove_weight_from_additional_information'] ))
                $this->product_options['m360_remove_weight_from_additional_information'] = ' checked';
            else
                $this->product_options['m360_remove_weight_from_additional_information'] = '';

            if(isset($product_options[ 'm360_remove_dimensions_from_additional_information'] ))
                $this->product_options['m360_remove_dimensions_from_additional_information'] = ' checked';
            else
                $this->product_options['m360_remove_dimensions_from_additional_information'] = '';
            /*
             * prodcut images aspect ratio
             */
            if(isset($product_options[ 'm360_keep_image_aspect_ratio'] ))
                $this->product_options['m360_keep_image_aspect_ratio'] = ' checked';
            else
                $this->product_options['m360_keep_image_aspect_ratio'] = '';


            if(isset($product_options[ 'm360_aspect_ration_class_name'] ))
                $this->product_options['m360_aspect_ration_class_name'] = $product_options[ 'm360_aspect_ration_class_name'];
            else
                $this->product_options['m360_aspect_ration_class_name'] = 'product-element-top';

            if(isset($product_options[ 'm360_aspect_ration_class_height'] ))
                $this->product_options['m360_aspect_ration_class_height'] = $product_options[ 'm360_aspect_ration_class_height'];
            else
                $this->product_options['m360_aspect_ration_class_height'] = '200px';

            /*
             * product image border
             */

            /* border */
            if(isset($product_options[ 'm360_add_border_to_product_image'] ))
                $this->product_options['m360_add_border_to_product_image'] = ' checked';
            else
                $this->product_options['m360_add_border_to_product_image'] = '';
            /* border color */
            if(isset($product_options[ 'm360_product_image_border_color'] ))
                $this->product_options['m360_product_image_border_color'] = $product_options[ 'm360_product_image_border_color'];
            else
                $this->product_options['m360_product_image_border_color'] = '#000000';

            /* shadow */
            if(isset($product_options[ 'm360_product_image_add_shadow'] ))
                $this->product_options['m360_product_image_add_shadow'] = ' checked';
            else
                $this->product_options['m360_product_image_add_shadow'] = '';
            /* radius */
            if(isset($product_options[ 'm360_product_image_border_radius'] ))
                $this->product_options['m360_product_image_border_radius'] = $product_options[ 'm360_product_image_border_radius'];
            else
                $this->product_options['m360_product_image_border_radius'] = '5px';

            /* width */
            if(isset($product_options[ 'm360_product_image_border_width'] ))
                $this->product_options['m360_product_image_border_width'] = $product_options[ 'm360_product_image_border_width'];
            else
                $this->product_options['m360_product_image_border_width'] = '5px';

            /* style */
            if(isset($product_options[ 'm360_product_image_border_style'] ))
                $this->product_options['m360_product_image_border_style'] = $product_options[ 'm360_product_image_border_style'];
            else
                $this->product_options['m360_product_image_border_style'] = 'solid';

            /* pagnation */
            if(isset($product_options[ 'm360_add_pagination'] ))
                $this->product_options['m360_add_pagination'] = ' checked';
            else
                $this->product_options['m360_add_pagination'] = '';

        }
        public function sanitise( $input ) {
            return $input;
        }


        public function drawProductSection(){

            register_setting(
                'm360_ekstra_functions_product_group', // group
                'm360_ekstra_functions_product_options', // name
                array( $this, 'sanitise' ) // sanitise method
            );

            add_settings_section(
                'm360_ekstra_functions_product_section',
                'product functions',
                '',
                'm360_ekstra_functions_page_product'
            );

            /*
             * sku
             */
            add_settings_field(
                'show_product_sku_on_product_page_switch', // id
                'Show SKU in single product page', // title
                array( $this, 'show_product_sku_on_product_page_switch_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'show_product_sku_on_product_category_switch', // id
                'Show sku in products category', // title
                array( $this, 'show_product_sku_on_product_category_switch_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'show_product_sku_on_product_details_switch', // id
                'Show sku in products additional information', // title
                array( $this, 'show_product_sku_on_product_details_switch_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            /*
             * remove some product attributes from product page
             */
            add_settings_field(
                'm360_remove_weight_from_additional_information', // id
                'Remove weight from product additional information', // title
                array( $this, 'm360_remove_weight_from_additional_information_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_remove_dimensions_from_additional_information', // id
                'Remove dimensions from product additional information', // title
                array( $this, 'm360_remove_dimensions_from_additional_information_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            /*
             * limit product title height
             */

            add_settings_field(
                'm360_product_limit_line', // id
                'Limit product lines by title height in px', // title
                array( $this, 'm360_product_limit_line_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_product_limit_line_class_name', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            /*
             * category description and image
             */
            add_settings_field(
                'm360_show_product_category_description', // id
                'show category discription and thumbnail', // title
                array( $this, 'm360_show_product_category_description_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );
            /*
             * prodcut images aspect ratio
             */
            add_settings_field(
                'm360_keep_image_aspect_ratio', // id
                'Keep aspect ration for the product thumbnail', // title
                array( $this, 'm360_keep_image_aspect_ratio_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_aspect_ration_class_name', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );

            add_settings_field(
                'm360_aspect_ration_class_height', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_price', // page
                'm360_ekstra_functions_price_section' // section
            );
            /*
             * product image border
             */
            add_settings_field(
                'm360_add_border_to_product_image', // id
                'Add border to product image', // title
                array( $this, 'm360_add_border_to_product_image_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );
            add_settings_field(
                'm360_product_image_border_color', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_product_image_add_shadow', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );
            add_settings_field(
                'm360_product_image_border_radius', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_product_image_border_width', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            add_settings_field(
                'm360_product_image_border_style', // id
                '', // title
                array( $this, 'emptyFunction' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

            /* add pagination */
            add_settings_field(
                'm360_add_pagination', // id
                'add pagination', // title
                array( $this, 'm360_add_pagination_html' ), // callback
                'm360_ekstra_functions_page_product', // page
                'm360_ekstra_functions_product_section' // section
            );

        }

        public function emptyFunction(){

        }
        /* add pagination */
        public function m360_add_pagination_html(){
            $checked = $this->product_options[ 'm360_add_pagination'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[m360_add_pagination]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        /* thumbnail aspect ration */
        public function m360_keep_image_aspect_ratio_html(){
            $checked = $this->product_options[ 'm360_keep_image_aspect_ratio'];
            $class_name = $this->product_options[ 'm360_aspect_ration_class_name'];
            $height = $this->product_options[ 'm360_aspect_ration_class_height'];

            printf(
                '<p><label class="switch"><input id="m360_aspect_ratio_switcher" type="checkbox" name="m360_ekstra_functions_product_options[m360_keep_image_aspect_ratio]" %s /><span class="slider round"></span></label></p>',
                $checked
            );

            printf(
                '<div class="m360_ascpect_ration_class_in_admin">
                   <ul>
                     <li><div class="m360_30left">Image thumbnail class:</div><div><input type="text" name="m360_ekstra_functions_product_options[m360_aspect_ration_class_name]" value="%s" /></div></li>
                     <li><div class="m360_30left">Image thumbnail height:</div><div><input type="text" name="m360_ekstra_functions_product_options[m360_aspect_ration_class_height]" value="%s" /></div></li>
                    </ul>
                </div>',
                $class_name,$height);
        }
        /* end */
        /* remove weight and dimensions */
        public function m360_remove_weight_from_additional_information_html(){
            $checked = $this->product_options[ 'm360_remove_weight_from_additional_information'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[m360_remove_weight_from_additional_information]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function m360_remove_dimensions_from_additional_information_html(){
            $checked = $this->product_options[ 'm360_remove_dimensions_from_additional_information'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[m360_remove_dimensions_from_additional_information]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }
        /* ------------- */
        /* add border to product image */
        public function m360_add_border_to_product_image_html(){

            $checked = $this->product_options[ 'm360_add_border_to_product_image'];
            $border_color = $this->product_options[ 'm360_product_image_border_color'];
            $border_shadow = $this->product_options[ 'm360_product_image_add_shadow'];
            $border_radius = $this->product_options[ 'm360_product_image_border_radius'];
            $border_width = $this->product_options[ 'm360_product_image_border_width'];
            $border_style = $this->product_options[ 'm360_product_image_border_style'];


            printf(
                '<p><label class="switch"><input id="m360_add_border_to_product_image" type="checkbox" name="m360_ekstra_functions_product_options[m360_add_border_to_product_image]" %s /><span class="slider round"></span></label></p>',
                $checked
            );


            printf(
                '<ul class="m360_product_image_admin_fields">
             <li><div class="m360_30left">Border width: </div><div><input type="text" name="m360_ekstra_functions_product_options[m360_product_image_border_width]" value="%s" /></div></li>
             <li><div class="m360_30left">Border color: </div><div><input type="text" name="m360_ekstra_functions_product_options[m360_product_image_border_color]" value="%s" /></div></li>
             <li><div class="m360_30left">Border shadow:</div><div><label class="switch"><input id="m360_product_image_add_shadow" type="checkbox" name="m360_ekstra_functions_product_options[m360_product_image_add_shadow]" %s /><span class="slider round"></span></label></div></li>
             <li><div class="m360_30left">Border radius: </div><div><input type="text" name="m360_ekstra_functions_product_options[m360_product_image_border_radius]" value="%s" /></div></li>
             <li>
                <div class="m360_30left">Border style: </div>
                <div>
                    <input type="text" name="m360_ekstra_functions_product_options[m360_product_image_border_style]" value="%s" />
                    <sub style="display: block;color: red;font-weight: 400;text-decoration: underline;">use dashed ,dotted, double, groove, ridge or solid , just use one style</sub>
                </div>
             </li>
             </ul>',
                $border_width,$border_color,$border_shadow,$border_radius,$border_style);

        }

        public function show_product_sku_on_product_details_switch_html(){
            $checked = $this->product_options[ 'show_product_sku_on_product_details_switch'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[show_product_sku_on_product_details_switch]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function m360_show_product_category_description_html(){
            $checked = $this->product_options[ 'm360_show_product_category_description'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[m360_show_product_category_description]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function m360_product_limit_line_html(){
            $product_height = $this->product_options['m360_product_limit_line'];
            $title_class = $this->product_options['m360_product_limit_line_class_name'];
            printf(
                '<ul>
                 <li><div class="m360_30left">Limit height to:</div><div><input type="text" id="hide_price_message" name="m360_ekstra_functions_product_options[m360_product_limit_line]" style="width: 250px;" value="%s" /></div></li>
                <li><div class="m360_30left">Product title class: </div><div><input type="text" name="m360_ekstra_functions_product_options[m360_product_limit_line_class_name]" value="%s" /></div></li></ul>',
                $product_height,$title_class);

        }

        public function show_product_sku_on_product_category_switch_html(){
            $checked = $this->product_options[ 'show_product_sku_on_product_category_switch'];
            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[show_product_sku_on_product_category_switch]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function show_product_sku_on_product_page_switch_html(){
            $checked = $this->product_options[ 'show_product_sku_on_product_page_switch'];

            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_product_options[show_product_sku_on_product_page_switch]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }


    }
}

