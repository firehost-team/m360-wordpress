<?php
/**
 * Created by PhpStorm.
 * User: ibrahim-2015
 * Date: 12.09.2017
 * Time: 14.29
 */

if(!class_exists('M360_EKSTRA_PLUGINS_MENUS')){
    class M360_EKSTRA_PLUGINS_MENUS{
        public $menus_options;
        function __construct(){
            $this->menus_options = array();

            $menus_options = get_option( 'm360_ekstra_functions_menus_options' );
            if(isset($menus_options[ 'add_icons_upload_function_to_menus_switch'] ))
                $this->menus_options['add_icons_upload_function_to_menus_switch'] = ' checked';
            else
                $this->menus_options['add_icons_upload_function_to_menus_switch'] = '';

            if(isset($menus_options[ 'fix_wc_hierarchical'] ))
                $this->menus_options['fix_wc_hierarchical'] = ' checked';
            else
                $this->menus_options['fix_wc_hierarchical'] = '';

        }
        public function sanitise( $input ) {
            return $input;
        }


        public function drawProductSection(){

            register_setting(
                'm360_ekstra_functions_menus_group', // group
                'm360_ekstra_functions_menus_options', // name
                array( $this, 'sanitise' ) // sanitise method
            );

            add_settings_section(
                'm360_ekstra_functions_menus_section',
                'product functions',
                '',
                'm360_ekstra_functions_page_menus'
            );

            add_settings_field(
                'add_icons_upload_function_to_menus_switch', // id
                'add ability to upload custom icons in menus', // title
                array( $this, 'add_icons_upload_function_to_menus_switch_html' ), // callback
                'm360_ekstra_functions_page_menus', // page
                'm360_ekstra_functions_menus_section' // section
            );

            add_settings_field(
                'fix_wc_hierarchical', // id
                'Fix woocommerce categories hierarchical in menus settings', // title
                array( $this, 'fix_wc_hierarchical_html' ), // callback
                'm360_ekstra_functions_page_menus', // page
                'm360_ekstra_functions_menus_section' // section
            );

        }

        public function emptyFunction(){

        }

        /* thumbnail aspect ration */
        public function add_icons_upload_function_to_menus_switch_html(){
            $checked = $this->menus_options[ 'add_icons_upload_function_to_menus_switch'];

            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_menus_options[add_icons_upload_function_to_menus_switch]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }

        public function fix_wc_hierarchical_html(){
            $checked = $this->menus_options[ 'fix_wc_hierarchical'];

            printf(
                '<label class="switch"><input type="checkbox" name="m360_ekstra_functions_menus_options[fix_wc_hierarchical]" %s /><span class="slider round"></span></label>',
                $checked
            );
        }


    }
}