<?php
/**
 * Created by PhpStorm.
 * User: ibrahim-2015
 * Date: 29.08.2017
 * Time: 15.17
 */


class M360_EKSTRA_PLUGIN_WP_HOOKS{

    function __construct(){

    }


    public function m360_wp_hooks(){
        global $price_page;
        global $product_page;
        global $menus_page;

        /* price hooks start*/
        if($price_page->price_options[ 'hide_price_switch'] == ' checked'){
            add_action('init', array($this,'bbloomer_hide_price_add_cart_not_logged_in'));
        }

        if (!is_admin()){
            if($price_page->price_options[ 'show_price_with_and_without_tax'] == ' checked'){
                add_action( 'get_footer', array($this,'add_price_tax_button_after_breadcrumbs' ));
                add_filter( 'woocommerce_get_price_html', array($this,'show_product_with_and_without_tax'), 100, 2 );
            }else{
                $_COOKIE['m360_show_price_with_tax'] = 'yes';
            }
        }else{
            $_COOKIE['m360_show_price_with_tax'] = 'yes';
        }

        /* end price */

        /* product hooks start */

        if($product_page->product_options[ 'show_product_sku_on_product_page_switch'] == ' checked')
            add_action( 'woocommerce_after_add_to_cart_button', array($this,'m360_add_sku'),10,0);

        if($product_page->product_options[ 'show_product_sku_on_product_category_switch'] == ' checked')
            add_action( 'woocommerce_after_shop_loop_item_title', array($this,'m360_add_sku'),10,0 );

        if(intval($product_page->product_options[ 'm360_product_limit_line']) > 0)
            add_action( 'get_footer', array($this,'m360_add_dotdotdot'),10,1);

        if($product_page->product_options['m360_show_product_category_description'] == ' checked')
            add_action( 'woocommerce_before_main_content', array($this,'m360_woocommerce_category_image'), 2 );

        if($product_page->product_options['show_product_sku_on_product_details_switch'] == ' checked')
            add_action( 'woocommerce_after_single_product', array($this,'m360_sku_in_additional_information'), 10,2);

        if($product_page->product_options['m360_remove_weight_from_additional_information'] == ' checked')
            add_filter( 'wc_product_enable_weight_display', '__return_false' );

        if($product_page->product_options['m360_remove_dimensions_from_additional_information'] == ' checked')
            add_filter( 'wc_product_enable_dimensions_display', '__return_false' );

        if($product_page->product_options['m360_keep_image_aspect_ratio'] == ' checked'){
            add_action( 'woocommerce_after_shop_loop_item_title', array($this,'inject_css_to_image'),10,0 );
        }

        if($product_page->product_options['m360_add_border_to_product_image'] == ' checked'){
            add_action( 'wp_footer', array($this,'wrap_product_element_with_border'), 10);
        }
        /* add pagination */
        if($product_page->product_options['m360_add_pagination'] == ' checked'){
            add_action( 'pre_get_posts', array($this,'m360_add_paged_param_to_product_attributes'));
            add_action( 'loop_end', array($this,'loop_end'), 100 );
            add_action( 'woocommerce_after_template_part', array($this,'add_pagination'), 10, 4 );
            add_filter( 'woocommerce_shortcode_products_query', array($this,'shortcode_products_query_args'), 10, 3 );
            add_filter( 'woocommerce_composite_component_options_query_args', array($this,'composite_component_options_query_args'), 10, 3 );
        }

        /* product hooks end */

        /* menus hooks start */
        if($menus_page->menus_options['add_icons_upload_function_to_menus_switch'] == ' checked'){
            //add_action( 'woocommerce_after_shop_loop_item_title', array($this,'inject_css_to_image'),10,0 );
            $menus = new Menu_Image_Plugin();
            $menus->addImageIconUploadAbility();
        }

        if($menus_page->menus_options['fix_wc_hierarchical'] == ' checked'){
            add_action( 'pre_get_posts',    array($this,'disable_paging_for_hierarchical_post_types'));
            add_filter( 'get_terms_args',   array($this,'remove_limit_for_hierarchical_taxonomies'), 10, 2 );
            add_filter( 'get_terms_fields', array($this,'remove_page_links_for_hierarchical_taxonomies'), 10, 3 );
        }
        /* menus hooks end */

        add_action('init', array($this,'keepHTML'));

    }

    /* list products by attributes */
    public function is_product_query( $query ) {
        if( ! isset( $query->query['post_type'] ) || ! empty( $query->query['p'] ) || isset( $query->query['composite_component'] ) ) {
            return false;
        }
        $post_type = $query->query['post_type'];
        if( is_array( $post_type ) && in_array('product', $post_type) ) {
            return true;
        }
        if( $post_type == "product" ) {
            return true;
        }
        return false;
    }
    public function get_paged_var() {

        $query_var = is_front_page() ? 'page' : 'paged';
        return get_query_var( $query_var ) ? get_query_var( $query_var ) : 1;

    }
    public function m360_add_paged_param_to_product_attributes( $query ) {
        $is_product_query = $this->is_product_query( $query );

        if ( is_archive() || is_post_type_archive() || ! $is_product_query )
            return;

        $paged = $this->get_paged_var();

        if ( $query->is_main_query() && $is_product_query ) {
            $GLOBALS['woocommerce_loop']['paged'] = $paged;
        }

        $query->is_paged = true;
        $query->query['paged'] = $paged;
        $query->query_vars['paged'] = $paged;
        $query->query['no_found_rows'] = false;
        $query->query_vars['no_found_rows'] = false;

    }
    public function loop_end( $query ) {

        if ( is_archive() || is_post_type_archive() || !$this->is_product_query( $query ) )
            return;

        $paged = $this->get_paged_var();

        $GLOBALS['woocommerce_loop']['pagination']['paged'] = $paged;
        $GLOBALS['woocommerce_loop']['pagination']['found_posts'] = $query->found_posts;
        $GLOBALS['woocommerce_loop']['pagination']['max_num_pages'] = $query->max_num_pages;
        $GLOBALS['woocommerce_loop']['pagination']['post_count'] = $query->post_count;
        $GLOBALS['woocommerce_loop']['pagination']['current_post'] = $query->current_post;

    }
    public function has_woo_shortcodes( $content ) {

        $shortcodes = apply_filters('jck-wsp-shortcodes', array(
            'product_attribute',
            'product_category',
            'recent_products',
            'featured_products',
            'sale_products',
            'best_selling_products',
            'top_rated_products'
        ));

        if( empty( $shortcodes ) )
            return false;

        foreach( $shortcodes as $shortcode ) {

            if( has_shortcode( $content, $shortcode ) )
                return true;

        }

        return false;

    }
    public function add_pagination( $template_name, $template_path, $located, $args ) {

        global $post;

        if ( $template_name !== 'loop/loop-end.php' )
            return;

        $queried_post = get_queried_object();

        if( !$queried_post || !isset( $queried_post->post_content ) )
            return;

        if( !$this->has_woo_shortcodes( $queried_post->post_content ) )
            return;

        global $wp_query;

        if ( ! isset( $GLOBALS['woocommerce_loop']['pagination'] ) )
            return;

        $wp_query->query_vars['paged'] = $GLOBALS['woocommerce_loop']['pagination']['paged'];
        $wp_query->query['paged'] = $GLOBALS['woocommerce_loop']['pagination']['paged'];
        $wp_query->max_num_pages = $GLOBALS['woocommerce_loop']['pagination']['max_num_pages'];
        $wp_query->found_posts = $GLOBALS['woocommerce_loop']['pagination']['found_posts'];
        $wp_query->post_count = $GLOBALS['woocommerce_loop']['pagination']['post_count'];
        $wp_query->current_post = $GLOBALS['woocommerce_loop']['pagination']['current_post'];

        // Custom pagination function or default woocommerce_pagination()
        woocommerce_pagination();

    }
    public function shortcode_products_query_args( $query_args, $atts, $loop_name ) {

        $query_args['paged'] = $this->get_paged_var();

        return $query_args;

    }
    public function composite_component_options_query_args( $args, $query_args, $component_data ) {
        $args['composite_component'] = true;

        return $args;
    }
    /* end adding pagination */

    public function wrap_product_element_with_border() {
        global $product_page;

        $border_color = $product_page->product_options[ 'm360_product_image_border_color'];
        $border_shadow = $product_page->product_options[ 'm360_product_image_add_shadow'];
        $border_radius = $product_page->product_options[ 'm360_product_image_border_radius'];
        $border_width = $product_page->product_options[ 'm360_product_image_border_width'];
        $border_style = $product_page->product_options[ 'm360_product_image_border_style'];
        ?>
        <style>
            .m360_product_imagewrapper{
                border-width: <?php echo $border_width;?>;
                border-style: <?php echo $border_style;?>;
                border-color: <?php echo $border_color; ?>;
                border-radius: <?php echo $border_radius; ?>;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            .product-grid-item {
                padding-left: 10px;
                padding-right: 10px;
            }

            <?php if($border_shadow == ' checked'):?>
            .m360_product_imagewrapper{
                box-shadow: 0px 5px 5px #888888;
            }
            <?php endif;?>
        </style>
        <script>

            var $j = jQuery.noConflict();
            var prodGrid = $j( ".product-grid-item" );
            prodGrid.wrapInner(function() {
                return "<div class='m360_product_imagewrapper'></div>";
            });

        </script>
        <?php
    }

    public function inject_css_to_image() {
        global $product_page;

        $class_name = $product_page->product_options['m360_aspect_ration_class_name'];
        $height = $product_page->product_options['m360_aspect_ration_class_height'];

        ?>
        <style>
            .m360-aspect-ratio{
                height: <?php echo intval($height);?>px;
            }
            .m360-aspect-ratio a{
                height: <?php echo intval($height);?>px;
                text-align: center ;
            }
            .m360-aspect-ratio img{
                max-height: 100%;
                width: auto;
            }
        </style>
        <script>
            var $j = jQuery.noConflict();
            $j( ".<?php echo $class_name?>" ).addClass("m360-aspect-ratio");
        </script>
        <?php
    }

    public function keepHTML(){
        // Disables Kses only for textarea saves
        foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
            remove_filter($filter, 'wp_filter_kses');
        }

        // Disables Kses only for textarea admin displays
        foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
            remove_filter($filter, 'wp_kses_data');
        }
    }

    /* start product hooks functions */
    public function m360_sku_in_additional_information() {
        global $product;
        $sku_content = false;
        if($product->get_sku())$sku_content = '<tr><th>Varenummer: </th><td>'.$product->get_sku().'</td></tr>'
        ?>
        <?php if(is_product() && $sku_content):?>
            <script>
                var $j = jQuery.noConflict();
                $j( ".shop_attributes tbody" ).prepend("<?php echo $sku_content; ?>");
            </script>
        <?php endif; ?>
        <?php
    }


    /* display woocomerce category description */
    function m360_woocommerce_category_image() {
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
        remove_action('woocommerce_before_main_content', 'woocommerce_archive_description', 20);

        if ( is_product_category() ){
            global $wp_query;
            $cat = $wp_query->get_queried_object();
            $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $thumbnail_id );
            if ( $image ) {
                echo '<style>div#content{padding: 0;}</style>';
                echo '<div class="m360_cat_header" style="background-image: url('. $image .')"><div class="m360_cat_header_title"><span>'. $cat->name .'</span></div></div>';
                if(strlen($cat->description)>0){
                    echo '<div class="m360_cat_description_under_header">';
                    echo '<input id="toggle" type="checkbox">';
                    echo '<label for="toggle" id="m360_cat_description_label">Mer om: '.$cat->name.'</label>';
                    echo '<div id="m360_cat_description_expand">';
                    echo '<section id="m360_cat_description_section">';
                    echo '<p>'.$cat->description.'</p>';
                    echo '</section>';
                    echo '</div>';
                    echo '</div>';
                }

            }
        }
    }

    public function m360_add_dotdotdot(){
        global $product_page;

        $product_height = $product_page->product_options[ 'm360_product_limit_line'];
        $title_class = $product_page->product_options['m360_product_limit_line_class_name'];

        if(intval($product_height)>0){
            if(is_product())$product_height=$product_height*0.5;
            ?>
            <style>
                .m360_custom_product_title{
                    height: <?php echo intval($product_height).'px'; ?>;
                    overflow: hidden;
                    width: 98%;
                    padding: 0 2%;
                <?php if(is_product())echo 'margin-bottom: 10px !important;' ?>
                }
            </style>
            <script>
                var $j = jQuery.noConflict();
                $j( ".<?php echo $title_class?>" ).addClass("m360_custom_product_title");

                $j(document).ready(function() {
                    $j('.m360_custom_product_title').dotdotdot();
                });
            </script>
            <?php
        }
    }

    public function m360_add_sku(){
        global $product;
        if($product->get_id()){
            if(is_product_category()){
                echo '<span class="sku_wrapper m360_sku_wrapper">Varenummer: <span class="sku m360_sku">' . $product->get_sku().'</span></span>';
            }else{
                echo '<div class="product_meta m360_product_meta"><span class="sku_wrapper m360_sku_wrapper">Varenummer: <span class="sku m360_sku">' . $product->get_sku().'</span></span></div>';
            }
        }
    }

    /* end product hooks functions */


    /* start price hooks functions */
    /**
     * @param $price
     * @param $product
     * @return string
     */

    public function add_price_tax_button_after_breadcrumbs(){
            $inclVAT = 'true';
            $exclVAT = 'false';
            if(isset($_COOKIE['m360_show_price_with_tax'])) {
                if($_COOKIE['m360_show_price_with_tax'] == 'no'){
                    $inclVAT = 'false';
                    $exclVAT = 'true';
                }
            }

            //echo '<h3>COOKIES: '.print_r($_COOKIE['m360_show_price_with_tax'],true).'</h3>';

            $with_tax = ($inclVAT == 'true')?'checked':'';
            $without_tax = ($exclVAT == 'true')?'checked':'';;

            $html = '<div class="m360_tax_switcher_cat"><form action="" method="POST" id="m360SetTax">';
            $html .= '<div class="verticalInline">';
            $html .= '<span>Vise priser:</span>';
            $html .= '</div>';
            $html .= '<div class="verticalInline">';
            $html .= '<span class="nonRadio '.$without_tax.'">';
            $html .= '<input type="radio" value="'.$inclVAT.'"name="exclVAT" id="priceExclVAT" '.$without_tax.'>';
            $html .= '</span>';
            $html .= '<label for="priceExclVAT" class="selectVAT">ekskl. mva</label>';
            $html .= '<span class="nonRadio '.$with_tax.'">';
            $html .= '<input type="radio" value="'.$exclVAT.'"name="inclVAT" id="priceInclVAT" '.$with_tax.'>';
            $html .= '</span>';
            $html .= '<label for="priceInclVAT" class="selectVAT">inkl. mva</label>';
            $html .= '</div>';
            $html .= '</form></div>';
            ?>
            <script>
                var $j = jQuery.noConflict();
                var navs = $j( "nav" ).toArray();
                if(navs.length >0){
                    //console.log(navs);
                    $j( '<?php echo $html; ?>' ).insertAfter($j(navs[0]));
                }
            </script>
	<?php
    }

    public function show_product_with_and_without_tax($price, $product){

        //return $price;
        global $price_page;

        if (!is_admin() ){
            if($price_page->price_options[ 'show_price_with_and_without_tax'] == ' checked'){
                $med_moms_style = 'style="display: block"';
                $uten_moms_style = 'style="display: none"';

                if(isset($_COOKIE['m360_show_price_with_tax'])) {
                    if($_COOKIE['m360_show_price_with_tax'] == 'no'){
                        $med_moms_style = 'style="display: none"';
                        $uten_moms_style = 'style="display: block"';
                    }
                }
                /*
                $price_without_tax = $product->get_price() - round($product->get_price() * ( 25 / 100 ), 2);
                $price_with_tax = $product->get_price();
                */

                $price_with_tax = $product->get_regular_price();
                if($product->get_sale_price()>0)$price_with_tax = $product->get_sale_price();
                //$price_without_tax = $product->get_price();
                $price_without_tax = wc_get_price_excluding_tax($product);

                $html = '<div class="pris_med_moms" '.$med_moms_style.'>'.wc_price($price_with_tax).'</div>'.'<div class="pris_uten_moms" '.$uten_moms_style.'>'.wc_price($price_without_tax).'</div>';
                return $html;
            }else{
                return $price;
            }
        }else{
            return $price;
        }
    }

    /**
     * hide buy for unlogged users
     */
    public function bbloomer_print_login_to_see() {
        global $price_page;
        $hide_price_message = $price_page->price_options[ 'hide_price_message'];

        echo '<a href="' . get_permalink(wc_get_page_id('myaccount')) . '">' .$hide_price_message. '</a>';
    }

    public function bbloomer_hide_price_add_cart_not_logged_in() {
        if ( !is_user_logged_in() ) {
            remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
            remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
            add_action( 'woocommerce_single_product_summary', array($this,'bbloomer_print_login_to_see'), 31 );
            add_action( 'woocommerce_after_shop_loop_item', array($this,'bbloomer_print_login_to_see'), 11 );
        }
    }

    /* end price hooks functions */

    /* fix woocommerce hierarchical */

    public function disable_paging_for_hierarchical_post_types( $query ) {
        global $current_screen;
        if(!is_object($current_screen))return;

        if ( ! is_admin() || 'nav-menus' !== $current_screen->id ) {
            return;
        }

        if ( ! is_post_type_hierarchical( $query->get( 'post_type' ) ) ) {
            return;
        }

        if ( 50 == $query->get( 'posts_per_page' ) ) {
            $query->set( 'nopaging', true );
        }
    }

    public function remove_limit_for_hierarchical_taxonomies( $args, $taxonomies ) {
        global $current_screen;
        if(!is_object($current_screen))return $args;

        if ( ! is_admin() || 'nav-menus' !== $current_screen->id ) {
            return $args;
        }

        if ( ! is_taxonomy_hierarchical( reset( $taxonomies ) ) ) {
            return $args;
        }

        if ( 50 == $args['number'] ) {
            $args['number'] = '';
        }

        return $args;
    }

    public function remove_page_links_for_hierarchical_taxonomies( $selects, $args, $taxonomies ) {
        global $current_screen;
        if(!is_object($current_screen))return $selects;

        if ( ! is_admin() || 'nav-menus' !== $current_screen->id ) {
            return $selects;
        }

        if ( ! is_taxonomy_hierarchical( reset( $taxonomies ) ) ) {
            return $selects;
        }

        if ( 'count' === $args['fields'] ) {
            $selects = array( '1' );
        }

        return $selects;
    }
    /* end fixingin woocommerce hierarchical */

}