��    `        �         (     )     -  	   4     >     K  
   Z     e     l     q  
   x     �     �     �     �     �     �     �     �     �     �     �     �     �     �     	     	     "	     '	     -	     C	     T	     Y	     ^	     a	     j	     {	  	   	  	   �	     �	  	   �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	  
   �	  
   
     
     
     2
     E
     V
     c
     p
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  	   	  	             .     7     R     m     {     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �  *  �                    +     :     Q  	   Z     d     j  
   o     z     �     �     �  
   �     �     �     �     �     �     �  
   �     �     �          #     +     0     7     O     g     l     r  	   u          �  
   �  	   �     �     �     �     �     �  
   �  	   �     �     �                  
   !  
   ,     7  
   C     N     a     t     �     �     �  	   �     �     �     �     �     �     
               )     -  
   <  
   G     R     Z     h     q     �     �     �  
   �     �     �     �     �     �     �     �     �                           
        (   "   M             F       -      &       A       6       4   0   5       @       B   I      :         U   $       T   L   =   R       D           P   H   K   J          \            ,      `      !                     V   S   *         O   '   	           [             Z       C           /               9   X         ]      Q   ?   8       ^   Y          W      2   +   
   <      1               %   G           #       3   7   N   _       >      ;                 .   )          E    ASC Active Add Value All Products Average rating Background Border Both Bottom Categories Class Clear all filters Close filter Color Compare Content Count DESC Default Delete Description Disabled Filter Filter Preset Filter Products Filter selected Flat Found Found a single result General Settings HTML Hide ID In Stock Instock Products Key Load More Max Price Maximum Meta key. Min Price Minimum Name Newness None Normal Number Order Order By Out Of Stock Overrides Pagination Popularity Price Range Price: high to low Price: low to high Product Category Product Meta Product Name Product Tag Products Products Per Page Random Products Range Review Count Sale Products Search Search Products Settings Show Show Categories Show Filter Show Less Show More Show More Button Show all Show only products on sale Show out of stock products Show products Showing Showing all Slug Sort By Style Tags Text Thumbnail Title Top Total Type Value White of results Project-Id-Version: WooCommerce Product Filter 6.5.6
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/prdctfltr
POT-Creation-Date: 2018-03-31 15:20:07+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-05-07 09:21+0200
Language-Team: 
X-Poedit-KeywordsList: esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.0.7
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nb_NO
X-Poedit-SearchPath-0: ..
 Stigende Aktiv Legg til verdi Alle produkter Gjennomsnittsrangering Bakgrunn Kantlinje Begge Bunn Kategorier Klasse Fjern alle filtre Lukk filter Farge Sammenlign Innhold Antall Synkende Standard Slett Beskrivelse Deaktivert Filtrer Fjern forhåndssatt filter Filtrer produkter Filtrer Flat Funnet Fant et enkelt resultat Generelle innstillinger HTML Skjul ID På Lager Produkter på lager Nøkkel Last Flere Maks Pris Maksimum Meta nøkkel. Min Pris Minimum Navn Nyhetsgrad Nullstill Normal Nummer Rekkefølge Sorter etter Utsolgt Overstyrer Paginering Popularitet Prisklasse Pris: høy til lav Pris: lav til høy Produktkategori Produktmeta Produktnavn produktstikkord Produkter Produkter per side Tilfeldige Produkter Skala Antall anmeldelser Produkter på salg Søk Søk hybler Innstillinger Vis Vis kategorier Vis filter Vis mindre Vis mer Vis mer knapp Vis alle Vis kun produkter på salg Vis utsolgte produkter Vis produkter Viser Viser alle Slug Sorter Etter Stil Stikkord Tekst Miniatyrbilde Tittel Topp Total Type Verdi Hvit av resultater 