<?php
/*
Plugin Name: M360 cache
Description: Helper plugin for caching certain page elements
Version: 1.0.0
Author: Patrick De Amorim
*/

if(!class_exists('Predis\Client')) {
    require __DIR__ . "/includes/predis-1.1/src/Autoloader.php";
    Predis\Autoloader::register();
}

$GLOBALS['M360_Redis'] = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => WP_REDIS_HOST,
    'port'   => 6379,
    'password' => WP_REDIS_PASSWORD
]);



function m360Cache_get($key) {
    global $M360_Redis;
    return $M360_Redis->get(WP_CACHE_KEY_SALT . '-m360_cache-' . $key);
}

function m360Cache_set($key, $val) {
    global $M360_Redis;
    return $M360_Redis->set(WP_CACHE_KEY_SALT . '-m360_cache-' . $key, $val);
}