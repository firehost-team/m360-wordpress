<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_functions.php' );				
class M360PCKOrderStatus{
	
	const PCK_ORDERSTATUS_FULLYDELIVERED = 3; // when slett restlinjer is  Checked
	const PCK_ORDERSTATUS_PARTDELIVERED = 5; // when slett restlinjer is NOT Checked
	const PCK_ORDERSTATUS_FAILED = 7;
	const PCK_RESEND_CREDIT_APPLICANT = 10;
    const PCK_RESEND_CREDIT_APPLICANT2 = 11;

	const STATUS_NEW = 'new';
	const STATUS_SUBMITTED = 'submitted';
  	const STATUS_CANCELED = 'canceled';
  	const STATUS_PARTLYDELIVERED = 'partly';
  	const STATUS_DELIVERED = 'completed';
	
	
	public static function GetStatus($order_id){
		global $wpdb;

        //$table_name = $wpdb->base_prefix."m360_pckasse_orderstatus";
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        //$attID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title like '$post_title'");
		return $wpdb->get_var("SELECT status FROM $table_name WHERE order_id LIKE $order_id");
	}
	
	public static function UpdateOrderStatus($order_id,$wc_order_total=0,$wc_order_delivered=0,$status='new',$nettbutikk_id=0,$total_submitted=0){
        //WriteLog(__FUNCTION__.' '.$order_id.' total_ordered: '.$wc_order_total);
        //WriteLog('Total_submitted: '.$total_submitted);

		global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
			CreateOrderStatusTables();
		}
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);

        $pckws_id = $wpdb->get_var("SELECT pckws_id FROM $table_name WHERE order_id LIKE $order_id");
		if(!$pckws_id){
			$wpdb->insert( 
						$table_name, 
						array( 
							'order_id' => $order_id,
							'wc_order_total'=>$wc_order_total,
				  			'wc_order_delivered'=>$wc_order_delivered,
							'status' => $status,
                            'nettbutikk_id' => $nettbutikk_id,
                            'total_submitted'=> $total_submitted,
						) 
					);
		}else{			
			$wpdb->update(
			$table_name,
			array('pckws_id'=>$pckws_id,
				  'order_id'=>$order_id,
				  'wc_order_total'=>$wc_order_total,
				  'wc_order_delivered'=>$wc_order_delivered,
				  'status'=>$status,
                  'nettbutikk_id' => $nettbutikk_id,
                'total_submitted'=> $total_submitted),
				 array('pckws_id'=>$pckws_id));
		}
		
	}
	
	public static function UpdateShipmentStatus($send_id,$shipment_id=' ',$nettbutikk_id=0){
		global $wpdb;		
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
			CreateShipmentsTables();
		}
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);

        $_id = $wpdb->get_var("SELECT id FROM $table_name WHERE send_id LIKE $send_id");
		if(!$_id){
			$wpdb->insert(
			$table_name,
			array('send_id'=>$send_id,
				'shipment_id'=>$shipment_id,'nettbutikk_id' => $nettbutikk_id));
		}else{
			$wpdb->update(
			$table_name,
			array('id'=>$_id,
				'send_id'=>$send_id,
				'shipment_id'=>$shipment_id,'nettbutikk_id' => $nettbutikk_id),
				array('id'=>$_id));
		}
		
	}

    public static function UpdateItemsDeleverd($order_id,$wc_item_id,$count,$nettbutikk_id=0){

        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_items_delevered");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateItemsDeleveredTables();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);
        $pckws_item_id = $wpdb->get_var("SELECT pckws_item_id FROM $table_name WHERE order_id = $order_id AND item_id = $wc_item_id AND nettbutikk_id = $nettbutikk_id");

        if(!$pckws_item_id){
            $wpdb->insert(
                $table_name,
                array('order_id' => $order_id,
                    'item_id'=>$wc_item_id,
                    'count'=>$count,
                    'nettbutikk_id' => $nettbutikk_id)
            );
        }else{
            $wpdb->update(
                $table_name,
                array('count'=>$count,'nettbutikk_id' => $nettbutikk_id),
                array('item_id'=>$wc_item_id));
        }

    }

}

