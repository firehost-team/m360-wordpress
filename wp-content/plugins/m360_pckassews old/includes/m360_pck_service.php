<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_send.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_receive.php' );
require_once( plugin_dir_path( __FILE__ ) . 'm360_pck_receive_samling.php');

class customer{
    public $contactId;
    public $pckCustomerId;
    public $email;
    public $fax;
    public $invoiceAddress;
    public $invoiceAddress2;
    public $invoiceOnEmail;
    public $invoicePostCity;
    public $invoicePostNo;
    public $orgNo;
    public $phone1;
}
class currentPCKasse {
    public $PCKasseNr;
    public $PCKLocation;
    public $PCKasseUserName;
    public $PCKassePassword;
    public $PCKasseLicense;
    public $PCKasseIsDefault;
}

$current_PCK 			= new currentPCKasse();

class M360PCKasseServices{
    private $_errorMessage;
    public function setErrorMessage($message){
        $this->_errorMessage = $message;
    }


    private function setTheCurrenPCKasse($pckassenr,$setupOptions,$key1){
        $username   = $setupOptions[$key1]['username'];
        $location   = $setupOptions[$key1]['location'];
        $password   = $setupOptions[$key1]['password'];
        $lisens     = $setupOptions[$key1]['license'];
        $default    = $setupOptions[$key1]['default'];

        global $current_PCK;
        $current_PCK = new currentPCKasse();
        $current_PCK->PCKasseNr = $pckassenr;

        $current_PCK->PCKLocation = $location;
        $current_PCK->PCKasseUserName = $username;
        $current_PCK->PCKassePassword = $password;
        $current_PCK->PCKasseLicense = $lisens;
        $current_PCK->PCKasseIsDefault = $default;
    }

    public function login($credentials){
	    $admin_email = get_option('admin_email');
	    $admin_user = get_user_by('email',$admin_email);
	    $admin_id = $admin_user->ID;
	    $session = WP_Session_Tokens::get_instance( $admin_id );


        $pckassenr = getKasseNrForCredintals($credentials->login,$credentials->password);
        $setupOptions = get_option( 'm360_pck_options_setup' );


	    if(!$session){
            $api_result = APICheckLicence();
            if($api_result->status == 'found'){
	            wp_set_auth_cookie($admin_id);
            }else{
                $this->_errorMessage = 'Du trenger å registere din PCKasse med M360';
                return false;
            }
        }

        $key1 = 'pck_credintal_'.$pckassenr.'';
        $this->setTheCurrenPCKasse($pckassenr,$setupOptions,$key1);

        global $current_PCK;
        $this->_errorMessage = false;

        if(($credentials->login == $current_PCK->PCKasseUserName) && ($credentials->password ==$current_PCK->PCKassePassword)) {
            return true;
        } else {
            $this->_errorMessage = 'Access denied. Wrong login or password';
            return false;
        }
    }

    protected function sendNewCompanyEmail($login,$passowrd,$toEmail, $toName){
        $html = '<!DOCTYPE html><html><head><meta charset="iso-8859-1"/>';
        $html .= '<meta http-equiv="content-type" content="application/xhtml+xml; charset=iso-8859-1" /></head>';
        $html .= '<body><div>';
        $html .= 'Hei '.$toName.'<br /> Her er din brukernavn og passord til din Woocommerce nettbutikk<br/>';
        $html .= 'Brukernavn: '.$login.'<br/>';
        $html .= 'Passord: '.$passowrd.'<br/>';
        $html .= 'Det blir lagret automatisk på PCKasse settings i magento<br />Mvh PCKasse';
        $html .= '</div></body></html>';
        function m360_set_html_mail_content_type() {
            return 'text/html';
        }
        add_filter( 'wp_mail_content_type', 'm360_set_html_mail_content_type' );
        try {
            wp_mail( $toEmail, 'Din PCkasse WebShop login', $html );
        } catch (Exception $e) {
            //WriteLog('oops: ' . $e->getMessage());
        }

    }

    public function createWebshop($data){
        //WriteLog(__FUNCTION__.' '.print_r($data,true));
        $webcompany = $data->webcompany;
        $login = mt_rand(10000000, 99999999);
        $password = wp_generate_password( 10, true, true );

        $postAddressLine1 = (strlen($webcompany->deliveryAddressLine1)>0)?$webcompany->deliveryAddressLine1:$webcompany->postAddressLine1;
        $postCity = (strlen($webcompany->deliveryCity)>0)?$webcompany->deliveryCity:$webcompany->postCity;
        $postZipCode = (strlen($webcompany->deliveryZipCode)>0)?$webcompany->deliveryZipCode:$webcompany->postZipCode;

        $location = $postAddressLine1.','.$postCity.','.$postZipCode;
        $kasse_nummer = getKasseNrForLocation($location);

        if($kasse_nummer == -1)$kasse_nummer = 0;
        $api_result = APICheckLicence();


        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $saved_key = isset( $setupTabOptions[ 'm360_pck_license' ] ) ? esc_attr( $setupTabOptions[ 'm360_pck_license' ] ) : '';
        if($api_result->status == 'found' && $saved_key == $api_result->pck_key){
            $data_update = array("method"=> "update","key"=>$api_result->pck_key,"pck"=>json_encode($webcompany));
            CallLicenceAPI("POST",json_encode($data_update));
            //WriteLog('Update_result: '.print_r($update_result,true));
            $result = array(
                'adminUserName' => (string)$webcompany->emailWebshopInfo,
                'adminUserPassword' => '',
                'deltasoftId' => (int)$login,
                'insertUpdate' => $this->_getStatus(false,$this->_errorMessage),
                'password' => (string)$password);

            $setupTabOptions = get_option( 'm360_pck_options_setup' );

            $setupTabOptions['m360_number_of_pckasser'] = $kasse_nummer+1;
            $setupTabOptions['m360_pck_license'] = $api_result->pck_key;

            $setupTabOptions['pck_credintal_'.$kasse_nummer]['location'] = $location;
            $setupTabOptions['pck_credintal_'.$kasse_nummer]['license'] = $webcompany->licenseNo;
            $setupTabOptions['pck_credintal_'.$kasse_nummer]['username'] = $login;
            $setupTabOptions['pck_credintal_'.$kasse_nummer]['password'] = $password;
            $is_default = ($kasse_nummer == 0)?true:false;
            $setupTabOptions['pck_credintal_'.$kasse_nummer]['default'] = $is_default;
            update_option("m360_pck_options_setup",$setupTabOptions);

            $this->sendNewCompanyEmail($login,$password,$webcompany->emailWebshopInfo,$webcompany->name);
            addKasse($kasse_nummer,$location,$webcompany->licenseNo,$login,$password,$is_default);
	        update_option('pck_company_'.$kasse_nummer,serialize($webcompany),'yes');
	        return $this->_getReturn($result);
        }else{
            return $this->_getReturn($this->_getStatus(true,'Error while creating webshop'));
        }

    }

    public function updateStockCount($data){
        $_error = false;
        //$_product_found = false;
        if ($this->login($data)){
            $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');
            if($creation_type == 'samling_samme_vare_nr'){
                $result = M360PCKReceiverSamlin::SamlingUpdateProductStockLevel($data->updateStock);

                if(is_string($result) && strlen($result)>0){
                    $this->_errorMessage = 'Error while updating stock level';
                    $_error = true;
                }

            }else{
                $result = M360PCKReceiver::UpdateProductStockLevel($data->updateStock);
                if(is_string($result) && strlen($result)>0){
                    return $this->_getReturn($this->_getStatus(false,$result));
                }

            }

        }else{
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    private function getOrderLines($orderInfo){
        $orderLines = $orderInfo->orderLines;
        if(is_object($orderLines)) {
            $orderLines = array($orderLines);
        }
        return $orderLines;
    }

    private function getOrderLines_amount($orderLines){
        $orderLines_amount = 0;
        foreach ($orderLines as $orderLine){
            $orderLines_amount += $orderLine->amount;
        }
        return $orderLines_amount;
    }

    private function clean_order($orserLines_to_delete,$wc_order,$canceled=false,$freight=0,$reson='',$extra_amount=0){

        $wc_order_items = $wc_order->get_items();
        $itemsLevert = get_post_meta($wc_order->get_id(),'m360_pck_items_delevered',true);

        $_totalAmount = 0;
        foreach($orserLines_to_delete as $line){
            $_totalAmount += $wc_order->get_item_total( $line,true,false )*$line['qty'];
        }

        $refunded = true;
        $refund = wc_create_refund( array(
            'amount'     => $_totalAmount+$freight+$extra_amount,
            'reason'     => $reson,
            'order_id'   => $wc_order->get_id(),
            'line_items' => $orserLines_to_delete
        ) );
        if ( is_wp_error( $refund )) {
            $refunded = false;
        }else{
            $refunded_ammount = $wc_order->get_total_refunded();
            $payment_gateways = wc_get_payment_gateway_by_order($wc_order);
            if($payment_gateways->supports( 'refunds' )){
                $result = $payment_gateways->process_refund( $wc_order->get_id(), $refunded_ammount, $reson);
                if ( is_wp_error( $result ) ) {
                    WriteLog(print_r($result,true));
                } elseif ( ! $result ) {
                    $this->_errorMessage =  new WP_Error( 'woocommerce_rest_create_order_refund_api_failed', __( 'An error occurred while attempting to create the refund using the payment gateway API.', 'woocommerce' ), 500 );
                }else{
                    WriteLog('klarna refunded_ammount '.$refunded_ammount);
                }
            }
            if(!empty($reson))$wc_order->add_order_note($reson, true);
            if($canceled){
                $wc_order->add_order_note('Order deleted from Woocommerce by PCK', false);
                $wc_order->update_status( 'cancelled' );
            }

            if($wc_order->get_remaining_refund_amount()<=0)$wc_order->update_status( 'refunded' );
            else if($wc_order->status == 'processing' && $wc_order->get_remaining_refund_amount() > 0 && count($wc_order_items) == $itemsLevert)$wc_order->update_status( 'completed' );
            else $wc_order->update_status( 'completed' );
        }

        return $refunded;
    }
    /*
        private function getLineItem($line_item_id){
            global $wpdb;
            return $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE order_item_id = ". $line_item_id,OBJECT);

        }
    */
    private function createCopyItem($item,$qty){
        $itemTotal = $item['line_total']/$item['qty'];
        $itemTax = $item['line_tax']/$item['qty'];

        $itemSubtotal = $item['line_subtotal']/$item['qty'];
        $itemSubtax = $item['line_subtotal_tax']/$item['qty'];

        $copyItem = $item;
        $copyItem['qty'] = $qty;

        $copyItem['line_total'] = $itemTotal*$qty;
        $copyItem['line_tax'] = $itemTax*$qty;
        $copyItem['line_subtotal'] = $itemSubtotal*$qty;
        $copyItem['line_subtotal_tax'] = $itemSubtax*$qty;
        return $copyItem;
    }

    public function checkIfOrderHaveBundelProduct($order){
        $bundel_product = false;
        $items = $order->get_items();
        foreach($items as $item){
            $wc_prod = wc_get_product($item['product_id']);
            if(is_object($wc_prod) && $wc_prod->get_type() == 'yith_bundle'){
                $bundel_product = $wc_prod;
                break;
            };
        }
        return $bundel_product;
    }

    private function empty_respose($authorzationId,$freightCost,$paymentMethod){
        $emptyResponse = array(
            'amount' => 0,
            'authorzationId' => $authorzationId,
            'extraCost' => 0,
            'freightCost' => $freightCost,
            'insertUpdate' => $this->_getStatus(false),
            'paymentMethod' => $paymentMethod);

        return $emptyResponse;
    }

    private function klarna_order($order_id){
        $request      = new WC_Klarna_Order_Management_Request( array(
            'request'  => 'retrieve',
            'order_id' => $order_id,
        ) );
        $klarna_order = $request->response();
        return $klarna_order;
    }

    private function send_update_order_lines_to_klarna($order_id){
        if(class_exists('WC_Klarna_Order_Management_Request')){
            $klarna_order = $this->klarna_order($order_id);
            $request  = new WC_Klarna_Order_Management_Request( array(
                'request'      => 'update_order_lines',
                'order_id'     => $order_id,
                'klarna_order' => $klarna_order,
            ) );
            $response = $request->response();
            return $response;
        }
        return true;
    }

    private function delete_rest_lines($items_to_delete,$items_to_delever,$wc_order,$leverNow){
        $product_to_delete_note = '';
        if(count($items_to_delete)>0){
            foreach ($items_to_delete as $key =>$item_to_delete_array){
                $item_to_delete = $item_to_delete_array['item_to_delete'];
                $item = $wc_order->get_item($item_to_delete->get_id());
                $product = $item->get_product();

                $qty_to_delete = $item_to_delete_array['qty_to_delete'];
                $item_to_delete_name = $item->get_name();

                $old_qty = ($item->get_quantity()>0)?$item->get_quantity():1;
                $new_qty = $old_qty-$qty_to_delete;
                $line_total = $wc_order->get_line_total( $item,false,false );
                $price = $line_total/$old_qty;

                $item->set_quantity($new_qty);
                $item->set_total($price*$new_qty);

                if($new_qty<=0 || !array_key_exists($key,$items_to_delever)){
                    $wc_order->remove_item($item->get_id());
                    $product_to_delete_note .= $item_to_delete_name.'('.$product->get_sku().') ble slette, ';
                }else{
                    $product_to_delete_note .= $item_to_delete_name.'('.$product->get_sku().') antall endret fra '.$old_qty.' til '.$new_qty.', ';
                }
                $item->save();
            }
        }else{
            foreach ($items_to_delever as $key =>$item_to_delever){
                $item = $wc_order->get_item($item_to_delever->get_id());
                $product = $item->get_product();
                $item_to_delete_name = $item->get_name();

                $old_qty = ($item->get_quantity()>0)?$item->get_quantity():1;
                $line_total = $wc_order->get_line_total( $item,false,false );
                $price = $line_total/$old_qty;

                $item->set_quantity($leverNow);
                $item->set_total($price*$leverNow);

                if($leverNow<=0 || !array_key_exists($key,$items_to_delever)){
                    $wc_order->remove_item($item->get_id());
                    $product_to_delete_note .= $item_to_delete_name.'('.$product->get_sku().') ble slette, ';
                }else{
                    $product_to_delete_note .= $item_to_delete_name.'('.$product->get_sku().') antall endret fra '.$old_qty.' til '.$leverNow.', ';
                }
                $item->save();
            }
        }
        //WriteLog('$product_to_delete_note: '.$product_to_delete_note);
        $wc_order->calculate_totals();
        $wc_order->save();
        if(strlen($product_to_delete_note) >0){
            $product_to_delete_note =  rtrim($product_to_delete_note,', ');
            $product_to_delete_note = 'PCKasse har endret ordrelinjer ('.$product_to_delete_note.').';
        }
        return $product_to_delete_note;
    }
    public function updateOrderStatus($data){

        if ($this->login($data)){
            $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
            $valgt_status = 'completed';
            $ordersTabOptions = get_option( 'm360_pck_options_orders' );

            if(isset($ordersTabOptions[ 'm360_pck_credit_order_status'] )){
                $valgt_status = $ordersTabOptions[ 'm360_pck_credit_order_status'];
            }

            $orderInfo = $data->updateOrder;

            $items_to_delete = array();
            $items_to_delever = array();
            $_totalAmount = 0;
            $_amount = 0;
            $is_bundle = false;

            $shipment_id = '';
            $status = 'completed';
            $wc_order = wc_get_order($orderInfo->deltaOrderId);
            $paymentMethod = $wc_order->get_payment_method_title();
            if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
                $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
                if( !is_numeric($isCredit)){
                    if($wc_order->get_payment_method() == $m360_pck_options_orders[ 'm360_pck_credit_order' ]){
                        $paymentMethod = 3;
                    }
                }
            }

            $wc_order_items = $wc_order->get_items();
            $wc_order_qty = $wc_order->get_item_count();//count($wc_order_items);//(integer)QtyOrdered($orderInfo->deltaOrderId);
	        $kasse_nr = retriveKasseNr();

            if($orderInfo->orderStatusId == M360PCKOrderStatus::PCK_RESEND_CREDIT_APPLICANT ||
                $orderInfo->orderStatusId == M360PCKOrderStatus::PCK_RESEND_CREDIT_APPLICANT2){
	            update_post_meta($wc_order->get_id(),'_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,false);
	            $wc_order->add_order_note('order nr: '.$wc_order->get_id().' klarte ikke å sende til PCKasse , kunde må godkjenne for kredit ordre');
                return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$orderInfo->message .' for order nr. '.$orderInfo->deltaOrderId)));

            }else if($orderInfo->orderStatusId == M360PCKOrderStatus::PCK_ORDERSTATUS_FAILED) {
                update_post_meta($wc_order->get_id(),'_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,false);
                $wc_order->add_order_note('order nr: '.$wc_order->get_id().' klarte ikke å sende til PCKasse');
                return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$orderInfo->message .'order nr. '.$orderInfo->deltaOrderId)));
            }

            foreach ($wc_order_items as $wc_order_item){
                $item_product = $wc_order_item->get_product( );
                if(is_object($item_product) && $item_product->get_type( )== 'yith_bundle'){
                    $is_bundle = true;
                    break;
                }
            }
            $_freightCost = $wc_order->calculate_shipping()+$wc_order->get_shipping_tax();
            $authorzationId = get_post_meta($wc_order->get_id(), '_order_key',true);
            $itemsLevert = get_post_meta($wc_order->get_id(),'m360_pck_items_delevered',true);
            $price_to_substract = 0;

            $orderLines = $this->getOrderLines($orderInfo);
            $orderLines_amount = $this->getOrderLines_amount($orderLines);
            $leverNow = $orderLines_amount+$itemsLevert;
            //WriteLog('$leverNow: '.$leverNow);
            update_post_meta($wc_order->get_id(),'m360_pck_items_delevered',$leverNow);

            // new to skip cancelled or refunded ordered //
            if($wc_order->get_status() == 'refunded' || $wc_order->get_status() == 'cancelled'){
                $extraCost = 0;
                $response = array(
                    'amount' => $this->formatDecimal($_amount)+$this->formatDecimal($extraCost)+$this->formatDecimal($_freightCost),
                    'authorzationId' => $authorzationId,
                    'extraCost' => $this->formatDecimal($extraCost),
                    'freightCost' => $this->formatDecimal($_freightCost),
                    'insertUpdate' => $this->_getStatus(false),
                    'paymentMethod' => $paymentMethod);


                return $this->_getReturn($response);
            }
            // ------------------ //
            if(isset($orderInfo->orderLines)){
                if($leverNow < $wc_order_qty && !$this->checkIfOrderHaveBundelProduct($wc_order)){
                    $status = M360PCKOrderStatus::STATUS_PARTLYDELIVERED;
                }

                foreach($orderLines as $orderLine){
                    if(array_key_exists($orderLine->orderLineId,$wc_order_items) ){
                        $item = $wc_order_items[$orderLine->orderLineId];
                        if($item['qty']==$orderLine->amount){
                            $items_to_delever[$orderLine->orderLineId] = $item;
                        }else{
                            $itemToDelever = $this->createCopyItem($item,$orderLine->amount);
                            $items_to_delever[$orderLine->orderLineId] = $itemToDelever;
                        }
                    }
                }

                foreach ($items_to_delever as $key => $item) {
                    $price = $wc_order->get_line_total( $item,true,false );
                    if($price<=0 && isset($item['_wcpdf_regular_price'])){
                        $price_data = $item['_wcpdf_regular_price'];
                        $price = $price_data['excl'];
                    }

                    $_amount = $_amount+$price;
                    $_totalAmount = $_totalAmount+($item['line_subtotal']+$item['line_subtotal_tax']);
                }
            }
            else{
                foreach($itemsLevert as $levertItemRef){
                    $item = $wc_order_items[$levertItemRef->item_id];
                    /* Deleverd item */
                    $items_to_delever[$levertItemRef->item_id] = $this->createCopyItem($item,$levertItemRef->count);
                }
            }

            foreach($wc_order_items as $key => $wc_order_item){
                $qty = $wc_order_item->get_quantity();
                if(array_key_exists($key,$items_to_delever)){
                    $delevedItem = $items_to_delever[$key];
                    if($delevedItem['qty']<$qty){
                        $qty = $qty-$delevedItem['qty'];
                        $items_to_delete[$key] = array('item_to_delete'=>$wc_order_item,'qty_to_delete'=>$qty);//$this->createCopyItem($wc_order_item,$qty);
                    }
                }else{
                    $items_to_delete[$key] = array('item_to_delete'=>$wc_order_item,'qty_to_delete'=>$qty);//$this->createCopyItem($wc_order_item,$qty);
                }
            }

            //WriteLog('items to delete: '.print_r($items_to_delete,true));
            foreach ($items_to_delete as $Key => $item) {
                $item_to_delete = $item['item_to_delete'];
                $price_to_substract = $price_to_substract+$wc_order->get_line_total( $item_to_delete,true,false );
            }

            $extraCost = 0;//($wc_order->get_total()-$price_to_substract)-($_amount+$_freightCost);

            if($orderInfo->orderStatusId == M360PCKOrderStatus::PCK_ORDERSTATUS_FULLYDELIVERED) {
                //$t_refunds = $wc_order->get_refunds();
                if(isset($orderInfo->orderLines)){
                    if($isCredit){
                        $wc_order->update_status($valgt_status,'order '.$wc_order->get_id().' has been sendt to PCK as a credit order' );
                    }else{
                        $product_to_delete_note = $this->delete_rest_lines($items_to_delete,$items_to_delever,$wc_order,$leverNow);
                        if(strlen($product_to_delete_note) >0){
                            $this->send_update_order_lines_to_klarna($wc_order->get_id());
                            $wc_order->add_order_note($product_to_delete_note, false);
                        }
                        if($wc_order->get_status() != 'refunded'){
                            $wc_order->update_status( 'completed' );
                        }
                    }
                    $response = array(
                        'amount' => $this->formatDecimal($_amount)+$this->formatDecimal($extraCost)+$this->formatDecimal($_freightCost),
                        'authorzationId' => $authorzationId,
                        'extraCost' => $this->formatDecimal($extraCost),
                        'freightCost' => $this->formatDecimal($_freightCost),
                        'insertUpdate' => $this->_getStatus(false),
                        'paymentMethod' => $paymentMethod);

                    return $this->_getReturn($response);
                }else{ // TODO: check this function
                    $itemsLevert = get_post_meta($wc_order->get_id(),'m360_pck_items_delevered',true);
                    if(count($itemsLevert)>0){
                        $product_to_delete_note = $this->delete_rest_lines($items_to_delete,$items_to_delever,$wc_order,$leverNow);
                        if(strlen($product_to_delete_note) >0){
                            $wc_order->add_order_note($product_to_delete_note, false);
                            $this->send_update_order_lines_to_klarna($wc_order->get_id());
                        }
                        $response = array(
                            'amount' => $this->formatDecimal($_amount)+$this->formatDecimal($extraCost)+$this->formatDecimal($_freightCost),
                            'authorzationId' => $authorzationId,
                            'extraCost' => $this->formatDecimal($extraCost),
                            'freightCost' => $this->formatDecimal($_freightCost),
                            'insertUpdate' => $this->_getStatus(false),
                            'paymentMethod' => $paymentMethod);
                        return $this->_getReturn($response);
                    }else{
                        $wc_order->add_order_note('Order slettet fra Woocommerce via PCkasse', false);
                        $wc_order->update_status( 'cancelled' );

                        return $this->_getReturn($this->empty_respose($authorzationId,$this->formatDecimal($_freightCost),$paymentMethod));
                    }
                }
            }
            else if($orderInfo->orderStatusId == M360PCKOrderStatus::PCK_ORDERSTATUS_PARTDELIVERED) {
                $response = array(
                    'amount' => $this->formatDecimal($_amount)+$this->formatDecimal($extraCost)+$this->formatDecimal($_freightCost),
                    'authorzationId' => $authorzationId,
                    'extraCost' => $this->formatDecimal($extraCost),
                    'freightCost' => $this->formatDecimal($_freightCost),
                    'insertUpdate' => $this->_getStatus(false),
                    'paymentMethod' => $paymentMethod);
                if($isCredit){
                    if($wc_order->get_status() != $valgt_status){
                        $wc_order->update_status($valgt_status,'order '.$wc_order->get_id().' has been sendt to PCK as a credit order' );
                    }
                }else{
                    if($is_bundle){
                        $wc_order->update_status( 'completed' );
                    }else{
                        if($leverNow >= $wc_order_qty){
                            $wc_order->update_status( 'completed' );
                        }else{
                            $wc_order->update_status( 'processing' );
                        }
                    }
                }

                $message = 'PCK sendt ( '. $leverNow .' ) product to the customer, with sending id ('.$orderInfo->sendId . ')' ;
                $wc_order->add_order_note($message, false);
                return $this->_getReturn($response);
            }
        }
        return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$this->_errorMessage)));
    }

    public function getOrders($data){
        $_error = false;
        $ordersArray = array();

        if($this->login($data)) {
            $ordersTabOptions = get_option( 'm360_pck_options_orders' );
            $can_send_orders = (isset($ordersTabOptions[ 'm360_pck_allow_sending_order_to_pck']))?$ordersTabOptions[ 'm360_pck_allow_sending_order_to_pck']:'dont_send';
            global $current_PCK;
            if(!m360_pck_is_default_pck($current_PCK)){
                $this->_errorMessage = 'Ordre skal bare sendes til den standard PCK, den du kan sette i PCK settings i din nettbutikk backend';

            }else if($can_send_orders == 'dont_send'){
                $this->_errorMessage = 'Transfeering orders from Woocommerce to PCK are not allowed, Please change it from M360 PCK settings in your wordpress store';
                $_error = true;
            }else{
                $m360_pkc_sender = new M360PCKSender();
                $ordersArray = $m360_pkc_sender->getOrderFromWC();
                if(is_string($ordersArray) && strlen($ordersArray)>0){
                    $this->_errorMessage = $ordersArray;
                    return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$this->_errorMessage), 'listWebOrders' => array()));
                }else if(count($ordersArray)<=0){
                    $this->_errorMessage = 'There is no new orders';
                    $_error = ($ordersTabOptions[ 'm360_pck_show_no_orders_to_transfeer_message'] == 'show')?true:false;
                    return $this->_getReturn(array('insertUpdate' => $this->_getStatus($_error,$this->_errorMessage), 'listWebOrders' => array()));
                }else if(!is_array($ordersArray)){
                    $this->_errorMessage = 'Some thing went wrong while sending error to PCKasse';
                    return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$this->_errorMessage), 'listWebOrders' => array()));
                }
            }

        }else {
            $_error = true;
        }
        $r = $this->_getReturn(array('insertUpdate' => $this->_getStatus($_error,$this->_errorMessage), 'listWebOrders' => $ordersArray));
        return $r;

    }

    public function sendImage($data){
        $productTabOptions = get_option( 'm360_pck_options_product' );
        if(isset($productTabOptions[ 'm360_pck_synk_product_images'] )){
            if($productTabOptions[ 'm360_pck_synk_product_images'] == 'no'){
                return $this->_getReturn($this->_getStatus(false,'Syncing av images is not activ in woocommerce backend'));
            }
        }

        $_error = false;


        if ($this->login($data)){
            $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');
            if($data->articleid == -10){
                $result = M360PCKReceiver::UpdateCompanyLogo($data);
                if(is_string($result) && strlen($result)>0){
                    $this->_errorMessage = $result;
                    $_error = true;
                }
            }
            else if(isset($data->image)){
                if ($creation_type == 'samling_samme_vare_nr') {
                    $result = M360PCKReceiverSamlin::SamlingUpdateProductInfo($data->articleid,$data->image);
                    if(is_string($result) && strlen($result)>0){
                        $this->_errorMessage = $result;
                        $_error = true;
                    }
                }
                else{
                    $result = M360PCKReceiver::UpdateProductInfo($data->articleid,$data->image);
                    if(is_string($result) && strlen($result)>0){
                        $this->_errorMessage = $result;
                        $_error = true;
                    }
                }


            }else{
                $result = M360PCKReceiver::removeProductImage($data->articleid);
                if(is_string($result) && strlen($result)>0){
                    $this->_errorMessage = $result;
                    $_error = true;
                }
            }
        }else{
            $_error = true;
        }

        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function sendImageColor($data){
        $_error = false;
        if ($this->login($data)){
            if(isset($data->image)){
                if (!M360PCKReceiver::UpdateExtraImages($data->articleid,$data->imageid, $data->image,false,$data->colorid)){
                    $this->_errorMessage = 'Can upload extra image '.$data->imageid.' for product '.$data->articleid;
                    $_error = true;
                }
            }
        }else{
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function sendColor($data){
        $_error = false;
        if ($this->login($data)){
            /*
            $args = array(
                'description' =>"pck_color_id: {$data->color->colorId}",
                'slug' => "",
                'parent' => ""
            );
            */
            $color_attribute = M360PCKReceiver::colorAttribute();
            $colorTitle = trim($data->color->name);
            //wp_insert_term( $colorTitle, $color_attribute );
            insertTerms($colorTitle, $color_attribute);
        }else{
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }
    public function sendSize($data){
        $_error = false;
        if ($this->login($data)){
            $size_attribute = M360PCKReceiver::sizeAttribute();
            $sizeTitle = trim($data->size->name);
            //wp_insert_term( $sizeTitle, $size_attribute );
            insertTerms($sizeTitle, $size_attribute);
        }else{
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function getSizeColorIdFromArticleId($varID){
        global $wpdb;
        global $current_PCK;

        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;
        $table_name = m360_pck_get_table_name("postmeta");
        $key = '_sizeColorId_'.$kasse_nr;

        $qur = "SELECT post_id FROM $table_name WHERE meta_key = '{$key}' AND meta_value = {$varID} LIMIT 1";
        $sizeColorId = $wpdb->get_var($qur);

        return (integer)$sizeColorId;
    }

    public function testExistingOfProduct($articleId,$articleNo=NULL){

        global $wpdb;
        global $current_PCK;
        $kasse_nr = (is_object($current_PCK))?$current_PCK->PCKasseNr:0;

        $table_name = m360_pck_get_table_name("postmeta");

        $ProductId = $wpdb->get_var($wpdb->prepare(
            "SELECT post_id FROM " . $table_name . " WHERE meta_key = 'm360_pck_article_id_".$kasse_nr."' AND meta_value = '%s' LIMIT 1",$articleId
        ));

        if(is_numeric($ProductId) && $ProductId > 0){return $ProductId;}

        if($ProductId<=0 && $articleNo){
            $ProductId = $wpdb->get_var($wpdb->prepare(
                "SELECT post_id FROM " . $table_name. " WHERE meta_key = '_sku' AND meta_value = '%s' LIMIT 1",$articleNo
            ));

        }

        return $ProductId;
    }
    public function continueCreating($product_info,$samling = false){
        if($product_info->articleStatus != 0 || !$product_info->visibleOnWeb){
            $exits_product_id = $this->testExistingOfProduct($product_info->articleId,$product_info->articleNo);
            if($exits_product_id>0){
                wp_delete_post($exits_product_id,true);
            }
            return false;
        }

        $_error = false;

        if (!M360PCKReceiver::IsExistingProduct($product_info->articleId,$product_info->articleNo)) {
            if($samling) {
                $result = M360PCKReceiverSamlin::CheckTheProductAfterCreatingForSamling($product_info);
                if (is_string($result) && strlen($result) > 0) {
                    if ($result == 'Finished') {
                        $this->_errorMessage = 'Finished';
                        $_error = false;
                    }
                }
            }
            else {
                $result = M360PCKReceiver::InsertProduct($product_info);
                if(is_string($result) && strlen($result)>0){
                    $this->_errorMessage = $result;
                    $_error = true;
                }
            }
        }
        else {
            if($samling){
                $result = M360PCKReceiverSamlin::CheckTheProductAfterCreatingForSamling($product_info);
                if(is_string($result) && strlen($result)>0){
                    if($result == 'Finished'){
                        $this->_errorMessage = 'Finished';
                        $_error = false;
                    }
                }
            }else{
                $result = M360PCKReceiver::UpdateProduct($product_info);
                if (!$result) {
                    $this->_errorMessage = 'Error updating product';
                    $_error = true;
                }
            }
            
        }
        return $_error;
    }
    public function sendArticle($data){
        //WriteLog(print_r($data,true));
        $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');
        $_error = false;
        if ($this->login($data)){
            global $blog_id;
            $main_blog_id =   $blog_id;
            if(is_multisite())switch_to_blog( $main_blog_id );

            $product_info = $data->article;
            if(!$product_info->articleNo){
                return $this->_getReturn($this->_getStatus(true,'Article number (VAREKODE) is missing'));
            }

            if($creation_type == 'samling_samme_vare_nr'){

                $result = M360PCKReceiverSamlin::SamlingSendArticle($product_info);

                if(is_string($result) && strlen($result)>0){
                    if($result == 'continue_creating'){
                        $_error = $this->continueCreating($product_info);
                    }else{
                        $this->_errorMessage = $result;
                        $_error = true;
                    }

                }
            }
            else{
                $_error = $this->continueCreating($product_info);
            }
            if(is_multisite())restore_current_blog();

        }
        else {
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function removeAricle($data){
        $_error = false;
        if ($this->login($data)){
            $main_blog_id =   get_current_blog_id();;
            if(is_multisite())switch_to_blog( $main_blog_id );

            $productTabOptions = get_option( 'm360_pck_options_product' );
            $allowDeleting = 'yes';
            if(isset($productTabOptions[ 'm360_pck_allow_product_deleting'] )){
                $allowDeleting = $productTabOptions[ 'm360_pck_allow_product_deleting'];
            }
            if($allowDeleting == 'no'){
                return $this->_getReturn($this->_getStatus(true,'Deleting av product not allowd in M360 PCKasseWS woocommerce plugin'));
            }
            $articleid = $data->articleid;
            if(!$articleid){
                return $this->_getReturn($this->_getStatus(true,'Article number (VAREKODE) is missing'));
            }
            global $wpdb;
            global $current_PCK;

            $table_name = m360_pck_get_table_name("postmeta");

            $ProductId = $wpdb->get_var($wpdb->prepare(
                "SELECT post_id FROM " . $table_name. " WHERE meta_key = 'm360_pck_article_id_".$current_PCK->PCKasseNr."' AND meta_value = '%s' LIMIT 1", $articleid
            ));
            if ($ProductId) {
                $WC_product = wc_get_product( $ProductId );
                $WC_product->delete(true);
                //wp_delete_post($ProductId,true);
            } else {
                $_error = true;
                $this->_errorMessage = 'product '.$articleid.' '.$ProductId.'not found';
            }
            if(is_multisite())restore_current_blog();

        }else {
            $_error = true;
        }
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function sendArticleGroup($data){
        $_error = false;

        if ($this->login($data)){
            $this->_errorMessage = 'Sending direct varegruppe direct from pck is Not implemented, please send it ';
        }else {
            $_error = true;
        }

        //$_error = true;
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function sendManufacturer($data){
        $_error = false;

        if ($this->login($data)){
            $this->_errorMessage = 'Sending  Manufacturer direct from pck is Not implemented, please send it ';
        }else {
            $_error = true;
        }

        //$_error = true;
        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function creditOrder($data){
        $_totalAmount = 0;
        $price_to_substract = 0;
        $_paymentMethod = '';
        $authorzationId = '';
        $_error = false;
        $extra_amount = 0;
        //$frightCoast = 0;
        $freight_to_credit = 0;

        $items_to_credit = array();
        $items_to_keep = array();

        if($this->login($data)) {
            $order = wc_get_order($data->orderId);
            $frightCoast = $order->calculate_shipping()+$order->get_shipping_tax();

            if ($order->get_id()) {
                $itemsLevert = get_post_meta($order->get_id(),'m360_pck_items_delevered',true);
                $_paymentMethod = $order->payment_method_title;
                $authorzationId = get_post_meta($order->get_id(), '_order_key', true);
                $orderItems = $order->get_items();

                if (isset($data->orderLine)) {
                    $orderLines = $data->orderLine;
                    if (is_object($orderLines)) $orderLines = array($orderLines);

                    $hasItemsToCredit = false;
                    foreach ($orderLines as $line) {
                        if ($line->orderLineId == "-10") {
                            $freight_to_credit = $frightCoast;
                        }else{
                            $hasItemsToCredit = true;
                            $item = $orderItems[$line->orderLineId];
                            $itemTotal = $item['line_total']/$item['qty'];
                            $itemTax = $item['line_tax']/$item['qty'];
                            $itemSubtotal = $item['line_subtotal']/$item['qty'];
                            $itemSubtax = $item['line_subtotal_tax']/$item['qty'];

                            if($item['qty'] == $line->amount){
                                $items_to_credit[$line->orderLineId]=$item;
                            }
                            else{
                                $item['qty'] = $line->amount;

                                $item['line_total'] = $itemTotal*$line->amount;
                                $item['line_tax'] = $itemTax*$line->amount;
                                $item['line_subtotal'] = $itemSubtotal*$line->amount;
                                $item['line_subtotal_tax'] = $itemSubtax*$line->amount;
                                $items_to_credit[$line->orderLineId]=$item;
                            }
                        }
                    }

                    if($hasItemsToCredit){
                        foreach($itemsLevert as $item_levert){
                            $levert_item_id = $item_levert->item_id;
                            $levert_item_count = $item_levert->count;

                            $item = $orderItems[$levert_item_id];
                            $itemTotal = $item['line_total']/$item['qty'];
                            $itemTax = $item['line_tax']/$item['qty'];
                            $itemSubtotal = $item['line_subtotal']/$item['qty'];
                            $itemSubtax = $item['line_subtotal_tax']/$item['qty'];

                            if(array_key_exists($levert_item_id,$items_to_credit) ) {
                                $item_in_credit = $items_to_credit[$levert_item_id];
                                if($levert_item_count != $item_in_credit['qty']){
                                    $itemsToKeep = $levert_item_count-$item_in_credit['qty'];
                                    if($itemsToKeep >0){
                                        $itemToKeep = $orderItems[$levert_item_id];
                                        $itemToKeep['qty'] = $itemsToKeep;

                                        $itemToKeep['line_total'] = $itemTotal*$itemsToKeep;
                                        $itemToKeep['line_tax'] = $itemTax*$itemsToKeep;
                                        $itemToKeep['line_subtotal'] = $itemSubtotal*$itemsToKeep;
                                        $itemToKeep['line_subtotal_tax'] = $itemSubtax*$itemsToKeep;
                                        $items_to_keep[$line->orderLineId]=$itemToKeep;
                                    }
                                }
                            }
                            else{
                                $item_to_keep = $orderItems[$levert_item_id];

                                $item_to_keep['qty'] = $levert_item_count;

                                $item_to_keep['line_total'] = $itemTotal*$levert_item_count;
                                $item_to_keep['line_tax'] = $itemTax*$levert_item_count;
                                $item_to_keep['line_subtotal'] = $itemSubtotal*$levert_item_count;
                                $item_to_keep['line_subtotal_tax'] = $itemSubtax*$levert_item_count;

                                $items_to_keep[$levert_item_id]=$item_to_keep;

                            }

                        }
                    }
                }
                if (isset($data->amount)){
                    $extra_amount = $data->amount;
                }
            }
            else{
                return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,'Order# ' . $data->orderId.' not found')));

            }

            if(count($items_to_credit)>0){
                foreach($items_to_credit as $item_to_credit){
                    if(($item_to_credit['line_total']+$item_to_credit['line_tax']) <= 0  && isset($item_to_credit['_wcpdf_regular_price'])){
                        $price_data = $item_to_credit['_wcpdf_regular_price'];
                        $_totalAmount = $_totalAmount+ $price_data['excl'];
                    }else{
                        $_totalAmount = $_totalAmount+($item_to_credit['line_total']+$item_to_credit['line_tax']);
                    }
                }

                foreach($items_to_keep as $item_to_keep){
                    if(($item_to_credit['line_total']+$item_to_credit['line_tax']) <= 0  && isset($item_to_keep['_wcpdf_regular_price'])){
                        $price_data = $item_to_keep['_wcpdf_regular_price'];
                        $price_to_substract = $price_to_substract+ $price_data['excl'];
                    }else{
                        $price_to_substract = $price_to_substract+($item_to_keep['line_total']+$item_to_keep['line_tax']);
                    }

                }

                $extraCost = 0;//($order->get_remaining_refund_amount()-$price_to_substract)-($_totalAmount+$frightCoast);
            }
        }

        if(!$_error){
            $result = array(
                'amount' => $_totalAmount+$freight_to_credit+$extraCost+$extra_amount,
                'authorzationId' => $authorzationId,
                'extraCost' => $extraCost-$extra_amount,
                'freightCost' => $freight_to_credit,
                'insertUpdate' => $this->_getStatus($_error,$this->_errorMessage),
                'paymentMethod' => $_paymentMethod);

            if(count($items_to_credit)>0 || $freight_to_credit > 0 || $extra_amount > 0)$this->clean_order($items_to_credit,$order,false,$freight_to_credit,$data->reason,$extra_amount);
            return $this->_getReturn($result);
        }else{
            return $this->_getReturn(array('insertUpdate' => $this->_getStatus(true,$this->_errorMessage.' Error while crediting Order# ' . $data->orderId)));
        }

    }

    protected static function m360_pck_create_roles($customerGroup) {
        global $wp_roles;

        if ( ! class_exists( 'WP_Roles' ) ) {
            return;
        }

        if ( ! isset( $wp_roles ) ) {
            $wp_roles = new WP_Roles();
        }

        $role = get_role($customerGroup->name);
        if(!$role){
            $role = add_role( $customerGroup->name, $customerGroup->name, array(
                'read' 						=> true,
                'edit_posts' 				=> false,
                'delete_posts' 				=> false
            ) );
        }

        if(!$role)return get_role('customer');
        return $role;
    }

    public function getWelcomeMailTemplate($data){
        $_error = false;
        if ($this->login($data)){
            $mailTemplate = array(
                'footer' => 'footer',
                'header' => 'header',
                'message' => 'message',
                );
            $response = array(

                'insertUpdate' =>$this->_getStatus($_error,$this->_errorMessage),
                'mailTemplate' => $mailTemplate);

            return $this->_getReturn($response);
        }else{
            $_error = true;
        }

        return $this->_getStatus($_error,$this->_errorMessage);
    }
    protected function createCustomer($data){
        $role = $this->m360_pck_create_roles($data->customerGroup);
        $email = $data->email;
        if(strlen($email)<=0){
            $this->_errorMessage = 'Email is required for webshop customer registration';
            return false;
        }
        /*
        $fullName = explode(" ", $data->name);
        $firstName = (count($fullName)>=1)?$fullName[0]:'';
        $b = array_slice($fullName, -1);
        $lastName = (count($fullName)>=2)?array_pop($b):'';
        */

        $firstName = current( explode( ' ', $data->name ) );
        $lastName = end( explode( ' ', $data->name ) );

        if(strlen($firstName)<=0)$firstName = current( explode( '@', $email ) );
        if(strlen($lastName)<=0 || $firstName == $lastName)$lastName = end( explode( '@', $email ) );

        if($data->deltaCustomerId > 0){$user = get_user_by( 'id', $data->deltaCustomerId);
        }else{$user = get_user_by( 'email', $email);}

        if(!$user){
            $password = wp_generate_password( 12, true, true );
            //$user_id = wp_create_user(sanitize_email($email),$password,sanitize_email($email));
            //$newusername = sanitize_user( current( explode( '@', $email ) ), true );
            $newusername = sanitize_user($email, true );
            $user_id = wc_create_new_customer( sanitize_email( $email ), wc_clean( $newusername ), $password );
            $setupTabOptions = get_option( 'm360_pck_options_setup' );
            $send_email = isset($setupTabOptions[ 'm360_pck_send_new_user_email'] )?$setupTabOptions[ 'm360_pck_send_new_user_email']:'no';
            if($send_email){
                if (is_wp_error($user_id)) {
                    WriteLog('Error while creating new user: '.$user_id->get_error_message());
                }
            }

            if(!wp_mail( $email, 'Vellkommen!', 'Din passord er: ' . $password )){
                $this->_errorMessage = 'Customer sendt to webshop but could not send the message to the customer';
                return false;
            }
            $user = get_user_by( 'email', $email );
        }else{
            $user_id =  $user->ID;
        }

        if(strlen($data->welcomeMessage)>0){
            if(!wp_mail( $email, 'Melding fra '.get_bloginfo('name'), $data->welcomeMessage )){
                $this->_errorMessage = 'Customer sendt to webshop but could not send the message to the customer';
                return false;
            }
        }

        update_user_meta( $user_id, "first_name", $firstName );
        update_user_meta( $user_id, "last_name", $lastName );
        update_user_meta( $user_id, "billing_first_name", $firstName );
        update_user_meta( $user_id, "billing_last_name", $lastName );
        update_user_meta( $user_id, "billing_address_1", $data->address1 );
        update_user_meta( $user_id, "billing_address_2", $data->address2 );
        update_user_meta( $user_id, "billing_city", $data->postCity);
        update_user_meta( $user_id, "billing_postcode", $data->postNo );
        update_user_meta( $user_id, "billing_email", $email);
        update_user_meta( $user_id, "billing_phone", $data->phoneNo );
        update_user_meta( $user_id, "billing_orgno", $data->orgNo );

        if($role && property_exists($data,'customerGroup')){
            global $current_PCK;
            UpdateCustomerGroupsTable($data->customerGroup->customerGroupId,$role->name,$current_PCK);
        }

        if(property_exists($data,'listDiscounts')){
            foreach($data->listDiscounts as $discount){
                if(is_object($discount) && property_exists($discount,'discount'))add_user_meta( $user_id, "pck_rabatt", $discount->discount );
            }
        }

        if($user){
            $user->set_first_name($firstName);
            $user->set_last_name($lastName);
            $user->set_billing_first_name($firstName);
            $user->set_billing_last_name($lastName);
            $user->set_billing_address_1($data->address1);
            $user->set_billing_address_2($data->address2);
            $user->set_billing_city($data->postCity);
            $user->set_billing_postcode($data->postNo);
            $user->set_billing_email($email);
            $user->set_billing_phone($data->phoneNo);
            if($role){
                $user->add_role($role->name);
            }
            $user->save();

        }

        return $user_id;

    }

    public function getCreditApplicants($data){
        if ($this->login($data)){
            $customers_a = getAllPCKKreditCustomers();
            $customers = array();
            foreach($customers_a as $customer){
                if($customer->creditApproved)continue;
                $customer_to_send = new customer();
                $orgno = ($customer->orgNo)?$customer->orgNo:'rand_org_no_'.mt_rand(100000000000000, 99999999999999999);

                $customer_to_send->contactId = $customer->contactId;
                //$customer_to_send->pckCustomerId = $customer->pckCustomerId;
                $customer_to_send->email = $customer->email;
                $customer_to_send->fax = $customer->fax;
                $customer_to_send->invoiceAddress = $customer->invoiceAddress;
                $customer_to_send->invoiceAddress2 = $customer->invoiceAddress2;
                $customer_to_send->invoiceOnEmail = $customer->invoiceOnEmail;
                $customer_to_send->invoicePostCity = $customer->invoicePostCity;
                $customer_to_send->invoicePostNo = $customer->invoicePostNo ;
                $customer_to_send->name = $customer->name;
                $customer_to_send->orgNo = $orgno;
                $customer_to_send->phone1 = $customer->phone1;
                $customers[] = $customer_to_send;
            }
            $response = array(
                'insertUpdate' =>$this->_getStatus(false,$this->_errorMessage),
                'listCustomers' => $customers);
            return $this->_getReturn($response);

        }else {
            $response = array(
                'insertUpdate' =>$this->_getStatus(true,$this->_errorMessage),
                'listCustomers' => array());
            return $this->_getReturn($response);

        }


    }
    private function updateThePrice($product,$discount,$date_to){
        $discount_precntage = ($discount>0)?$discount/100:0;
        $org_price = $product->get_regular_price();
        $price = ($discount_precntage>0)?$org_price-($org_price*$discount_precntage):$org_price;
        if($price==$org_price)$date_to='';
        $product->set_date_on_sale_from('');
        $product->set_date_on_sale_to($date_to);
        $product->set_sale_price($price);
        $product->set_price($price);
        $product->save();
    }
    public function sendDiscount($data){
        $discountClass = $data->discount;
        $discount = $discountClass->discount;

        $_error = false;
        $date_to   = isset( $discountClass->validUntil ) ? $discountClass->validUntil : '';
        if(strtotime( 'NOW', current_time( 'timestamp' ) ) > strtotime( $date_to ))$date_to='';

        if ($this->login($data)) {
            $deleteDiscount = $discountClass->deleteDiscount;
            if($deleteDiscount == 1){
                $discountObj = GetDiscountByDiscountId($discountClass->discountId);
                $grb_id = $discountObj->customerGroupId;
                if($grb_id>0){
                    $role_name = getRoleNameByCustomerGroupId($grb_id);
                    $role = get_role($role_name);
                    if($role->has_cap('pck_group_rabatt'))$role->remove_cap('pck_group_rabatt');
                    if($role->has_cap('pck_group_rabatt_valid_until'))$role->remove_cap('pck_group_rabatt_valid_until');
                    flush_rewrite_rules();
                }

                $cstmr_id = $discountObj->customerId;
                if($cstmr_id>0){
                    $userId = getUserIdByPCKCustomerID($cstmr_id);
                    if($userId>0){
                        delete_user_meta($userId,"pck_rabatt");
                        delete_user_meta($userId,"pck_rabatt_valid_until");
                    }
                }
                DeleteDiscountFromDiscountsTable($discountObj->discountId);
            }else{
                if($discountClass->customerGroupId){
                    $role_name = getRoleNameByCustomerGroupId($discountClass->customerGroupId);
                    $role = get_role($role_name);
                    if($discount>0){
                        $role->add_cap('pck_group_rabatt',$discount);
                        $role->add_cap('pck_group_rabatt_valid_until',$date_to);
                    }else{
                        if($role->has_cap('pck_group_rabatt'))$role->remove_cap('pck_group_rabatt');
                        if($role->has_cap('pck_group_rabatt_valid_until'))$role->remove_cap('pck_group_rabatt_valid_until');
                    }
                    flush_rewrite_rules();
                }

                if($discountClass->customerId>0){
                    $userId = getUserIdByPCKCustomerID($discountClass->customerId);
                    if($discount>0){
                        update_user_meta( $userId, "pck_rabatt", $discount );
                        update_user_meta( $userId, "pck_rabatt_valid_until", $date_to );
                    }else{
                        delete_user_meta($userId,"pck_rabatt");
                        delete_user_meta($userId,"pck_rabatt_valid_until");
                    }

                }

                UpdateDiscountTable($discountClass);
            }

        }else {
            $_error = true;
        }

        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function sendCustomerInfo($data){
        $_error = false;
        if ($this->login($data)){
            $customerInfo = $data->customerInfo;

            $cutomer_id = $this->createCustomer($customerInfo);
            if (is_wp_error($cutomer_id)) {
                return $this->_getReturn($this->_getStatus(true,$cutomer_id->get_error_message()));
            }
            if($cutomer_id>0){
                UpdatePCKKreditCustomersFromPCK($customerInfo,$cutomer_id);
            }else{
                $_error = true;
            }
        }else {
            $_error = true;
        }


        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage,$cutomer_id));
    }

    public function getAllPaymentTypes($credentials){
        $_error = false;
        if($this->login($credentials)) {
            global $woocommerce;
            $payments = $woocommerce->payment_gateways->get_available_payment_gateways();
            $paymentToSend = array();
            foreach ($payments as $payment) {
                $paymentToSend[] = array('name'=>$payment->get_title(),'paymentId'=>$payment->get_id());
            }

            $response = array(

                'insertUpdate' =>$this->_getStatus($_error,$this->_errorMessage),
                'payments' => $paymentToSend);

            return $this->_getReturn($response);
        }else{
            $_error = true;
        }

        return $this->_getReturn($this->_getStatus($_error,$this->_errorMessage));
    }

    public function getStatus($data){
        if ($this->login($data)) {
            $customers_a = getAllPCKKreditCustomers();

            $response = array(
                'creditApplicants' => count($customers_a),
                'message' => 'HellowMessage',
                'onlineCustomers' => 2,
                'operationResult' => 3,
                'orders' => 4);

            return $this->_getReturn($response);
        }
        return $this->_getReturn($this->_getStatus(true,$this->_errorMessage));
    }

    public function getOrderInfoURL($data){
        if ($this->login($data)) {
            $order_url = get_admin_url()."post.php?post=".$data->orderid."&action=edit";
            return $this->_getReturn($order_url);
        }
        return $this->_getReturn($this->_getStatus(true,$this->_errorMessage));
    }
    public function getReceiptURL($data){
        if ($this->login($data)) {
            $order_url = get_admin_url()."post.php?post=".$data->orderid."&action=edit";
            return $this->_getReturn($order_url);
        }
        return $this->_getReturn($this->_getStatus(true,$this->_errorMessage));
    }

    public function getArticleURL($data){
        if ($this->login($data)) {
            $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');
            if($creation_type == 'samling_samme_vare_nr'){
                $product_id =  $this->getSizeColorIdFromArticleId($data->pckid);
            }else{
                $product_id = $this->testExistingOfProduct($data->pckid);
            }


            $product_url = get_admin_url()."post.php?post=".$product_id."&action=edit";
            return $this->_getReturn($product_url);
        }
        return $this->_getReturn($this->_getStatus(true,$this->_errorMessage));
    }


    protected function _getConfig(){
        return true;
    }

    public function handlePhpError($errorCode, $errorMessage, $errorFile){
        if (in_array($errorCode, array(E_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR))) {
            $this->_fault('internal');
        }
        return true;
    }

    protected function _fault($faultName, $resourceName=null, $customMessage=null){
        $faults = $this->_getConfig()->getFaults($resourceName);
        if (!isset($faults[$faultName]) && !is_null($resourceName)) {
            $this->_fault($faultName);
            return;
        } elseif (!isset($faults[$faultName])) {
            $this->_fault('unknown');
            return;
        }
        $this->fault(
            $faults[$faultName]['code'],
            (is_null($customMessage) ? $faults[$faultName]['message'] : $customMessage)
        );
    }

    public function fault($code, $message){
        if ($this->_extensionLoaded()) {
            throw new SoapFault($code, $message);
        } else {
            die('<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
				<SOAP-ENV:Body>
				<SOAP-ENV:Fault>
				<faultcode>' . $code . '</faultcode>
				<faultstring>' . $message . '</faultstring>
				</SOAP-ENV:Fault>
				</SOAP-ENV:Body>
				</SOAP-ENV:Envelope>');
        }

    }

    protected function _extensionLoaded(){
        return class_exists('SoapServer', false);
    }

    public function _getReturn($data=array()) {

        $_trace = debug_backtrace();
        $_callStr = '';
        if (count($_trace) > 1) {
            $_caller=$_trace[1];
            if (isset($_caller['class'])) {
                $_callStr .= $_caller['class'].'::';
            }
            $_callStr .= $_caller['function'];
        }

        WriteLog($_callStr."\n".print_r($data,true));

        return array('return' => $data);
    }

    public function _getStatus($error=false,$message='',$customer_id='-1') {
        $status = array('deltaId' => $customer_id);

        if (!empty($message)) {
            $_trace=debug_backtrace();
            $_callStr = '';

            $_caller=$_trace[1];
            if (isset($_caller['class'])) {
                $_callStr .= $_caller['class'].'::';
            }
            $_callStr .= $_caller['function'];
           //WriteLog('['.$_callStr.'] '.$message);
        }

        if ($error) {
            $status['errorHelpLink'] = '';
            $status['errorMessage'] = $message;
            $status['humanErrorMessage'] = $message;
            $status['operationResult'] = 1;
        }else {
            $status['operationResult'] = 0;
        }

        return $status;
    }

    public function insertUpdateResponse($data){
        //WriteLog(__FUNCTION__.' '.print_r($data,true));
    }

    protected function formatDecimal($number){
        return round($number,4);
    }


}

$services = new M360PCKasseServices();

ini_set("soap.wsdl_cache_enabled", "0");


// setup web service server, register web service methods then handle current request
$Server = new SoapServer(dirname(__FILE__) . "/wsdl.xml",
    array("cache_wsdl" => WSDL_CACHE_NONE));
$Server->setClass("M360PCKasseServices");

$Server->handle();
