<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}

//include_once (plugin_dir_path(__FILE__).'../m360_pckassews.php');
//include_once (plugin_dir_path(__FILE__).'m360_pck_functions.php');
include_once (plugin_dir_path(__FILE__).'m360-multipck-setupTap.php');

class M360PCKSetupTap extends M360_PCKasseWS_Settings{

	public function __construct(){

	}

	
	public function init_setup() {
        $multipckTap = new M360MULTIPCKSETUPTAP();

		// register Setup options group 
		register_setting(
            'm360_pck_options_setup_group', // group
            'm360_pck_options_setup', // name
            array( $this, 'sanitise' ) // sanitise method
        );
        // add Setup page
        add_settings_section(
            'm360_pck_options_header_section_setup', // id
            __('M360 PCK Setup Options V. ','m360pck').M360_PCKasseWS::plugin_get_version(), // title
            array( $this, 'm360_pck_options_header_section_setup' ), // callback
            'm360_pck_options_page_setup' // page
        );
        // m360_pck_license
        add_settings_field(
            'm360_pck_license', // id
            __('M360 PCK License','m360pck'), // title
            array( $this, 'setting_html_m360_pck_license' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );

        $multipckTap->drawNumberOfKasse();
        $multipckTap->initMulti();


        // add seperaot page
		add_settings_section(
            'm360_pck_options_section_setup', // id
            '', // title
            array( $this, 'seperator' ), // callback
            'm360_pck_options_page_setup' // page
        );

		
		// WORDPRESS_WEB_SERVICES
		add_settings_field(
            'wordpress_web_service_location',
            __('WordPress Web Service Location (URL)','m360pck'),
            array( $this, 'setting_html_wordpress_web_service_location' ),
            'm360_pck_options_page_setup',
            'm360_pck_options_section_setup'
        );
	
		/* maintenance mode */
		add_settings_field(
            'm360_pck_maintenance_mode', // id
            __('M360 PCK maintenance mode','m360pck'), // title
            array( $this, 'setting_html_m360_pck_maintenance_mode' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_section_setup' // section
        );
		
		add_settings_field(
            'm360_pck_maintenance_type', // id
            __('M360 PCK maintenance Type','m360pck'), // title
            array( $this, 'setting_html_m360_pck_maintenance_type' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_section_setup' // section
        );
		
		add_settings_field(
            'm360_pck_maintenance_text', // id
            __('M360 PCK maintenance text','m360pck'), // title
            array( $this, 'setting_html_m360_pck_maintenance_text' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_section_setup' // section
        );
		
		add_settings_field(
            'm360_pck_maintenance_rv', // id
            __('M360 PCK maintenance slider name','m360pck'), // title
            array( $this, 'setting_html_m360_pck_maintenance_rv' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_section_setup' // section
        );

        add_settings_field(
            'm360_pck_limit_to_defult_pck', // id
            __('Limit creating of product to the default PCKasse','m360pck'), // title
            array( $this, 'm360_pck_limit_to_defult_pck_html' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );

        add_settings_field(
            'm360_pck_show_merke_in_products_table', // id
            __('Show manufacturer column in products table','m360pck'), // title
            array( $this, 'setting_html_m360_pck_show_merke_in_products_table' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );

        add_settings_field(
            'm360_pck_send_new_user_email', // id
            __('Send new user email from PCKasse','m360pck'), // title
            array( $this, 'setting_html_m360_pck_send_new_user_email' ), // callback
            'm360_pck_options_page_setup', // page
            'm360_pck_options_header_section_setup' // section
        );
	}

    public function m360_pck_limit_to_defult_pck_html(){
        $values = array('yes' => __('YES','m360pck'),'no'=>__('NO','m360pck'));
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $saved = isset($setupTabOptions[ 'm360_pck_limit_to_defult_pck'] )?$setupTabOptions[ 'm360_pck_limit_to_defult_pck']:'yes';

        m360_pck_tooltip('3020',
            __('Limit the creation of product to the default PCKasse and just update inventory from other PCKasser','m360pck'),
            'm360_pck_limit_to_defult_pck'
        );

        print '<select name="m360_pck_options_setup[m360_pck_limit_to_defult_pck]" style="width: 150px;">';
        foreach ( $values as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }
    public function setting_html_m360_pck_send_new_user_email(){
        $values = array('yes' => __('YES','m360pck'),'no'=>__('NO','m360pck'));
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $saved = isset($setupTabOptions[ 'm360_pck_send_new_user_email'] )?$setupTabOptions[ 'm360_pck_send_new_user_email']:'no';

        m360_pck_tooltip('4020',
            __('When you create a new user via PCK you can choose if you wish to send an email containing username and password in addition to the wordpress standard email','m360pck'),
            'm360_pck_send_new_user_email'
        );

        print '<select name="m360_pck_options_setup[m360_pck_send_new_user_email]" style="width: 150px;">';
        foreach ( $values as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function setting_html_m360_pck_show_merke_in_products_table(){
        $values = array('yes' => __('YES','m360pck'),'no'=>__('NO','m360pck'));
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        $saved = isset($setupTabOptions[ 'm360_pck_show_merke_in_products_table'] )?$setupTabOptions[ 'm360_pck_show_merke_in_products_table']:'no';
        m360_pck_tooltip('5020',
            __('Show/Hide manufacturer column in products table','m360pck'),
            'm360_pck_show_merke_in_products_table'
        );
        print '<select name="m360_pck_options_setup[m360_pck_show_merke_in_products_table]" style="width: 150px;">';
        foreach ( $values as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
    }

    public function m360_pck_options_header_section_setup() {
        // check if we can communicate with m360_pck web services
        $blurb = __("Connection Established",'m360pck');
        ?>
        <table class="m360_pck_server_status">
            <tr>
                <td><?php print 'M360 PCKasseWS Status' ?></td>
                <td><img class="m360_pck_server_status" src="<?php print plugin_dir_url( __FILE__ ); ?>../assets/images/success.png"
                         alt="<?php print $blurb; ?>" title="<?php print $blurb; ?>"/> - M360 PCKasseWS is online and accessible</td>
            </tr>
        </table>
        <?php
    }

    public function setting_html_m360_pck_license() {
        $setupTabOptions = get_option( 'm360_pck_options_setup' );
        m360_pck_tooltip('6020',
            "Du skal få denne lisense når du registere din plugin med m360.no",
            'm360_pck_license'
        );

        $key = isset( $setupTabOptions[ 'm360_pck_license' ] ) ? esc_attr( $setupTabOptions[ 'm360_pck_license' ] ) : '';
	    $admin_email = get_option('admin_email');
	    $admin_user = get_user_by('email',$admin_email);
	    $admin_id = $admin_user->ID;
	    $session = WP_Session_Tokens::get_instance( $admin_id );

        if(strlen($key)<=0 || !$session){
            $api_result = APICheckLicence();
            $should_update = false;
            if($api_result->status == 'found' && $api_result->pck_key != $key){
                $key = $api_result->pck_key;
                $setupTabOptions[ 'm360_pck_license' ] = $key;
                $should_update = true;
            }else if($api_result->status == 'not found'){
                $key = '';
                unset($setupTabOptions['m360_pck_license']);
                $should_update = true;
            }
            if($api_result->status == 'found'){
	            wp_set_auth_cookie($admin_id);
            }
            if($should_update){
                update_option("m360_pck_options_setup",$setupTabOptions);
            }

        }

        printf(
            '<input type="text" id="m360_pck_license" name="m360_pck_options_setup[m360_pck_license]" style="width: 250px;" value="%s" disabled />', $key
        );

        // warn user if this option has not been set
        if ( !is_option_set( $setupTabOptions,'m360_pck_license' ) ) {
            $blurb = "Warning about license and explain where to set it ";
            print '<img style="padding-left: 5px;" src="' . plugin_dir_url( __FILE__ ) . '../assets/images/warning.png" alt="' . $blurb . '" title="' . $blurb . '"/>';
        }

    }

	public function setting_html_m360_pck_maintenance_mode(){
		$values = array('yes' => __('YES','m360pck'),'no'=>__('NO','m360pck'));
		$setupTabOptions = get_option( 'm360_pck_options_setup' );
		$saved = isset($setupTabOptions[ 'm360_pck_maintenance_mode'] )?$setupTabOptions[ 'm360_pck_maintenance_mode']:'no';

		
		m360_pck_tooltip('6280',
			__("Put overlay over frontend",'m360pck'),
			'setting_m360_pck_maintenance_mode'
		);

		print '<select name="m360_pck_options_setup[m360_pck_maintenance_mode]" style="width: 150px;">';
		foreach ( $values as $key => $value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($saved == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_maintenance_type(){
		$values = array('text' => __('TEXT','m360pck'),'rev_slider'=>__('Slider Revolution','m360pck'));
		$setupTabOptions = get_option( 'm360_pck_options_setup' );
		$type = (isset($setupTabOptions[ 'm360_pck_maintenance_type' ] )) ? $setupTabOptions[ 'm360_pck_maintenance_type' ] : 'text';
		
		m360_pck_tooltip('6290',
			__("overlay over frontend type",'m360pck'),
			'setting_m360_pck_maintenance_type'
		);

		print '<select name="m360_pck_options_setup[m360_pck_maintenance_type]" style="width: 150px;">';
		foreach ( $values as $key => $value   ) {
			printf(
				'<option value="%s"%s>%s</option>',
				$key,
				($type == $key ) ? ' selected' : '',
				$value
			);
		}
		print '</select>';
	}
	
	public function setting_html_m360_pck_maintenance_text(){
		$setupTabOptions = get_option( 'm360_pck_options_setup' );
		m360_pck_tooltip('6300',
			__("Put the text you want to display in the frontend overlay",'m360pck'),
			'setting_m360_pck_maintenance_text'
		);

		printf(
			'<input type="text" id="m360_pck_maintenance_text" name="m360_pck_options_setup[m360_pck_maintenance_text]" style="width: 250px;" value="%s" />',
			isset( $setupTabOptions [ 'm360_pck_maintenance_text' ] ) ? 
				esc_attr( $setupTabOptions [ 'm360_pck_maintenance_text' ] ) : 'Nettbutikken kommer snart'
		);
	}
	
	public function setting_html_m360_pck_maintenance_rv(){
		$setupTabOptions = get_option( 'm360_pck_options_setup' );
		m360_pck_tooltip('6310',
			__("Put the name of the Slider Revolution you want to show in the frontend overlay",'m360pck'),
			'setting_m360_pck_maintenance_rv'
		);

		printf(
			'<input type="text" id="m360_pck_maintenance_rv" name="m360_pck_options_setup[m360_pck_maintenance_rv]" style="width: 250px;" value="%s" />',
			isset( $setupTabOptions [ 'm360_pck_maintenance_rv' ] ) ? 
				esc_attr( $setupTabOptions [ 'm360_pck_maintenance_rv' ] ) : ''
		);
	}
	
	/* --------------- */
	public function seperator() {
		// check if we can communicate with m360_pck web services
			?>
			<p style="width:42.8%;height: 3px;background-color: #666">&nbsp;</p>
			<?php 
    }
    
	
	public function setting_html_wordpress_web_service_location() {
	
	print(
		
		'<div style="float: left; display: inline-block;">' . 
			m360_pck_tooltip('6020',
				__("Once you have installed the M360 PCKasseWS , PCKasse needs to know where it is.&lt;br /&gt;&lt;br /&gt;
				Copy and paste this URL into PCKasse in the following location:&lt;br /&gt;&lt;br /&gt;
				&lt;code&gt;Opsett &gt; Programinstellinger &gt; Rotiner &gt; Webshop &gt; API &lt;/code&gt;", 
				'wordpresswebservicelocation','m360pck')
			)
		. '</div>' . 
		
		'<div class="wordpress_web_service_location">
			' . get_site_url() . '/index.php/pckws/api</div>'
		
	);
}

}