<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once ('order_action_page.php');
$order_action_page = new M360ORDERPCKActionPage();

class M360PCKExtraOrderTap{
	function __construct() {
		add_action( 'wp_ajax_edit_product_id', array( $this, 'ajax_edit_product_id' ) );
	}

	public function drawThePage() {

		$this->settings_html_m360_pck_order();
		$this->order_html();
		/*
		$this->render_fix_order_elements();

		if(isset($_POST['fix_order_items']) && isset($_POST['fix_items_order_nr'])){
			$order_nr = trim($_POST['fix_items_order_nr']);
			if(strlen($order_nr)>0){
				$this->draw_order_items_form($order_nr);
			}
		}
        */
	}
	
	public function settings_html_m360_pck_order() {
			?>
			<div class="wrap">
            	<h1>If you have a problem with sending an order to PCK,  you can use this tool to try to fix it </h1>
            </div>
			<?php
    }
	
	
	public function order_html() {
		if(count($_POST)){
			global $order_action_page;
			if(isset($_POST['find']) && isset($_POST['order_number'])){
				$order_action_page->findTheOrder($_POST['order_number']);
			}
			if(isset($_POST['update'])){
				$order_action_page->updateTheOrder();
			}
			/*
			if(isset($_POST['create_new'])){
				$order_action_page->InsertNewOrderIntoOrderStatusTable();
			}
			*/
			
		}
		?>
        <style>
		.submit{
			display: inline-block;
			margin-left:2px;
		}
		.me60_nopadding{
			padding: 0 !important;
		}
		.m360_buttonRed{
			background: #BA0000 !important;
			border-color: #AA0000 #990000 #990000 !important;
			-webkit-box-shadow: 0 1px 0 #990000 !important;
			box-shadow: 0 1px 0 #990000 !important;
			color: #fff !important;
			text-decoration: none;
		}
		.m360_fit{
			width:1%;
			white-space:nowrap;
		}
		</style>
        <?php
			$order_number = '';
			
			if(isset($_POST['order_number'])){
				$order_number = $_POST['order_number'];
			}
			
		?>
        <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
        	<table class="form-table">
            	<tbody>
                    <tr style="border-bottom:1px solid #000;">
                    	<td class="me60_nopadding m360_fit">
                        	Find the order by order number:
                        </td>
                        <td class="me60_nopadding m360_fit">
                        	<span style="padding-left:20px;"><input type="text" name="order_number" value="<?php echo $order_number; ?>"></span>
						</td>
                        <td class="me60_nopadding">
                        	<?php submit_button('Search', 'primary', 'find', true, array( 'id' => 'find' )); ?>
                       </td>
					</tr>
	            </tbody>
            </table>
        </form>
        
        <?php
    }
	
    /*
    private function render_fix_order_elements(){
	    ?>
        <h3>Fiks order elementer</h3>
        <div class="container-fluid">
            <form class="form-inline" method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
                <label class="form-group mb-2" style="margin-right: 20px;">Order nr:</label>
                <input type="text" class="form-control" style="margin-right: 20px;" id="order_nr" name="fix_items_order_nr" placeholder="Order nr">
                <button type="submit" class="btn btn-primary mb-2" name="fix_order_items">Søk</button>
            </form>
        </div>

        <?php
    }

    private function draw_order_items_form($order_nr){
	    $order = wc_get_order($order_nr);
	    $items = $order->get_items();
	    ?>
        <h3>Order nr: <?php echo $order_nr; ?></h3>
        <div class="container-fluid">
            <table class="table" id="pck_order_to_edit" data-order_id="<?php echo $order_nr; ?>">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Produktnavn</th>
                    <th scope="col">Produkt id</th>
                    <th scope="col">Variasjon id</th>
                    <th scope="col">Antall</th>
                    <th scope="col">Pris</th>
                    <th scope="col">Handling</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item):?>
                    <?php $product_name = $item->get_name();?>
	                <?php $product_id   = $item->get_product_id();?>
	                <?php $variation_id = $item->get_variation_id();?>
	                <?php $quantity     = $item->get_quantity();?>
	                <?php $subtotal     = $item->get_subtotal();?>

                    <tr>
                        <td><?php echo $product_name;?></td>
                        <td><?php echo $product_id;?></td>
                        <td><?php echo $variation_id;?></td>
                        <td><?php echo $quantity;?></td>
                        <td><?php echo $subtotal;?></td>
                        <td><a class="btn pck_edit_item_action" href="#" data-item_id="<?php echo $item->get_id(); ?>"><i class="fas fa-edit"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        <?php
    }

    public function ajax_edit_product_id(){
	    WriteLog(__FUNCTION__.' post: '.print_r($_POST,true));
	    wp_send_json_success( array('message'=>'continue') );
	    wp_die();
    }
    */
}