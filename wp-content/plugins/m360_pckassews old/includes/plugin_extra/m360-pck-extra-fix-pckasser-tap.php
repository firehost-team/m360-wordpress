<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360PCKExtraFixPckTap{

    public function __construct(){

    }


    public function drawThePage() {
        $this->removeAll_PckasserHtml();
    }


    public function removeAll_PckasserHtml() {
        if(count($_POST)){
            if(isset($_POST['fixpckasser'])){
                $table_name = m360_pck_get_table_name("m360_pckasse_multikasser");
                global $wpdb;
                $query = "TRUNCATE {$table_name}";
                $wpdb->query($query);

                $setupTabOptions = get_option( 'm360_pck_options_setup' );
                $counter = isset($setupTabOptions['m360_number_of_pckasser']) ? $setupTabOptions['m360_number_of_pckasser']: 1;

                for($i = 0; $i<$counter ; $i++){
                    $credintals =$setupTabOptions['pck_credintal_'.$i];
                    $location = $credintals['location'];
                    $license = $credintals['license'];
                    $username = $credintals['username'];
                    $password = $credintals['password'];
                    $is_default = $credintals['default'];

                    addKasse($i,$location,$license,$username,$password,$is_default);
                }
            }

        }
        ?>

        <form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=fixpck">
            <table class="form-table">
                <tbody>
                <tr>
                    <td>
                        <h3>remove all Pckasser from wordpress database and create a new one from pck:</h3>
                        <?php submit_button('Do it', 'primary', 'fixpckasser', true, array( 'id' => 'fixpckasser' )); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>

        <?php
    }


}