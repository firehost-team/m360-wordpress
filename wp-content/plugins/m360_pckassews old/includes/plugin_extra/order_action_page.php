<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360ORDERPCKActionPage{
	public function __construct(){

	}
	
	private function GetOrderIdByOrderNumber($order_number){
		// get WOOCOMM product id from PCK SKU
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare(
			"SELECT post_id FROM " . $wpdb->postmeta . " WHERE meta_key = '_order_number' AND meta_value = '%s' LIMIT 1", $order_number
		));
	}


	private function StatusArray($kasse_nr){
	    $location = getLocationForKasseNr($kasse_nr);
		$values = array(true => 'Ble sendt til '.$location,false => 'Ble ikke sendt til '.$location);
		return $values;
	}
	private function drawUpdateForm($did_sendt_to_pck,$order_id,$kasse_nr,$updated = false){

		$values = $this->StatusArray($kasse_nr);

		if($updated){
			echo '<p>Order <span style="font-weight:800; color:red">#: '.$order_id.'</span> status has been Changed to: <span style="font-weight:800; color:red">'.$values[$did_sendt_to_pck].'</span></p>';
		}
		?>
		<form method="post" action="<?php echo get_admin_url()?>admin.php?page=m360_extra_pck_plugin_options_page&tab=order">
			<table class="form-table">
			<tbody>
				<tr style="border-bottom:1px solid #000;">
					<td class="me60_nopadding m360_fit">
						Order <span style="font-weight:800; color:red">#:<?php echo $order_id ?></span>:
					</td>
					<td class="me60_nopadding m360_fit">
						<select name="status">
							<?php foreach ( $values as $key => $value):?>
								<?php $selected = '';
									if($did_sendt_to_pck == $key ){
										$selected = ' selected';
									}
								?>
								<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
							<?php endforeach; ?>
							</select>
                            <input type="hidden" name="order_number_to_update" value="<?php echo $order_id; ?>">
                        <input type="hidden" name="kasse_nr" value="<?php echo $kasse_nr; ?>">
                    </td>
					<td class="me60_nopadding">
						<?php submit_button('Update', 'm360_buttonRed', 'update', true, array( 'id' => 'update' )); ?>
				   </td>
				</tr>
			</tbody>
		</table>
		</form>
		<?php
	}


	
	public function findTheOrder($order_number){
		$order_id = $order_number;//$this->GetOrderIdByOrderNumber($order_number);
		if(!$order_id){
			echo '<p>Order <span style="font-weight:800; color:red">#: '.$order_number.'</span> does not exist in woocommerce orders</p>';
		}else{
		    $kasser = kasserInDB();
		    foreach ($kasser as $kasse){
			    $did_sendt_to_pck = get_post_meta($order_id,'_did_sendt_to_pck_to_kasse_nr_'.$kasse->kasse_nr,true);
			    $this->drawUpdateForm($did_sendt_to_pck,$order_id,$kasse->kasse_nr);
            }

		}
	}
	
	public function updateTheOrder(){
	    $order_id = $_POST['order_number_to_update'];
		$selected_status = $_POST['status'];
	    $kasse_nr = $_POST['kasse_nr'];

        update_post_meta($order_id,'_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,$selected_status);

		$this->drawUpdateForm($selected_status,$order_id,$kasse_nr,true);

	}
	
	
}