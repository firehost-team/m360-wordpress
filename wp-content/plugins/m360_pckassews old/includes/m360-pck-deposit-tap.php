<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
class M360DepositTap extends M360_PCKasseWS_Settings{

    public function __construct(){

    }

    public function init_tap(){
        register_setting(
            'm360_pck_options_deposits_group',
            'm360_pck_options_deposits',
            array($this, 'sanitise')
        );

        // Logging Options
        add_settings_section(
            'm360_pck_options_section_deposits',
            'M360 PCKasseWS deposits Options',
            array($this, 'settings_html_m360_pck_deposits_message'),
            'm360_pck_options_page_deposits'
        );

        add_settings_field(
            'm360_pck_deposits_active',
            __('Aktiv','m360pck'),
            array( $this, 'm360_pck_deposits_active_html' ),
            'm360_pck_options_page_deposits',
            'm360_pck_options_section_deposits'
        );

        add_settings_field(
            'm360_pck_deposits_product_sku',
            __('Depositum vare nr.','m360pck'),
            array( $this, 'm360_pck_deposits_product_sku_html' ),
            'm360_pck_options_page_deposits',
            'm360_pck_options_section_deposits'
        );
    }

    public function settings_html_m360_pck_deposits_message(){
        echo '<p>'.__('Bruk denne valg hvis selge du noe varer som krever betaling av depositom fra deres kunde, set aktivering til ja , og deretter skriver du inn vare nr som skal brukes som depositum vare i pck','m360pck').'</p>';
    }

    public function m360_pck_deposits_active_html(){
        $values = array('yes' => __('YES','m360pck'),'no'=>__('NO','m360pck'));
        $depositsTabOptions = get_option( 'm360_pck_options_deposits' );
        $saved = isset($depositsTabOptions[ 'm360_pck_deposits_active'] )?$depositsTabOptions[ 'm360_pck_deposits_active']:'no';
        print '<select name="m360_pck_options_deposits[m360_pck_deposits_active]" id="m360_pck_deposits_active">';
        foreach ( $values as $key => $value   ) {
            printf(
                '<option value="%s"%s>%s</option>',
                $key,
                ($saved == $key ) ? ' selected' : '',
                $value
            );
        }
        print '</select>';
        ?>
        <script>
            jQuery(document).ready(function() {
                <?php if($saved == 'no'):?>
                    jQuery('#m360_pck_deposits_product_sku').closest('tr').hide();
                <?php endif;?>
                jQuery('#m360_pck_deposits_active').change(function() {
                    var selected = jQuery('#m360_pck_deposits_active :selected').text();
                    if(selected == '<?php echo __('YES','m360pck');?>'){
                        //alert( "Handler for .change() called." + selected);
                        jQuery('#m360_pck_deposits_product_sku').closest('tr').show();
                    }else{
                        jQuery('#m360_pck_deposits_product_sku').closest('tr').hide();
                    }
                });
            });
        </script>
        <?php
    }

    public function m360_pck_deposits_product_sku_html(){
        $depositsTabOptions = get_option( 'm360_pck_options_deposits' );
        printf(
            '<input type="text" id="m360_pck_deposits_product_sku" name="m360_pck_options_deposits[m360_pck_deposits_product_sku]" value="%s" />',
            isset( $depositsTabOptions [ 'm360_pck_deposits_product_sku' ] ) ?
                esc_attr( $depositsTabOptions [ 'm360_pck_deposits_product_sku' ] ) : ''
        );
    }
}