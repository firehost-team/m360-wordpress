<?php
if( !defined( 'ABSPATH' ) ) {
    exit;
}
include_once(dirname(__FILE__) . '/m360_pck_functions.php'); 

class M360PCKSender {
	public function __construct(){
		
	}


	private static function getPCKArticleIDFromSKU($articleId,$articleNo=NULL,$nettbutikk=0){
        global $wpdb;
        if($nettbutikk>0)switch_to_blog( $nettbutikk );

        $kasse_nr = retriveKasseNr();
        $PCK_articleID = get_post_meta( $articleId, 'm360_pck_article_id_'.$kasse_nr,true );

        if(is_numeric($PCK_articleID)){
            if($nettbutikk>0)switch_to_blog( $nettbutikk );
            return $PCK_articleID;

        }
        /* ekstra 6.42 */
        $PCK_articleID = get_post_meta( $articleId, 'm360_pck_article_id',true );

        if(is_numeric($PCK_articleID)){
            if($nettbutikk>0)switch_to_blog( $nettbutikk );
            return $PCK_articleID;
        }


        $table_name = m360_pck_get_table_name("postmeta");
        $qur = "SELECT meta_value FROM " . $table_name . " WHERE meta_key = 'm360_pck_article_id_{$kasse_nr}' AND post_id = $articleId LIMIT 1";
        $ProductId = $wpdb->get_var($qur);
        $isExist = false;

        if(is_numeric($ProductId)){$isExist = true;}

        if(!$isExist && $articleNo){
            $qur2 = "SELECT post_id FROM " . $table_name . " WHERE meta_key = '_sku' AND meta_value = '".$articleNo."' LIMIT 1";
            $ProductId = $wpdb->get_var($qur2);
            if(is_numeric($ProductId)){
                add_post_meta($ProductId, 'm360_pck_article_id_'.$kasse_nr, $articleId, true);
            }
        }

        return $ProductId;
    }
	
	private static function getSKUFromProductID($product_id){
		global $wpdb;
		// get WOOCOMM product id from PCK SKU
		return $wpdb->get_var($wpdb->prepare(
			"SELECT meta_value FROM " . $wpdb->postmeta . " WHERE meta_key = '_sku' AND post_id = '%s' LIMIT 1", $product_id
		));
	}
	
	public function get_billing_address($order){
		$billing_address = array(
								'first_name' => $order->get_billing_first_name(),
								'last_name'  => $order->get_billing_last_name(),
								'company'    => $order->get_billing_company(),
								'address_1'  => $order->get_billing_address_1(),
								'address_2'  => $order->get_billing_address_2(),
								'city'       => $order->get_billing_city(),
								'postcode'   => $order->get_billing_postcode(),
								'country'    => $order->get_billing_country(),
								'email'      => $order->get_billing_email(),
								'phone'      => $order->get_billing_phone(),
							  );
		return $billing_address;
	}
	
	public function get_shipping_address($order){
		$shipping_address = array(
								'first_name' => $order->get_shipping_first_name(),
								'last_name'  => $order->get_shipping_last_name(),
								'company'    => $order->get_shipping_company(),
								'address_1'  => $order->get_shipping_address_1(),
								'address_2'  => $order->get_shipping_address_2(),
								'city'       => $order->get_shipping_city(),
								'postcode'   => $order->get_shipping_postcode(),
								'country'    => $order->get_shipping_country(),
							  );
		return $shipping_address;
		
	}
	
	public function getSizeColorIdFromVariationId($varID){
        global $wpdb;
        global $current_PCK;

        $kasse_nr = (is_object($current_PCK))?getRealKasseNrForLocation($current_PCK->PCKLocation):0;

        $table_name = m360_pck_get_table_name("postmeta");
        $key = '_sizeColorId_'.$kasse_nr;

        $qur = "SELECT meta_value FROM $table_name WHERE meta_key = '{$key}' AND post_id = {$varID}";
        $sizeColorId = $wpdb->get_var($qur);

        if(!$sizeColorId){
            $qur_2 = "SELECT meta_value FROM $table_name WHERE meta_key = '_sizeColorId' AND post_id = {$varID}";
            $sizeColorId = $wpdb->get_var($qur_2);
        }

        return (integer)$sizeColorId;
	}
	
	public function tmpC(){
		//global $woocommerce;
		//global $wpdb;
		$order = wc_get_order(3192);
		$OrderDetails = $order->get_items();
		
		foreach ($OrderDetails as $key => $Product) {
			$customFieldPrice = 0;
			if(!empty($Product['wccf'])){
				$fields = unserialize($Product['wccf']);
				$customFieldMessage = '';
				foreach ($fields as $field){
					$customFieldPrice += $field['pricing']['value'];
				}
			}
			$price = (($Product['line_subtotal']+$Product['line_subtotal_tax'])/ ($Product['qty'] == 0 ? 1 : $Product['qty']))-$customFieldPrice;
			//WriteLog('price: '.$price);
		}
	}



    private function checkOrderStatusTable(){
        global $wpdb;
        $table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            CreateOrderStatusTables();
        }
        addnettbutikkIdKolToOrderStatusIfNotExists($table_name);

    }

    private function getCreditOrderStatusFromOptions(){
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $saved = isset($ordersTabOptions[ 'm360_pck_credit_order_status'] )?$ordersTabOptions[ 'm360_pck_credit_order_status']:'completed';
        return $saved;
    }

    private function order_item_location($item){
	    $location = '';
	    $product_id = $item->get_product_id();
		$variation_id = $item->get_variation_id();
	    $kasserInDB = kasserInDB();

	    $locations = array();
	    foreach($kasserInDB as $kasse){
		    if($variation_id > 0){
			    $stock = get_post_meta($variation_id,'m360_stock_for_pck_'.$kasse->kasse_nr,true);
			    $locations[$stock] = get_post_meta($variation_id,'m360_location_for_pck_'.$kasse->kasse_nr,true);
		    }else{
			    $stock = get_post_meta($product_id,'m360_stock_for_pck_'.$kasse->kasse_nr,true);
			    $locations[$stock] = get_post_meta($product_id,'m360_location_for_pck_'.$kasse->kasse_nr,true);
		    }
	    }

	    if(count($locations)>0){
		    ksort($locations);

		    $location .= 'Kan hentes fra ';
		    foreach ($locations as $stock =>$loc){
			    $location .= '('.$stock.' inn på '.$loc.') ,';
		    }
		    $location = rtrim($location,' ,');
	    }
		return $location;
    }
    public function getOrderFromWC($update_order_status=true){
        //add_total_submitted_col_to_orderstatus_table();
	    $kasse_nr = retriveKasseNr();
	    $current_kasse_location = getLocationForKasseNr($kasse_nr);

        $this->checkOrderStatusTable();
        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $sending_type = isset($ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ] )?$ordersTabOptions[ 'deliveredItemsAndCapturedPaymentInfo' ]:'normal';
        $creation_type = getProductOption('m360_pck_how_varianter_created','farge_storrelse_i_btuk');
        $order_info_settings = 'no';

        if(isset($ordersTabOptions[ 'm360_pck_item_info_in_order_info'] )){
            $order_info_settings = $ordersTabOptions[ 'm360_pck_item_info_in_order_info'];
        }

        $ordersArray = array();
        $totalAmount = 0.0;
        $totalQty = 0;

        $ordersTabOptions = get_option( 'm360_pck_options_orders' );
        $allowd_status = $ordersTabOptions['m360_pck_order_status_ids'];
        $multiStorePlugin = n_is_plugin_active('woocommerce-multistore/woocommerce-multistore.php')?true:false;

        //$kasse_nettbutikk_id = get_current_blog_id();
        //WriteLog('$kasse_nettbutikk_id: '.$kasse_nettbutikk_id);

        $args = array(
            'post_type'     => 'shop_order',
            'post_status'   => $allowd_status,
            'posts_per_page'      => -1,
            'meta_query' => array(
	            'relation' => 'OR',
	            array(
		            'key'     => '_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,
		            'compare' => 'NOT EXISTS',
		            'value' => ''
	            ),
	            array(
		            'key'     => '_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,
		            'value'   => 0
	            ),
            ),
        );

        //$orders = wc_get_orders( $args );
	    $posts = get_posts($args);

        foreach($posts as $post){
        	$order = wc_get_order($post->ID);
            $order_id = $order->get_id();
            if($sending_type == 'use_warehouses' && $order->get_status() != 'completed'){
                WriteLog('coninue becuase it use_warehouses');
                continue;
            }

            $OrderDetails = $order->get_items();
            $shipping_cost = $order->calculate_shipping()+$order->get_shipping_tax();
            $billing_address = $this->get_billing_address($order);
            $shipping_address = $this->get_shipping_address($order);


            if (!$shipping_address || empty($shipping_address)) {
                $shipping_address = $billing_address;
            }
            $order_to_pck = array();
            $orderLines = array();

            $extraCost = 0;
            $extraCoastDesc = '';
            $ekstra_product_info = '';

            foreach ($OrderDetails as $key => $Product) {
                $created_location = get_post_meta($Product['product_id'],'m360_location_for_pck_'.$kasse_nr,true);
                if($created_location && $created_location != $current_kasse_location)continue;

                $innskudd = $Product->get_meta('_innskudd_for_varenr');

                $totalQty += $Product['qty'];
                $customFieldMessage = '';
                $customFieldPrice = 0;
                if(array_key_exists('wccf',$Product)){
                    $fields = unserialize($Product['wccf']);
                    foreach ($fields as $field){
                        $extraCoastDesc .= $field['label'].' ';
                        $customFieldMessage .= ' '.$field['label'].': '.$field['value'].' ';
                        $customFieldPrice += $field['pricing']['value'];
                    }
                }
                $sizeColorId = wc_get_order_item_meta($key,'_sizeColorId_'.$kasse_nr);
                if(!$sizeColorId || $sizeColorId<=0)
                    $sizeColorId = $this->getSizeColorIdFromVariationId($Product['variation_id']);


                $percentage = ( ( $order->get_line_subtotal( $Product,true,false ) - $order->get_line_total( $Product,true,false ) ) / $order->get_line_subtotal( $Product,true,false ) ) * 100 ;
                $totalAmount += $order->get_line_total( $Product,true,false );
                $ordet_subtotal = $order->get_line_subtotal( $Product,true,false )/$Product['qty'];

                $wc_prod = wc_get_product($Product['product_id']);
                $article_id_to_send = wc_get_order_item_meta($key,'m360_pck_article_id_'.$kasse_nr);
                if(!$article_id_to_send || $article_id_to_send<=0) {
                    $p_id = (method_exists($wc_prod,'get_id'))?$wc_prod->get_id():$Product['product_id'];
                    $p_sku = (method_exists($wc_prod,'get_sku'))?$wc_prod->get_sku():get_post_meta( $p_id, '_sku', true);
                    $article_id_to_send = (integer)self::getPCKArticleIDFromSKU($p_id,$p_sku);
                }

                if(is_object($wc_prod) && $wc_prod->get_type() == 'yith_bundle'){
                    continue;
                }

                if($ordet_subtotal<=0 && isset($Product['_wcpdf_regular_price'])){
                    $price_data = $Product['_wcpdf_regular_price'];
                    $ordet_subtotal = $price_data['excl'];
                }
                $warehouseId = wc_get_order_item_meta($Product->get_id(),'lager');
                $warehouse_name = '';
                if($warehouseId > 0){
                    $warehouse = getFrontEndWarehousByWarehouseId($warehouseId);
                    $warehouse_name .= (strlen($warehouse->name)>0)?$Product['qty'].' varer fra '.$warehouse->name.' , ':$Product['qty'].' varer fra '.'Standard , ';
                    $ekstra_product_info .= ' || Plukklisteinfo for: '.$wc_prod->get_name().' Skal hentes fra: '.$warehouse_name.' || ';
                    $customFieldMessage .= ' Lager: '.$warehouse_name;
                }

	            $customFieldMessage .= ' '.$this->order_item_location($Product);

                if(strlen($innskudd)>0){
                    $customFieldMessage .= ' innskudd for varenr: '.$innskudd;
                }
                if ($creation_type == 'samling_samme_vare_nr')$article_id_to_send = $sizeColorId;
                $orderLines[] = array(
                    'articleId'     => $article_id_to_send,
                    'count'         => (integer)$Product['qty'],
                    'orderLineId'   => $key,
                    'price'         => $ordet_subtotal,
                    'sizeColorId'   => $sizeColorId,
                    'discount'	    => is_nan($percentage)?0:$percentage,
                    'info'		    => $customFieldMessage,
                    'warehouseId'   => $warehouseId
                    );

            }


            $m360_pck_options_orders = get_option( 'm360_pck_options_orders' );
            $paymentMethod = $order->get_payment_method_title();

            if(isset($m360_pck_options_orders[ 'm360_pck_credit_order' ] )){
                $isCredit = $m360_pck_options_orders[ 'm360_pck_credit_order' ];
                $pyment_id = get_post_meta( $order->get_id(), '_payment_method', true );
                //WriteLog('isCredit: '.$isCredit.' paymentMethod: '.$pyment_id);
                if($pyment_id == $isCredit || $order->get_payment_method() == 'wc-booking-gateway'){
                    $paymentMethod = 3;
                }
            }
            $label = esc_attr( $m360_pck_options_orders [ 'm360_pck_order_extra_coast_label' ] );

            $extraCost = $order->get_total()-($totalAmount+$shipping_cost);
            if(strlen($extraCoastDesc)<=0){
                if($extraCost>0){
                    $extraCoastDesc = (strlen($label)>0)?$label:'Gebyr';
                }
            }

            $contactAddressline1 = $billing_address['address_1'];
            $contactAddressline2 = $billing_address['address_2'];

            $billing_first_name = $billing_address['first_name'];
            $billing_last_name = $billing_address['last_name'];

            $contactName = $billing_first_name.' '.$billing_last_name;
            $contactPostCity = $billing_address['city'];
            $contactPostNo = $billing_address['postcode'];

            $deliveryAddressLine1 = (strlen($shipping_address['address_1'])>0)?$shipping_address['address_1']:$contactAddressline1;
            $deliveryAddressLine2 = (strlen($shipping_address['address_2'])>0)?$shipping_address['address_2']:$contactAddressline2;

            $shipping_first_name = (strlen($shipping_address['first_name'])>0)?$shipping_address['first_name']:$billing_first_name;
            $shipping_last_name = (strlen($shipping_address['last_name'])>0)?$shipping_address['last_name']:$billing_last_name;

            $deliveryName = $shipping_first_name.' '.$shipping_last_name;
            $deliveryPostCity = (strlen($shipping_address['city'])>0)?$shipping_address['city']:$contactPostCity;
            $deliveryPostNo = (strlen($shipping_address['postcode'])>0)?$shipping_address['postcode']:$contactPostNo;



            if($multiStorePlugin && $paymentMethod == 3){
                $user = getUserByEmailAndNettbutikk($billing_address['email']);
                //WriteLog('THE USER: '.print_r($user,true));
                if($user){
                    $user_id = $user->contactId;
                    if(strlen($contactAddressline1)<=0 || ctype_space($contactAddressline1))
                        $contactAddressline1 = $user->invoiceAddress;

                    if(strlen($contactAddressline2)<=0 || ctype_space($contactAddressline2))
                        $contactAddressline2 = $user->invoiceAddress2;

                    if(strlen($contactName)<=0 || ctype_space($contactName))
                        $contactName = $user->name;

                    if(strlen($contactPostCity)<=0 || ctype_space($contactPostCity))
                        $contactPostCity = $user->invoicePostCity;

                    if(strlen($contactPostNo )<=0 || ctype_space($contactPostNo))
                        $contactPostNo = $user->invoicePostNo;

                    if(strlen($deliveryAddressLine1)<=0 || ctype_space($deliveryAddressLine1))
                        $deliveryAddressLine1 = $user->invoiceAddress;

                    if(strlen($deliveryAddressLine2)<=0 || ctype_space($deliveryAddressLine2))
                        $deliveryAddressLine2 = $user->invoiceAddress2;

                    if(strlen($deliveryName)<=0 || ctype_space($deliveryName))
                        $deliveryName = $user->name;

                    if(strlen($deliveryPostCity)<=0 || ctype_space($deliveryPostCity))
                        $deliveryPostCity = $user->invoicePostCity;

                    if(strlen($deliveryPostNo)<=0 || ctype_space($deliveryPostNo))
                        $deliveryPostNo = $user->invoicePostNo;

                }
            }
            else{
                $user = get_user_by( 'email', $billing_address['email'] );
                if(!$user){
                    $user_id = createCustomerFromOrder($order);
                    update_post_meta( $order->get_id(), '_customer_user', $user_id );

                }else{
                    $user_id =  $user->ID;
                }
            }
            $user_info = get_userdata($user_id);
            if(!in_array('customer',$user_info->roles)){
                $role = get_role('customer');
                if($role&&$user){
                    $user->add_role($role->name);
                }
            }

            $costumer_id_from_order = get_post_meta( $order->get_id(), '_customer_user', true );
            if(empty($costumer_id_from_order) || $costumer_id_from_order != $user_id){
                update_post_meta( $order->get_id(), '_customer_user', $user_id );
            }

            $kunde_mesage = (strlen($order->get_customer_note())>0)?'Melding fra kunde: '.$order->get_customer_note():'';
            if($order_info_settings == 'yes'){
                $the_message_ink_plukklisteinfo = $ekstra_product_info.' '.$kunde_mesage;
            }else{
                $the_message_ink_plukklisteinfo = $kunde_mesage;
            }

            if(strlen($contactName)<=0 || ctype_space($contactName))
                $contactName = $billing_address['email'];

            if(strlen($deliveryName)<=0 || ctype_space($deliveryName))
                $deliveryName = $billing_address['email'];

            $order_to_pck['contactAddressline1'] = $contactAddressline1;
            $order_to_pck['contactAddressline2'] = $contactAddressline2;
            $order_to_pck['contactId'] = $user_id;
            $order_to_pck['contactName'] = $contactName;
            $order_to_pck['contactPostCity'] = $contactPostCity;
            $order_to_pck['contactPostNo'] = $contactPostNo;



            $order_to_pck['deliveryAddressLine1'] = $deliveryAddressLine1;
            $order_to_pck['deliveryAddressLine2'] = $deliveryAddressLine2;
            $order_to_pck['deliveryName'] = $deliveryName;
            $order_to_pck['deliveryPostCity'] = $deliveryPostCity;
            $order_to_pck['deliveryPostNo'] = $deliveryPostNo;

            $order_to_pck['deltaOrderId'] = $order_id;

            $order_to_pck['email'] = $billing_address['email'];
            $order_to_pck['extraCost'] = 0.0;//round($extraCost,2);
            $order_to_pck['extraCostDescription'] = $extraCoastDesc;
            $order_to_pck['freightCost'] = round((integer)$shipping_cost,2);

            $order_to_pck['orderLines'] = $orderLines;
            $order_to_pck['paymentMethod']=$paymentMethod;
            $order_to_pck['phone'] = $billing_address['phone'];

            $order_to_pck['freightCostDescription'] = $order->get_shipping_method();//$this->cleanFreightDescription($magentoOrder->getShippingDescription());
            $order_to_pck['message'] = $the_message_ink_plukklisteinfo;
            $order_to_pck['storePickup'] = ($order->get_shipping_method() == 'Hent i butikken')?true:false;

            if($sending_type == 'use_warehouses'){
                $paymentMethod_title = $order->get_payment_method_title();
                $authorzationId = get_post_meta($order_id, '_order_key',true);

                $order_to_pck['deliveredItemsAndCapturedPaymentInfo'] = array(
                    'amount' => $order->get_total(),
                    'authorzationId'=> $authorzationId,
                    'extraCost' => 0.0,
                    'freightCost'=>round((integer)$shipping_cost,2),
                    'paymentMethod'=>$paymentMethod_title
                );
            }

            $ordersArray[] = $order_to_pck;
            $valgt_status = 'completed';
            if(isset($ordersTabOptions[ 'm360_pck_credit_order_status'] )){
                $valgt_status = $ordersTabOptions[ 'm360_pck_credit_order_status'];
            }



            if($paymentMethod == 3 && $order->get_status() != $valgt_status){
                $valgt_status = $this->getCreditOrderStatusFromOptions();
                $order->update_status($valgt_status,'order '.$order_id.' has been sendt to PCK as a credit order' );
            }else if($sending_type != 'use_warehouses'){
                $order->update_status( 'processing','order '.$order_id.' has been sendt to PCK' );
            }

        }

	    $result_to_return = $this->cleanOrdersArray($ordersArray,$update_order_status);
        //WriteLog('result_to_return: '.print_r($result_to_return,true));
        //return array();
        return $result_to_return;
    }

    private function cleanOrdersArray($ordersArray,$update_order_status){
        //global $wpdb;
        //$table_name = m360_pck_get_table_name("m360_pckasse_orderstatus");
        //WriteLog('Orders: '.print_r($ordersArray,true));
        /*
	    foreach ($ordersArray as $order_to_send){
	        $order_id = $order_to_send['deltaOrderId'];
            $order = wc_get_order($order_id);

            $qurr = "SELECT * FROM ".$table_name." WHERE order_id = $order_id";
            $WP_order = $wpdb->get_row($qurr,OBJECT);

            $submitting_now = count($order_to_send['orderLines']);
            $old_submuitted = $WP_order->total_submitted;//get_total_submitted($order_id);

            $total_submitting = $submitting_now+$old_submuitted;
            $new_status = $WP_order->status;

            if($total_submitting >= $order->get_item_count()){
                $new_status = M360PCKOrderStatus::STATUS_SUBMITTED;
            }

            if($update_order_status)M360PCKOrderStatus::UpdateOrderStatus($order_id,$WP_order->wc_order_total,$WP_order->wc_order_delivered,$new_status,$WP_order->nettbutikk_id,$total_submitting);
        }
        */
        //
        foreach ($ordersArray as $index =>$order_to_send){
            $orderLines = $order_to_send['orderLines'];
            if(count($orderLines)<=0){
                unset($ordersArray[$index]);
            }else{
	            $kasse_nr = retriveKasseNr();
	            update_post_meta($order_to_send['deltaOrderId'],'_did_sendt_to_pck_to_kasse_nr_'.$kasse_nr,true);
            }
        }

        //WriteLog('Orders2: '.print_r($ordersArray,true));
        //return array();
        return $ordersArray;

    }
}
