<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;
$product_id = $product->get_id();

$with_deposits = get_post_meta( $product_id, '_m360_with_deposits', true );
$deposits_price = get_post_meta( $product_id, '_m360_with_deposits_price', true );


?>
<p class="price">
    <?php echo $product->get_price_html(); ?>
</p>
<?php if(!empty($with_deposits) && $with_deposits == 'yes' && !empty($deposits_price) && strlen($deposits_price)>0):?>
    <p style="padding: 5px; background-color: rgba(255,25,23,0.5);display: inline-flex">Det er&nbsp;&nbsp;<?php echo wc_price($deposits_price); ?>&nbsp;&nbsp;på denne varen</p>
<?php endif; ?>
