<?php 
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'bring_plugin_options_install'); 
// function to create the DB / Options / Defaults					
function bring_plugin_options_install() {
   	global $wpdb; 
	$bring_postoffice = $wpdb->prefix . 'bring_postoffices'; 
	echo 'here';
	exit; 
	// create the ECPT metabox database table
	if($wpdb->get_var("SHOW TABLES LIKE '$bring_postoffice'") != $bring_postoffice) 
	{
		$sql = "CREATE TABLE " . $bring_postoffice . " (
		`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
			`postoffice_id` VARCHAR(64) NOT NULL COMMENT 'Servicepoint Id',
			`postoffice_data` TEXT NOT NULL COMMENT 'Servicepoint Data',
			PRIMARY KEY (`id`),
			UNIQUE INDEX `UNQ_BRING_POSTOFFICES_POSTOFFICE_ID` (`postoffice_id`)
		)
		COMMENT='bring_postoffices'
		COLLATU='utf8_general_ci'
		ENGINE=InnoDB
		);";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);  
	}
 
}