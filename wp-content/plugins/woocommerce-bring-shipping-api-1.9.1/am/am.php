<?php

/**
 ******************************************************************************************************
 *	Todd Lahman support staff will integrate this code into your product for a small fee.
 *
 *	Visit https://www.toddlahman.com/shop/api-code-integration/ to get started.
 ******************************************************************************************************
 *
 *	Intellectual Property rights, and copyright, reserved by Todd Lahman, LLC as allowed by law include,
 *	but are not limited to, the working concept, function, and behavior of this plugin,
 *	the logical code structure and expression as written.
 *
 *
 * @package     WooCommerce API Manager Example Plugin
 * @author      Todd Lahman LLC
 * @category    Plugin
 * @copyright   Copyright (c) Todd Lahman LLC
 */
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Displays an inactive message if the API License Key has not yet been activated
 */
if ( get_option( WC_Bring_Shipping_Class::plugin_name() . '_activated' ) != 'Activated' ) {
    add_action( 'admin_notices', 'WC_Bring_Shipping_Class::wc_bring_shipping_inactive_notice' );
}
class WC_Bring_Shipping_Class {
	/**
	 * Self Upgrade Values
	 */
	// Base URL to the remote upgrade API Manager server. If not set then the Author URI is used.
	public $upgrade_url;
	/**
	 * @var string
	 */
	public $version;
	/**
	 * @var string
	 * This version is saved after an upgrade to compare this db version to $version
	 */
	public $wc_bring_shipping_version_name;
	/**
	 * @var string
	 */
	public $plugin_url;
	/**
	 * @var string
	 */
	public $my_plugin;
	/**
	 * @var string
	 */
	public $plugin_name;
	/**
	 * @var string
	 * used to defined localization for translation, but a string literal is preferred
	 *
	 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/issues/59
	 * http://markjaquith.wordpress.com/2011/10/06/translating-wordpress-plugins-and-themes-dont-get-clever/
	 * http://ottopress.com/2012/internationalization-youre-probably-doing-it-wrong/
	 */
	public $text_domain;
	/**
	 * Data defaults
	 * @var mixed
	 */
	private $ame_software_product_id;
	public $ame_data_key;
	public $ame_api_key;
	public $ame_activation_email;
	public $ame_product_id_key;
	public $ame_instance_key;
	public $ame_deactivate_checkbox_key;
	public $ame_activated_key;
	public $ame_deactivate_checkbox;
	public $ame_activation_tab_key;
	public $ame_deactivation_tab_key;
	public $ame_settings_menu_title;
	public $ame_settings_title;
	public $ame_menu_tab_activation_title;
	public $ame_menu_tab_deactivation_title;
	public $ame_options;
	public $ame_plugin_name;
	public $ame_product_id;
	public $ame_renew_license_url;
	public $ame_instance_id;
	public $ame_domain;
	public $ame_software_version;
	public $ame_plugin_or_theme;
	public $ame_update_version;
	public $ame_update_check = 'wc_bring_shipping_update_check';
	/**
	 * Used to send any extra information.
	 * @var mixed array, object, string, etc.
	 */
	public $ame_extra;
    /**
     * @var The single instance of the class
     */
    protected static $_instance = null;
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
        	self::$_instance = new self();
        }
        return self::$_instance;
    }
	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.2
	 */
	private function __clone() {}
	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.2
	 */
	private function __wakeup() {}
	public function __construct() {
		// Run the activation function

    register_activation_hook( WBS_FILE, array( $this, 'activation' ) );
		// Ready for translation
		//load_plugin_textdomain( $this->text_domain, false, dirname( untrailingslashit( plugin_basename( WBS_FILE ) ) ) . '/languages' );
		if ( is_admin() ) {
			$this->upgrade_url = WBS_UPRADE_URL;
			$this->text_domain = WBS_TEXT_DOMAIN;
			if ( ! function_exists( 'get_plugin_data' ) ) {
				include_once( ABSPATH . 'wp-admin/includes/plugin.php');
			}
			$this->my_plugin = get_plugin_data( WBS_FILE, false, false );
			$this->plugin_name = strtolower( str_replace(' ', '_', $this->my_plugin['Name'] ) );
			// Check for external connection blocking
			add_action( 'admin_notices', array( $this, 'check_external_blocking' ) );
			/**
			 * Software Product ID is the product title string
			 * This value must be unique, and it must match the API tab for the product in WooCommerce
			 */
			$this->ame_software_product_id = $this->my_plugin['Name'];
			// Set the version based on this plugin's header Version
			$this->version = $this->my_plugin['Version'];
			// This version is saved after an upgrade to compare this db version to $version
			$this->wc_bring_shipping_version_name = $this->my_plugin['Name'] . '_version';
			/**
			 * Set all data defaults here
			 */
			$this->ame_data_key 				= $this->plugin_name;
			$this->ame_api_key 					= 'api_key';
			$this->ame_activation_email 		= 'activation_email';
			$this->ame_product_id_key 			= $this->plugin_name . '_product_id';
			$this->ame_instance_key 			= $this->plugin_name . '_instance';
			$this->ame_deactivate_checkbox_key 	= $this->plugin_name . '_deactivate_checkbox';
			$this->ame_activated_key 			= $this->plugin_name . '_activated';
			/**
			 * Set all admin menu data
			 */
			$this->ame_deactivate_checkbox 			= 'am_deactivate_example_checkbox';
			$this->ame_activation_tab_key 			= $this->plugin_name . '_dashboard';
			$this->ame_deactivation_tab_key 		= $this->plugin_name . '_deactivation';
			$this->ame_settings_menu_title 			= WBS_SETTINGS_MENU_TITLE;
			$this->ame_settings_title 				= WBS_SETTINGS_TITLE;
			$this->ame_menu_tab_activation_title 	= __( 'License Activation', 'api-manager-example' );
			$this->ame_menu_tab_deactivation_title 	= __( 'License Deactivation', 'api-manager-example' );
			/**
			 * Set all software update data here
			 */
			$this->ame_options 				= get_option( $this->ame_data_key );
			$this->ame_plugin_name 			= untrailingslashit( plugin_basename( WBS_FILE ) ); // same as plugin slug. if a theme use a theme name like 'twentyeleven'
			$this->ame_product_id 			= get_option( $this->ame_product_id_key ); // Software Title
			$this->ame_renew_license_url 	= WBS_RENEW_LICENSE_URL; // URL to renew a license. Trailing slash in the upgrade_url is required.
			$this->ame_instance_id 			= get_option( $this->ame_instance_key ); // Instance ID (unique to each blog activation)
			/**
			 * Some web hosts have security policies that block the : (colon) and // (slashes) in http://,
			 * so only the host portion of the URL can be sent. For example the host portion might be
			 * www.example.com or example.com. http://www.example.com includes the scheme http,
			 * and the host www.example.com.
			 * Sending only the host also eliminates issues when a client site changes from http to https,
			 * but their activation still uses the original scheme.
			 * To send only the host, use a line like the one below:
			 *
			 * $this->ame_domain = str_ireplace( array( 'http://', 'https://' ), '', home_url() ); // blog domain name
			 */
			$this->ame_domain 				= str_ireplace( array( 'http://', 'https://' ), '', home_url() ); // blog domain name
			$this->ame_software_version 	= $this->version; // The software version
			$this->ame_plugin_or_theme 		= 'plugin'; // 'theme' or 'plugin'
			// Performs activations and deactivations of API License Keys
			require_once( 'classes/class-wc-key-api.php' );
			// Checks for software updatess
			require_once( 'classes/class-wc-plugin-update.php' );
			// Admin menu with the license key and license email form
			require_once( 'admin/class-wc-api-manager-menu.php' );
			$options = get_option( $this->ame_data_key );
			/**
			 * Check for software updates
			 */
			if ( ! empty( $options ) && $options !== false ) {
				$this->update_check(
					$this->upgrade_url,
					$this->ame_plugin_name,
					$this->ame_product_id,
					$this->ame_options[$this->ame_api_key],
					$this->ame_options[$this->ame_activation_email],
					$this->ame_renew_license_url,
					$this->ame_instance_id,
					$this->ame_domain,
					$this->ame_software_version,
					$this->ame_plugin_or_theme,
					$this->text_domain
					);
			}
		}
		/**
		 * Deletes all data if plugin deactivated
     * 8-05-2017 Uncommented to have the data saved in the db retained
		 */
		register_deactivation_hook( WBS_FILE, array( $this, 'uninstall' ) );
	}
	/** Load Shared Classes as on-demand Instances **********************************************/
	/**
	 * API Key Class.
	 *
	 * @return WC_Bring_Shipping_Key
	 */
	public function key() {
		return WC_Bring_Shipping_Key::instance();
	}
	/**
	 * Update Check Class.
	 *
	 * @return WC_Bring_Shipping_Update_API_Check
	 */
	public function update_check( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra = '' ) {
		return WC_Bring_Shipping_Update_API_Check::instance( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra );
	}
	public static function plugin_name( $normalize = false ) {
		if ( ! function_exists( 'get_plugin_data' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php');
		}
		$my_plugin = get_plugin_data( WBS_FILE, false, false );
		if ( $normalize == false ) {
			return strtolower( str_replace(' ', '_', $my_plugin['Name'] ) );
		} else {
			return $my_plugin['Name'];
		}
	}
	public function plugin_url() {
		if ( isset( $this->plugin_url ) ) {
			return $this->plugin_url;
		}
		return $this->plugin_url = plugins_url( '/', WBS_FILE );
	}
	/**
	 * Generate the default data arrays
	 */
	public function activation() {
		global $wpdb;
    $exists = $this->delete_authentication_info( $product_authentication_info, 'activation' );
    $global_options = array(
			$this->ame_api_key 				=> ($exists['api_key'] ?: ''),
			$this->ame_activation_email 	=> ($exists['activation_email'] ?: ''),
		);
		update_option( $this->ame_data_key, $global_options );

		require_once( 'classes/class-wc-api-manager-passwords.php' );
		$wc_bring_shipping_password_management = new WC_Bring_Shipping_Password_Management();
		// Generate a unique installation $instance id
		$instance = $wc_bring_shipping_password_management->generate_password( 12, false );
		$single_options = array(
			$this->ame_product_id_key 			=> $this->ame_software_product_id,
			$this->ame_instance_key 			=> $instance,
			$this->ame_deactivate_checkbox_key 	=> 'on',
			$this->ame_activated_key 			=> ($exists ? 'Activated' : 'Deactivated'),
			);
		foreach ( $single_options as $key => $value ) {
			update_option( $key, $value );
		}
		$curr_ver = get_option( $this->wc_bring_shipping_version_name );

		// checks if the current plugin version is lower than the version being installed
		if ( version_compare( $this->version, $curr_ver, '>' ) ) {
			// update the version
			update_option( $this->wc_bring_shipping_version_name, $this->version );
		}
	}
	/**
	 * Deletes all data if plugin deactivated
	 * @return void
	 */
	public function uninstall() {
		global $wpdb, $blog_id;
		$this->license_key_deactivation();
		// Remove options
		if ( is_multisite() ) {
			switch_to_blog( $blog_id );
			foreach ( array(
					$this->ame_data_key,
					$this->ame_product_id_key,
					$this->ame_instance_key,
					$this->ame_deactivate_checkbox_key,
					$this->ame_activated_key,
					) as $option) {
            $product_authentication_info = get_option($option, true);
            $this->delete_authentication_info( $product_authentication_info, 'uninstall', $option );
					}
			restore_current_blog();
		} else {
			foreach ( array(
					$this->ame_data_key,
					$this->ame_product_id_key,
					$this->ame_instance_key,
					$this->ame_deactivate_checkbox_key,
					$this->ame_activated_key
					) as $option) {
            $product_authentication_info = get_option($option, true);
            $this->delete_authentication_info( $product_authentication_info, 'uninstall', $option );
					}
		}
	}

  /**
   * Deletes the info unless there is already an api key filled in
   * @return void
   */
  private function delete_authentication_info( $product_authentication_info, $state, $option = false )
  {
    if($state == 'activation')
    {
      $saved_key = get_option('am_woocommerce_saved_key'); // Saved options

      // If there is an api key and email available return the saved options
      if( isset($saved_key) && !empty( $saved_key['api_key'] ) && !empty( $saved_key['activation_email'] ) )
      {
        return $saved_key;
      }
      else
      {
        return false;
      }
    }
    elseif($state == 'uninstall' )
    {
      // If there is an api key set save it in a seperate option
      if( is_array( $product_authentication_info ) && isset( $product_authentication_info['api_key'] ) )
      {
        update_option( 'am_woocommerce_saved_key', $product_authentication_info );
      }
      delete_option( $option ); // Delete every bring option
    }
  }

	/**
	 * Deactivates the license on the API server
	 * @return void
	 */
	public function license_key_deactivation() {
		$activation_status = get_option( $this->ame_activated_key );
		$api_email = $this->ame_options[$this->ame_activation_email];
		$api_key = $this->ame_options[$this->ame_api_key];
		$args = array(
			'email' => $api_email,
			'licence_key' => $api_key,
			);

		if ( $activation_status == 'Activated' && $api_key != '' && $api_email != '' ) {
			$this->key()->deactivate( $args ); // reset license key activation
		}
	}
    /**
     * Displays an inactive notice when the software is inactive.
     */
	public static function wc_bring_shipping_inactive_notice() { ?>
		<?php if ( ! current_user_can( 'manage_options' ) ) return; ?>
		<?php if ( isset( $_GET['page'] ) && self::plugin_name() . '_dashboard' == $_GET['page'] ) return; ?>
		<div id="message" class="error">
			<p><?php printf( __( 'The %s API License Key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin.', 'api-manager-example' ), self::plugin_name( true ), '<a href="' . esc_url( admin_url( 'options-general.php?page=' . self::plugin_name() . '_dashboard' ) ) . '">', '</a>' ); ?></p>
		</div>
		<?php
	}
	/**
	 * Check for external blocking contstant
	 * @return string
	 */
	public function check_external_blocking() {
		// show notice if external requests are blocked through the WP_HTTP_BLOCK_EXTERNAL constant
		if( defined( 'WP_HTTP_BLOCK_EXTERNAL' ) && WP_HTTP_BLOCK_EXTERNAL === true ) {

			// check if our API endpoint is in the allowed hosts
			$host = parse_url( $this->upgrade_url, PHP_URL_HOST );

			if( ! defined( 'WP_ACCESSIBLE_HOSTS' ) || stristr( WP_ACCESSIBLE_HOSTS, $host ) === false ) {
				?>
				<div class="error">
					<p><?php printf( __( '<b>Warning!</b> You\'re blocking external requests which means you won\'t be able to get %s updates. Please add %s to %s.', 'api-manager-example' ), $this->ame_software_product_id, '<strong>' . $host . '</strong>', '<code>WP_ACCESSIBLE_HOSTS</code>'); ?></p>
				</div>
				<?php
			}
		}
	}

} // End of class
function WBS() {
    return WC_Bring_Shipping_Class::instance();
}
// Initialize the class instance only once
WBS();
