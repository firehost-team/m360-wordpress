<?php

/**
 * WooCommerce Bring class for calculating and adding rates.
 *
 */
define('PLUGINDIRURLBRING',plugin_dir_url(__FILE__));
class WC_Bring extends WC_Shipping_Method
{

    const TEXT_DOMAIN = 'woocommerce-bring';
    public $NO_PRODUCTS = [
      //'MINIPAKKE'                   => 'MINIPAKKE', // -
      //'A-POST'                      => 'A-POST', // -
      //'SMAAPAKKER_A-POST'           => 'SMAAPAKKER_A-POST', // -
      //'B-POST'                      => 'B-POST', // -
      //'SMAAPAKKER_B-POST'           => 'SMAAPAKKER_B-POST', // -
      'MAIL'                      	    => 'MAIL',  //Add Temporary
      'NORGESPAKKE'                 => 'NORGESPAKKE', //
      'BPAKKE_DOR-DOR'              => 'BPAKKE_DOR-DOR', // -
      'SERVICEPAKKE'                => 'SERVICEPAKKE', // Pickuppoint  -
      'PA_DOREN'                    => 'PA_DOREN', // -
      'EKSPRESS09'                  => 'EKSPRESS09', // -
      'CARGO_GROUPAGE'              => 'CARGO_GROUPAGE', //
      'PICKUP_PARCEL' 		    => 'PICKUP_PARCEL', // Pickuppoint //CARRYON_HOMESHOPPING_NORWAY
      'CARRYON_BUSINESS_NORWAY'     => 'CARRYON_BUSINESS_NORWAY', //
      'COURIER_VIP'                 => 'COURIER_VIP', //
      'COURIER_1H'                  => 'COURIER_1H', //
      'COURIER_2H'                  => 'COURIER_2H', //
      'COURIER_4H'                  => 'COURIER_4H', //
      'COURIER_6H'                  => 'COURIER_6H', //
      'EXPRESS_NORDIC_SAME_DAY'     => 'EXPRESS_NORDIC_SAME_DAY', //
      'EXPRESS_INTERNATIONAL_0900'  => 'EXPRESS_INTERNATIONAL_0900', //
      'EXPRESS_INTERNATIONAL_1200'  => 'EXPRESS_INTERNATIONAL_1200', //
      'EXPRESS_INTERNATIONAL'       => 'EXPRESS_INTERNATIONAL', //
      'EXPRESS_ECONOMY'             => 'EXPRESS_ECONOMY', //
      'CARGO_GROUPAGE'              => 'CARGO_GROUPAGE', //
      'CARGO_INTERNATIONAL'         => 'CARGO_INTERNATIONAL', //
      'BUSINESS_PARCEL'             => 'BUSINESS_PARCEL', //
      'OIL_EXPRESS'                 => 'OIL_EXPRESS', //
      'FRIGO'                  	    => 'FRIGO', //

    ];

    public $DK_PRODUCTS = [

      //'CARRYON_HOMESHOPPING_DENMARK' => 'CARRYON_HOMESHOPPING_DENMARK', // Pickuppoint
      //'CARRYON_BUSINESS_DENMARK'     => 'CARRYON_BUSINESS_DENMARK',
      //'COURIER_VIP'                  => 'COURIER_VIP',
      //'COURIER_1H'                   => 'COURIER_1H',
      //'COURIER_2H'                   => 'COURIER_2H',
      //'COURIER_4H'                   => 'COURIER_4H',
      //'COURIER_6H'                   => 'COURIER_6H',
      'BUSINESS_PALLET'                => 'BUSINESS_PALLET',
      'BUSINESS_PARCEL'                => 'BUSINESS_PARCEL',
      'BUSINESS_PARCEL_BULK'           => 'BUSINESS_PARCEL_BULK',
      'BUSINESS_PARCEL_HALFPALLET'     => 'BUSINESS_PARCEL_HALFPALLET',
      'BUSINESS_PARCEL_QUARTERPALLET'  => 'BUSINESS_PARCEL_QUARTERPALLET',
      'EXPRESS_NORDIC_0900_BULK'       => 'EXPRESS_NORDIC_0900_BULK',
      'HOME_DELIVERY_MAILBOX'          => 'HOME_DELIVERY_MAILBOX',
      'HOME_DELIVERY_PARCEL'           => 'HOME_DELIVERY_PARCEL',
      'PICKUP_PARCEL'                  => 'PICKUP_PARCEL',
      'PICKUP_PARCEL_BULK'             => 'PICKUP_PARCEL_BULK'
	  ];

    public $SE_PRODUCTS = [

     // 'CARRYON_HOMESHOPPING_SWEDEN' => 'CARRYON_HOMESHOPPING_SWEDEN', // Pickuppoint
      //'CARRYON_BUSINESS_SWEDEN'     => 'CARRYON_BUSINESS_SWEDEN',
     // 'COURIER_VIP'                 => 'COURIER_VIP',
     // 'COURIER_1H'                  => 'COURIER_1H',
     // 'COURIER_2H'                  => 'COURIER_2H',
     // 'COURIER_4H'                  => 'COURIER_4H',
     // 'COURIER_6H'                  => 'COURIER_6H',
	  'BUSINESS_PALLET'                => 'BUSINESS_PALLET',
      'BUSINESS_PARCEL'                => 'BUSINESS_PARCEL',
      'BUSINESS_PARCEL_BULK'           => 'BUSINESS_PARCEL_BULK',
      'BUSINESS_PARCEL_HALFPALLET'     => 'BUSINESS_PARCEL_HALFPALLET',
      'BUSINESS_PARCEL_QUARTERPALLET'  => 'BUSINESS_PARCEL_QUARTERPALLET',
      'EXPRESS_NORDIC_0900_BULK'       => 'EXPRESS_NORDIC_0900_BULK',
      'HOME_DELIVERY_MAILBOX'          => 'HOME_DELIVERY_MAILBOX',
      'HOME_DELIVERY_PARCEL'           => 'HOME_DELIVERY_PARCEL',
      'PICKUP_PARCEL'                  => 'PICKUP_PARCEL',
      'PICKUP_PARCEL_BULK'             => 'PICKUP_PARCEL_BULK'

    ];
    public $FI_PRODUCTS = [

      //'CARRYON_HOMESHOPPING_FINLAND' => 'CARRYON_HOMESHOPPING_FINLAND',
      //'CARRYON_BUSINESS_FINLAND'     => 'CARRYON_BUSINESS_FINLAND',
      'BUSINESS_PALLET'                => 'BUSINESS_PALLET',
      'BUSINESS_PARCEL'                => 'BUSINESS_PARCEL',
      'BUSINESS_PARCEL_BULK'           => 'BUSINESS_PARCEL_BULK',
      'BUSINESS_PARCEL_HALFPALLET'     => 'BUSINESS_PARCEL_HALFPALLET',
      'BUSINESS_PARCEL_QUARTERPALLET'  => 'BUSINESS_PARCEL_QUARTERPALLET',
      'EXPRESS_NORDIC_0900_BULK'       => 'EXPRESS_NORDIC_0900_BULK',
      'HOME_DELIVERY_MAILBOX'          => 'HOME_DELIVERY_MAILBOX',
      'HOME_DELIVERY_PARCEL'           => 'HOME_DELIVERY_PARCEL',
      'PICKUP_PARCEL'                  => 'PICKUP_PARCEL',
      'PICKUP_PARCEL_BULK'             => 'PICKUP_PARCEL_BULK'
    ];
	public $rateServicePakee = -1;
	const CUSTOMER_ENDPOINT = 'https://api.bring.com/booking/api/customers';
    const BOOKING_ENDPOINT  = 'https://api.bring.com/booking/api/booking';
    const RETURN_ENDPOINT  = 'https://api.bring.com/documents/ipc/ers/label';
    /**
     * @var WooCommerce
     */
    protected $wooCommerce;

    /**
     * @var WC_Bring
     */
    protected $bring_settings;

    /**
     * @var bool
     */
    public $mybringEnabled;

    /**
     * @var string
     */
    protected $apiEmail;

    /**
     * @var string
     */
    protected $apiKey;

    public $NO_MYBRING_PRODUCTS = [
      'SERVICEPAKKE'                        => 'SERVICEPAKKE',
      //'A-POST'                      	    => 'A-POST',   //Add Temporary
      //'B-POST'                      	    => 'B-POST',  //Add Temporary
      'MAIL'                      	    => 'MAIL',  //Add Temporary
      'PICKUP_PARCEL' 			    => 'PICKUP_PARCEL', // Pickuppoint //CARRYON_HOMESHOPPING_NORWAY ////Add Temporary
      'EKSPRESS09'                          => 'EKSPRESS09',
      'BPAKKE_DOR-DOR'                      => 'BPAKKE_DOR-DOR',
      'PA_DOREN'                            => 'PA_DOREN',
      'BPAKKE_DOR-DOR_RETURSERVICE'         => 'BPAKKE_DOR-DOR_RETURSERVICE',
      'EKSPRESS09_RETURSERVICE'             => 'EKSPRESS09_RETURSERVICE',
      'SERVICEPAKKE_RETURSERVICE'           => 'SERVICEPAKKE_RETURSERVICE',
      //'MINIPAKKE'                           => 'MINIPAKKE',
      'CARRYON_BUSINESS'                    => 'CARRYON_BUSINESS',
      'CARRYON_HOMESHOPPING'                => 'CARRYON_HOMESHOPPING',
      'CARRYON_BUSINESS_PALLET'             => 'CARRYON_BUSINESS_PALLET',
      'CARRYON_BUSINESS_HALFPALLET'         => 'CARRYON_BUSINESS_HALFPALLET',
      'CARRYON_BUSINESS_QUARTERPALLET'      => 'CARRYON_BUSINESS_QUARTERPALLET',
      'CARRYON_BUSINESS_BULKSPLIT'          => 'CARRYON_BUSINESS_BULKSPLIT',
      'CARRYON_BUSINESS_BULKSPLIT_0900'     => 'CARRYON_BUSINESS_BULKSPLIT_0900',
      'CARRYON_HOMESHOPPING_BULKSPLIT'      => 'CARRYON_HOMESHOPPING_BULKSPLIT',
      'CARRYON_HOMESHOPPING_BULKSPLIT_HOME' => 'CARRYON_HOMESHOPPING_BULKSPLIT_HOME',
      'CARRYON_HOMESHOPPING_BULKSPLIT_MINI' => 'CARRYON_HOMESHOPPING_BULKSPLIT_MINI',
    ];
	/**
     * __construct function.
     *

     */

    function __construct()

    {
        $this->id = 'bring';

        $this->method_title = __( 'Bring', self::TEXT_DOMAIN );

        $this->init();

    }

    /**
     * init function.
     *

     */

    function init()

    {

        global $woocommerce;

        // Load the form fields.

        $this->init_form_fields();

        // Load the settings.

        $this->init_settings();

        // Debug configuration

        $this->debug = $this->settings[ 'debug' ];

        $this->log = new WC_Logger();

        $this->weight_unit = get_option( 'woocommerce_weight_unit' );

        $this->dimens_unit = get_option( 'woocommerce_dimension_unit' );

        // Define shop's base country

        $this->shopbasecountry = $this->settings[ 'shopbasecountry' ];

        // Define Productservices for Bring

        $this->noproductstype = $this->settings[ 'noproductstype' ];

        $this->dkproductstype = $this->settings[ 'dkproductstype' ];

        $this->seproductstype = $this->settings[ 'seproductstype' ];

        $this->fiproductstype = $this->settings[ 'fiproductstype' ];

        $this->minimum_cart_dimensions = isset($this->settings[ 'minimum_cart_dimensions' ]) ? $this->settings[ 'minimum_cart_dimensions' ] : null;
        $this->default_product_dimensions = isset($this->settings[ 'default_product_dimensions' ]) ? $this->settings[ 'default_product_dimensions' ]: null;

        // Define user set variables

        $this->enabled = $this->settings[ 'enabled' ];


        $this->title = $this->settings[ 'title' ];

        if (isset( $this->settings[ 'availability' ] ))

        {
            $this->availability = $this->settings[ 'availability' ];
        }

        if (isset( $this->settings[ 'countries' ] ))

        {
            $this->countries = $this->settings[ 'countries' ];
        }

        //$this->fee          = $this->settings['handling_fee'];

        $this->from_zip = $this->settings[ 'from_zip' ];

        //$this->postOffice   = $this->settings['post_office'];

        $this->vat = $this->settings[ 'vat' ];
        $this->helptext = isset($this->settings[ 'helptext' ]) ? $this->settings[ 'helptext' ] : null;;
        $this->language = isset($this->settings[ 'language' ]) ? $this->settings[ 'language' ] : null;;
	    $this->pickuppoint = isset($this->settings[ 'pickuppoint' ]) ? $this->settings[ 'pickuppoint' ] : null;
	    $this->pickuppoint_address = isset($this->settings[ 'pickuppoint_address' ]) ? $this->settings[ 'pickuppoint_address' ] : null;;
        $this->pickuppoint_time = isset($this->settings[ 'pickuppoint_time' ]) ? $this->settings[ 'pickuppoint_time' ] : null;;
        $this->pickuppoint_postoffice = isset($this->settings[ 'pickuppoint_postoffice' ]) ? $this->settings[ 'pickuppoint_postoffice' ] : null;
        $this->pickuppoint_mapkey = isset($this->settings[ 'pickuppoint_mapkey' ]) ? $this->settings[ 'pickuppoint_mapkey' ] : null;
        //$this->pickuppoint_map = isset($this->settings[ 'pickuppoint_map' ]) ? $this->settings[ 'pickuppoint_map' ] : null;
        //$this->pickuppoint_mapsingle = isset($this->settings[ 'pickuppoint_mapsingle' ]) ? $this->settings[ 'pickuppoint_mapsingle' ] : null;
        $this->pickuppoint_map_select = isset($this->settings[ 'pickuppoint_map_select' ]) ? $this->settings[ 'pickuppoint_map_select' ] : null;
        $this->evarsling = isset($this->settings[ 'evarsling' ]) ? $this->settings[ 'evarsling' ] : null;
        $this->price_diffrence = isset($this->settings[ 'price_diffrence' ]) ? $this->settings[ 'price_diffrence' ] : null;
        $this->price_round = isset($this->settings[ 'price_round' ]) ? $this->settings[ 'price_round' ] : null;
        $this->price_decimal = isset($this->settings[ 'price_decimal' ]) ? $this->settings[ 'price_decimal' ] : null;
        $this->posting_office = isset($this->settings[ 'posting_office' ]) ? $this->settings[ 'posting_office' ] : null;
        $this->price_adjustment = isset($this->settings[ 'price_adjustment' ]) ? $this->settings[ 'price_adjustment' ] : null;
        $this->bring_free_shipping = isset($this->settings[ 'bring_free_shipping' ]) ? $this->settings[ 'bring_free_shipping' ] : null;
        $this->bring_free_shipping_cart = isset($this->settings[ 'bring_free_shipping_cart' ]) ? $this->settings[ 'bring_free_shipping_cart' ] : null;
        $this->bring_free_comment_above = isset($this->settings[ 'bring_free_comment_above' ]) ? $this->settings[ 'bring_free_comment_above' ] : null;
        $this->bring_free_comment_below = isset($this->settings[ 'bring_free_comment_below' ]) ? $this->settings[ 'bring_free_comment_below' ] : null;
        $this->bring_checkout_info_field = isset($this->settings[ 'bring_checkout_info_field' ]) ? $this->settings[ 'bring_checkout_info_field' ] : null;

        //$this->url = 'http://fraktguide.bring.no/fraktguide/products/all.json';

	    $this->mybringEnabled = $this->get_option( 'mybring_enabled' ) === 'yes';
        $this->returnEnabled = $this->get_option( 'return_enabled' ) === 'yes';

        $this->apiEmail = $this->get_option( 'mybring_api_email' );
        $this->apiKey   = $this->get_option( 'mybring_api_key' );

        //add_action( 'woocommerce_order_status_processing', [ $this, 'getBookingLabel' ] );
        //add_action( 'woocommerce_update_options_integration_' . $this->id, [ $this, 'process_admin_options' ] );
        //add_filter( 'woocommerce_admin_order_actions', [ $this, 'showBookingLabel' ], 10, 2 );

        $this->url =            "https://api.bring.com/shippingguide/products/all.json";
	    $this->pickuppointapi = 'https://api.bring.com/pickuppoint/api/pickuppoint';

        add_action( 'woocommerce_update_options_shipping_' . $this->id, [ &$this, 'process_admin_options' ] );
	$this->bring_plugin_tabel_install();
        if (!$this->is_valid_for_use())
        {
            $this->enabled = false;
        }
    }

    /**
     * Check if weight or dimensions are enabled.
     *
     */
    public function is_valid_for_use()
    {
        //$dimensions = get_option( 'woocommerce_enable_dimensions' );
        //$weight     = get_option( 'woocommerce_enable_weight' );
        //$currency   = get_option( 'woocommerce_currency' );
        return true;
    }
    /**
     * Default settings
     *
     */
    function init_form_fields()
    {
        global $woocommerce;
        $this->form_fields = [
          'enabled'             => [
            'title'   => __( 'Enable/Disable', self::TEXT_DOMAIN ),
            'type'    => 'checkbox',
            'label'   => __( 'Enable Bring', 'woocommerce-bring' ),
            'default' => 'no',
          ],
          'mybring_title'     => [
            'title'       => __( 'Mybring', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
		  'mybring_enabled'     => [
            'title'       => __( 'Mybring integration', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'description' => __( 'Enable MyBring integration to get pre-made shipping labels and automatic invoicing from Bring.', WC_Bring::TEXT_DOMAIN ),
            'default'     => 'off',
          ],
		      'mybring_testing'     => [
            'title'       => __( 'Mybring Testing', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'description' => __( 'This will generate test labels, and no charges will be incur at mybring', WC_Bring::TEXT_DOMAIN ),
            'default'     => 'on',
          ],
          'mybring_api_email'   => [
            'title'       => __( 'Mybring Api email', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This is the email you use to login mybring.com.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'mybring_api_key'     => [
            'title'       => __( 'Mybring Api key', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'You can find your API key on your profile on mybring.com, there\'s a tab called "My API-Key".', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_name'         => [
            'title'       => __( 'Sender name', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the name in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_addressline1' => [
            'title'       => __( 'Sender address line 1', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the addresss line 1 in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_addressline2' => [
            'title'       => __( 'Sender address line 2', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the addresss line 2 in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_zip_code'     => [
            'title'       => __( 'Sender zip code', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the zip code in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_zip_name'     => [
            'title'       => __( 'Sender zip name/city', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This will be the zip name/city in the return part of the shipping label.', WC_Bring::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'sender_email'        => [
            'title'   => __( 'Sender email', WC_Bring::TEXT_DOMAIN ),
            'type'    => 'text',
            'default' => '',
          ],
          'sender_phone'        => [
            'title'   => __( 'Sender phone', WC_Bring::TEXT_DOMAIN ),
            'type'    => 'text',
            'default' => '',
          ],
          'return_enabled'     => [
            'title'       => __( 'Return label integration', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'description' => __( 'Enable to get pre-made return labels from Bring upon booking (Requires MyBring)', WC_Bring::TEXT_DOMAIN ),
            'default'     => 'off',
          ],

          'general_title'     => [
            'title'       => __( 'General settings', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
          'title'               => [
            'title'   => __( 'Method Title', self::TEXT_DOMAIN ),
            'type'    => 'text',
            'default' => __( 'Bring', self::TEXT_DOMAIN ),
          ],
	  'language'     => [
            'title'   => __( 'Product texts language', self::TEXT_DOMAIN ),
            'type'    => 'select',
            'default' => 'no',
            'description' => __( 'Which language the descriptive product texts should have. If no language is set, or text is not available in the requested language, Norwegian text is returned.', WC_Bring::TEXT_DOMAIN ),
            'options' => [
    		  'no' => __( 'Norwegian', self::TEXT_DOMAIN ),
                  'da' => __( 'Danish', self::TEXT_DOMAIN ),
                  'fi' => __( 'Finnish', self::TEXT_DOMAIN ),
                  'se' => __( 'Swedish', self::TEXT_DOMAIN ),
                  'en' => __( 'English', self::TEXT_DOMAIN ),
    			  ],
			    ],
          'shopbasecountry'     => [
            'title'   => __( 'Shop/Seller Base Country', self::TEXT_DOMAIN ),
            'type'    => 'select',
            'default' => 'norway',
            'class'   => 'basecountry',
            'options' => [
              'NO' => __( 'Norway', self::TEXT_DOMAIN ),
              'DK' => __( 'Denmark', self::TEXT_DOMAIN ),
              'SE' => __( 'Sweden', self::TEXT_DOMAIN ),
              'FI' => __( 'Finland', self::TEXT_DOMAIN ),
            ],
          ],
          'from_zip'            => [
            'title'       => __( 'From zip', self::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This is the zip code of where you deliver from. For example, the post office. Should be 4 digits.', self::TEXT_DOMAIN ),
            'default'     => '',
          ],


          'minimum_cart_dimensions'            => [
            'title'       => __( 'Minimum cart dimensions', self::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This is the minimum dimensions that will be sent to bring. Format: lxwxh (use x between numbers, . as decimal separator. Ex. 2.0x3.5x4.0)', self::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'default_product_dimensions'            => [
            'title'       => __( 'Default product dimensions', self::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( "This is the default dimensions that will be used for a product that doesn't have them. Format: lxwxh (use x between numbers, . as decimal separator. Ex. 2.0x3.5x4.0)", self::TEXT_DOMAIN ),
            'default'     => '',
          ],








          'shipping_products_title'     => [
            'title'       => __( 'Shipping products', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
          'noproductstype'      => [
            'title'       => __( 'Norway bring product list', self::TEXT_DOMAIN ),
            'type'        => 'multiselect',
            'class'       => 'chosen_select',
            'css'         => 'width: 450px;',
            'description' => __( 'Bring products for Norway as base country and My Bring is not supported to MAIL Method. More more info you can click <a target="_blank" href="http://developer.bring.com/api/products/">here</a> to see my Bring API product List.', self::TEXT_DOMAIN ),
            'default'     => '',
            'options'     => $this->NO_PRODUCTS, // array of options for select/multiselects only
          ],
          'dkproductstype'      => [
            'title'       => __( 'Denmark bring product list', self::TEXT_DOMAIN ),
            'type'        => 'multiselect',
            'class'       => 'chosen_select',
            'css'         => 'width: 450px;',
            'description' => __( 'Bring products for Denmark as base country', self::TEXT_DOMAIN ),
            'default'     => '',
            'options'     => $this->DK_PRODUCTS, // array of options for select/multiselects only
          ],
          'seproductstype'      => [
            'title'       => __( 'Sweden bring product list', self::TEXT_DOMAIN ),
            'type'        => 'multiselect',
            'class'       => 'chosen_select',
            'css'         => 'width: 450px;',
            'description' => __( 'Bring products for Sweden as base country', self::TEXT_DOMAIN ),
            'default'     => '',
            'options'     => $this->SE_PRODUCTS, // array of options for select/multiselects only
          ],
          'fiproductstype'      => [
            'title'       => __( 'Finland bring product list', self::TEXT_DOMAIN ),
            'type'        => 'multiselect',
            'class'       => 'chosen_select',
            'css'         => 'width: 450px;',
            'description' => __( 'Bring products for Finland as base country', self::TEXT_DOMAIN ),
            'default'     => '',
            'options'     => $this->FI_PRODUCTS, // array of options for select/multiselects only
          ],
          /*
		 * Doesn't require Shipping from post office option
		 *
		 * 'post_office' => array(
            'title'   => __( 'Post office', self::TEXT_DOMAIN ),
            'type'    => 'checkbox',
            'label'   => __( 'Shipping from post office', self::TEXT_DOMAIN ),
            'default' => 'no'
          ),*/
          'from_zip'            => [
            'title'       => __( 'From zip', self::TEXT_DOMAIN ),
            'type'        => 'text',
            'description' => __( 'This is the zip code of where you deliver from. For example, the post office. Should be 4 digits.', self::TEXT_DOMAIN ),
            'default'     => '',
          ],
          'prices_title'     => [
            'title'       => __( 'Prices', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
          'bring_free_shipping'   => [
                'title'       => __( 'Free Bring Shipping', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'All Shipping will be Free', self::TEXT_DOMAIN ),
                'description' => __( '', self::TEXT_DOMAIN ),
                'default'     => 'no',
          ],
          'vat'                 => [
            'title'       => __( 'Display price', self::TEXT_DOMAIN ),
            'type'        => 'select',
            'description' => __( 'How to calculate delivery charges:<br /><b>Important:</b> In order for the shipping calculation to work then the WooCommerce tax settings must be based on customer shipping address, only. IF you get an error 528, then please check this setting or contact <a href="mailto:support@nettpilot.no" target="_top">support@nettpilot.no</a>.', self::TEXT_DOMAIN ),
            'default'     => 'exclude',
            'options'     => [
            'include' => __( 'VAT included', self::TEXT_DOMAIN ),
            'exclude' => __( 'VAT excluded', self::TEXT_DOMAIN ),
            ],
          ],
          'price_diffrence'               => [

                'title'       => __( 'Display the shipping price difference', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'Display the shipping price difference', self::TEXT_DOMAIN ),
                'description' => __( 'When ticked; displays the shipping price difference', self::TEXT_DOMAIN ),
                'default'     => 'no',
          ],
    		'price_round'               => [
                'title'       => __( 'Shipping price rounding', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'Shipping price rounding', self::TEXT_DOMAIN ),
                'description' => __( 'Rounds the Bring shipping prices. EG: If the price is 25,30 Kr then it is rounded to 25 Kr and if it is 25,60 then it is rounded to 26 Kr.', self::TEXT_DOMAIN ),
                'default'     => 'no',
          ],
    		'price_decimal'               => [
                'title'       => __( 'Shipping price decimal removal', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'Shipping price decimal removal', self::TEXT_DOMAIN ),
                'description' => __( 'Removes the decimal value from the shipping price. EG: If the price is Kr 20.00, it will display as Kr 20.', self::TEXT_DOMAIN ),
                'default'     => 'no',
          ],
          	'price_adjustment'   => [
                'title'       => __( 'Bring price adjustment', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'label'       => __( 'Bring price adjustment using the Query String', self::TEXT_DOMAIN ),
                'description' => __( 'Bring Price Adjustment from Request Query Parameters. EG: priceAdjustment=m20p and if there is more than one adjustment. EG: priceAdjustment=SERVICEPAKKE_79&priceAdjustment=EKSPRESS09_m20p <br /><b>Note: please do not add the "&" sign to the first and the last query string.</b><br />The format of it is <b>[(product code)_][p(lus)|m(inus)]factor[p(ercentage)] </b>where parts inside square brackets are optional and parts inside parentheses are for informational purpose. For more info how to apply, click <a target="_blank" href="http://developer.bring.com/api/shipping-guide/">here</a> and find the "Adjust prices" tab.', self::TEXT_DOMAIN ),
                'default'     => '',
          ],
    		'bring_free_shipping_cart'   => [
                'title'       => __( 'Free Bring shipping on cart total', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'label'       => __( 'Free Bring shipping on cart total', self::TEXT_DOMAIN ),
                'description' => __( 'Free shipping greater than the specific amount mentioned in the field above. EG: 500, when the cart total is greater than 500, the Bring shipping will be free. If you want to specify a cart limit for a selection of bring products use the format, ex.: PA_DOREN=200,SERVICEPAKKE=300. This would limit free shipping for PA_DOREN and SERVICEPAKKE to carts above 200 and 300 respectively.', self::TEXT_DOMAIN ),
                'default'     => '',

          ],
            'pickuppoint_title'     => [
            'title'       => __( 'Pickup point', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],

          'pickuppoint'               => [
                'title'       => __( 'Pickup point', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'Enable pickup point', self::TEXT_DOMAIN ),
                'description' => __( ' ' ),
                'default'     => 'no',
          ],
    		'pickuppoint_postoffice'               => [
                'title'       => __( 'Pickup point post office', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'description' => __( 'Number of post offices that will be shown during checkout. Minimum 1, Maximum 10.', self:: TEXT_DOMAIN ),
                'default'     => '3',
          ],

    		'pickuppoint_address'               => [
                'title'       => __( 'Show pickup point address', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
    		'label'       => __( 'Show the pickup point post office address on the cart and checkout page', self::TEXT_DOMAIN ),
                'description' => __( '', self:: TEXT_DOMAIN ),
                'default'     => 'no',

          ],
    		 'pickuppoint_time'               => [
                'title'       => __( 'Show pickup point opening times', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
    		'label'       => __( 'Show pickup point post office opening times on cart and checkout page', self::TEXT_DOMAIN ),
                'description' => __( '', self:: TEXT_DOMAIN ),
                'default'     => 'no',
          ],
    		'pickuppoint_mapkey'               => [
                'title'       => __( 'Pickup point map key', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'description' => __( 'In order to get a Google Map into your pickup point map you will need a Google Map API key. <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">Learn how and get your API key here</a>.', self:: TEXT_DOMAIN ),
                'default'     => '',
            ],
    		'pickuppoint_map_select'                 => [
                'title'       => __( 'Pickup point map', self::TEXT_DOMAIN ),
                'type'        => 'select',
                'description' => __( 'To show/hide the pickup point map link on cart and checkout', self::TEXT_DOMAIN ),
                'default'     => 'pickuppoint_mapsingle',
                'options'     => [
                    'pickuppoint_map' => __( 'Pickup point map', self::TEXT_DOMAIN ),
                    'pickuppoint_mapsingle' => __( 'Pickup point single map', self::TEXT_DOMAIN ),
                ],
          ],
          'packagedelivery_title'     => [
            'title'       => __( 'Package delivery', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
          'posting_office'   => [
                'title'       => __( 'Delivers the goods to the nearest post office', self::TEXT_DOMAIN ),
                'type'        => 'checkbox',
                'label'       => __( 'Delivers the goods to the nearest post office', self::TEXT_DOMAIN ),
                'description' => __( '', self::TEXT_DOMAIN ),
                'default'     => 'no',
          ],
          'helptext_title'     => [
          'title'       => __( 'Help text and comments', WC_Bring::TEXT_DOMAIN ),
          'type'        => 'title'
          ],
	   'helptext'               => [
            'title'       => __( 'Help Text', self::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'label'       => __( 'Enable help text on cart and checkout page', self::TEXT_DOMAIN ),
            'description' => __( ' ' ),
            'default'     => 'no',
          ],
          'bring_free_comment_above'     => [
                'title'       => __( 'Bring comment above', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'description' => __( 'Comment on the cart and checkout page after the shipping title. Maximum of 75 characters.', self:: TEXT_DOMAIN ),
                'default'     => '',
          ],
    		'bring_free_comment_below'     => [
                'title'       => __( 'Bring comment below', self::TEXT_DOMAIN ),
                'type'        => 'text',
                'description' => __( 'Comment on the cart and checkout page after the shipping options. Maximum of 75 characters.', self:: TEXT_DOMAIN ),
                'default'     => '',
          ],
          'bring_checkout_info_field'     => [
              'title'       => __( 'Checkout shipping field ', self::TEXT_DOMAIN ),
              'type'        => 'text',
              'description' => __( 'Optional. Name of field used for shipping information that should be sent to bring. The data sent to bring will be maximum 35 characters long so be sure to put a limit on the field, or inform the customer', self:: TEXT_DOMAIN ),
              'default'     => '',
        ],
          'notification_title'     => [
            'title'       => __( 'Notifications', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
	'evarsling'          => [
            'title'       => __( 'Enable eVarsling', self::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'label'       => __( 'eVarsling', self::TEXT_DOMAIN ),
            'description' => __( 'A valid mobile phone number or email address is required for eVarsling', self::TEXT_DOMAIN ),
            'default'     => 'no',
          ],
		 /* 'pickuppoint_map'               => [
            'title'       => __( 'Pickup Point Map', self::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'label'       => __( 'Enable Pickuppoint Map', self::TEXT_DOMAIN ),
            'description' => __( 'To Show/Hide Pickup point map link on cart and checkout', self::TEXT_DOMAIN ),
            'default'     => 'no',
          ],
		  'pickuppoint_mapsingle'               => [
            'title'       => __( 'Pickup Point Single Map', self::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'label'       => __( 'Enable Pickuppoint Single Map', self::TEXT_DOMAIN ),
            'description' => __( 'To Show/Hide Pickup point Single map link on cart and checkout', self::TEXT_DOMAIN ),
            'default'     => 'no',
          ],*/
          /*'availability' => array(
            'title'    => __( 'Availability', self::TEXT_DOMAIN ),
            'type'     => 'select',
            'default'  => 'all',
            'class'    => 'availability',
            'options'  => array(
              'all'      => __( 'All allowed countries', self::TEXT_DOMAIN ),
              'specific' => __( 'Specific Countries', self::TEXT_DOMAIN )
            )
          ),

          'countries' => array(
            'title'   => __( 'Specific Countries', self::TEXT_DOMAIN ),
            'type'    => 'multiselect',
            'class'   => 'chosen_select',
            'css'     => 'width: 450px;',
            'default' => '',
            'options' => $woocommerce->countries->countries

          ),*/
          'debug_title'     => [
            'title'       => __( 'Debugging', WC_Bring::TEXT_DOMAIN ),
            'type'        => 'title'
          ],
          'debug'               => [
            'title'       => __( 'Debug', self::TEXT_DOMAIN ),
            'type'        => 'checkbox',
            'label'       => __( 'Enable debug logs', self::TEXT_DOMAIN ),
            'description' => __( 'These logs will be saved in <code>/wc-logs/</code>', self::TEXT_DOMAIN ),
            'default'     => 'no',
          ],
        ];
    }
	/*
	protected function validate_mybring_enabled_field( $key )
    {
        if ($this->is_mybring_enabled())
        {
            $shipping_methods = $this->get_option( 'noproductstype' );
            $unsupported      = array_diff( $shipping_methods, array_keys( $this->NO_MYBRING_PRODUCTS ) );

            if (count( $unsupported ) > 0)
            {
                $this->errors[] = sprintf(
                  __( "%s is/are not supported by bring, and can't be used with mybring integration.", WC_Bring::TEXT_DOMAIN ),
                  join( ',', $unsupported )
                );

                return 'no';
            }
        }

        return $this->validate_checkbox_field( $key );
    }
    protected function is_mybring_enabled()
    {
        return $this->validate_checkbox_field( 'mybring_enabled' ) === 'yes';
    }
*/
    public function display_errors()
    {
        foreach ($this->errors as $key => $value)
        {
            ?>
            <div class="error">
                <p><?php _e( $value, WC_Bring::TEXT_DOMAIN ); ?></p>
            </div>
            <?php
        }
    }


    public function getPickupPoint($shippingMethod){
		$shipping_method =  current(array_filter( $shippingMethod, function ( $shipping_method ) {

            $id_string = $shipping_method->get_meta('id_string');
            if ( $shipping_method->get_method_id() == 'bring' && !empty($id_string) ) return true;

            // Backwards compatability
            $haystack = $shipping_method[ 'method_id' ];
            $needle   = 'bring';

            return strrpos( $haystack, $needle, -strlen( $haystack ) ) !== false;
        }));

        $id_string = $shipping_method->get_meta('id_string');
        if ( !empty($id_string) )   $method_id = $id_string;
        else                        $method_id = $shipping_method[ 'method_id' ];


		$method_id = str_replace( '-bringfirst', '', $method_id );
		$method_id = str_replace( '-bringlast', '', $method_id );

        $expBring = explode (':',$method_id);

        if(isset($expBring[2]) && $expBring[3]) {
			return array("id" => $expBring[2],"countryCode" => $expBring[3]);
		}
		return null;
	}
    public function createBookingObject( WC_Order $order, $customerNumber, $packageType )
    {
$brings 	= new WC_Bring();
      $weight = 0;
      $height = 0;
      $length = 0;
      $width  = 0;

		/*
		foreach ($order->get_items() as $item)
        {
			$product = new WC_Product( $item[ 'product_id' ] );
			$itemQty = $item['qty'];
			if ($product->needs_shipping())
            {
                $weight += $brings->get_weight( $product->get_weight() * $itemQty) ;
                $height += $brings->get_dimension( $product->get_height());
                $length += $brings->get_dimension( $product->get_length() );
                $width 	+= $brings->get_dimension( $product->get_width() );
            }
        }
		 */
		$getDimensions = $this->getDimensionsAdmin($order);
		$weight = $brings->get_weight( $getDimensions['weight']);
		$height = $brings->get_dimension( $getDimensions['height']);
		$width 	= $brings->get_dimension( $getDimensions['width'] );
		$length = $brings->get_dimension( $getDimensions['length'] );
		$shipping_address_2 = trim($order->get_shipping_address_2()) == '' ? '' : substr($order->get_shipping_address_2(),0,33);
		$pickupPoint = null;
		if( $packageType == 'SERVICEPAKKE'){
			$pickupPoint =  $this->getPickupPoint($order->get_shipping_methods());
		}

        $request =  [
          'schemaVersion' => 1,
          'testIndicator' => $brings->get_option( 'mybring_testing' ) === 'yes',
          'consignments'  => [
            [
              'shippingDateTime' => ( time() + ( 60 * 60 ) ) * 1000,
              'parties'          => [
                'sender'      => [
                  'name'         => $brings->get_option( 'sender_name' ),
                  'addressLine'  => $brings->get_option( 'sender_addressline1' ),
                  'addressLine2' => $brings->get_option( 'sender_addressline2' ),
                  'postalCode'   => $brings->get_option( 'sender_zip_code' ),
                  'city'         => $brings->get_option( 'sender_zip_name' ),
                  'countryCode'  => $brings->get_option( 'shopbasecountry' ),
                  'contact'      => [
                    'name'        => $brings->get_option( 'sender_name' ),
                    'email'       => $brings->get_option( 'sender_email' ),
                    'phoneNumber' => $brings->get_option( 'sender_phone' ),
                  ],
                ],
                'recipient'   => [
                  'name'                  => sprintf( '%s %s', $order->get_shipping_first_name(), $order->get_shipping_last_name() ),
                  'addressLine'           => substr($order->get_shipping_address_1(),0,35),
                  'addressLine2'          => $shipping_address_2,
                  'postalCode'            => $order->get_shipping_postcode(),
                  'city'                  => $order->get_shipping_city(),
                  'countryCode'           => $order->get_shipping_country(),//'NO',
                  'additionalAddressInfo' => substr($this->bring_checkout_info_field ? get_post_meta( $this->order->get_id(), $this->bring_checkout_info_field, true ) : $order->get_customer_note(),0,35),
                  'contact'               => [
                    'name'        => sprintf( '%s %s', $order->get_shipping_first_name(), $order->get_shipping_last_name() ),
                    'email'       => substr($order->get_billing_email(),0,35),
                    'phoneNumber' => $order->get_billing_phone(),
                  ],
                ],
                'pickupPoint' => $pickupPoint,
              ],
              'product'          => [
                'id'                 => $packageType,
                'customerNumber'     => $customerNumber,
                'services'           => null,
                'services'           => [
                  'recipientNotification' =>
                    [
                      'email'  => $order->get_billing_email(),
                      'mobile' => $order->get_billing_phone(),
                    ],
                ],
                'customsDeclaration' => null,
              ],
              'purchaseOrder'    => null,
              'correlationId'    => $order->get_id(),
              'packages'         => [
                [
                  'weightInKg' => (ceil($weight)) / 1000.0,
                  'dimensions' => [
                    'heightInCm' => ceil($height),
                    'widthInCm'  => ceil($width),
                    'lengthInCm' => ceil($length),
                  ],
				  //'numberOfItems' => 5,
				 ],
              ],
            ],
          ],
        ];

		if ($request){
            $this->log->add( $this->id, __( 'My Bring Request: ' . print_r( $request, true ), self::TEXT_DOMAIN ) );
		}
		return $request;
    }

    public function createReturnObject( WC_Order $order, $customerNumber, $package_id,$shipment_id ) {
        $brings 	= new WC_Bring();
        $weight = 0;
        $getDimensions = $this->getDimensionsAdmin($order);
		$weight = $brings->get_weight( $getDimensions['weight']);
		$shipping_address_2 = trim($order->get_shipping_address_2()) == '' ? '' : substr($order->get_shipping_address_2(),0,33);


        $request =  [
                'CustomerId'     => $customerNumber,
                'OrderDate'     => $order->get_date_paid(),

                'Sender'      => [
                  'Name'         => $brings->get_option( 'sender_name' ),
                  'PostalCode'   => $brings->get_option( 'sender_zip_code' ),
                  'City'         => $brings->get_option( 'sender_zip_name' ),
                  'CountryCode'  => $brings->get_option( 'shopbasecountry' ),
                  'Street'  => $brings->get_option( 'sender_addressline1' ),
                  #'AddressLine'  => $brings->get_option( 'sender_addressline2' ),
                ],
                'Recipient'   => [
                  'Name'                  => sprintf( '%s %s', $order->get_shipping_first_name(), $order->get_shipping_last_name() ),
                  'PostalCode'            => $order->get_shipping_postcode(),
                  'City'                  => $order->get_shipping_city(),
                  'CountryCode'           => $order->get_shipping_country(),//'NO',
                  'Street'           => substr($order->get_shipping_address_1(),0,35),
                  #'AddressLine'          => $shipping_address_2,
                ],
                'Shipment' => [
                    'PackageId' => $package_id,
                    'ShipmentId' => $shipment_id,
                    //'CustomerReference' => "",
                    'ProductCode' => 0341,
                    'Weight' => ceil($weight) / 1000.0
                ]
        ];


		if ($request){
            $this->log->add( $this->id, __( 'My Bring Request: ' . print_r( $request, true ), self::TEXT_DOMAIN ) );
		}
		return $request;
    }








	/*
    * calculate dimentions...
    */
    public function getDimensionsAdmin($order)
    {
        foreach ($order->get_items() as $item)
        {
            $productId = $item[ 'product_id' ];
			if($item[ 'variation_id' ] > 0){ // if variation of product
				$productId = $item[ 'variation_id' ];
				$_product = new WC_Product_Variation( $productId);
			}else{
				$_product = new WC_Product( $productId );
			}
			$itemQty = $item['qty'];
            //first put all values in corresponding arrays
	$calWeight[ 'weight' ] += $_product->get_weight() * $itemQty;
            for ($i = 0; $i < $itemQty; $i++)
            {
                $check[ 'height' ][] = $_product->get_height();
                $check[ 'length' ][] = $_product->get_length();
                $check[ 'width' ][] = $_product->get_width();
            }
        }
        //

        $calcul[ 'height' ] = array_sum( $check[ 'height' ] );
        $calcul[ 'length' ] = array_sum( $check[ 'length' ] );
        $calcul[ 'width' ] = array_sum( $check[ 'width' ] );
        //this one has the lowest sum when summed, and will be used as the sum-factor
        $toUse = min( $calcul );
        $isUsed = false;
        foreach ($calcul as $key => $val)
        {
            if ($val == $toUse && !$isUsed)
            {
                $isUsed = true;
                $ret[ $key ] = $val;
                continue;
            }
            else
            {

                $ret[ $key ] = max( $check[ $key ] );

            }

        }
		$ret[ 'weight' ] = $calWeight[ 'weight' ];
        return $ret;

    }

    public function getPackageType( WC_Order $order )
    {
		$brings 	= new WC_Bring();
        $bring = current( $brings->findBringShipping( $order ) );

        $id_string = $bring->get_meta('id_string');
        if ( !empty($id_string) ) $method_id = $id_string;
        else $method_id = $bring[ 'method_id' ];

		$method_id = str_replace( '-bringfirst', '', $method_id );
		$method_id = str_replace( '-bringlast', '', $method_id );

        $expBring = explode (':',$method_id);
		isset($brings->settings[ 'pickuppoint' ]) ? $brings->settings[ 'pickuppoint' ] : null;
		return isset($expBring[1]) ? strtoupper($expBring[1]): null ;
        //return strtoupper( str_replace( 'bring:', '', $bring[ 'method_id' ] ) );
    }

    /**
     * @param WC_Order $order
     * @return string
     */
    public function getBringCustomerNumber( WC_Order $order, $packageType )
    {
		$brings 	= new WC_Bring();
        $response = wp_remote_get( self::CUSTOMER_ENDPOINT, [
          'method'  => 'GET',
          'headers' => $brings->getHeaders(),
        ] );

        $result = json_decode( $response[ 'body' ] );

        $customer = array_filter( $result->customers, function ( $customer ) use ( $packageType )
        {
            return $customer->countryCode == 'NO' && in_array( $packageType, $customer->products );
        } );

        return count( $customer ) > 0 ? current( $customer )->customerNumber : false;
    }

    public function getHeaders()
    {
		$brings 	= new WC_Bring();
        return [
          'X-MyBring-API-Uid'  => $brings->apiEmail,
          'X-MyBring-API-Key'  => $brings->apiKey,
          'Content-Type'       => 'application/json',
          'Accept'             => 'application/json',
          'X-Bring-Client-URL' => urlencode( get_site_url() ),
        ];
    }

    public function findBringShipping( WC_Order $order )
    {
        return array_filter( $order->get_shipping_methods(), function ( $shipping_method ) {

            $id_string = $shipping_method->get_meta('id_string');
            if ( $shipping_method->get_method_id() == 'bring' && !empty($id_string) ) return true;

            // Backwards compatability
            $haystack = $shipping_method[ 'method_id' ];
            $needle   = 'bring';

            return strrpos( $haystack, $needle, -strlen( $haystack ) ) !== false;
        });
    }

    /**
     * Display settings in HTML
     *

     */

    public function admin_options()

    {

        global $woocommerce; ?>


        <h3><?php echo $this->method_title; ?></h3>

        <p><?php _e( 'Bring is a shipping method using Bring Shipping Guide to calculate rates. By activating MyBring API function you can make bookings, track your orders and print shipment labels directly from the <a href="edit.php?post_type=shop_order">order view page</a>. The booking and shipment label is created when the order status changes to processing. <br/>', self::TEXT_DOMAIN ); ?></p>

        <div class="notice-warning notice is-dismissible">
          <p>
            <strong><?php _e( 'The shipping calculator requires weight &amp; dimensions to be enabled on the products in the cart in order to function.', self::TEXT_DOMAIN ); ?></strong>
            <?php _e( 'Please also check if dimensions are set in product variables.', self::TEXT_DOMAIN ); ?>
          </p>
        </div>

        <div class="notice-warning notice is-dismissible">
          <p>
            <strong><?php _e( 'Are you using Klarna or customized themes?', self::TEXT_DOMAIN ); ?></strong>
            <?php _e( 'Some themes and payment solutions have their own overrides regarding the shipping setup and do not take into account flexible shipping methods.', self::TEXT_DOMAIN ); ?>
          </p>
        </div>

        <div class="notice-warning notice is-dismissible">
          <p>
            <strong><?php _e( 'Regarding shipping zones:', self::TEXT_DOMAIN ); ?></strong>
            <?php _e( 'Please note to NOT set Norway as a shipping zone without adding the specific ZIP codes. If Norway is set as a shipping zone without other settings, then the Bring shipping method will not be activated.', self::TEXT_DOMAIN ); ?>
          </p>
        </div>

        <?php submit_button();?>

        <div class="ColumnLeft">

      		<?php

      		if($this->mybringEnabled){
      		if($this->checkConnection()){ ?>
      				<div id="message" class="updated">
      					<p>
      						<strong><?php _e( 'Bring API Connection Built Successfully.', self::TEXT_DOMAIN ); ?></strong>
      					</p>
      				</div>
      			<?php }else{ ?>
      				<div id="message" class="error">
      					<p>
      						<strong><?php _e( 'Bring API Detail is not Correct, Please verify Detail.', self::TEXT_DOMAIN ); ?></strong>
      					</p>
      				</div>
      		<?php } }?>

          <table class="form-table">

              <?php //if (get_option( WC_Bring_Shipping_Class::plugin_name() . '_activated' ) != 'Activated') : //@ano ?>

                    <!--<div class="inline error">
                        <p>
                            <?php
                            //printf( __( 'The %s API License Key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin.',
                            //'api-manager-example' ), WC_Bring_Shipping_Class::plugin_name( true ),
                            //'<a href="' . esc_url( admin_url( 'options-general.php?page=' . WC_Bring_Shipping_Class::plugin_name() . '_dashboard' ) ) . '">', '</a>' );
                            ?>
                        </p>
                    </div>-->

              <?php
              //elseif ($this->is_valid_for_use()) :
              if ($this->is_valid_for_use()) :

                  $this->generate_settings_html();

              else : ?>

                  <div class="inline error"><p><strong><?php _e( 'Gateway Disabled', self::TEXT_DOMAIN ); ?></strong>
                          <br/> <?php printf( __( 'Bring shipping method requires <strong>weight &amp; dimensions</strong> to be enabled. Please enable them on the <a href="%s">Catalog tab</a>. <br/> In addition, Bring also requires the <strong>Norweigian Krone</strong> currency. Choose that from the <a href="%s">General tab</a>',
                            self::TEXT_DOMAIN ), 'admin.php?page=woocommerce_settings&tab=catalog', 'admin.php?page=woocommerce_settings&tab=general' ); ?></p></div>

              <?php endif; ?>

          </table>

        </div>

        <div class="ColumnRight">
          <div class="Content">
            <!--<a target="_blank" href="http://www.bring.no/skjema/registrer-din-nettbutikk-til-arets-netthandelspriser"><img alt="Bring advertisement" src="<?php echo plugin_dir_url( dirname( __FILE__ ) ) . 'img/bring-advertisement.png'; ?>"></a>-->
            <h3><?php echo __( 'Useful links', WC_Bring::TEXT_DOMAIN );?></h3>
            <ul>
              <li><a target="_blank" href="https://www.nettpilot.no/bring-kunnskapsbase/"><?php echo __( 'Bring knowledge base', WC_Bring::TEXT_DOMAIN );?></a></li>
              <li><a target="_blank" href="https://www.nettpilot.no/svar/how-do-upgrade-my-woocommerce-plugins/"><?php echo __( 'How do I upgrade my WooCommerce plugins and WordPress?', WC_Bring::TEXT_DOMAIN );?></a></li>
            </ul>
          </div>
        </div>

        <?php

    }

	/*

    * check connection while save changes

    */

    public function checkConnection()

    {

		$brings 	= new WC_Bring();

		if(!trim($this->apiEmail) && !trim($this->apiKey)){
			return false;
		}
        $response = wp_remote_get( self::CUSTOMER_ENDPOINT, [
          'method'  => 'GET',
          'headers' => $brings->getHeaders(),
        ] );

        $result = json_decode( $response[ 'body' ] );
		if(isset($result->customers)){
			return true;
		}
		return false;
	}
    /*

    * calculate dimentions...

    */

    public function getDimensions() {
        global $woocommerce;

        $default_product_dimensions_matches = [];
        $default_prod_dims_isset = $this->default_product_dimensions && preg_match("/^([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)$/", $this->default_product_dimensions, $default_product_dimensions_matches);

        $this->log->add( $this->id, 'Min. LxBxH. : '.$default_product_dimensions_matches[1]." ".$default_product_dimensions_matches[2]." ".$default_product_dimensions_matches[3] );

        foreach ($woocommerce->cart->get_cart() as $values) {
            $_product = $values[ 'data' ];

            if (!$_product->needs_shipping()) {
                continue;
            }
            //first put all values in corresponding arrays
            for ($i = 0; $i < $values[ 'quantity' ]; $i++) {
                $check[ 'height' ][] = $_product->get_height() ?: $default_product_dimensions_matches[1];
                $check[ 'length' ][] = $_product->get_length() ?: $default_product_dimensions_matches[2];
                $check[ 'width' ][] = $_product->get_width() ?: $default_product_dimensions_matches[3];
            }
        }
        //
        $calcul[ 'height' ] = array_sum( $check[ 'height' ] );
        $calcul[ 'length' ] = array_sum( $check[ 'length' ] );
        $calcul[ 'width' ] = array_sum( $check[ 'width' ] );

        //this one has the lowest sum when summed, and will be used as the sum-factor
        $toUse = min( $calcul );
        $isUsed = false;
        foreach ($calcul as $key => $val)
        {
            if ($val == $toUse && !$isUsed)
            {
                $isUsed = true;
                $ret[ $key ] = $val;
                continue;
            }
            else
            {
                $ret[ $key ] = max( $check[ $key ] );
            }
        }
        return $ret;
    }
    /**
     * Calculate shipping costs
     *
     */

    public function calculate_shipping($package=array())
    {
        global $woocommerce;
        $titles = [ ];
        $shipping_required = false;
        $weight = $length = $width = $height = 0;

        $default_product_dimensions_matches = [];
        $default_prod_dims_isset = $this->default_product_dimensions && preg_match("/^([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)$/", $this->default_product_dimensions, $default_product_dimensions_matches);


        foreach ($woocommerce->cart->get_cart() as $values) {
            $_product = $values[ 'data' ];
            // Check if the product has shipping enabled.
            if ( !$_product->needs_shipping() ) {
                continue;
            }


            // Does the product have dimensions?
            if ( $_product->has_dimensions() || $default_prod_dims_isset) {
                $shipping_required = true;

                // Weight of current product.
                $this->log->add( $this->id, "vekten på produktet er :" . $_product->get_weight() );
                $weight += $_product->get_weight() * $values[ 'quantity' ];

                //$length += $_product->length * $values['quantity'];
                //$width  += $_product->width  * $values['quantity'];
                //$height += $_product->height * $values['quantity'];

                if ($this->debug != 'no') {
                    $titles[] = $_product->get_title();
                }
            }
            else {

                if ($this->debug != 'no') {
                    $this->log->add( $this->id, __( 'Cannot calculate Bring shiping as a product added is missing dimensions. Product: ' . $_product->get_title(), self::TEXT_DOMAIN ) );
                }
                return false;
            }

        }

        $dimmentions = $this->getDimensions();

        $length = $dimmentions[ 'length' ];
        $height = $dimmentions[ 'height' ];
        $width = $dimmentions[ 'width' ];


        # Check for minimum cart dimensions
        $min_cart_dimensions_matches = [];
        if ( $this->minimum_cart_dimensions && preg_match("/^([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)x([0-9]*\.?[0-9]+)$/", $this->minimum_cart_dimensions, $min_cart_dimensions_matches) ) {
            $length = max( $length, floatval($min_cart_dimensions_matches[1]) );
            $height = max( $height, floatval($min_cart_dimensions_matches[2]) );
            $width  = max( $width,  floatval($min_cart_dimensions_matches[3]) );
        }

        $this->log->add( $this->id, 'Final LxBxH. : '.$length." ".$width." ".$height );




        // No products are shipable, or no products have dimensions.

        if (empty( $length ))
        {

            if ($shipping_required)
            {

                if ($this->debug != 'no')
                {

                    $this->log->add( $this->id,
                      __( 'The products that were added to the cart do not have dimensions and therefore Bring cannot calculate shipping. Products: ' . print_r( $titles, true ),
                        self::TEXT_DOMAIN ) );
                }
            }
            return false;
        }
        $params = [
          'fromCountry'   => $this->shopbasecountry,
          'from'          => $this->from_zip,
          'to'            => $woocommerce->customer->get_shipping_postcode(),
          'toCountry'     => $woocommerce->customer->get_shipping_country(),
          'length'        => $this->get_dimension( $length ),
          'width'         => $this->get_dimension( $width ),
          'height'        => $this->get_dimension( $height ),
          'weightInGrams' => $this->get_weight( $weight ),

          //'postingAtPostoffice' => ( $this->postOffice == 'no' ) ? 'false' : 'true',
        ];
		if ('yes' == $this->posting_office){
			$paramsPost = ['postingAtPostOffice' => 'true'];
			$params = array_merge($params,$paramsPost);
		}
		if($this->language){
			$params['language'] = $this->language;
		}
        $params = array_filter( $params );
        $query = add_query_arg( $params, $this->url );
		$queryServicePakke = $query;
		$isPickuppoint = false;$isServicePakke = false;
        $priceAdjustmentPacks = [];
        if ('DK' == $this->shopbasecountry)
        {
            if ($this->dkproductstype)
            {
                $prod = "";
                foreach ($this->dkproductstype as $code => $value)
                {
                    if ($this->settings[ 'dkproductstype' ])
                    {
                        if($value == 'PICKUP_PARCEL'){ // For Check Pickuppoint selection
                            $isPickuppoint = true;
                        }
                        if (trim($this->price_adjustment)){
							$pos = strpos($this->price_adjustment,$value.'_');
							if($pos !== false){
								$priceAdjustmentPacks[] =  $value;
								continue;
							}
						}

                        $prod .= '&product=' . $value;

                    }

                }
                $customerQuery = $query;
                $query .= $prod;
            }
        }
        elseif ('SE' == $this->shopbasecountry)
        {
            if ($this->seproductstype)
            {
                $prod = "";

                foreach ($this->seproductstype as $code => $value)
                {

                    if ($this->settings[ 'seproductstype' ])
                    {
                        if($value == 'PICKUP_PARCEL'){ // For Check Pickuppoint selection
                            $isPickuppoint = true;
                        }
						if (trim($this->price_adjustment)){
							$pos = strpos($this->price_adjustment,$value.'_');
							if($pos !== false){
								$priceAdjustmentPacks[] =  $value;
								continue;
							}
						}
                        $prod .= '&product=' . $value;

                    }

                }
				$customerQuery = $query;
                $query .= $prod;

            }

        }
        elseif ('FI' == $this->shopbasecountry)
        {

            if ($this->fiproductstype)
            {

                $prod = "";

                foreach ($this->fiproductstype as $code => $value)
                {

                    if ($this->settings[ 'fiproductstype' ])
                    {
                        if($value == 'PICKUP_PARCEL'){ // For Check Pickuppoint selection
                            $isPickuppoint = true;
                        }
						if (trim($this->price_adjustment)){
							$pos = strpos($this->price_adjustment,$value.'_');
							if($pos !== false){
								$priceAdjustmentPacks[] =  $value;
								continue;
							}
						}
                        $prod .= '&product=' . $value;

                    }

                }
				$customerQuery = $query;
                $query .= $prod;

            }

        }
        else
        {

            if ($this->noproductstype)
            {

                $prod = "";

                foreach ($this->noproductstype as $code => $value)
                {

                    if ($this->settings[ 'noproductstype' ])
                    {

												if($value == 'SERVICEPAKKE' || $value == 'PICKUP_PARCEL'){ // For Check Pickuppoint selection
													$isPickuppoint = true;
												}
												if($value == 'SERVICEPAKKE'){
													$isServicePakke = true;
													$prodServicePakke = '&product=' . $value;
													//continue;
												}
												if (trim($this->price_adjustment)){
													$pos = strpos($this->price_adjustment,$value.'_');
													if($pos !== false){
														$priceAdjustmentPacks[] =  $value;
														continue;
													}
												}
                        $prod .= '&product=' . $value;

                    }

                }
				$customerQuery = $query;
                $query .= $prod;
            }
        }
		$customRequestQuery = array();
		if (trim($this->price_adjustment)){
			$exppriceAdjs = explode('&',$this->price_adjustment);
			if(count($exppriceAdjs) > 0){
				//$priceAdjustmentPacks = implode(';',$priceAdjustmentPack);
				foreach($priceAdjustmentPacks as $priceAdjustmentPack){
					$prodServicePakkepriceAdj = '';
					foreach($exppriceAdjs as $exppriceAdj){
					$exppriceAdj = str_replace('amp;','',$exppriceAdj);
					$exppriceAdj = str_replace('&','',$exppriceAdj);
					$pos = strpos($exppriceAdj, 'SERVICEPAKKE');
					if($pos !== false){
						$prodServicePakkepriceAdj = '&'.$exppriceAdj;
					}
					$posA = strpos($exppriceAdj, $priceAdjustmentPack);
					if($posA !== false){
						$customRequestQuery[$priceAdjustmentPack] = '&'.$exppriceAdj;
						continue;
					}


					}
				}

			}

			//$query .= '&'.$this->price_adjustment;
		}
		$ratesbring = array();
		if($prod){
			$query .= "&clientUrl=Nettpilot_wordpress_plugin";
			$response = wp_remote_get( $query );

			if (is_wp_error( $response )){
				wc_add_notice( __( '<strong>ERROR:</strong> Something went wrong while fetching price from Bring API.', self::TEXT_DOMAIN ), $notice_type='error' );
				return false;
			}
			if ($this->debug != 'no'){
				   $this->log->add( $this->id, __( 'Request comman url : '.print_r( $query, true ), self::TEXT_DOMAIN ) );
			}
			$ratespickup = array();
			$decoded = $this->convert_xml_to_array( $response[ 'body' ] );
			$ratesbring = $this->get_services_from_response( $decoded );
		}


		if(count($customRequestQuery) > 0){
			foreach($customRequestQuery as $prodPackage => $customRequest){
				$customerQueryFinal = '';
				$customerQueryFinal  = $customerQuery;
				$customerQueryFinal .= '&product=' . $prodPackage;
				$customerQueryFinal .= $customRequest;
				$customerQueryFinal .= "&clientUrl=Nettpilot_wordpress_plugin";
				$response = wp_remote_get( $customerQueryFinal );

				if (is_wp_error( $response )){
					wc_add_notice( __( '<strong>ERROR:</strong> Something went wrong while fetching price from Bring API.', self::TEXT_DOMAIN ), $notice_type='error' );
					return false;
				}
				if ($this->debug != 'no'){
				   $this->log->add( $this->id, __( 'Request url for '.$prodPackage.':'. print_r( $customerQueryFinal, true ), self::TEXT_DOMAIN ) );
				}
				$ratespickup = array();
				$decoded = $this->convert_xml_to_array( $response[ 'body' ] );
				$ratespickup = $this->get_services_from_response( $decoded );
				if(is_array($ratespickup) && count($ratespickup) > 0){
					$ratesbring[] = $ratespickup[0];
				}
				//$customeRequestRate[] = array_merge($customeRequestRate,$decodeResponse);
			}
		}


		//https://api.bring.com/shippingguide/products/all.json?fromCountry=NO&from=3520&to=0562&toCountry=NO&length=45&width=10&height=10&weightInGrams=400&product=MINIPAKKE&product=BPAKKE_DOR-DOR&clientUrl=Nettpilot_wordpress_plugin

		//$query = get_site_url().'/bringresponse.txt';


		$rates = $this->array_sort($ratesbring,'cost',SORT_ASC); //SORT_DESC  SORT_ASC

		if($isServicePakke){
			$queryServicePakke .= $prodServicePakke;
			$queryServicePakke .= $prodServicePakkepriceAdj;
			$queryServicePakke .= "&additional=evarsling&clientUrl=Nettpilot_wordpress_plugin";
			$responseServicePakke = wp_remote_get( $queryServicePakke );
			$decodedServicePakke = $this->convert_xml_to_array( $responseServicePakke[ 'body' ] );
			$ratesServicePakke = $this->get_services_from_response( $decodedServicePakke );
			if(!empty($ratesServicePakke)){
				foreach($ratesServicePakke as $servicepakkeRate){
					$this->rateServicePakee = $servicepakkeRate['cost'];
				}
			}
		}

		// For Pickup point
		if ( $this->pickuppoint != 'no' && !empty($rates) ) {
		    if ($isPickuppoint){
				$max_postoffice = $this->pickuppoint_postoffice;
				$places = $this->getDeliveryPlaces($params['toCountry'],$params['to']);
				if (is_array($places)) {
					//$places = array_reverse(array_slice($places,0,$max_postoffice));
					$places = array_slice($places,0,$max_postoffice);
					$rates = $this->get_pickservices_from_response( $places,$rates ,$params['toCountry'] );
				}
			}
		}


		$freeCartShipping = false;
        $free_cart_shipping = trim($this->bring_free_shipping_cart);
        $free_shipping_prods = [];

		if ( $free_cart_shipping ) {
			$cartExcludeTax = $woocommerce->cart->cart_contents_total;
			$cartTaxAmount = 0;
			foreach ( $woocommerce->cart->get_tax_totals() as $code => $tax ) {
						$cartTaxAmount += $tax->amount;
			}
			$cartIncludeTax = $cartExcludeTax + $cartTaxAmount;

            if ( is_numeric($free_cart_shipping) && $cartIncludeTax > $free_cart_shipping ) {
                $freeCartShipping = true;
            }
            else {
                // Free shipping for product
                $prod_lines = explode(",",$free_cart_shipping);

                foreach ($prod_lines as $prod_line) {
                    $prod_line_array = explode("=", $prod_line);
                    $free_prod = $prod_line_array[0];
                    $free_prod_total = floatval($prod_line_array[1]);

                    if ($cartIncludeTax > $free_prod_total ) {
                        $free_shipping_prods[$free_prod] = true;
                    }
                    else {
                        $free_shipping_prods[$free_prod] = false;
                    }
                }
            }

		}

        if ($this->debug != 'no')
        {
			if ($rates)
            {

                $this->log->add( $this->id, __( 'Rates were found: ' . print_r( $rates, true ), self::TEXT_DOMAIN ) );

            }
            else
            {

                $this->log->add( $this->id, __( 'No rates were found for params: ' . "\n" . print_r( $params, true ), self::TEXT_DOMAIN ) );

            }
            if($this->evarsling != 'no'){
				$this->log->add( $this->id, __( 'Request url with Evarsling: ' . print_r( $queryServicePakke, true ), self::TEXT_DOMAIN ));
			}
		}

        if ($rates) {
			$countRates = count($rates);$i=1;


			foreach ($rates as $rate)
            {
				if($countRates == $i) {
					$rate['id'] = $rate['id'].'-bringlast';
				}
				if($i == 1) {
					$rate['id'] = $rate['id'].'-bringfirst';
				}
				if($this->bring_free_shipping != 'no' || $freeCartShipping) {
					$rate['cost'] = 0;
				}

                if ( count($free_shipping_prods) ) {
                    if ( isset($free_shipping_prods[$rate['product_id']]) && $free_shipping_prods[$rate['product_id']] ) {
                        $rate['cost'] = 0;
                    }
                }



                $rate['meta_data'] = [
                    'id_string' => $rate[id]
                ];


				$i++;
                $this->add_rate( $rate );

            }

        }

    }

	 /**
     * Convert the Bring XML response to pickup point places array
     *
     * @param array $places, array $ratesbring , string $toCountry
     * @return array
     */

	public function get_pickservices_from_response( $places , $ratesbring, $toCountry)
    {
		$cost = 0;
		$servicePack = 0;

		if(is_array($ratesbring) && count($ratesbring) > 0){
			foreach($ratesbring as $key => $ratesF){
				if($ratesF['id'] == 'bring:servicepakke' || $ratesF['id'] == 'bring:pickup_parcel'){

					$cost = $ratesF['cost'];
					if($this->evarsling != 'no'){
						if($ratesF['id'] == 'bring:servicepakke' ){
							$cost = $this->rateServicePakee;
						}
					}
					$servicePack = $ratesF['id'];
                    $product_id = $ratesF['product_id'];
					unset($ratesbring[$key]);
					break;
				}
			}
		}
		$rates = $ratesbring;
		if(is_array($places) && count($places) > 0){
			foreach ($places as $place)
			{

			   // $service = $serviceDetails[ 'Price' ][ 'PackagePriceWithoutAdditionalServices' ];

				//$rate = $this->vat == 'exclude' ? $service[ 'AmountWithoutVAT' ] : $service[ 'AmountWithVAT' ];
				$id = $servicePack . ':' . sanitize_title( $place[ 'id' ]). ':' .$toCountry;

				$rate = [
                  'product_id' => $product_id,
				  'id'    => $id,
				  'cost'  => $cost,
				  'label' => $place['name']

				];
				array_push( $rates, $rate );

			}
		}

        return $rates;

    }

	/**
     * Convert the Bring XML response to display html description
     *
     * @param array $places, string $toCountry
     * @return string
     */

	public function formatMethodDescription($place, $toCountry ) {
        if ($toCountry === "SE") {
            return $this->formatMethodDescriptionSE($place);
        } elseif ($toCountry === "FI") {
            return $this->formatMethodDescriptionFI($place);
        } else {
            return $this->formatMethodDescriptionNO($place);
        }
    }

    public function formatMethodDescriptionSE($place) {
        return sprintf('%s, %s %s', $place['address'], $place['postalCode'], $place['city']);
    }

    public function formatMethodDescriptionNO($place) {
        $description = '';//$place['name'].'<br>';
		$descriptionMapTitle = $place['name'];
		$id = $place['id'];
        // Add visiting address if present
		if($this->pickuppoint_address != 'no'){
			if (array_key_exists("visitingAddress", $place)) {
				$description .= mb_convert_case(ucfirst(strtolower($place['visitingAddress'])), MB_CASE_TITLE, "UTF-8") . ", ";
			}
		}
		/*$description .= sprintf('%s %s<br>%s<br><div id="map-wrapper"><a target="_blank" href="%s" class="mapLink">%s</a>',
            $place['visitingPostalCode'],
            $place['visitingCity'],
            $place['openingHoursNorwegian'],
            $place['googleMapsLink'],
            'Vis kart nedenfor',
        );
		*/
		$maplink = $place['googleMapsLink'];//http:\/\/maps.google.com\/maps?f=q&hl=en&geocode=&q=59.9143235049114,10.7741800506662",
    $maplink = preg_replace("/^http:/i", "https:", $maplink); // Må være over HTTPS
		if($this->pickuppoint_map_select == 'pickuppoint_map') {
				$description .= sprintf('%s %s %s <div id="map-wrapper"><a onClick="toggle_visibility(%s);" id="%s" href="javascript:;" class="mapLink"><span id="show-%s">%s</span><span id="hide-%s" style="display:none;">%s</span></a><div style="display:none" id="map-%s"><iframe width="250" height="250" frameborder="0" scrolling="no"  marginheight="0" marginwidth="0"  src="%s&amp;output=embed"></iframe></div></div>',
					($this->pickuppoint_address != 'no') ? mb_convert_case(ucfirst(strtolower($place['visitingPostalCode'])), MB_CASE_TITLE, "UTF-8") : '',
					($this->pickuppoint_address != 'no')  ? mb_convert_case(ucfirst(strtolower($place['visitingCity'])).'<br>', MB_CASE_TITLE, "UTF-8") : '',
					($this->pickuppoint_time != 'no')  ? $place['openingHoursNorwegian'].'<br>' : '',
					$place['id'],
					$place['id'],
					$place['id'],
					__('Show Map', WC_Bring::TEXT_DOMAIN ),
					$place['id'],
					__('Hide Map', WC_Bring::TEXT_DOMAIN ),
					$place['id'],
					$maplink
			);
		}else{
			$description .= sprintf('%s %s %s',
				($this->pickuppoint_address != 'no') ?  mb_convert_case(ucfirst(strtolower($place['visitingPostalCode']) ), MB_CASE_TITLE, "UTF-8") : '',
				($this->pickuppoint_address != 'no') ? mb_convert_case(ucfirst(strtolower($place['visitingCity'])).'<br>', MB_CASE_TITLE, "UTF-8") : '',
				($this->pickuppoint_time != 'no') ? $place['openingHoursNorwegian'].'<br>' : ''
			);
		}
		$descriptionMap = $description;
		return $description;
    }

	 public function formatMapDescriptionNO($place) {
         $description = '';//$place['name'].'<br>';
		 $descriptionMapTitle = $place['name'];
		 // Add visiting address if present
         if (array_key_exists("visitingAddress", $place)) {
             $description .= mb_convert_case(ucfirst(strtolower($place['visitingAddress'])), MB_CASE_TITLE, "UTF-8") . ", ";
         }
		 $description .= sprintf('%s %s<br>%s<br>',
			$place['visitingPostalCode'],
			mb_convert_case(ucfirst(strtolower($place['visitingCity'])), MB_CASE_TITLE, "UTF-8"),
			$place['openingHoursNorwegian']
		);
		return $description;
    }

  private function formatMethodDescriptionFI($place ) {
       return sprintf('%s, %s %s<br>%s<br><a target="_blank" href="%s" class="mapLink">%s</a>',
            $place['address'],
            $place['postalCode'],
            $place['city'],
            $place['openingHoursEnglish'],
            $place['googleMapsLink'],
            Mage::helper('bring')->__('Show map in new window')
        );
	}

	/**
     * Convert the Bring XML response of pickuppoint
     *
     * @param string $getToCountryCode, string $getToPostalCode
     * @return array
     */

	protected function getDeliveryPlaces($getToCountryCode,$getToPostalCode)
    {
        $url = $this->pickuppointapi."/".$getToCountryCode. "/postalCode/" .$getToPostalCode.'.json';
		//$url = get_site_url().'/pickuppoint.txt';

        $json = wp_remote_get($url);

        if ($json){
            $data = $this->convert_xml_to_array( $json[ 'body' ]);
			if (is_array($data) && isset($data['pickupPoint'])) {
                $this->savePostoffice($data['pickupPoint']);
                return $data['pickupPoint'];
            }
        }

        return false;
    }

	/**
     * Pickuppoint postoffice store in DB json
     *
     * @param array $postoffices, string $getToPostalCode
     * @return
     */
	protected function savePostoffice($postoffices)
    {
        global $wpdb;

		if(count($postoffices) > 0){

			foreach ($postoffices as $office){

				$id = $office['id'];

				$jsonData = json_encode($office);
				if (!$jsonData) {
					continue;
				}
				$postofficeId = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}bring_postoffices WHERE postoffice_id =  '$id'" );
				if(trim($postofficeId->postoffice_id)){
					 if ($postofficeId->postoffice_data != $jsonData) {
						$wpdb->update("{$wpdb->prefix}bring_postoffices", array('postoffice_data' => $jsonData), array('postoffice_id' => $postofficeId->postoffice_id));
					}
				}else{
					$query = "INSERT INTO {$wpdb->prefix}bring_postoffices (postoffice_id, postoffice_data) VALUES (%s, %s)";
					$wpdb->query($wpdb->prepare($query, $id, $jsonData));
				}

			}
		}
    }



	// Sorting Array
	function array_sort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
		$sortable_array = array();

		if (is_array($array)) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
				break;
				case SORT_DESC:
					arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

    /**
     * Convert the Bring XML response to an array
     *
     * @param string $response
     * @return array
     */

    public function convert_xml_to_array( $response )
    {

        return json_decode( $response, true );

    }

    /**
     *
     *

     */

    public function get_services_from_response( $array_response ) {
        if (!$array_response || ( is_array( $array_response ) && count( $array_response ) == 0 ) || empty( $array_response[ 'Product' ] )){
            return false;
        }


        $rates = [ ];

        // Fix for when only one product exists. It's not returned in an array :/

        if (empty( $array_response[ 'Product' ][ 0 ] ))
        {

            $cache = $array_response[ 'Product' ];

            unset( $array_response[ 'Product' ] );

            $array_response[ 'Product' ][] = $cache;

        }
		$hentPackages = array();
        foreach ($array_response[ 'Product' ] as $key => $serviceDetails)
        {

            $service = $serviceDetails[ 'Price' ][ 'PackagePriceWithoutAdditionalServices' ];
			$hentPackages[$key]['id'] = strtolower($serviceDetails[ 'ProductId' ]);
			$hentPackages[$key]['DescriptionText'] = $serviceDetails[ 'GuiInformation' ][ 'DescriptionText' ];
			$hentPackages[$key]['HelpText'] = $serviceDetails[ 'GuiInformation' ][ 'HelpText' ];
			$hentPackages[$key]['VAT'] = $service[ 'VAT' ];
			$hentPackages[$key]['AmountWithoutVAT'] = $service[ 'AmountWithoutVAT' ];
			$hentPackages[$key]['AmountWithVAT'] = $service[ 'AmountWithVAT' ];
            //$rate = $this->vat == 'exclude' ? $service[ 'AmountWithoutVAT' ] : $service[ 'AmountWithVAT' ];
            $rate =  $service[ 'AmountWithoutVAT' ];

            $rate = [
              'product_id' => $serviceDetails[ 'ProductId' ],
              'id'    => $this->id . ':' . sanitize_title( $serviceDetails[ 'ProductId' ] ),
              'cost'  => $rate,
              'label' => $serviceDetails[ 'GuiInformation' ][ 'DisplayName' ],

            ];

            array_push( $rates, $rate );

        }

		$this->savePostoffice($hentPackages);
        return $rates;

    }

    /**
     * Return weight in grams
     *

     */

    public function get_weight( $weight )
    {

        $this->log->add( $this->id, "annmeldt vekt er : $weight" );

        switch ($this->weight_unit)
        {

            case 'g' :

                return $weight;

            case 'kg' :

                return $weight / 0.0010000;

            case 'lbs' :

                return $weight / 0.0022046;

            case 'oz' :

                return $weight / 0.035274;

            /* Unknown weight unit */

            default :

                if ($this->debug != 'no')

                {
                    $this->log->add( $this->id, __( sprintf( 'Could not calculate weight unit for %s', $this->weight_unit ), self::TEXT_DOMAIN ) );
                }

                return false;

        }

    }

    /**
     * Return dimension in centimeters
     *

     */

    public function get_dimension( $dimension )
    {

        switch ($this->dimens_unit)
        {

            case 'cm' :

                return $dimension;

            case 'in' :

                return $dimension / 0.39370;

            case 'yd' :

                return $dimension / 0.010936;

            case 'mm' :

                return $dimension / 10.000;

            case 'm' :

                return $dimension / 0.010000;

            /* Unknown dimension unit */

            default :

                if ($this->debug != 'no')

                {
                    $this->log->add( $this->id, __( sprintf( 'Could not calculate dimension unit for %s', $this->dimens_unit ), self::TEXT_DOMAIN ) );
                }

                return false;

        }

    }

	// function to create the DB / Options / Defaults
	public function bring_plugin_tabel_install() {

			global $wpdb;
			$bring_postoffice = $wpdb->prefix . 'bring_postoffices';
			// create the  bring_postoffices database table

			if($wpdb->get_var("SHOW TABLES LIKE '$bring_postoffice'") != $bring_postoffice)
			{
				$sql = "CREATE TABLE " . $bring_postoffice . " (
				`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Id',
					`postoffice_id` VARCHAR(64) NOT NULL COMMENT 'Servicepoint Id',
					`postoffice_data` TEXT NOT NULL COMMENT 'Servicepoint Data',
					PRIMARY KEY (`id`),
					UNIQUE INDEX `UNQ_BRING_POSTOFFICES_POSTOFFICE_ID` (`postoffice_id`)
				)
				COMMENT='bring_postoffices'
				COLLATE='utf8_general_ci'
				ENGINE=InnoDB;";
				require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
				dbDelta($sql);
			}

		}
}

/**
 * add_bring_method function.
 *

 */

function np_add_bring_method( $methods )
{

    $methods[] = 'WC_Bring';

    return $methods;

}

	//Add tracking url customer order view if status completed
function add_order_view_order_trackingurl( $order ) {
		if ( 'wc-completed' == $order->get_status()) {
		  // add tracking url when order email sent
		  $url = get_post_meta( $order->id, '_tracking_url', true );
		  if(trim($url)){
			echo "<p>'.__( '<strong>Order Tracking Url:', WC_Bring::TEXT_DOMAIN ).' </strong><em><a href='$url' target='_blank'>$url </a></em>.</p>";
		  }
		}
	}



//Add tracking url when order email sent on status completed
/*function add_order_email_trackingurl( $order, $sent_to_admin ) {
	  if ( ! $sent_to_admin ) {
		if ( 'wc-completed' == $order->post_status) {
		  // add tracking url when order email sent
		  $url = get_post_meta( $order->id, '_tracking_url', true );
		  if(trim($url)){
			echo "<p><strong>Order Tracking Url: </strong><em>$url</em>.</p>";
		  }
		}
	  }
	}
*/
// Round Shipping and Tax Rate
	function bring_adjust_shipping_rate( $rates ) {
	 // Loop through each rate
	  $brings 	= new WC_Bring();
	  $bringRoundEnable = $brings->price_round;
	  $bringRemoveDecimal = $brings->price_decimal;
	  if($bringRoundEnable == 'yes'){
		   foreach ( $rates as $key=>$rate ) {
				$haystack = $key;
				$needle   = 'bring';
				if(strrpos( $haystack, $needle, -strlen( $haystack ) ) !== false){
					// Store the previous cost in $cost
					$rate->set_cost( round($rate->get_cost()) );
					if(isset($rate->taxes)){
						 //Adjust the cost as needed
                         $taxes = $rate->get_taxes();
						 if(isset($taxes[1])){
							$taxes[1] = round($taxes[1]);
                            $rate->set_taxes($taxes);
						}
					}
				}
		   }
	   }
		return $rates;
	}


//Update Description html while select shipping method
	function add_description_shipping( $method )
	{
		global $wpdb;
		global $mapObj;
		//global $lastbring;
		//bringlast
		$bring_postoffice = $wpdb->prefix . 'bring_postoffices';
		$brings 	= new WC_Bring();

		if($brings->pickuppoint_map_select == 'pickuppoint_mapsingle'){

			if (strpos($method->id, 'bringfirst') !== false) {

				require_once("simpleGMapAPI.php");
				$mapObj = new simpleGMapAPI();
				$mapObj->setWidth('100%');
				$mapObj->setHeight('400px');
				$mapObj->setBackgroundColor('#000');
				$mapObj->setMapDraggable(true);
				$mapObj->setDoubleclickZoom(true);
				$mapObj->setScrollwheelZoom(true);

				$mapObj->showDefaultUI(true);
				$mapObj->showMapTypeControl(true, 'DROPDOWN_MENU');
				$mapObj->showNavigationControl(true, 'DEFAULT');
				$mapObj->showScaleControl(true);
				$mapObj->showStreetViewControl(true);

				//$this->mapObj>setZoomLevel(6); // not really needed because showMap is called in this demo with auto zoom
				$mapObj->setInfoWindowBehaviour('SINGLE_CLOSE_ON_MAPCLICK');
				$mapObj->setInfoWindowTrigger('CLICK');

			}
		}
		//var_dump($mapObj);
		if($method->id){
			 $methodIdexp = explode(':',$method->id);

			 if(isset($methodIdexp[1])){
				//$postofficeId = str_replace('bring:','',$methodIdexp[0]);
				$package = $methodIdexp[1];
				if(isset($methodIdexp[3])){
					$package = $methodIdexp[2];
				}
				$postofficeId = str_replace('-bringfirst','',$package);
				$postofficeId = str_replace('-bringlast','',$postofficeId);


				$postofficeInfo = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}bring_postoffices WHERE postoffice_id =  '$postofficeId'");
				if(trim($postofficeInfo->postoffice_data)){
					$place = $brings->convert_xml_to_array($postofficeInfo->postoffice_data);

					if(isset($methodIdexp[3])){
						$toCountry = $methodIdexp[3];
						$description = $brings->formatMethodDescription($place, $toCountry);

						if($brings->pickuppoint_map_select == 'pickuppoint_mapsingle'){
							$descriptionMapTitle = $place['name'];
							$descriptionMap = $brings->formatMapDescriptionNO($place);
							$mapObj->addMarker($place['latitude'], $place['longitude'] ,$descriptionMapTitle,"<h3>$descriptionMapTitle</h3>".$descriptionMap,'',$method->id);
						}
						echo "<div class='bring-shipping-description'>".$description."</div>";

					}
                    else{
						//$packageService = json_decode($postofficeInfo->postoffice_data);
						if($brings->helptext == 'yes' && $place['DescriptionText'] != 'null' && $place['HelpText'] != 'N/A'){
							$description = $place['DescriptionText'];
							$HelpText = $place['HelpText'];
							echo "<div class='bring-shipping-helptext'>".$description."</div>";
							$imgpath = str_replace('classes/','',PLUGINDIRURLBRING).'img/tooltip.png';
									echo '<a href="#" class="bringtooltip">'.__( 'More..', WC_Bring::TEXT_DOMAIN ).'
										<span>
												<b></b><!--the small arrow on top-->
												'.__( $HelpText, WC_Bring::TEXT_DOMAIN ).'

										</span>
									</a>';
						}
					}

				}
			}
		}
		if($brings->pickuppoint_map_select == 'pickuppoint_mapsingle'){
			if (strpos($method->id, 'bringlast') !== false) {
				echo '<br />';
				//$mapObj->printGMapsJS();
				$mapObj->showMap(true);
			}
		}
		if (strpos($method->id, 'bringlast') !== false) {
				if(trim($brings->bring_free_comment_below)){
					echo '<br /><h4 class="bring-comment-below">'.__( $brings->bring_free_comment_below, WC_Bring::TEXT_DOMAIN ).'</h3>';
				}
		}

	}


// Cart label for Price lable showing
function woo_add_cart_label($label,$method) {
    global $firstcost;
    global $firsttitle;
    global $hentservice;
    global $firstlabeltooltip;
    $label = $method->label;
    $id = $method->id;
    $methodIdexp = explode(':',$method->id);

    $label = apply_filters( 'bring_modify_shipping_label', $label );

    if(count($methodIdexp) > 1){
        if(isset($methodIdexp[0])){
            if($methodIdexp[0] == 'bring'){
                $wcpriceArg = array();
                $showPriceDiffrence = false;
                $brings 	= new WC_Bring();
                $bringRemoveDecimal = $brings->price_decimal;
                $bringPriceDiffrence = $brings->price_diffrence;
                if($bringRemoveDecimal != 'no'){
                    $wcpriceArg = array('decimals'=>'0');
                }

                if($bringPriceDiffrence != 'no'){
                    $showPriceDiffrence = true;
                }

                $rateShipping = $brings->vat == 'exclude' ? 0 : $method->get_shipping_tax();
                //if ( $method->cost > 0 ) {

                if ( WC()->cart->tax_display_cart == 'excl' ) {

                    if($firsttitle <= 0 || count($methodIdexp) > 2){
                        if(!$hentservice && count($methodIdexp) > 2){
                            $hentservice = true;
                            echo '<h3 class="pickupservice">'.__( 'Hent varene selv', WC_Bring::TEXT_DOMAIN ).'<span style="margin-left:10px;">'.wc_price( $method->cost + $rateShipping,$wcpriceArg).'</span></h3>';
                            if($firsttitle <= 0){
                                $firsttitle = 1;
                                if(trim($brings->bring_free_comment_above)){
                                    echo '<h4 class="bring-comment-above">'.__( $brings->bring_free_comment_above, WC_Bring::TEXT_DOMAIN ).'</h4>';
                                }
                            }
                        }

                        //$label .= ': ' . wc_price( $method->cost);
                    }
                    else{
                        if($showPriceDiffrence){
                            $label .= ': +' . wc_price( ($method->cost - $firstcost) + $rateShipping,$wcpriceArg);
                            $imgpath = str_replace('classes/','',PLUGINDIRURLBRING).'img/tooltip-01.png';
                            $label .= '<a href="#" class="bringtooltip"><img src="'.$imgpath.'">
                            <span>
                            <b></b><!--the small arrow on top-->
                            '.__( 'i forhold til '.$firstlabeltooltip, WC_Bring::TEXT_DOMAIN ).'

                            </span>
                            </a>';
                        }
                        else{
                            $label .= ': ' . wc_price( ($method->cost) + $rateShipping,$wcpriceArg);

                        }

                        //$label .= ': +' . wc_price( $method->cost);

                    }
                    if ( $method->get_shipping_tax() > 0 && WC()->cart->prices_include_tax ) {
                        //$label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
                    }
                } else {
                    if($firsttitle <= 0 || count($methodIdexp) > 2){
                        if(!$hentservice && count($methodIdexp) > 2){
                            $hentservice = true;
                            echo '<h3 class="pickupservice">'.__( 'Hent varene selv', WC_Bring::TEXT_DOMAIN ).'<span style="margin-left:10px;">'.wc_price( $method->cost + $rateShipping,$wcpriceArg).'</span></h3>';
                            if($firsttitle <= 0){
                                $firsttitle = 1;
                                if(trim($brings->bring_free_comment_above)){
                                    echo '<h4 class="bring-comment-above">'.__( $brings->bring_free_comment_above, WC_Bring::TEXT_DOMAIN ).'</h4>';
                                }
                            }
                        }

                    }
                    {
                        if($showPriceDiffrence){
                            $label .= ': +' . wc_price( ($method->cost - $firstcost)  + $rateShipping ,$wcpriceArg);
                            //$label .= ': +' . wc_price( ($method->cost )  + $method->get_shipping_tax() );
                            $imgpath = str_replace('classes/','',PLUGINDIRURLBRING).'img/tooltip-01.png';
                            $label .= '<a href="#" class="bringtooltip"><img src="'.$imgpath.'">
                            <span>
                            <b></b><!--the small arrow on top-->
                            '.__( 'i forhold til '.$firstlabeltooltip, WC_Bring::TEXT_DOMAIN ).'

                            </span>
                            </a>';
                        }else{
                            $label .= ': ' . wc_price( ($method->cost) + $rateShipping,$wcpriceArg);

                        }


                    }
                    if ( $method->get_shipping_tax() > 0 && ! WC()->cart->prices_include_tax ) {
                        //$label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
                    }
                }
                if($firsttitle <= 0 && count($methodIdexp) <= 2){
                    $firstcost = $method->cost + $rateShipping;

                    $firsttitle = 1;
                    $firstlabeltooltip =  $method->label;
                    echo '<h3 class="pickupservice">'.__( 'Få varene levert', WC_Bring::TEXT_DOMAIN ).'</h3>';
                    $label .= ': ' . wc_price( $method->cost + $rateShipping,$wcpriceArg);
                    if(trim($brings->bring_free_comment_above)){
                        echo '<h4 class="bring-comment-above">'.__( $brings->bring_free_comment_above, WC_Bring::TEXT_DOMAIN ).'</h4>';
                    }
                }
                //}
                if ( round($method->cost) ==  0 ) {
                    $label .= ' (' . __( 'Free', WC_Bring::TEXT_DOMAIN ) . ')';
                }
            }
        }

    }
    else {
        $label .= ': ' . wc_price( ($method->cost )  + $method->get_shipping_tax() );
    }

    return $label;
}

function getTaxRate(){
			$taxTotal = WC()->cart->get_tax_totals();

			$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
			if(isset($chosen_methods[0])){
				$brings 	= new WC_Bring();
				if($brings->vat != 'exclude'){
					$shippingMethod = $chosen_methods[0];
				     global $wpdb;
					 $methodIdexp = explode(':',$shippingMethod);
					 if(isset($methodIdexp[1])){
						//$postofficeId = str_replace('bring:','',$methodIdexp[0]);
						$package = $methodIdexp[1];
						//if(isset($methodIdexp[3])){
							//$package = $methodIdexp[2];
						//}
						$postofficeId = str_replace('-bringfirst','',$package);
						$postofficeId = str_replace('-bringlast','',$postofficeId);


						$postofficeInfo = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}bring_postoffices WHERE postoffice_id =  '$postofficeId'");
						if(trim($postofficeInfo->postoffice_data)){
							$place = $brings->convert_xml_to_array($postofficeInfo->postoffice_data);
							if(isset($place['VAT'])){
								return $place['VAT'];
							}
						}

					}
			}

		}
	return 0;
}

  function bring_cart_totals_order_total_html(){

	$value = '<strong>' . WC()->cart->get_total() . '</strong> ';
	$bringAmount = getTaxRate();


	// If prices are tax inclusive, show taxes here
	if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'incl' ) {
		$tax_string_array = array();

		if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) {
			foreach ( WC()->cart->get_tax_totals() as $code => $tax )
				//$tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
				$tax_string_array[] = sprintf( '%s %s', wc_price($tax->amount + $bringAmount), $tax->label );
		} else {
			$tax_string_array[] = sprintf( '%s %s', wc_price( WC()->cart->get_taxes_total( true, true ) + $bringAmount ), WC()->countries->tax_or_vat() );
		}

		if ( ! empty( $tax_string_array ) ) {
			$estimated_text = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf( ' ' . __( 'estimated for %s', 'woocommerce' ), WC()->countries->estimated_for_prefix() . __( WC()->countries->countries[ WC()->countries->get_base_country() ], 'woocommerce' ) )
				: '';


				$value .= '<small class="includes_tax">' . sprintf( __( '(includes %s%s)', 'woocommerce' ), implode( ', ', $tax_string_array ), $estimated_text ) . '</small>';


		}
	 }
	return $value;
  }


 /*function showBookingLabel( array $actions, WC_Order $order )
    {

        if ($url = get_post_meta( $order->id, '_tracking_label', true ))
        {
            $actions[] = [
              'url'    => $url,
              'name'   => __( 'Tracking label', WC_Bring::TEXT_DOMAIN ),
              'action' => 'tracking-label',
            ];
        }

        $styles = plugins_url( '../stylesheet.css', __FILE__ );
        wp_enqueue_style( 'wc-bring-booking', $styles );

        return $actions;
    }
	*/
add_filter( 'woocommerce_shipping_methods', 'np_add_bring_method' );

//add_action( 'woocommerce_update_options_integration_' . $this->id, [ $this, 'process_admin_options' ] );

add_action( 'woocommerce_after_shipping_rate', 'add_description_shipping' );
add_filter(	'woocommerce_cart_shipping_method_full_label', 'woo_add_cart_label',10, 2 );
//add_action( 'woocommerce_email_before_order_table', 'add_order_email_trackingurl', 10, 2 );
add_action( 'woocommerce_order_details_after_order_table', 'add_order_view_order_trackingurl', 10, 2 );
add_filter( 'woocommerce_package_rates', 'bring_adjust_shipping_rate', 10 );
//add_filter( 'woocommerce_admin_order_actions', 'showBookingLabel',10);
