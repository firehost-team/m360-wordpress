/* Add bring admin js Code here */


jQuery(document).ready(function(){

	//jQuery('.form-table tr:nth-child(2) input[type="checkbox"]')

	jQuery('#woocommerce_bring_mybring_enabled').click(function() {
				if(jQuery(this).prop("checked") == true){
					showrow();
					//jQuery('#woocommerce_bring_from_zip').prop("disabled", true);
					jQuery('#woocommerce_bring_from_zip').val(jQuery('#woocommerce_bring_sender_zip_code').val());
				}
				else if(jQuery(this).prop("checked") == false){
					hiderow();
					jQuery('#woocommerce_bring_from_zip').prop("disabled", false);
				}

	});



	if (jQuery('#woocommerce_bring_mybring_enabled').is(':checked')){
		showrow();
		//jQuery('#woocommerce_bring_from_zip').prop("disabled", true);
		jQuery('#woocommerce_bring_from_zip').val(jQuery('#woocommerce_bring_sender_zip_code').val());
		jQuery('#woocommerce_bring_sender_zip_code').bind('keypress keyup blur', function() {
			jQuery('#woocommerce_bring_from_zip').val(jQuery(this).val());
		});
	}else{
		hiderow();
		jQuery('#woocommerce_bring_from_zip').prop("disabled", false);
	}

});

function hiderow(){
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(2)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(3)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(4)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(5)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(6)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(7)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(8)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(9)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(10)').hide();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(11)').hide();
}

function showrow(){
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(2)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(3)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(4)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(5)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(6)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(7)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(8)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(9)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(10)').show();
	jQuery('#woocommerce_bring_mybring_title + .form-table tr:nth-child(11)').show();
}

jQuery('.ColumnLeft .wc-settings-sub-title + table').hide();

jQuery('.ColumnLeft .wc-settings-sub-title').click(function() {
	if(jQuery(this).find('+ table').is(":visible"))
	{
		jQuery(this).find('+ table').hide();
	}
	else
	{
		jQuery(this).find('+ table').show();
	}
});
