<?php
/**
 * Plugin Name:         WooCommerce Bring Shipping
 * Plugin URI:          https://www.nettpilot.no/
 * Description:         Automatically calculates shipping charge from shipping provider <a href="http://www.bring.com">Bring</a> for the WooCommerce plugin.
 * Author:              Nettpilot
 * Author URI:          https://www.nettpilot.no/
 *
 * Version:             1.9.1
 *
 * Requires at least:   3.2.1
 * Tested up to:        4.7.2
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.4.0
 *
 * Text Domain:         woocommerce-bring
 * Domain Path:         /languages/
 *
 * Copyright Nettpilot
 *

 */





/**
 * WooCommerce API Manager Integration
 */

 /*

if (!defined( 'WBS_UPRADE_URL' ))
{

    define( 'WBS_UPRADE_URL', 'https://www.nettpilot.no' );

}

if (!defined( 'WBS_RENEW_LICENSE_URL' ))
{

    define( 'WBS_RENEW_LICENSE_URL', 'https://www.nettpilot.no/min-konto/' );

}


if (!defined( 'WBS_SETTINGS_MENU_TITLE' ))
{

    define( 'WBS_SETTINGS_MENU_TITLE', 'Bring Shipping Activation' );

}


if (!defined( 'WBS_SETTINGS_TITLE' ))
{

    define( 'WBS_SETTINGS_TITLE', 'WooCommerce Bring Shipping License Activation' );

}


if (!defined( 'WBS_TEXT_DOMAIN' ))
{

    define( 'WBS_TEXT_DOMAIN', 'woocommerce-bring' );

}


if (!defined( 'WBS_FILE' ))
{

    define( 'WBS_FILE', __FILE__ );

}


require_once( 'am/am.php' );
*/ //@ano - Remove API Manager

/**
 * Start WooCommerce Bring Shipping
 */

function np_init_bring()
{


    /**
     * Check if WooCommerce is active
     * woocommerce_shipping_method class is for woocommerce 1.6.6 or below
     * WC_Shipping_Method class is for woocommerce 2.0 onward
     */
    if (class_exists( 'WC_Shipping_Method' ) && class_exists( 'WC_Integration' ))
    {
        // load language file of plugin if exists

        load_plugin_textdomain( 'woocommerce-bring', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );


        /**
         * Load Class file from "classes" folder
         */

        function np_bring_woocommerce_shipping_init()
        {

            include_once 'classes/class-wc-bring.php';

        }

        add_action( 'woocommerce_shipping_init', 'np_bring_woocommerce_shipping_init' );


       // include_once 'classes/class-wc-bring-booking.php';
       // function np_bring_woocommerce_booking_init( $methods )
       // {
       //     $methods[] = 'WC_Bring_Booking';

       //     return $methods;
       // }


       // add_action( 'woocommerce_integrations', 'np_bring_woocommerce_booking_init' );
    }
}


add_action( 'plugins_loaded', 'np_init_bring' );
/*
Fix issue and it will load only when it is on wp-active page
*/
function returnScript(){
	if($_SERVER['PHP_SELF'] == '/wp-activate.php'){
			return true;
	}
	return false;
}
/*
Load google map js only cart and chekout page
*/
function add_googlemap_js() {
	if(returnScript())return;
	if (is_cart() || is_checkout()) {
		require_once( 'classes/class-wc-bring.php' );
		$brings 	= new WC_Bring();
        $mapKey = $brings->pickuppoint_mapkey;


		echo "\n<script src=\"https://maps.google.com/maps/api/js?key=$mapKey\" type=\"text/javascript\"></script>\n";
	}
}
//add js and css for bring checkout and cart page
function load_script_css_bring() {
		if(returnScript())return;
		$scriptpath = plugin_dir_url(__FILE__).'js/';
		$csspath = plugin_dir_url(__FILE__).'css/';
		//wp_enqueue_style ( 'XXXXXXX-style', $stylepath.'XXXXXX.min.css' );
		//! is_woocommerce() && ! is_cart() && ! is_checkout()

		if (is_cart() || is_checkout()) {
			wp_enqueue_script ('bringjs', $scriptpath . 'bringjs.js',false,
			'1.0',
			true);
			//wp_enqueue_script ('googlemap', 'http://maps.google.com/maps/api/js', false,'1',true);
			wp_enqueue_style ('bringcss', $csspath . 'bring.css',true,
			'1.1');
		}
	}
 //Add tracking url when order email sent on status completed
 function add_order_email_trackingurl( $order, $sent_to_admin ) {
	  if ( ! $sent_to_admin ) {
		if ( 'wc-completed' == $order->post_status) {
		  // add tracking url when order email sent
		  $url = get_post_meta( $order->id, '_tracking_url', true );
		  if(trim($url)){
			echo "<p><strong>Order Tracking Url: </strong><em>$url</em>.</p>";
		  }
		}
	  }
	}

/*
Print Mybring booking label to store booking label Url and Mybring tracking Url
*/
function getBookingLabel( $order_id )
    {
		require_once( 'classes/class-wc-bring.php' );
		$brings 	= new WC_Bring();
        if (!$brings->mybringEnabled) {
            return;
        }

        $order = new WC_Order( $order_id );

		if (!$order->needs_shipping_address()) {
            WC_Admin_Settings::add_error( 'No shipping necessary for this order' );

            return;
        }

        $packageType    = $brings->getPackageType( $order );

        $customerNumber = $brings->getBringCustomerNumber( $order, $packageType );

        $bookingObject = $brings->createBookingObject( $order, $customerNumber, $packageType );

//        var_dump( $bookingObject );
		//Send request booking object to the my bring and return response
        $response = wp_remote_post( $brings::BOOKING_ENDPOINT, [
          'method'  => 'POST',
          'headers' => $brings->getHeaders(),
          'body'    => json_encode( $bookingObject ),
        ] );

        $package_id = "";
        $shipment_id = "";
        if (!is_wp_error( $response ))
        {
            $body = json_decode( $response[ 'body' ] );
			// Iterate Booking Object
            foreach ($body->consignments as $consignment)
            {
                $confirmation = $consignment->confirmation;
                if (!is_null( $confirmation ))
                {
                    $deliveryDate = ( new DateTime( sprintf( '@%d', $confirmation->dateAndTimes->expectedDelivery / 1000 ) ) )->format( 'd.m.Y' );

                    $order->add_order_note( sprintf( 'Expected delivery: %s', $deliveryDate ) );
                    // Storded booking label url to order note
					$order->add_order_note(
                      sprintf( 'Label: <a href="%s" target="_blank">%s</a>', esc_attr( $confirmation->links->labels ), $confirmation->links->labels )
                    );
					// Storded booking tracking url to order note
                    $order->add_order_note(
                      sprintf( 'Tracking: <a href="%s" target="_blank">%s</a>', esc_attr( $confirmation->links->tracking ), $confirmation->links->tracking )
                    );
					// Storded consignment number as order note
                    $order->add_order_note(
                      sprintf( 'Consignment number: %s', $confirmation->consignmentNumber )
                    );

                    update_post_meta( $order->id, '_tracking_label', $confirmation->links->labels );
					update_post_meta( $order->id, '_tracking_url', $confirmation->links->tracking );
                    update_post_meta( $order->id, '_expected_delivery', $deliveryDate );

                    $package_id = $confirmation->packages[0]->packageNumber;
                    $shipment_id = $confirmation->consignmentNumber;


                    /* RETURN LABEL */
                    /*
                    if (!$brings->returnEnabled) {
                        return;
                    }

                    $bookingObject = $brings->createReturnObject( $order, $customerNumber, $package_id, $shipment_id );

                    function array_to_xml($array, &$xml_user_info) {
                        foreach($array as $key => $value) {
                            if(is_array($value)) {
                                if(!is_numeric($key)){
                                    $subnode = $xml_user_info->addChild("$key");
                                    array_to_xml($value, $subnode);
                                }else{
                                    $subnode = $xml_user_info->addChild("item$key");
                                    array_to_xml($value, $subnode);
                                }
                            }else {
                                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
                            }
                        }
                    }
                    $xml = new SimpleXMLElement("<LabelRequest></LabelRequest>");
                    array_to_xml($bookingObject,$xml);


                    $brings->log->add( $brings->id, __( 'Return label request body: ' . "\n" . print_r( $xml->asXML(), true ), $brings::TEXT_DOMAIN ) );

                    $headers = $brings->getHeaders();
                    $headers["Content-Type"] = "text/xml";
                    unset($headers["Accept"]);

            		//Send request booking object to the my bring and return response
                    $response = wp_remote_post( $brings::RETURN_ENDPOINT, [
                      'method'  => 'POST',
                      'headers' => $brings->getHeaders(),
                      'body'    => $xml->asXML(),
                    ] );


                    if (!is_wp_error( $response )) {
                        $brings->log->add( $brings->id, __( 'Return label response body: ' . "\n" . print_r( $response[ 'body' ], true ), $brings::TEXT_DOMAIN ) );
                        $response_xml = new SimpleXMLElement( $response[ 'body' ] );

                        $return_label_uri = $response_xml->Url['url'];


                        $order->add_order_note(
                          sprintf( 'Label: <a href="%s" target="_blank">%s</a>', esc_attr( $return_label_uri ),$return_label_uri )
                        );

                        update_post_meta( $order->id, '_return_label', $return_label_uri );

                    }
                    */

                    /* END RETURN LABEL*/


                }
                else
                {
					// Any Api requested error if any
                    foreach ($consignment->errors as $error)
                    {
                        $order->add_order_note(
                          sprintf( sprintf( 'ERROR! Code: %s. Message. %s', $error->code, current( $error->messages )->message ) )
                        );
                    }

                }
            }
        }




    }




// Showing Booking Label on admin Order Grid page
// WC_Order
 function showBookingLabel( $actions, $the_order = null ) {

		if ($url = get_post_meta( $the_order->id, '_tracking_label', true ) ) {
            $actions[] = [
              'url'    => $url,
              'name'   => __( 'Tracking label'),
              'action' => 'tracking-label',
            ];
        }
        if ($url = get_post_meta( $the_order->id, '_return_label', true ) ) {
            $actions[] = [
              'url'    => $url,
              'name'   => __( 'Return label'),
              'action' => 'return-label',
            ];
        }

        $styles = plugins_url( 'css/tracking-label.css', __FILE__ );
        wp_enqueue_style( 'wc-bring-booking', $styles );

        return $actions;
  }
  add_filter( 'woocommerce_admin_order_actions', 'showBookingLabel', 10, 2 );

/*
Add bring admin js in Admin page
*/
function bring_admin_js( $hook ) {

    $scriptpath = plugin_dir_url(__FILE__).'js/';
	//wp_dequeue_script('bringadminjs');
    wp_enqueue_script ('bringadminjscript', $scriptpath . 'bringadminjs.js',false,
			'2.0',
			true);

}
function bring_admin_css( $hook ) {

    $csspath = plugin_dir_url(__FILE__).'css/';
	wp_enqueue_style ('bringadmincss', $csspath . 'bringadmin.css',true,'1.1');
}
// Add the method to WooCommerce.
add_action(	'wp_head', 'add_googlemap_js' );
if(isset($_GET['section']) && isset($_GET['tab'])){
	if($_GET['section'] == 'bring' && $_GET['tab'] == 'shipping'){
		add_action('admin_enqueue_scripts', 'bring_admin_js');
	}
}
add_action( 'wp_enqueue_scripts', 'load_script_css_bring' );
add_action( 'admin_enqueue_scripts', 'bring_admin_css' );
add_action( 'woocommerce_email_before_order_table', 'add_order_email_trackingurl', 10, 2 );
add_action( 'woocommerce_order_status_processing', 'getBookingLabel');













/*
Add bring shipping amount to calcualte Tax on cart page
*/
function bring_calc_tax_add_shipping( $taxes, $price, $rates, $price_includes_tax, $suppress_rounding ) {
		require_once( 'classes/class-wc-bring.php' );
		$bringAmount = getTaxRate();

		if(is_array($taxes)){
			$taxes[1] = $taxes[1] + $bringAmount;
		}

		return $taxes;

}
///add_filter( 'woocommerce_calc_tax', 'bring_calc_tax_add_shipping',10,5);
add_filter( 'manage_edit-product_columns', 'admin_add_in_promo', 10, 2 );
function admin_add_in_promo($columns){
   //remove column
   //unset( $columns['tags'] );
   //add column

   $columns['is_bringvalid'] = __( 'Bring validate');
   return $columns;
}

add_action( 'manage_posts_custom_column', 'admin_show_in_promo', 10 );
function admin_show_in_promo( $column) {
  global $post;
    if ( $column == 'is_bringvalid' ) {
        if( function_exists('wc_get_product') ){
			$product = wc_get_product( $post->ID );
			 $height = $product->height;
			 $length = $product->length;
             $width = $product->width;
			 $weight = $product->weight;
			 $notifiation = false;
			 $missing = array();
			 if(trim($height) <= 0){
				$notifiation = true;
				$missing[] = "Height";
			 }
			 if(trim($length) <= 0){
				$notifiation = true;
				$missing[] = "Length";
			 }
			 if(trim($width) <= 0){
				$notifiation = true;
				$missing[] = "Width";
			 }
			 if(trim($weight) <= 0){
				$notifiation = true;
				$missing[] = "Weight";
			 }

			 if($notifiation){
				$isOrare = ' is ' ;
				 if(count($missing) > 1){
					$isOrare = ' are ' ;
				 }
				 $missingAttribute = implode(',',$missing). $isOrare. 'missing';
				 $imgpath = plugin_dir_url(__FILE__) .'img/error.png';
				 //echo '<img height="20" width="20" src="'.$imgpath.'" />';
				 echo '<a href="#" class="bringtooltip"><img height="20" width="20" src="'.$imgpath.'">
					<span>
							<b></b><!--the small arrow on top-->
							'.__( $missingAttribute).'

					</span>
				</a>';
			 }else{
				 $imgpath = plugin_dir_url(__FILE__) .'img/success.png';
				 echo '<img height="20" width="20" src="'.$imgpath.'" />';
			 }
        }
    }
}

function bring_activation_redirect( $plugin ) {
    if( $plugin == plugin_basename( __FILE__ ) ) {
        exit( wp_redirect( admin_url( 'options-general.php?page=woocommerce_bring_shipping_dashboard' ) ) );
    }
}
add_action( 'activated_plugin', 'bring_activation_redirect' );
