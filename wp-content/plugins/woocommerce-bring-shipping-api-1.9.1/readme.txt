=== DEVELOPER STRUCTURE ===
This repo now consists of two branches. The master branch holds the
current version and should NEVER be edited directly. All code changes
needs to be done in the "dev" branch.

When a new version is done, and stabil in the dev branch we merge it into
the master branch and tags it with the version number.

This is how you do it:
- $ git checkout master ( change from dev to master )
- $ git merge dev -m "MERGE MESSAGE" ( merging dev into master )
- $ git push
- $ git tag vX.X.X
- $ git push --tags
- $ git checkout dev ( move back to dev )

=== GRUNT FOR EASY TESTING ===
How to get grunt up and running.
- $ npm install - Installs all the grunt dependencies

### Build and push plugin to dev server
1. Duplicate the .ftppass-sample file and replace key1 with the correct FTP cridentials
2. run $ grunt build - This builds the plugin to a /dist folder ready for uploading.
3. run $ grunt dev - This pushes the plugin up to the correct dev server

=== WooCommerce Bring Shipping ===

Automatically calculates rates from Nordic shipping provider <a href="http://www.bring.com">Bring</a> an connects all orders to a href="http://www.mybring.com">MyBring</a>, for the WooCommerce ECommerce plugin.

=== Description ===

Automatically calculate shipment prices of products in your customer's cart using the services of Bring. Also gets shipping labels, traking ID and consignment number directly into WooCommerce order view when order is being made.

Bring is a Nordic challenger in the mail and logistics industry. Bring consists of nine specialist areas: Cargo, Citymail, Dialog, Express, Frigo, Mail, Parcels, Supply Services and Warehousing, each of which are leading players within their areas. Together, they give Bring a broad range of products and services and increase the Group’s competitiveness.

In order to calculate volume, your products must have:
  * Weight
  * Length
  * Height
  * Width


There will be no rates calculated if a product doesn't have the above requirements.
You will allways be able to check prices and calculations on the Bring Demo API at http://fraktguide.bring.no/fraktguide/demoVelgFraktalternativ.do?

=== IMPORTANT NOTE ===
This plugin extends WooCommerce with Bring Shipping and MyBring Booking functionality. The plugin will only work if WooCommerce is installed and activated.

=== Installation ===

1. Unzip and upload the plugin’s folder to your /wp-content/plugins/ directory
2. Activate the extension through the ‘Plugins’ menu in WordPress, and then enter the licensekey and license email to activate settings editor
3. Go to WooCommerce > Settings > Shipping > Bring to configure the plugin
4. For Support, go to https://www.nettpilot.no/support/
5. To upgrade, if not possible directly via the pluginlist view, then deactivate license for plugin, then deactivate and delete plugin. Get new plugin at https://www.nettpilot.no/min-konto/ and start from 1.
