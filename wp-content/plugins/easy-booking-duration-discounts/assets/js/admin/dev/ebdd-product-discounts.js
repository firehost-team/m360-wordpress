(function($) {
	$(document).ready(function() {

		$('#woocommerce-product-data').on('click','.ebdd_product_discounts a.add-discount',function() {
			$(this).closest('.ebdd_product_discounts').find('tbody').append( $(this).data( 'row' ) );
			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');
			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );
			return false;
		});

		$('#woocommerce-product-data').on('click','.ebdd_product_discounts a.delete-discount',function() {
			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');
			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );
			$(this).closest('tr').remove();
			return false;
		});

		$( '#bundled_product_data' ).on( 'change', '.priced_individually input', function() {

			var $this = $(this),
				$el             = $this.closest( '.wc-bundled-item' ),
				$content        = $el.find( 'div.item-data' ),
				$ebdd_discounts = $content.find( '.ebdd_apply_discounts' );

			if ( $this.is( ':checked' ) ) {
				$ebdd_discounts.show();
			} else {
				$ebdd_discounts.hide();
			}

		});

		$( '#bundled_product_data .priced_individually input' ).change();

		$( '#bundled_product_data' ).on( 'wc-bundled-products-changed', function() {
			$(this).find( '.priced_individually input' ).change();
		});

	});
})(jQuery);
