=== FG Magento to WooCommerce Premium ===
Contributors: Kerfred
Plugin Uri: https://www.fredericgilles.net/fg-magento-to-woocommerce/
Tags: magento, woocommerce, import, importer, convert magento to wordpress, migrate magento to wordpress, migration, migrator, converter, wpml, dropshipping
Requires at least: 4.5
Tested up to: 5.0
Stable tag: 2.61.0
Requires PHP: 5.3
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=fred%2egilles%40free%2efr&lc=FR&item_name=fg-magento-to-woocommerce&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted

A plugin to migrate categories, products, images, users, customers, orders and CMS from Magento to WooCommerce

== Description ==

This plugin migrates product categories, products, images, users, customers, orders, coupons and CMS from Magento to WooCommerce.

It has been tested with **Magento versions 1.3 to 2.2** and **Wordpress 5.0**. It is compatible with multisite installations.

Major features include:

* migrates the product categories
* migrates the product categories images
* migrates the products
* migrates the product thumbnails
* migrates the product images galleries
* migrates the product stocks
* migrates the product attributes
* migrates the product options
* migrates the product variations
* migrates the grouped products
* migrates the Up Sell, Cross Sell and related products
* migrates the downloadable products
* migrates the CMS
* migrates the users
* migrates the customers
* authenticate the users and the customers in WordPress with their Magento passwords
* migrates the orders
* migrates the ratings and reviews
* migrates the discount coupons
* migrates the SEO meta data
* SEO: redirects the Magento URLs
* multisites/multistores: Option to choose which website/store to import
* update the already imported products stocks and orders
* compatible with Magento Enterprise Edition
* ability to run the import automatically from the cron (for dropshipping for example)

No need to subscribe to an external web site.

= Add-ons =

The Premium version allows the use of add-ons that enhance functionality:

* Multilingual with WPML
* Move Magento order numbers
* Move Magento customer groups
* Move Magento manufacturers
* Move Magento product options as add-ons
* Move Magento costs
* Move Magento custom order statuses

== Installation ==

1.  Install the plugin in the Admin => Plugins menu => Add New => Upload => Select the zip file => Install Now
2.  Activate the plugin in the Admin => Plugins menu
3.  Run the importer in Tools > Import > Magento
4.  Configure the plugin settings. You can find the Magento database parameters in the Magento file app/etc/local.xml<br />
    Hostname = host<br />
    Port     = 3306 (standard MySQL port)<br />
    Database = dbname<br />
    Username = username<br />
    Password = password<br />

= Automatic import from cron =
In the crontab, add this row to run the import every day at 0:00:
0 0 * * * php /path/to/wp/wp-content/plugins/fg-magento-to-woocommerce-premium/cron_import.php >>/dev/null

== Translations ==
* English (default)
* French (fr_FR)
* other can be translated

== Changelog ==

= 2.61.0 =
* Tested with WordPress 5.0

= 2.60.2 =
* Fixed: Workaround to WooCommerce bug that doesn't process well the attributes containing "pa_"
 
= 2.60.1 =
* Fixed: Regression from 2.59.2: when used with WPML, the translations were not imported

= 2.60.0 =
* New: Add an option to not import the disabled products

= 2.59.4 =
* Fixed: Images not imported because of a missing starting slash

= 2.59.3 =
* Fixed: Some category images were not imported

= 2.59.2 =
* Fixed: Children of grouped products may be imported as duplicates

= 2.59.1 =
* Fixed: Notice: Undefined index: type_id

= 2.59.0 =
* New: Option to not import the customers who didn't make any order

= 2.58.0 =
* New: Generate the audio and video meta data (ID3 tag, featured image)
* Fixed: Notice: Trying to get property of non-object in woocommerce/includes/wc-attribute-functions.php on line 172
* Fixed: Set the price = 0 for the bundle products
* Fixed: Set manage_stock = no for the bundle products

= 2.57.5 =
* Fixed: Some product option values were not sorted correctly

= 2.57.4 =
* Fixed: Sort the product option values by title if they don't have a sort order value

= 2.57.3 =
* Fixed: Some Magento 2 product stocks were not updated

= 2.57.2 =
* Fixed: Some Magento 2 product stocks were not imported
* Fixed: Sort the product option values alphabetically if they don't have a sort order value
* Tweak: Cache some database results to increase import speed

= 2.57.1 =
* Fixed: Don't remove the WooCommerce pages associations when we delete only the imported data

= 2.57.0 =
* New: Support the Bengali alphabet
* Fixed: Wrong products pagination with out of stock products

= 2.56.1 =
* Fixed: Stock not imported and products not set as variable with some databases

= 2.56.0 =
* New: Import the downloadable product permissions
* New: Import the download limit with the downloaded products
* Fixed: Orders show pa_xxx when the attributes are skipped

= 2.55.2 =
* Fixed: Regression from 2.55.1: products not imported on Magento < 2

= 2.55.1 =
* Fixed: Products may be imported as duplicates

= 2.55.0 =
* New: Add a hook to be able to import the orders costs of goods

= 2.54.0 =
* New: Add the shipping tax amount in the tax column on the order screen
* Fixed: The shipping tax amount was counted twice in the order

= 2.53.0 =
* New: Allow Arabic characters

= 2.52.0 =
* New: Compatibility with the wholesale price feature of the Customer Groups add-on
* Fixed: Warning: Illegal string offset 'region'
* Fixed: Warning: Cannot assign an empty string to a string offset
* Tested with WordPress 4.9.8

= 2.51.0 =
* New: Import the Magento related products as up-sells

= 2.50.1 =
* Fixed: WordPress database error: [Cannot truncate a table referenced in a foreign key constraint (`wp_wc_download_log`, CONSTRAINT `fk_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permission)]

= 2.50.0 =
* Fixed: All the order comments were not imported
* Fixed: Empty the WooCommerce wc_download_log and woocommerce_downloadable_product_permissions tables upon database emptying
* Change: Wording of the label "Remove only previously imported data"
* Tested with WordPress 4.9.7

= 2.49.0 =
* New: Add an option to import the customers and orders from all stores or from the selected store only

= 2.48.3 =
* Fixed: The downloadable files with relative URLs were not downloaded

= 2.48.2 =
* Fixed: Order statuses containing uppercase characters were imported as Pending
* Tested with WordPress 4.9.6

= 2.48.1 =
* Fixed: The tax amount was not displayed in the order item rows
* Fixed: The order items corresponding to a missing downloadable file were not imported

= 2.48.0 =
* New: Import the datetime product attributes

= 2.47.0 =
* New: Import the downloads in the orders

= 2.46.2 =
* Fixed: Some variations were incomplete (due to the crc32() function that can return negative numbers)
* Fixed: Remove extra "-" in the SKU
* Tweak: Delete the wc_var_prices transient when emptying WordPress data
* Tested with WordPress 4.9.5

= 2.46.1 =
* Fixed: Fatal error: Uncaught Error: Cannot use object of type WP_Error as array

= 2.46.0 =
* New: Allow the import of the Magento 2 brands (with the Brands add-on)
* Fixed: Media path was wrong on some Magento 2 sites

= 2.45.4 =
* Fixed: Notice: Undefined index: short_description
* Fixed: Notice: unserialize(): Error at offset 0 of 348 bytes

= 2.45.3 =
* Fixed: Media not imported for some Magento 2 sites
* Fixed: [ERROR] Error:SQLSTATE[42S02]: Base table or view not found: 1146 Table 'sales_flat_order' doesn't exist

= 2.45.2 =
* Fixed: Import only the customers from the selected web site

= 2.45.1 =
* Fixed: All the customers were imported and not only the ones from the selected store

= 2.45.0 =
* New: Import the media shortcodes like {{media url="filename.jpg"}}

= 2.44.1 =
* Fixed: It was not possible to import the "Admin" store
* Tested with WordPress 4.9.4

= 2.44.0 =
* New: Ability to run the import automatically from the cron (for dropshipping for example)
* Tweak: Use WP_IMPORTING

= 2.43.0 =
* New: Import the long attribute values as custom attributes

= 2.42.0 =
* New: Check if we need the Custom Order Statuses add-on
* New: Add a hook for the order statuses mapping

= 2.41.0 =
* New: Set the "Manage stock" checkbox according to the Magento "manage stock" value
* New: Put the stock status as "in stock" when the product stock is not managed
* Tested with WordPress 4.9.1

= 2.40.0 =
* New: Add some hooks for the Cost of Goods add-on

= 2.39.0 =
* New: Allow the import of the "Admin" web site and of the "Admin" store

= 2.38.3 =
* Fixed: Wrong attribute values imported

= 2.38.2 =
* Fixed: The passwords containing a backslash were not recognized
* Fixed: The variations were imported as simple products (Magento < 1.4)
* Tested with WordPress 4.9

= 2.38.1 =
* Fixed: The attributes and variations were not imported when defined as super attributes on Magento 1.3 and less
* Tested with WordPress 4.8.3

= 2.38.0 =
* New: Make the products visibility compatible with WooCommerce 3

= 2.37.1 =
* Fixed: Categories imported in wrong language

= 2.37.0 =
* New: Check if we need the Product Options add-on
* New: Sanitize the media file names

= 2.36.0 =
* New: Add some functions for the Product Options module
* Tested with WordPress 4.8.2

= 2.35.1 =
* Fixed: Categories with duplicated names were not imported

= 2.35.0 =
* Fixed: Security cross-site scripting (XSS) vulnerability in the Ajax importer

= 2.34.0 =
* New: Compatible with Magento 2.x
* Fixed: "[ERROR] Error:SQLSTATE[42S22]: Column not found: 1054 Unknown column 'p.website_id' in 'where clause'" for Magento < 1.4
* Fixed: CMS articles may be imported as duplicates
* Improvement: Import speed optimization

= 2.33.0 =
* New: Import the prices from the Magento super attributes
* Tested with WordPress 4.8.1

= 2.32.0 =
* New: Allow HTML in term descriptions
* New: Import the product categories meta data (title, description, keywords) to Yoast SEO

= 2.31.0 =
* New: Import the image caption in the media attachment page

= 2.30.1 =
* Fixed: Disabled child products were imported as variations

= 2.30.0 =
* New: Authenticate the imported users by their email

= 2.29.0 =
* New: Block the import if the URL field is empty and if the media are not skipped
* New: Add error messages and information

= 2.28.1 =
* Fixed: Customers may be incompletely imported if the import hangs
* Fixed: Users with passwords encrypted with 64 characters were not authentified with their Magento password

= 2.28.0 =
* New: Add the percentage in the progress bar
* New: Display the progress and the log when returning to the import page
* Change: Restyling the progress bar
* Fixed: Typo - replace "complete" by "completed"
* Fixed: Notice: Undefined index: data
* Tested with WordPress 4.8

= 2.27.0 =
* New: Import the attributes by batch
* New: Import the downloadable files of type "URL"
* New: Can import multiple downloadable files per product
* New: Create one variation per downloadable file if the Magento links can be purchased separately

= 2.26.2 =
* Fixed: Infinite loop during the customers import if some records in the customer_entity table have got no matching record in the customer_entity_varchar table
* Tested with WordPress 4.7.5

= 2.26.1 =
* Fixed: Some attributes and variations were not imported due to trailing spaces in attribute values
* Fixed: Error:SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ORDER BY ov.store_id DESC
* Tested with WordPress 4.7.4

= 2.26.0 =
* New: Import the downloadable products

= 2.25.0 =
* New: Compatibility with WooCommerce 3.0: Grouped products are linked from the parent rather than the children. Children can be in more than one group.

= 2.24.1 =
* Fixed: Child products from grouped products are imported as duplicates
* Tweak: Code refactoring

= 2.24.0 =
* New: Import the coupons usages
* Fixed: Duplicated image in the product gallery

= 2.23.1 =
* Fixed: Some attributes and variations were not imported when using WPML

= 2.23.0 =
* New: Import the products visibility
* New: Import the child products of a grouped product even if they are not visible individually

= 2.22.1 =
* Fixed: The attributes options were not imported when their value is empty on the selected store

= 2.22.0 =
* New: Enable the attributes translations (with the WPML add-on)
* Fixed: Some prices of variable products shown as free
* Tweak: Clear WooCommerce transients when emptying WordPress content
* Tested with WordPress 4.7.3

= 2.21.5 =
* Fixed: Fatal error: Call to undefined method FG_Magento_to_WooCommerce_Orders::table_exists()

= 2.21.4 =
* Fixed: Fatal error: Call to undefined method FG_Magento_to_WooCommerce_Orders::table_exists()

= 2.21.3 =
* Fixed: Notice: Undefined index: name

= 2.21.2 =
* Fixed: The coupons had got trailing zeros

= 2.21.1 =
* Fixed: With Magento 1.4: [ERROR] Error:SQLSTATE[42S02]: Base table or view not found: 1146 Table 'sales_order_varchar' doesn't exist
* Fixed: With Magento 1.4: [ERROR] Error:SQLSTATE[42S02]: Base table or view not found: 1146 Table 'sales_order_entity' doesn't exist
* Fixed: With Magento 1.4: Notice: Undefined index: firstname
* Fixed: With Magento 1.4: Notice: Undefined index: lastname
* Fixed: With Magento 1.4: Orders shipping and billing addresses not imported

= 2.21.0 =
* New: Migrates the discount coupons

= 2.20.3 =
* Fixed: Term meta data not deleted when we delete the imported data only
* Fixed: Orders were not imported when importing all the languages with the WPML add-on

= 2.20.2 =
* Fixed: Users whose nicename is longer than 50 characters were not imported
* Fixed: Users whose login is longer than 60 characters were not imported
* Fixed: Avoid hangs due to orders with thousands of notes

= 2.20.1 =
* Fixed: Display 0 products and 0 orders the first time we click on the "Test the database connection" button

= 2.20.0 =
* New: Update the lists of websites and stores when clicking on "Test the database connection"
* New: Remove the button "Refresh the lists of websites and stores"

= 2.19.0 =
* New: Import the redirects from the url_path field
* New: Import the redirects from the Magento Enterprise Edition
* Tested with WordPress 4.7.2

= 2.18.1 =
* Change: Import the Manufacturer's Suggested Retail Price as the regular price instead of the sale price

= 2.18.0 =
* New: Add an option to import the Special Price or the Manufacturer's Suggested Retail Price

= 2.17.0 =
* New: Import the products Up Sell and Cross Sell
* New: Import the length, width and height as shipping attributes
* Fixed: The option names were imported instead of the option labels
* Tweak: Code refactoring

= 2.16.2 =
* Fixed: All the orders were imported and not only the ones from the selected store

= 2.16.1 =
* Fixed: Magento 1.4.0.x order notes not imported
* Tested with WordPress 4.7

= 2.16.0 =
* New: Multiwebsites: Add an option to choose which web site to import

= 2.15.2 =
* Fixed: Magento 1.4.0.x orders not imported

= 2.15.1 =
* Fixed: Existing images attached to imported products were removed when deleting the imported data

= 2.15.0 =
* New: Set the virtual and downloadable attributes
* Fixed: The child products which are visible individually were not imported

= 2.14.8 =
* Fixed: Wrong progress bar color

= 2.14.7 =
* Fixed: The progress bar didn't move during the first import
* Fixed: The log window was empty during the first import

= 2.14.6 =
* Fixed: The "IMPORT COMPLETE" message was still displayed when the import was run again

= 2.14.5 =
* Fixed: Database passwords containing "<" were not accepted
* Tweak: Code refactoring

= 2.14.4 =
* Fixed: Attributes with decimal values were not imported

= 2.14.3 =
* Fixed: Missing product attribute values if the WordPress site was already containing some categories

= 2.14.2 =
* Fixed: Wrong number of product categories displayed
* Fixed: Manufacturers not imported when the WPML add-on is active

= 2.14.1 =
* Fixed: Extra attribute value added in the product when both the Magento attribute and attribute option have the same value
* Fixed: Long attribute values were missing if another attribute values have the same first 29 characters
* Tweak: If the import is blocked, stop sending AJAX requests

= 2.14.0 =
* New: Authorize the connections to Web sites that use invalid SSL certificates
* Fixed: Some attributes containing commas were not imported

= 2.13.0 =
* New: Option to delete only the new imported data
* Fixed: WordPress database error when SKU contains quotes

= 2.12.3 =
* Fixed: MySQL 5.7 incompatibility: [ERROR] Error:SQLSTATE[HY000]: General error: 3065 Expression #1 of ORDER BY clause is not in SELECT list, references column 'gv.position' which is not in SELECT list; this is incompatible with DISTINCT
* Fixed: MySQL 5.7 incompatibility: [ERROR] Error:SQLSTATE[HY000]: General error: 3065 Expression #1 of ORDER BY clause is not in SELECT list, references column 'o.sort_order' which is not in SELECT list; this is incompatible with DISTINCT

= 2.12.2 =
* Fixed: Some images were duplicated in the product gallery

= 2.12.1 =
* Fixed: Product variations were not displayed on the front-end when the option "Hide out of stock items from the catalog" is selected and when the stock is not managed at the variation level
* Fixed: Review link broken

= 2.12.0 =
* New: Allow the manufacturers import
* New: Display the needed add-ons during the database testing and before importing
* Fixed: Wrong number of comments displayed
* Tested with WordPress 4.6.1

= 2.11.0 =
* New: Display the number of data found in the Magento database before importing
* Fixed: Variations with spaces were not imported

= 2.10.0 =
* New: Import the Magento ratings and reviews

= 2.9.1 =
* Fixed: Column not found: 1054 Unknown column 'otv.stock' in 'field list'
* Fixed: Only the first value of the product attributes was imported

= 2.9.0 =
* New: Import the product variations stocks
* Fixed: Some product variations were not imported on multistore sites
* Fixed: The product attributes containing "&" were imported without SKU and price
* Fixed: Regular prices were wrong for product variations when they have got a sale price
* Tested with WordPress 4.6

= 2.8.0 =
* New: Import the order items SKU
* New: Import the order comments
* Fixed: Remove notices which were displayed if some customer data were missing
* Fixed: The attributes and variations were not imported when the import was stopped and resumed

= 2.7.1 =
* Fixed: Some links could not be redirected correctly
* Fixed: Attributes and product attributes were not displayed
* Fixed: Notice: Undefined index: attribute_code
* Fixed: Notice: Undefined index: value

= 2.7.0 =
* New: Redirect the Magento URLs
* Tweak: Limit the number of variations to import
* Tweak: Optimize the product options import
* Fixed: Notice: Undefined index: password_hash

= 2.6.3 =
* Fixed: Variable products whose children have a null price, had got a null price
* Fixed: Notice: Undefined index: url_key

= 2.6.2 =
* Fixed: WordPress database error: [You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'xxx' yyy' LIMIT 1' at line 4

= 2.6.1 =
* Fixed: Wrong number of CMS articles was displayed

= 2.6.0 =
* New: Option to not import the products categories

= 2.5.2 =
* New: Add the hook fgm2wc_post_insert_order
* Change: Rename the Update button to "Update stocks and orders status"

= 2.5.1 =
* Fixed: Allow bad characters like "²" in the attribute names

= 2.5.0 =
* New: Import the SKU and the attributes in the order items

= 2.4.1 =
* Fixed: the store ID was not set during stock and orders update

= 2.4.0 =
* Fixed: CMS pages from all languages were imported
* Fixed: Notice: Undefined index: name
* Tweak: Refactor some functions to allow multilingual import by the WPML addon

= 2.3.0 =
* New: Add a button to update the already imported products stocks and orders
* New: Add the hook fgm2wc_pre_insert_product
* Fixed: PHP Notice: Object of class WP_Error could not be converted to int
* Fixed: Notice: Undefined index: url_key

= 2.2.0 =
* New: Import the product featured images
* New: Multistore: Add an option to choose which store to import
* Change: Remove the Paypal Donate button

= 2.1.0 =
* Fixed: Recount the terms after the products import
* Fixed: Display an error message when the process hangs
* Tweak: Increase the speed of counting the terms
* Tweak: Don't reimport the product attributes if they have already been imported
* Tweak: Don't reimport the product options if they have already been imported
* Tested with WordPress 4.5.3

= 2.0.0 =
* New: Run the import in AJAX
* New: Add a progress bar
* New: Add a logger frame to see the logs in real time
* New: Ability to stop the import
* New: Compatible with PHP 7
* New: Compatible with WooCommerce 2.6.0

= 1.13.4 =
* Fixed: The products without stock were not imported

= 1.13.3 =
* Fixed: Set the type of Magento multiselect attributes to "select"
* Fixed: Some attribute option IDs were imported as attribute values in addition to their text value

= 1.13.2 =
* Fixed: Products belonging to several bundles were imported as duplicates

= 1.13.1 =
* Fixed: Some descriptions were not imported correctly

= 1.13.0 =
* New: Import the boolean attributes
* Tested with WordPress 4.5.2

= 1.12.1 =
* Fixed: Attributes with value 0 were not imported
* Fixed: Compatibility issues with Magento 1.3
* Tested with WordPress 4.5.1

= 1.12.0 =
* New: Migrate the SEO meta data
* New: Add option to import the meta keywords as tags
* Tested with WordPress 4.5

= 1.11.0 =
* New: Import the free text attributes

= 1.10.2 =
* Fixed: Notice: Undefined index: short_description
* Fixed: Column 'post_excerpt' cannot be null

= 1.10.1 =
* Fixed: Products not imported. Error: "WordPress database error Column 'post_content' cannot be null"

= 1.10.0 =
* New: Import the grouped products
* New: Import the product options and their variations

= 1.9.1 =
* Tested with WordPress 4.4.2

= 1.9.0 =
* New: Add the min and max variation prices to the postmeta data for the variable products

= 1.8.4 =
* Tested with WordPress 4.4.1

= 1.8.3 =
* Fixed: Attributes not visible on front were not imported

= 1.8.2 =
* Fixed: Fatal error: Call to undefined function add_term_meta()

= 1.8.1 =
* Fixed: Better clean the taxonomies cache

= 1.8.0 =
* New: Add the option to not import the attributes
* Tweak: Optimize the termmeta table

= 1.7.0 =
* Tweak: Use the WordPress 4.4 term metas
* Fixed: WordPress database error: [Column 'order_item_name' cannot be null]

= 1.6.1 =
* Tested with WordPress 4.4

= 1.6.0 =
* New: Compatibility with Magento 1.3
* New: Support the accented Greek characters

= 1.5.0 =
* New: Add a link to the FAQ in the connection error message

= 1.4.0 =
* New: Add an Import link on the plugins list page
* Tweak: Code refactoring

= 1.3.1 =
* Fixed: Refresh the display of the product categories
* Fixed: Notice: Undefined property: FG_Magento_to_WooCommerce_Product_Attributes::$global_tax_rate
* Fixed: Notice: Undefined index: postcode
* Fixed: Error: 1054 Unknown column 'e.store_id' in 'where clause'

= 1.3.0 =
* New: Migrates all the attributes and not only the ones used in variations
* Fixed: Notice: Undefined index: url_key
* Tweak: Speed optimization for the variation import

= 1.2.1 =
* Fixed: Duplicate images
* Fixed: Avoid a double slash in the media filename
* Fixed: Error:SQLSTATE[42000]: Syntax error or access violation
* Fixed: Notice: Undefined index: region_id
* Fixed: Notice: Undefined index: region
* Fixed: Notice: Undefined index: company
* Fixed: Import the original category name instead of the translation

= 1.2.0 =
* New: Compatible with Magento 1.4 to 1.9
* New: Support the table prefix
* New: Import the product attributes
* New: Import the product variations
* Fixed: Don't import the child products as single products

= 1.1.0 =
* New: Migrates the users
* New: Migrates the users passwords
* New: Migrate the customers
* New: Authenticate the users and the customers in WordPress with their Magento passwords
* New: Migrate the orders

= 1.0.1 =
* Tested with WordPress 4.3.1

= 1.0.0 =
* Initial version: Import Magento product categories, products, images and CMS
