<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
*
* Gets all dates between start and end dates
*
* @param string $start
* @param string $end
* @param bool $past - Get passed dates
* @return array $unix_dates - Array of dates converted to Unix timestamp
*
**/
function ebdida_get_dates_from_range( $start, $end, $offset = false, $past = false ) {

	// Get Easy Booking settings
    $easy_booking_settings = get_option('easy_booking_settings');

    // Calculation mode (Days or Nights)
    $calc_mode = $easy_booking_settings['easy_booking_calc_mode'];

    // If calculation mode is set to "Nights", add one day to start day (as you book the night)
    if ( $offset && $calc_mode === 'nights' ) {
       $start = date( 'Y-m-d', strtotime( $start . ' +1 day' ) );
    }

    $dates = array( $start );

    // Get current date
    $current_date = date('Y-m-d');

    // Convert current date to timestamp
    $day = strtotime( $current_date );

    while ( end( $dates ) < $end ) {
        $dates[] = date( 'Y-m-d', strtotime( end( $dates ) . ' +1 day') );
    }

    $unix_dates = array();
    foreach ( $dates as $date ) {

    	// Get time
        $unix_date = strtotime( $date );

        // If past is true, add all dates
        if ( $past ) {
            $unix_dates[] = $date;
        } else if ( $unix_date >= $day ) { // Else, add only the one superior to the current day
            $unix_dates[] = $date;
        }
            
    }

    return $unix_dates;
}

/**
*
* Group an array of dates in dateranges with quantity
*
* @param array $dates
* @return array $dateranges - Array of dateranges
*
**/
function ebdida_group_dates_in_dateranges( $dates ) {

    // Get all dates
    $entries = array_values( $dates );

    // Init variables
    $i      = 0;
    $j      = 0;
    $k      = 0;
    $exists = true;
    $range  = false;

    $dateranges = array();
    while ( $exists ) {

        if ( array_key_exists( $i, $entries ) && ! $range ) {

            $index    = $entries[$i]; // Date
            $dateranges[$k][0] = ebdida_get_formatted_date( $index );

        }

        if ( ! array_key_exists( $i+1, $entries ) ) {
            $exists = false;
        }

        $next_index = intval( $i+$j );

        if ( ! array_key_exists( $next_index+1, $entries ) ) {

            $exists = false;
            $next_day = false;

        } else {

            // Get next day date
            $next_day = date( 'Y-m-d', strtotime( $entries[$next_index] . ' +1 day' ) );

        }

        // If next entry is next day
        if ( $exists && $entries[$next_index+1] === $next_day ) {

            $dateranges[$k][1] = ebdida_get_formatted_date( $entries[$next_index+1] );
            ksort( $dateranges[$k] );

            $j++;

            $range = true;

        } else {

            $tmp   = $i+1;
            $i     = $tmp+$j;
            $j     = 0;

            $k++;

            $range = false;

        }

    }

    return $dateranges;
}

/**
*
* Formats a date from "yyyy-mm-dd" oe "yyyy,mm,dd" to an array "0 => 'yyyy', 1 => 'mm', 2 => 'dd'"
*
* @param str $date
* @return array $formatted_date
*
**/
function ebdida_get_formatted_date( $date ) {

    if ( preg_match( '/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})$/', $date ) ) {
        $disabled_date  = explode( '-', $date );
    } else if ( preg_match( '/^([0-9]{4}\,[0-9]{2}\,[0-9]{2})$/', $date ) ) {
        $disabled_date  = explode( ',', $date );
    } else {
        return false;
    }
    
    $formatted_date = array();

    foreach ( $disabled_date as $date_item ) {

        $formatted_date = array(
            intval( $disabled_date[0] ),
            intval( $disabled_date[1]  - 1 ), // In Javascript months are zero-indexed
            intval( $disabled_date[2] )
        );

    }

    return $formatted_date;

}

/**
*
* Sort dates in ascending order
*
* @param str $a - First date
* @param str $b - Second date
* @return bool
*
**/
function ebdida_sort_dates( $a, $b ) {
    return strtotime( $a ) - strtotime( $b );
}

/**
*
* Formats and sanitizes disabled dates before saving
*
* @param array $disabled
* @return array $ebdida_disabled_dates
*
**/
function ebdida_sanitize_disabled_dates( array $disabled ) {

    $ebdida_disabled_dates = array();

    if ( ! isset( $disabled['ebdida_date_type'] ) ) {
        return $ebdida_disabled_dates;
    }

    // Count disabled dates
    $disabled_count = count( $disabled['ebdida_date_type'] );
    
    for ( $i = 0; $i < $disabled_count; $i++ ) {

        $type = isset( $disabled['ebdida_date_type'][$i] ) ? sanitize_text_field( $disabled['ebdida_date_type'][$i] ) : '';

        // If type is not set, return
        if ( empty( $type ) ) {
            continue;
        }

        $date_to_disable[$i] = '';

        switch ( $type ) {

            case 'ebdida_day' :

                $date_to_disable[$i]    = isset( $disabled['ebdida_disable_day'][$i] ) ? sanitize_text_field( $disabled['ebdida_disable_day'][$i] ) : '';
                $disable_every_year[$i] = 'off';

            break;
            case 'ebdida_date' :

                $date_to_disable[$i]    = isset( $disabled['ebdida_disable_date'][$i] ) ? sanitize_text_field( $disabled['ebdida_disable_date'][$i] ) : '';
                $disable_every_year[$i] = isset( $disabled['ebdida_repeat'][$i] ) ? sanitize_text_field( $disabled['ebdida_repeat'][$i] ) : 'off';

            break;
            case 'ebdida_date_range' :

                // Convert dates to times
                $start = isset( $disabled['ebdida_disable_daterange_start'][$i] ) ? strtotime( $disabled['ebdida_disable_daterange_start'][$i] ) : '';
                $end   = isset( $disabled['ebdida_disable_daterange_end'][$i] ) ? strtotime( $disabled['ebdida_disable_daterange_end'][$i] ) : '';

                $date_to_disable[$i] = array(
                    sanitize_text_field( $disabled['ebdida_disable_daterange_start'][$i] ),
                    sanitize_text_field( $disabled['ebdida_disable_daterange_end'][$i] )
                );
                
                // If one of the date is missing or start is superior to end, remove dates
                if ( empty( $start ) || empty( $end ) || $start > $end ) {
                    $date_to_disable[$i] = array();
                }

                $disable_every_year[$i] = isset( $disabled['ebdida_repeat'][$i] ) ? sanitize_text_field( $disabled['ebdida_repeat'][$i] ) : 'off';

            break;
            
        }

        // If no date(s) is (are) set, return
        if ( empty( $date_to_disable[$i] ) ) {
            continue;
        }

        $ebdida_disabled_dates[$i] = array(
            'type'     => $type,
            'disabled' => $date_to_disable[$i],
            'repeat'   => $disable_every_year[$i],
            'calendar' => isset( $disabled['ebdida_calendar'][$i] ) ? sanitize_text_field( $disabled['ebdida_calendar'][$i] ) : 'start_end'
        );

    }

    return $ebdida_disabled_dates;

}