<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Disabled_Admin_Assets' ) ) :

class Easy_Booking_Disabled_Admin_Assets {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'ebdida_admin_scripts' ), 50 );
    }

    public function ebdida_admin_scripts() {
        $screen = get_current_screen();

        wp_register_script(
            'ebdida-admin-functions',
            wceb_get_file_path( 'admin', 'ebdida-admin-functions', 'js', EBDIDA_PLUGIN_FILE ),
            array( 'jquery', 'pickadate' ),
            '1.0',
            true
        );

        wp_register_script(
            'ebdida-admin-product-functions',
            wceb_get_file_path( 'admin', 'ebdida-admin-product-functions', 'js', EBDIDA_PLUGIN_FILE ),
            array( 'jquery', 'pickadate' ),
            '1.0',
            true
        );

        wp_register_style(
            'ebdida-settings',
            wceb_get_file_path( 'admin', 'ebdida-settings', 'css', EBDIDA_PLUGIN_FILE ),
            array(),
            '1.0'
        );

        if ( in_array( $screen->id, array( 'product' ) ) ) {

            // JS
            if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
                wp_enqueue_script( 'picker' );
                wp_enqueue_script( 'legacy' );
                wp_enqueue_script( 'pickadate' );
            } else {
                wp_enqueue_script( 'pickadate' );
            }
            
            wp_enqueue_script( 'ebdida-admin-product-functions' );
            wp_enqueue_script( 'datepicker.language' );

            // CSS
            wp_enqueue_style( 'default-picker' );
            wp_enqueue_style( 'ebdida-settings' );

        }

	}

}

return new Easy_Booking_Disabled_Admin_Assets();

endif;