<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Easy_Booking_Disabled_Admin_Product' ) ) :

class Easy_Booking_Disabled_Admin_Product {

	public function __construct() {

		// Filter product types
        if ( WCEB()->allowed_types ) foreach ( WCEB()->allowed_types as $type ) {

            // If product is grouped, the disabled dates are set on the children (simple) products
            if ( $type !== 'grouped' ) {
				add_action( 'easy_booking_after_' . $type . '_booking_options', array( $this, 'ebdida_product_disabled_dates' ), 25, 1 );
				add_action( 'woocommerce_process_product_meta_' . $type, array( $this, 'ebdida_save_disabled_dates' ), 10, 1);

			}

		}

		// Variation disabled dates
		add_action( 'easy_booking_after_variation_booking_options', array( $this, 'ebdida_variation_disabled' ), 25, 2 );

		// Save variation disabled dates
		add_action( 'woocommerce_save_product_variation', array( $this, 'ebdida_save_variation_disabled_dates' ), 15, 2 );

		// Add product bundles options
        add_action( 'woocommerce_bundled_product_admin_config_html', array( $this, 'ebdida_bundled_product_options' ), 12, 4 );

	}

	/**
	 *
	 * Single, variable and other product type disabled dates form
	 *
	 */
	public function ebdida_product_disabled_dates( $product ) {

		$product_id = is_callable( array( $product, 'get_id' ) ) ? $product->get_id() : $product->id;
		$disabled_dates = get_post_meta( $product_id, '_ebdida_disabled_dates', true );

		echo '<div class="options_group">';
			include( 'views/html-ebdida-admin-product.php' );
		echo '</div>';
	}

	/**
	 *
	 * Variation disabled dates form
	 *
	 */
	public function ebdida_variation_disabled( $loop, $variation ) {

		$variation_id = is_callable( array( $variation, 'get_id' ) ) ? $variation->get_id() : $variation->ID;
		$disabled_dates = get_post_meta( $variation_id, '_ebdida_disabled_dates', true );
		
		echo '<div class="show_if_variation_bookable">';
			include( 'views/html-ebdida-admin-product.php' );
		echo '</div>';
	}

	public function ebdida_bundled_product_options( $loop, $product_id, $item_data, $post_id ) {

        $product = wc_get_product( $product_id );
        $disable_dates = get_post_meta( $product_id, '_ebdida_bundle_disable_dates', true );

        if ( ! wceb_is_bookable( $product ) ) {
            return;
        }

        ?>

        <div class="ebdida_disable_dates">

            <div class="form-field">
                <label for="_ebdida_bundle_disable_dates[<?php echo $loop; ?>]">
                    <?php _e( 'Disable dates from Easy Booking: Disable Dates?', 'easy_booking_disable_dates' ); ?>
                </label>
                <input type="checkbox" name="_ebdida_bundle_disable_dates[<?php echo $loop; ?>]" id="_ebdida_bundle_disable_dates[<?php echo $loop; ?>]" class="checkbox" <?php echo ( 'yes' === $disable_dates ? 'value="1"' : '' ) ?> <?php checked( $disable_dates, 'yes' ); ?>>
                <?php echo wc_help_tip( __( 'Check to disable the product\'s disabled dates.', 'easy_booking_disable_dates' ) ); ?>
            </div>

        </div>

        <?php

    }

	/**
	 *
	 * Save single, variable and other product type disabled dates
	 *
	 */
	public function ebdida_save_disabled_dates( $post_id ) {

		// Get disabled dates
		$disabled = isset( $_POST['ebdida_settings'] ) ? $_POST['ebdida_settings'] : array();

		// Format and sanitize values
		$ebdida_disabled_dates = ebdida_sanitize_disabled_dates( $disabled );

		// Update post meta
		update_post_meta( $post_id, '_ebdida_disabled_dates', $ebdida_disabled_dates );

		if ( isset( $_POST['bundle_data'] ) ) {

            foreach ( $_POST['bundle_data'] as $index => $value ) {
                $id = absint( $_POST['bundle_data'][$index]['product_id'] );

                if ( isset( $_POST['_ebdida_bundle_disable_dates'][$index] ) ) {
                    update_post_meta( $id, '_ebdida_bundle_disable_dates', 'yes' );
                } else {
                    update_post_meta( $id, '_ebdida_bundle_disable_dates', 'no' );
                }
            }
            
        }

	}

	/**
	 *
	 * Save variation disabled dates
	 *
	 */
	public function ebdida_save_variation_disabled_dates( $variation_id , $v ) {

		// Get disabled dates
		$settings = isset( $_POST['ebdida_settings'] ) ? $_POST['ebdida_settings'] : array();

		$disabled = array();

		$disabled_options = array(
			'var_ebdida_date_type',
			'var_ebdida_disable_day',
			'var_ebdida_disable_date',
			'var_ebdida_disable_daterange_start',
			'var_ebdida_disable_daterange_end',
			'var_ebdida_repeat',
			'var_ebdida_calendar'
		);

		// Get disabled dates for the variation
		foreach ( $disabled_options as $option ) {
			$name = str_replace( 'var_', '', $option );
			$disabled[$name] = isset( $settings[$option][$v] ) ? $settings[$option][$v] : '';
		}

		// Format and sanitize values
		$ebdida_disabled_dates = ebdida_sanitize_disabled_dates( $disabled );
		
		// Update post meta
		update_post_meta( $variation_id, '_ebdida_disabled_dates', $ebdida_disabled_dates );

	}

}

return new Easy_Booking_Disabled_Admin_Product();

endif;