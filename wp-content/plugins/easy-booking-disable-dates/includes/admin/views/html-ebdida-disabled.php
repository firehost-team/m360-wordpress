<tr class="form-row disabled-row">

	<td class="date_type">

		<?php $selected_type = $disabled_date['type']; ?>

		<select class="ebdida_select_type" name="ebdida_settings[ebdida_date_type][]">
			<option value="ebdida_day" <?php selected( $selected_type, 'ebdida_day', true ) ?>>
				<?php _e('Day', 'easy_booking_disable_dates'); ?>
			</option>
			<option value="ebdida_date" <?php selected( $selected_type, 'ebdida_date', true ) ?>>
				<?php _e('Date', 'easy_booking_disable_dates'); ?>
			</option>
			<option value="ebdida_date_range" <?php selected( $selected_type, 'ebdida_date_range', true ) ?>>
				<?php _e('Date Range', 'easy_booking_disable_dates'); ?>
			</option>
		</select>

	</td>

	<td class="date_to_disable">

	<?php $selected = $disabled_date['disabled']; 

		if ( $selected_type === 'ebdida_day' ) {

			$selected_day = $selected;
			$selected_date = '';
			$selected_daterange[0] = '';
			$selected_daterange[1] = '';
			
		} else if ( $selected_type === 'ebdida_date' ) {

			$selected_date = $selected;
			$selected_day = '1';
			$selected_daterange[0] = '';
			$selected_daterange[1] = '';

		} else if ( $selected_type === 'ebdida_date_range' ) {

			$selected_daterange[0] = $selected[0];
			$selected_daterange[1] = $selected[1];
			$selected_day = '1';
			$selected_date = '';

		}

	?>

		<span data-type="ebdida_day" style="display:<?php echo $selected_type === 'ebdida_day' ? 'block' : 'none'; ?>;">
			<select name="ebdida_settings[ebdida_disable_day][]">
				<option value="1" <?php selected( $selected_day, '1', true ) ?>><?php _e('Monday', 'easy_booking_disable_dates');?></option>
				<option value="2" <?php selected( $selected_day, '2', true ) ?>><?php _e('Tuesday', 'easy_booking_disable_dates');?></option>
				<option value="3" <?php selected( $selected_day, '3', true ) ?>><?php _e('Wednesday', 'easy_booking_disable_dates');?></option>
				<option value="4" <?php selected( $selected_day, '4', true ) ?>><?php _e('Thursday', 'easy_booking_disable_dates');?></option>
				<option value="5" <?php selected( $selected_day, '5', true ) ?>><?php _e('Friday', 'easy_booking_disable_dates');?></option>
				<option value="6" <?php selected( $selected_day, '6', true ) ?>><?php _e('Saturday', 'easy_booking_disable_dates');?></option>
				<option value="7" <?php selected( $selected_day, '7', true ) ?>><?php _e('Sunday', 'easy_booking_disable_dates');?></option>
			</select>
		</span>

		<span data-type="ebdida_date" style="display:<?php echo $selected_type === 'ebdida_date' ? 'block' : 'none'; ?>;">
			<input type="text" name="ebdida_settings[ebdida_disable_date][]" class="ebdida_datepicker ebdida_disable_date_picker" data-value="<?php echo $selected_type === 'ebdida_date' ? esc_attr( $selected_date ) : ''; ?>" value="">	
		</span>

		<span data-type="ebdida_date_range" style="display:<?php echo $selected_type === 'ebdida_date_range' ? 'block' : 'none'; ?>;">
			<input type="text" name="ebdida_settings[ebdida_disable_daterange_start][]" class="ebdida_datepicker ebdida_disable_daterange_start" data-value="<?php echo $selected_type === 'ebdida_date_range' ? esc_attr( $selected_daterange[0] ) : ''; ?>" value="">
			<input type="text" name="ebdida_settings[ebdida_disable_daterange_end][]" class="ebdida_datepicker ebdida_disable_daterange_end" data-value="<?php echo $selected_type === 'ebdida_date_range' ? esc_attr( $selected_daterange[1] ) : ''; ?>" value="">
		</span>

	</td>

	<td>

		<?php $repeat = $disabled_date['repeat']; ?>

		<span class="repeat_row" style="display:<?php echo $selected_type === 'ebdida_day' ? 'none' : 'block'; ?>;">

			<label for="date_every_year[<?php echo $i; ?>]"><?php _e('Every year?', 'easy_booking_disable_dates'); ?></label>
			<input type="checkbox" id="date_every_year[<?php echo $i; ?>]" name="ebdida_settings[ebdida_repeat][]" class="repeat" <?php checked( $disabled_date['repeat'], 'on' ) ?>>
			<input type="hidden" name="ebdida_settings[ebdida_repeat][]" value="off" <?php echo $repeat === 'on' ? 'disabled' : ''; ?>>
		</span>

	</td>

	<td class="show_if_two_dates">

		<?php $calendar = $disabled_date['calendar']; ?>
		
		<select name="ebdida_settings[ebdida_calendar][]">
			<option value="start_end" <?php selected( $calendar, 'start_end', true ) ?>>
				<?php _e('Start and end', 'easy_booking_disable_dates');?>
			</option>
			<option value="start" <?php selected( $calendar, 'start', true ) ?>>
				<?php _e('Start only', 'easy_booking_disable_dates');?>
				</option>
			<option value="end" <?php selected( $calendar, 'end', true ) ?>>
				<?php _e('End only', 'easy_booking_disable_dates');?>
			</option>
		</select>

	</td>

	<td width="1%" class="delete">

		<a href="#" class="delete-disabled-date">
			<?php _e( 'Delete', 'woocommerce' ); ?>
		</a>

	</td>

</tr>