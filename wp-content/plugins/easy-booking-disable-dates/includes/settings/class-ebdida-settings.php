<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Ebdida_Settings' ) ) :

class Ebdida_Settings {

	private $settings;
	private $global_settings;

	public function __construct() {

		// get plugin options values
		$this->settings = get_option('ebdida_settings');
		
		// initialize options the first time
		if ( ! $this->settings ) {
		
		    $this->settings = array(
				'ebdida_license_key'            => '',
				'ebdida_disabled_dates'         => array(),
				'ebdida_allow_to_book_disabled' => ''
		    );

		    add_option( 'ebdida_settings', $this->settings );

		}

		if ( is_multisite() ) {
			
			$this->global_settings = get_option('easy_booking_global_settings');

			if ( ! isset( $this->global_settings['ebdida_license_key'] ) ) {
				$this->global_settings['ebdida_license_key'] = '';
			}

			update_option( 'easy_booking_global_settings', $this->global_settings );

		}

		// Add settings page
		add_action( 'admin_menu', array( $this, 'ebdida_add_setting_page' ), 20 );

		// Init plugin settings
		add_action( 'admin_init', array( $this, 'ebdida_settings_init' ) );

		$license_set = ! isset( $this->settings['ebdida_license_key'] ) || empty( $this->settings['ebdida_license_key'] ) ? false : true;

		if ( get_option( 'easy_booking_display_notice_ebdida_license' ) !== '1' && ! $license_set ) {
			update_option( 'easy_booking_display_notice_ebdida_license', 0 );
		} else {
			update_option( 'easy_booking_display_notice_ebdida_license', '1' );
		}

	}

	/**
	 *
	 * Plugin settings page
	 *
	 */
	public function ebdida_add_setting_page() {
		$option_page = add_submenu_page(
			'easy-booking',
			'Disable Dates',
			'Disable Dates',
			apply_filters( 'easy_booking_settings_capability', 'manage_options' ),
			'easy-booking-disable-dates',
			array( $this, 'ebdida_option_page' )
		);

		// Load scripts on this page only
		add_action( 'admin_print_scripts-'. $option_page, array( $this, 'ebdida_load_scripts' ) );
	}

	/**
	 *
	 * Load scripts and styles
	 *
	 */
	public function ebdida_load_scripts() {

		// JS

		wp_enqueue_script( 'pickadate' );

		wp_enqueue_script( 'ebdida-admin-functions' );

		wp_enqueue_script( 'datepicker.language' );

		// CSS

		wp_enqueue_style( 'picker' );
		wp_enqueue_style( 'ebdida-settings' );

	}

	/**
	 *
	 * Init settings
	 *
	 */
	public function ebdida_settings_init() {

		include_once( 'includes/ebdida-settings.php' );

		// Multisite settings
		if ( is_multisite() ) {
			include_once( 'includes/ebdida-network-settings.php' );
		}

	}

	/**
	 *
	 * Display settings page content
	 *
	 */
	public function ebdida_option_page() {
		include_once( 'views/html-ebdida-settings.php' );
	}

	/**
	 *
	 * Main settings section
	 *
	 */
	public function ebdida_section_general() {
		echo '';
	}

	/**
	 *
	 * License key field
	 *
	 */
	public function ebdida_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'ebdida_license_key',
			'name'              => 'ebdida_settings[ebdida_license_key]',
			'value'             => $this->settings['ebdida_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_disable_dates' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	/**
	 *
	 * Global disabled dates
	 *
	 */
	public function ebdida_disable_dates_form() {
		$disabled_dates = $this->settings['ebdida_disabled_dates'];
		include_once( 'views/html-ebdida-table.php' );
	}

	/**
	 *
	 * Allow disabled dates inside booking period field
	 *
	 */
	public function ebdida_allow_to_book_disabled() {
		wceb_settings_checkbox( array(
			'id'          => 'ebdida_allow_to_book_disabled',
			'name'        => 'ebdida_settings[ebdida_allow_to_book_disabled]',
			'description' => __( 'Disable dates for start and end only and allow disabled dates inside booking period. Only for "Disable Dates" addon, it doesn\'t include unavailable dates ("Availability Check" addon).', 'easy_booking_disable_dates' ),
			'value'       => isset( $this->settings['ebdida_allow_to_book_disabled'] ) ? 'on' : '',
			'cbvalue'     => 'on'
		));
	}

	/**
	 *
	 * Multisite settings section
	 *
	 */
	public function ebdida_multisite_settings() {
		do_settings_sections('ebdida_multisite_settings');
	}

	/**
	 *
	 * Multisite license key field
	 *
	 */
	public function ebdida_multisite_license_key() {
		wceb_settings_input( array(
			'type'              => 'text',
			'id'                => 'ebdida_license_key',
			'name'              => 'easy_booking_global_settings[ebdida_license_key]',
			'value'             => $this->global_settings['ebdida_license_key'],
			'description'       => __( 'Enter your license key', 'easy_booking_availability' ),
			'custom_attributes' => array(
				'size' => '40'
			)
		));
	}

	/**
	 *
	 * Sanitize options
	 *
	 */
	public function sanitize_values( $settings ) {
		
		// Count disabled dates
		$disabled_count = isset( $settings['ebdida_date_type'] ) ? count( $settings['ebdida_date_type'] ) : 0;

		$ebdida_disabled_dates = array();
		for ( $i = 0; $i < $disabled_count; $i++ ) {

			// If type is not set, return
			if ( empty( $settings['ebdida_date_type'][$i] ) ) {
				continue;
			}

			switch ( $settings['ebdida_date_type'][$i] ) {
				case 'ebdida_day' :

					$date_to_disable[$i]    = sanitize_text_field( $settings['ebdida_disable_day'][$i] );
					$disable_every_year[$i] = 'off';

				break;
				case 'ebdida_date' :

					$date_to_disable[$i]    = sanitize_text_field( $settings['ebdida_disable_date'][$i] );
					$disable_every_year[$i] = sanitize_text_field( $settings['ebdida_repeat'][$i] );

				break;
				case 'ebdida_date_range' :

					// Convert dates to times
					$start = strtotime( $settings['ebdida_disable_daterange_start'][$i] );
					$end   = strtotime( $settings['ebdida_disable_daterange_end'][$i] );

					$date_to_disable[$i] = array(
						sanitize_text_field( $settings['ebdida_disable_daterange_start'][$i] ),
						sanitize_text_field( $settings['ebdida_disable_daterange_end'][$i] )
					);
					
					// If one of the date is missing or start is superior to end, remove dates
					if ( empty( $start ) || empty( $end ) || $start > $end ) {
						$date_to_disable[$i] = array();
					}

					$disable_every_year[$i] = sanitize_text_field( $settings['ebdida_repeat'][$i] );

				break;
			}

			// If no date(s) is (are) set, return
			if ( empty( $date_to_disable[$i] ) ) {
				continue;
			}

			$ebdida_disabled_dates[$i] = array(
				'type'     => sanitize_text_field( $settings['ebdida_date_type'][$i] ),
				'disabled' => $date_to_disable[$i],
				'repeat'   => $disable_every_year[$i],
				'calendar' => sanitize_text_field( $settings['ebdida_calendar'][$i] )
			);

		}

		$settings['ebdida_disabled_dates'] = (array) $ebdida_disabled_dates;

		// Sanitize other settings
		foreach ( $settings as $key => $value ) {

			if ( $key !== 'ebdida_disabled_dates' ) {
				$settings[$key] = is_array( $value ) ? array_map( 'esc_html', $value ) : esc_html( $value );
			}

		}

		return $settings;
	}

}

return new Ebdida_Settings();

endif;