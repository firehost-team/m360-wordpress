<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div class="wrap">

	<h2><?php _e( 'Easy Booking : Disable Dates Settings', 'easy_booking_disable_dates' ); ?></h2>

	<form method="post" id="ebdida-settings" action="options.php">

		<?php settings_fields( 'ebdida_settings' ); ?>
		<?php do_settings_sections( 'ebdida_settings' ); ?>
		 
		<?php submit_button(); ?>

	</form>

</div>