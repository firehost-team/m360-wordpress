(function($) {
	$(document).ready(function() {

		$.extend($.fn.pickadate.defaults,{
			formatSubmit: 'yyyy-mm-dd',
			hiddenName: true
		});

		$('.product_data').on('click', '.ebdida-disabled a.add-disabled-date',function() {

			$(this).closest('.ebdida-disabled').find('.ebdida-table').append( $(this).data( 'row' ) );

			var index = $('.disabled-row').length,
				addedRow = $('.disabled-row').last();

			addedRow.find('.ebdida_select_type').change();

			var span = addedRow.find('.repeat_row'),
				attr = 'date_every_year[' + index + ']';

			span.find('label').attr('for', attr);
			span.find('.repeat').attr('id', attr);

			$('#booking_product_data').find('.booking_dates').change();
			$(this).parents('.booking_variation_data').find('.booking_dates').change();

			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');

			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );

			return false;

		});

		function initPickers( datepicker, start, end ) {

			if ( typeof datepicker === 'undefined' ) {
				var datepicker = $('.ebdida_datepicker');
			}

			var $input = datepicker.pickadate(),
				picker = $input.pickadate('picker');

			if ( typeof start === 'undefined' ) {
				start = $('.ebdida_disable_daterange_start');
			}

			if ( typeof end === 'undefined' ) {
				end = $('.ebdida_disable_daterange_end');
			}

			start.each( function() {
				initPickersOnLoad( $(this), 'min' );
			});
			
			end.each( function() {
				initPickersOnLoad( $(this), 'max' );
			});
			
		}

		function initPickersOnLoad( input, thingToSet ) {

			var $daterangeInput = input.pickadate(),
				daterangePicker = $daterangeInput.pickadate('picker');

			setMinOrMax( daterangePicker, thingToSet );

			daterangePicker.on('set', function() {
				setMinOrMax( daterangePicker, thingToSet );
			});

			return false;

		}

		function setMinOrMax( picker, thingToSet ) {
			var select = picker.get('select');

			if ( ! select ) {
			 	select = Infinity;
			}

			var $sibling = picker.$node.siblings('.ebdida_datepicker').pickadate();
			
			var sliblingPicker = $sibling.pickadate('picker'),
				sliblingItem = sliblingPicker.component.item;

			if ( thingToSet === 'min' ) {
				sliblingItem.min = select;
			} else if ( thingToSet === 'max') {
				sliblingItem.max = select;
			}
			
			sliblingPicker.render();

		}

		initPickers();

		$( '#woocommerce-product-data' ).on( 'woocommerce_variations_loaded', function() {
			initPickers();
		});

		$('.product_data').on('change', '.ebdida-disabled .ebdida_select_type', function() {

			var selectedType = $(this).val(),
				parentRow = $(this).parents('.form-row'),
				selectLine = parentRow.find('.date_to_disable'),
				repeatLine = parentRow.find('.repeat_row');

			if ( ! selectedType ) {
				return;
			}

			selectLine.find('span').hide();

			var line = selectLine.find('span[data-type="' + selectedType + '"]'),
				datepicker = line.find('.ebdida_datepicker');

			if ( selectedType === 'ebdida_day' ) {
				repeatLine.hide();
			} else {
				repeatLine.show();
			}

			line.show();

			if ( selectedType === 'ebdida_date_range' ) {
				var startPicker = line.find('.ebdida_disable_daterange_start'),
					endPicker = line.find('.ebdida_disable_daterange_end');

				initPickers( datepicker, startPicker, endPicker );
			} else {
				initPickers( datepicker );
			}

		});

		$('.product_data').on('change', '.ebdida-disabled .repeat', function() {

			if ( $(this).is(':checked') ) {
				$(this).next('input[type="hidden"]').prop('disabled', true);
			} else {
				$(this).next('input[type="hidden"]').prop('disabled', false);
			}

		});

		$('.product_data').on('click','.ebdida-disabled a.delete-disabled-date', function() {

			$(this).parents('.woocommerce_variation').addClass('variation-needs-update');
			$( 'button.cancel-variation-changes, button.save-variation-changes' ).removeAttr( 'disabled' );
			$(this).closest('tr').remove();
			return false;

		});

	});
})(jQuery);
