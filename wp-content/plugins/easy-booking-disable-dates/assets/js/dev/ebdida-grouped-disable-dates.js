(function($) {
	$(document).ready(function() {

		var $inputStart = $('.datepicker_start').pickadate(),
			$inputEnd = $('.datepicker_end').pickadate();

		// Picker objects.
		var pickerStart = $inputStart.pickadate('picker'),
			pickerEnd   = $inputEnd.pickadate('picker');

		var pickerStartItem = pickerStart.component.item,
			pickerEndItem   = pickerEnd.component.item;

		var disabled = ebdida_data.disabled;

		var calcMode = ebdida_data.calc_mode,
			datesToDisable = {};

		function getChildrenDisabledDate( Id ) {

			if ( ! disabled[Id] ) {
				return {};
			}

			datesToDisableStart = [];
			datesToDisableEnd   = [];

			var disabledDays  = disabled[Id].days,
				disabledDates = disabled[Id].dates;

			var disableDaysStart = disabledDays.start,
				disableDaysEnd   = disabledDays.end;

			var disableDatesStart = disabledDates.start,
				disableDatesEnd   = disabledDates.end;

			if ( disableDaysStart && typeof disableDaysStart !== 'undefined' ) {
				$.each( disableDaysStart, function( index, day ) {
					var date = formatDate( 'day', 'start', day );
						datesToDisableStart.push( date );
				});
			}

			if ( disableDaysEnd && typeof disableDaysEnd !== 'undefined' ) {
				$.each( disableDaysEnd, function( index, day ) {
					var date = formatDate( 'day', 'end', day );
						datesToDisableEnd.push( date );
				});
			}

			if ( disableDatesStart && typeof disableDatesStart !== 'undefined' ) {
				$.each( disableDatesStart, function( index, date ) {
					var date = formatDate( 'date', 'start', date );
						datesToDisableStart.push( date );
				});
			}

			if ( disableDatesEnd && typeof disableDatesEnd !== 'undefined' ) {
				$.each( disableDatesEnd, function( index, date ) {
					var date = formatDate( 'date', 'end', date );
						datesToDisableEnd.push( date );
				});
			}
			
			datesToDisable = {
				start: datesToDisableStart,
				end  : datesToDisableEnd
			};

			return datesToDisable;
		}

		function formatDate( format, calendar, date ) {
			
			if ( format === 'day' ) {

				var disable = parseInt( date );

				if ( disable === 8 ) {
					var disable = 1;
				}

			} else if ( format === 'date' ) {

				start = date[0];

				// If is a daterange
				if ( typeof date[1] !== 'undefined' ) {
					end = date[1];
				}

				// If is a daterange
				if ( typeof date[1] !== 'undefined' ) {

					disable = {
						from: start,
						to  : end,
						type: 'disabled'
					}

				} else {

					disable = start;
					disable['type'] = 'disabled';
					
				}
						
			}

			return disable;
		}

		$('body').on('pickers_init', function(e, ids) {

			var datesToDisable    = [],
				endDatesToDisable = [];

			$.each( ids, function( id, qty ) {

				if ( qty > 0 ) {

					var childrenDisabled = getChildrenDisabledDate( id );

					if ( typeof childrenDisabled.start !== 'undefined' ) {

						$.each( childrenDisabled.start, function( index, date ) {
							datesToDisable.push( date );
						});

					}

					if ( typeof childrenDisabled.end !== 'undefined' ) {

						$.each( childrenDisabled.end, function( index, date ) {
							endDatesToDisable.push( date );
						});

					}
						
				}
				
			});

			// Get already disabled (unavailable) dates (Availability Check)
			alreadyDisabled    = pickerStartItem.disable;
			alreadyDisabledEnd = pickerEndItem.disable;

			// Merge unavailable dates and disabled dates
			toDisable    = alreadyDisabled.concat( datesToDisable );
			toDisableEnd = alreadyDisabledEnd.concat( endDatesToDisable );

			pickerStartItem.disable = toDisable;
			pickerEndItem.disable   = toDisableEnd;

			return false;
		});

		$('body').on('clear_start_picker', function(e, pickerEndItem) {
			pickerEndItem.disable = toDisableEnd;
			return false;
		});

		$('body').on('clear_end_picker', function(e, pickerStartItem) {
			pickerStartItem.disable = toDisable;
			return false;
		});

	});
})(jQuery);
