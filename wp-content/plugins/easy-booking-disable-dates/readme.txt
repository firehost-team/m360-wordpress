==== Easy Booking Disable Dates ====
Contributors: @_Ashanna
Tags: woocommerce, booking, renting, products, book, rent, e-commerce
Requires at least: 4.2, WooCommerce 2.5, WooCommerce Easy Booking 2.1.7
Tested up to: 4.9.4, WooCommerce 3.3.1
Stable tag: 1.5.5
License: GPLv2 or later

Easy Booking : Disable Dates is an add-on for WooCommerce Easy Booking to disable days or dates on the booking schedules.

== Description ==

This add-on allows to disable days or dates when renting products with WooCommerce Easy Booking.

You will be able to disable :

- Weekdays
- Specific dates (for example: public holidays)
- Dateranges

Specific dates and dateranges can repeat every year (until it reaches the limit year).

Disabled dates can be disabled on the start calendar, the end calendar or both calendars.
You can also choose to disable them for start and / or end only, allowing disabled dates inside the booking period.
Note that if disabled dates are inside the booking period, they are still charged.

== Installation ==

Make sure you have installed WooCommerce and WooCommerce Easy Booking.

1. Install the “Easy Booking : Disable Dates” Plugin.
2. Activate the plugin.
3. Enter your license key. For multisites, install the plugin and enter the license key on the network. Then, activate it on each sites.
4. In your administration panel, go to the Easy Booking menu, and the Disable Dates sub-menu to set the plugin settings and the disabled dates for every product.
5. In your administration panel, go to the bookable product page and set the days or dates to disable for this specific product or variation in the "Bookings" tab (and below the price inputs on the "Variations" tab for variable products). Save the product.
6. And that’s it !

== Changelog ==

= 1.5.5 =

* Add - Added an option to choose to disable dates or not in bundled items.
* add - Compatibility with "Last available date".

= 1.5.4 =

* Fix - Bundled products and variable products: fixed an issue where disabled dates were not applied when saved at the parent product level.
* Fix - Fixed issues when saving and displaying global disabled dates.

= 1.5.3 =

* Fix - Compatibility with WooCommerce 3.0 and WooCommerce Product Bundles 5.2.0.
* Fix - Array to string conversion error when saving a disabled dates.

= 1.5.2 =

* Fix - Compatibility with WooCommerce 3.0.
* Fix - [Admin] Update notification.

= 1.5.1 =

* Fix - Compatibility with WooCommerce Product Bundles > 5.0.
* Updated ebdida.pot file.
* Removed included language files, please visit http://herownsweetcode.com/easy-booking/documentation/disable-dates/localization/ to download available translation files.

= 1.5.0 =

* Add - [Feature] Compatibility with one date only.
* Add - [Feature] Compatibility with WooCommerce Product Bundles.

= 1.4 =

* Add     - [Admin] Parent product disabled dates for variable products.
* Removed - [Frontend] End calendar dates no longer have a 1 day offset in nights mode.
* Tweak   - [Admin] Store dates in dateranges if possible, instead of individually, before loading them on the frontend to avoid too much data.
* Tweak   - Reviewed, updated and improved code to optimize the plugin.

= 1.3.1 =

* Fix - [Admin] "Allow customers to book disabled dates?" option is now saved correctly.

= 1.3 =

* Add - [Feature] Option to allow disabled dates inside booking periods.
* Fix - [Frontend] Disabled dates when the first weekday is Sunday.
* Fix - [Admin] [Variable products] Error when no date is disabled when saving variations.
* Tweak - [Admin] Improved notices.

= 1.2 =

* Add - Option to disable dates on Start calendar, End calendar, or both.
* Fix - Settings sanitizing.

= 1.1.1 =

* Add - Multisite compatibility.
* Fix - Date format not supported by Safari.
* Delete database entries for each sites on multisites when uninstalling the plugin.

= 1.1 =

* Fix - Hook and priority to enqueue frontend scripts (compatibility with Easy Booking : Availability Check).
* Removed - 'ebdida_dependencies' filter.

= 1.0 =
* Initial Release

== Upgrade Notice ==

= 1.5.0 =

This version requires WooCommerce Easy Booking 2.0.

= 1.4 =

This version requires WooCommerce Easy Booking 1.9, please update it before or it won't work.