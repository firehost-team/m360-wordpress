<?php
namespace BooklyPro\Backend\Modules\Notifications;

use Bookly\Lib as BooklyLib;
use Bookly\Backend\Modules\Notifications\Forms;
use BooklyPro\Backend\Components\Notifications\Custom;

/**
 * Class Ajax
 * @package BooklyPro\Backend\Modules\Notifications
 */
class Ajax extends BooklyLib\Base\Ajax
{
    /**
     * @inheritdoc
     */
    protected static function permissions()
    {
        return array( '_default' => 'user' );
    }

    /**
     * Create new custom notification
     */
    public static function createCustomNotification()
    {
        $notification = new BooklyLib\Entities\Notification();
        $notification
            ->setType( BooklyLib\Entities\Notification::TYPE_APPOINTMENT_START_TIME )
            ->setToCustomer( 1 )
            ->setToStaff( 1 )
            ->setSettings( json_encode( BooklyLib\DataHolders\Notification\Settings::getDefault() ) )
            ->setGateway( 'email' )
            ->save();

        $notification = $notification->getFields();
        $id   = $notification['id'];
        $html = '';
        if ( self::parameter( 'render' ) ) {
            $form = new Forms\Notifications( 'email' );

            $html = Custom\Notification::render( $form, $notification, false );
        }
        wp_send_json_success( compact( 'html', 'id' ) );
    }
}