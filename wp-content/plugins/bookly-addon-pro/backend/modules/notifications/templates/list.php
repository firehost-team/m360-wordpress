<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/** @var Bookly\Backend\Modules\Notifications\Forms\Notifications $form */
use BooklyPro\Backend\Components\Notifications\Custom;
?>
<h4 class="bookly-block-head bookly-color-gray"><?php _e( 'Custom', 'bookly' ) ?></h4>
<div class="panel-group bookly-margin-vertical-xlg" id="bookly-js-custom-notifications">
    <?php foreach ( $form->getNotifications( 'custom' ) as $notification ) :
        Custom\Notification::render( $form, $notification );
    endforeach ?>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <button id="bookly-js-new-notification" type="button" class="btn btn-xlg btn-block btn-success-outline ladda-button" data-spinner-size="40" data-style="zoom-in" data-spinner-color="rgb(92, 184, 92)">
                <span class="ladda-label"><i class="dashicons dashicons-plus-alt"></i>
                    <?php _e( 'New Notification', 'bookly' ) ?>
                </span>
            </button>
        </div>
    </div>
</div>