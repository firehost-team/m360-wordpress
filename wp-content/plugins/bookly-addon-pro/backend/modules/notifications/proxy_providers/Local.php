<?php
namespace BooklyPro\Backend\Modules\Notifications\ProxyProviders;

use Bookly\Backend\Modules\Notifications\Proxy;

/**
 * Class Local
 * @package BooklyPro\Backend\Modules\Notifications\ProxyProviders
 */
class Local extends Proxy\Pro
{
    /**
     * @inheritdoc
     */
    public static function renderCustomEmailNotifications( $form )
    {
        self::renderTemplate( 'list', compact( 'form' ) );
    }
}