<?php
namespace BooklyPro\Lib;

use Bookly\Lib as BooklyLib;
use Bookly\Lib\Entities\Customer;
use Bookly\Lib\Entities\Notification;
use Bookly\Lib\DataHolders\Booking as DataHolders;
use Bookly\Lib\Notifications\Codes;

/**
 * Class NotificationSender
 * @package BooklyPro\Lib
 */
abstract class Sender extends BooklyLib\Notifications\Sender
{
    /**
     * Send email/sms with username and password for newly created WP user.
     *
     * @param Customer $customer
     * @param $username
     * @param $password
     */
    public static function sendNewUserCredentials( Customer $customer, $username, $password )
    {
        $codes                    = new Codes();
        $codes->client_address    = $customer->getPhone();
        $codes->client_email      = $customer->getEmail();
        $codes->client_first_name = $customer->getFirstName();
        $codes->client_last_name  = $customer->getLastName();
        $codes->client_name       = $customer->getFullName();
        $codes->client_phone      = $customer->getPhone();
        $codes->new_password      = $password;
        $codes->new_username      = $username;
        $codes->site_address      = site_url();

        $to_client = new Notification();
        if ( $to_client->loadBy( array( 'type' => 'client_new_wp_user', 'gateway' => 'email', 'active' => 1 ) ) ) {
            self::_sendEmailToClient( $to_client, $codes, $customer->getEmail() );
        }
        if ( $to_client->loadBy( array( 'type' => 'client_new_wp_user', 'gateway' => 'sms', 'active' => 1 ) ) ) {
            self::_sendSmsToClient( $to_client, $codes, $customer->getPhone() );
        }
    }

    /**
     * Send combined notifications.
     *
     * @param DataHolders\Order $order
     */
    public static function sendCombined( DataHolders\Order $order )
    {
        $wp_locale = self::_getWpLocale();
        $cart_info = array();
        $total     = 0.0;
        $items     = $order->getItems();
        $status    = get_option( 'bookly_gen_default_appointment_status' );

        $client_email_notification = self::_getCombinedEmailNotification( $status );
        $client_sms_notification   = self::_getCombinedSmsNotification( $status );

        $client_notify = ( $client_email_notification || $client_sms_notification );

        if ( $client_notify ) {
            // For recurring appointments,
            // key in array is unique_serial_id
            $first = reset( $items );
            if ( $first->isSeries() ) {
                /** @var $first DataHolders\Series */
                $sub_items  = $first->getItems();
                $first_item = $sub_items[0];
            } else {
                $first_item = $first;
            }

            $client_locale = $first_item->getCA()->getLocale() ?: $wp_locale;
        } else {
            $client_locale = $wp_locale;
        }

        foreach ( $items as $item ) {
            $sub_items = array();

            // Send notification to staff.
            switch ( $item->getType() ) {
                case DataHolders\Item::TYPE_SIMPLE:
                case DataHolders\Item::TYPE_COMPOUND:
                case DataHolders\Item::TYPE_COLLABORATIVE:
                    self::sendSingle( $item, $order, array(), true, false );
                    $sub_items[] = $item;
                    break;
                case DataHolders\Item::TYPE_SERIES:
                    /** @var DataHolders\Series $item */
                    BooklyLib\Proxy\RecurringAppointments::sendRecurring( $item, $order, array(), true, false );
                    $sub_items = $item->getItems();
                    if ( get_option( 'bookly_recurring_appointments_payment' ) == 'first' ) {
                        array_splice( $sub_items, 1 );
                    }
                    break;
            }
            if ( $client_notify ) {
                foreach ( $sub_items as $sub_item ) {
                    // Sub-item price.
                    $price = $sub_item->getTotalPrice();

                    $deposit_price = BooklyLib\Config::depositPaymentsActive()
                        ? BooklyLib\Proxy\DepositPayments::prepareAmount( $price, $sub_item->getDeposit(), $sub_item->getCA()->getNumberOfPersons() )
                        : 0;

                    // Prepare data for {cart_info} || {cart_info_c}.
                    $cart_info[] = array(
                        'appointment_price' => $price,
                        'appointment_start' => self::_applyTimeZone( $sub_item->getAppointment()->getStartDate(), $sub_item->getCA() ),
                        'cancel_url'        => admin_url( 'admin-ajax.php?action=bookly_cancel_appointment&token=' . $sub_item->getCA()->getToken() ),
                        'service_name'      => $sub_item->getService()->getTranslatedTitle( $client_locale ),
                        'staff_name'        => $sub_item->getStaff()->getTranslatedName( $client_locale ),
                        'extras'            => (array) BooklyLib\Proxy\ServiceExtras::getInfo( $sub_item->getExtras(), true, $client_locale ),
                        'tax'               => BooklyLib\Config::taxesActive() ? $sub_item->getTax() : null,
                        'deposit'           => BooklyLib\Config::depositPaymentsActive() ? BooklyLib\Proxy\DepositPayments::formatDeposit( $deposit_price, $sub_item->getDeposit() ) : null,
                        'appointment_start_info' => $sub_item->getService()->getDuration() < DAY_IN_SECONDS ? null : $sub_item->getService()->getStartTimeInfo(),
                    );

                    // Total price.
                    $total += $price;
                }
            }
        }

        // Send combined notifications to client.
        if ( $client_notify ) {
            self::_switchLocale( $client_locale );
            // Prepare codes.
            $codes = Codes::createForOrder( $order, $first_item );
            $codes->cart_info = $cart_info;
            if ( ! $order->hasPayment() ) {
                $codes->total_price = $total;
            }

            if ( $client_email_notification ) {
                self::_sendEmailToClient( $client_email_notification, $codes, $order->getCustomer()->getEmail() );
            }
            if ( $client_sms_notification ) {
                self::_sendSmsToClient( $client_sms_notification, $codes, $order->getCustomer()->getPhone() );
            }

            // Restore location.
            self::_switchLocale( $wp_locale );
        }
    }
}