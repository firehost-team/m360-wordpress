<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="col-md-3">
    <div class="checkbox">
        <label data-toggle="popover" data-trigger="hover" data-placement="auto" data-content="<?php echo esc_attr( 'Show number of persons required', 'bookly' ) ?>">
            <input type="checkbox" id="bookly-show-nop-on-time-step" <?php checked( get_option( 'bookly_group_booking_app_show_nop' ) ) ?>>
            <?php _e( 'Show information about group bookings', 'bookly' ) ?>
        </label>
    </div>
</div>