<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="row">
    <div class="col-sm-4 bookly-js-service bookly-js-service-simple">
        <div class="form-group">
            <label for="capacity_<?php echo $service['id'] ?>"><?php esc_html_e( 'Capacity (min and max)', 'bookly' ) ?></label>
            <p class="help-block"><?php esc_html_e( 'The minimum and maximum number of customers allowed to book the service for the certain time period.', 'bookly' ) ?></p>
            <div class="row">
                <div class="col-xs-6">
                    <input id="capacity_min_<?php echo $service['id'] ?>" class="form-control bookly-js-question bookly-js-capacity" type="number" min="1" step="1" name="capacity_min" value="<?php echo esc_attr( $service['capacity_min'] ) ?>">
                </div>
                <div class="col-xs-6">
                    <input id="capacity_max_<?php echo $service['id'] ?>" class="form-control bookly-js-question bookly-js-capacity" type="number" min="1" step="1" name="capacity_max" value="<?php echo esc_attr( $service['capacity_max'] ) ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 bookly-js-service bookly-js-service-simple">
        <div class="form-group">
            <label for="one_booking_per_slot_<?php echo $service['id'] ?>"><?php esc_html_e( 'One booking per time slot', 'bookly' ) ?></label>
            <p class="help-block"><?php esc_html_e( 'Enable this option if you want to limit the possibility of booking within the service capacity to one time.', 'bookly' ) ?></p>
            <select name="one_booking_per_slot" class="form-control bookly-js-one_booking_per_slot" id="one_booking_per_slot_<?php echo $service['id'] ?>">
                <option value="0" <?php selected( $service['one_booking_per_slot'], 0 ) ?>><?php esc_attr_e( 'Disabled', 'bookly' ) ?></option>
                <option value="1" <?php selected( $service['one_booking_per_slot'], 1 ) ?>><?php esc_attr_e( 'Enabled', 'bookly' ) ?></option>
            </select>
        </div>
    </div>
</div>