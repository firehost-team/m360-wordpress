<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="col-md-3">
    <div class="checkbox">
        <label>
            <input type="checkbox" id="bookly-show-nop" <?php checked( get_option( 'bookly_group_booking_enabled' ) ) ?>>
            <?php esc_html_e( 'Show number of persons', 'bookly' ) ?>
        </label>
    </div>
</div>
