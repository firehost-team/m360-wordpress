<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Lib as BooklyLib;
use Bookly\Backend\Modules\Appearance\Proxy;
?>
<div class="bookly-js-cart-settings bookly-margin-top-lg" style="display:none">
    <?php Proxy\ServiceExtras::renderShowCartExtras() ?>
</div>