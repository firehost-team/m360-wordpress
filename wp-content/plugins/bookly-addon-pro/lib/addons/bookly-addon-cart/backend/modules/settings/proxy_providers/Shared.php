<?php
namespace BooklyCart\Backend\Modules\Settings\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Backend\Modules\Settings\Proxy;
use Bookly\Backend\Components\Settings\Menu;

/**
 * Class Shared
 * @package BooklyCart\Backend\Modules\Settings\ProxyProviders
 */
class Shared extends Proxy\Shared
{
    /**
     * @inheritdoc
     */
    public static function renderMenuItem()
    {
        Menu::renderItem( esc_html__( 'Cart', 'bookly' ), 'cart' );
    }

    /**
     * @inheritdoc
     */
    public static function renderTab()
    {
        $cart_columns = array(
            'service'  => BooklyLib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_service' ),
            'date'     => __( 'Date', 'bookly' ),
            'time'     => __( 'Time', 'bookly' ),
            'employee' => BooklyLib\Utils\Common::getTranslatedOption( 'bookly_l10n_label_employee' ),
            'price'    => __( 'Price', 'bookly' ),
            'deposit'  => __( 'Deposit', 'bookly' ),
            'tax'      => __( 'Tax', 'bookly' ),
        );

        self::renderTemplate( 'settings_tab', compact( 'cart_columns' ) );
    }

    /**
     * @inheritdoc
     */
    public static function saveSettings( array $alert, $tab, array $params )
    {
        if ( $tab == 'cart' ) {
            $options = array( 'bookly_cart_show_columns' );
            foreach ( $options as $option_name ) {
                if ( array_key_exists( $option_name, $params ) ) {
                    update_option( $option_name, $params[ $option_name ] );
                }
            }
            $alert['success'][] = __( 'Settings saved.', 'bookly' );
            if ( get_option( 'bookly_wc_enabled' ) && $params['bookly_cart_enabled'] ) {
                $alert['error'][] = sprintf(
                    __( 'To use the cart, disable integration with WooCommerce <a href="%s">here</a>.', 'bookly' ),
                    BooklyLib\Utils\Common::escAdminUrl( \Bookly\Backend\Modules\Settings\Page::pageSlug(), array( 'tab' => 'woo_commerce' ) )
                );
            }
        }

        return $alert;
    }
}