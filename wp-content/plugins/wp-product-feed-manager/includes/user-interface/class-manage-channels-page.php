<?php
/**
 * WP Product Feed Manage Channels Page.
 *
 * @package WP Product Feed Manager/User-Interface/Classes
 * @version 2.1.1
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }


if ( ! class_exists( 'WPPFM_Manage_Channels_Page' ) ) :

	/**
	 * Manage Channels Page Class
	 */
	class WPPFM_Manage_Channels_Page extends WPPFM_Admin_Page {
		
		/** @var stores the maximum nr of channels allowed */
		private $_max = 1000;

		public function __construct() {
			parent::__construct();

			WPPFM_i18n_Scripts::wppfm_manage_channels_i18n();
			WPPFM_i18n_Scripts::wppfm_list_table_i18n();
		}

		/**
		 * Produces the Manage Channels page
		 * @param bool $updated
		 */
		public function show( $updated ) {
			echo $this->admin_page_header();

			echo $this->message_field();

			if ( ! is_wp_error( $updated ) ) {
				echo $this->channel_page_body_top( $updated );
			} else {
				/*translators: %s: link to the support page */
				echo wppfm_handle_wp_errors_response( $updated, sprintf( __( '2127 - Try refreshing the page and Please open a support ticket at %s if the issue persists.', 'wp-product-feed-manager' ), WPPFM_SUPPORT_PAGE_URL ) );
			}

			echo $this->admin_page_footer();
		}

		/**
		 * Returns the main body part of the Manage Channels page
		 * @param bool $updated
		 * @return string
		 */
		private function channel_page_body_top( $updated ) {
			$channels_class = new WPPFM_Channel();

			$response = $channels_class->get_channels_from_server();
			
			if ( ! is_wp_error( $response ) ) {
				$available_channels = json_decode( $response['body'] );
				
				if ( $available_channels ) {

					$installed_channels_names = $channels_class->get_installed_channel_names();
					
					$mic = count( $installed_channels_names ) >= $this->_max ? true : false;

					$channels_class->add_status_data_to_available_channels( $available_channels, $installed_channels_names, $updated );

					$installed_channels = array_filter( $available_channels, function( $obj ) {
						return( $obj->status === 'installed' );
					} );

					$uninstalled_channels = array_filter( $available_channels, function( $obj ) {
						return( $obj->status !== 'installed' );
					} );

					$ca = 0;

					$html = '<h3>' . esc_html__( 'Installed Channels:', 'wp-product-feed-manager' ) . '</h3>';
					$html .= esc_html__( 'The following channels are ready to use', 'wp-product-feed-manager' );
					$html .= '<table cellspacing="16" cellpadding="0" id="available-channels"><tbody>';

					foreach( $installed_channels as $channel ) {

						$html .= $this->make_channel_tile( $channel, $ca, $mic );
						$ca++;
					}

					$html .= '</tbody></table>';
					$html .= '<div><h3>' . esc_html__( 'Uninstalled Channels:', 'wp-product-feed-manager' ) . '</h3>';
					$html .= esc_html__( 'The following channels are ready to be installed', 'wp-product-feed-manager' ) . '</div>';
					$html .= '<table cellspacing="16" cellpadding="0" id="unavailable-channels"><tbody>';

					$cu = 0;

					foreach( $uninstalled_channels as $channel ) {
						$html .= $this->make_channel_tile( $channel, $cu, $mic );
						$cu++;
					}

					$html .= '</tbody></table>';
				} else {

					$html = '<div><h3>' . esc_html__( 'No channels available. Please check if your license is current and active on the Feed Manager tab', 'wp-product-feed-manager' ) . '</h3></div>';
				}
			} else {
				/*translators: %s: link to the support page */
				echo wppfm_handle_wp_errors_response( $response, sprintf( __( '2965 - Please try to refreh the page and Please open a support ticket at %s if the issue persists.', 'wp-product-feed-manager' ), WPPFM_SUPPORT_PAGE_URL ) );
				$html = '';
			}
			
			return $html;
		}
		
		private function make_channel_tile( $channel, $c, $mic ) {
			$html = '';
			
			$counter = ( $c % 3 );

			$html .= $counter === 0 ? '<tr>' : '';
			$html .= $this->channel_tile( $channel, $mic );
			$html .= $counter === 2 ? '</tr>' : '';
			
			return $html;
		}
		
		private function channel_tile( $channel, $mic ) {
			$version_status = floatval( $channel->version ) > floatval( $channel->installed_version ) ? 'outdated' : 'up to date';

			if ( $version_status === 'up to date' ) { 
				/* translators: %s: Version number of installed channel */
				$version_text = sprintf( esc_html__( 'Version %s', 'wp-product-feed-manager' ), $channel->installed_version );
			} elseif ( $channel->installed_version === 'installed') {
				/* translators: %s: Version number of installed channel */
				$version_text = sprintf( esc_html__( 'Version %s', 'wp-product-feed-manager' ), $channel->version );
				$version_status = 'installed'; // reset the version status
			} elseif ( $channel->installed_version === 'unknown' ) {
				/* translators: Channel version unknown */
				$version_text = esc_html__( 'Version unknown', 'wp-product-feed-manager' );
			} else {
				/* translators: 1: Current channel version number, 2: new channel version number */
				$version_text = sprintf( 
					esc_html__( 'Version %1$s. New version %2$s available!', 'wp-product-feed-manager' ), 
						$channel->installed_version, 
						$channel->version );
			}

			$html = '<td class="channel-tile" id="' . $channel->short_name . '">';
			$html .= '<div class="channel-inner">';
			
			$html .= '<img class="channel-thumbnail" src="' .  urldecode( $channel->image ) . '">';
			$html .= '<h3>' . $channel->channel . '</h3>';
			
			if ( $channel->status === 'installed' ) {
				$remove_nonce = wp_create_nonce( 'delete-channel-nonce' );

				$html .= '<div class="channel-version" id="' . $version_status . '">' . $version_text . '</div>';
				$html .= '<a href="admin.php?page=wp-product-feed-manager-manage-channels&wppfm_action=remove&wppfm_channel=';
				$html .= $channel->short_name . '&wppfm_nonce=' . $remove_nonce . '" class="button channel-button" title="'; 
				/* translators: %s: channel name */
				$html .= sprintf( esc_html__( 'Remove the %s channel.', 'wp-product-feed-manager' ), $channel->channel ) . '" onclick="return wppfm_alertRemoveChannel();">';
				$html .= esc_html__( 'Remove', 'wp-product-feed-manager' ) . '</a>';
				
				if ( $version_status === 'outdated' ) {
					$update_nonce = wp_create_nonce( 'update-channel-nonce' );
					$html .= '<a href="admin.php?page=wp-product-feed-manager-manage-channels&wppfm_action=update&wppfm_channel=';
					$html .= $channel->short_name . '&wppfm_code=' . $channel->dir_code . '&wppfm_nonce=' . $update_nonce . '" class="button" title="';
					$html .= esc_html__( 'Update this channel to the latest version', 'wp-product-feed-manager' ) . '">';
					$html .= esc_html__( 'Update', 'wp-product-feed-manager' ) . '</a>';
				}
			} else {
				if ( ! $mic ) {
					$install_nonce = wp_create_nonce( 'install-channel-nonce' );
					$html .= '<a href="admin.php?page=wp-product-feed-manager-manage-channels&wppfm_action=install&wppfm_channel=';
					$html .= $channel->short_name . '&wppfm_code=' . $channel->dir_code . '&wppfm_nonce=' . $install_nonce . '" class="button" title="';
					/* translators: %s: channel name */
					$html .= sprintf( esc_html__( 'Install the %s channel.', 'wp-product-feed-manager' ), $channel->channel ) . '">';
					$html .= esc_html__( 'Install', 'wp-product-feed-manager' ) . '</a>';
				} else {
					$html .= '<a href="javascript:void(0);" class="button" title="';
					$html .= esc_html__( 'The maximum number of channels for your license has been reached. Please remove an unused channel or upgrade your license.', 'wp-product-feed-manager' ) . '" disabled>';
					$html .= esc_html__( 'Install', 'wp-product-feed-manager' ) . '</a>';
				}
			}
			
			$html .= '</div></td>';
			
			return $html;
		}
	}

     // end of WPPFM_Manage_Channels_Page

endif;