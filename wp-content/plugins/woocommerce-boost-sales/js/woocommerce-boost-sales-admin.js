'use strict';
jQuery(document).ready(function () {

    jQuery('.wbs_exclude_product').dependsOn({
        'input[name="_woocommerce_boost_sales[show_with_category]"]': {
            checked: true
        }
    });
    jQuery('.select_product_bundle').dependsOn({
        'input[name="_woocommerce_boost_sales[enable_cart_page]"]': {
            checked: true
        }
    });
    jQuery('.select_product_bundle_checkout').dependsOn({
        'input[name="_woocommerce_boost_sales[enable_checkout_page]"]': {
            checked: true
        }
    });

});