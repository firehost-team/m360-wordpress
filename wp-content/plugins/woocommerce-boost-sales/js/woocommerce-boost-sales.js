'use strict';
jQuery(document).ready(function () {
	woo_boost_sale.init();
	var time_redirect = jQuery('.woocommerce-boost-sales').attr('data-time_rdt');
	if (time_redirect) {
		woo_boost_sale.counter(jQuery('#wbs_time_rdt'), time_redirect);
	}

});

var woo_boost_sale = {
	init                   : function () {
		this.destroy();
		this.add_to_cart();
		this.slider();

		jQuery('.wbs-close, .woocommerce-boost-sales .overlay').unbind();
		jQuery('.wbs-close, .woocommerce-boost-sales .overlay').on('click', function () {
			woo_boost_sale.hide();
		});

		this.initial_delay_icon();

		jQuery('.gift-button').on('click', function () {

			jQuery(document).scrollTop(0);
			//woo_boost_sale.hide_upsell();
			woo_boost_sale.show_cross_sell();
			//woo_boost_sale.slider_cross_sell();
			jQuery('.vi-wbs-headline').removeClass('wbs-crosssell-message').addClass('wbs-crosssell-message');

		});

		setInterval(function () {
			jQuery('.gift-button').toggleClass('bounce');
		}, 2000);

		jQuery('#wbs-gift-button-cat').on('click', function () {
			woo_boost_sale.hide_upsell();
			woo_boost_sale.show_cross_sell_archive();
		});

		if (jQuery('.vi-wbs-topbar').hasClass('wbs_top_bar')) {
			var windowsize = jQuery(window).width();
			jQuery('.vi-wbs-headline').css('top', '50px');
			if (windowsize >= 1366) {
				jQuery('.wbs-archive-upsells .wbs-content').css('margin-top', '45px');
			} else {
				jQuery('.wbs-archive-upsells .wbs-content').css('margin-top', '85px');
			}
		}

		if (jQuery('.vi-wbs-topbar').hasClass('wbs_bottom_bar')) {
			var windowsize = jQuery(window).width();
			if (windowsize < 1366) {
				jQuery('.wbs-archive-upsells .wbs-content').css('margin-top', '70px');
			}
			if (windowsize < 640) {
				jQuery('.wbs-archive-upsells .wbs-content').css('margin-top', '0px');
			}
		}

		if (!jQuery('div').hasClass('wbs-archive-upsells')) {
			jQuery('.wbs-content-up-sell').css('height', '0%');
		} else {
			jQuery('.wbs-content-up-sell').css('height', '100%');
		}

		if (jQuery('.wbs-content').hasClass('wbs-msg-congrats')) {
			setTimeout(function () {
				jQuery('.vi-wbs-headline').show();
			}, 0);
		}

		jQuery('.vi-wbs_progress_close').on('click', function () {
			jQuery('.vi-wbs-topbar').fadeOut('slow');
		});

		if (!jQuery('#flexslider-cross-sell .vi-flex-prev').hasClass('vi-flex-disabled')) {
			jQuery('#flexslider-cross-sell').hover(function () {
				jQuery('#flexslider-cross-sell .vi-flex-prev').css("opacity", "1");
			}, function () {
				jQuery('#flexslider-cross-sell .vi-flex-prev').css("opacity", "0");
			});
		}

		if (!jQuery('#flexslider-cross-sell .vi-flex-next').hasClass('vi-flex-disabled')) {
			jQuery('#flexslider-cross-sell').hover(function () {
				jQuery('#flexslider-cross-sell .vi-flex-next').css("opacity", "1");
			}, function () {
				jQuery('#flexslider-cross-sell .vi-flex-next').css("opacity", "0");
			});
		}

		/*Smooth Archive page*/
		jQuery('.wbs-single-page').animate({
			opacity: 1
		}, 200);


		woo_boost_sale.chosen_variable_upsell();
		jQuery('.wbs-upsells > li').find('div.vi_wbs_chosen:first').removeClass('wbp_hidden_variable').addClass('wbp_show_variable');

	},
	add_to_cart            : function () {
		jQuery('body').bind('added_to_cart', function (event, fragments, cart_hash, button) {
			jQuery(document).scrollTop(0);
			var product_id = jQuery(button).data('product_id');
			var quantity = jQuery(button).data('quantity');

			jQuery.ajax({
				type   : 'POST',
				data   : 'action=wbs_get_product' + '&id=' + product_id + '&quantity=' + quantity,
				url    : wboostsales_ajax_url,
				success: function (html) {
					var time_redirect = jQuery('.woocommerce-boost-sales').attr('data-time_rdt');
					jQuery('.wbs-content-up-sell').html(html);
					jQuery('.wbs-content-up-sell').fadeIn();
					woo_boost_sale.init();
					woo_boost_sale.hide_cross_sell();
					woo_boost_sale.slider();
					setTimeout(function () {
						jQuery('.wbs-archive-page').animate({
							opacity: 1
						}, 200);
					}, 200);
					if (time_redirect) {
						woo_boost_sale.counter(jQuery('#wbs_time_rdt'), time_redirect);
					}
				},
				error  : function (html) {
				}
			})
		});
	},
	hide                   : function () {
		jQuery('.woocommerce-boost-sales').fadeOut(200);
	},
	destroy                : function () {
		jQuery('body').unbind('added_to_cart');
	},
	slider                 : function () {
		var windowsize = jQuery(window).width();
		var item_per_row = jQuery('#flexslider-up-sell').attr('data-item_per_row');

		if (item_per_row == undefined) {
			item_per_row = 4;
		}
		if (windowsize < 768 && windowsize >= 600) {
			item_per_row = 2;
		}
		if (windowsize < 600) {
			item_per_row = 1;
		}

		jQuery('#flexslider-up-sell').vi_flexslider({
			animation    : "slide",
			animationLoop: false,
			itemWidth    : 145,
			itemMargin   : 12,
			controlNav   : false,
			minItems     : item_per_row,
			maxItems     : item_per_row
		});
	},
	cross_slider           : function () {
		var windowsize = jQuery(window).width(),
			min_item = 3,
			max_item = 3;
		if (windowsize < 768 && windowsize >= 600) {
			min_item = 2;
			max_item = 2;
		}
		if (windowsize < 600) {
			min_item = 1;
			max_item = 1;
		}

		jQuery('.wbs-item-crosssells').vi_flexslider({
			animation    : "slide",
			animationLoop: false,
			itemWidth    : 145,
			itemMargin   : 24,
			controlNav   : false,
			minItems     : min_item,
			maxItems     : max_item
		});

	},
	hide_upsell            : function () {
		jQuery('.wbs-content').fadeOut(200);
	},
	hide_cross_sell        : function () {
		jQuery('#wbs-content-cross-sell').fadeOut(200);
	},
	show_cross_sell        : function () {
		jQuery('#wbs-content-cross-sell').fadeIn('slow');
		woo_boost_sale.cross_slider();
	},
	show_cross_sell_archive: function () {
		jQuery('#wbs-cross-sell-archive').fadeIn('slow');
		woo_boost_sale.cross_slider();

	},
	counter                : function ($el, n) {
		var checkout_url = jQuery('#wbs_time_rdt').attr('data-url');
		(function loop() {
			$el.html(n);
			if (n == 0) {
				if (checkout_url) {
					window.location.href = checkout_url;
				}
			}
			if (n--) {
				setTimeout(loop, 1000);
			}
		})();
	},
	initial_delay_icon     : function () {
		var initial_delay = jQuery('.gift-button').attr('data-initial_delay');
		setTimeout(function () {
			jQuery('.gift-button').fadeIn('slow');
		}, initial_delay * 1000);
	},
	chosen_variable_upsell : function () {
		var attr = jQuery('select[name="vi_chosen_product_variable"]').attr('class');
		if (typeof attr !== typeof undefined && attr !== false) {
			jQuery('select[name="vi_chosen_product_variable"]').removeAttr('class');
		}

		jQuery('select[name="vi_chosen_product_variable"]').on('change', function () {
			var selected = jQuery(this).val();
			jQuery(this).closest('li').find('.vi_wbs_chosen[data-id_prd="' + selected + '"]').removeClass('wbp_hidden_variable').addClass('wbp_show_variable');
			jQuery(this).closest('li').find('.vi_wbs_chosen[data-id_prd !="' + selected + '"]').removeClass('wbp_show_variable').addClass('wbp_hidden_variable');
		});
	}
}


