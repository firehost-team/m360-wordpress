/**
 * Created by FalconNguyen on 8/17/2016.
 */
jQuery(function ($) {
	$('body').on('woocommerce-product-type-change', function (event, select_val, select) {
		if(jQuery('#product-type :selected').val() === 'wbs_bundle') {
			jQuery('.options_group.pricing, .general_tab').show()
		}

		jQuery('.options_group.pricing').addClass('show_if_wbs_bundle').show();
		//if ( select_val == 'wbs_bundle' ) {
		//
		//	$( 'input#_downloadable' ).prop( 'checked', false );
		//	$( 'input#_virtual' ).removeAttr( 'checked' );
		//
		//	$( '.show_if_external' ).hide();
		//	$( '.show_if_simple' ).show();
		//	$( '.show_if_bundle' ).show();
		//
		//	$( 'input#_downloadable' ).closest( '.show_if_simple' ).hide();
		//	$( 'input#_virtual').closest('.show_if_simple' ).hide();
		//
		//	$( 'input#_manage_stock' ).change();
		//	$( 'input#_per_product_pricing_active' ).change();
		//	$( 'input#_per_product_shipping_active' ).change();
		//
		//	$( '#_nyp' ).change();
		//
		//	$( '.product_data_tabs .general_tab' ).show();
		//} else {
		//	$( '.show_if_wbs_bundle' ).hide();
		//}

		jQuery(".wc-product-search1").select2({
			placeholder: "Please fill your title product",
			ajax: {
				allowClear: true,
				url: "admin-ajax.php?action=wbs_search_product_in_bundle",
				dataType: 'json',
				type: "GET",
				quietMillis: 50,
				delay: 250,
				data: function (params) {
					return {
						keyword: params.term
					};
				},
				processResults: function (data) {
					return {
						results: data
					};
				}
			},
			escapeMarkup: function (markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength: 1
		});

	});
	var bundled_items_cont = $('#wbs_bundled_product_data .wbs-wcpb-bundled-items'),
		add_bundled_product_btn = $('#wbs-wcpb-add-bundled-product'),
		block_params = {
			message   : null,
			overlayCSS: {
				background: '#fff url(' + woocommerce_admin_meta_boxes.plugin_url + '/assets/images/ajax-loader.gif) no-repeat center',
				opacity   : 0.6
			}
		},
		b_prod_id = $('#wbs-wcpb-bundled-product'),
		remove_bundled_product_btn = $('.wbs-wcpb-remove-bundled-product-item'),
		items_count = $('#wbs_bundled_product_data .wbs-wcpb-bundled-items .wbs-wcpb-bundled-item').size(),
		bundled_product_data_container = $('#wbs_bundled_product_data'),
		add_action_to_remove_btn = function () {
			remove_bundled_product_btn = $('.wbs-wcpb-remove-bundled-product-item');
			remove_bundled_product_btn.on('click', function () {
				$(this).parent().parent().remove();
				//items_count--;
			});
		};


	items_count++;
	add_bundled_product_btn.on('click', function () {
		if (b_prod_id.val() == 0) {
			return
		}

		bundled_product_data_container.block(block_params);
		var data = {
			action     : 'wbs_wcpb_add_product_in_bundle',
			open_closed: 'open',
			post_id    : woocommerce_admin_meta_boxes.post_id,
			id         : items_count,
			product_id : b_prod_id.val(),
		};

		$.post(woocommerce_admin_meta_boxes.ajax_url, data, function (response) {
			if (response == 'notsimple') {
				alert(ajax_object.free_not_simple);
				bundled_product_data_container.unblock();
				return;
			}
			bundled_items_cont.append(response);
			bundled_items_cont.find('.help_tip').tipTip();
			add_action_to_remove_btn();
			bundled_product_data_container.unblock();
			b_prod_id.val(0);
			items_count++;
		});
	});

	add_action_to_remove_btn();
});