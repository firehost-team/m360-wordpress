'use strict';
jQuery(document).ready(function () {
	/*Search*/
	/*jQuery.fn.select2.amd.require([
	 'select2/utils',
	 'select2/dropdown',
	 'select2/dropdown/attachBody'
	 ], function (Utils, Dropdown, AttachBody) {
	 function SelectAll() {
	 }

	 SelectAll.prototype.render = function (decorated) {
	 var $rendered = decorated.call(this);
	 var self = this;

	 var $selectAll = jQuery(
	 '<button type="button" class="button">Select All</button>'
	 );

	 $rendered.find('.select2-dropdown').prepend($selectAll);

	 $selectAll.on('click', function (e) {
	 var $results = $rendered.find('.select2-results__option[aria-selected=false]');

	 // Get all results that aren't selected
	 $results.each(function () {
	 var $result = jQuery(this);

	 // Get the data object for it
	 var data = $result.data('data');

	 // Trigger the select event
	 self.trigger('select', {
	 data: data
	 });
	 });

	 self.trigger('close');
	 });

	 return $rendered;
	 };

	 jQuery(".product-search-col1").select2({
	 placeholder: "Please fill your title product",
	 ajax: {
	 url: "admin-ajax.php?action=wbs_search_product",
	 dataType: 'json',
	 type: "GET",
	 quietMillis: 50,
	 delay: 250,
	 data: function (params) {
	 return {
	 keyword: params.term,
	 p_id: jQuery(this).closest('td').data('id')
	 };
	 },
	 processResults: function (data) {
	 return {
	 results: data
	 };
	 }
	 },
	 escapeMarkup: function (markup) {
	 return markup;
	 }, // let our custom formatter work
	 minimumInputLength:1,
	 dropdownAdapter: Utils.Decorate(
	 Utils.Decorate(
	 Dropdown,
	 AttachBody
	 ),
	 SelectAll
	 )
	 });
	 });*/

	jQuery(".product-search").select2({
		placeholder       : "Please fill your title product",
		ajax              : {
			url           : "admin-ajax.php?action=wbs_search_product",
			dataType      : 'json',
			type          : "GET",
			quietMillis   : 50,
			delay         : 250,
			data          : function (params) {
				return {
					keyword: params.term,
					p_id   : jQuery(this).closest('td').data('id')
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			}
		},
		escapeMarkup      : function (markup) {
			return markup;
		}, // let our custom formatter work
		minimumInputLength: 1
	});
	/*Save Up sell*/
	jQuery('.button-save').on('click', function () {
		var product_id = jQuery(this).closest('td').data('id');
		var btn = jQuery(this);
		if (product_id) {
			var u_id = jQuery('select.u-product-' + product_id).val();
			u_id = u_id.toString();
			btn.text('Saving');
			jQuery.ajax({
				type   : 'POST',
				data   : 'action=wbs_u_save_product' + '&id=' + product_id + '&u_id=' + u_id,
				url    : wboostsales_ajax_url,
				success: function (html) {
					var obj = jQuery.parseJSON(html);
					if (obj.check == 'done') {
						btn.text('Save');
						btn.removeClass('button-primary');
					} else {

					}
				},
				error  : function (html) {
				}
			})
		} else {
			return false;
		}
	});
	/*Remove all*/
	jQuery('.button-remove').on('click', function () {
		var r = confirm("Your products in up-sells of selected product will be removed all. Are you sure ?");
		if (r == true) {
			var product_id = jQuery(this).closest('td').data('id');

			var btn = jQuery(this);
			if (product_id) {
				btn.text('Removing');
				jQuery.ajax({
					type   : 'POST',
					data   : 'action=wbs_u_remove_product' + '&id=' + product_id,
					url    : wboostsales_ajax_url,
					success: function (html) {
						var obj = jQuery.parseJSON(html);
						if (obj.check == 'done') {
							btn.text('Remove all');
							jQuery('select.u-product-' + product_id).val('null').trigger("change");
						} else {

						}
					},
					error  : function (html) {
					}
				})
			} else {
				return false;
			}
		}
	});
	/*Action after selected product*/
	jQuery('.product-search').on("select2:selecting", function (e) {
		// what you would like to happen
		var p_id = jQuery(this).closest('td').data('id');
		jQuery('.product-action-' + p_id).find('.button-save').addClass('button-primary');
	});
	/*Action after remove product*/
	jQuery('.product-search').on("select2:unselecting", function (e) {
		var p_id = jQuery(this).closest('td').data('id');
		jQuery('.product-action-' + p_id).find('.button-save').addClass('button-primary');
	});
	/*Click Bulk Adds*/
	jQuery('.btn-bulk-adds').on('click', function () {
		jQuery('.bulk-adds').slideToggle('400');
		jQuery('.list-products').fadeToggle('400');
	});
	/*Bulk Add products Upsell*/
	jQuery('.ba-button-save').on('click', function () {

		var p_id = jQuery('select.ba-product').val();
		var u_id = jQuery('select.ba-u-product').val();
		var btn = jQuery(this);
		if (p_id && u_id) {
			u_id = u_id.toString();
			p_id = p_id.toString();
			btn.text('Adding');
			jQuery.ajax({
				type   : 'POST',
				data   : 'action=wbs_ba_save_product' + '&p_id=' + p_id + '&u_id=' + u_id,
				url    : wboostsales_ajax_url,
				success: function (html) {
					var obj = jQuery.parseJSON(html);
					if (obj.check == 'done') {
						reload_cache();
					} else {

					}
				},
				error  : function (html) {
				}
			})
		} else if (u_id && jQuery('input#vi_chk_selectall').is(':checked')) {
			u_id = u_id.toString();
			btn.text('Adding');
			jQuery.ajax({
				type   : 'POST',
				data   : 'action=wbs_ba_save_all_product' + '&u_id=' + u_id,
				url    : wboostsales_ajax_url,
				success: function (html) {
					var obj = jQuery.parseJSON(html);
					if (obj.check == 'done') {
						reload_cache();
					} else {

					}
				},
				error  : function (html) {
				}
			})
		} else {
			return false
		}
	});

	/*checkbox select all*/
	jQuery('input#vi_chk_selectall').on('change', function () {
		if (jQuery('input#vi_chk_selectall').is(':checked')) {
			jQuery(this).closest('td').find('span.select2-container').css('display', 'none');
		} else {
			jQuery(this).closest('td').find('span.select2-container').css('display', 'block');
		}
	});


	/*Remove all*/
	jQuery('.btn-sync-upsell').on('click', function () {
		var r = confirm("Product up-sells of WooCommerce will be got. Are you sure ?");
		if (r == true) {
			var btn = jQuery(this);
			btn.text('Getting');
			jQuery.ajax({
				type   : 'POST',
				data   : 'action=wbs_u_sync_product',
				url    : wboostsales_ajax_url,
				success: function (html) {
					var obj = jQuery.parseJSON(html);
					if (obj.check == 'done') {
						btn.text('Get Product Up-Sells');
						reload_cache();
					} else {

					}
				},
				error  : function (html) {
				}
			})
		}
	});
	/*Reload*/
	function reload_cache() {
		jQuery('.product-search').trigger('change');
		location.reload();
	}

	var wbs_different_up = jQuery('#wbs_different_up-cross-sell').data('wbs_up_crosssell');
	jQuery(document).tooltip({
		items   : "#wbs_different_up-cross-sell",
		position: {
			my: "right top+10"
		},
		track   : true,
		content : '<img class="wbs_img_tooltip_dfc" src="'+wbs_different_up+'" width="700px" style="float: left; margin-left: 180px;" />',
		show: {
			effect: "slideDown",
			delay: 150
		}
	});

});