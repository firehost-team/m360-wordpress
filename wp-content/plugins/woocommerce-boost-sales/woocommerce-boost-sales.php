<?php
/*
Plugin Name: WooCommerce Boost Sales
Plugin URI: http://villatheme.com
Description: Increases sales from every order by using Up-sell and Cross-sell techniques for your online store.
Version: 1.1.5
Author: Andy Ha (villatheme.com)
Author URI: http://villatheme.com
Copyright 2017 VillaTheme.com. All rights reserved.
*/
define( 'VI_WBOOSTSALES_VERSION', '1.1.5' );
/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
	$init_file = WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . "woocommerce-boost-sales" . DIRECTORY_SEPARATOR . "includes" . DIRECTORY_SEPARATOR . "define.php";
	require_once $init_file;
}


/**
 * Class VI_WBOOSTSALES
 */
class VI_WBOOSTSALES {
	public function __construct() {
		register_activation_hook( __FILE__, array( $this, 'install' ) );
		register_deactivation_hook( __FILE__, array( $this, 'uninstall' ) );
		add_action( 'admin_notices', array( $this, 'global_note' ) );
	}

	function global_note() {
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			deactivate_plugins( 'woocommerce-boost-sales/woocommerce-boost-sales.php' );
			unset( $_GET['activate'] );
			?>
			<div id="message" class="error">
				<p><?php _e( 'Please install WooCommerce and active. WooCommerce Boost Sales is going to working.', 'woocommerce-boost-sales' ); ?></p>
			</div>
			<?php
		}
	}


	/**
	 * When active plugin Function will be call
	 */
	public function install() {
		global $wp_version;
		if ( version_compare( $wp_version, "2.9", "<" ) ) {
			deactivate_plugins( basename( __FILE__ ) ); // Deactivate our plugin
			wp_die( "This plugin requires WordPress version 2.9 or higher." );
		}
		$json_data = 'a:18:{s:6:"coupon";s:4:"1779";s:15:"coupon_position";s:1:"1";s:19:"text_color_discount";s:7:"#4f5154";s:13:"process_color";s:7:"#d1e0d3";s:24:"process_background_color";s:7:"#5189bb";s:11:"coupon_desc";s:76:"SWEET! Add more products and get {discount_amount} off on your entire order!";s:15:"enable_thankyou";s:1:"1";s:16:"message_congrats";s:103:"You have successfully reached the goal, and a {discount_amount} discount will be applied to your order.";s:17:"text_btn_checkout";s:12:"Checkout now";s:18:"btn_checkout_color";s:7:"#ffffff";s:21:"btn_checkout_bg_color";s:7:"#2cd664";s:15:"exclude_product";a:1:{i:0;s:2:"60";}s:12:"item_per_row";s:1:"4";s:5:"limit";s:1:"8";s:16:"cart_page_option";s:1:"2";s:21:"crosssell_description";s:41:"Hang on! We have this offer just for you!";s:10:"init_delay";s:1:"3";s:13:"icon_position";s:1:"0";}';
		if ( ! get_option( 'wboostsales_params', '' ) ) {
			update_option( 'wboostsales_params', json_decode( $json_data, true ) );
		}
	}

	/**
	 * When deactive function will be call
	 */
	public function uninstall() {

	}
}

new VI_WBOOSTSALES();