<?php

/*
Class Name: VI_WBOOSTSALES_Admin_Crosssell
Author: Andy Ha (support@villatheme.com)
Author URI: http://villatheme.com
Copyright 2016 villatheme.com. All rights reserved.
*/

class VI_WBOOSTSALES_Admin_ZCrosssell {

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_filter( 'set-screen-option', array( $this, 'save_screen_options' ), 10, 3 );
		add_action( 'wp_ajax_wbs_search_product_crs', array( $this, 'wbs_search_product_crs' ) );
		add_action( 'wp_ajax_wbs_c_save_product', array( $this, 'wbs_c_save_product' ) );
		add_action( 'wp_ajax_wbs_update_product', array( $this, 'wbs_update_product' ) );
		add_action( 'wp_ajax_wbs_c_remove_product', array( $this, 'wbs_c_remove_product' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 99999 );

		add_action( 'admin_init', array( $this, 'cross_sells_data_update' ), 90 );
	}

	function cross_sells_data_update() {
		$dismiss_opt = get_option( 'dismiss_update_crsells' );
		if ( empty( $dismiss_opt ) || $dismiss_opt != '1' ) {
			$user_id   = get_current_user_id();
			$arg_first = array(
				'post_status'    => 'publish',
				'post_type'      => 'product',
				'posts_per_page' => - 1,
				'meta_query'     => array(
					array(
						'key'     => '_wbs_crosssells',
						'value'   => '',
						'compare' => '!='
					)
				)
			);
			$get_post  = get_posts( $arg_first );

			if ( count( $get_post ) ) {
				$array2 = array();
				foreach ( $get_post as $list_id ) {
					$get_pmt_cross     = get_post_meta( $list_id->ID, '_wbs_crosssells' );
					$array2['id'][]    = $list_id->ID;
					$array2['value'][] = $get_pmt_cross[0][0];
				}

				$duplicate_element = array_unique( array_diff_assoc( $array2['value'], array_unique( $array2['value'] ) ) );
				if ( count( $duplicate_element ) ) {
					foreach ( $duplicate_element as $key_sep => $separate_e ) {
						$dupp = $array2['id'][ $key_sep ];
						$prda = wc_get_product( $separate_e );

						if ( get_post_status( $separate_e ) == 'publish' ) {
							$meta_to_exclude = array_filter( apply_filters( 'woocommerce_duplicate_product_exclude_meta', array() ) );
							$duplicate       = clone $prda;
							$duplicate->set_id( 0 );
							$duplicate->set_name( sprintf( __( '%s (Copy)', 'woocommerce' ), $duplicate->get_name() ) );
							$duplicate->set_total_sales( 0 );
							if ( '' !== $prda->get_sku( 'edit' ) ) {
								$duplicate->set_sku( wc_product_generate_unique_sku( 0, $prda->get_sku( 'edit' ) ) );
							}
							$duplicate->set_status( 'publish' );
							$duplicate->set_date_created( null );
							$duplicate->set_slug( '' );
							$duplicate->set_rating_counts( 0 );
							$duplicate->set_average_rating( 0 );
							$duplicate->set_review_count( 0 );

							foreach ( $meta_to_exclude as $meta_key ) {
								$duplicate->delete_meta_data( $meta_key );
							}

							// This action can be used to modify the object further before it is created - it will be passed by reference. @since 3.0
							do_action( 'woocommerce_product_duplicate_before_save', $duplicate, $prda );

							// Save parent product.
							$duplicate->save();
							$dup_id = $duplicate->get_id();
							update_post_meta( $dupp, '_wbs_crosssells', array( $dup_id ) );
							add_user_meta( $user_id, 'dismiss_cross_sells_data_update', 'true', true );

						}
					}
				}
			}
			update_option( 'dismiss_update_crsells', 1 );
		}
	}

	/**
	 * Get all cross sells product chosen
	 */
	public function get_crs_select( $p_id ) {
		global $wpdb;
		$prds = wc_get_product( $p_id );
		if ( $prds->has_child() && $prds->get_type() == 'variable' ) {
			$children = $prds->get_children();
			if ( count( $children ) ) {
				foreach ( $children as $child ) {
					$sql_parent    = $wpdb->prepare( "SELECT post_id FROM " . $wpdb->prefix . "postmeta WHERE meta_key = '_wbs_wcpb_bundle_data' AND meta_value LIKE '%s'", '%' . $child . '%' );
					$result_parent = $wpdb->get_results( $sql_parent, OBJECT );

					if ( ! $result_parent ) {
						continue;
					} else {
						foreach ( $result_parent as $post_id ) {
							$array_pid  = (array) $post_id;
							$get_status = get_post_status( $array_pid['post_id'] );
							$get_pid    = get_post_meta( $array_pid['post_id'], '_wbs_wcpb_bundle_data' );

							if ( is_array( $get_pid ) && $get_status == 'publish' ) {
								if ( count( array_filter( $get_pid ) ) ) {
									foreach ( $get_pid as $items ) {
										foreach ( $items as $item ) {
											if ( in_array( $p_id, $item ) ) {
												return $array_pid['post_id'];
											}
										}
									}
								}
							} else {
								return 0;
							}
						}
					}
				}
			}
		}

		$sql    = $wpdb->prepare( "SELECT post_id FROM " . $wpdb->prefix . "postmeta WHERE meta_key = '_wbs_wcpb_bundle_data' AND meta_value LIKE '%s'", '%' . $p_id . '%' );
		$result = $wpdb->get_results( $sql, OBJECT );
		if ( ! $result ) {
			return 0;
		} else {
			foreach ( $result as $post_id ) {
				$array_pid   = (array) $post_id;
				$get_status2 = get_post_status( $array_pid['post_id'] );
				$get_pid     = get_post_meta( $array_pid['post_id'], '_wbs_wcpb_bundle_data' );

				if ( is_array( $get_pid ) && $get_status2 == 'publish' ) {
					if ( count( array_filter( $get_pid ) ) ) {
						foreach ( $get_pid as $items ) {
							foreach ( $items as $item ) {
								if ( in_array( $p_id, $item ) ) {
									return $array_pid['post_id'];
								}
							}
						}
					}
				} else {
					return 0;
				}
			}
		}

	}

	/**
	 * Get product bundle from id
	 */
	protected function get_product_bundle_from_id( $p_id ) {
		$array_wbs_bundle = array( $p_id );
		$arg_first        = array(
			'post_status'    => 'publish',
			'post_type'      => 'product',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				array(
					'key'     => '_wbs_wcpb_bundle_data',
					'value'   => '',
					'compare' => '!='
				)
			)
		);
		$post_alls        = get_posts( $arg_first );
		if ( count( $post_alls ) ) {
			foreach ( $post_alls as $post_all ) {
				$meta_a = get_post_meta( $post_all->ID, '_wbs_wcpb_bundle_data' );
				if ( count( $meta_a ) ) {
					foreach ( $meta_a as $meta_b ) {
						foreach ( $meta_b as $all_items ) {
							$array_wbs_bundle[] = $all_items['product_id'];
						}
					}
				}
			}
		}

		return $array_wbs_bundle;
	}

	/**
	 * Select 2 Search ajax
	 */
	public function wbs_search_product_crs() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		ob_start();

		$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_STRING );
		$p_id    = filter_input( INPUT_GET, 'p_id', FILTER_SANITIZE_STRING );

		if ( empty( $keyword ) ) {
			die();
		}

		$arg            = array(
			'post_status'    => 'publish',
			'post_type'      => 'product',
			'posts_per_page' => - 1,
			's'              => $keyword,
			'post__not_in'   => array( $p_id ),
			'tax_query'      => array(
				array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => 'wbs_bundle',
					'operator' => 'NOT IN'
				),
			),
			'meta_query'     => array(
				array(
					'key'   => '_stock_status',
					'value' => 'instock'
				)
			)
		);
		$the_query      = new WP_Query( $arg );
		$found_products = array();
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$get_wc_product = wc_get_product( get_the_ID() );
				if ( $get_wc_product->has_child() && $get_wc_product->get_type() == 'variable' ) {
					$product           = array(
						'id'   => get_the_ID(),
						'text' => get_the_title() . ' (#' . get_the_ID() . ') (#PARENT) '
					);
					$found_products[]  = $product;
					$children_variable = $get_wc_product->get_children();
					foreach ( $children_variable as $child ) {
						$product_child    = wc_get_product( $child );
						$product          = array(
							'id'   => $child,
							'text' => $product_child->get_name() . ' (#' . $child . ')'
						);
						$found_products[] = $product;
					}
				} else {
					$product          = array(
						'id'   => get_the_ID(),
						'text' => get_the_title() . ' (#' . get_the_ID() . ')'
					);
					$found_products[] = $product;
				}
			}
		}
		// Reset Post Data
		wp_reset_postdata();
		wp_send_json( $found_products );
		die;
	}

	/**
	 * Remove all Cross-sell
	 */
	public function wbs_c_remove_product() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		ob_start();
		$p_id              = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_STRING );
		$product_bundle_id = filter_input( INPUT_POST, 'product_bundle_id', FILTER_SANITIZE_STRING );
		$msg               = array();

		if ( empty( $p_id ) ) {
			die();
		}

		if ( $product_bundle_id ) {
			delete_post_meta( $product_bundle_id, '_wbs_wcpb_bundle_data' );
			$post1 = array( 'ID' => $product_bundle_id, 'post_status' => 'trash' );
			wp_update_post( $post1 );
		}

		$msg['check'] = 'done';
		ob_clean();
		echo json_encode( $msg );
		die;
	}

	/**
	 * Save cross sells
	 */
	public function wbs_c_save_product() {
		global $wp_error;
		$user         = wp_get_current_user();
		$current_user = $user->get( 'ID' );

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		ob_start();

		$p_id              = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_STRING );
		$c_id              = filter_input( INPUT_POST, 'c_id', FILTER_SANITIZE_STRING );
		$product_bundle_id = filter_input( INPUT_POST, 'product_bundle_id', FILTER_SANITIZE_STRING );
		$msg               = array();

		if ( empty( $p_id ) ) {
			die;
		}

		if ( empty( $c_id ) || $c_id == 'null' ) {
			/*delete_post_meta( $product_bundle_id, '_wbs_wcpb_bundle_data' );
			$post3 = array( 'ID' => $product_bundle_id, 'post_status' => 'draft' );
			wp_update_post( $post3 );*/
			$msg['check'] = 'wrong';
		}
		$c_id = array_filter( explode( ',', $c_id ) );
		if ( count( $c_id ) ) {
			if ( empty( $product_bundle_id ) ) {
				$product_name = 'Product Bundle of #' . $p_id;
				if ( ! array_search( $p_id, $c_id ) ) {
					$c_id[] = $p_id;
				}
				$arry_pa    = array();
				$get_prices = array();
				foreach ( $c_id as $absr => $pa_id ) {
					$indiv_product = wc_get_product( $pa_id );
					$get_prices[]  = $indiv_product->get_price();
					$step          = $absr + 1;
					$arry          = array(
						'bundle_order' => $step,
						'product_id'   => $pa_id,
						'bp_quantity'  => 1
					);
					$arry_pa[]     = $arry;
				}
				$total_price     = array_sum( $get_prices );
				$post_individual = array(
					'post_author'  => $current_user,
					'post_content' => '',
					'post_status'  => 'publish',
					'post_title'   => $product_name,
					'post_parent'  => '',
					'post_type'    => "product",
				);

				$post_id = wp_insert_post( $post_individual, $wp_error );
				if ( $post_id ) {
					$attach_id = get_post_meta( $p_id, "_thumbnail_id", true );
					add_post_meta( $post_id, '_thumbnail_id', $attach_id );
				}
				wp_set_object_terms( $post_id, 'wbs_bundle', 'product_type' );

				update_post_meta( $post_id, '_visibility', 'hidden' );
				update_post_meta( $post_id, '_stock_status', 'instock' );
				update_post_meta( $post_id, 'total_sales', '0' );
				update_post_meta( $post_id, '_downloadable', 'no' );
				update_post_meta( $post_id, '_virtual', 'yes' );
				update_post_meta( $post_id, '_regular_price', $total_price );
				update_post_meta( $post_id, '_sale_price', '' );
				update_post_meta( $post_id, '_purchase_note', '' );
				update_post_meta( $post_id, '_featured', 'no' );
				update_post_meta( $post_id, '_weight', '' );
				update_post_meta( $post_id, '_length', '' );
				update_post_meta( $post_id, '_width', '' );
				update_post_meta( $post_id, '_height', '' );
				update_post_meta( $post_id, '_sku', '' );
				update_post_meta( $post_id, '_product_attributes', array() );
				update_post_meta( $post_id, '_sale_price_dates_from', '' );
				update_post_meta( $post_id, '_sale_price_dates_to', '' );
				update_post_meta( $post_id, '_price', $total_price );
				update_post_meta( $post_id, '_sold_individually', '' );
				update_post_meta( $post_id, '_manage_stock', 'no' );
				update_post_meta( $post_id, '_backorders', 'no' );
				update_post_meta( $post_id, '_stock', '' );


				$product_new = wc_get_product( $post_id );
				$terms       = array( 'exclude-from-search', 'exclude-from-catalog' );

				if ( ! is_wp_error( wp_set_post_terms( $post_id, $terms, 'product_visibility', false ) ) ) {
					delete_transient( 'wc_featured_products' );
					do_action( 'woocommerce_product_set_visibility', $post_id, $product_new->get_catalog_visibility() );
				}

				if ( count( $arry_pa ) ) {
					update_post_meta( $post_id, '_wbs_wcpb_bundle_data', $arry_pa );
					//update_post_meta( $p_id, '_wbs_cross_sell_of', $post_id );
					update_post_meta( $p_id, '_wbs_crosssells', array( $post_id ) );
				}
				$msg['check'] = 'done';
				$test         = get_post_meta( '1419', '_wbs_wcpb_bundle_data' );
			} else {
				if ( ! array_search( $p_id, $c_id ) ) {
					$c_id[] = $p_id;
				}
				$arry_pa = array();
				foreach ( $c_id as $absr => $pa_id ) {
					$step      = $absr + 1;
					$arry      = array(
						'bundle_order' => $step,
						'product_id'   => $pa_id,
						'bp_quantity'  => 1
					);
					$arry_pa[] = $arry;
				}
				if ( count( $arry_pa ) ) {
					$post2 = array( 'ID' => $product_bundle_id, 'post_status' => 'publish' );
					wp_update_post( $post2 );
					update_post_meta( $product_bundle_id, '_wbs_wcpb_bundle_data', $arry_pa );
					update_post_meta( $p_id, '_wbs_crosssells', array( $product_bundle_id ) );
				}
				$msg['check'] = 'done';
			}

		} else {
			$msg['check'] = 'error';
		}
		ob_clean();

		echo json_encode( $msg );
		die;
	}

	/**
	 * Update product bundle
	 */
	public function wbs_update_product() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		ob_start();

		$p_id  = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_STRING );
		$title = filter_input( INPUT_POST, 'title', FILTER_SANITIZE_STRING );
		$price = filter_input( INPUT_POST, 'price', FILTER_SANITIZE_STRING );

		if ( empty( $p_id ) ) {
			die;
		}

		$post_bundle = array(
			'ID'         => $p_id,
			'post_title' => $title
		);

		wp_update_post( $post_bundle, true );

		$meta_price = get_post_meta( $p_id, '_sale_price', true );
		if ( trim( $meta_price ) ) {
			update_post_meta( $p_id, '_sale_price', $price );
		} else {
			update_post_meta( $p_id, '_regular_price', $price );
		}
		update_post_meta( $p_id, '_price', $price );

		$msg = array();
		if ( ! is_wp_error( $p_id ) ) {
			$msg['check'] = 'done';
		} else {
			$msg['check'] = 'wrong';
			$errors       = $p_id->get_error_messages();
			foreach ( $errors as $error ) {
				$msg['detail_err'] = $error;
			}
		}

		ob_clean();
		echo json_encode( $msg );
		die;
	}

	/**
	 * Init scripts
	 */
	public function enqueue_scripts() {
		$page = isset( $_REQUEST['page'] ) ? $_REQUEST['page'] : '';
		if ( $page == 'woocommerce-boost-sales-crosssell' ) {
			global $wp_scripts, $wp_styles;
			$scripts = $wp_scripts->registered;

			foreach ( $scripts as $k => $script ) {
				preg_match( '/select2/i', $k, $result );
				if ( count( array_filter( $result ) ) ) {
					unset( $wp_scripts->registered[ $k ] );
					wp_dequeue_script( $script->handle );
				}
			}

			$styles = $wp_styles->registered;
			foreach ( $styles as $k => $style ) {
				preg_match( '/woocommerce/i', $k, $result );
				if ( count( array_filter( $result ) ) ) {
					unset( $wp_styles->registered[ $k ] );
					wp_dequeue_style( $k );
				}
			}
			wp_enqueue_style( 'select2', VI_WBOOSTSALES_CSS . 'select2.min.css' );
			wp_enqueue_script( 'select2-v4', VI_WBOOSTSALES_JS . 'select2.js', array( 'jquery' ), '4.0.3' );
			wp_enqueue_script( 'jquery-ui-tooltip' );
			wp_enqueue_script( 'woocommerce-boost-sales-crosssell-admin', VI_WBOOSTSALES_JS . 'woocommerce-boost-sales-crosssell-admin.js', array( 'jquery' ), VI_WBOOSTSALES_VERSION );
		}
	}

	/**
	 * Add Menu
	 */
	public function admin_menu() {
		$send_now = add_submenu_page(
			'woocommerce-boost-sales',
			esc_html__( 'Cross-Sells', 'woocommerce-boost-sales' ),
			esc_html__( 'Cross-Sells', 'woocommerce-boost-sales' ),
			'manage_options',
			'woocommerce-boost-sales-crosssell',
			array( $this, 'page_callback' )
		);
		add_action( "load-$send_now", array( $this, 'screen_options_page' ) );
	}

	/**
	 * Save options from screen options
	 *
	 * @param $status
	 * @param $option
	 * @param $value
	 *
	 * @return mixed
	 */
	public function save_screen_options( $status, $option, $value ) {
		if ( 'wbsc_per_page' == $option ) {
			return $value;
		}

		return $status;
	}

	/**
	 * Add Screen Options
	 */
	public function screen_options_page() {

		$option = 'per_page';

		$args = array(
			'label'   => esc_html__( 'Number of items per page', 'wp-admin' ),
			'default' => 30,
			'option'  => 'wbsc_per_page'
		);

		add_screen_option( $option, $args );
	}

	/**
	 * Menu page call back
	 */
	public function page_callback() {
		global $wpdb;
		$user     = get_current_user_id();
		$screen   = get_current_screen();
		$option   = $screen->get_option( 'per_page', 'option' );
		$per_page = get_user_meta( $user, $option, true );

		if ( empty ( $per_page ) || $per_page < 1 ) {
			$per_page = $screen->get_option( 'per_page', 'default' );
		}
		$paged = isset( $_GET['paged'] ) ? $_GET['paged'] : 1;

		?>
		<div class="wrap">
			<h2><?php esc_html_e( 'CROSS-SELLS', 'woocommerce-boost-sales' ) ?></h2>

			<p class="description"><?php esc_html_e( 'Cross-sells are products that instead of you buy this product you can buy bundle products contain this product, based on the current product. **For example, if you are selling a laptop, cross-sells might be a protective case or stickers or a special adapter.**', 'woocommerce-boost-sales' ) ?>
				<br>
				<a href="javascript:void(0)" id="wbs_different_up-cross-sell" title="" data-wbs_up_crosssell="http://new2new.com/envato/woocommerce-boost-sales/product-cross-sells.gif"><?php esc_html_e( 'What is CROSS-SELLS?', 'woocommerce-boost-sales' ); ?></a>
			</p>
			<form action="<?php echo esc_url( admin_url( '?page=woocommerce-boost-sales-upsell' ) ) ?>" method="post">
				<?php wp_nonce_field( 'wp_send_email_send_email', '_wsm_nonce' ) ?>
				<?php
				$sql    = "SELECT DISTINCT(p.ID) FROM " . $wpdb->prefix . "posts as p LEFT JOIN " . $wpdb->prefix . "term_relationships as tr ON tr.object_id = p.ID LEFT JOIN " . $wpdb->prefix . "terms as t ON t.term_id = tr.term_taxonomy_id WHERE t.slug='wbs_bundle'";
				$result = $wpdb->get_col( $sql );
				$args   = array(
					'post_type'      => 'product',
					'post_status'    => 'publish',
					'order'          => 'DESC',
					'orderby'        => 'ID',
					'posts_per_page' => $per_page,
					'paged'          => $paged,
					'post__not_in'   => $result,
					'meta_query'     => array(
						array(
							'key'   => '_stock_status',
							'value' => 'instock'
						)
					)
				);

				$the_query = new WP_Query( $args ); ?>
				<div class="list-products">
					<table class="wp-list-table widefat fixed striped">
						<thead>
						<tr>
							<th scope="col" id="product-name" class="manage-column column-product-name column-primary sortable desc">
								<a href="#"><span><?php esc_html_e( 'Product Name', 'woocommerce-boost-sales' ) ?></span></a>
							</th>
							<th scope="col" id="up-sells" class="manage-column column-up-sells sortable desc">
								<span><?php esc_html_e( 'Cross-sells', 'woocommerce-boost-sales' ) ?></span>
							</th>
							<th scope="col" id="actions" class="manage-column column-actions sortable desc">
								<?php esc_html_e( 'Actions', 'woocommerce-boost-sales' ) ?>
							</th>
						</tr>
						</thead>
						<?php if ( $the_query->have_posts() ) { ?>
							<tbody id="the-list" data-wp-lists="list:product">
							<?php
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								$p_id = get_the_ID(); ?>
								<tr id="product-<?php echo $p_id ?>">
									<td class="product column-product has-row-actions column-primary" data-colname="product-name">
										<strong><a href="<?php echo esc_url( 'post.php?action=edit&post=' . $p_id ) ?>"><?php echo the_title( '', '', '' ) ?></a></strong>
									</td>
									<td data-id="<?php echo $p_id ?>" class="name column-cross-sells" data-colname="<?php esc_attr_e( 'Cross sells', 'woocommerce-boost-sales' ) ?>">
										<?php $post_meta_of = get_post_meta( $p_id, '_wbs_crosssells', true ); ?>
										<input type="hidden" name="_wbs_cross_sell_of" value="<?php
										if ( ! empty( $post_meta_of ) && count( $post_meta_of ) && get_post_status( $post_meta_of[0] ) !== false ) {
											echo $post_meta_of[0];
										} else {
											echo '';
										}
										?>">

										<select multiple="multiple" name="_wbs_cross_sell"
												class="product-search-crs u-product-<?php echo get_the_ID() ?>"
												placeholder="<?php esc_attr_e( 'Please select products', 'woocommerce-boost-sales' ) ?>">
											<?php
											if ( ! empty( $post_meta_of ) && count( $post_meta_of ) && get_post_status( $post_meta_of[0] ) == 'publish' ) {
												$product_chosen = get_post_meta( $post_meta_of[0], '_wbs_wcpb_bundle_data', true );
												if ( ! empty( $product_chosen ) && count( $product_chosen ) ) {
													foreach ( $product_chosen as $product_chose ) {
														if ( $product_chose['product_id'] != null && $p_id != $product_chose['product_id'] ) {
															$dt_product = wc_get_product( $product_chose['product_id'] );
															$parent     = '';
															if ( $dt_product->has_child() && $dt_product->get_type() == 'variable' ) {
																$parent = ' (#PARENT)';
															}
															if ( get_post_status( $product_chose['product_id'] ) == 'publish' ) {
																?>
																<option selected="selected" value="<?php echo esc_attr( $product_chose['product_id'] ); ?>">
																	<?php echo esc_html( $dt_product->get_name() . ' (#' . $product_chose['product_id'] . ')' . $parent ) ?>
																</option>
																<?php
															}
														}
													}
												}
											}
											?>
										</select>
										<?php
										if ( ! empty( $post_meta_of ) && count( $post_meta_of ) && get_post_status( $post_meta_of[0] ) == 'publish' ) {
											$detail_bundle = wc_get_product( $post_meta_of[0] ); ?>
											<br>
											<a target="_blank" href="<?php echo get_edit_post_link( $post_meta_of[0] ); ?>" class="button-edit"><?php esc_attr_e( 'Edit product bundle', 'woocommerce-boost-sales' ) ?></a> |
											<span class="button-edit button-quick-edit"><?php esc_attr_e( 'Quick Edit product bundle', 'woocommerce-boost-sales' ) ?></span>
											<div class="inline-edit-row" data-product_bundle_id="<?php echo $post_meta_of[0]; ?>">
												<fieldset class="">
													<legend class="inline-edit-legend">Quick Edit</legend>
													<div class="inline-edit-col">
														<label>
															<span class="title"><?php esc_html_e( 'Title', 'woocommerce-boost-sales' ) ?></span>
															<span class="input-text-wrap"><input type="text" name="post_bundle_title" class="ptitle" value="<?php echo $detail_bundle->get_title(); ?>"></span>
														</label>

														<label>
															<span class="title"><?php esc_html_e( 'Price', 'woocommerce-boost-sales' ) ?></span>
															<span class="input-text-wrap"><input class="text wc_input_price" type="text" name="product_bundle_regular_price" title="<?php esc_html_e( 'Please enter the number of price', 'woocommerce-boost-sales' ) ?>" value="<?php echo $detail_bundle->get_price(); ?>"></span>
														</label>

													</div>
												</fieldset>
												<p class="submit inline-edit-save">
													<?php wp_nonce_field( 'wp_update_bundle_product', '_wbs_update_nonce' ) ?>
													<button type="button" class="button cancel alignleft button-cancel">Cancel</button>
													<button type="button" class="button button-primary save alignright button-update">Update</button>
													<span class="spinner"></span>
												</p>
											</div>
											<?php
										}
										?>
									</td>
									<td class="email column-action product-action-<?php echo esc_attr( $p_id ); ?>" data-colname="<?php esc_attr_e( 'Actions', 'woocommerce-boost-sales' ) ?>" data-id="<?php echo esc_attr( $p_id ); ?>">
										<a class="button" target="_blank" href="<?php echo the_permalink( $p_id ) ?>"><?php esc_attr_e( 'View', 'woocommerce-boost-sales' ) ?></a>
										<span class="button button-save"><?php esc_attr_e( 'Save', 'woocommerce-boost-sales' ) ?></span>
										<span class="button button-remove"><?php esc_attr_e( 'Remove all', 'woocommerce-boost-sales' ) ?></span>
									</td>
								</tr>
							<?php } ?>
							</tbody>
						<?php }
						// Reset Post Data
						wp_reset_postdata();
						?>
					</table>
					<div class="tablenav ">
						<div class="tablenav-pages">
							<?php
							$count      = wp_count_posts( 'product' );
							$count      = $count->publish;
							$total_page = ( $count % $per_page ) == 0 ? intval( $count / $per_page ) : intval( $count / $per_page ) + 1;

							/*Previous button*/
							if ( $per_page * $paged > $per_page ) {
								$p_paged = $paged - 1;
							} else {
								$p_paged = 0;
							}
							if ( $p_paged ) {
								$p_url = add_query_arg( array(
									'page'  => 'woocommerce-boost-sales-crosssell',
									'paged' => $p_paged
								), admin_url( 'admin.php' ) ); ?>
								<a class="prev-page" href="<?php echo esc_url( $p_url ) ?>"><span class="screen-reader-text"><?php esc_html_e( 'Previous Page', 'woocommerce-boost-sales' ) ?></span><span aria-hidden="true">‹</span></a>
							<?php } else { ?>
								<span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
							<?php } ?>
							<span class="screen-reader-text"><?php esc_html_e( 'Current Page', 'woocommerce-boost-sales' ) ?></span>
							<span id="table-paging" class="paging-input">
								<span class="tablenav-paging-text"><?php echo esc_html( $paged ) ?> of <span class="total-pages"><?php echo esc_html( $total_page ) ?></span></span>
							</span>
							<?php /*Next button*/
							if ( $per_page * $paged < $count ) {
								$n_paged = $paged + 1;
							} else {
								$n_paged = 0;
							}
							if ( $n_paged ) {
								$n_url = add_query_arg( array(
									'page'  => 'woocommerce-boost-sales-crosssell',
									'paged' => $n_paged
								), admin_url( 'admin.php' ) ); ?>
								<a class="next-page" href="<?php echo esc_url( $n_url ) ?>"><span class="screen-reader-text"><?php esc_html_e( 'Next Page', 'woocommerce-boost-sales' ) ?></span><span aria-hidden="true">›</span></a>
							<?php } else { ?>
								<span class="tablenav-pages-navspan" aria-hidden="true">›</span>
							<?php } ?>
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php }
}