<?php

/*
Class Name: WP_SM_Admin_Settings
Author: Andy Ha (support@villatheme.com)
Author URI: http://villatheme.com
Copyright 2016 villatheme.com. All rights reserved.
*/

class VI_WBOOSTSALES_Admin_Settings {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'menu_page' ) );
		add_filter( 'wbs_data_settings', array( $this, 'set_options' ) );
	}

	/**
	 * Get list shortcode
	 * @return array
	 */
	public static function page_callback() { ?>
		<div class="wrap woocommerce-boost-sales">
			<h2><?php esc_attr_e( 'WooCommerce Boost Sales Settings', ' woocommerce-boost-sales' ) ?></h2>
			<?php
			do_action( 'villatheme_setting_html' );
			?>

			<div id="vi_wbs_getsupport" class="villatheme-support_247">
				<h4><?php esc_attr_e( 'Need help ?', 'woo-boost-sales' ) ?></h4>

				<p><?php esc_html_e( 'Please be one of the special few to support this plugin with a gift or review. Free download and support 24/7.', 'woo-boost-sales' ); ?>
					<b><?php esc_html_e( 'Thank you!', 'woo-boost-sales' ); ?></b>
				</p>

				<p>
					<?php
					echo '<a href="https://goo.gl/7pccyl" class="button" target="_blank">' . esc_html( 'Read Document' ) . '</a>';
					echo '<a href="https://goo.gl/fnf4Mi" class="button" target="_blank">' . esc_html( 'Support Forum' ) . '</a>';
					echo '<a href="https://goo.gl/GU7Q0U" class="button" target="_blank">' . esc_html( 'Add Your Review' ) . '</a>';
					?>
				</p>

				<p>
					<i><?php esc_html_e( 'Visit ', 'woo-boost-sales' ); ?>
						<a href="https://villatheme.com/" target="_blank"><?php echo esc_html( 'villatheme.com' ) ?></a>
						<?php esc_html_e( 'and follow us on ', 'woo-boost-sales' ) ?>
						<a href="https://www.facebook.com/villatheme/" target="_blank"><?php esc_html_e( 'Facebook', 'woo-boost-sales' ); ?></a>
						<?php esc_html_e( 'and ', 'woo-boost-sales' ) ?>
						<a href="https://twitter.com/villatheme/" target="_blank"><?php esc_html_e( 'Twitter', 'woo-boost-sales' ); ?></a>
					</i>
				</p>
			</div>
		</div>
	<?php }

	/**
	 * Get list option
	 * @return array
	 */
	public function set_options( $data ) {

		$data['general']  =
			array(
				'title'  => esc_html__( 'General', 'woocommerce-boost-sales' ),
				'active' => true,
				'fields' => array(
					array(
						'name'        => 'enable',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Enable', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'enable_mobile',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Enable Mobile', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					)
				)
			);
		$data['discount'] =
			array(
				'title'  => esc_html__( 'Discount', 'woocommerce-boost-sales' ),
				'fields' => array(
					array(
						'name'        => 'enable_discount',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Enable', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'coupon',
						'type'        => 'select',
						'value'       => 0,
						'description' => esc_html__( 'If 2 coupons name coincide - the coupon latest will eventually be used', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Select Coupon', 'woocommerce-boost-sales' ),
						'class'       => 'vi-wbs-dcoupon',
						'options'     => $this->get_list_coupons()
					),
					array(
						'name'        => 'coupon_position',
						'type'        => 'select',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Select Position', 'woocommerce-boost-sales' ),
						'options'     => array(
							'0' => esc_html__( 'Top', 'woocommerce-boost-sales' ),
							'1' => esc_html__( 'Bottom', 'woocommerce-boost-sales' )
						)
					),
					array(
						'name'        => 'text_color_discount',
						'type'        => 'color-picker',
						'value'       => '#4f5154',
						'description' => esc_html__( 'Color for text of process bar.', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Text color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'  => 'process_color',
						'type'  => 'color-picker',
						'value' => '#d1e0d3',
						'label' => esc_html__( 'Process bar main color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'process_background_color',
						'type'        => 'color-picker',
						'value'       => '#5189bb',
						'description' => esc_html__( 'Color of main process bar.', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Process bar background color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'coupon_desc',
						'type'        => 'textarea',
						'value'       => esc_html__( 'SWEET! Add more products and get {discount_amount} off on your entire order!', 'woocommerce-boost-sales' ),
						'description' => esc_html__( '{discount_amount} - The number of discount', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Description', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'enable_thankyou',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => esc_html__( 'Congrats when get coupon', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Thank You', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'message_congrats',
						'type'        => 'textarea',
						'value'       => esc_html__( 'You have successfully reached the goal, and a {discount_amount} discount will be applied to your order.', 'woocommerce-boost-sales' ),
						'description' => esc_html__( '{discount_amount} - The number of discount', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Message congratulation', 'woocommerce-boost-sales' ),
						'class'       => 'wbs-message_congrats'
					),
					array(
						'name'  => 'text_btn_checkout',
						'type'  => 'text',
						'value' => esc_html__( 'Checkout now', 'woocommerce-boost-sales' ),
						'label' => esc_html__( 'Text button checkout', 'woocommerce-boost-sales' ),
					),
					array(
						'name'  => 'btn_checkout_color',
						'type'  => 'color-picker',
						'value' => '#ffffff',
						'label' => esc_html__( 'Button text color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'  => 'btn_checkout_bg_color',
						'type'  => 'color-picker',
						'value' => '#2cd664',
						'label' => esc_html__( 'Button background color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'    => 'enable_checkout',
						'type'    => 'checkbox',
						'value'   => 0,
						'label'   => esc_html__( 'Auto redirect checkout', 'woocommerce-boost-sales' ),
						'options' => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'  => 'redirect_after_second',
						'type'  => 'number',
						'value' => 5,
						'label' => esc_html__( 'Redirect after second', 'woocommerce-boost-sales' ),
						'class' => 'wbs-enable_checkout',
					),


				)
			);
		$data['upsell']   =
			array(
				'title'  => esc_html__( 'Upsell', 'woocommerce-boost-sales' ),
				'fields' => array(
					array(
						'name'        => 'hide_on_single_product_page',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Hide on Single Product Page', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'show_if_empty_upsells',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Show popup when empty up-sells product', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'show_with_category',
						'type'        => 'checkbox',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Show all products in category', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => 'Yes',
						)
					),
					array(
						'name'        => 'exclude_product',
						'type'        => 'select2_ajax',
						'value'       => '',
						'description' => '',
						'label'       => esc_html__( 'Exclude product', 'woocommerce-boost-sales' ),
						'class'       => 'search wbs_exclude_product',
						'multiple'    => 'multiple'
					),
					array(
						'name'  => 'item_per_row',
						'type'  => 'number',
						'value' => '4',
						'label' => esc_html__( 'Item per row', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'limit',
						'type'        => 'number',
						'value'       => '8',
						'label'       => esc_html__( 'Item per row', 'woocommerce-boost-sales' ),
						'description' => esc_html__( 'Maximum upsell per product', 'woocommerce-boost-sales' ),
					),
					array(
						'name'    => 'select_template',
						'type'    => 'radio',
						'value'   => '1',
						'label'   => esc_html__( 'Popup style', 'woocommerce-boost-sales' ),
						'class'   => 'wbs_template_upsell',
						'options' => array(
							'1' => $this->get_url_template( 'upsell-template1.png' ),
							'2' => $this->get_url_template( 'upsell-template2.png' )
						)
					),
					array(
						'name'        => 'message_bought',
						'type'        => 'textarea',
						'value'       => esc_html__( 'Frequently bought with {name_product}', 'woocommerce-boost-sales' ),
						'description' => esc_html__( '{name_product} - The name of product purchased', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Message in popup', 'woocommerce-boost-sales' ),
					),
				)
			);

		$data['crosssell'] =
			array(
				'title'  => esc_html__( 'Cross sell', 'woocommerce-boost-sales' ),
				'fields' => array(
					array(
						'name'    => 'crosssell_enable',
						'type'    => 'checkbox',
						'value'   => 0,
						'label'   => esc_html__( 'Enable', 'woocommerce-boost-sales' ),
						'options' => array(
							'1' => 'Yes',
						),
					),
					array(
						'name'    => 'hide_cross_sell_archive',
						'type'    => 'checkbox',
						'value'   => 0,
						'label'   => esc_html__( 'Hide on archive page', 'woocommerce-boost-sales' ),
						'options' => array(
							'1' => 'Yes',
						),
					),
					array(
						'name'    => 'enable_cart_page',
						'type'    => 'checkbox',
						'value'   => 0,
						'label'   => esc_html__( 'Enable on Cart page', 'woocommerce-boost-sales' ),
						'options' => array(
							'1' => 'Yes',
						),
					),
					array(
						'name'        => 'cart_page_option',
						'type'        => 'select',
						'value'       => 1,
						'label'       => esc_html__( 'Product bundle type', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => esc_html__( 'Random bundle', 'woocommerce-boost-sales' ),
							'2' => esc_html__( 'Highest price of bundle', 'woocommerce-boost-sales' )
						),
						'class'       => 'select_product_bundle',
						'description' => esc_html__( 'Select product bundle type on Cart page', 'woocommerce-boost-sales' )
					),
					array(
						'name'    => 'enable_checkout_page',
						'type'    => 'checkbox',
						'value'   => 0,
						'label'   => esc_html__( 'Enable on Checkout page', 'woocommerce-boost-sales' ),
						'options' => array(
							'1' => 'Yes',
						),
					),
					array(
						'name'        => 'checkout_page_option',
						'type'        => 'select',
						'value'       => 1,
						'label'       => esc_html__( 'Product bundle type', 'woocommerce-boost-sales' ),
						'options'     => array(
							'1' => esc_html__( 'Random bundle', 'woocommerce-boost-sales' ),
							'2' => esc_html__( 'Highest price of bundle', 'woocommerce-boost-sales' )
						),
						'class'       => 'select_product_bundle_checkout',
						'description' => esc_html__( 'Select product bundle type on Checkout page', 'woocommerce-boost-sales' )
					),
					array(
						'name'  => 'crosssell_description',
						'type'  => 'textarea',
						'value' => esc_html__( 'Hang on! We have this offer just for you!', 'woocommerce-boost-sales' ),
						'label' => esc_html__( 'Description', 'woocommerce-boost-sales' ),
					),
					array(
						'name'  => 'init_delay',
						'type'  => 'number',
						'value' => '3',
						'label' => esc_html__( 'Init delay', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'icon_position',
						'type'        => 'select',
						'value'       => 0,
						'description' => '',
						'label'       => esc_html__( 'Icon Position', 'woocommerce-boost-sales' ),
						'options'     => array(
							'0' => esc_html__( 'Bottom right', 'woocommerce-boost-sales' ),
							'1' => esc_html__( 'Botton left', 'woocommerce-boost-sales' )
						)
					),
					array(
						'name'        => 'bg_color_cross_sell',
						'type'        => 'color-picker',
						'value'       => '#ffffff',
						'description' => esc_html__( 'Background color for popup cross-sell.', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Background Color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'bg_image_cross_sell',
						'type'        => 'text',
						'value'       => '',
						'description' => esc_html__( 'Copy your path image and paste to here', 'woocommerce-boost-sales' ),
						'label'       => esc_html__( 'Background Image', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'text_color_cross_sell',
						'type'        => 'color-picker',
						'value'       => '#7F7F7F',
						'description' => '',
						'label'       => esc_html__( 'Text Color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'price_text_color_cross_sell',
						'type'        => 'color-picker',
						'value'       => '#5189bb',
						'description' => '',
						'label'       => esc_html__( 'Price Color', 'woocommerce-boost-sales' ),
					),
					array(
						'name'        => 'save_price_text_color_cross_sell',
						'type'        => 'color-picker',
						'value'       => '#000000',
						'description' => '',
						'label'       => esc_html__( 'Save Price Color', 'woocommerce-boost-sales' ),
					)
				)
			);
		$data['update']    =
			array(
				'title'  => esc_html__( 'Update', 'woocommerce-boost-sales' ),
				'fields' => array(
					array(
						'name'  => 'key',
						'type'  => 'text',
						'value' => '',
						'label' => esc_html__( 'Purchased code', 'woocommerce-boost-sales' )
					),
				)
			);

		return $data;
	}

	/**
	 * Get list coupon
	 * @return array
	 */
	protected function get_list_coupons() {
		$args = array(
			'posts_per_page' => 100,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'post_type'      => 'shop_coupon',
			'post_status'    => 'publish',
		);

		$coupons         = get_posts( $args );
		$coupon_array    = array();
		$coupon_arr_id   = array();
		$coupon_arr_name = array();
		foreach ( $coupons as $coupon ) {
			$coupon_id = $coupon->ID;
			array_push( $coupon_arr_id, $coupon_id );
			$coupon_name = $coupon->post_title;
			array_push( $coupon_arr_name, $coupon_name );
			$coupon_array = array_combine( $coupon_arr_id, $coupon_arr_name );
		}

		if ( count( $coupon_array ) ) {
			return $coupon_array;
		} else {
			return array();
		}
	}

	protected function get_url_template( $src ) {
		$imag = '<img src="' . VI_WBOOSTSALES_IMAGES . $src . '" />';
		if ( $src ) {
			return $imag;
		}

		return '';
	}

	/**
	 * Register a custom menu page.
	 */
	public function menu_page() {
		add_menu_page(
			esc_html__( 'WooCommerce Boost Sales', 'woocommerce-boost-sales' ),
			esc_html__( 'Woo Boost Sales', 'woocommerce-boost-sales' ),
			'manage_options',
			'woocommerce-boost-sales',
			array( $this, 'page_callback' ),
			'dashicons-chart-line',
			2
		);

	}
} ?>