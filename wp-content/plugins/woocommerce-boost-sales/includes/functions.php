<?php
/**
 * Function include all files in folder
 *
 * @param $path   Directory address
 * @param $ext    array file extension what will include
 * @param $prefix string Class prefix
 */
if ( ! function_exists( 'vi_include_folder' ) ) {
	function vi_include_folder( $path, $prefix = '', $ext = array( 'php' ) ) {

		/*Include all files in payment folder*/
		if ( ! is_array( $ext ) ) {
			$ext = explode( ',', $ext );
			$ext = array_map( 'trim', $ext );
		}
		$sfiles = scandir( $path );
		foreach ( $sfiles as $sfile ) {
			if ( $sfile != '.' && $sfile != '..' ) {
				if ( is_file( $path . "/" . $sfile ) ) {
					$ext_file  = pathinfo( $path . "/" . $sfile );
					$file_name = $ext_file['filename'];
					if ( $ext_file['extension'] ) {
						if ( in_array( $ext_file['extension'], $ext ) ) {
							$class = preg_replace( '/\W/i', '_', $prefix . ucfirst( $file_name ) );

							if ( ! class_exists( $class ) ) {
								require_once $path . $sfile;
								if ( class_exists( $class ) ) {
									new $class;
								}
							}
						}
					}
				}
			}
		}
	}
}

if ( ! function_exists( 'woocommerce_boost_sales_prefix' ) ) {
	function woocommerce_boost_sales_prefix() {
		$prefix = get_option( '_woocommerce_boost_sales_prefix', date( "Ymd" ) );

		return $prefix . '_products_' . date( "Ymd" );
	}
}

if ( !function_exists( 'wbs_set_prop' ) ) {
	/**
	 *
	 */
	function wbs_set_prop( $object, $arg1, $arg2 = false ) {

		if ( !is_array( $arg1 ) ) {
			$arg1 = array(
				$arg1 => $arg2
			);
		}

		$prop_map   = wbs_return_new_attribute_map();
		$is_wc_data = $object instanceof WC_Data;

		foreach ( $arg1 as $key => $value ) {
			if ( $is_wc_data ) {
				$key = ( array_key_exists( $key, $prop_map ) ) ? $prop_map[ $key ] : $key;

				if ( ( $setter = "set{$key}" ) && method_exists( $object, $setter ) ) {
					$object->$setter( $value );
				} elseif ( ( $setter = "set_{$key}" ) && method_exists( $object, $setter ) ) {
					$object->$setter( $value );
				} else {
					$object->update_meta_data( $key, $value );
				}
			} else {
				$key = ( in_array( $key, $prop_map ) ) ? array_search( $key, $prop_map ) : $key;
				( strpos( $key, '_' ) === 0 ) && $key = substr( $key, 1 );

				if ( wbs_wc_check_post_columns( $key ) ) {
					$object->post->$key = $value;
				} else {
					$object->$key = $value;
				}
			}
		}
	}
}

if ( !function_exists( 'wbs_return_new_attribute_map' ) ) {
	function wbs_return_new_attribute_map() {
		return array(
			'post_parent'                => 'parent_id',
			'post_title'                 => 'name',
			'post_status'                => 'status',
			'post_content'               => 'description',
			'post_excerpt'               => 'short_description',
			/* Orders */
			'paid_date'                  => 'date_paid',
			'_paid_date'                 => '_date_paid',
			'completed_date'             => 'date_completed',
			'_completed_date'            => '_date_completed',
			'_order_date'                => '_date_created',
			'order_date'                 => 'date_created',
			'order_total'                => 'total',
			'customer_user'              => 'customer_id',
			'_customer_user'             => 'customer_id',
			/* Products */
			'visibility'                 => 'catalog_visibility',
			'_visibility'                => '_catalog_visibility',
			'sale_price_dates_from'      => 'date_on_sale_from',
			'_sale_price_dates_from'     => '_date_on_sale_from',
			'sale_price_dates_to'        => 'date_on_sale_to',
			'_sale_price_dates_to'       => '_date_on_sale_to',
			'product_attributes'         => 'attributes',
			'_product_attributes'        => '_attributes',
			/*Coupons*/
			'coupon_amount'              => 'amount',
			'exclude_product_ids'        => 'excluded_product_ids',
			'exclude_product_categories' => 'excluded_product_categories',
			'customer_email'             => 'email_restrictions',
			'expiry_date'                => 'date_expires',
		);
	}
}

if ( !function_exists( 'wbs_wc_check_post_columns' ) ) {
	/**
	 *
	 */
	function wbs_wc_check_post_columns( $key ) {
		$columns = array(
			'post_author',
			'post_date',
			'post_date_gmt',
			'post_content',
			'post_title',
			'post_excerpt',
			'post_status',
			'comment_status',
			'ping_status',
			'post_password',
			'post_name',
			'to_ping',
			'pinged',
			'post_modified',
			'post_modified_gmt',
			'post_content_filtered',
			'post_parent',
			'guid',
			'menu_order',
			'post_type',
			'post_mime_type',
			'comment_count',
		);

		return in_array( $key, $columns );
	}
}

/**
 *
 * @param string $version
 *
 * @return bool
 */
if ( ! function_exists( 'woocommerce_version_check' ) ) {
	function woocommerce_version_check($version = '3.0.0'){
		global $woocommerce;

		if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
			return true;
		}
		return false;
	}
}