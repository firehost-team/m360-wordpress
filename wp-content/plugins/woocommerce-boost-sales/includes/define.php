<?php

define( 'VI_WBOOSTSALES_DIR', WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . "woocommerce-boost-sales" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_ADMIN', VI_WBOOSTSALES_DIR . "admin" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_FRONTEND', VI_WBOOSTSALES_DIR . "frontend" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_LANGUAGES', VI_WBOOSTSALES_DIR . "languages" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_INCLUDES', VI_WBOOSTSALES_DIR . "includes" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_TEMPLATES', VI_WBOOSTSALES_DIR . "templates" . DIRECTORY_SEPARATOR );

define( 'VI_WBOOSTSALES_CSS', WP_PLUGIN_URL . "/woocommerce-boost-sales/css/" );
define( 'VI_WBOOSTSALES_CSS_DIR', VI_WBOOSTSALES_DIR . "css" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_JS', WP_PLUGIN_URL . "/woocommerce-boost-sales/js/" );
define( 'VI_WBOOSTSALES_JS_DIR', VI_WBOOSTSALES_DIR . "js" . DIRECTORY_SEPARATOR );
define( 'VI_WBOOSTSALES_IMAGES', WP_PLUGIN_URL . "/woocommerce-boost-sales/images/" );


/*Include functions file*/
if ( is_file( VI_WBOOSTSALES_INCLUDES . "functions.php" ) ) {
	require_once VI_WBOOSTSALES_INCLUDES . "functions.php";
}
/*Include functions file*/
if ( is_file( VI_WBOOSTSALES_INCLUDES . "mobile_detect.php" ) ) {
	require_once VI_WBOOSTSALES_INCLUDES . "mobile_detect.php";
}
/*Include functions file*/
if ( is_file( VI_WBOOSTSALES_INCLUDES . "fields.php" ) ) {
	require_once VI_WBOOSTSALES_INCLUDES . "fields.php";
}
/*Include functions file*/
if ( is_file( VI_WBOOSTSALES_INCLUDES . "check_update.php" ) ) {
	require_once VI_WBOOSTSALES_INCLUDES . "check_update.php";
}
/*Include functions file*/
if ( is_file( VI_WBOOSTSALES_INCLUDES . "update.php" ) ) {
	require_once VI_WBOOSTSALES_INCLUDES . "update.php";
}
vi_include_folder( VI_WBOOSTSALES_ADMIN, 'VI_WBOOSTSALES_Admin_' );
vi_include_folder( VI_WBOOSTSALES_FRONTEND, 'VI_WBOOSTSALES_Frontend_' );
?>