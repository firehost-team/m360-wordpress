<?php

if ( ! class_exists( 'VillaTheme_Admin_Fields' ) ) {
	define( 'VILLATHEME_FIELD_CSS', VI_WBOOSTSALES_CSS );
	define( 'VILLATHEME_FIELD_JS', VI_WBOOSTSALES_JS );

	class VillaTheme_Admin_Fields {
		var $params = null;
		var $prefix = '_woocommerce_boost_sales';
		var $post_type = array();
		var $page = array( 'woocommerce-boost-sales' );

		function __construct( $prefix = '', $load_scripts = true ) {
			if ( $prefix ) {
				$this->prefix = $prefix;
				add_action( 'villatheme_setting_html_' . $prefix, array( $this, 'villatheme_setting_html' ) );
			}
			add_action( 'admin_init', array( $this, 'update_options' ) );
			//			if ( $load_scripts ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
			//			}
			add_action( 'villatheme_setting_html', array( $this, 'villatheme_setting_html' ) );

			add_action( 'wp_ajax_wbs_select_coupon', array( $this, 'wbs_select_coupon' ) );

			add_action( 'wp_ajax_wbs_search_product_excl', array( $this, 'wbs_search_product_excl' ) );
		}

		/**
		 * Enqueue scripts
		 */
		public function admin_enqueue_scripts() {
			global $wp_scripts, $wp_styles;
			if ( count( $this->post_type ) ) {
				if ( ! in_array( get_post_type(), $this->post_type ) ) {
					return;
				}
			}
			if ( count( $this->page ) ) {
				if ( isset( $_GET['page'] ) ) {
					if ( ! in_array( $_GET['page'], $this->page ) ) {
						return;
					}
				} else {
					return;
				}
			}
			/*Stylesheet*/
			wp_enqueue_style( 'villatheme-field-image', VILLATHEME_FIELD_CSS . 'image.min.css' );
			wp_enqueue_style( 'villatheme-field-transition', VILLATHEME_FIELD_CSS . 'transition.min.css' );
			wp_enqueue_style( 'villatheme-field-form', VILLATHEME_FIELD_CSS . 'form.min.css', 89 );
			wp_enqueue_style( 'villatheme-field-icon', VILLATHEME_FIELD_CSS . 'icon.min.css' );
			wp_enqueue_style( 'villatheme-field-dropdown', VILLATHEME_FIELD_CSS . 'dropdown.min.css' );
			wp_enqueue_style( 'villatheme-field-checkbox', VILLATHEME_FIELD_CSS . 'checkbox.min.css' );
			wp_enqueue_style( 'villatheme-field-segment', VILLATHEME_FIELD_CSS . 'segment.min.css' );
			wp_enqueue_style( 'villatheme-field-menu', VILLATHEME_FIELD_CSS . 'menu.min.css' );
			wp_enqueue_style( 'villatheme-field-tab', VILLATHEME_FIELD_CSS . 'tab.css' );
			wp_enqueue_style( 'villatheme-field-message', VILLATHEME_FIELD_CSS . 'message.min.css' );
			wp_enqueue_style( 'villatheme-fields', VILLATHEME_FIELD_CSS . 'fields.css' );

			/*Script*/

			wp_enqueue_script( 'villatheme-field-dependsOn', VILLATHEME_FIELD_JS . 'dependsOn-1.0.2.min.js', array( 'jquery' ) );
			wp_enqueue_script( 'villatheme-field-transition', VILLATHEME_FIELD_JS . 'transition.min.js', array( 'jquery' ) );
			wp_enqueue_script( 'villatheme-field-dropdown', VILLATHEME_FIELD_JS . 'dropdown.js', array( 'jquery' ) );
			wp_enqueue_script( 'villatheme-field-checkbox', VILLATHEME_FIELD_JS . 'checkbox.js', array( 'jquery' ) );
			wp_enqueue_script( 'villatheme-field-tab', VILLATHEME_FIELD_JS . 'tab.js', array( 'jquery' ) );
			wp_enqueue_script( 'villatheme-fields', VILLATHEME_FIELD_JS . 'fields.js', array( 'jquery' ) );
			/*Color picker*/
			wp_enqueue_script( 'iris', admin_url( 'js/iris.min.js' ), array(
				'jquery-ui-draggable',
				'jquery-ui-slider',
				'jquery-touch-punch'
			), false, 1 );


			$scripts = $wp_scripts->registered;

			foreach ( $scripts as $k => $script ) {
				preg_match( '/select2/i', $k, $result );
				if ( count( array_filter( $result ) ) ) {
					unset( $wp_scripts->registered[ $k ] );
					wp_dequeue_script( $script->handle );
				}
			}

			$styles = $wp_styles->registered;
			foreach ( $styles as $k => $style ) {
				preg_match( '/woocommerce/i', $k, $result );
				if ( count( array_filter( $result ) ) ) {
					unset( $wp_styles->registered[ $k ] );
					wp_dequeue_style( $k );
				}
			}

			wp_enqueue_style( 'select2', VI_WBOOSTSALES_CSS . 'select2.min.css' );
			wp_enqueue_script( 'select2-v4', VI_WBOOSTSALES_JS . 'select2.js', array( 'jquery' ), '4.0.3', true );

			$text_color_discount              = $this->get_option( 'text_color_discount' );
			$process_color                    = $this->get_option( 'process_color' );
			$process_background_color         = $this->get_option( 'process_background_color' );
			$btn_checkout_color               = $this->get_option( 'btn_checkout_color' );
			$btn_checkout_bg_color            = $this->get_option( 'btn_checkout_bg_color' );
			$bg_color_cross_sell              = $this->get_option( 'bg_color_cross_sell' );
			$bg_image_cross_sell              = $this->get_option( 'bg_image_cross_sell' );
			$text_color_cross_sell            = $this->get_option( 'text_color_cross_sell' );
			$price_text_color_cross_sell      = $this->get_option( 'price_text_color_cross_sell' );
			$save_price_text_color_cross_sell = $this->get_option( 'save_price_text_color_cross_sell' );
			$css_inline                       = "
				.vi-ui.form input[name='_woocommerce_boost_sales[text_color_discount]']{
					background: {$text_color_discount};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[process_color]']{
					background: {$process_color};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[process_background_color]']{
					background-color: {$process_background_color};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[btn_checkout_color]']{
					background-color: {$btn_checkout_color};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[btn_checkout_bg_color]']{
					background-color: {$btn_checkout_bg_color};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[bg_color_cross_sell]']{
					background-color: {$bg_color_cross_sell};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[text_color_cross_sell]']{
					background-color: {$text_color_cross_sell};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[price_text_color_cross_sell]']{
					background-color: {$price_text_color_cross_sell};
				}
				.vi-ui.form input[name='_woocommerce_boost_sales[save_price_text_color_cross_sell]']{
					background-color: {$save_price_text_color_cross_sell};
				}
			";
			wp_add_inline_style( 'villatheme-fields', $css_inline );

			/*admin ajax*/
			$script = 'var wbs_admin_ajax_url = "' . admin_url( 'admin-ajax.php' ) . '"';
			wp_add_inline_script( 'villatheme-fields', $script );
		}

		/**
		 * Ajax select coupon discount tab
		 */
		public function wbs_select_coupon() {
			$coupon_selected = $_POST['value'];
			$coupon          = new WC_Coupon( $coupon_selected );
			$coupon_data     = array(
				'expiry_date' => date( 'Y-m-d', $coupon->expiry_date ),
			);
			//echo $coupon_data['expiry_date'];
			if ( ! empty( $coupon_data['expiry_date'] ) ) {
				if ( $coupon_data['expiry_date'] <= date( 'Y-m-d' ) ) { ?>
					<div class="ui negative message vi-ui-expired">
						<div class="header">
							<?php esc_html_e( 'The coupon has expired !', 'woocommerce-boost-sales' ) ?>
						</div>
					</div>
				<?php }
			}

			wp_die();
		}

		/* ajax search product*/
		public function wbs_search_product_excl() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}

			ob_start();

			$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_STRING );
			if ( empty( $keyword ) ) {
				die();
			}

			$arg_p = array(
				'post_status'    => 'publish',
				'post_type'      => 'product',
				's'              => $keyword,
				'posts_per_page' => - 1,
				'orderby'        => 'title',
				'tax_query'      => array(
					array(
						'taxonomy' => 'product_type',
						'field'    => 'slug',
						'terms'    => 'wbs_bundle',
						'operator' => 'NOT IN'
					),
				),
				'meta_query'     => array(
					array(
						'key'   => '_stock_status',
						'value' => 'instock'
					)
				)
			);

			$the_query      = new WP_Query( $arg_p );
			$found_products = array();
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$product          = array(
						'id'   => get_the_ID(),
						'text' => get_the_title() . ' - (#' . get_the_ID() . ')'
					);
					$found_products[] = $product;
				}
			}
			wp_reset_postdata();
			wp_send_json( $found_products );
			die;
		}

		/**
		 * Run setting layout
		 *
		 * @param array $args
		 */
		public function villatheme_setting_html() {
			$args = apply_filters( 'wbs_data_settings', array() );
			$args = apply_filters( 'wbs_data_settings_' . $this->prefix, $args );
			if ( ! $this->params ) {
				$this->params = get_option( $this->prefix, array() );
			}
			ob_start();
			if ( count( $args ) ) { ?>
				<form method="post" action="" class="vi-ui form">
					<?php wp_nonce_field( $this->prefix . '_settings', $this->prefix . '_nonce' ) ?>
					<div class="vi-ui attached tabular menu">
						<?php foreach ( $args as $k => $arg ) { ?>
							<div class="item <?php echo isset( $arg['active'] ) ? $arg['active'] ? 'active' : '' : '' ?>"
								 data-tab="<?php echo esc_attr( $this->reformat_str( $k ) ) ?>"><?php echo isset( $arg['title'] ) ? $arg['title'] : '' ?></div>
						<?php } ?>
					</div>
					<?php
					foreach ( $args as $k => $arg ) { ?>
						<div
							class="vi-ui bottom attached tab segment <?php echo isset( $arg['active'] ) ? $arg['active'] ? 'active' : '' : '' ?>"
							data-tab="<?php echo esc_attr( $this->reformat_str( $k ) ) ?>">
							<table class="optiontable form-table">
								<?php

								if ( $k == 'discount' ) {
									if ( $this->check_enable_coupon() == false ) { ?>
										<div
											class="ui red message"><?php esc_html_e( 'Please enable Coupon in WooCommerce >> Settings >> Checkout before enable Discount !', 'woocommerce-boost-sales' ) ?></div>
									<?php }
								}

								if ( isset( $arg['fields'] ) && count( $arg['fields'] ) ) {
									foreach ( $arg['fields'] as $field ) {
										$name        = isset( $field['name'] ) ? $field['name'] : '';
										$type        = isset( $field['type'] ) ? $field['type'] : 'text';
										$description = isset( $field['description'] ) ? $field['description'] : '';
										$label       = isset( $field['label'] ) ? $field['label'] : '';
										$class       = isset( $field['class'] ) ? $field['class'] : '';
										$options     = isset( $field['options'] ) ? $field['options'] : array();
										if ( ! $name ) {
											continue;
										}
										?>
										<tr valign="top" class="<?php echo $class; ?>">
											<th scope="row">
												<label
													for="<?php echo esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']' ?>">
													<?php echo $label ?>
												</label>
											</th>
											<td>
												<?php
												switch ( $type ) {
													case 'text':
													case 'radio':
													case 'checkbox':
													case 'number':
													case 'email':
													case 'password':
													case 'color-picker':
													case 'date-picker':
													case 'file':
														echo $this->text_field( $field );
														break;
													case 'select':
														echo $this->select( $field );
														break;
													case 'textarea':
														echo $this->textarea( $field );
														break;
													case 'select2_ajax':
														echo $this->select2_ajax( $field );
														break;
												}

												if ( $class == "vi-wbs-dcoupon" && $options == array() ) {
													echo esc_html__( 'Dashboard >> WooCommerce >> Coupons >>', 'woocommerce-boost-sales' );
													echo '<a target="_bank" href="' . esc_url( admin_url( 'post-new.php?post_type=shop_coupon' ) ) . '">' . esc_html__( 'Add New Coupon', 'woocommerce-boost-sales' ) . '</a>';
												}
												?>
												<?php if ( $description ) { ?>
													<p class="description"><?php echo esc_html( $description ) ?></p>
												<?php } ?>
											</td>
										</tr>
									<?php }
								} ?>
							</table>

						</div>
					<?php } ?>
					<p>
						<input type="submit" class="button button-primary"
							   value=" <?php esc_html_e( 'Save', 'woocommerce-boost-sales' ) ?> " />
					</p>
				</form>
			<?php }
		}

		/**
		 * Save setting page
		 * @return bool
		 */
		public function update_options() {

			if ( ! isset( $_POST[ $this->prefix ] ) || ! isset( $_POST[ $this->prefix ] ) ) {
				return false;
			}
			if ( ! wp_verify_nonce( $_POST[ $this->prefix . '_nonce' ], $this->prefix . '_settings' ) ) {
				return false;
			}
			if ( ! current_user_can( 'manage_options' ) ) {
				return false;
			}

			update_option( $this->prefix, $_POST[ $this->prefix ] );
		}

		/**
		 * check enable coupon woocommerce
		 */
		public function check_enable_coupon() {
			global $wpdb;
			$query  = $wpdb->prepare( "SELECT option_value FROM {$wpdb->prefix}options WHERE option_name = %s", 'woocommerce_enable_coupons' );
			$result = $wpdb->get_results( $query, OBJECT );
			if ( count( $result ) ) {
				foreach ( $result as $key => $item ) {
					$enable_cp = $item->option_value;
					if ( 'yes' == $enable_cp ) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				return false;
			}
		}

		/**
		 * Get value
		 *
		 * @param null   $field   Name field
		 * @param string $default Data default
		 *
		 * @return bool|null|string
		 */
		public function get_option( $field = null, $default = '' ) {
			if ( ! $field ) {
				return false;
			}
			if ( ! $this->params ) {
				$this->params = get_option( $this->prefix, array() );
			}

			if ( isset( $this->params[ $field ] ) && $field ) {
				return $this->params[ $field ];
			} else {
				return $default;
			}
		}

		/**
		 * Input text
		 *
		 * @param array $field
		 *
		 * @return string|void
		 */
		protected function text_field( $field = array() ) {
			$name    = isset( $field['name'] ) ? $field['name'] : '';
			$type    = isset( $field['type'] ) ? $field['type'] : 'text';
			$value   = isset( $field['value'] ) ? $this->get_option( $field['name'], $field['value'] ) : '';
			$options = isset( $field['options'] ) ? $field['options'] : array();
			$holder  = isset( $field['holder'] ) ? $field['holder'] : '';
			$class   = isset( $field['class'] ) ? $field['class'] : '';
			$html    = '';
			switch ( $type ) {
				case 'text':
				case 'number':
				case 'email':
				case 'password':
					$html .= '<input value="' . esc_attr( $value ) . '" type="' . esc_attr( $type ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" placeholder="' . esc_attr( $holder ) . '" />';
					break;
				case 'radio':
				case 'checkbox':
					if ( count( $options ) ) {
						foreach ( $options as $k => $title ) {
							if ( $k == $value ) {
								$checked = 'checked="checked"';
							} else {
								$checked = '';
							}
							$html .= '<div class="vi-ui toggle ' . esc_attr( $type ) . '">';
							$html .= '<input ' . $checked . ' id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']' . '" type="' . esc_attr( $type ) . '" tabindex="0" class="hidden" value="' . esc_attr( $k ) . '" name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']' . '"/>';
							$html .= '<label>' . $title . '</label>';
							$html .= '</div>';
						}
					}
					break;
				case 'color-picker':
				case 'date-picker':

					$class .= ' ' . $type;
					$type  = 'text';
					$html  .= '<input value="' . esc_attr( $value ) . '" type="' . esc_attr( $type ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" placeholder="' . esc_attr( $holder ) . '" />';
					break;
				case 'file':
					$html = '<div class="vi-ui action input"><input type="text" readonly value="' . esc_attr( $value ) . '" /><input type="' . esc_attr( $type ) . '" value="' . esc_attr( $value ) . '" name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" /><div class="vi-ui icon button"><i class="cloud upload icon"></i></div></div>';
					break;
				default:
					return;
			}

			return $html;
		}

		/**
		 * Text area
		 *
		 * @param array $arg
		 *
		 * @return string
		 */
		protected function textarea( $field = array() ) {
			$name   = isset( $field['name'] ) ? $field['name'] : '';
			$value  = isset( $field['value'] ) ? $this->get_option( $field['name'], $field['value'] ) : '';
			$holder = isset( $field['holder'] ) ? $field['holder'] : '';
			$class  = isset( $field['class'] ) ? $field['class'] : '';
			$cols   = isset( $field['cols'] ) ? $field['cols'] : 30;
			$rows   = isset( $field['rows'] ) ? $field['rows'] : 10;

			$html = '<textarea cols="' . esc_attr( $cols ) . '" rows="' . esc_attr( $rows ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" placeholder="' . esc_attr( $holder ) . '" />' . esc_html( $value ) . '</textarea>';

			return $html;
		}

		/**
		 * Select field
		 *
		 * @param array $arg
		 *
		 * @return string|void
		 */
		protected function select( $field = array() ) {
			$name     = isset( $field['name'] ) ? $field['name'] : '';
			$value    = isset( $field['value'] ) ? $this->get_option( $field['name'], $field['value'] ) : '';
			$options  = isset( $field['options'] ) ? $field['options'] : array();
			$class    = isset( $field['class'] ) ? $field['class'] : '';
			$multiple = isset( $field['multiple'] ) ? $field['multiple'] : '';
			$html     = '';
			if ( count( $options ) ) {
				if ( $multiple ) {
					$multiple = 'multiple="multiple"';
					$m_data   = '[]';
				} else {
					$multiple = '';
					$m_data   = '';
				}

				$html .= '<select ' . $multiple . ' name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']' . $m_data . '" id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" class="vi-ui fluid dropdown ' . esc_attr( $class ) . '">';
				foreach ( $options as $k => $title ) {
					if ( $multiple ) {
						if ( is_array( $value ) ) {

							if ( in_array( $k, $value ) ) {
								$selected = 'selected="selected"';
							} else {
								$selected = '';
							}
						} else {
							$selected = '';
						}

					} else {
						if ( selected( $k, $value, false ) ) {
							$selected = 'selected="selected"';;
						} else {
							$selected = '';
						}
					}

					$html .= '<option ' . $selected . ' value="' . esc_attr( $k ) . '">' . esc_html( $title ) . '</option>';
				}
				$html .= '</select>';
			} else {
				return '';
			}

			return $html;
		}

		protected function select2_ajax( $field = array() ) {
			$name     = isset( $field['name'] ) ? $field['name'] : '';
			$value    = isset( $field['value'] ) ? $this->get_option( $field['name'], $field['value'] ) : '';
			$class    = isset( $field['class'] ) ? $field['class'] : '';
			$multiple = isset( $field['multiple'] ) ? $field['multiple'] : '';
			$html     = '';

			if ( $multiple ) {
				$multiple = 'multiple="multiple"';
				$m_data   = '[]';
			} else {
				$multiple = '';
				$m_data   = '';
			}

			$html .= '<select ' . $multiple . ' name="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']' . $m_data . '" id="' . esc_attr( $this->prefix ) . '[' . $this->reformat_str( $name ) . ']" class="product-search ' . esc_attr( $class ) . '">';

			if ( $multiple ) {
				if ( is_array( $value ) && count( $value ) ) {

					foreach ( $value as $item ) {
						$real_prod = wc_get_product( $item );
						if ( $real_prod ) {
							$html .= '<option selected="selected" value="' . esc_attr( $item ) . '">' . esc_html( $real_prod->get_title() . ' - #' . $item ) . '</option>';
						}
					}
				} else {
					$html .= '';
				}
			} else {
				$real_prod = wc_get_product( $value );
				if ( $real_prod ) {
					$html .= '<option selected="selected" value="' . esc_attr( $value ) . '">' . esc_html( $real_prod->get_title() . ' - #' . $value ) . '</option>';
				}
			}
			$html .= '</select>';

			return $html;
		}


		/**
		 * Reformat string (ID, Class)
		 *
		 * @param string $string
		 *
		 * @return mixed|string
		 */
		private function reformat_str( $string = '' ) {
			$string = trim( $string );
			$string = preg_replace( '/\W/i', '_', $string );
			$string = strtolower( $string );

			return $string;
		}

		public function get_coupon_code( $coupon ) {
			if ( $coupon ) {
				$the_post = get_post( $coupon );

				if ( $the_post->post_type == 'shop_coupon' && $the_post->post_status == 'publish' ) {
					return $the_post->post_title;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/*get coupon amount*/
		public function get_coupon_amount( $coupon ) {
			$coupon_data = new WC_Coupon( $coupon );

			$discount = $coupon_data->get_amount();
			if ( $discount ) {
				return $discount;
			}

			return false;
		}

		/*get minimum amount coupon*/
		public function get_coupon_min_amount( $coupon ) {
			$coupon_data = new WC_Coupon( $coupon );
			$min_amout   = $coupon_data->get_minimum_amount();
			if ( $min_amout ) {
				return $min_amout;
			}

			return false;
		}

		/**
		 * Get discount type
		 *
		 * @param $coupon
		 * @param $discount
		 *
		 * @return string
		 */
		public function get_discount_type( $coupon, $discount ) {
			$coupon_data   = new WC_Coupon( $coupon );
			$discount_type = $coupon_data->get_discount_type();
			switch ( $discount_type ) {
				case 'percent':
					return $discount . esc_html__( '% Cart', 'woocommerce-boost-sales' );
					break;
				case 'fixed_cart':
					return wc_price( apply_filters( 'woocommerce_boost_sales_coupon_amount_price', $discount ) ) . esc_html__( ' of Cart', 'woocommerce-boost-sales' );
					break;
				default:
					return wc_price( apply_filters( 'woocommerce_boost_sales_coupon_amount_price', $discount ) ) . esc_html__( ' per Product', 'woocommerce-boost-sales' );
			}

		}
	}

	new VillaTheme_Admin_Fields();
}