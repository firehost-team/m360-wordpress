<?php

/**
 * Class VI_WBOOSTSALES_Frontend_Notify
 */
class VI_WBOOSTSALES_Frontend_Archive {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'init_scripts' ), 99999 );

		add_action( 'init', array( $this, 'init' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );

		add_action( 'wp_ajax_nopriv_wbs_get_product', array( $this, 'product_html' ) );
		add_action( 'wp_ajax_wbs_get_product', array( $this, 'product_html' ) );

		/*WordPress lower 4.5*/
		add_action( 'wp_print_scripts', array( $this, 'custom_script' ) );

	}

	/**
	 * Script in Wp 4.2
	 */
	public function custom_script() {
		$script = 'var wboostsales_ajax_url = "' . admin_url( 'admin-ajax.php' ) . '"'; ?>
		<script type="text/javascript">
			<?php echo $script; ?>
		</script>
	<?php }

	/**
	 * Show HTML on front end
	 */
	public function product_html() {
		$params     = new VillaTheme_Admin_Fields();
		$enable     = $params->get_option( 'enable' );
		$product_id = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );
		if ( $enable && $product_id ) {
			echo $this->show_product( $product_id );
		}
		die;
	}

	/**
	 * Init function
	 */
	public function init() {


	}

	/**
	 * Show HTML code
	 */
	public function wp_footer() {
		$params        = new VillaTheme_Admin_Fields();
		$enable        = $params->get_option( 'enable' );
		$detect_mobile = $params->get_option( 'enable_mobile' );

		$mb_detect = new VILLA_Mobile_Detect();
		if ( $mb_detect->isMobile() ) {
			if ( $detect_mobile == 0 ) {
				return false;
			}
		}

		if ( $enable && is_archive() ) {
			echo $this->show_product();
		}
	}

	/**
	 * Show discount
	 */
	public function show_discount() {
		$params          = new VillaTheme_Admin_Fields();
		$enable_discount = $params->get_option( 'enable_discount' );
		$product_id      = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );

		if ( $enable_discount && $params->check_enable_coupon() ) {
			return $this->discount_html( $product_id );
		}

		return false;
	}

	/**
	 * Discount html
	 */
	protected function discount_html( $product_id = null ) {
		$params                = new VillaTheme_Admin_Fields();
		$coupon_desc           = $params->get_option( 'coupon_desc' );
		$coupon                = $params->get_option( 'coupon' );
		$coupon_position       = $params->get_option( 'coupon_position' );
		$enable_checkout       = $params->get_option( 'enable_checkout' );
		$redirect_after_second = $params->get_option( 'redirect_after_second' );
		$checkout_url          = wc_get_checkout_url();
		$show_if_empty_upsells = $params->get_option( 'show_if_empty_upsells' );

		if ( $product_id ) {
			$upsells = get_post_meta( $product_id, '_wbs_upsells', true );
		} else {
			$upsells = array();
		}

		if ( $coupon ) {


			$discount = $params->get_coupon_amount( $coupon );

			$discount1   = $params->get_discount_type( $coupon, $discount );
			$description = str_replace( '{discount_amount}', $discount1, strip_tags( $coupon_desc ) );

			$min_amount = $params->get_coupon_min_amount( $coupon );

			ob_start();
			if ( ! WC()->cart->prices_include_tax ) {
				$total_cart = WC()->cart->subtotal;
			} else {
				$total_cart = WC()->cart->subtotal + WC()->cart->tax_total;
			}

			if ( $total_cart >= $min_amount ) {
				$current_percent = 100;
			} else {
				$current_percent = round( ( $total_cart * 100 ) / $min_amount, 2 );
			}

			?>
			<div id="vi-wbs-bundle-progress-bar" class="vi-wbs-fixed-pg">
				<div class="vi-wbs-topbar <?php if ( $coupon_position == 0 ) {
					echo 'wbs_top_bar';
				} else {
					echo 'wbs_bottom_bar';
				} ?>">
					<div class="vi-wbs-goal"><?php esc_html_e( 'Discount: ', 'woocommerce-boost-sales' ) ?>
						<strong><?php echo $discount1; ?></strong>
					</div>
					<?php if ( $min_amount == 0 && $total_cart !== 0 ) { ?>
						<div
							class="vi-wbs_msg_apply_cp"><?php esc_html_e( 'Your cart have applied coupon', 'woocommerce-boost-sales' ); ?></div>
					<?php } else { ?>
						<div class="vi-wbs-current-progress">
							<?php esc_html_e( 'Get over of: ', 'woocommerce-boost-sales' ) ?>
							<span class="money"><?php echo wc_price( $min_amount ); ?></span>
						</div>
						<div
							class="vi-wbs-progress-limit vi-wbs-progress-limit-start"><?php echo wc_price( $total_cart ); ?></div>
						<div class="vi-wbs-progress-container">
							<div class="vi-wbs-progress">
								<div class="vi-wbs-progress-bar vi-wbs-progress-bar-success" role="progressbar"
									 aria-valuenow="<?php echo isset( $current_percent ) ? $current_percent : 0; ?>"
									 aria-valuemin="0" aria-valuemax="100"
									 style="width: <?php echo isset( $current_percent ) ? $current_percent : 0; ?>%;">
									<?php
									if ( isset( $current_percent ) && $current_percent > 0 ) {
										echo '<span class="vi-wbs-only">' . $current_percent . '%</span>';
									}
									?>
								</div>
							</div>
						</div>
						<div
							class="vi-wbs-progress-limit vi-wbs-progress-limit-end"><?php echo wc_price( $min_amount ); ?></div>
					<?php } ?>
					<div class="vi-wbs_progress_close">&times;</div>
				</div>

				<?php if ( ( $coupon_desc && ! empty( $upsells ) ) || $show_if_empty_upsells ) { ?>
					<div class="vi-wbs-headline">
						<div class="vi-wbs-typo-container">
							<span class="vi-wbs-headline-text">
								<?php
								if ( $total_cart >= $min_amount ) {
									if ( $enable_checkout ) {
										echo 'You will be redirected in <span id="wbs_time_rdt" data-url="' . $checkout_url . '">' . $redirect_after_second . '</span> second ....';
									} else {
										echo '';
									}
								} else {
									echo $description;
								}
								?>
							</span>
						</div>
					</div>
				<?php } ?>
			</div>
			<?php return ob_get_clean();
		}
	}

	/**
	 * Add Script and Style
	 */
	function init_scripts() {
		$params = new VillaTheme_Admin_Fields();
		$enable = $params->get_option( 'enable' );
		if ( ! $enable ) {
			return false;
		}

		if ( is_woocommerce() || is_cart() || is_checkout() ) {
			$params                           = new VillaTheme_Admin_Fields();
			$process_color                    = $params->get_option( 'process_color' );
			$process_background_color         = $params->get_option( 'process_background_color' );
			$btn_checkout_color               = $params->get_option( 'btn_checkout_color' );
			$btn_checkout_bg_color            = $params->get_option( 'btn_checkout_bg_color' );
			$text_color_discount              = $params->get_option( 'text_color_discount' );
			$select_template                  = $params->get_option( 'select_template' );
			$bg_color_cross_sell              = $params->get_option( 'bg_color_cross_sell' );
			$bg_image_cross_sell              = $params->get_option( 'bg_image_cross_sell' );
			$text_color_cross_sell            = $params->get_option( 'text_color_cross_sell' );
			$price_text_color_cross_sell      = $params->get_option( 'price_text_color_cross_sell' );
			$save_price_text_color_cross_sell = $params->get_option( 'save_price_text_color_cross_sell' );

			wp_enqueue_style( 'woocommerce-boost-sales-icon', VI_WBOOSTSALES_CSS . 'icon.min.css', array() );

			/*Flexslider*/
			wp_enqueue_style( 'jquery-vi_flexslider', VI_WBOOSTSALES_CSS . 'vi_flexslider.css', array(), '2.6.3' );
			wp_enqueue_script( 'jquery-vi_flexslider', VI_WBOOSTSALES_JS . 'jquery.vi_flexslider-min.js', array( 'jquery' ), VI_WBOOSTSALES_VERSION, true );

			wp_enqueue_style( 'woocommerce-boost-sales', VI_WBOOSTSALES_CSS . 'woocommerce-boost-sales.css', array(), VI_WBOOSTSALES_VERSION );

			if ( $select_template == '2' ) {
				wp_enqueue_style( 'woocommerce-boost-sales-template2', VI_WBOOSTSALES_CSS . 'styles/style-2.css', array(), VI_WBOOSTSALES_VERSION );
			}

			wp_enqueue_script( 'woocommerce-boost-sales', VI_WBOOSTSALES_JS . 'woocommerce-boost-sales.js', array( 'jquery' ), VI_WBOOSTSALES_VERSION, true );

			$css_inline = "
			.vi-wbs-progress{
				background-color: {$process_color} !important;
			}
			.vi-wbs-topbar .vi-wbs-goal strong{
				color: {$process_background_color};
			}
			.vi-wbs-progress-bar-success{
				background-color: {$process_background_color} !important;
			}
			.wbs-message-success .vi-wbs-btn-redeem{
				color: {$btn_checkout_color};
				background: {$btn_checkout_bg_color};
			}
			span.vi-wbs-highlight{
				color: {$process_background_color};
			}
			.vi-wbs-fixed-pg .vi-wbs-topbar,.vi-wbs-topbar .vi-wbs-progress-limit{
				color: {$text_color_discount};
			}
			#wbs-content-cross-sell .wbs-content-crossell .wbs-content-inner-crs{
				background-color: {$bg_color_cross_sell};
				background-image: url({$bg_image_cross_sell});
				background-size: cover;
				background-repeat: no-repeat;
				background-position: center center;
				color: {$text_color_cross_sell};
			}
			#wbs-content-cross-sell .wbs-content-crossell .wbs-content-inner-crs .upsell-title,.wbs-item-crosssells .product-type-wbs_bundle .product-desc .price span.amount, .wbs-item-crosssells .product-type-wbs_bundle:not(:last-child)::after, .wbs-crosssells-price span.wbs-crs-sale-price span.wbs-new-title, .wbs-crosssells-price span.wbs-crs-regular-price span.wbs-new-title{
				color: {$text_color_cross_sell};
			}
			.wbs-crosssells-price span.wbs-crs-regular-price span.money span{
				color: {$price_text_color_cross_sell};
			}
			.wbs-crosssells-price span.wbs-crs-sale-price span.money span{
				color: {$save_price_text_color_cross_sell};
			}
			";

			wp_add_inline_style( 'woocommerce-boost-sales', $css_inline );
		}
	}

	/**
	 * Show product
	 *
	 * @param $product_id Product ID
	 *
	 * @return html
	 */
	protected function show_product( $product_id = null ) {
		$bundle_cr               = new VI_WBOOSTSALES_Frontend_Bundles();
		$params                  = new VillaTheme_Admin_Fields();
		$redirect_after_second   = $params->get_option( 'redirect_after_second' );
		$coupon                  = $params->get_option( 'coupon' );
		$enable_thankyou         = $params->get_option( 'enable_thankyou' );
		$message_congrats        = $params->get_option( 'message_congrats' );
		$enable_checkout         = $params->get_option( 'enable_checkout' );
		$item_per_row            = $params->get_option( 'item_per_row' );
		$item_limit              = $params->get_option( 'limit' );
		$crosssell_enable        = $params->get_option( 'crosssell_enable' );
		$coupon_position         = $params->get_option( 'coupon_position' );
		$icon_position           = $params->get_option( 'icon_position' );
		$init_delay              = $params->get_option( 'init_delay' );
		$crosssell_description   = $params->get_option( 'crosssell_description' );
		$text_btn_checkout       = $params->get_option( 'text_btn_checkout' );
		$hide_cross_sell_archive = $params->get_option( 'hide_cross_sell_archive' );
		$show_if_empty_upsells   = $params->get_option( 'show_if_empty_upsells' );
		$show_with_category      = $params->get_option( 'show_with_category' );
		$exclude_product         = $params->get_option( 'exclude_product' );
		$select_template         = $params->get_option( 'select_template' );
		$message_bought          = $params->get_option( 'message_bought' );

		$discount  = $params->get_coupon_amount( $coupon );
		$discount1 = $params->get_discount_type( $coupon, $discount );

		$min_amount = $params->get_coupon_min_amount( $coupon );
		$message    = str_replace( '{discount_amount}', '<span class="vi-wbs-highlight">' . $discount1 . '</span>', strip_tags( $message_congrats ) );

		ob_start();

		if ( ! WC()->cart->prices_include_tax ) {
			$total_cart = WC()->cart->subtotal;
		} else {
			$total_cart = WC()->cart->subtotal + WC()->cart->tax_total;
		}

		$number_cart = WC()->cart->get_cart_contents_count();

		if ( $total_cart >= $min_amount && $this->show_discount() ) {
			$coupon_name = $params->get_coupon_code( $coupon );
			if ( ! WC()->cart->applied_coupons ) {
				$add_coupon = WC()->cart->add_discount( $coupon_name );
				add_action( 'woocommerce_before_cart', $add_coupon );
			}
		}

		if ( ! $product_id ) { ?>
			<div id="wbs-content-up-sell"
				 class="woocommerce-boost-sales wbs-content-up-sell" <?php if ( $this->show_discount() && $enable_checkout ) {
				echo 'data-time_rdt="' . esc_attr( $redirect_after_second ) . '"';
			} ?> style="display: none;">
			</div>
		<?php } else {
			$product      = wc_get_product( $product_id );
			$quantity     = filter_input( INPUT_POST, 'quantity', FILTER_SANITIZE_NUMBER_INT );
			$image        = $product->get_image();
			$title        = $product->get_title();
			$price        = $product->get_price();
			$price2       = $price * $quantity;
			$cart_url     = wc_get_cart_url();
			$checkout_url = wc_get_checkout_url();
			$shop_url     = esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) );

			if ( $this->show_discount() ) {
				echo $this->show_discount();
			}

			$upsells = get_post_meta( $product->get_id(), '_wbs_upsells', true );
			$check   = 0;
			if ( empty( $item_limit ) ) {
				$item_limit = 8;
			}

			if ( is_array( $upsells ) ) {
				if ( count( array_filter( $upsells ) ) ) {
					$u_args      = array(
						'post_status'    => 'publish',
						'post_type'      => 'product',
						'post__in'       => $upsells,
						'posts_per_page' => $item_limit
					);
					$the_upsells = new WP_Query( $u_args );
					$check       = 1;
				}
			}

			$check_1 = 0;
			$term_id = array();
			$cate    = wp_get_post_terms( $product->get_id(), 'product_cat' );
			if ( is_array( $cate ) ) {
				if ( count( array_filter( $cate ) ) ) {
					foreach ( $cate as $cae ) {
						$term_id[] = $cae->slug;
					}

					if ( $show_with_category ) {
						if ( ! isset( $exclude_product ) ) {
							$exclude_product1 = array();
						} else {
							$exclude_product1 = $exclude_product;
						}
					} else {
						$exclude_product1 = array();
					}

					$arg_p = array(
						'post_status'    => 'publish',
						'post_type'      => 'product',
						'posts_per_page' => $item_limit,
						'tax_query'      => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'product_cat',
								'field'    => 'slug',
								'terms'    => $term_id,
								'operator' => 'IN'
							)
						),
						'post__not_in'   => $exclude_product1,
						'orderby'        => 'title'
					);

					$the_upsells_cate = new WP_Query( $arg_p );
					$check_1          = 1;
				}
			}

			if ( $enable_thankyou && $total_cart >= $min_amount && $this->show_discount() ) { ?>
				<div class="overlay"></div>
				<div class="wbs-wrapper">
					<div class="wbs-content wbs-msg-congrats">
						<div class="wbs-content-inner">
							<a class="wbs-close" href="#" title="Close"><span>X</span></a>

							<div class="wbs-message-success">
								<p><?php echo $message; ?></p>
								<a href="<?php echo $checkout_url; ?>"
								   class="vi-wbs-btn-redeem"><?php esc_html_e( $text_btn_checkout, 'woocommerce-boost-sales' ) ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php } elseif ( ( is_array( $upsells ) && $check ) || $show_if_empty_upsells ) { ?>
				<div class="overlay"></div>
				<div class="wbs-wrapper wbs-archive-upsells wbs-archive-page" style="opacity: 0">
					<div class="wbs-content">
						<div class="wbs-content-inner">
							<a class="wbs-close" href="#" title="Close"><span>X</span></a>

							<div class="wbs-breadcrum">
								<?php
								if ( $select_template == 1 ) { ?>
									<p class="wbs-notify_added wbs-title_style1">
										<i class="checkmark icon"></i> <?php echo esc_html( $quantity . ' new product has added to your cart' ); ?>
									</p>
								<?php }
								?>
								<div class="wbs-header-right">
									<?php
									if ( $select_template == 2 ) {
										echo '<p class="wbs-current_total_cart">';

										if ( $number_cart > 1 ) {
											$temporary_number = $number_cart . ' products';
										} else {
											$temporary_number = $number_cart . ' product';
										}
										echo esc_html( 'Your current cart (' . $temporary_number . ') : ' ) . wc_price( $total_cart ) . '</p>';
									}
									?>
									<a href="<?php echo esc_url( $cart_url ) ?>"
									   class="wbs-button-view"><?php esc_html_e( 'View Cart', 'woocommerce-boost-sales' ) ?></a>
									<a href="<?php echo esc_url( $shop_url ) ?>"
									   class="wbs-button-continue <?php if ( $total_cart < $min_amount ) {
										   echo 'bounceUp';
									   } ?>"><?php esc_html_e( 'Continue Shopping', 'woocommerce-boost-sales' ) ?></a>
									<a href="<?php echo esc_url( $checkout_url ) ?>"
									   class="wbs-button-check <?php if ( $total_cart >= $min_amount ) {
										   echo 'bounceUp';
									   } ?>">
										<?php esc_html_e( 'Checkout', 'woocommerce-boost-sales' ) ?>
									</a>
								</div>
								<div class="wbs-product">
									<?php
									if ( $select_template == 2 ) { ?>
										<p class="wbs-notify_added wbs-title_style2">
											<i class="checkmark icon"></i> <?php echo esc_html( $quantity . ' new product has added to your cart' ); ?>
										</p>
									<?php }
									?>
									<div class="wbs-p-image">
										<?php echo $image; ?>
									</div>
									<?php if ( $select_template == 2 ) {
										echo '<div class="wbs-combo_popup_style2">';
									} ?>
									<div class="wbs-p-title"><?php echo esc_html( $title ); ?></div>
									<div class="wbs-p-price">
										<div class="wbs-p-quantity">
											<span class="quatty"><?php esc_html_e( 'Quantity:' ); ?></span>
											<span class="money" style="float: none;"><?php echo esc_html( $quantity ); ?></span>
										</div>
										<div class="wbs-price-total">
											<span
												class="wbs-total-price"><?php esc_html_e( 'Total', 'woocommerce-boost-sales' ) ?>
												<span class="money"
													  style="float: none;"><?php echo wc_price( $price2 ); ?></span>
											</span>
										</div>
									</div>
									<?php if ( $select_template == 2 ) {
										echo '</div>';
									} ?>
								</div>
							</div>
							<?php if ( $show_with_category && is_array( $cate ) && $check_1 ) { ?>
								<div class="wbs-bottom">
									<h3 class="upsell-title"><?php echo str_replace( '{name_product}', $title, strip_tags( $message_bought ) ); ?></h3>
									<hr />
									<div class="vi_flexslider" id="flexslider-up-sell"
										 data-item_per_row="<?php echo esc_attr( $item_per_row ); ?>">

										<ul class="wbs-upsells wbs_vi_slides">
											<?php
											if ( $the_upsells_cate->have_posts() ) {
												while ( $the_upsells_cate->have_posts() ) {
													$the_upsells_cate->the_post();
													$product = wc_get_product( get_the_ID() );
													if ( $product->has_child() && $product->get_type() == 'variable' ) {
														$children_variable = $product->get_children(); ?>
														<li <?php post_class(); ?>>
															<?php
															foreach ( $children_variable as $child_variable ) {
																$prds = wc_get_product( $child_variable ); ?>
																<div class="vi_wbs_chosen wbp_hidden_variable" data-id_prd="<?php echo esc_attr( $child_variable ); ?>">
																	<div class="product-top">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="product-image">
																			<?php
																			if ( has_post_thumbnail( $prds->get_id() ) ) {
																				echo get_the_post_thumbnail( $prds->get_id(), 'thumbnail' );
																			} elseif ( has_post_thumbnail( get_the_ID() ) ) {
																				echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																			} else {
																				echo '<img src="' . wc_placeholder_img_src() . '" alt="' . $prds->get_name() . '" width="150px" height="150px" />';
																			}

																			?>
																		</a>

																		<div class="controls">
																			<ul class="icon-links">
																				<li class="add-to-cart">
																					<a rel="nofollow" href="<?php echo esc_url( $prds->add_to_cart_url() ); ?>"
																					   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																					   data-x="X" data-product_id="<?php echo esc_attr( $prds->get_id() ); ?>"
																					   data-product_sku="<?php echo esc_attr( $prds->get_sku() ); ?>"
																					   class="button add_to_cart_button ajax_add_to_cart"
																					><?php echo esc_html( $prds->add_to_cart_text() ); ?></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="product-desc">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="woocommerce-LoopProduct-link">
																			<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $prds->get_name() ); ?></h2>
																		</a>
																		<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																		<span class="price"><?php echo wc_price( $prds->get_price() ); ?></span>
																	</div>
																</div>
															<?php } ?>
															<select name="vi_chosen_product_variable">
																<?php
																foreach ( $children_variable as $child_variable ) {
																	$sp         = wc_get_product( $child_variable );
																	$attributes = $sp->get_attributes();
																	$name_attr  = '';
																	$count      = count( $attributes );
																	$i          = 0;
																	foreach ( $attributes as $attribute ) {
																		if ( ++ $i !== $count ) {
																			$symbol = '-';
																		} else {
																			$symbol = '';
																		}
																		$name_attr .= ucfirst( $attribute ) . $symbol;
																	}
																	echo '<option value="' . $sp->get_id() . '" >' . $name_attr . '</option>';
																}
																?>
															</select>
														</li>
														<?php
													} else {
														?>
														<li <?php post_class(); ?>>
															<div class="product-top">
																<a href="<?php esc_url( the_permalink() ); ?>" class="product-image">
																	<?php
																	if ( has_post_thumbnail() ) {
																		echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																	} else {
																		echo '<img src="' . wc_placeholder_img_src() . '" alt="' . get_the_title() . '" width="150px" height="150px" />';
																	}
																	?>
																</a>

																<div class="controls">
																	<ul class="icon-links">
																		<li class="add-to-cart">
																			<a rel="nofollow" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"
																			   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																			   data-x="X" data-product_id="<?php echo esc_attr( $product->get_id() ); ?>"
																			   data-product_sku="<?php echo esc_attr( $product->get_sku() ); ?>"
																			   class="button add_to_cart_button ajax_add_to_cart"
																			><?php echo esc_html( $product->add_to_cart_text() ); ?></a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="product-desc">
																<a href="<?php esc_url( the_permalink() ); ?>" class="woocommerce-LoopProduct-link">
																	<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
																</a>
																<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																<span class="price"><?php echo wc_price( $product->get_price() ); ?></span>
															</div>
														</li>
													<?php }
												}

											}
											wp_reset_postdata();
											?>
										</ul>
									</div>
								</div>
								<?php
							} elseif ( is_array( $upsells ) && $check ) { ?>
								<div class="wbs-bottom">
									<h3 class="upsell-title"><?php echo str_replace( '{name_product}', $title, strip_tags( $message_bought ) ); ?></h3>
									<hr />
									<div class="vi_flexslider" id="flexslider-up-sell"
										 data-item_per_row="<?php echo esc_attr( $item_per_row ); ?>">
										<ul class="wbs-upsells wbs_vi_slides">
											<?php
											if ( $the_upsells->have_posts() ) {
												while ( $the_upsells->have_posts() ) {
													$the_upsells->the_post();
													$product = wc_get_product( get_the_ID() );
													if ( $product->has_child() && $product->get_type() == 'variable' ) {
														$children_variable = $product->get_children(); ?>
														<li <?php post_class(); ?>>
															<?php
															foreach ( $children_variable as $child_variable ) {
																$prds = wc_get_product( $child_variable ); ?>
																<div class="vi_wbs_chosen wbp_hidden_variable" data-id_prd="<?php echo esc_attr( $child_variable ); ?>">
																	<div class="product-top">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="product-image">
																			<?php
																			if ( has_post_thumbnail( $prds->get_id() ) ) {
																				echo get_the_post_thumbnail( $prds->get_id(), 'thumbnail' );
																			} elseif ( has_post_thumbnail( get_the_ID() ) ) {
																				echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																			} else {
																				echo '<img src="' . wc_placeholder_img_src() . '" alt="' . $prds->get_name() . '" width="150px" height="150px" />';
																			}
																			?>
																		</a>

																		<div class="controls">
																			<ul class="icon-links">
																				<li class="add-to-cart">
																					<a rel="nofollow" href="<?php echo esc_url( $prds->add_to_cart_url() ); ?>"
																					   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																					   data-x="X" data-product_id="<?php echo esc_attr( $prds->get_id() ); ?>"
																					   data-product_sku="<?php echo esc_attr( $prds->get_sku() ); ?>"
																					   class="button add_to_cart_button ajax_add_to_cart"
																					><?php echo esc_html( $prds->add_to_cart_text() ); ?></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="product-desc">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="woocommerce-LoopProduct-link">
																			<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $prds->get_name() ); ?></h2>
																		</a>
																		<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																		<span class="price"><?php echo wc_price( $prds->get_price() ); ?></span>
																	</div>
																</div>
															<?php } ?>
															<select name="vi_chosen_product_variable">
																<?php
																foreach ( $children_variable as $child_variable ) {
																	$sp         = wc_get_product( $child_variable );
																	$attributes = $sp->get_attributes();
																	$name_attr  = '';
																	$count      = count( $attributes );
																	$i          = 0;
																	foreach ( $attributes as $attribute ) {
																		if ( ++ $i !== $count ) {
																			$symbol = '-';
																		} else {
																			$symbol = '';
																		}
																		$name_attr .= ucfirst( $attribute ) . $symbol;
																	}
																	echo '<option value="' . $sp->get_id() . '" >' . $name_attr . '</option>';
																}
																?>
															</select>
														</li>
														<?php
													} else { ?>
														<li <?php post_class(); ?>>
															<div class="product-top">

																<a href="<?php esc_url( the_permalink() ); ?>" class="product-image">
																	<?php
																	if ( has_post_thumbnail() ) {
																		echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																	} else {
																		echo '<img src="' . wc_placeholder_img_src() . '" alt="' . get_the_title() . '" width="150px" height="150px" />';
																	}
																	?>
																</a>

																<div class="controls">
																	<ul class="icon-links">
																		<li class="add-to-cart">
																			<a rel="nofollow" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"
																			   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																			   data-x="X" data-product_id="<?php echo esc_attr( $product->get_id() ); ?>"
																			   data-product_sku="<?php echo esc_attr( $product->get_sku() ); ?>"
																			   class="button add_to_cart_button ajax_add_to_cart"
																			><?php echo esc_html( $product->add_to_cart_text() ); ?></a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="product-desc">
																<a href="<?php esc_url( the_permalink() ); ?>" class="woocommerce-LoopProduct-link">
																	<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
																</a>
																<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																<span class="price"><?php echo wc_price( $product->get_price() ); ?></span>
															</div>
														</li>
													<?php }
												}

											}
											wp_reset_postdata();
											?>
										</ul>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			<?php }

			$crosssell = get_post_meta( $product_id, '_wbs_crosssells' );

			if ( is_array( $crosssell ) ) {
				if ( count( array_filter( $crosssell ) ) ) {
					foreach ( $crosssell as $cross ) {
						$check_cs = 0;
						if ( is_array( $cross ) ) {
							if ( count( array_filter( $cross ) ) ) {
								$u_args        = array(
									'post_status' => 'publish',
									'post_type'   => 'product',
									'post__in'    => $cross
								);
								$the_crosssell = new WP_Query( $u_args );
								$check_cs      = 1;
							}
						}
						if ( $crosssell_enable && ! $hide_cross_sell_archive && is_array( $cross ) && $check_cs && $the_crosssell->have_posts() ) { ?>
							<div class="overlay"></div>
							<div class="wbs-wrapper wbs-archive-crosssells">
								<div id="wbs-gift-button-cat"
									 class="gift-button animated bounce <?php if ( $coupon_position !== 0 ) {
									     echo 'gift-bottom_progress ';
								     } else {
									     echo 'gift-top_progress ';
								     }

								     if ( $icon_position == 0 ) {
									     echo 'gift_right';
								     } else {
									     echo 'gift_left';
								     }
								     ?>" style="display: none;"
									 data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>"></div>
								<div id="wbs-cross-sell-archive"
									 class="wbs-content-crossell <?php if ( $coupon_position !== 0 ) {
									     echo 'gift-bottom_progress ';
								     } else {
									     echo 'gift-top_progress ';
								     }

								     if ( $icon_position == 0 ) {
									     echo 'gift_right';
								     } else {
									     echo 'gift_left';
								     } ?>" style="display: none;">
									<div class="wbs-content-inner wbs-content-inner-crs">
										<a class="wbs-close" href="#" title="Close"><span>X</span></a>

										<div class="wbs-bottom">
											<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

											<form class="cart" method="post" enctype='multipart/form-data'>
												<div class="wbs-crosssells">
													<?php
													if ( $the_crosssell->have_posts() ) {
														while ( $the_crosssell->have_posts() ) {
															$the_crosssell->the_post();
															echo $bundle_cr->show_crossell_html();
														}
														wp_reset_postdata();
													} ?>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						<?php }
					}

				}
			}
		}

		return ob_get_clean();
	}
}