<?php

/**
 * Class VI_WBOOSTSALES_Frontend_Single
 */
class VI_WBOOSTSALES_Frontend_Single {

	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
		add_action( 'wp_footer', array( $this, 'show_product_cross_sell' ) );

		add_action( 'wp_ajax_nopriv_wbs_get_product', array( $this, 'product_html' ) );
		add_action( 'wp_ajax_wbs_get_product', array( $this, 'product_html' ) );

	}

	/**
	 * Show HTML cross sells product
	 */
	public function show_product_cross_sell() {

		$bundle_cr             = new VI_WBOOSTSALES_Frontend_Bundles();
		$params                = new VillaTheme_Admin_Fields();
		$enable_checkout       = $params->get_option( 'enable_checkout' );
		$redirect_after_second = $params->get_option( 'redirect_after_second' );
		$coupon_position       = $params->get_option( 'coupon_position' );
		$icon_position         = $params->get_option( 'icon_position' );
		$crosssell_description = $params->get_option( 'crosssell_description' );
		$crosssell_enable      = $params->get_option( 'crosssell_enable' );
		$init_delay            = $params->get_option( 'init_delay' );
		$product_id            = filter_input( INPUT_POST, 'add-to-cart', FILTER_SANITIZE_NUMBER_INT );
		$quantity              = filter_input( INPUT_POST, 'quantity', FILTER_SANITIZE_NUMBER_INT );
		$enable_cart_page      = $params->get_option( 'enable_cart_page' );
		$cart_page_option      = $params->get_option( 'cart_page_option' );
		$enable_checkout_page  = $params->get_option( 'enable_checkout_page' );
		$checkout_page_option  = $params->get_option( 'checkout_page_option' );

		$detect_mobile = $params->get_option( 'enable_mobile' );

		$mb_detect = new VILLA_Mobile_Detect();
		if ( $mb_detect->isMobile() ) {
			if ( $detect_mobile == 0 ) {
				return false;
			}
		}

		if ( is_product() ) {
			if ( $product_id && $quantity ) {
				$crosssells = get_post_meta( $product_id, '_wbs_crosssells' );
			} else {
				$crosssells = get_post_meta( get_the_ID(), '_wbs_crosssells' );
			}

			if ( is_array( $crosssells ) ) {

				if ( count( array_filter( $crosssells ) ) ) {
					$u_args        = array(
						'post_status' => 'publish',
						'post_type'   => 'product',
						'post__in'    => $crosssells[0]
					);
					$the_crosssell = new WP_Query( $u_args );

					if ( $crosssell_enable && $the_crosssell->have_posts() ) { ?>
						<div id="gift-button" class="gift-button animated bounce
							<?php
						if ( $icon_position == 0 ) {
							echo 'gift_right';
						} else {
							echo 'gift_left';
						}
						?>" style="display: none;" data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>">
						</div>

						<div id="wbs-content-cross-sell" class="woocommerce-boost-sales" <?php if ( $this->show_discount() && $enable_checkout ) {
							echo 'data-time_rdt="' . $redirect_after_second . '"';
						} ?> style="display: none;">
							<div class="overlay"></div>
							<div class="wbs-wrapper">
								<?php echo $this->show_discount(); ?>
								<div class="wbs-content-crossell <?php if ( $coupon_position == 0 ) {
									echo 'gift-top_progress ';
								} else {
									echo 'gift-bottom_progress ';
								}

								if ( $icon_position == 0 ) {
									echo 'gift_right';
								} else {
									echo 'gift_left';
								} ?>">
									<div class="wbs-content-inner wbs-content-inner-crs">
										<a class="wbs-close" href="#" title="Close"><span>X</span></a>

										<div class="wbs-bottom">
											<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

											<form class="cart" method="post" enctype='multipart/form-data'>
												<div class="wbs-crosssells">
													<?php
													while ( $the_crosssell->have_posts() ) {
														$the_crosssell->the_post();
														echo $bundle_cr->show_crossell_html();
													}
													wp_reset_postdata();
													?>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
					}

				}
			}
		} else {

			$carts = WC()->cart->get_cart();

			if ( count( array_filter( $carts ) ) ) {
				$list_products_in_cart = array();
				foreach ( $carts as $cart ) {
					$list_products_in_cart[] = $cart['product_id'];
				}
				$crosssells = array();
				if ( count( array_filter( $list_products_in_cart ) ) ) {

					foreach ( $list_products_in_cart as $item ) {
						$crosssell_data = get_post_meta( $item, '_wbs_crosssells', true );

						if ( is_array( $crosssell_data ) ) {
							$crosssells = array_merge( $crosssell_data, $crosssells );
						}
					}
				}


				if ( is_cart() ) {

					// enable on cart page
					if ( count( $crosssells ) && $enable_cart_page ) {
						if ( $cart_page_option == 2 ) {
							$price1 = array();
							foreach ( $crosssells as $key => $crosssell ) {
								$_product             = wc_get_product( $crosssell );
								$price1[ $crosssell ] = $_product->get_price();

							}
							if ( count( $price1 ) ) {
								$maxs_bundle = array_keys( $price1, max( $price1 ) );

								$max_bundle[] = $maxs_bundle[0];

								$check_bd = 0;
								if ( is_array( $max_bundle ) ) {
									if ( count( array_filter( $max_bundle ) ) ) {
										$u_args        = array(
											'post_status' => 'publish',
											'post_type'   => 'product',
											'post__in'    => $max_bundle
										);
										$the_crosssell = new WP_Query( $u_args );
										$check_bd      = 1;
									}
								}

								if ( $crosssell_enable && is_array( $max_bundle ) && $check_bd && $the_crosssell->have_posts() ) { ?>
									<div id="gift-button" class="gift-button animated bounce
							<?php
									if ( $icon_position == 0 ) {
										echo 'gift_right';
									} else {
										echo 'gift_left';
									}
									?>" style="display: none;" data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>">
									</div>

									<div id="wbs-content-cross-sell" class="woocommerce-boost-sales" <?php if ( $this->show_discount() && $enable_checkout ) {
										echo 'data-time_rdt="' . $redirect_after_second . '"';
									} ?> style="display: none;">
										<div class="overlay"></div>
										<div class="wbs-wrapper">
											<?php echo $this->show_discount(); ?>
											<div class="wbs-content-crossell <?php if ( $coupon_position == 0 ) {
												echo 'gift-top_progress ';
											} else {
												echo 'gift-bottom_progress ';
											}

											if ( $icon_position == 0 ) {
												echo 'gift_right';
											} else {
												echo 'gift_left';
											} ?>">
												<div class="wbs-content-inner wbs-content-inner-crs">
													<a class="wbs-close" href="#" title="Close"><span>X</span></a>

													<div class="wbs-bottom">
														<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

														<form class="cart" method="post" enctype='multipart/form-data'>
															<div class="wbs-crosssells">
																<?php
																while ( $the_crosssell->have_posts() ) {
																	$the_crosssell->the_post();
																	echo $bundle_cr->show_crossell_html();
																}
																wp_reset_postdata();
																?>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							}
						} else {
							shuffle( $crosssells );
							$crosssell[] = $crosssells[0];
							$check_cs    = 0;
							if ( is_array( $crosssell ) ) {
								if ( count( array_filter( $crosssell ) ) ) {
									$u_args        = array(
										'post_status' => 'publish',
										'post_type'   => 'product',
										'post__in'    => $crosssell
									);
									$the_crosssell = new WP_Query( $u_args );
									$check_cs      = 1;
								}
							}
							if ( $crosssell_enable && is_array( $crosssell ) && $check_cs && $the_crosssell->have_posts() ) { ?>
								<div id="gift-button" class="gift-button animated bounce
							<?php
								if ( $icon_position == 0 ) {
									echo 'gift_right';
								} else {
									echo 'gift_left';
								}
								?>" style="display: none;" data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>">
								</div>

								<div id="wbs-content-cross-sell" class="woocommerce-boost-sales" <?php if ( $this->show_discount() && $enable_checkout ) {
									echo 'data-time_rdt="' . $redirect_after_second . '"';
								} ?> style="display: none;">
									<div class="overlay"></div>
									<div class="wbs-wrapper">
										<?php echo $this->show_discount(); ?>
										<div class="wbs-content-crossell <?php if ( $coupon_position == 0 ) {
											echo 'gift-top_progress ';
										} else {
											echo 'gift-bottom_progress ';
										}

										if ( $icon_position == 0 ) {
											echo 'gift_right';
										} else {
											echo 'gift_left';
										} ?>">
											<div class="wbs-content-inner wbs-content-inner-crs">
												<a class="wbs-close" href="#" title="Close"><span>X</span></a>

												<div class="wbs-bottom">
													<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

													<form class="cart" method="post" enctype='multipart/form-data'>
														<div class="wbs-crosssells">
															<?php
															while ( $the_crosssell->have_posts() ) {
																$the_crosssell->the_post();
																echo $bundle_cr->show_crossell_html();
															}
															wp_reset_postdata();
															?>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
							}
						}
					}
				} elseif ( is_checkout() ) {
					// enable on checkout page
					if ( count( $crosssells ) && $enable_checkout_page ) {

						if ( $checkout_page_option == 2 ) {
							$price1 = array();

							foreach ( $crosssells as $key => $crosssell ) {
								$_product             = wc_get_product( $crosssell );
								$price1[ $crosssell ] = $_product->get_price();

							}
							if ( count( $price1 ) ) {
								$maxs_bundle = array_keys( $price1, max( $price1 ) );

								$max_bundle[] = $maxs_bundle[0];
								$check_bd     = 0;
								if ( is_array( $max_bundle ) ) {
									if ( count( array_filter( $max_bundle ) ) ) {
										$u_args        = array(
											'post_status' => 'publish',
											'post_type'   => 'product',
											'post__in'    => $max_bundle
										);
										$the_crosssell = new WP_Query( $u_args );
										$check_bd      = 1;
									}
								}

								if ( $crosssell_enable && is_array( $max_bundle ) && $check_bd && $the_crosssell->have_posts() ) { ?>
									<div id="gift-button" class="gift-button animated bounce
							<?php
									if ( $icon_position == 0 ) {
										echo 'gift_right';
									} else {
										echo 'gift_left';
									}
									?>" style="display: none;" data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>">
									</div>

									<div id="wbs-content-cross-sell" class="woocommerce-boost-sales" <?php if ( $this->show_discount() && $enable_checkout ) {
										echo 'data-time_rdt="' . $redirect_after_second . '"';
									} ?> style="display: none;">
										<div class="overlay"></div>
										<div class="wbs-wrapper">
											<?php echo $this->show_discount(); ?>
											<div class="wbs-content-crossell <?php if ( $coupon_position == 0 ) {
												echo 'gift-top_progress ';
											} else {
												echo 'gift-bottom_progress ';
											}

											if ( $icon_position == 0 ) {
												echo 'gift_right';
											} else {
												echo 'gift_left';
											} ?>">
												<div class="wbs-content-inner wbs-content-inner-crs">
													<a class="wbs-close" href="#" title="Close"><span>X</span></a>

													<div class="wbs-bottom">
														<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

														<form class="cart" method="post" enctype='multipart/form-data'>
															<div class="wbs-crosssells">
																<?php
																while ( $the_crosssell->have_posts() ) {
																	$the_crosssell->the_post();
																	echo $bundle_cr->show_crossell_html();
																}
																wp_reset_postdata();
																?>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							}
						} else {
							shuffle( $crosssells );
							$crosssell[] = $crosssells[0];
							$check_cs    = 0;
							if ( is_array( $crosssell ) ) {
								if ( count( array_filter( $crosssell ) ) ) {
									$u_args        = array(
										'post_status' => 'publish',
										'post_type'   => 'product',
										'post__in'    => $crosssell
									);
									$the_crosssell = new WP_Query( $u_args );
									$check_cs      = 1;
								}
							}
							if ( $crosssell_enable && is_array( $crosssell ) && $check_cs && $the_crosssell->have_posts() ) { ?>
								<div id="gift-button" class="gift-button animated bounce
							<?php
								if ( $icon_position == 0 ) {
									echo 'gift_right';
								} else {
									echo 'gift_left';
								}
								?>" style="display: none;" data-initial_delay="<?php echo isset( $init_delay ) ? $init_delay : 0; ?>">
								</div>

								<div id="wbs-content-cross-sell" class="woocommerce-boost-sales" <?php if ( $this->show_discount() && $enable_checkout ) {
									echo 'data-time_rdt="' . $redirect_after_second . '"';
								} ?> style="display: none;">
									<div class="overlay"></div>
									<div class="wbs-wrapper">
										<?php echo $this->show_discount(); ?>
										<div class="wbs-content-crossell <?php if ( $coupon_position == 0 ) {
											echo 'gift-top_progress ';
										} else {
											echo 'gift-bottom_progress ';
										}

										if ( $icon_position == 0 ) {
											echo 'gift_right';
										} else {
											echo 'gift_left';
										} ?>">
											<div class="wbs-content-inner wbs-content-inner-crs">
												<a class="wbs-close" href="#" title="Close"><span>X</span></a>

												<div class="wbs-bottom">
													<h3 class="upsell-title"><?php esc_html_e( $crosssell_description, 'woocommerce-boost-sales' ) ?></h3>

													<form class="cart" method="post" enctype='multipart/form-data'>
														<div class="wbs-crosssells">
															<?php
															while ( $the_crosssell->have_posts() ) {
																$the_crosssell->the_post();
																echo $bundle_cr->show_crossell_html();
															}
															wp_reset_postdata();
															?>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
							}
						}
					}
				}

			}
		}

	}

	/**
	 * Get cross sells product
	 */
	public function get_product_cross_sell( $product_id ) {
		global $wpdb;
		$sql = $wpdb->prepare( "SELECT post_id FROM " . $wpdb->prefix . "postmeta WHERE meta_key = '_wbs_crosssells' && meta_value like '%%%s%%'", serialize( $product_id ) );
		$res = $wpdb->get_results( $sql, OBJECT );

		return $res;
	}

	/**
	 * Show HTML on front end
	 */
	public function product_html() {
        $params     = new VillaTheme_Admin_Fields();
		$enable     = $params->get_option( 'enable' );
		$product_id = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );

        if ( $enable && $product_id ) {
			echo $this->show_product( $product_id );
		}
		die;
	}

	/**
	 * Init function
	 */
	public function init() {


	}

	/**
	 * Show discount
	 */
	public function show_discount() {
		$params          = new VillaTheme_Admin_Fields();
		$enable_discount = $params->get_option( 'enable_discount' );
		$product_id      = filter_input( INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT );
		if ( $enable_discount && $params->check_enable_coupon() ) {
			return $this->discount_html( $product_id );
		}

		return false;
	}

	/**
	 * Discount html
	 */
	protected function discount_html( $product_id = null ) {
		$params                      = new VillaTheme_Admin_Fields();
		$coupon_desc                 = $params->get_option( 'coupon_desc' );
		$coupon                      = $params->get_option( 'coupon' );
		$coupon_position             = $params->get_option( 'coupon_position' );
		$enable_checkout             = $params->get_option( 'enable_checkout' );
		$redirect_after_second       = $params->get_option( 'redirect_after_second' );
		$show_if_empty_upsells       = $params->get_option( 'show_if_empty_upsells' );
		$hide_on_single_product_page = $params->get_option( 'hide_on_single_product_page' );

		if ( $product_id ) {
			$upsells = get_post_meta( $product_id, '_wbs_upsells', true );
		} else {
			$upsells = array();
		}
		$checkout_url = wc_get_checkout_url();

		$total_cart = 0;
		if ( $coupon ) {

			$discount = $params->get_coupon_amount( $coupon );

			/*get discount type*/
			$discount1 = $params->get_discount_type( $coupon, $discount );

			/*get minimum amount coupon*/
			$min_amount = $params->get_coupon_min_amount( $coupon );

			/*get current cart*/
			if ( ! WC()->cart->prices_include_tax ) {
				$total_cart = WC()->cart->subtotal;
			} else {
				$total_cart = WC()->cart->subtotal + WC()->cart->tax_total;
			}

			if ( $total_cart >= $min_amount ) {
				$current_percent = 100;
			} else {
				$current_percent = round( ( $total_cart * 100 ) / $min_amount, 2 );
			}

			ob_start(); ?>
			<div id="vi-wbs-bundle-progress-bar" class="vi-wbs-fixed-pg">
				<div class="vi-wbs-topbar <?php if ( $coupon_position == 0 ) {
					echo 'wbs_top_bar';
				} else {
					echo 'wbs_bottom_bar';
				} ?>">
					<div class="vi-wbs-goal"><?php esc_html_e( 'Discount:', 'woocommerce-boost-sales' ) ?>
						<strong><?php echo $discount1; ?></strong></div>
					<?php if ( $min_amount == 0 && $total_cart !== 0 ) { ?>
						<div class="vi-wbs_msg_apply_cp"><?php esc_html_e( 'Your cart have applied coupon', 'woocommerce-boost-sales' ); ?></div>
					<?php } else { ?>
						<div class="vi-wbs-current-progress">
							<?php esc_html_e( 'Get over of: ', 'woocommerce-boost-sales' ) ?>
							<span class="money"><?php echo wc_price( $min_amount ); ?></span>
						</div>
						<div class="vi-wbs-progress-limit vi-wbs-progress-limit-start"><?php echo wc_price( $total_cart ); ?></div>
						<div class="vi-wbs-progress-container">
							<div class="vi-wbs-progress">
								<div class="vi-wbs-progress-bar vi-wbs-progress-bar-success" role="progressbar" aria-valuenow="<?php echo isset( $current_percent ) ? $current_percent : 0; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset( $current_percent ) ? $current_percent : 0; ?>%;">
									<?php
									if ( isset( $current_percent ) && $current_percent > 0 ) {
										echo '<span class="vi-wbs-only">' . esc_html( $current_percent ) . '%</span>';
									}
									?>
								</div>
							</div>
						</div>
						<div class="vi-wbs-progress-limit vi-wbs-progress-limit-end"><?php echo wc_price( $min_amount ); ?></div>
					<?php } ?>
					<div class="vi-wbs_progress_close">&times;</div>
				</div>

				<?php if ( ( empty( $hide_on_single_product_page ) && $coupon_desc && ! empty( $upsells ) ) || ( empty( $hide_on_single_product_page ) && $show_if_empty_upsells ) ) {
					$description = str_replace( '{discount_amount}', $discount1, strip_tags( $coupon_desc ) );
					?>
					<div class="vi-wbs-headline">
						<div class="vi-wbs-typo-container">
							<span class="vi-wbs-headline-text">
								<?php
								if ( $total_cart >= $min_amount ) {
									if ( $enable_checkout ) {
										echo 'You will be redirected in <span id="wbs_time_rdt" data-url="' . $checkout_url . '">' . $redirect_after_second . '</span> second ....';
									} else {
										echo '';
									}
								} else {
									echo $description;
								}
								?>
							</span>
						</div>
					</div>
				<?php } ?>
			</div>

			<?php return ob_get_clean();
		}
	}


	/**
	 * Show HTML code
	 */
	public function wp_footer() {
        $params                = new VillaTheme_Admin_Fields();
		$enable                = $params->get_option( 'enable' );
		$enable_checkout       = $params->get_option( 'enable_checkout' );
		$redirect_after_second = $params->get_option( 'redirect_after_second' );
		$product_id            = filter_input( INPUT_POST, 'add-to-cart', FILTER_SANITIZE_NUMBER_INT );
		$quantity              = filter_input( INPUT_POST, 'quantity', FILTER_SANITIZE_NUMBER_INT );
		$detect_mobile         = $params->get_option( 'enable_mobile' );

        //mail('ibrahim@m360.no','booking: ',print_r($product_id,true));

		$mb_detect = new VILLA_Mobile_Detect();
		if ( $mb_detect->isMobile() ) {
			if ( $detect_mobile == 0 ) {
				return false;
			}
		}

        if ($product_id) {
            $product = wc_get_product($product_id);
            //mail('ibrahim@m360.no',__FUNCTION__,$product->get_type());
            if($quantity || $product->get_type()=='booking'){
                if ( $enable && is_product() ) { ?>
                    <div class="woocommerce-boost-sales wbs-content-up-sell" <?php if ( $this->show_discount() && $enable_checkout ) {
                        echo 'data-time_rdt="' . $redirect_after_second . '"';
                    } ?> >
                        <?php
                        echo $this->show_product( $product_id );
                        ?>
                    </div>
                <?php }
            }
		}
	}

	/**
	 * Show product
	 *
	 * @param $product_id Product ID
	 *
	 */
	protected function show_product( $product_id = null ) {

        $params                      = new VillaTheme_Admin_Fields();
		$coupon                      = $params->get_option( 'coupon' );
		$enable_thankyou             = $params->get_option( 'enable_thankyou' );
		$message_congrats            = $params->get_option( 'message_congrats' );
		$item_per_row                = $params->get_option( 'item_per_row' );
		$item_limit                  = $params->get_option( 'limit' );
		$hide_on_single_product_page = $params->get_option( 'hide_on_single_product_page' );
		$text_btn_checkout           = $params->get_option( 'text_btn_checkout' );
		$show_if_empty_upsells       = $params->get_option( 'show_if_empty_upsells' );
		$show_with_category          = $params->get_option( 'show_with_category' );
		$exclude_product             = $params->get_option( 'exclude_product' );
		$select_template             = $params->get_option( 'select_template' );
		$message_bought              = $params->get_option( 'message_bought' );

		$discount  = $params->get_coupon_amount( $coupon );
		$discount1 = $params->get_discount_type( $coupon, $discount );

		$min_amount = $params->get_coupon_min_amount( $coupon );

		$message = str_replace( '{discount_amount}', '<span class="vi-wbs-highlight">' . $discount1 . '</span>', strip_tags( $message_congrats ) );

		ob_start();
		if ( ! $product_id ) { ?>
			<div id="wbs-content-up-sell" class="woocommerce-boost-sales wbs-content-up-sell" style="display: none;"></div>
		<?php } else {
			$quantity     = filter_input( INPUT_POST, 'quantity', FILTER_SANITIZE_NUMBER_INT );
			$product      = wc_get_product( $product_id );
			$image        = $product->get_image();
			$title        = $product->get_title();
			$price        = $product->get_price();
			$price2       = $price * $quantity;
			$cart_url     = wc_get_cart_url();
			$checkout_url = wc_get_checkout_url();
			$shop_url     = esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) );
			/*Get Product up sells*/
			$upsells = get_post_meta( $product->get_id(), '_wbs_upsells', true );

            $check   = 0;
			if ( empty( $item_limit ) ) {
				$item_limit = 8;
			}

			if ( is_array( $upsells ) ) {
				if ( count( array_filter( $upsells ) ) ) {
					$u_args      = array(
						'post_status'    => 'publish',
						'post_type'      => 'product',
						'post__in'       => $upsells,
						'posts_per_page' => $item_limit
					);
					$the_upsells = new WP_Query( $u_args );
					$check       = 1;
				}
			}
			/*show discount progress bar*/
			if ( $this->show_discount() ) {
				echo $this->show_discount();
			}

			if ( ! WC()->cart->prices_include_tax ) {
				$total_cart = WC()->cart->subtotal;
			} else {
				$total_cart = WC()->cart->subtotal + WC()->cart->tax_total;
			}

			$number_cart = WC()->cart->get_cart_contents_count();

			if ( $total_cart >= $min_amount && $this->show_discount() ) {
				$coupon_name = $params->get_coupon_code( $coupon );
				if ( ! WC()->cart->applied_coupons ) {
					$add_coupon = WC()->cart->add_discount( $coupon_name );
					add_action( 'woocommerce_before_cart', $add_coupon );
				}
			}

			$check_1 = 0;
			$term_id = array();
			$cate    = wp_get_post_terms( $product->get_id(), 'product_cat' );
			if ( is_array( $cate ) ) {
				if ( count( array_filter( $cate ) ) ) {
					foreach ( $cate as $cae ) {
						$term_id[] = $cae->slug;
					}

					if ( $show_with_category ) {
						if ( ! isset( $exclude_product ) ) {
							$exclude_product1 = array();
						} else {
							$exclude_product1 = $exclude_product;
						}
					} else {
						$exclude_product1 = array();
					}

					$arg_p = array(
						'post_status'    => 'publish',
						'post_type'      => 'product',
						'posts_per_page' => $item_limit,
						'tax_query'      => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'product_cat',
								'field'    => 'slug',
								'terms'    => $term_id,
								'operator' => 'IN'
							)
						),
						'post__not_in'   => $exclude_product1,
						'orderby'        => 'title'
					);

					$the_upsells_cate = new WP_Query( $arg_p );
					$check_1          = 1;
				}
			}

			if ( $enable_thankyou && $total_cart >= $min_amount && $this->show_discount() ) { ?>
				<div class="overlay"></div>
				<div class="wbs-wrapper">
					<div class="wbs-content wbs-msg-congrats">
						<div class="wbs-content-inner">
							<a class="wbs-close" href="#" title="Close"><span>X</span></a>

							<div class="wbs-message-success">
								<p><?php echo $message; ?></p>
								<a href="<?php echo esc_url( $checkout_url ); ?>" class="vi-wbs-btn-redeem"><?php esc_html_e( $text_btn_checkout, 'woocommerce-boost-sales' ) ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php } elseif ( ( empty( $hide_on_single_product_page ) && is_array( $upsells ) && $check ) || ( empty( $hide_on_single_product_page ) && $show_if_empty_upsells ) ) { ?>
				<div class="overlay"></div>
				<div class="wbs-wrapper wbs-archive-upsells wbs-single-page" style="opacity: 0">
					<div class="wbs-content">
						<div class="wbs-content-inner">
							<a class="wbs-close" href="#" title="Close"><span>X</span></a>

							<div class="wbs-breadcrum">
								<?php
								if ( $select_template == 1 ) { ?>
									<p class="wbs-notify_added wbs-title_style1">
										<i class="checkmark icon"></i> <?php echo esc_html( $quantity . ' new product has added to your cart' ); ?>
									</p>
								<?php }
								?>
								<div class="wbs-header-right">
									<?php
									if ( $select_template == 2 ) {
										echo '<p class="wbs-current_total_cart">';

										if ( $number_cart > 1 ) {
											$temporary_number = $number_cart . ' products';
										} else {
											$temporary_number = $number_cart . ' product';
										}
										echo esc_html( 'Your current subtotal cart (' . $temporary_number . ') : ' ) . wc_price( $total_cart ) . '</p>';
									}
									?>
									<a href="<?php echo esc_url( $cart_url ) ?>" class="wbs-button-view"><?php esc_html_e( 'View Cart', 'woocommerce-boost-sales' ) ?></a>
									<a href="<?php echo esc_url( $shop_url ) ?>" class="wbs-button-continue <?php if ( $total_cart < $min_amount ) {
										echo 'bounceUp';
									} ?>"><?php esc_html_e( 'Continue Shopping', 'woocommerce-boost-sales' ) ?></a>
									<a href="<?php echo esc_url( $checkout_url ) ?>" class="wbs-button-check <?php if ( $total_cart >= $min_amount ) {
										echo 'bounceUp';
									} ?>">
										<?php esc_html_e( 'Checkout', 'woocommerce-boost-sales' ) ?>
									</a>
								</div>
								<div class="wbs-product">
									<?php
									if ( $select_template == 2 ) { ?>
										<p class="wbs-notify_added wbs-title_style2">
											<i class="checkmark icon"></i> <?php echo esc_html( $quantity . ' new product has added to your cart' ); ?>
										</p>
									<?php }
									?>
									<div class="wbs-p-image">
										<?php echo $image ?>
									</div>
									<?php if ( $select_template == 2 ) {
										echo '<div class="wbs-combo_popup_style2">';
									} ?>
									<div class="wbs-p-title"><?php echo esc_html( $title ); ?></div>
									<div class="wbs-p-price">
										<div class="wbs-p-quantity">
											<span class="quatty"><?php esc_html_e( 'Quantity:' ); ?></span>
											<span class="money" style="float: none;"><?php echo esc_html( $quantity ); ?></span>
										</div>
										<div class="wbs-price-total">
											<span class="wbs-total-price"><?php esc_html_e( 'Total:', 'woocommerce-boost-sales' ) ?>
												<span class="money" style="float: none;"><?php echo wc_price( $price2 ); ?></span></span>
										</div>
									</div>
									<?php if ( $select_template == 2 ) {
										echo '</div>';
									} ?>
								</div>
							</div>
							<?php
							if ( $show_with_category && is_array( $cate ) && $check_1 ) { ?>
								<div class="wbs-bottom">
									<h3 class="upsell-title"><?php echo str_replace( '{name_product}', $title, strip_tags( $message_bought ) ); ?></h3>
									<hr />
									<div class="vi_flexslider" id="flexslider-up-sell"
										 data-item_per_row="<?php echo esc_attr( $item_per_row ); ?>">

										<ul class="wbs-upsells wbs_vi_slides">
											<?php
											if ( $the_upsells_cate->have_posts() ) {
												while ( $the_upsells_cate->have_posts() ) {
													$the_upsells_cate->the_post();
													$product = wc_get_product( get_the_ID() );
													if ( $product->has_child() && $product->get_type() == 'variable' ) {
														$children_variable = $product->get_children(); ?>
														<li <?php post_class(); ?>>
															<?php
															foreach ( $children_variable as $child_variable ) {
																$prds = wc_get_product( $child_variable ); ?>
																<div class="vi_wbs_chosen wbp_hidden_variable" data-id_prd="<?php echo esc_attr( $child_variable ); ?>">
																	<div class="product-top">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="product-image">
																			<?php
																			if ( has_post_thumbnail( $prds->get_id() ) ) {
																				echo get_the_post_thumbnail( $prds->get_id(), 'thumbnail' );
																			} elseif ( has_post_thumbnail( get_the_ID() ) ) {
																				echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																			} else {
																				echo '<img src="' . wc_placeholder_img_src() . '" alt="' . $prds->get_name() . '" width="150px" height="150px" />';
																			}
																			?>
																		</a>

																		<div class="controls">
																			<ul class="icon-links">
																				<li class="add-to-cart">
																					<a rel="nofollow" href="<?php echo esc_url( $prds->add_to_cart_url() ); ?>"
																					   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																					   data-product_id="<?php echo esc_attr( $prds->get_id() ); ?>"
																					   data-product_sku="<?php echo esc_attr( $prds->get_sku() ); ?>"
																					   class="button add_to_cart_button ajax_add_to_cart"
																					><?php echo esc_html( $prds->add_to_cart_text() ); ?></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="product-desc">
																		<a href="<?php echo esc_url( get_permalink( $child_variable ) ); ?>" class="woocommerce-LoopProduct-link">
																			<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $prds->get_name() ); ?></h2>
																		</a>
																		<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																		<span class="price"><?php echo wc_price( $prds->get_price() ); ?></span>
																	</div>
																</div>
															<?php } ?>
															<select name="vi_chosen_product_variable">
																<?php
																foreach ( $children_variable as $child_variable ) {
																	$sp         = wc_get_product( $child_variable );
																	$attributes = $sp->get_attributes();
																	$name_attr  = '';
																	$count      = count( $attributes );
																	$count      = count( $attributes );
																	$i          = 0;
																	foreach ( $attributes as $attribute ) {
																		if ( ++ $i !== $count ) {
																			$symbol = '-';
																		} else {
																			$symbol = '';
																		}
																		$name_attr .= ucfirst( $attribute ) . $symbol;
																	}
																	echo '<option value="' . $sp->get_id() . '" >' . $name_attr . '</option>';
																}
																?>
															</select>
														</li>
														<?php
													} else {
														?>
														<li <?php post_class(); ?>>
															<div class="product-top">
																<a href="<?php esc_url( the_permalink() ); ?>" class="product-image">
																	<?php
																	if ( has_post_thumbnail() ) {
																		echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																	} else {
																		echo '<img src="' . wc_placeholder_img_src() . '" alt="' . get_the_title() . '" width="150px" height="150px" />';
																	}
																	?>
																</a>

																<div class="controls">
																	<ul class="icon-links">
																		<li class="add-to-cart">
																			<a rel="nofollow" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"
																			   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																			   data-product_id="<?php echo esc_attr( $product->get_id() ); ?>"
																			   data-product_sku="<?php echo esc_attr( $product->get_sku() ); ?>"
																			   class="button add_to_cart_button ajax_add_to_cart"
																			><?php echo esc_html( $product->add_to_cart_text() ); ?></a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="product-desc">
																<a href="<?php esc_url( the_permalink() ); ?>" class="woocommerce-LoopProduct-link">
																	<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
																</a>
																<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																<span class="price"><?php echo wc_price( $product->get_price() ); ?></span>
															</div>
														</li>
													<?php }
												}
												wp_reset_postdata();
											} ?>
										</ul>
									</div>
								</div>
								<?php
							} elseif ( is_array( $upsells ) && $check ) { ?>
								<div class="wbs-bottom">
									<h3 class="upsell-title"><?php echo str_replace( '{name_product}', $title, strip_tags( $message_bought ) ); ?></h3>
									<hr />
									<div class="vi_flexslider" id="flexslider-up-sell" data-item_per_row="<?php echo $item_per_row; ?>">
										<ul class="wbs-upsells wbs_vi_slides">
											<?php
											if ( $the_upsells->have_posts() ) {
												while ( $the_upsells->have_posts() ) {
													$the_upsells->the_post();
													$product = wc_get_product( get_the_ID() );
													if ( $product->has_child() && $product->get_type() == 'variable' ) {
														$children_variable = $product->get_children(); ?>
														<li <?php post_class(); ?>>
															<?php

															foreach ( $children_variable as $child_variable ) {
																$prds = wc_get_product( $child_variable ); ?>
																<div class="vi_wbs_chosen wbp_hidden_variable" data-id_prd="<?php echo esc_attr( $child_variable ); ?>">
																	<div class="product-top">
																		<a href="<?php echo get_permalink( $child_variable ); ?>" class="product-image">
																			<?php
																			if ( has_post_thumbnail( $prds->get_id() ) ) {
																				echo get_the_post_thumbnail( $prds->get_id(), 'thumbnail' );
																			} elseif ( has_post_thumbnail( get_the_ID() ) ) {
																				echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																			} else {
																				echo '<img src="' . wc_placeholder_img_src() . '" alt="' . $prds->get_name() . '" width="150px" height="150px" />';
																			}
																			?>
																		</a>

																		<div class="controls">
																			<ul class="icon-links">
																				<li class="add-to-cart">
																					<a rel="nofollow" href="<?php echo esc_url( $prds->add_to_cart_url() ); ?>"
																					   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																					   data-product_id="<?php echo esc_attr( $prds->get_id() ); ?>"
																					   data-product_sku="<?php echo esc_attr( $prds->get_sku() ); ?>"
																					   class="button add_to_cart_button ajax_add_to_cart"
																					><?php echo esc_html( $prds->add_to_cart_text() ); ?></a>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="product-desc">
																		<a href="<?php echo get_permalink( $child_variable ); ?>" class="woocommerce-LoopProduct-link">
																			<h2 class="woocommerce-loop-product__title"><?php echo esc_html( $prds->get_name() ); ?></h2>
																		</a>
																		<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																		<span class="price"><?php echo wc_price( $prds->get_price() ); ?></span>
																	</div>
																</div>
															<?php } ?>
															<select name="vi_chosen_product_variable">
																<?php
																foreach ( $children_variable as $child_variable ) {
																	$sp         = wc_get_product( $child_variable );
																	$attributes = $sp->get_attributes();
																	$name_attr  = '';
																	$count      = count( $attributes );
																	$i          = 0;
																	foreach ( $attributes as $attribute ) {
																		if ( ++ $i !== $count ) {
																			$symbol = '-';
																		} else {
																			$symbol = '';
																		}
																		$name_attr .= ucfirst( $attribute ) . $symbol;
																	}
																	echo '<option value="' . $sp->get_id() . '" >' . $name_attr . '</option>';
																}
																?>
															</select>
														</li>
														<?php
													} else { ?>
														<li <?php post_class(); ?>>
															<div class="product-top">
																<a href="<?php esc_url( the_permalink() ); ?>" class="product-image">
																	<?php
																	if ( has_post_thumbnail() ) {
																		echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
																	} else {
																		echo '<img src="' . wc_placeholder_img_src() . '" alt="' . get_the_title() . '" width="150px" height="150px" />';
																	}
																	?>
																</a>

																<div class="controls">
																	<ul class="icon-links">
																		<li class="add-to-cart">
																			<a rel="nofollow" href="<?php echo esc_url( $product->add_to_cart_url() ); ?>"
																			   data-quantity="<?php echo esc_attr( isset( $quantity ) ? $quantity : 1 ); ?>"
																			   data-product_id="<?php echo esc_attr( $product->get_id() ); ?>"
																			   data-product_sku="<?php echo esc_attr( $product->get_sku() ); ?>"
																			   class="button add_to_cart_button ajax_add_to_cart"
																			><?php echo esc_html( $product->add_to_cart_text() ); ?></a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="product-desc">
																<a href="<?php esc_url( the_permalink() ); ?>" class="woocommerce-LoopProduct-link">
																	<h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
																</a>
																<?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
																<span class="price"><?php echo wc_price( $product->get_price() ); ?></span>
															</div>
														</li>
													<?php }
												}
												wp_reset_postdata();
											} ?>
										</ul>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php
			}
		}

		return ob_get_clean();
	}
}