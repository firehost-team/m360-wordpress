<?php

if ( ! class_exists( 'VI_WBOOSTSALES_Frontend_Bundles' ) ) {
	/**
	 * Frontend class.
	 * The class manage all the Frontend behaviors.
	 *
	 */
	class VI_WBOOSTSALES_Frontend_Bundles {

		/**
		 * Single instance of the class
		 *
		 * @var VI_WBOOSTSALES_Frontend_Bundles
		 * @since 1.0.0
		 */
		protected static $instance;

		public $this_is_product = null;

		public $templates = array();

		/**
		 * Returns single instance of the class
		 *
		 * @return VI_WBOOSTSALES_Frontend_Bundles
		 * @since 1.0.0
		 */
		public static function get_instance() {

			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @access public
		 * @since  1.0.0
		 */
		public function __construct() {
		    global $product;

		    if(get_class($product) != 'WC_Product_Yith_Bundle' && get_class($product) != 'VI_WBOOSTSALES_Frontend_Bundles'){
                add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

                // C A R T
                add_action( 'woocommerce_wbs_bundle_add_to_cart', array( $this, 'woocommerce_wbs_bundle_add_to_cart' ) );
                add_filter( 'woocommerce_add_to_cart_validation', array(
                    $this,
                    'woocommerce_add_to_cart_validation'
                ), 10, 6 );

                //add_filter( 'woocommerce_loop_add_to_cart_link', array( $this, 'woocommerce_loop_add_to_cart_link' ), 10, 2 );

                add_filter( 'woocommerce_add_cart_item_data', array( $this, 'woocommerce_add_cart_item_data' ), 10, 2 );
                add_action( 'woocommerce_add_to_cart', array( $this, 'woocommerce_add_to_cart' ), 10, 6 );
                add_filter( 'woocommerce_cart_item_remove_link', array(
                    $this,
                    'woocommerce_cart_item_remove_link'
                ), 10, 2 );

                add_filter( 'woocommerce_cart_item_quantity', array( $this, 'woocommerce_cart_item_quantity' ), 10, 2 );
                add_action( 'woocommerce_after_cart_item_quantity_update', array(
                    $this,
                    'update_cart_item_quantity'
                ), 1, 2 );

                add_action( 'woocommerce_before_cart_item_quantity_zero', array( $this, 'update_cart_item_quantity' ), 1 );

                add_filter( 'woocommerce_cart_item_price', array( $this, 'woocommerce_cart_item_price' ), 99, 3 );
                add_filter( 'woocommerce_cart_item_subtotal', array( $this, 'bundles_item_subtotal' ), 99, 3 );
                add_filter( 'woocommerce_checkout_item_subtotal', array( $this, 'bundles_item_subtotal' ), 10, 3 );
                add_filter( 'woocommerce_add_cart_item', array( $this, 'woocommerce_add_cart_item' ), 10, 2 );
                add_action( 'woocommerce_cart_item_removed', array( $this, 'woocommerce_cart_item_removed' ), 10, 2 );
                add_action( 'woocommerce_cart_item_restored', array( $this, 'woocommerce_cart_item_restored' ), 10, 2 );

                add_filter( 'woocommerce_cart_contents_count', array( $this, 'woocommerce_cart_contents_count' ) );

                add_filter( 'woocommerce_cart_item_class', array( $this, 'table_item_class_bundle' ), 10, 2 );

                add_filter( 'woocommerce_get_cart_item_from_session', array(
                    $this,
                    'woocommerce_get_cart_item_from_session'
                ), 10, 3 );

                // O R D E R
                add_filter( 'woocommerce_order_formatted_line_subtotal', array(
                    $this,
                    'woocommerce_order_formatted_line_subtotal'
                ), 10, 3 );
                add_action( 'woocommerce_add_order_item_meta', array( $this, 'woocommerce_add_order_item_meta' ), 10, 3 );
                add_filter( 'woocommerce_order_item_class', array( $this, 'table_item_class_bundle' ), 10, 2 );

                // S H I P P I N G
                add_filter( 'woocommerce_cart_shipping_packages', array( $this, 'woocommerce_cart_shipping_packages' ) );
            }
		}

		/**
		 * Edit the count of cart contents
		 * exclude bundled items from the count
		 *
		 * @param $count
		 *
		 * @return int
		 */
		public function woocommerce_cart_contents_count( $count ) {
			$cart_contents = WC()->cart->cart_contents;

			$bundled_items_count = 0;
			foreach ( $cart_contents as $cart_item_key => $cart_item ) {
				if ( ! empty( $cart_item['bundled_by'] ) ) {
					$bundled_items_count += $cart_item['quantity'];
				}
			}

			return intval( $count - $bundled_items_count );
		}

		/**
		 * add CSS class in table items in checkout and order
		 *
		 * @access public
		 */
		public function table_item_class_bundle( $classname, $cart_item ) {
			if ( isset( $cart_item['bundled_by'] ) ) {
				return $classname . ' wbs-wcpb-child-of-bundle-table-item';
			} elseif ( isset( $cart_item['cartstamp'] ) ) {
				return $classname . ' wbs-wcpb-bundle-table-item';
			}

			return $classname;
		}

		/**
		 * create item data [create the cartstamp if not exist]
		 */
		public function woocommerce_add_cart_item_data( $cart_item_data, $product_id ) {
			$product = wc_get_product( $product_id );
			if ( ! $product || ! $product->is_type( 'wbs_bundle' ) ) {
				return $cart_item_data;
			}

			/** @var WC_Product_Wbs_Bundle $product */

			if ( isset( $cart_item_data['cartstamp'] ) && isset( $cart_item_data['bundled_items'] ) ) {
				return $cart_item_data;
			}

			$bundled_items = $product->get_bundled_items();
			if ( ! ! $bundled_items ) {

				$cartstamp  = array();
				$variations = $_POST['vi_chosen_product_variable'];

				foreach ( $bundled_items as $bundled_item_id => $bundled_item ) {

					$id                   = $bundled_item->product_id;
					$bundled_product_type = $bundled_item->product->get_type();

					if ( $bundled_product_type == 'variable' && $bundled_item->product->has_child() ) {
						$vars = $bundled_item->product->get_available_variations();
						if ( count( $vars ) && is_array( $variations ) && count( $variations ) ) {
							foreach ( $vars as $var ) {
								if ( in_array( $var['variation_id'], $variations ) ) {
									$id                   = $var['variation_id'];
									$bundled_product_type = 'variation';
								}
							}
						}
					}

					$bundled_product_quantity = isset ( $_REQUEST[ apply_filters( 'woocommerce_product_wbs_bundle_field_prefix', '', $product_id ) . 'wbs_bundle_quantity_' . $bundled_item_id ] ) ? absint( $_REQUEST[ apply_filters( 'woocommerce_product_wbs_bundle_field_prefix', '', $product_id ) . 'wbs_bundle_quantity_' . $bundled_item_id ] ) : $bundled_item->get_quantity();

					$cartstamp[ $bundled_item_id ]['product_id'] = $id;
					$cartstamp[ $bundled_item_id ]['type']       = $bundled_product_type;
					$cartstamp[ $bundled_item_id ]['quantity']   = $bundled_product_quantity;
					$cartstamp[ $bundled_item_id ]               = apply_filters( 'woocommerce_wbs_bundled_item_cart_item_identifier', $cartstamp[ $bundled_item_id ], $bundled_item_id );
				}

				$cart_item_data['cartstamp']     = $cartstamp;
				$cart_item_data['bundled_items'] = array();
			}

			return $cart_item_data;
		}

		/**
		 * Add to cart for Product Bundle
		 *
		 */
		public function woocommerce_add_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {
			if ( isset( $cart_item_data['cartstamp'] ) && ! isset( $cart_item_data['bundled_by'] ) ) {
				//				print_r($cart_item_data);die;
				$bundled_items_cart_data = array( 'bundled_by' => $cart_item_key );

				foreach ( $cart_item_data['cartstamp'] as $bundled_item_id => $bundled_item_stamp ) {
					$bundled_item_cart_data                    = $bundled_items_cart_data;
					$bundled_item_cart_data['bundled_item_id'] = $bundled_item_id;

					$item_quantity = $bundled_item_stamp['quantity'];
					$i_quantity    = $item_quantity * $quantity;
					$prod_id       = $bundled_item_stamp['product_id'];

					$bundled_item_cart_key = $this->bundled_add_to_cart( $product_id, $prod_id, $i_quantity, $variation_id, '', $bundled_item_cart_data );

					if ( $bundled_item_cart_key && ! in_array( $bundled_item_cart_key, WC()->cart->cart_contents[ $cart_item_key ]['bundled_items'] ) ) {
						WC()->cart->cart_contents[ $cart_item_key ]['bundled_items'][] = $bundled_item_cart_key;
						WC()->cart->cart_contents[ $cart_item_key ]['wbs_parent'][]    = $cart_item_key;
					}
				}
			}
		}

		/**
		 * add single bundled item to cart
		 *
		 */
		public function bundled_add_to_cart( $bundle_id, $product_id, $quantity = 1, $variation_id = '', $variation = '', $cart_item_data ) {
			if ( $quantity <= 0 ) {
				return false;
			}

			$cart_item_data = ( array ) apply_filters( 'woocommerce_add_cart_item_data', $cart_item_data, $product_id, $variation_id );
			$cart_id        = WC()->cart->generate_cart_id( $product_id, $variation_id, $variation, $cart_item_data );
			$cart_item_key  = WC()->cart->find_product_in_cart( $cart_id );

			if ( 'product_variation' == get_post_type( $product_id ) ) {
				$variation_id = $product_id;
				$product_id   = wp_get_post_parent_id( $variation_id );
			}

			$product_data = wc_get_product( $variation_id ? $variation_id : $product_id );
			wbs_set_prop( $product_data, 'wbs_wcpb_is_bundled', true );

			if ( ! $cart_item_key ) {

				$cart_item_key                              = $cart_id;
				WC()->cart->cart_contents[ $cart_item_key ] = apply_filters( 'woocommerce_add_cart_item', array_merge( $cart_item_data, array(
					'product_id'   => $product_id,
					'variation_id' => $variation_id,
					'variation'    => $variation,
					'quantity'     => $quantity,
					'data'         => $product_data
				) ), $cart_item_key );
			}

			return $cart_item_key;
		}

		/**
		 * remove 'remove link' for bundled product in cart
		 */
		public function woocommerce_cart_item_remove_link( $link, $cart_item_key ) {
			if ( isset( WC()->cart->cart_contents[ $cart_item_key ]['bundled_by'] ) ) {
				$bundle_cart_key = WC()->cart->cart_contents[ $cart_item_key ]['bundled_by'];
				if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
					return '';
				}
			}

			return $link;
		}

		/**
		 * cart item quantity
		 *
		 */
		public function woocommerce_cart_item_quantity( $quantity, $cart_item_key ) {
			if ( isset( WC()->cart->cart_contents[ $cart_item_key ]['bundled_by'] ) ) {
				return WC()->cart->cart_contents[ $cart_item_key ]['quantity'];
			}

			return $quantity;
		}

		/**
		 * update cart item quantity
		 *
		 */
		public function update_cart_item_quantity( $cart_item_key, $quantity = 0 ) {
			if ( ! empty( WC()->cart->cart_contents[ $cart_item_key ] ) ) {

				if ( $quantity <= 0 ) {
					$quantity = 0;
				} else {
					$quantity = WC()->cart->cart_contents[ $cart_item_key ]['quantity'];
				}

				if ( ! empty( WC()->cart->cart_contents[ $cart_item_key ]['cartstamp'] ) && ! isset( WC()->cart->cart_contents[ $cart_item_key ]['bundled_by'] ) ) {
					$stamp = WC()->cart->cart_contents[ $cart_item_key ]['cartstamp'];
					foreach ( WC()->cart->cart_contents as $key => $value ) {
						if ( isset( $value['bundled_by'] ) && $cart_item_key == $value['bundled_by'] ) {
							$bundle_item_id  = $value['bundled_item_id'];
							$bundle_quantity = $stamp[ $bundle_item_id ]['quantity'];
							WC()->cart->set_quantity( $key, $quantity * $bundle_quantity, false );
						}
					}
				}
			}
		}

		/**
		 * remove cart item price for bundled product
		 *
		 */
		public function woocommerce_cart_item_price( $price, $cart_item, $cart_item_key ) {
			if ( isset( $cart_item['bundled_by'] ) ) {
				$bundle_cart_key = $cart_item['bundled_by'];
				if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
					return '';
				}
			}

			return $price;
		}

		/**
		 * remove cart item subtotal for bundled product
		 */
		public function bundles_item_subtotal( $subtotal, $cart_item, $cart_item_key ) {
			if ( isset( $cart_item['bundled_by'] ) ) {
				$bundle_cart_key = $cart_item['bundled_by'];
				if ( isset( WC()->cart->cart_contents[ $bundle_cart_key ] ) ) {
					return '';
				}
			}
			if ( isset( $cart_item['bundled_items'] ) ) {
				if ( $cart_item['data']->get_price() == 0 ) {
					return '';
				}
			}

			return $subtotal;
		}

		/**
		 * get template for Bundle Product add to cart
		 *
		 */
		public function woocommerce_wbs_bundle_add_to_cart() {
			/** @var WC_Product_Wbs_Bundle $product */
			global $product;
			$bundled_items = $product->get_bundled_items();
			if ( $bundled_items ) {
				wc_get_template( 'single-product/add-to-cart/wbs-bundle.php', array(), '', VI_WBOOSTSALES_TEMPLATES );
			}
		}

		/**
		 * woocommerce loop add to cart link
		 */
		public function woocommerce_loop_add_to_cart_link( $link, $product ) {

			if ( $product->get_type() == 'wbs_bundle' ) {

				if ( $product->is_in_stock() && $product->all_items_in_stock() && ! $product->has_variables() ) {
					return str_replace( 'product_type_bundle', 'product_type_bundle product_type_simple', $link );
				} else {
					return str_replace( 'add_to_cart_button', '', $link );
				}
			}

			return $link;
		}

		/**
		 * woocommerce Validation Bundle Product for add to cart
		 *
		 */
		public function woocommerce_add_to_cart_validation( $add_flag, $product_id, $product_quantity, $variation_id = '', $variations = array(), $cart_item_data = array() ) {
			/** @var WC_Product_Wbs_Bundle $product */
			$product = wc_get_product( $product_id );

			if ( $product->is_type( 'wbs_bundle' ) && get_option( 'woocommerce_manage_stock' ) == 'yes' ) {
				$bundled_items = $product->get_bundled_items();
				foreach ( $bundled_items as $bundled_item ) {
					/** @var WBS_WC_Bundled_Item $bundled_item */
					$bundled_prod = $bundled_item->get_product();
					if ( ! $bundled_prod->has_enough_stock( intval( $bundled_item->get_quantity() ) * intval( $product_quantity ) ) ) {
						wc_add_notice( __( 'You cannot add this quantity of items, because there are not enough in stock.', 'wbs-woocommerce-product-bundles' ), 'error' );

						return false;
					}
				}
			}


			return $add_flag;
		}

		/**
		 * set bundled product price = 0
		 */
		public function woocommerce_add_cart_item( $cart_item, $cart_key ) {

			$cart_contents = WC()->cart->cart_contents;

			if ( isset( $cart_item['bundled_by'] ) ) {
				$bundle_cart_key = $cart_item['bundled_by'];
				if ( isset( $cart_contents[ $bundle_cart_key ] ) ) {
					$parent                   = $cart_contents[ $bundle_cart_key ]['data'];
					$bundled_item_id          = $cart_item['bundled_item_id'];
					$cart_item['data']->price = 0;
				}
			}

			return $cart_item;
		}

		/**
		 * when a bundle product is removed, its bundled items are removed too.
		 *
		 */
		public function woocommerce_cart_item_removed( $cart_item_key, $cart ) {

			if ( ! empty( $cart->removed_cart_contents[ $cart_item_key ]['bundled_items'] ) ) {

				$bundled_item_cart_keys = $cart->removed_cart_contents[ $cart_item_key ]['bundled_items'];

				foreach ( $bundled_item_cart_keys as $bundled_item_cart_key ) {

					if ( ! empty( $cart->cart_contents[ $bundled_item_cart_key ] ) ) {

						$remove                                                = $cart->cart_contents[ $bundled_item_cart_key ];
						$cart->removed_cart_contents[ $bundled_item_cart_key ] = $remove;

						unset( $cart->cart_contents[ $bundled_item_cart_key ] );

						do_action( 'woocommerce_cart_item_removed', $bundled_item_cart_key, $cart );
					}
				}
			}
		}

		/**
		 * when a bundle product is restored, its bundled items are restored too.
		 *
		 * @access public
		 * @since  1.0.19
		 * @author Leanza Francesco <leanzafrancesco@gmail.com>
		 *
		 * @param         $cart_item_key
		 * @param WC_Cart $cart
		 */
		public function woocommerce_cart_item_restored( $cart_item_key, $cart ) {
			if ( ! empty( $cart->cart_contents[ $cart_item_key ]['bundled_items'] ) ) {
				$bundled_item_cart_keys = $cart->cart_contents[ $cart_item_key ]['bundled_items'];
				foreach ( $bundled_item_cart_keys as $bundled_item_cart_key ) {
					$cart->restore_cart_item( $bundled_item_cart_key );
				}
			}
		}

		/**
		 * get cart item from session
		 *
		 */
		public function woocommerce_get_cart_item_from_session( $cart_item, $item_session_values, $cart_item_key ) {
			$cart_contents = ! empty( WC()->cart ) ? WC()->cart->cart_contents : '';
			if ( isset( $item_session_values['bundled_items'] ) && ! empty( $item_session_values['bundled_items'] ) ) {
				$cart_item['bundled_items'] = $item_session_values['bundled_items'];
			}

			if ( isset( $item_session_values['cartstamp'] ) ) {
				$cart_item['cartstamp'] = $item_session_values['cartstamp'];
			}

			if ( isset( $item_session_values['bundled_by'] ) ) {
				$cart_item['bundled_by']      = $item_session_values['bundled_by'];
				$cart_item['bundled_item_id'] = $item_session_values['bundled_item_id'];
				$bundle_cart_key              = $cart_item['bundled_by'];

				if ( isset( $cart_contents[ $bundle_cart_key ] ) ) {
					wbs_set_prop( $cart_item['data'], 'price', 0 );
					if ( isset( $cart_item['data']->subscription_sign_up_fee ) ) {
						wbs_set_prop( $cart_item['data'], 'subscription_sign_up_fee', 0 );
					}
				}
			}

			return $cart_item;
		}

		/* -----------------------------------------
		ORDER
		---------------------------------------- */

		/**
		 * delete subtotal for bundled items in order
		 *
		 */
		public function woocommerce_order_formatted_line_subtotal( $subtotal, $item, $order ) {
			if ( isset( $item['bundled_by'] ) ) {
				return '';
			}

			return $subtotal;
		}

		/**
		 * add meta in order
		 *
		 */
		public function woocommerce_add_order_item_meta( $item_id, $values, $cart_item_key ) {
			if ( isset( $values['bundled_by'] ) ) {
				wc_add_order_item_meta( $item_id, '_bundled_by', $values['bundled_by'] );
			} else {
				if ( isset( $values['cartstamp'] ) ) {
					wc_add_order_item_meta( $item_id, '_cartstamp', $values['cartstamp'] );
				}
			}
		}

		public function woocommerce_cart_shipping_packages( $packages ) {

			if ( ! empty( $packages ) ) {
				foreach ( $packages as $package_key => $package ) {
					if ( ! empty( $package['contents'] ) ) {
						foreach ( $package['contents'] as $cart_item => $cart_item_data ) {
							if ( isset( $cart_item_data['bundled_items'] ) ) {
								// SINGULAR SHIPPING
                                if(isset($cart_item_data['wbs_parent']) && is_array($cart_item_data['wbs_parent'])){
                                    foreach ( $cart_item_data['wbs_parent'] as $parent_bundle_key ) {
                                        if ( isset( $package['contents'][ $parent_bundle_key ] ) ) {
                                            unset( $packages[ $package_key ]['contents'][ $parent_bundle_key ] );
                                        }
                                    }
                                }

							}
						}
					}
				}
			}

			return $packages;
		}

		public function show_crossell_html() {
			global $product;
			if ( $product->is_in_stock() ) :
				$product_bundle_price = $product->get_price();
				?>
				<div class="wbs-item-crosssells" id="flexslider-cross-sell">
					<?php do_action( 'woocommerce_before_add_to_cart_form' );
					$bundled_items = $product->get_bundled_items();
					$array_price   = array();

					if ( $bundled_items ) { ?>
						<ul class="wbs-upsells wbs_vi_slides">
							<?php
							foreach ( $bundled_items as $bundled_item ) {
								/**
								 * @var WBS_WC_Bundled_Item $bundled_item
								 */
								$bundled_product = $bundled_item->get_product();
								$array_price[]   = $bundled_product->get_price();
								?>
								<li <?php post_class( 'post-' . $bundled_product->get_id() ); ?>>
									<div class="product-top">
										<div class="product-image">
											<?php echo $bundled_product->get_image(); ?>
										</div>
									</div>
									<div class="product-desc">
										<b class="product-title"><?php echo $bundled_product->get_name(); ?></b>
										<?php echo wc_get_rating_html( $bundled_product->get_average_rating() ); ?>
										<span class="price"><?php echo wc_price( $bundled_product->get_price() ); ?></span>
										<?php if ( $bundled_product->has_child() && $bundled_product->get_type() == 'variable' ) { ?>
											<select name="vi_chosen_product_variable[]">
												<?php

												$children_variable = $bundled_product->get_children();
												foreach ( $children_variable as $child_variable ) {
													$sp         = wc_get_product( $child_variable );
													$attributes = $sp->get_attributes();
													$name_attr  = '';
													$count      = count( $attributes );
													$count      = count( $attributes );
													$i          = 0;
													foreach ( $attributes as $attribute ) {
														if ( ++ $i !== $count ) {
															$symbol = '-';
														} else {
															$symbol = '';
														}
														$name_attr .= ucfirst( $attribute ) . $symbol;
													}
													echo '<option value="' . $sp->get_id() . '" >' . $name_attr . '</option>';
												}

												?>
											</select>
										<?php } ?>
									</div>
								</li>
								<?php
							} ?>
						</ul>
					<?php } ?>
				</div>
				<?php
				$sum_pr     = array_sum( $array_price );
				$save_price = $sum_pr - $product_bundle_price;
				?>
				<div class="vi-crosssells-atc">
					<div class="wbs-crosssells-price">
		<span class="wbs-crs-regular-price">
		<span class="wbs-new-title"><?php esc_html_e( 'Price', 'woocommerce-boost-sales' ) ?></span>
		<strong><span class="money" style="float: none;"><?php echo wc_price( $product->get_price() ); ?></span>
		</strong>
		</span>
						<span class="wbs-crs-sale-price">
		(<span class="wbs-new-title"><?php esc_html_e( 'Save: ', 'woocommerce-boost-sales' ) ?></span>
		<strong><span class="money" style="float: none;"><?php echo wc_price( $save_price ); ?></span></strong>)
						</span>

					</div>

					<div class="wbs-crosssells-button-atc">
						<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
						<?php
						if ( ! $product->is_sold_individually() ) {
							woocommerce_quantity_input(
								array(
									'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
									'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
								)
							);
						}

						?>
						<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />
						<button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>
						<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

						<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
					</div>
				</div>

			<?php endif;

		}


		public
		function enqueue_scripts() {
			$params = new VillaTheme_Admin_Fields();
			$enable = $params->get_option( 'enable' );
			if ( ! $enable ) {
				return false;
			}

			wp_enqueue_style( 'wbs-wcpb-admin-styles', VI_WBOOSTSALES_CSS . 'bundle-frontend.css' );
		}

	}
}
/**
 * Unique access to instance of VI_WBOOSTSALES_Frontend_Bundles class
 *
 * @return \VI_WBOOSTSALES_Frontend_Bundles
 * @since 1.0.0
 */
function VI_WBOOSTSALES_Frontend_Bundles() {
	return VI_WBOOSTSALES_Frontend_Bundles::get_instance();
}

?>