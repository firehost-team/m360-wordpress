<?php

	/*
	 * IVPA Settings
	 */
	class WC_Ivpa_Settings {

		public static function init() {
			add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::ivpa_add_settings_tab', 50 );
			add_action( 'woocommerce_settings_tabs_ivpawoo', __CLASS__ . '::ivpa_settings_tab' );
			add_action( 'woocommerce_update_options_ivpawoo', __CLASS__ . '::ivpa_save_settings' );
			add_action( 'admin_enqueue_scripts', __CLASS__ . '::ivpa_settings_scripts' );
			add_action( 'wp_ajax_ivpa_get_fields', __CLASS__ . '::ivpa_get_fields' );
			add_action( 'wp_ajax_ivpa_get_terms', __CLASS__ . '::ivpa_get_terms' );
		}

		public static function ivpa_settings_scripts( $settings_tabs ) {
			if ( isset($_GET['page'], $_GET['tab']) && ($_GET['page'] == 'wc-settings' || $_GET['page'] == 'woocommerce_settings') && $_GET['tab'] == 'ivpawoo' ) {
				wp_enqueue_style( 'ivpa-style', WC_Improved_Variable_Produt_Attributes::$url_path . '/assets/css/admin.css', '2.4.2' );
				wp_enqueue_script( 'ivpa-admin', WC_Improved_Variable_Produt_Attributes::$url_path . '/assets/js/admin.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-sortable' ), '2.4.2', true );
				$curr_args = array(
					'ajax' => admin_url( 'admin-ajax.php' ),
				);
				wp_localize_script( 'ivpa-admin', 'ivpa', $curr_args );

				if ( function_exists( 'wp_enqueue_media' ) ) {
					wp_enqueue_media();
				}

				wp_enqueue_style('wp-color-picker');
				wp_enqueue_script('wp-color-picker');
			}
		}

		public static function ivpa_add_settings_tab( $settings_tabs ) {
			$settings_tabs['ivpawoo'] = __( 'Improved Variable Product Attributes', 'ivpawoo' );
			return $settings_tabs;
		}

		public static function ivpa_settings_tab() {
			woocommerce_admin_fields( self::ivpa_get_settings( 'get' ) );
		}

		public static function ivpa_save_settings() {

			if ( isset($_POST['ivpa_attr']) ) {

				$ivpa_attrs = array();

				for ( $i = 0; $i < count($_POST['ivpa_attr']); $i++ ) {

					if ( $_POST['ivpa_attr'][$i] !== '' ) {

						$ivpa_attrs['ivpa_attr'][$i] = $_POST['ivpa_attr'][$i];
						$ivpa_attrs['ivpa_title'][$i] = stripslashes($_POST['ivpa_title'][$i]);
						$ivpa_attrs['ivpa_desc'][$i] = stripslashes($_POST['ivpa_desc'][$i]);
						$ivpa_attrs['ivpa_style'][$i] = $_POST['ivpa_style'][$i];
						$ivpa_attrs['ivpa_archive_include'][$i] = isset( $_POST['ivpa_archive_include'][$i] ) ? 'yes' : 'no';

						switch ( $ivpa_attrs['ivpa_style'][$i] ) {

							case 'ivpa_text' :
								$ivpa_attrs['ivpa_custom'][$i]['style'] = $_POST['ivpa_term'][$i]['style'];
								$ivpa_attrs['ivpa_custom'][$i]['normal'] = $_POST['ivpa_term'][$i]['normal'];
								$ivpa_attrs['ivpa_custom'][$i]['active'] = $_POST['ivpa_term'][$i]['active'];
								$ivpa_attrs['ivpa_custom'][$i]['disabled'] = $_POST['ivpa_term'][$i]['disabled'];
								$ivpa_attrs['ivpa_custom'][$i]['outofstock'] = $_POST['ivpa_term'][$i]['outofstock'];
								foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
								}
							break;

							case 'ivpa_color' :
								foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_custom'][$i][$k] = $v;
								}
								foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
								}
							break;

							case 'ivpa_image' :
								foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_custom'][$i][$k] = $v;
								}
								foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
								}
							break;

							case 'ivpa_html' :
								foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_custom'][$i][$k] = stripslashes($v);
								}
								foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
									$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
								}
							break;

							default :
							break;

						}
					}
				}
			}
			else {
				$ivpa_attrs = array();
			}

			woocommerce_update_options( self::ivpa_get_settings( 'update' ) );

			$get_language = WC_Improved_Variable_Produt_Attributes::ivpa_wpml_language();

			if ( $get_language === false ) {
				update_option('wc_ivpa_attribute_customization', $ivpa_attrs);
			}
			else {
				update_option('wc_ivpa_attribute_customization_' . $get_language, $ivpa_attrs);
			}

		}

		public static function ivpa_get_settings( $action = 'get' ) {

			$settings = array();

			if ( $action == 'get' ) {
		?>
			<div id="ivpa_manager" class="ivpa_manager">
				<h3><?php _e( 'Attribute Styles Manager', 'ivpawoo' ); ?></h3>
				<p><?php _e( 'Use the drag and drop manager bellow to customize product attributes appearance.', 'ivpawoo' ); ?></p>
				<div class="ivpa_fields">
					<a href="#" class="ivpa_add_customization button-primary"><?php _e( 'Add Attribute Customization', 'ivpawoo' ); ?></a>
				</div>
				<div class="ivpa_customizations">
			<?php

				$curr_language = WC_Improved_Variable_Produt_Attributes::ivpa_wpml_language();

				if ( isset($_POST['ivpa_attr']) ) {

					$ivpa_attrs = array();

					for ( $i = 0; $i < count($_POST['ivpa_attr']); $i++ ) {

						if ( $_POST['ivpa_attr'][$i] !== '' ) {

							$ivpa_attrs['ivpa_attr'][$i] = $_POST['ivpa_attr'][$i];
							$ivpa_attrs['ivpa_title'][$i] = stripslashes($_POST['ivpa_title'][$i]);
							$ivpa_attrs['ivpa_desc'][$i] = stripslashes($_POST['ivpa_desc'][$i]);
							$ivpa_attrs['ivpa_style'][$i] = $_POST['ivpa_style'][$i];
							$ivpa_attrs['ivpa_archive_include'][$i] = isset( $_POST['ivpa_archive_include'][$i] ) ? 'yes' : 'no';

							switch ( $ivpa_attrs['ivpa_style'][$i] ) {

								case 'ivpa_text' :
									$ivpa_attrs['ivpa_custom'][$i]['style'] = $_POST['ivpa_term'][$i]['style'];
									$ivpa_attrs['ivpa_custom'][$i]['normal'] = $_POST['ivpa_term'][$i]['normal'];
									$ivpa_attrs['ivpa_custom'][$i]['active'] = $_POST['ivpa_term'][$i]['active'];
									$ivpa_attrs['ivpa_custom'][$i]['disabled'] = $_POST['ivpa_term'][$i]['disabled'];
									$ivpa_attrs['ivpa_custom'][$i]['outofstock'] = $_POST['ivpa_term'][$i]['outofstock'];
									foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
									}
								break;

								case 'ivpa_color' :
									foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_custom'][$i][$k] = $v;
									}
									foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
									}
								break;

								case 'ivpa_image' :
									foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_custom'][$i][$k] = $v;
									}
									foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
									}
								break;

								case 'ivpa_html' :
									foreach ( $_POST['ivpa_term'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_custom'][$i][$k] = stripslashes($v);
									}
									foreach ( $_POST['ivpa_tooltip'][$i] as $k => $v ) {
										$ivpa_attrs['ivpa_tooltip'][$i][$k] = $v;
									}
								break;

								default :
								break;

							}
						}
					}

					$curr_customizations = $ivpa_attrs;

				}
				else {
					if ( $curr_language === false ) {
						$curr_customizations = get_option( 'wc_ivpa_attribute_customization', '' );
					}
					else {
						$curr_customizations = get_option( 'wc_ivpa_attribute_customization_' . $curr_language, '' );
					}
				}

				if ( $curr_customizations == '' ) {
					$curr_customizations = array();
				}

				if ( !empty($curr_customizations) ) {

					$attributes = WC_Improved_Variable_Produt_Attributes::ivpa_get_attributes();

					for ( $i = 0; $i < count($curr_customizations['ivpa_attr']); $i++ ) {

						$html = '<div class="ivpa_element" data-id="' . $i . '"><div class="ivpa_manipulate"><a href="#" class="ivpa_attribute_title">' . wc_attribute_label( $curr_customizations['ivpa_attr'][$i] ) . '</a><a href="#" class="ivpa_remove"><i class="ivpa-remove"></i></a><a href="#" class="ivpa_reorder"><i class="ivpa-reorder"></i></a><a href="#" class="ivpa_slidedown"><i class="ivpa-slidedown"></i></a><div class="ivpa_clear"></div></div><div class="ivpa_holder">';

						$html .= '<label><span>' . __( 'Select Attribute', 'ivpawoo' ) . '</span> <select class="ivpa_attr_select ivpa_s_attribute" name="ivpa_attr[' . $i . ']">';

						$html .= '<option value="">' . __('Select Attribute', 'ivpawoo') . '</option>';

						foreach ( $attributes as $k => $v ) {
							$curr_label = wc_attribute_label( $v );
							$html .= '<option value="' . $v . '"' . ( $curr_customizations['ivpa_attr'][$i] == $v ? ' selected="selected"' : '' ) . '>' . $curr_label . '</option>';
						}

						$html .= '</select></label>';

						$html .= '<label><span>' . __( 'Override Attribute Name', 'ivpawoo' ) . '</span> <input type="text" name="ivpa_title[' . $i . ']" value="' . $curr_customizations['ivpa_title'][$i] . '" /></label>';

						$html .= '<label><span>' . __( 'Add Attribute Description' ,'ivpawoo' ) . '</span> <textarea name="ivpa_desc[' . $i . ']">' . $curr_customizations['ivpa_desc'][$i] . '</textarea></label>';

						$html .= '<label><span>' . __( 'Select Attribute Style', 'ivpawoo' ) . '</span> <select class="ivpa_attr_select ivpa_s_style" name="ivpa_style[' . $i . ']">';

						$styles = array(
							'ivpa_text' => __( 'Plain Text', 'ivpawoo' ),
							'ivpa_color' => __( 'Color', 'ivpawoo' ),
							'ivpa_image' => __( 'Thumbnail', 'ivpawoo' ),
							'ivpa_selectbox' => __( 'Select Box', 'ivpawoo' ),
							'ivpa_html' => __( 'HTML', 'ivpawoo' )
						);

						foreach ( $styles as $k => $v ) {
							$html .= '<option value="' . $k . '"' . ( $curr_customizations['ivpa_style'][$i] == $k ? ' selected="selected"' : '' ) . '>' . $v . '</option>';
						}

						$html .= '</select></label>';

						$html .= '<label><input type="checkbox" name="ivpa_archive_include[' . $i . ']"' . ( isset( $curr_customizations['ivpa_archive_include'][$i] ) && $curr_customizations['ivpa_archive_include'][$i] == 'yes' ? ' checked="checked"' : '' ) . ' /> <span class="ivpa_checkbox_desc">' . __( 'Show on Shop/Archives (This only works if the Shop/Archive mode is set to Show Only)', 'ivpawoo' ) . '</span></label>';

						$html .= '<div class="ivpa_terms">';

						$curr_tax = $curr_customizations['ivpa_attr'][$i];
						$curr_style = $curr_customizations['ivpa_style'][$i];

						$catalog_attrs = get_terms( $curr_tax, array( 'hide_empty' => false ) );

						if ( !empty( $catalog_attrs ) && !is_wp_error( $catalog_attrs ) ){

							ob_start();

							switch ( $curr_style ) {

								case 'ivpa_text' :

									?>
										<div class="ivpa_term_style">
											<span class="ivpa_option">
												<?php _e('CSS', 'ivpawoo'); ?>
												<select name="ivpa_term[<?php echo $i; ?>][style]">
											<?php
												$styles = array(
													'ivpa_border' => __( 'Border', 'ivpawoo' ),
													'ivpa_background' => __( 'Background', 'ivpawoo' ),
													'ivpa_round' => __( 'Round', 'ivpawoo' )
												);

												foreach ( $styles as $k => $v ) {
											?>
													<option value="<?php echo $k; ?>"<?php echo ( $curr_customizations['ivpa_custom'][$i]['style'] == $k ? ' selected="selected"' : '' ); ?>><?php echo $v; ?></option>
											<?php
												}
											?>
												</select>
											</span>
											<span class="ivpa_option">
												<?php _e('Normal', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[<?php echo $i; ?>][normal]" value="<?php echo $curr_customizations['ivpa_custom'][$i]['normal']; ?>"/>
											</span>
											<span class="ivpa_option">
												<?php _e('Active', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[<?php echo $i; ?>][active]" value="<?php echo $curr_customizations['ivpa_custom'][$i]['active']; ?>"/>
											</span>
											<span class="ivpa_option">
												<?php _e('Disabled', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[<?php echo $i; ?>][disabled]" value="<?php echo $curr_customizations['ivpa_custom'][$i]['disabled']; ?>"/>
											</span>
											<span class="ivpa_option">
												<?php _e('Out of stock', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[<?php echo $i; ?>][outofstock]" value="<?php echo $curr_customizations['ivpa_custom'][$i]['outofstock']; ?>"/>
											</span>

										</div>
									<?php

									foreach ( $catalog_attrs as $term ) {

									?>
										<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
											<span class="ivpa_option ivpa_option_plaintext">
												<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo ( isset( $curr_customizations['ivpa_tooltip'][$i][$term->slug] ) ? $curr_customizations['ivpa_tooltip'][$i][$term->slug] : '' ); ?>"/>
											</span>
										</div>
									<?php
									}

								break;

								case 'ivpa_color' :

									foreach ( $catalog_attrs as $term ) {

									?>
										<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
											<span class="ivpa_option ivpa_option_color">
												<em><?php echo $term->name . ' ' . __('Color', 'ivpawoo'); ?></em> <input class="ivpa_color" type="text" name="ivpa_term[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo $curr_customizations['ivpa_custom'][$i][$term->slug]; ?>" />
											</span>
											<span class="ivpa_option">
												<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo ( isset( $curr_customizations['ivpa_tooltip'][$i][$term->slug] ) ? $curr_customizations['ivpa_tooltip'][$i][$term->slug] : '' ); ?>"/>
											</span>
										</div>
									<?php
									}

								break;

								case 'ivpa_image' :

									foreach ( $catalog_attrs as $term ) {

									?>
										<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
											<span class="ivpa_option">
												<em><?php echo $term->name . ' ' . __('Image URL', 'ivpawoo'); ?></em> <input type="text" name="ivpa_term[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo $curr_customizations['ivpa_custom'][$i][$term->slug]; ?>"/>
											</span>
											<span class="ivpa_option ivpa_option_button">
												<em><?php _e( 'Add/Upload image', 'ivpawoo' ); ?></em> <a href="#" class="ivpa_upload_media button"><?php _e('Image Gallery', 'ivpawoo'); ?></a>
											</span>
											<span class="ivpa_option">
												<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo ( isset( $curr_customizations['ivpa_tooltip'][$i][$term->slug] ) ? $curr_customizations['ivpa_tooltip'][$i][$term->slug] : '' ); ?>"/>
											</span>
										</div>
									<?php
									}

								break;

								case 'ivpa_html' :

									foreach ( $catalog_attrs as $term ) {

									?>
										<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
											<span class="ivpa_option ivpa_option_text">
												<em><?php echo $term->name . ' ' . __('HTML', 'ivpawoo'); ?></em> <textarea type="text" name="ivpa_term[<?php echo $i; ?>][<?php echo $term->slug; ?>]"><?php echo $curr_customizations['ivpa_custom'][$i][$term->slug]; ?></textarea>
											</span>
											<span class="ivpa_option">
												<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[<?php echo $i; ?>][<?php echo $term->slug; ?>]" value="<?php echo ( isset( $curr_customizations['ivpa_tooltip'][$i][$term->slug] ) ? $curr_customizations['ivpa_tooltip'][$i][$term->slug] : '' ); ?>"/>
											</span>
										</div>
									<?php
									}

								break;

								case 'ivpa_selectbox' :
								?>
									<div class="ivpa_selectbox"><i class="ivpa-warning"></i> <span><?php _e( 'This style has no extra settings!', 'ivpawoo' ); ?></span></div>
								<?php
								break;

								default :
								break;

							}

							$html .= ob_get_clean();

						}

						$html .= '</div>';

						$html .= '</div></div>';

						echo $html;

					}

				}
			?>
				</div>
			</div>
		<?php
			}

			$settings = array(
				'section_settings_title' => array(
					'name' => __( 'General Settings', 'ivpawoo' ),
					'type' => 'title',
					'desc' => __( 'General plugin settings for the Improved Variable Product Attributes for WooCommerce.', 'ivpawoo' )
				),
				'ivpa_archive_enable' => array(
					'name' => __( 'Enable/Disable Attributes On Archives', 'ivpawoo' ),
					'type' => 'checkbox',
					'desc' => __( 'Check this option to enable attribute selection in shop and the product archives.', 'ivpawoo' ),
					'id'   => 'wc_settings_ivpa_archive_enable',
					'default' => 'no'
				),
				'ivpa_archive_mode' => array(
					'name' => __( 'Select Archive Display Mode', 'prdctfltr' ),
					'type' => 'select',
					'desc' => __( 'Select style to use with the attributes on shop and archives pages.', 'prdctfltr' ),
					'id'   => 'wc_settings_ivpa_archive_mode',
					'options' => array(
						'ivpa_showonly' => __( 'Show Only', 'prdctfltr' ),
						'ivpa_selection' => __( 'Enable Selection and Add to Cart', 'prdctfltr' )
					),
					'default' => 'ivpa_selection',
					'css' => 'width:300px;margin-right:12px;'
				),
				'ivpa_single_ajax' => array(
					'name' => __( 'Enable/Disable AJAX Variation Add To Cart', 'ivpawoo' ),
					'type' => 'checkbox',
					'desc' => __( 'Check this option to enable AJAX add to cart for variations on the single product pages.', 'ivpawoo' ),
					'id'   => 'wc_settings_ivpa_single_ajax',
					'default' => 'no'
				),
				'section_settings_end' => array(
					'type' => 'sectionend'
				),
				'section_advanced_title' => array(
					'name' => __( 'Advanced Settings', 'ivpawoo' ),
					'type' => 'title',
					'desc' => __( 'Advanced plugin settings for the Improved Variable Product Attributes for WooCommerce. Actions and selectors.', 'ivpawoo' )
				),
				'ivpa_archive_action' => array(
					'name' => __( 'Override Default Product Archive Action', 'ivpawoo' ),
					'type' => 'text',
					'desc' => __( 'Change default init action on product archives. Use actions initiated in your content-product.php file.', 'ivpawoo' ) . ' (default: woocommerce_after_shop_loop_item )',
					'id'   => 'wc_settings_ivpa_archive_action',
					'default' => ''
				),
				'ivpa_single_action' => array(
					'name' => __( 'Override Default Single Product Action', 'ivpawoo' ),
					'type' => 'text',
					'desc' => __( 'Change default init action on single product pages. Use actions initiated in your content-single-product.php file.', 'ivpawoo' ) . ' (default: woocommerce_before_add_to_cart_form )',
					'id'   => 'wc_settings_ivpa_single_action',
					'default' => ''
				),
				'ivpa_archive_selector' => array(
					'name' => __( 'Override Default Archive Product Selector', 'ivpawoo' ),
					'type' => 'text',
					'desc' => __( 'Change default product selector on archives. Use the product class from your product archive pages.', 'ivpawoo' ) . ' (default: .type-product )',
					'id'   => 'wc_settings_ivpa_archive_selector',
					'default' => ''
				),
				'ivpa_price_selector' => array(
					'name' => __( 'Override Default Archive Price Selector', 'ivpawoo' ),
					'type' => 'text',
					'desc' => __( 'Change default price selector on archives. Use the price class from your product archive pages.', 'ivpawoo' ) . ' (default: .price )',
					'id'   => 'wc_settings_ivpa_price_selector',
					'default' => ''
				),
				'section_advanced_end' => array(
					'type' => 'sectionend'
				),
			);

			return apply_filters( 'wc_ivpa_settings', $settings );
		}

		public static function ivpa_get_fields() {
			$attributes = WC_Improved_Variable_Produt_Attributes::ivpa_get_attributes();

			$html = '';

			$html .= '<label><span>' . __( 'Select Attribute', 'ivpawoo' ) . '</span> <select class="ivpa_attr_select ivpa_s_attribute" name="ivpa_attr[%%]">';

			$html .= '<option value="">' . __('Select Attribute', 'ivpawoo') . '</option>';

			foreach ( $attributes as $k => $v ) {
				$curr_label = wc_attribute_label( $v );
				$html .= '<option value="' . $v . '">' . $curr_label . '</option>';
			}

			$html .= '</select></label>';

			$html .= '<label><span>' . __( 'Override Attribute Name', 'ivpawoo' ) . '</span> <input type="text" name="ivpa_title[%%]" /></label>';

			$html .= '<label><span>' . __( 'Add Attribute Description' ,'ivpawoo' ) . '</span> <textarea name="ivpa_desc[%%]"></textarea></label>';

			$html .= '<label><span>' . __( 'Select Attribute Style', 'ivpawoo' ) . '</span> <select class="ivpa_attr_select ivpa_s_style" name="ivpa_style[%%]">';

			$styles = array(
				'ivpa_text' => __( 'Plain Text', 'ivpawoo' ),
				'ivpa_color' => __( 'Color', 'ivpawoo' ),
				'ivpa_image' => __( 'Thumbnail', 'ivpawoo' ),
				'ivpa_selectbox' => __( 'Select Box', 'ivpawoo' ),
				'ivpa_html' => __( 'HTML', 'ivpawoo' )
			);

			$c=0;
			foreach ( $styles as $k => $v ) {
				$html .= '<option value="' . $k . '" ' . ($c==0?' selected="selected"':'') . '>' . $v . '</option>';
				$c++;
			}

			$html .= '</select></label>';

			$html .= '<label><input type="checkbox" name="ivpa_archive_include[%%]" checked="checked" /> <span class="ivpa_checkbox_desc">' . __( 'Show on Shop/Archives (This only works if the Shop/Archive mode is set to Show Only)', 'ivpawoo' ) . '</span></label>';

			$html .= '<div class="ivpa_terms">';

			foreach ( $attributes as $v ) {

			}

			$html .= '</div>';

			die($html);
			exit;

		}

		public static function ivpa_get_terms() {

			$curr_tax = ( isset($_POST['taxonomy']) ? $_POST['taxonomy'] : '' );
			$curr_style = ( isset($_POST['style']) ? $_POST['style'] : '' );

			if ( $curr_tax == '' || $curr_style == '' ) {
				die();
				exit;
			}

			$catalog_attrs = get_terms( $curr_tax, array( 'hide_empty' => false ) );

			if ( !empty( $catalog_attrs ) && !is_wp_error( $catalog_attrs ) ){

				ob_start();

				switch ( $curr_style ) {

					case 'ivpa_text' :

						?>
							<div class="ivpa_term_style">
								<span class="ivpa_option">
									<?php _e('CSS', 'ivpawoo'); ?>
									<select name="ivpa_term[%%][style]">
								<?php
									$styles = array(
										'ivpa_border' => __( 'Border', 'ivpawoo' ),
										'ivpa_background' => __( 'Background', 'ivpawoo' ),
										'ivpa_round' => __( 'Round', 'ivpawoo' )
									);

									$c=0;
									foreach ( $styles as $k => $v ) {
								?>
										<option value="<?php echo $k; ?>"<?php echo ($c==0?' selected="selected"':''); ?>><?php echo $v; ?></option>
								<?php
										$c++;
									}
								?>
									</select>
								</span>
								<span class="ivpa_option">
									<?php _e('Normal', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[%%][normal]" value="#bbbbbb"/>
								</span>
								<span class="ivpa_option">
									<?php _e('Active', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[%%][active]" value="#333333"/>
								</span>
								<span class="ivpa_option">
									<?php _e('Disabled', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[%%][disabled]" value="#e45050"/>
								</span>
								<span class="ivpa_option">
									<?php _e('Out of stock', 'ivpawoo'); ?> <input class="ivpa_color" type="text" name="ivpa_term[%%][outofstock]" value="#e45050"/>
								</span>

							</div>
						<?php

							foreach ( $catalog_attrs as $term ) {

							?>
								<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
									<span class="ivpa_option ivpa_option_plaintext">
										<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[%%][<?php echo $term->slug; ?>]""/>
									</span>
								</div>
							<?php
							}

					break;


					case 'ivpa_color' :

						foreach ( $catalog_attrs as $term ) {

						?>
							<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
								<span class="ivpa_option ivpa_option_color">
									<em><?php echo $term->name . ' ' . __('Color', 'ivpawoo'); ?></em> <input class="ivpa_color" type="text" name="ivpa_term[%%][<?php echo $term->slug; ?>]" value="#cccccc" />
								</span>
								<span class="ivpa_option">
									<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[%%][<?php echo $term->slug; ?>]""/>
								</span>
							</div>
						<?php
						}

					break;


					case 'ivpa_image' :

						foreach ( $catalog_attrs as $term ) {

						?>
							<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
								<span class="ivpa_option">
									<em><?php echo $term->name . ' ' . __('Image URL', 'ivpawoo'); ?></em> <input type="text" name="ivpa_term[%%][<?php echo $term->slug; ?>]" />
								</span>
								<span class="ivpa_option ivpa_option_button">
									<em><?php _e( 'Add/Upload image', 'ivpawoo' ); ?></em> <a href="#" class="ivpa_upload_media button"><?php _e('Image Gallery', 'ivpawoo'); ?></a>
								</span>
								<span class="ivpa_option">
									<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[%%][<?php echo $term->slug; ?>]""/>
								</span>
							</div>
						<?php
						}

					break;


					case 'ivpa_html' :

						foreach ( $catalog_attrs as $term ) {

						?>
							<div class="ivpa_term" data-term="<?php echo $term->slug; ?>">
								<span class="ivpa_option ivpa_option_text">
									<em><?php echo $term->name . ' ' . __('HTML', 'ivpawoo'); ?></em> <textarea type="text" name="ivpa_term[%%][<?php echo $term->slug; ?>]"></textarea>
								</span>
								<span class="ivpa_option">
									<em><?php echo $term->name . ' ' . __('Tooltip', 'ivpawoo'); ?></em> <input type="text" name="ivpa_tooltip[%%][<?php echo $term->slug; ?>]""/>
								</span>
							</div>
						<?php
						}

					break;

					case 'ivpa_selectbox' :
					?>
						<div class="ivpa_selectbox"><i class="ivpa-warning"></i> <span><?php _e( 'This style has no extra settings!', 'ivpawoo' ); ?></span></div>
					<?php
					break;

					default :
					break;

				}

				$html = ob_get_clean();

				die($html);
				exit;

			}
			else {
				die();
				exit;
			}

		}

	}

	add_action( 'init', 'WC_Ivpa_Settings::init');

?>